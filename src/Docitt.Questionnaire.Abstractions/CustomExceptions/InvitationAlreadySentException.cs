﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class InvitationAlreadySentException : Exception
    {
        public InvitationAlreadySentException()
        {
        }

        public InvitationAlreadySentException(string message) : base(message)
        {
        }

        public InvitationAlreadySentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvitationAlreadySentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}