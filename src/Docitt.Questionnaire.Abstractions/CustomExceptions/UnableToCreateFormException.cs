﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class UnableToCreateFormException : Exception
    {
        public UnableToCreateFormException()
        {
        }

        public UnableToCreateFormException(string message) : base(message)
        {
        }

        public UnableToCreateFormException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnableToCreateFormException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}