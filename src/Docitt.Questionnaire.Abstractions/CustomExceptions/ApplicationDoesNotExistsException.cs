﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class ApplicationDoesNotExistsException : Exception
    {
        public ApplicationDoesNotExistsException()
        {
        }

        public ApplicationDoesNotExistsException(string message) : base(message)
        {
        }

        public ApplicationDoesNotExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ApplicationDoesNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}