﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class UserEmailAlreadyExistsException : Exception
    {
        public UserEmailAlreadyExistsException()
        {
        }

        public UserEmailAlreadyExistsException(string message) : base(message)
        {
        }

        public UserEmailAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserEmailAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}