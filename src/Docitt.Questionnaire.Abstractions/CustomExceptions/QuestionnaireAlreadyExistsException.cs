﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class QuestionnaireAlreadyExistsException : Exception
    {
        public QuestionnaireAlreadyExistsException()
        {
        }

        public QuestionnaireAlreadyExistsException(string message) : base(message)
        {
        }

        public QuestionnaireAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QuestionnaireAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}