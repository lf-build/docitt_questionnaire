﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class ApplicationNotAvailableForThisQuestionnaireException : Exception
    {
        public ApplicationNotAvailableForThisQuestionnaireException()
        {
        }

        public ApplicationNotAvailableForThisQuestionnaireException(string message) : base(message)
        {
        }

        public ApplicationNotAvailableForThisQuestionnaireException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ApplicationNotAvailableForThisQuestionnaireException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}