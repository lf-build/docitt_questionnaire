﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class QuestionSectionDoesNotExistsException : Exception
    {
        public QuestionSectionDoesNotExistsException()
        {
        }

        public QuestionSectionDoesNotExistsException(string message) : base(message)
        {
        }

        public QuestionSectionDoesNotExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QuestionSectionDoesNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}