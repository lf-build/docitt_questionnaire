﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class AnswerNotProvidedException : Exception
    {
        public AnswerNotProvidedException()
        {
        }

        public AnswerNotProvidedException(string message) : base(message)
        {
        }

        public AnswerNotProvidedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AnswerNotProvidedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}