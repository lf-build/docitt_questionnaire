﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class QuestionDoesNotExistsException : Exception
    {
        public QuestionDoesNotExistsException()
        {
        }

        public QuestionDoesNotExistsException(string message) : base(message)
        {
        }

        public QuestionDoesNotExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QuestionDoesNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}