﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class FormAlreadyExistsException : Exception
    {
        public FormAlreadyExistsException()
        {
        }

        public FormAlreadyExistsException(string message) : base(message)
        {
        }

        public FormAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FormAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}