﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class SubSectionAlreadyExistsException : Exception
    {
        public SubSectionAlreadyExistsException()
        {
        }

        public SubSectionAlreadyExistsException(string message) : base(message)
        {
        }

        public SubSectionAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SubSectionAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}