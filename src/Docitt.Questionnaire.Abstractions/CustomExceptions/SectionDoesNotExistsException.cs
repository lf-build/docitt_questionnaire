﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class SectionDoesNotExistsException : Exception
    {
        public SectionDoesNotExistsException()
        {
        }

        public SectionDoesNotExistsException(string message) : base(message)
        {
        }

        public SectionDoesNotExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SectionDoesNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}