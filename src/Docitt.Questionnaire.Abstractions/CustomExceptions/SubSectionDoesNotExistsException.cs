﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class SubSectionDoesNotExistsException : Exception
    {
        public SubSectionDoesNotExistsException()
        {
        }

        public SubSectionDoesNotExistsException(string message) : base(message)
        {
        }

        public SubSectionDoesNotExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SubSectionDoesNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}