﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class QuestionnaireNotFoundException : Exception
    {
        public QuestionnaireNotFoundException()
        {
        }

        public QuestionnaireNotFoundException(string message) : base(message)
        {
        }

        public QuestionnaireNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected QuestionnaireNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}