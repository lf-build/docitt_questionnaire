﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class SectionAlreadyExistsException: Exception
    {
        public SectionAlreadyExistsException()
        {
        }

        public SectionAlreadyExistsException(string message) : base(message)
        {
        }

        public SectionAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SectionAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}