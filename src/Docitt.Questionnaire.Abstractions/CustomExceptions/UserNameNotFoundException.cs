﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class UserNameNotFoundException : Exception
    {
        public UserNameNotFoundException()
        {
        }

        public UserNameNotFoundException(string message) : base(message)
        {
        }

        public UserNameNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UserNameNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}