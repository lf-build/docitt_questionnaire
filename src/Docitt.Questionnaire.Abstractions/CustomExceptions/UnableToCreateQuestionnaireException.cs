﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class UnableToCreateQuestionnaireException : Exception
    {
        public UnableToCreateQuestionnaireException()
        {
        }

        public UnableToCreateQuestionnaireException(string message) : base(message)
        {
        }

        public UnableToCreateQuestionnaireException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnableToCreateQuestionnaireException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}