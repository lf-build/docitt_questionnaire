﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class FormDoesNotExistsException : Exception
    {
        public FormDoesNotExistsException()
        {
        }

        public FormDoesNotExistsException(string message) : base(message)
        {
        }

        public FormDoesNotExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FormDoesNotExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}