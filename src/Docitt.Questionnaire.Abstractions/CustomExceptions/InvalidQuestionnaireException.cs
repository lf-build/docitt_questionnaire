﻿using System;
using System.Runtime.Serialization;

namespace Docitt.Questionnaire
{
    [Serializable]
    public class InvaidQuestionnaireException : Exception
    {
        public InvaidQuestionnaireException()
        {
        }

        public InvaidQuestionnaireException(string message) : base(message)
        {
        }

        public InvaidQuestionnaireException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvaidQuestionnaireException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}