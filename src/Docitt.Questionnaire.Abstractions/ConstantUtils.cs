namespace Docitt.Questionnaire.Abstractions
{
    public static class ConstantUtils
    {
        public const string BorrowerReoInfoKey = "BorrowerREOInfo";
        public const string CoBorrowerReoInfoKey = "CoBorrowerREOInfo";
        public const string BorrowerReoKey = "BorrowerREO";
        public const string BorrowerEmployerIsCurrentKey = "BorrowerEmployerIsCurrent";
        public const string BorrowerIncomeTypeEmployerKey = "BorrowerIncomeTypeEmployer";

        public const string BorrowerIncomeTypeBusinessKey = "BorrowerIncomeTypeBusiness";

        public const string BorrowerIncomeTypeMilitaryPayKey = "BorrowerIncomeTypeMilitaryPay";

        public const string BorrowerIncomeTypeInterestDividentKey = "BorrowerIncomeTypeInterestDivident";

        public const string BorrowerIncomeTypeRentalKey = "BorrowerIncomeTypeRental";
        public const string BorrowerIncomeTypeAlimonyChildKey = "BorrowerIncomeTypeAlimonyChild";

        public const string BorrowerIncomeTypeSocialSecurityKey = "BorrowerIncomeTypeSocialSecurity";

        public const string BorrowerIncomeTypeOtherKey = "BorrowerIncomeTypeOther";

        public const string BorrowerAlimonyChildKey = "BorrowerAlimonyChild";

        #region Income Sub Section name"

        public const string CurrentEmployment = "currentEmployment";

        public const string CurrentEmploymentContact = "currentEmploymentContact";

        public const string BorrowerEmployerContactKey = "BorrowerEmployerContact";

        public const string Employment = "employment";

        public const string EmploymentContact = "employmentContact";

        public const string Business = "business";

        public const string BusinessAnother = "businessAnother";

        public const string InterestDividend = "interestDividend";

        public const string _InterestDividend = "_interestDividend";

        public const string RentalIncome = "rentalIncome";

        public const string _RentalIncome = "_rentalIncome";

        public const string OtherIncome = "otherIncome";

        public const string _OtherIncome = "_otherIncome";

        public const string Alimony = "alimony";
        public const string AlimonySupport = "alimonySupport";
        public const string BorrowerIncomeTypeAlimonySupportKey = "BorrowerIncomeTypeAlimonySupport";

        public const string BorrowerAlimonyChildName = "borroweralimonychildname";
        public const string BorrowerAlimonyChildDOB = "borroweralimonychilddob";

        public const string BorrowerAlimonyChildSupportName = "childsupportname";
        public const string BorrowerAlimonyChildSupportDOB = "childsupportdob";
        
        public const string MilitaryPay = "militaryPay";

        public const string SocialSecurity = "socialSecurity";

        public const string IncomeSearch = "incomeSearch";

        public const string BorrowerCurrentlyWork = "BorrowerCurrentlyWork";

        public const string BorrowerEmployerStartDate = "BorrowerEmployerStartDate";

        public const string BorrowerEmployerEndDate = "BorrowerEmployerEndDate";

        public const string BorrowerEmployerEndDateFalse = "BorrowerEmployerEndDateFalse";

        public const string BorrowerBusinessEndDateFalse = "BorrowerBusinessEndDateFalse";

        public const string BorrowerBusinessStartDate = "BorrowerBusinessStartDate";

        public const string BorrowerBusinessEndDate = "BorrowerBusinessEndDate";
        public const string Calculator = "Calculator";

        public const string RefinanceWorkflow = "Refinance";
        public const string  BorrowerCurrentAddressSame = "BorrowerCurrentAddressSame";

        public const string BorrowerTransactionType = "BorrowerTransactionType";

        public const string PostFixCheckBoxFalse = "CheckBoxFalse";

        public const string PostFixMarried = "Married";

        public const string PostFixCheckBoxTrue = "CheckBoxTrue";


        #endregion Income Sub Section name"

        #region "Section Constant"

       public const string AddCoBorrower = "CoBorrowerList";

        public const string AdditionalResidenceTemplate = "additionalResidence";
        public const string BorrowerAdditionalAddressList = "BorrowerAdditionalAddressList";

        public const string BorrowerMaritalStatusMarried = "BorrowerMaritalStatusMarried";

        public const string BorrowerMaritalStatus = "BorrowerMaritalStatus";

        public const string BorrowerAddSpouseAsCoBorrower = "BorrowerAddSpouseAsCoBorrower";

        public const string SpouseFirstNameMarried = "SpouseFirstNameMarried";

        public const string SpouseLastNameMarried = "SpouseLastNameMarried";

        public const string SpouseBirthDate = "SpouseBirthDate";

        public const string SpouseSSN = "SpouseSSN";

        public const string SpousePreferredEmail = "SpousePreferredEmail";

        public const string JointCreditSubSectionId = "SS4"; //Who is on loan

        public const string RealEstateSectionId = "SS9";

        public const string SpousePhone = "SpousePhone";
        public const string SpouseCellPhone = "SpouseCellPhone";
        
        public const string BorrowerAddAsNewCoBorrower = "BorrowerAddAsNewCoBorrower";

        public const string BorrowerAddAsCoBorrower = "BorrowerAddAsCoBorrower";


        public const string JointCredit =  "Joint Credit";

        public const string Married =  "Married";

        public const string Separated =  "Separated";

         public const string IndividualCredit =  "Individual Credit";

         public const string Unmarried =  "Unmarried";

         public const string CoBorrowerMaritalStatusInfoTemplate = "coBorrowerMaritalStatusInfo";

         public const string BorrowerIndividualOrJointCredit = "BorrowerIndividualOrJointCredit";

        public const string SpouseCommunicationInfoTemplate = "spouseCommunicationInfo";

        public const string SpouseReoInfoTemplate = "spouseREOInfo";

        public const string QuestionBorrowerPreferredEmail = "BorrowerPreferredEmail";

        public const string QuestionSectionNameSpouseResendInvite = "SpouseResendInvite";

         public const string QuestionSectionNameCoBorrowerSpouseResendInvite = "CoBorrowerSpouseResendInvite";

        public const string QuestionSectionNameAddCoBorrower = "AddCoBorrower";

        public const string CoBorrowerSpouseInfoSubSection = "coBorrowerSpouseInfo";

        public const string CoBorrowerSpouseReoInfoSubSection = "coboSpouseREOInfo";

        public const string CoBorrowerSpouseCommunicationInfoSubSection = "coBorrowerSpouseCommunicationInfo";
        public const string LoanOfficerSectionId = "SS11";

        #endregion "Section Constant"

        #region "Questionnaire Configuration subSectionTemplateFieldName"

        public const string ssTemplateFieldNameCoBorrowerResidenceDetail = "coBorrowerResidenceDetail";
        public const string ssTemplateFieldNameBorrowerResidenceDetail = "borrowerResidenceDetail";

        #endregion
    }
}
