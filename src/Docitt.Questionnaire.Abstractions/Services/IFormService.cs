using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IFormService
    {
        Task<IForm> GetActive(string formName);
        Task<IForm> Get(string formName, string version);
        Task<IForm> Add(IForm form);
        Task<IForm> Update(string formName, string version, IForm form);
        Task<List<IForm>> GetAll(string formName);
        Task<List<IForm>> GetAll();
        Task Activate(string formName, string version);
        Task Deactivate(string formName, string version);
    }
}