﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Docitt.Application;
using Docitt.Questionnaire.Abstractions;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireService
    {
        FormType GetFormType(string type);

        #region "Application"

        Task<IApplicationOverAllStatus> GetApplicationStatus();
        Task<List<Dictionary<string, ISummary>>> GetSummary(string temporaryApplicationNumber, IQuestionnaire questionnaire = null, bool isFromLenderDashboard = false, bool isAllowBlankFieldData = false);
        Task<List<Dictionary<string, ISummary>>> GetApplicationSummary(string applicationNumber);
        Task<IApplication> GetApplication(string temporaryApplicationNumber);
        Task<ITransitionFormExtended> GetApplicationLoanDetail(string temporaryApplicationNumber, string userName = null);
        Task<IApplicationResponse> CreateApplication(IQuestionnaire questionnaire,IBorrowerCoBorrowerSpouseInfo borrowerInfo);
        Task<IApplicationCreationResponse> Confirm(string temporaryApplicationNumber);
        Task<bool> CreatePostApplication(string temporaryApplicationNumber);
        Task<bool> UpdateApplicationStatus(string temporaryApplicationNumber, string applicationStatus);
        Task<bool> CompletePostApplication(string applicationNumber);
        
        Task<IUserBasicInfo> GetBasicInformation(string applicationNumber);

        #endregion

        #region "Sections"

        Task<ISection> GetSection(int SectionId);
        Task<ISection> GetSection(string temporaryApplicationNumber, int SectionId);
        Task<ISection> GetRawSection(string temporaryApplicationNumber, int SectionId);
        Task<ISectionListResponse> GetSectionList(string formType, string temporaryApplicationNumber = ""); //(FormType formType);


        Task<bool> AppendQuestionSection(QuestionSection questionSectionRequest, string temporaryApplicationNumber, int sectionId, string subSectionId);
        Task<bool> UpdateSectionCompletionStatus(string temporaryApplicationNumber, int sectionId, bool isCompleted);

        Task<bool> DeleteQuestionSection(string temporaryApplicationNumber, int sectionId, string subSectionId, int questionSectionSeqNo);

        #endregion

        #region "SubSections"       
        Task<bool> AppendSubSections(AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, int sectionId);
        Task<bool> RemoveSubSections(string temporaryApplicationNumber, int sectionId, SubSectionIdList subSectionIds);
        Task<bool> DeleteSubSection(string temporaryApplicationNumber, int sectionId, string subSectionId, IQuestionnaire questionnaire = null);
        
        Task<bool> IsEmailAlreadyExists(string temporaryApplicationNumber, string emailId);

        #endregion

        #region "Events"

        Task<bool> ExecuteEvent(string temporaryApplicationNumber, int sectionId, string subSectionId, string eventName);

        #endregion

        #region "Questionnaires"
        /// <summary>
        /// Get UserState Information for a given application number
        /// </summary>
        /// <param name="applicationNumber">application number</param>
        /// <returns></returns>
        Task<List<IUserAccessedStatus>> GetUserStateInformation(string applicationNumber);
        Task<List<IQuestionnaire>> GetQuestionnaires();
        Task<IQuestionnaire> GetQuestionnaire();
        Task<IQuestionnaire> GetQuestionnaire(string temporaryApplicationNumber, string userName = null);
        Task<IQuestionnaire> GetStarted(string formType, bool allowNew=false);
        Task<bool> UpdateUsernameField(string inviteId);
        Task<IQuestionnaire> GetQuestionnaireStatus();
        Task<IQuestionnaire> GetQuestionnaireStatus(string temporaryApplicationNumber);
//        Task<IQuestion> GetByQuestionId(string questionId);
        Task<ITransitionFormExtended> GetInformationFromQuestionnaire(string temporaryApplicationNumber); //GenerateApplicationFromQuestionnaire();Task<ITransitionFormExtended> GenerateApplicationFromQuestionnaire();
        Task<IQuestion> GetQuestion(string temporaryApplicationNumber, int sectionId, string subSectionId, string questionId, IQuestionnaire questionnaire=null);
        Task<IQuestion> GetLoanPurposeQuestion(string temporaryApplicationNumber);
        Task<Dictionary<string, List<IBankInfo>>> GetBankInfo(string temporaryApplicationNumber);
        Task<IBankAlert> GetBankAlertInfo(string temporaryApplicationNumber, string customerId);

        Task<IQuestionnaire> CreateQuestionnaireAndGetQuestionnaireStatus(string formType);
//        Task<IQuestionnaireService> Add(IQuestionnaireRequest questionnaireRequest);
//        Task<bool> AddQuestion(IQuestion question, string formId, string sectionId);
        Task<string> AddQuestionnaire(FormType formType, bool allowNew=false);
        Task<bool> AddBankInfo(BankInfo bankInfo, string temporaryApplicationNumber);

        Task<bool> UpdateQuestion(IAnswerUpdateRequest questionRequest); //, string temporaryapplicationNumber);          
        Task<bool> UpdatePurposeOfLoanQuestion(string temporaryApplicationNumber, AnswerUpdateRequest questionRequest);
        Task<bool> UpdateQuestionAndStatus(IAnswerUpdateRequest questionRequest);

        Task<Dictionary<string, List<IBankInfo>>> DeleteBankInfo(string temporaryApplicationNumber, string bankInfoId);

        Task<List<Dictionary<string, ISummary>>> GetQuestionnaireSummary(string temporaryApplicationNumber, bool isAllowBlankAnswerField);

        #endregion

        #region "Users"

        Task<List<IBorrowerCoBorrowerInfo>> GetBorrowersAndCoBorrowers(string temporaryApplicationNumber, bool ForApplicantScreen = false, IQuestionnaire questionnaire = null, bool isSpouseFromJointCredit = true, bool getCoborrowerSpouses = false);
        Task<List<IBorrowerCoBorrowerInfo>> GetBorrowersAndCoBorrowersAndSpouse(string temporaryApplicationNumber, bool ForApplicantScreen = false);
        Task<List<IBorrowerCoBorrowerSpouseInfo>> GetBorrowersAndCoBorrowersAndSpouseName(string temporaryApplicationNumber, bool ForApplicantScreen = false, IQuestionnaire questionnaire = null);
        Task<bool> AssignPrimaryBorrower(string temporaryApplicationNumber, int sectionId, string subSectionId);

        #endregion

        #region "Hub"
        Task<InviteHubResponse> AddNewCoborrowerForHub(string temporaryApplicationNumber, AddCoBorrowerModal borrower, bool allowEmail);
        #endregion

        #region All submitted Applications
        Task<List<Docitt.Application.IApplication>> GetAllSubmittedApplications();
        
        #endregion

        #region Refinanace address same
        Task UpdateRefinanceAddressSameQuestions(string temporaryApplicationNumber,int sectionId, string subSectionId);
        #endregion

         Task<bool> RemoveSubSectionAndAnswer(string applicationNumber,SubSectionIdList subSectionIds,int sectionId,string actionType);

        Task<bool> EventReminderInvite(ReminderInviteRequest reminderInviteRequest, string temporaryApplicationNumber, 
            int sectionId, string subSectionId, int questionSectionSeqNo, string questionId, string questionType, string eventName);
    
        Task<IApplicantSignUpStatus> HasUserSignedUp(string temporaryApplicationNumber, string applicantEmailId);
        Task CopyAnswer(string fromApplicationNumber, string toApplicationNumber);
        Task<bool> CopyAnswerForSpouse(string fromApplicationNumber, string toApplicationNumber);
        Task<bool> VerifyApplication(string applicationNumber);

        Task<bool> SendInviteToCoBorrower(SendInviteRequest InviteRequest, string temporaryApplicationNumber, 
            int sectionId, string subSectionId, int questionSectionSeqNo, string questionId);
        Task<bool> JointToIndividualCredit(string applicationNumber);
        Task<bool> RemoveCoBorrower(string applicationNumber);

    }
}