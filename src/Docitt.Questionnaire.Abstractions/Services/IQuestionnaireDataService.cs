﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireDataService
    {
        Task<Object> GetQuestionnaireSectionData(string applicationId,string borrowerId,string sectionName);
        Task<Dictionary<string, object>> GetAllSectionOriginalData(string applicationId, string borrowerId);
    }
}