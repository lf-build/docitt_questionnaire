﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IQuestionSubSectionRepository : IRepository<ISubSection>
    {
        Task<List<ISubSection>> GetAllByApplicationNumber(string applicationNumber);
        Task<List<ISubSection>> GetAllBySectionId(string applicationNumber, int sectionId);
        Task<List<ISubSection>> GetAllBySectionIdWithoutDetail(string applicationNumber, int sectionId);
        Task AddSubSections(string temporaryApplicationNumber,List<ISubSection> sections, bool isExistingApplication = false);
        Task AddSubSections(string temporaryApplicationNumber, int sectionId, List<ISubSection> subsections);
        Task RemoveSubSections(string applicationNumber, int sectionId, List<string> subSectionIds);
        Task UpdateSubSectionWithSubSectionId(IAnswerUpdateRequest answerRequest, ISubSection subSection);
        Task UpdateUserName(string temporaryApplicationNumber, int sectionId, string subSectionId, string userName);
        Task<List<SectionWithSubSectionId>> GetSubSectionList(string applicationNumber,List<string> userNames);    
        Task<string> GetMaritalSubSectionId(string applicationNumber, string userName, string templateName, int sectionId);
        Task<bool> UpdateSubSectionWithTemplateId(string temporaryApplicationNumber, int sectionId, List<string> subSectionIds, string userName, string newTemplateId);

         Task<string> GetSubSectionIdByTemplateName(string applicationNumber, string templateName, int sectionId);
    }
}
