﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireAnswerRepository : IRepository<IQuestionnaireQuestionAnswer>
    {
        Task<List<IQuestionnaireQuestionAnswer>> GetByApplicationNumber(string temporaryApplicationNumber);
        Task<List<IQuestionnaireQuestionAnswer>> GetBySectionId(string temporaryApplicationNumber, int sectionId);
        Task<List<IQuestionnaireQuestionAnswer>> GetByQuestionId(string temporaryApplicationNumber, int sectionId, string subSectionId, string questionId);
        Task InsertOrUpdateAnswer(IQuestionnaireQuestionAnswer questionnaireQuestionAnswer);
        Task InsertAnswers(List<IQuestionnaireQuestionAnswer> questionAnswers);
        Task DeleteQuestionByQuestionId(IQuestionnaireQuestionAnswer questionnaireQuestionAnswer);

        Task Remove(string applicationNumber, int sectionId, List<string> subSectionIds);
    }
}