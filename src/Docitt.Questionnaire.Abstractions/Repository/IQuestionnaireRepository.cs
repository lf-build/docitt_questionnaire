﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireRepository : IRepository<IQuestionnaire>
    {
        Task<IQuestionnaire> GetQuestionnaireWithoutDetail(string temporaryApplicationNumber);
        Task<IQuestionnaire> GetQuestionnaire(string tempApplicationNumber, string userName);
        Task<IQuestionnaire> GetQuestionnaire(string tempApplicationNumber);
        Task<List<IQuestionnaire>> GetQuestionnaires();

        Task<IQuestion> GetQuestionByQuestionId(string temporaryApplicationNumber,
            IAnswerUpdateRequest questionUpdateRequest);

        Task<IQuestionnaire> GetQuestionnaireFromApplicationNumber(string applicationNumber);

        Task<string> GetApplicationNumberGivenUserName(string userName);

        Task<ISection> GetSectionById(string temporaryApplicationNumber, int sectionId);

        Task<bool> AddQuestionnaire(string userId, IForm form, string temporaryApplicationNumber,
            bool allowNew = false);


        Task<bool> UpdateApplicationNumber(string temporaryApplicationNumber, string applicationNumber,
            string userName);


        Task UpdateQuestionnaireStatus(string applicationNumber, int highestSectionReached,
            string highestSubSectionReached, IUserAccessedStatus userStatus,
            int? sectionIdCompleted = null, bool? sectionIdIsCompleted = null);

        Task<IQuestion> UpdateQuestion(IAnswerUpdateRequest answerRequest, IQuestion questionToUpdate);

        Task UpdateStatus(string temporaryApplicationNumber, string userName, FormStatus status,
            int lastAccessedSection, string lastAccessedSubSectionId);

        Task UpdateStatus(string applicationNumber, FormStatus status, string userName);
        Task UpdateUserStates(string temporaryApplicationNumber, List<IUserAccessedStatus> userStates);

        /// <summary>
        /// Update UserState with the Spouse Username
        /// </summary>
        /// <param name="temporaryApplicationNumber">application number</param>
        /// <param name="username">username</param>
        /// <param name="spouseUsername">spouse username</param>
        /// <returns></returns>
        Task UpdateUserStateWithSpouseUsername(string temporaryApplicationNumber, string username, string spouseUsername);

        Task PushUserState(string temporaryApplicationNumber, IUserAccessedStatus userState);

        Task UpdateLastAccessedStatus(string temporaryApplicationNumber, string userName, int sectionId,
            string subSectionId);

        Task AssignLoanOfficerToQuestionnaire(string temporaryApplicationNumber, string loanOfficerUserName);

        Task<bool> RemoveFromUserState(string applicationNumber, List<string> userList);

        Task<bool> UpdateSpouseUserNameInUserState(string applicationNumber, string userName);

        Task<bool> UpdateUserEmailIdInUserState(string applicationNumber, string userName, string emailId);
        Task UpdateReferenceApplicationNumber(string referenceApplicationNumber, string temporaryApplicationNumber);

        Task<List<string>> GetMultipleTemporaryApplicationNumberByUserName(string userName);
    }
}
