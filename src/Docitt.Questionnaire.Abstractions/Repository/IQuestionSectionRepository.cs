﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IQuestionSectionRepository : IRepository<ISection>
    {
        Task<List<ISection>> GetAllApplicationSection(string temporaryApplicationNumber);
        Task<ISection> GetApplicationSection(string temporaryApplicationNumber, int sectionId);
        Task<List<ISection>> GetAllApplicationSectionWithoutDetail(string temporaryApplicationNumber);

        Task UpdateSections(string temporaryApplicationNumber, List<ISection> sections);

        Task UpdateSectionCompleted(int sectionId, string temporaryApplicationNumber, bool isCompleted);

        Task InsertSections(string temporaryApplicationNumber, List<ISection> sections);
    }
}
