﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IQuestionSectionsRepository : IRepository<IQuestionSection>
    {
        Task<List<IQuestionSection>> GetByApplicationNumber(string applicationNumber);
        Task<List<IQuestionSection>> GetBySectionId(string applicationNumber, int sectionId);
        Task<List<IQuestionSection>> GetBySubSectionId(string applicationNumber, int sectionId,string subSectionId);
        Task Add(List<IQuestionSection> questionSections);
        Task UpdateQuestionSection(IAnswerUpdateRequest answerRequest);
        Task UpdateInviteQuestionSection(IAnswerUpdateRequest answerRequest, string visibleStatus="show");
        Task Remove(string applicationNumber, IQuestionSection questionSection, int sectionId, string subSectionId);
        Task Remove(string applicationNumber, int sectionId, string subSectionId);
        Task Remove(string applicationNumber, int sectionId, List<string> subSectionIds);
        new Task Add(IQuestionSection questionSection);
        Task<bool> UpdateResendInviteVisibility(IAnswerUpdateRequest updateRequest, string status);
        Task<bool> UpdateQuestionReadOnly(IAnswerUpdateRequest answerRequest, bool value);

        Task<bool> UpdateQuestionVisibilityStatus(IAnswerUpdateRequest updateRequest, string status);
    }
}