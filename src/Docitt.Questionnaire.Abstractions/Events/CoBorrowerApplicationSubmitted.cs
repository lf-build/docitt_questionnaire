﻿namespace Docitt.Questionnaire.Events
{
    public class CoBorrowerApplicationSubmitted : ApplicationSubmitted
    {
        public CoBorrowerApplicationSubmitted(string temporaryApplicationNumber,ApplicationSubmitted applicationSubmitted):base(temporaryApplicationNumber)
        {
			this.TemporaryApplicationNumber = applicationSubmitted.TemporaryApplicationNumber;
			this.email = applicationSubmitted.email;
			this.borrowerName = applicationSubmitted.borrowerName;
	   		this.borrowerTemplateName = applicationSubmitted.borrowerTemplateName;
	    	this.borrowerTemplateVersion= applicationSubmitted.borrowerTemplateVersion;
	   		this.loanOfficerEmail= applicationSubmitted.loanOfficerEmail;
	    	this.loanOfficerName= applicationSubmitted.loanOfficerName;
	   	    this.loanOfficerTemplateName= applicationSubmitted.loanOfficerTemplateName;
	        this.loanOfficerTemplateVersion= applicationSubmitted.loanOfficerTemplateVersion;
	        this.loanOfficerNMLS= applicationSubmitted.loanOfficerNMLS;
	        this.loanOfficerPhoneNumber= applicationSubmitted.loanOfficerPhoneNumber;
	        this.borrowerLoginURL= applicationSubmitted.borrowerLoginURL;
	        this.loanOfficerLoginURL= applicationSubmitted.loanOfficerLoginURL;
        }
    }
}
