﻿namespace Docitt.Questionnaire.Events
{
    public class ApplicationSubmitted
    {
        public ApplicationSubmitted(string temporaryApplicationNumber)
        {
            TemporaryApplicationNumber = temporaryApplicationNumber;           
        }

        public string TemporaryApplicationNumber { get; set; }

        public string email{get;set;}
	    public string borrowerName {get;set;}
	    public string borrowerTemplateName {get;set;}
	    public string borrowerTemplateVersion {get;set;}
	    public string loanOfficerEmail {get;set;}
	    public string loanOfficerName {get;set;}
	    public string loanOfficerTemplateName {get;set;}
	    public string loanOfficerTemplateVersion {get;set;}
	    public string loanOfficerNMLS {get;set;}
	    public string loanOfficerPhoneNumber {get;set;}
	    public string borrowerLoginURL {get;set;}
	    public string loanOfficerLoginURL {get;set; }
    }
}
