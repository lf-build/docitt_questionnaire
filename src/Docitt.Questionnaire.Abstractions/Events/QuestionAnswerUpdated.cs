﻿namespace Docitt.Questionnaire.Events
{
    public class QuestionAnswerUpdated
    {
        public string EntityId { get; set; }
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionId { get; set; }
        public string QuestionFieldName { get; set; }
        public string QuestionText { get; set; }
        public string OldAnswer { get; set; }
        public string NewAnswer { get; set; }
    }
}
