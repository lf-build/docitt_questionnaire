﻿namespace Docitt.Questionnaire.Events
{
    public class QuestionnaireApplicationCreated
    {
        public QuestionnaireApplicationCreated(string temporaryApplicationNumber)
        {
            TemporaryApplicationNumber = temporaryApplicationNumber;
        }

        public string TemporaryApplicationNumber { get; set; }
    }
}