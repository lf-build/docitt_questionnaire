﻿namespace Docitt.Questionnaire.Events
{
    public class SpouseCopiedSuccessFully
    {
        public SpouseCopiedSuccessFully(string username, string entityId)
        {
            Username = username;
            EntityId = entityId;            
        }

        public string Username { get; set; }
        public string EntityId { get; set; }
    }
}