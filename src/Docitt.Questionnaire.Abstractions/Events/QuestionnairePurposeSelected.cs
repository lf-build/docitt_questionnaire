namespace Docitt.Questionnaire
{
    public class QuestionnairePurposeSelected
    {
         public QuestionnairePurposeSelected(string temporaryApplicationNumber, string loanPurpose, bool updateStatus)
        {
            TemporaryApplicationNumber = temporaryApplicationNumber;
            LoanPurpose = loanPurpose;
            UpdateStatus = updateStatus;
        }

        public string TemporaryApplicationNumber { get; set; }

        public string LoanPurpose {get; set;}

        public bool UpdateStatus {get; set;}
    }
}