﻿namespace Docitt.Questionnaire.Events
{
    public class QuestionSubSectionRemoved
    {
        public QuestionSubSectionRemoved(string entityId,int sectionId,string subSectionId,string subSectionName)
        {
            EntityId = entityId;
            SubSectionName = subSectionName;
            SectionId = sectionId;
            SubSectionId = subSectionId;
        }

        public string EntityId { get; set; }
        public string SubSectionName { get; set; }
        public int SectionId { get; set; }

        public string SubSectionId { get; set; }
    }
}