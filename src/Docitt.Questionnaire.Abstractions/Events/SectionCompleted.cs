﻿namespace Docitt.Questionnaire.Events
{
    public class SectionCompleted
    {
        public SectionCompleted(
            string temporaryApplicationNumber) : this(temporaryApplicationNumber, null, null, null) { }

        public SectionCompleted(
            string temporaryApplicationNumber,
            string sectionId,
            string sectionName,
            string userName)
        {
            TemporaryApplicationNumber = temporaryApplicationNumber;
            SectionId = sectionId;
            SectionName = sectionName;
            UserName = userName;
        }

        public string TemporaryApplicationNumber { get; set; }

        public string SectionId { get; set; }

        public string SectionName { get; set; }

        public string UserName { get; set; }
    }
}