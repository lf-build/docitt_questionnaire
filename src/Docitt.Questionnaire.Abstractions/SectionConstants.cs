namespace Docitt.Questionnaire
{
    public static class SectionConstants
    {
       
        public const int CoBorrowerInvitationSubSectionSeqNo = 4001;
        public const string CoborrowerInvitationSubSectionId = "ID-f5601c6b-78b8-b78c-947f-63dee6ecb978";       
        public const string CoBorrowerInvitationTemplateName = "coBorrowerInvitationInfo";       
        public const string JointCreditAnswerOption = "Joint Credit";
       
    }
}