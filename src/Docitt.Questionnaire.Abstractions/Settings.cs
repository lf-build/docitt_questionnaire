using System;

namespace Docitt.Questionnaire
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "questionnaire";
    }
}