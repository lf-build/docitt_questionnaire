namespace Docitt.Questionnaire
{
    public static class Constant
    {
        public const string BorrowerIncomeRentRentalIncome = "BorrowerIncomeRentRentalIncome";

        public const string BorrowerREORentalIncome = "BorrowerREORentalIncome";

        public const string InvestmentProperty = "Investment Property";

        public const string BorrowerIncomeRentPropertyUse = "BorrowerIncomeRentPropertyUse";
        public const string BorrowerREOPropertyUse = "BorrowerREOPropertyUse";

        public const string BorrowerCurrentAddressSame = "BorrowerCurrentAddressSame";

        public const string AddressPrepopulateQuestionId = "213";

        public const string BorrowerCurrentStreetAddressCheckBoxFalse = "BorrowerCurrentStreetAddressCheckBoxFalse";

        public const string BorrowerCurrentStreetAddressCheckBoxTrue = "BorrowerCurrentStreetAddressCheckBoxTrue";

        public const string BorrowerTransactionType = "BorrowerTransactionType";

        public const string RefinanceWorkflow = "Refinance";

        public const string RefinanceRentOrOwnCheckBoxTruePostFix = "100";

        public const string RefinanceRentOrOwnCheckBoxFalsePostFix = "200";

        public const string BorrowerPropertyUseQuestionId = "12";

        public const string TemplateIdForRefinanceAddressPrepopulate = "addressPrefill";

        public const string BorrowerCommunication = "BorrowerCommunication";

        public const string BorrowerHomePhone = "BorrowerHomePhone";

        public const string BorrowerCellPhone = "BorrowerCellPhone";

        public const string BorrowerSSN = "BorrowerSSN";

        public const string BorrowerBirthDate = "BorrowerBirthDate";

        public const string Rent = "Rent";

        public const string MonthlyRentalAmount = "MonthlyRentalAmount";

        public const string MonthlyRentalAmountCheckBoxFalse = "MonthlyRentalAmountCheckBoxFalse";

        public const string EmploymentSection = "Employment";

        public const string BusineeSelfEmploymentSection = "BusineeSelfEmploymentSection";
        
        public const string BorrowerEmploymentGap = "BorrowerEmploymentGap";
        public const string BorrowerSelfEmploymentGap = "BorrowerSelfEmploymentGap";

         public const string EmploymentGapSection = "employmentGap";
        public const string SelfEmploymentGapSection = "selfEmploymentGap";
        public const string BorrowerEmployerStartDate = "BorrowerEmployerStartDate";
        public const string BorrowerEmployerEndDateFalse = "BorrowerEmployerEndDateFalse";
        public const string BorrowerBusinessStartDate = "BorrowerBusinessStartDate";

        public const string BorrowerBusinessEndDateFalse = "BorrowerBusinessEndDateFalse";

        public const string BorrowerIncomeAlimonySupport = "BorrowerIncomeAlimonySupport";

        public const string AlimonyAmount = "alimonyAmount";

        public const string AlimonyStartDate = "startDate";

        public const string AlimonyChildSupportSubSection = "Alimony/Child's support";
        
        public const string AlimonyStartDateDisplayText = "Start Date (MM/YYYY)";

        public const string BorrowerIncomeAlimonySupportAmount = "BorrowerIncomeAlimonySupportAmount";
        public const string BorrowerIncomeAlimonySupportStartDate = "BorrowerIncomeAlimonySupportStartDate";

        public const string ChildQuestionSectionTemplateId = "childSupportQuestions";

        public const string TragetPropForDischargeFromActiveDuty = "DischargeFromActiveDuty";

        public const string EnumVACurrentlyRetired = "EnumVACurrentlyRetired";
       
        public const string BorrowerAddSpouseAsCoBorrower = "BorrowerAddSpouseAsCoBorrower";        
    }
}