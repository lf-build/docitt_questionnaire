﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class RequiredConditionRuleParam: IRequiredConditionRuleParam
    {
        public string priority { get; set; }
        public string dueDate { get; set; }
        public string sourceBy { get; set; }
        public string role { get; set; }
        public string entityId { get; set; }
        public bool isCustom { get; set; }
        public List<string> sendTo { get; set; }
        public IApplicationData applicationData { get; set; }   
        public string spouseUserName { get; set; }
        public bool isSpouseUser { get; set; }
        public string UserName { get; set; }
    }     
}