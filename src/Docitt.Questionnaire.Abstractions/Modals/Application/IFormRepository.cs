﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;


namespace Docitt.Questionnaire
{
    public interface IFormRepository : IRepository<IForm>
    {
        Task<IForm> GetActive(string formName);
        Task<IForm> Get(string formName, string version);
        Task Activate(string formName, string version);
        Task Deactivate(string formName, string version);
        Task<List<IForm>> GetAll(string formName);
        Task<List<IForm>> GetAll();        
    }
}