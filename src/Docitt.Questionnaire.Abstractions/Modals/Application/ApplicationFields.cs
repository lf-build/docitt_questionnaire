﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class ApplicationFields : IApplicationFields
    {
        public List<ApplicationFieldMap> Map { get; set; }
    }

    public class QuestionnaireConfig : IQuestionnaireConfig
    {
        //public Dictionary<string, QuestionnaireConfigMap> Map { get; set; }
        public QuestionnaireConfigMap Map { get; set; }
    }


    public class ApplicantSubSectionFields : IApplicantSubSectionFields
    {
        public List<ApplicantSubSectionFieldMap> Map { get; set; }
        public int? SeqNumberRange { get; set; }
    }

    public class QuestionnaireSummaryFields : IQuestionnaireSummaryFields
    {
        public List<QuestionnaireSummaryConfig> Map { get; set; }
    }


    public class ApplicationDynamicSection : IApplicationFields
    {
        public List<ApplicationFieldMap> Map { get; set; }
    }

    public class TenantConfiguration : ITenantConfiguration
    {
        public bool IsRequiredQuestionSkipable  { get; set; }
    }
}
