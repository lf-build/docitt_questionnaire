﻿namespace Docitt.Questionnaire
{
    public class CreditInfo: ICreditInfo
    {     
        public string DateOfBirth { get; set; }

        public string Ssn { get; set; }

        public string Phone { get; set; }

        public string SubSectionId { get; set; }

        public int SectionId { get; set; }

        public string UserName { get; set; }

    }
}
