﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IFormRequest 
    {        
        [JsonConverter(typeof(InterfaceConverter<IForm, Form>))]
        IForm ApplicationForm { get; set; }
    }
}
