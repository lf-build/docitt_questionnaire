﻿namespace Docitt.Questionnaire
{
    public class RequiredConditionRequest: IRequiredConditionRequest
    {
        public string entityType { get; set; }
        public string entityId { get; set; }
        public IRequiredConditionRuleParam payload { get; set; }
    }   
}