﻿namespace Docitt.Questionnaire
{
    public class ConsentRequest : IConsentRequest
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.
        public string entityType { get; set; }

        public string entityId { get; set; }

        public string consentName { get; set; }

        public IConsentRequestPayload payload { get; set; }


    }

    public class ConsentRequestPayload :IConsentRequestPayload
    {
        public string FullName { get; set; }

        public string ApplicationNumber { get; set; }

        public string DateOfBirth { get; set; }

        public string Ssn { get; set; }

        public string PhoneNumber { get; set; }

        public string ipAddress { get; set; }

        public IConsentMetaData metaData { get; set; }

    }

    public class ConsentMetaData : IConsentMetaData
    {
        public string createdBy { get; set; }
    }
}
