﻿namespace Docitt.Questionnaire
{
    public class ApplicationCreationResponse : IApplicationCreationResponse
    {
        public bool Status { get; set; }

        public string ApplicationNumber { get; set; }        
        
    }
}
