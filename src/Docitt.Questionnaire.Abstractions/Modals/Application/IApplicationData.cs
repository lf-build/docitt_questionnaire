﻿namespace Docitt.Questionnaire
{

    public interface IApplicationData
    {
        string purposeLoan { get; set; }
        string residencyType { get; set; }
        string isInContract { get; set; }
        double employmentIncome { get; set; }
        string gender { get; set; }
        double businessSelfEmploymentIncome { get; set; }
        double alimonyorChildSupportAmount { get; set; }
        double militaryPayAmount { get; set; }
        bool isBorrowerVAEligible {get; set;}
        double socialSecurityIncome { get; set; }
        string usCitizen { get; set; }
        string isBankBankrupt { get; set; }
        string transactionClosingStatement { get; set; }
        string propertyType { get; set; }
        bool companyStackLT25Per { get; set; }
        string isLargeDeposit { get; set; }
        bool IsEmploymentGap {get;set;}    
        bool IsSelfEmploymentGap {get;set;}
        bool IsAcknowledgement { get; set; }
        bool DischargeFromActiveDuty { get; set; }
        bool SpouseAsNotACoBorrower { get; set; }

    }
}
