﻿namespace Docitt.Questionnaire
{
    public interface IRequiredConditionRequest
    {
        string entityType { get; set; }
        string entityId { get; set; }
        IRequiredConditionRuleParam payload { get; set; }
    }    
}