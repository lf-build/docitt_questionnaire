﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class FormRequest: IFormRequest 
    {        
        [JsonConverter(typeof(InterfaceConverter<IForm, Form>))]
        public IForm ApplicationForm { get; set; }
    }
}