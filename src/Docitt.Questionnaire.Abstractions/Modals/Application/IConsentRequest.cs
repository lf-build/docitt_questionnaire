﻿namespace Docitt.Questionnaire
{
    public interface IConsentRequest
    {
        string entityType { get; set; }

        string entityId { get; set; }

        string consentName { get; set; }
        IConsentRequestPayload payload { get; set; }

    }

    public interface IConsentRequestPayload
    {
        string FullName { get; set; }

        string ApplicationNumber { get; set; }

        string DateOfBirth { get; set; }

        string Ssn { get; set; }

        string PhoneNumber { get; set; }

        string ipAddress { get; set; }

        IConsentMetaData metaData { get; set; }

    }

    public interface IConsentMetaData
    {
        string createdBy { get; set; }
    }
    
}
