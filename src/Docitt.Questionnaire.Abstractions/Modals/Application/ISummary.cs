﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface ISummary
    {
        List<ISubSectionSummary> SubSectionSummary { get; set; }
        List<IRequiredQuestionDetails> RequiredQuestions { get; set; }

        string ApplicantType {get; set;}

        string SpouseUserName {get; set;}
    }
}
