﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class Summary : ISummary
    {
        public List<ISubSectionSummary> SubSectionSummary { get; set; }
        public List<IRequiredQuestionDetails> RequiredQuestions { get; set; }
        public string ApplicantType {get; set;}
        public string SpouseUserName {get; set;}
    }
}
