﻿namespace Docitt.Questionnaire
{
    public interface IInviteRequest
    {
        string entityType { get; set; }

        string entityId { get; set; }

        IInviteRequestPayload payload { get; set; }

        bool allowEmail {get; set;}

    }

    public interface IInviteRequestPayload
    {
        string InvitedBy { get; set; }
        string InviteEmail { get; set; }
        string InviteFirstName { get; set; } /*TODO: Check if this can be changed to InviteFirstName*/
        string InviteLastName { get; set; }
        string InviteMobileNumber { get; set; }
        string Role { get; set; }
        string Team { get; set; }

    }
}
