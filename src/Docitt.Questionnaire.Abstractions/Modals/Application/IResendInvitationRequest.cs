namespace Docitt.Questionnaire
{
    public interface IResendInvitationRequest
    {
        string invitationId { get; set; }

        string inviteEmailId { get; set; }
    
    }
}