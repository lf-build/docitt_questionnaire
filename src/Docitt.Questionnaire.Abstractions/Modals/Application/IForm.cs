﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public interface IForm:IAggregate
    {
        string FormName { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISection, Section>))]
        List<ISection> Sections { get; set; }
        Dictionary<string, Section> SectionTemplates { get; set; }
        bool IsActive { get; set; }
        string Version { get; set; }
        bool WasEverActive { get; set; }
    }
}
