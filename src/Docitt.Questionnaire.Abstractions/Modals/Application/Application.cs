﻿using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class Application : IApplication
    {
        public string TemporaryApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBorrowerCoBorrowerInfo, BorrowerCoBorrowerInfo>))]
        public List<IBorrowerCoBorrowerInfo> BorrowerCoBorrowerList { get; set; }
        public string LoanPurpose { get; set; }
        public string LoanAmount { get; set; }
        
        [JsonConverter(typeof(InterfaceListConverter<ISectionBasic, SectionBasic>))]
        public List<ISectionBasic> Sections { get; set; }

        public TimeBucket CreatedDate { get; set; }
        public FormStatus Status { get; set; }
    }
}
