﻿namespace Docitt.Questionnaire
{
    public interface IApplicationCreationResponse
    {
        bool Status { get; set; }

        string ApplicationNumber { get; set; }        
        
    }
}
