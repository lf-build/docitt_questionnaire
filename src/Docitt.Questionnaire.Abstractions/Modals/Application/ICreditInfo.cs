﻿namespace Docitt.Questionnaire
{
    public interface ICreditInfo
    {     
        string DateOfBirth { get; set; }

        string Ssn { get; set; }

        string Phone { get; set; }

        string SubSectionId { get; set; }
        
        int SectionId { get; set; }

        string UserName { get; set; }

    }
}
