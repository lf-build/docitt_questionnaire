﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IRequiredConditionRuleParam
    {
        string priority { get; set; }
        string dueDate { get; set; }
        string sourceBy { get; set; }
        string role { get; set; }
        string entityId { get; set; }
        bool isCustom { get; set; }
        List<string> sendTo { get; set; }
        IApplicationData applicationData { get; set; }    
        string spouseUserName { get; set; }
        bool isSpouseUser { get; set; }
        string UserName { get; set; }
    }  
}