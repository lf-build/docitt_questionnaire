﻿namespace Docitt.Questionnaire
{
    public class InviteRequest : IInviteRequest
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.
        public string entityType { get; set; }

        public string entityId { get; set; }

        public IInviteRequestPayload payload { get; set; }

        public bool allowEmail {get; set;}
    }

    public class InviteRequestPayload : IInviteRequestPayload
    {
        public string InvitedBy { get; set; }
        public string InviteEmail { get; set; }
        public string InviteFirstName { get; set; } /*TODO: Check if this can be changed to InviteFirstName*/
        public string InviteLastName { get; set; }
        public string InviteMobileNumber { get; set; }
        public string Role { get; set; }
        public string Team { get; set; }
    }
}
