﻿using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public class QuestionnaireQuestionAnswer : Aggregate, IQuestionnaireQuestionAnswer
    {
        public string TemporaryApplicationNumber { get; set; }
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public int QuestionSectionSeqNo { get; set; }
        public string QuestionId { get; set; }
        public string Answer { get; set; }
    }
}
