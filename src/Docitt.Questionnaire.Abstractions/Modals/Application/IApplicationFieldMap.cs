﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IApplicationFieldMap
    {
        int SectionId { get; set; }
        string QuestionId { get; set; }
        string QuestionFieldNameBorrower {get; set;}
        string QuestionFieldNameCoBorrower {get; set;}
        string TargetProperty { get; set; }
    }

    public interface IApplicantSubSectionFieldMap
    {

        int SectionId { get; set; }
        string SubSectionId { get; set; }
        string TemplateId { get; set; }

        int? SeqNumberRange { get; set; }

        bool IsSectionTemplate { get; set; }

        List<string> ExcludeUserType { get; set; }
    }

    public interface IQuestionnaireConfigMap
    {

        int SectionId { get; set; }
        string SubSectionId { get; set; }
        string QuestionId { get; set; }
        int QuestionSectionSeqNo { get; set; }
        int RealEstateSectionId { get; set;}
        string RealEstateAgentSubSectionId { get; set; }
        string QuestionAnswer {get;set;}
    }
}
