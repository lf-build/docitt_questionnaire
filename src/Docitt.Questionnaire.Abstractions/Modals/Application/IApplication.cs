﻿using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IApplication
    {
        string TemporaryApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBorrowerCoBorrowerInfo, BorrowerCoBorrowerInfo>))]
        List<IBorrowerCoBorrowerInfo> BorrowerCoBorrowerList { get; set; }
        string LoanPurpose { get; set; }
        string LoanAmount { get; set; }
        
        [JsonConverter(typeof(InterfaceListConverter<ISectionBasic, SectionBasic>))]
        List<ISectionBasic> Sections { get; set; }
        TimeBucket CreatedDate { get; set; }
        FormStatus Status { get; set; }
    }
}
