﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class ApplicationFieldMap : IApplicationFieldMap
    {
        public int SectionId { get; set; }
        public string QuestionId { get; set; }
        public string QuestionFieldNameBorrower {get; set;}
        public string QuestionFieldNameCoBorrower {get; set;}
        public string TargetProperty { get; set; }
    }

    public class ApplicantSubSectionFieldMap : IApplicantSubSectionFieldMap
    {
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string TemplateId { get; set; }

        public int? SeqNumberRange { get; set; }
        public bool IsSectionTemplate { get; set; }
        public List<string> ExcludeUserType { get; set; }

    }

    public class QuestionnaireConfigMap : IQuestionnaireConfigMap
    {
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionId { get; set; }
        public int QuestionSectionSeqNo { get; set; }

        public int RealEstateSectionId { get; set; }

        public string RealEstateAgentSubSectionId { get; set; }

        public string QuestionAnswer {get;set;}
    }
}
