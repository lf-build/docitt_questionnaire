﻿namespace Docitt.Questionnaire
{
    public interface ILargeDepositRequest
    {
        string entityType { get; set; }
        string entityId { get; set; }
        ILargeDepositPayload payload { get; set; }

    }

    public interface ILargeDepositPayload
    {
        string customerId { get; set; }
        string applicationNumber { get; set; }      
    }
}