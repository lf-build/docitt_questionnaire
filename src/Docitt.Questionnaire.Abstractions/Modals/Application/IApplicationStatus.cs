﻿namespace Docitt.Questionnaire
{
    public interface IApplicationStatus
    {
        string Review { get; set; }

        string Processing { get; set; }

        string Underwriting { get; set; }

        string Documents { get; set; }

        string Funding { get; set; }

        string ApplicationNumber { get; set; }
    }

    public interface IApplicationOverAllStatus
    {

        string LoanNumber { get; set; }

        string LoanDuration { get; set; }

        string DaysToClose { get; set; }

        string PropertyAddress { get; set; }

        IAddressInfo PropertyAddressInfo { get; set; }

        string Percentage { get; set; }

        string LoanAmount { get; set; }

        IApplicationStatus TabStatus { get; set; }
        string BorrowerFirstName { get; set; }

        string BorrowerLastName { get; set; }

        double DaysLeft { get; set; }

        IAddressInfo  Address { get; set; }
    }

}
