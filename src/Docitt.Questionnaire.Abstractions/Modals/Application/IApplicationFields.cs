﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IApplicationFields
    {
        List<ApplicationFieldMap> Map { get; set; }
    }

    public interface IApplicantSubSectionFields
    {
        List<ApplicantSubSectionFieldMap> Map { get; set; }
        int? SeqNumberRange { get; set; }
    }

   

    public interface IQuestionnaireSummaryFields
    {
        List<QuestionnaireSummaryConfig> Map { get; set; }
    }

    public interface IQuestionnaireConfig
    {
        //Dictionary<string, QuestionnaireConfigMap> Map { get; set; }
        QuestionnaireConfigMap Map { get; set; }
    }

    public interface ITenantConfiguration
    {
        bool IsRequiredQuestionSkipable { get; set; }
    }
}
