﻿using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireQuestionAnswer : IAggregate
    {
        string TemporaryApplicationNumber { get; set; }
        int SectionId { get; set; }
        string SubSectionId { get; set; }
        int QuestionSectionSeqNo { get; set; }
        string QuestionId { get; set; }
        string Answer { get; set; }
    }
}
