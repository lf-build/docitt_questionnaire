﻿namespace Docitt.Questionnaire
{
    public class ApplicationData: IApplicationData
    {
        public string purposeLoan { get; set; }
        public string residencyType { get; set; }
        public string isInContract { get; set; }
        public double employmentIncome { get; set; }
        public string gender { get; set; }
        public double businessSelfEmploymentIncome { get; set; }
        public double alimonyorChildSupportAmount { get; set; }
        public double militaryPayAmount { get; set; }
        public bool isBorrowerVAEligible {get; set;}
        public double socialSecurityIncome { get; set; }
        public string usCitizen { get; set; }
        public string isBankBankrupt { get; set; }
        public string transactionClosingStatement { get; set; }
        public string propertyType { get; set; }
        public bool companyStackLT25Per { get; set; }

        public string isLargeDeposit { get; set; }
        public bool IsEmploymentGap {get;set;}    
        public bool IsSelfEmploymentGap {get;set;}

        public bool IsAcknowledgement { get; set; }

        public bool DischargeFromActiveDuty { get; set; }

        public bool SpouseAsNotACoBorrower { get; set; } = false;

    }
}
