﻿namespace Docitt.Questionnaire
{
    public class LargeDepositRequest: ILargeDepositRequest
    {
        public string entityType { get; set; }
        public string entityId { get; set; }
        public ILargeDepositPayload payload { get; set; }
    }

    public class LargeDepositPayload: ILargeDepositPayload
    {
        public string customerId { get; set; }
        public string applicationNumber { get; set; }       
    }    
}