﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class Form : Aggregate, IForm
    {
        public string FormName { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISection, Section>))]
        public List<ISection> Sections { get; set; }
        public Dictionary<string, Section> SectionTemplates { get; set; }
        public bool IsActive { get; set; }
        public string Version { get; set; }
        public bool WasEverActive { get; set; }
    }
}
