namespace Docitt.Questionnaire
{
     public class ResendInvitationRequest : IResendInvitationRequest
    {
        public string invitationId { get; set; }

        public string inviteEmailId { get; set; }
    }
}