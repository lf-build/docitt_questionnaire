﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class ApplicationStatus: IApplicationStatus
    {
        public string Review { get; set; }

        public string Processing { get; set; }

        public string Underwriting { get; set; }

        public string Documents { get; set; }

        public string Funding { get; set; }

        public string ApplicationNumber { get; set; }
    }

    public class ApplicationOverAllStatus : IApplicationOverAllStatus
    {
        /*public string Review { get; set; }

        public string Processing { get; set; }

        public string Underwriting { get; set; }

        public string Documents { get; set; }

        public string Funding { get; set; }

        public string ApplicationNumber { get; set; }
        */
        public string LoanNumber { get; set; }

        public string DaysToClose { get; set; }

        public string PropertyAddress { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddressInfo, AddressInfo>))]
        public IAddressInfo PropertyAddressInfo { get; set; }

        public string Percentage { get; set; }

        public string LoanAmount { get; set; }

        public IApplicationStatus TabStatus {get; set;}

        public string LoanDuration { get; set; }

        public string BorrowerFirstName { get; set; }

        public string BorrowerLastName { get; set; }

        public double DaysLeft { get; set; }

        public IAddressInfo Address { get; set; }
    }

}
