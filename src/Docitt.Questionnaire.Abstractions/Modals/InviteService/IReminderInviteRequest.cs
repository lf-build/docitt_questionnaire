namespace Docitt.Questionnaire
{
    public interface IReminderInviteRequest
    {
        string FirstName {get; set;}

        string LastName {get; set;}

        string Email {get; set;}

        string Relationship {get; set;}
    }
}