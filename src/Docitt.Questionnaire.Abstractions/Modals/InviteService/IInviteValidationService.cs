using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public interface IInviteValidationService
    {
        bool IsBorrowerSpouseCoApplicant(List<ISubSection> subSectionsSpouse);
        bool IsSpouseTemplateAvailable(ISection profileSection);        
        bool IsBorrowerSpouseAlreadyInvited(ISection profileSection);
        bool IsBorrowerSpouseAlreadySignedup(List<IUserAccessedStatus> userStats, string borrowerUserName);
        IResendInvitationRequest GetBorrowerSpouseInviteDetails(List<IUserAccessedStatus> userStats,AddCoBorrowerModal borrowerSpouseInfo, string borrowerUserName);
    }
}