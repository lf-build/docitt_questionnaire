namespace Docitt.Questionnaire
{
    public class InviteHubResponse
    {
        public bool status {get; set;}
        public Invite invite {get; set;}
        public InviteErrorResponse error {get; set;}
    }
}
