﻿namespace Docitt.Questionnaire
{
    public class AssignmentResponse
    {
        public string Status { get; set; }

        public bool Data { get; set; }
    }
}