namespace Docitt.Questionnaire
{
    public class InviteErrorResponse
    {
        public string Code {get; set;}
        public string Message {get; set;}
    }
}
