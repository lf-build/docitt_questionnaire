namespace Docitt.Questionnaire
{
    public interface IApplicantSignUpStatus
    {
        string ApplicationNumber {get; set;}

        string ApplicantEmailId {get; set;}

        bool HasSignedUp {get; set;}
    }
}