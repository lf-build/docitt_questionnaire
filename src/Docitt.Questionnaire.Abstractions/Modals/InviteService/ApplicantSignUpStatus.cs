namespace Docitt.Questionnaire
{
    public class ApplicantSignUpStatus : IApplicantSignUpStatus
    {
        public string ApplicationNumber {get; set;}

        public string ApplicantEmailId {get; set;}

        public bool HasSignedUp {get; set;}
    }
}