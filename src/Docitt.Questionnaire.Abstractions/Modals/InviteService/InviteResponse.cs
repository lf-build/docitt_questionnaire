﻿namespace Docitt.Questionnaire
{
    public class InviteResponse
    {
        public string Status { get; set; }

        public Invite Data { get; set; }
    }
}
