﻿namespace Docitt.Questionnaire
{
    public class UserApplicationAssignment: IUserApplicationAssignment
    {
        public string entityType { get; set; }
        public string entityId { get; set; }
        public string userName { get; set; }        
    }    
}