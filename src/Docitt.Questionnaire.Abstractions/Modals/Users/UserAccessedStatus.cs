﻿namespace Docitt.Questionnaire
{
    public class UserAccessedStatus : IUserAccessedStatus
    {
        public string UserName { get; set; }
        public string SpouseUserName { get; set; }
        public int LastAccessedSection { get; set; }
        public string LastAccessedSubSection { get; set; }
        public int LastAccessedSectionSeqNo { get; set; }
        public int LastAccessedSubSectionSeqNo { get; set; }
        public string LastAccessedSubSectionId { get; set; }
        public int HighestSectionReached { get; set; }
        public string HighestSubSectionReached { get; set; }
        public FormStatus Status { get;  set;}
        public string InviteId {get; set;}
        public string UserEmailId {get; set;}

        public string ApplicantType {get; set;}
    }
}