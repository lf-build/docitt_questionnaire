﻿namespace Docitt.Questionnaire
{
    public interface IUserApplicationAssignment
    {
        string entityType { get; set; }
        string entityId { get; set; }
        string userName { get; set; }        
    }    
}