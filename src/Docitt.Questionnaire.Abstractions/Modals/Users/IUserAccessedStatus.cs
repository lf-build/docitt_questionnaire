﻿namespace Docitt.Questionnaire
{
    public interface IUserAccessedStatus
    {
        string UserName { get; set; }
        string SpouseUserName { get; set; }
        int LastAccessedSection { get; set; }
        string LastAccessedSubSection { get; set; }
        int LastAccessedSectionSeqNo { get; set; }
        int LastAccessedSubSectionSeqNo { get; set; }
        string LastAccessedSubSectionId { get; set; }
        int HighestSectionReached { get; set; }
        string HighestSubSectionReached { get; set; }
        string InviteId {get; set;}
        string UserEmailId {get; set;}
        string ApplicantType {get; set;}
        FormStatus Status { get; set; }
    }
}