﻿namespace Docitt.Questionnaire
{
    public interface IUserBasicInfo
    {
        string ApplicationNumber { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        string PhoneNumber { get; set; }

        string Email { get; set; }

        string LoanAmount { get; set; }

        string LoanPurpose { get; set; }

        AddressInfo PropertyAddress { get; set; }
        string CellPhoneNumber { get; set; }
    }
}
