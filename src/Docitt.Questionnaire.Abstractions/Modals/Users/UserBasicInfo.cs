﻿namespace Docitt.Questionnaire
{
    public class UserBasicInfo : IUserBasicInfo
    {
        public string ApplicationNumber { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string LoanAmount { get; set; }

        public string LoanPurpose { get; set; }

        public AddressInfo PropertyAddress { get; set; }
        
        public string CellPhoneNumber { get; set; }
    }
}
