﻿namespace Docitt.Questionnaire
{
    public interface IAddQuestionSectionRequest
    {
        string TemporaryApplicationNumber { get; set; }
        int SectionId { get; set; }
        string SubSectionId { get; set; }
        string QuestionSectionId { get; set; }
        QuestionSection QuestionSection { get; set; }
    }
}