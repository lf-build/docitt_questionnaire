﻿namespace Docitt.Questionnaire
{
    public class AddQuestionSectionRequest :IAddQuestionSectionRequest
    {        
        public string TemporaryApplicationNumber { get; set; }
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionSectionId { get; set; }
        public QuestionSection QuestionSection { get; set; }
    }
}