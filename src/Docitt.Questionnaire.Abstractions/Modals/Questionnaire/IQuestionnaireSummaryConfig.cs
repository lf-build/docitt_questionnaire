﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireSummaryConfig
    {
        int SectionId { get; set; }
        string SubSectionId { get; set; }
        string SubSectionName { get; set; }
        string[] QuestionIds { get; set; }
        Dictionary<string,QuestionInfo> QuestionInformation { get; set; }
        string SubSectionType { get; set; }
        string SubSectionTypeInfo { get; set; }
        string subSectionTemplateFieldName { get; set; }
        string CoBorrowerNamePlaceHolder { get; set; }
        string BorrowerNamePlaceHolder { get; set; }
        bool IsVisible { get; set; }
        bool IsHiddenForAffinityPartner { get; set; }
        string[] AllowedZeroOrBlankValues { get; set; }
    }

    public interface IQuestionInfo
    {
        string QuestionId { get; set; }
        string QuestionText { get; set; }
        string DisplayQuestionText { get; set; }
        //if recursive, then get the answer of all the child questions.
        bool Recursive { get; set; }
    }
}