﻿namespace Docitt.Questionnaire
{
    public class ChildConditionalQuestion : IChildConditionalQuestion
    {
        public string Answer { get; set; }
        public string QuestionId { get; set; }
        public string SubSectionId { get; set; }
        public int SectionId { get; set; }
        public int QuestionSectionSeqNo { get; set; }
    }
}