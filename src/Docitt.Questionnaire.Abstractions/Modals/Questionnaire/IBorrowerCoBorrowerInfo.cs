﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IBorrowerCoBorrowerInfo
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        ApplicantType ApplicantType { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string SubSectionId { get; set; }        
        int SectionId { get; set; }        
        string UserName { get; set; } 
        ApplicantType ActualApplicantType { get; set; }
        List<string> Tag { get; set; }
    }
}