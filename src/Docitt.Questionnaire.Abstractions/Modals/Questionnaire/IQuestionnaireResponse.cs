﻿using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireResponse 
    {
        string TemporaryApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IForm, Form>))]
        IForm ApplicationForm { get; set; }
        TimeBucket ApplicationDate { get; set; }
        TimeBucket ExpiryDate { get; set; }        
    }
}