﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public interface IAvailableAns
    {
        string Answer { get; set; }
        string Icon { get; set; }
        int SeqNo { get; set; }
        string EnumFieldName { get; set; }
        string Label { get; set; }
        string Value { get; set; }
        string Key {get;set;}
        string Type {get;set;}

        [JsonConverter(typeof(InterfaceListConverter<IAvailableAns, AvailableAns>))]
        List<IAvailableAns> Childs {get;set;}
    }
}