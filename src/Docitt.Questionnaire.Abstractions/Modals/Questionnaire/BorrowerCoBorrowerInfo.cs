﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class BorrowerCoBorrowerInfo : IBorrowerCoBorrowerInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ApplicantType ApplicantType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string SubSectionId { get; set; }
        public int SectionId { get; set; }
        public string UserName { get; set; }
        public ApplicantType ActualApplicantType { get; set; }
        public List<string> Tag { get; set; }
    }
}