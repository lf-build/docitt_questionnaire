﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class QuestionnaireResponse : IQuestionnaireResponse
    {
        public QuestionnaireResponse() { }

        public QuestionnaireResponse(IQuestionnaire questionnaire)
        {
            
            //QuestionnaireId = questionnaire.QuestionnaireId;
            TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
            ApplicantId = questionnaire.ApplicantId;
            ApplicationForm = questionnaire.ApplicationForm;
            
        }

        public string TemporaryApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IForm, Form>))]
        public IForm ApplicationForm { get; set; }
        public TimeBucket ApplicationDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }        
    }
}