﻿namespace Docitt.Questionnaire
{
    public class LoanCalculationInfo : ILoanCalculationInfo
    {
        public string PurchasePrice { get; set; }
        public string DownPayment { get; set; }
        public string LoanPercentage { get; set; }
        public string LoanAmount { get; set; }
    }
}