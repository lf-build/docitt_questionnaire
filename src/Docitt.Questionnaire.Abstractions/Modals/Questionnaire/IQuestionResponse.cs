﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public interface IQuestionResponse :IAggregate
    {     
        string TemporaryApplicationNumber { get; set; }
        string ApplicantId { get; set; }         
        TimeBucket ApplicationDate { get; set; }
        TimeBucket ExpiryDate { get; set; }        
    }
}