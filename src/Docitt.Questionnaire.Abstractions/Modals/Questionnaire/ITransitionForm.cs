﻿namespace Docitt.Questionnaire
{
    public interface ITransitionForm
    {
        IAddressInfo BorrowerResidenceAddress { get; set; }
        string Address { get; set; }
        bool AgreeToTerms { get; set; }
        double AnnualGrossIncome { get; set; }
        string Channel { get; set; }
        string City { get; set; }
        bool ConsentToESign { get; set; }
        string DobDay { get; set; }
        string DobMonth { get; set; }
        string DobYear { get; set; }
        string Email { get; set; }
        string EmploymentStatus { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        string FirstName { get; set; }
        string HomeOwnership { get; set; }
        string LastName { get; set; }
        string LoanAmount { get; set; }
        string LoanPurpose { get; set; }
        string Phone { get; set; }
        string Ssn { get; set; }
        string State { get; set; }
        string WorkflowId { get; set; }
        string Zip { get; set; }
        string DownPayment { get; set; }
        double PurchasePrice { get; set; }        
        ApplicantType ApplicantType { get; set; }
        string UserName { get;  set;}
        bool ResidenceSameAsSpouse { get; set; }
        bool IsSpouse { get; set; }
        IAddressInfo SpouseResidenceAddress { get; set; }
        string PropertyType { get; set; }
        double EmploymentIncome { get; set; }
        double BusinessEmploymentIncome { get; set; }
        double AlimonyAmount { get; set; }
        bool IsBorrowerVAEligible { get;  set;}
        double MilitaryPayAmount { get; set; }
        double SocialSecurityIncome { get; set; }
        string IsBankrupt { get; set; }
        string isTransactionClosingStatement { get; set; }
        string Gender { get; set; }
        string IsInContract { get; set; }
        string ResidencyType { get; set; }
        string UsCitizen { get; set; }
        bool PropertySelected { get;  set;}
        bool companyStackLessThan25Percentage { get; set; }
        string isLargeDeposit { get; set; }
        bool DischargeFromActiveDuty { get; set; }        
    }

    public interface ITransitionFormExtended : ITransitionForm
    {
        IAddressInfo PropertyAddress { get; set; }
        string PropertyCity { get; set; }
        string PropertyZip { get; set; }
        string PropertyState { get; set; }
        string PropertyCounty {get; set;}
        string Line1 { get; set; }
    }
}