﻿namespace Docitt.Questionnaire
{
    public interface IRequiredQuestionDetails
    {
        string QuestionId { get; set; }
        string QuestionText { get; set; }
        int SectionId { get; set; }
        string SubSectionId { get; set; }
        int HighestSectionReached { get; set; }
        string HighestSubSectionReached { get; set; }
        bool IsEmploymentGap { get; set; }
        bool IsMissingCoBorrower { get; set; }
        string EmployementGapPopupText {get;set;}
        string EmployementGapPopupTitle {get;set;}
        IEmploymentGapDetails EmploymentGapPayload { get; set; }
    }

    public interface IEmploymentGapDetails
    {
        int SectionId { get; set; }
        string SubSectionId { get; set; }
        string QuestionId { get; set; }
        int QuestionSectionSeqNo {get; set;}
    }
}