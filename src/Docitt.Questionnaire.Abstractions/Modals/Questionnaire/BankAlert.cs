﻿namespace Docitt.Questionnaire
{
    public class BankAlert : IBankAlert
    {
        public double TotalBankBalance { get; set; }
        public double DisclosedBankBalance { get; set; }
        public double VerifiedBankBalance { get; set; }
        public double LoanAmount { get; set; }
        public double DownPayment { get; set; }
        public double PurchasePrice { get; set; }
    }
}