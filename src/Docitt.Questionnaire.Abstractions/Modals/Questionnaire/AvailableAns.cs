﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class AvailableAns: IAvailableAns
    {
        public string Answer { get; set; }
        public string Icon { get; set; }
        public int SeqNo { get; set; }
        public string EnumFieldName { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public string Key {get;set;}
        public string Type {get;set;}

        [JsonConverter(typeof(InterfaceListConverter<IAvailableAns, AvailableAns>))]
        public List<IAvailableAns> Childs {get;set;}
    }
}