﻿namespace Docitt.Questionnaire
{
    public interface IAutoFillQuestion
    {
        string SectionId { get; set; }
        string SubSectionId { get; set; }
        string QuestionId { get; set; }
        int QuestionSectionSeqNo { get; set; }
    }
}