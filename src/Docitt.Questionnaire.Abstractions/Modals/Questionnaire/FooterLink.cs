﻿namespace Docitt.Questionnaire
{
    public class FooterLink :IFooterLink
    {
        public string Text { get; set; }
        public string TemplateName { get; set; }
        public string Version { get; set; }
    }
}