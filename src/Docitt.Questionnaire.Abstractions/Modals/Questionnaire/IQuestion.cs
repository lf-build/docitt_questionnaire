﻿using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IQuestion
    {
        string QuestionId { get; set; }
        string QuestionFieldName { get; set; }
        string QuestionText { get; set; }
        string QuestionTextBorrower { get; set; }
        string QuestionTextSpouse {get; set;}
        [JsonConverter(typeof(InterfaceListConverter<IAvailableAns, AvailableAns>))]
        List<IAvailableAns> AvailableAns { get; set; }
        List<string> AvailableAnsList { get; set; }
        int SeqNo { get; set; }
        QuestionType QuestionType { get; set; }

        string LoanPurposeWorkflow {get; set;}

        string Required { get; set; }
        string Answer { get; set; }
      
        [JsonConverter(typeof(InterfaceListConverter<IChildConditionalQuestion, ChildConditionalQuestion>))]
        List<IChildConditionalQuestion> ChildQuestions { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDynamicRequired, DynamicRequired>))]
        List<IDynamicRequired> DynamicRequired { get; set; }
        bool IsParentQuestion { get; set; }
        string HelpText { get; set; }
        string Reg { get; set; }
        string Mask { get; set; }
        string HelpTextTrigger { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDynamicTemplate, DynamicTemplate>))]
        List<IDynamicTemplate> DynamicTemplate { get; set; }
        string PlaceHolder { get; set; }
        //string InputPosition { get; set; }
        Dictionary<string, LinkTemplate> QuestionPlaceHolders { get; set; }
        string Validator { get; set; }
        string QuestionItemNo { get; set; } //Used for declaration section
        string ResultQuestionId { get; set; }
        AutoFillQuestion[] AutoFillQuestions { get; set; }
        string DynamicQuestionSectionTemplate { get; set; }
        bool IsReadOnly { get; set; }
        bool IsSensitive { get; set; }
        string QuestionVisibleStatus {get; set;} //Added for Hide feature for Send Invitation link
        string EventName {get; set;} //Added for FE Actions 
        string EventSubscribe {get; set;} //Added to publish event when FE component loads
    }    
}