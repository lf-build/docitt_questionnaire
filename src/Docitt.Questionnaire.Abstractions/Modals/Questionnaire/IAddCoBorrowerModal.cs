namespace Docitt.Questionnaire
{
    public interface IAddCoBorrowerModal
    {
        string FirstName {get; set;}
        string LastName {get; set;}
        string EmailAddress {get; set;}
        string Relationship {get; set;}
        string QuestionId {get; set;}
    }
}