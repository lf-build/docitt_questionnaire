﻿namespace Docitt.Questionnaire
{
    public class AnswerUpdateRequest: IAnswerUpdateRequest
    {
        
        public string TemporaryApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        public int SectionId { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionId { get; set; }
        public string QuestionFieldName {get; set;}
        public string Answer { get; set; }
        public string UserName { get; set; }
        public int QuestionSectionSeqNo { get; set; }
        public string QuestionSectionName{get; set;}
    }
}