﻿namespace Docitt.Questionnaire
{
    public class AutoFillQuestion : IAutoFillQuestion
    {
        public string SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionId { get; set; }
        public int QuestionSectionSeqNo { get; set; }        
    }
}