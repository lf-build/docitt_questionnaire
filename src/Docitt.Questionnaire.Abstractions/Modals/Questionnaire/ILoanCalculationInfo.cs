﻿namespace Docitt.Questionnaire
{
    public interface ILoanCalculationInfo
    {
        string PurchasePrice { get; set; }
        string DownPayment { get; set; }
        string LoanPercentage { get; set; }
        string LoanAmount { get; set; }        
    }
}