﻿namespace Docitt.Questionnaire
{
    public class BankInfo : IBankInfo
    {
        public string BankName { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }
        public string BankInfoId { get; set; }
        public string AccountHolderName { get; set; }
    }
}