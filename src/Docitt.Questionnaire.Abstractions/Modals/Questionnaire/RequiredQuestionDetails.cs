﻿namespace Docitt.Questionnaire
{
    public class RequiredQuestionDetails : IRequiredQuestionDetails
    {
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public int HighestSectionReached { get; set; }
        public string HighestSubSectionReached { get; set; }
        public bool IsEmploymentGap { get; set; }

        public bool IsMissingCoBorrower { get; set; }
        public string EmployementGapPopupText {get;set;}
        public string EmployementGapPopupTitle {get;set;}
        public IEmploymentGapDetails EmploymentGapPayload { get; set; }

        //This is used to display popup when spouse is added but no coborrower added and that time 
        // he/she says no on that popup then need to call obly remove co-borrower information 
        public bool IsMissingCoBorrowerWithSpouse {get;set;}
        
    }

    public class EmploymentGapDetails : IEmploymentGapDetails
    {
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionId { get; set; }
        public int QuestionSectionSeqNo {get; set;}
    }
}