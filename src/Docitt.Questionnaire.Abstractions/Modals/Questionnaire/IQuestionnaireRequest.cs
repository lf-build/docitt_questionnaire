﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireRequest 
    {
        string TemporaryApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IForm, Form>))]
        IForm ApplicationForm { get; set; }
    }
}