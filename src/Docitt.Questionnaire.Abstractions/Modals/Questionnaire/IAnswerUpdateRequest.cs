﻿namespace Docitt.Questionnaire
{
    public interface IAnswerUpdateRequest
    {        
        string TemporaryApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        int SectionId { get; set; }
        string SubSectionName { get; set; }
        string SubSectionId { get; set; }
        string QuestionId { get; set; }
        string QuestionFieldName {get; set;}
        string Answer { get; set; }
        string UserName { get; set; }
        int QuestionSectionSeqNo { get; set; }
        string QuestionSectionName{get; set;}
    }
}