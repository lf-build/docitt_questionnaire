﻿namespace Docitt.Questionnaire
{
    public interface IAddressInfo
    {
        string Address { get; set; }
        string Addressline2 { get; set; }
        //string Line1 { get; set; }
        //string Line2 { get; set; }
        string Line3 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string County {get; set;}
        string Zip { get; set; }
    }
}