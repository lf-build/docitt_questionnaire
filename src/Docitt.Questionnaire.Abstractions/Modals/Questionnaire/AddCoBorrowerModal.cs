using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class AddCoBorrowerModal : IAddCoBorrowerModal
    {
        [JsonProperty("firstName")]
        public string FirstName {get; set;}

        [JsonProperty("lastName")]
        public string LastName {get; set;}

        [JsonProperty("email")]
        public string EmailAddress {get; set;}
        
        [JsonProperty("relationship")]
        public string Relationship {get; set;} 

        public string QuestionId {get; set;}
    }
}