﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class TransitionForm : ITransitionFormExtended
    {
        [JsonConverter(typeof(InterfaceConverter<IAddressInfo, AddressInfo>))]
        public IAddressInfo BorrowerResidenceAddress { get; set; }
        public string Address { get; set; }
        public bool AgreeToTerms { get; set; }
        public double AnnualGrossIncome { get; set; }
        public string Channel { get; set; }
        public string City { get; set; }
        public bool ConsentToESign { get; set; }
        public string DobDay { get; set; }
        public string DobMonth { get; set; }
        public string DobYear { get; set; }
        public string Email { get; set; }
        public string EmploymentStatus { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string FirstName { get; set; }
        public string HomeOwnership { get; set; }
        public string LastName { get; set; }
        public string LoanAmount { get; set; }
        public string LoanPurpose { get; set; }
        public string Phone { get; set; }
        public string Ssn { get; set; }
        public string State { get; set; }
        public string WorkflowId { get; set; }
        public string Zip { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddressInfo, AddressInfo>))]
        public IAddressInfo PropertyAddress { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyZip { get; set; }
        public string PropertyState { get; set; }
        public string PropertyCounty {get; set;}
        public string Line1 { get; set; }
        public string DownPayment { get; set; }
        public double PurchasePrice { get; set; }        
        public ApplicantType ApplicantType { get; set; }
        public string UserName { get; set; }
        public bool ResidenceSameAsSpouse { get; set; }
        public bool IsSpouse { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddressInfo, AddressInfo>))]
        public IAddressInfo SpouseResidenceAddress { get; set; }        
        public string ResidenceType { get; set; }
        public string PropertyType { get; set; }
        public double EmploymentIncome { get; set; }
        public double BusinessEmploymentIncome { get; set; }
        public double AlimonyAmount { get; set; }
        public bool IsBorrowerVAEligible { get; set; }
        public double MilitaryPayAmount { get; set; }
        public double SocialSecurityIncome { get; set; }
        public string IsBankrupt { get; set; }
        public string isTransactionClosingStatement { get; set; }
        public string Gender { get; set; }
        public string IsInContract { get; set; }
        public string ResidencyType { get; set; }
        public string UsCitizen { get; set; }
        public bool PropertySelected { get; set; }
        public bool companyStackLessThan25Percentage { get; set; }
        public string isLargeDeposit { get; set; }
        public bool DischargeFromActiveDuty { get; set; } = false;
    }
}