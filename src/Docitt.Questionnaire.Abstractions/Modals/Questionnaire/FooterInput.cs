﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class FooterInput : IFooterInput
    {
        public string Text { get; set; }
        public string Type { get; set; }
        public string Action { get; set; }
        public DynamicTemplate dynamicTemplate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IFooterInputActionConfig, FooterInputActionConfig>))]
        public IFooterInputActionConfig ActionConfig { get; set; }
    }

    public class FooterInputActionConfig : IFooterInputActionConfig
    {
        public string SkipToTemplateId { get; set; }
    }
}