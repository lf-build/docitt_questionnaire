﻿using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class StartEndDate
    {
        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }
}