﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public class QuestionnaireRequest : IQuestionnaireRequest
    {
        public string TemporaryApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IForm, Form>))]
        public IForm ApplicationForm { get; set; }
    }
}