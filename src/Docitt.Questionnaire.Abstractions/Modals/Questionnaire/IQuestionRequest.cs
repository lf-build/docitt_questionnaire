﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IQuestionRequest : IAggregate
    {        
        string TemporaryApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        int SectionId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IQuestion, Question>))]
        IQuestion QuestionInfo { get; set; }
        string SubSectionName { get; set; }
        string QuestionSectionName { get; set; }
        int LastAccessedSectionSeqNo { get; set; }
        int LastAccessedSubSectionSeqNo { get; set; }
    }
}