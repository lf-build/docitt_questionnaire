 namespace Docitt.Questionnaire
{
    public interface IBorrowerCoBorrowerSpouseInfo : IBorrowerCoBorrowerInfo
    {     
        string SpouseUserName { get; set; } 
    }
}