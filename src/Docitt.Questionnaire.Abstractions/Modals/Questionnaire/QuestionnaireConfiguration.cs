﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class QuestionnaireConfiguration : IQuestionnaireConfiguration
    { 
        public ApplicationFields ApplicationFieldMap { get; set; }
        public ApplicationFields BorrowerSubsectionFieldMap { get; set; }
        public ApplicationFields SpouseSubsectionFieldMap { get; set; }
        public ApplicationFields CoborrowerSubsectionFieldMap { get; set; }
        public ApplicationFields CreditFieldMap { get; set; }
        public QuestionnaireSummaryFields ApplicationSummary { get; set; }
        public QuestionnaireConfig QuestionnaireLoanPurpose { get; set; }
        public ApplicantSubSectionFields ApplicantDynamicSections { get; set; }
        public TenantConfiguration TenantConfiguration { get; set; }
        
        public Dictionary<string,string> Dependencies { get; set; }
        public string Database { get; set; }

        public bool IsManualAssetWorkFlow{get;set;}

        public List<ManualAssetWorkFlowChanges> ManualAssetWorkFlowChangeList {get;set;}

        public List<RefinanceAdressSameDependantFields> RefinanceAdressSameDependantFieldsList{get;set;}
        public string DefaultSectionGroupName {get;set;} = "PROGRESS";

         public string[] TreeViewTextBoxControl { get; set; }

         public string[] RefinanceAddressSameList { get; set; }

         public string[] SpouseInfoQuestionList { get; set; }

         public string GetBankInfoRule { get; set; }

        public string BorrowerTemplateName {get;set;}
        public string BorrowerTemplateVersion  {get;set;}
        public string LoanOfficerTemplateName  {get;set;}
        public string LoanOfficerTemplateVersion  {get;set;}
        public string BorrowerLoginURL  {get;set;}
	    public string LenderLoginURL  {get;set;}

        public string EmploymentGapMessage {get;set;}

        public string SelfEmploymentGapMessage {get;set;}

         public string EmploymentGapTitle {get;set;}

        public string SelfEmploymentGapTitle {get;set;}

        public List<string> CoBorrowerSpouseSectionList {get; set;}

        public string MultipleInviteExpireRule {get;set;}
        public string RemoveApplicantFromApplicationRule {get;set;}

         public string GetInviteInfoByIdRule {get;set;}    
         public Dictionary<string,AnswerCopyConfiguration> AnswerCopyConfigurations { get; set; }

        public AutoSyncQuestionAnswerMap AutoSyncQuestionAnswer {get; set;}

        public QuestionnaireConfig QuestionnaireJointToIndividual { get; set; }

         public string MissingCoBorrowerText {get;set;}      

         public string MissingCoBorrowerTitle {get;set;}

         public QuestionnaireConfig QuestionnaireAddCoBorrowerToNo { get; set; }

    }

    public class AnswerCopyConfiguration
    {
        public Dictionary<int,DynamicSubSectionFilterConfiguration> DynamicSubSectionFilterConfigurations { get; set; }
        public Dictionary<int,List<SectionFilterConfiguration>> CopyAnswerConfigurations { get; set; }
        public Dictionary<string,List<CustomMappingForAnswerCopy>> CustomMappingForAnswerCopies { get; set; }
    }
        

    public class DynamicSubSectionFilterConfiguration
    {
        public List<string> TemplateIds { get; set; }
        public List<string> DeleteExistingTemplates { get; set; }
        public List<string> OnlyIfWorkflowIsSameTemplateIds { get; set; }
    }

    public class TemplateIdConfiguration
    {
        public bool OnlyIfWorkflowIsSame { get; set; }
    }

    public class SectionFilterConfiguration
    {
        public SectionFilterConfiguration(string subSectionId, bool onlyIfWorkflowIsSame=false,List<string> excludeQuestionIds=null)
        {
            SubSectionId = subSectionId;
            OnlyIfWorkflowIsSame = onlyIfWorkflowIsSame;
            ExcludeQuestionIds = excludeQuestionIds;
        }

        public SectionFilterConfiguration()
        {
            
        }
        
        public string SubSectionId { get; set; }        
        public bool OnlyIfWorkflowIsSame { get; set; }
        public List<string> ExcludeQuestionIds { get; set; } 
    }

    public class CustomMappingForAnswerCopy
    {
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public MappingForAnswerCopy Source { get; set; }
        public MappingForAnswerCopy Destination { get; set; }
    }

    public class MappingForAnswerCopy
    {
        public int QuestionSeqNumber { get; set; }
        public string QuestionId { get; set; }
    }
}