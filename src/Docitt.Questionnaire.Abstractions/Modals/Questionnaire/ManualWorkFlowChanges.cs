using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

public interface IManualAssetWorkFlowChanges
    {
        int SectionId { get; set; }
        List<IRemoveDynamicTemplate> RemoveDynamicTemplateList {get;set;}
        string[] UpdatedMessageHeader { get; set; }       
    }

    public class ManualAssetWorkFlowChanges : IManualAssetWorkFlowChanges
    {
       public int SectionId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IRemoveDynamicTemplate, RemoveDynamicTemplate>))]
       public List<IRemoveDynamicTemplate> RemoveDynamicTemplateList {get;set;}
        public string[] UpdatedMessageHeader { get; set; }
       
    }

    public interface IRemoveDynamicTemplate
    {
        string subSectionId {get;set;}
		string QuestionId {get;set;}
        List<string> TemplateIds { get; set; }
    }

     public class  RemoveDynamicTemplate : IRemoveDynamicTemplate
    {
        public string subSectionId {get;set;}
		public string QuestionId {get;set;}
        public List<string> TemplateIds { get; set; }
    }
