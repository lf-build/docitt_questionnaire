﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaire : IAggregate
    {
        string TemporaryApplicationNumber { get; set; }
        string ApplicantId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IApplicationForm, ApplicationForm>))]
        IApplicationForm ApplicationForm { get; set; }
        //This property will hold the ID of the last section that was updated by the user.        
        FormStatus Status { get; set; }
        string UserName { get; set; }
        string ApplicationNumber { get; set; }
        int HighestSectionReached { get; set; }
        string HighestSubSectionReached { get; set; }
        List<IUserAccessedStatus> UserState { get; set; }
        string AssignedLoanOfficerUserName { get; set; }
        TimeBucket CreatedDate { get; set; }
        // This Property Hold the application number from where application data is copied
        // during multiloan workflow .
        string ReferenceApplicationNumber { get; set; }
    }

    public interface IApplicationForm : IForm
    {
        
    }

    public class ApplicationForm : Form,IApplicationForm
    {
        public ApplicationForm(IForm form)
        {
            FormName = form?.FormName;
            Id = form?.Id;
            Sections = form?.Sections;
            SectionTemplates = form?.SectionTemplates;
            TenantId = form?.TenantId;
            Version = form?.Version;            
        }
    }
}