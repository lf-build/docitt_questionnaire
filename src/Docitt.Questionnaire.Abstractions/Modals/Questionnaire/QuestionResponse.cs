﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public class QuestionResponse : Aggregate, IQuestionResponse
    {
        public QuestionResponse(IQuestionRequest question)
        {
            TemporaryApplicationNumber = question.TemporaryApplicationNumber;
            ApplicantId = question.ApplicantId;
        }

        public QuestionResponse(IAnswerUpdateRequest question)
        {
            TemporaryApplicationNumber = question.TemporaryApplicationNumber;
            ApplicantId = question.ApplicantId;
        }

        public string TemporaryApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        public TimeBucket ApplicationDate { get; set; }
        public TimeBucket ExpiryDate { get; set; }        
    }
}