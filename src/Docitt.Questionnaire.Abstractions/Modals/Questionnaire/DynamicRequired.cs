namespace Docitt.Questionnaire
{
    public class DynamicRequired : IDynamicRequired
    {
        public string TriggerAnswer { get; set; }
        public string QuestionId { get; set; }
        public string SubSectionId { get; set; }
        public int SectionId { get; set; }
        public int QuestionSectionSeqNo { get; set; }
    }
}