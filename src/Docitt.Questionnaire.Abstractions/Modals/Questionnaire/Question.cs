﻿using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class Question : IQuestion
    {
        public string QuestionId { get; set; }
        public string QuestionFieldName { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTextBorrower {get; set;}
        public string QuestionTextSpouse {get; set;}
        
        [JsonConverter(typeof(InterfaceListConverter<IAvailableAns, AvailableAns>))]
        public List<IAvailableAns> AvailableAns { get; set; }
        public List<string> AvailableAnsList { get; set; }
        public int SeqNo { get; set; }
        public QuestionType QuestionType { get; set; }
        public string LoanPurposeWorkflow {get; set;}

        public string Required { get; set; }
        public string Answer { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IChildConditionalQuestion, ChildConditionalQuestion>))]
        public List<IChildConditionalQuestion> ChildQuestions { get; set; }
      
       [JsonConverter(typeof(InterfaceListConverter<IDynamicRequired, DynamicRequired>))]
        public List<IDynamicRequired> DynamicRequired { get; set; }
      
        public bool IsParentQuestion { get; set; }
        public string HelpText { get; set; }
        public string Reg { get; set; }
        public string Mask { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDynamicTemplate, DynamicTemplate>))]
        public List<IDynamicTemplate> DynamicTemplate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string HelpTextTrigger { get; set; }
        public string PlaceHolder { get; set; }
      
        public Dictionary<string, LinkTemplate> QuestionPlaceHolders { get; set; }
        public string Validator { get; set; }
        public string QuestionItemNo { get; set; }
        public string ResultQuestionId { get; set; }
        public AutoFillQuestion[] AutoFillQuestions { get; set; }
        public string DynamicQuestionSectionTemplate { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsSensitive { get; set; }
        public string QuestionVisibleStatus {get; set;} 
        public string EventName {get; set;} 
        public string EventSubscribe {get; set;}
    }
}