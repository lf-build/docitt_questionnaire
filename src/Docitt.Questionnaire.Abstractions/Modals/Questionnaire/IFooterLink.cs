﻿namespace Docitt.Questionnaire
{
    public interface IFooterLink
    {
        string Text { get; set; }
        string TemplateName { get; set; }
        string Version { get; set; }
    }
}