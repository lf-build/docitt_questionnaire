﻿using Newtonsoft.Json;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class QuestionRequest : Aggregate, IQuestionRequest
    {
        public string TemporaryApplicationNumber { get; set; }
        public string ApplicantId { get; set; }
        public int SectionId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IQuestion, Question>))]
        public IQuestion QuestionInfo { get; set; }
        public string SubSectionName { get; set; }
        public string QuestionSectionName { get; set; }
        public int LastAccessedSectionSeqNo { get; set; }
        public int LastAccessedSubSectionSeqNo { get; set; }
    }
}