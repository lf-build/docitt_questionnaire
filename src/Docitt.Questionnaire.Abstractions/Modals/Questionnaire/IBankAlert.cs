﻿namespace Docitt.Questionnaire
{
    public interface IBankAlert
    {
        double TotalBankBalance { get; set; }
        double DisclosedBankBalance { get; set; }
        double VerifiedBankBalance { get; set; }
        double LoanAmount { get; set; }
        double DownPayment { get; set; }
        double PurchasePrice { get; set; }
    }
}