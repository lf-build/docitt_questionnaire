﻿namespace Docitt.Questionnaire
{
    public interface IChildConditionalQuestion
    {
        string Answer { get; set; }
        string QuestionId { get; set; }
        string SubSectionId { get; set; }
        int SectionId { get; set; }
        int QuestionSectionSeqNo { get; set; }
    }
}