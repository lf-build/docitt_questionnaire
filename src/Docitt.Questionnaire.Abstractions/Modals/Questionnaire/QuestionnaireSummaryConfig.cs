﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class QuestionnaireSummaryConfig : IQuestionnaireSummaryConfig
    {
        public int SectionId { get; set; }
        public string SubSectionName { get; set; }
        public string[] QuestionIds { get; set; }
        public Dictionary<string, QuestionInfo> QuestionInformation { get; set; }
        public string SubSectionType { get; set; }
        public string SubSectionTypeInfo { get; set; }
        
        public string subSectionTemplateFieldName { get; set; }
        public string CoBorrowerNamePlaceHolder { get; set; }
        public string BorrowerNamePlaceHolder { get; set; }
        public string SubSectionId { get; set; }
        public bool IsVisible { get; set; }
        public bool IsHiddenForAffinityPartner { get; set; }
        public string[] AllowedZeroOrBlankValues { get; set; }
    }

    public class QuestionInfo : IQuestionInfo
    {
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string DisplayQuestionText { get; set; }
        public bool Recursive { get; set; }        
    }
}