﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class Questionnaire : Aggregate, IQuestionnaire
    {
        public string TemporaryApplicationNumber { get; set; }
        public string ApplicantId { get; set; }

        //This is the Application Form that will be shown on the UI
        //It will have answers
        [JsonConverter(typeof(InterfaceConverter<IApplicationForm, ApplicationForm>))]
        public IApplicationForm ApplicationForm { get; set; }

        //Once the application gets filled, system will generate the Application Number, this Application Number will then be used used in the application.
        public string ApplicationNumber { get; set; }
        public FormStatus Status { get; set; }
        public string UserName { get; set; }
        public List<string> users { get; set; }
        public int HighestSectionReached { get; set; }
        public string HighestSubSectionReached { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IUserAccessedStatus, UserAccessedStatus>))]
        public List<IUserAccessedStatus> UserState { get; set; }
        public string AssignedLoanOfficerUserName { get; set; }
        public TimeBucket CreatedDate { get; set; }

        // This Property Hold the application number from where application data is copied
        // during multiloan workflow .
        public string ReferenceApplicationNumber { get; set; }
    }
}