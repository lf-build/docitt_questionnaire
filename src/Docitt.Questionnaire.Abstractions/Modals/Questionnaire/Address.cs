﻿namespace Docitt.Questionnaire
{
    public class AddressInfo : IAddressInfo
    {
        public string Address { get; set; }
        public string Addressline2 { get; set; }
        public string Line3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County {get; set;}
        public string Zip { get; set; }
    }
}