﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public interface IQuestionnaireConfiguration: IDependencyConfiguration
    {     
        ApplicationFields ApplicationFieldMap { get; set; }
        ApplicationFields BorrowerSubsectionFieldMap { get; set; }
        ApplicationFields SpouseSubsectionFieldMap { get; set; }
        ApplicationFields CoborrowerSubsectionFieldMap { get; set; }
        ApplicationFields CreditFieldMap { get; set; }
        QuestionnaireSummaryFields ApplicationSummary { get; set; }
        QuestionnaireConfig QuestionnaireLoanPurpose { get; set; }
        ApplicantSubSectionFields ApplicantDynamicSections { get; set; }
         string[] TreeViewTextBoxControl { get; set; }

         string[] RefinanceAddressSameList { get; set; }

         string[] SpouseInfoQuestionList { get; set; }
         string GetBankInfoRule { get; set; }

        string BorrowerTemplateName {get;set;}
        string BorrowerTemplateVersion  {get;set;}
        string LoanOfficerTemplateName  {get;set;}
        string LoanOfficerTemplateVersion  {get;set;}
        string BorrowerLoginURL  {get;set;}
        string LenderLoginURL  {get;set;}

         string EmploymentGapMessage {get;set;}

         string SelfEmploymentGapMessage {get;set;}

        string EmploymentGapTitle {get;set;}

        string SelfEmploymentGapTitle {get;set;}

        List<string> CoBorrowerSpouseSectionList {get; set;}

        string MultipleInviteExpireRule {get;set;}
        string RemoveApplicantFromApplicationRule {get;set;}

        string GetInviteInfoByIdRule {get;set;}
        
        /// <summary>
        /// AutoSyncQuestionAnswer contains information of copying the Answer from source to destination details 
        /// </summary>
        /// <value></value>
        AutoSyncQuestionAnswerMap AutoSyncQuestionAnswer {get; set;}

        QuestionnaireConfig QuestionnaireJointToIndividual { get; set; }

         string MissingCoBorrowerText {get;set;}      

         string MissingCoBorrowerTitle {get;set;}

        QuestionnaireConfig QuestionnaireAddCoBorrowerToNo { get; set; }

    }

    public class AutoSyncQuestionAnswerMap
    {
        public List<AutoSyncQuestionAnswer> Map {get; set;}
    }

    public class AutoSyncQuestionAnswer
    {
        public AutoSyncQuestionAnswerDetails CopyFrom {get; set;}
        public AutoSyncQuestionAnswerDetails CopyTo {get; set;}
    }

    public class AutoSyncQuestionAnswerDetails
    {
        public string QuestionFieldName {get; set;}
        public int SectionId {get; set;}
        public string SubSectionId {get; set;}
        public string SubSectionTemplateFieldName {get; set;}
        public int QuestionSectionSeqNo {get; set;}
        public string QuestionId {get; set;}
    }


}