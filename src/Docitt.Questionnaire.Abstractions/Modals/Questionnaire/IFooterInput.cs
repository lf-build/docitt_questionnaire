﻿namespace Docitt.Questionnaire
{
    public interface IFooterInput
    {
        string Text { get; set; }
        string Type { get; set; }
        string Action { get; set; }
        DynamicTemplate dynamicTemplate { get; set; }
        IFooterInputActionConfig ActionConfig { get; set; }
    }

    public interface IFooterInputActionConfig
    {
        string SkipToTemplateId { get; set; }
    }
}