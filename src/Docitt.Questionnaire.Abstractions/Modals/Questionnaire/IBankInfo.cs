﻿namespace Docitt.Questionnaire
{
    public interface IBankInfo 
    {
        string BankName { get; set; }
        string AccountType { get; set; }
        string AccountNumber { get; set; }
        double CurrentBalance { get; set; }
        string BankInfoId { get; set; }
        string AccountHolderName { get; set; }
    }
}