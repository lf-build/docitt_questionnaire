﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IDynamicTemplate
    {
        string TriggerAnswer { get; set; }
        List<string> TemplateIds { get; set; }
        string TriggerEvent { get; set; }
        string FunctionName { get; set; }
        bool IsSingleton { get; set; }
        bool ForcedToBeNext { get; set; }
        int SkipScreens { get; set; }
        bool IsDelete { get; set; }
    }
}