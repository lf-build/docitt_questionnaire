namespace Docitt.Questionnaire
{
    public class TemplateConstant
    {
        public const string BorrowerREOInfoSubSection = "BorrowerREOInfo";

        public const string BorrowerREOInfo = "borrowerREOInfo";

        public const string CoBorrowerREOInfo = "coBorrowerREOInfo";
        
        public const string SpouseBorrowerREOInfo = "spouseREOInfo";
        
        public const string SpouseBorrowerREOAdditionalInfo = "spouseREOAdditionalInfo";


        public const string CoborrowerSpouseBorrowerREOInfo = "coboSpouseREOInfo";

        public const string CoborrowerSpouseBorrowerREOAdditionalInfo = "coboSpouseREOAdditionalInfo";

    }
}