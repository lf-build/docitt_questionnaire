﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class DynamicTemplate: IDynamicTemplate
    {
        public string TriggerAnswer { get; set; }
        public List<string> TemplateIds { get; set; }
        public string TriggerEvent { get; set; }
        public string FunctionName { get; set; }
        public bool IsSingleton { get; set; }
        public bool ForcedToBeNext { get; set; }        
        public int SkipScreens { get; set; }
        public bool IsDelete { get; set; }
    }
}