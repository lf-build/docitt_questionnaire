﻿namespace Docitt.Questionnaire
{
    public interface IDynamicQuestionSectionTemplate
    {
        string QuestionSectionTemplateId { get; set; }
    }
}