﻿using System.Collections;

namespace Docitt.Questionnaire
{
    public class Templates : ITemplates
    {
        //This is a collection of Template having a Template name and subsection
        private Templates()
        { }

        private Hashtable hashTemplates = new Hashtable();
        public Hashtable TemplateList
        {
            get
            {
                return hashTemplates;
            }
            set
            {
                value = hashTemplates;
            }
        }
        public void AddTemplate(string templateName, ISubSection subSection)
        {
            if (hashTemplates.Contains(templateName) == false)
            {
                hashTemplates.Add(templateName, subSection);
            }
            else
            {
                hashTemplates[templateName] = subSection;
            }
        }
    }
}