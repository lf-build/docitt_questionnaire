﻿using System.Collections;

namespace Docitt.Questionnaire
{
    public interface ITemplates
    {
        //This is a collection of Template having a Template name and subsection
        Hashtable TemplateList { get; set; }
    }
}