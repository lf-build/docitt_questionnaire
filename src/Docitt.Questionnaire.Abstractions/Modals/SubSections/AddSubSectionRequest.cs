﻿namespace Docitt.Questionnaire
{
    public class AddSubSectionRequest :IAddSubSectionRequest
    {        
        public string TemporaryApplicationNumber { get; set; }
        public SubSection SubSection { get; set; }
        public int SectionId { get; set; }
    }
}