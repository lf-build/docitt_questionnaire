﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Questionnaire
{

    public class UserSubSectionSummary : IUserSubSectionSummary
    {
        //This is used to group all the subsections belonging to the user in one collection.
        public List<ISubSectionSummary> SubSectionSummary { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string SubSectionType { get; set; }
    }

    public class SubSectionSummary : ISubSectionSummary
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.
        [JsonConverter(typeof(InterfaceConverter<ISubSection, SubSection>))]
        public ISubSection SubSection { get; set; }
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionType { get; set; }
        public string SubSectionTypeInfo { get; set; }
        public int HighestSectionReached { get; set; }
        public string HighestSubSectionReached { get; set; }
    }
}