﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class AddSubSectionsRequest :IAddSubSectionsRequest
    {        
        public string TemporaryApplicationNumber { get; set; }
        public List<SubSection> SubSections { get; set; }
        public int SectionId { get; set; }
    }
}