﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IAddSubSectionsRequest
    {
        string TemporaryApplicationNumber { get; set; }
        List<SubSection> SubSections { get; set; }
        int SectionId { get; set; }
    }
}