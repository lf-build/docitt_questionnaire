﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface ISubSectionIdList
    {
        List<string> SubSectionIds { get; set; }
    }
}