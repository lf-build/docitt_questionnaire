﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace Docitt.Questionnaire
{
    public interface ISubSection : IAggregate
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.

        string ApplicationNumber { get; set; }
        int SectionId { get; set; }
        string SubSectionName { get; set; }

        string SubSectionTextBorrower { get; set; }

        string SubSectionTextSpouse { get; set; }

        string SubSectionApplicationSummary { get; set; }

        //This collection will hold both the UIQuestions and the Questions 
        [JsonConverter(typeof(InterfaceListConverter<IQuestionSection, QuestionSection>))]
        List<IQuestionSection> QuestionSections { get; set; }
        string SubSectionId { get; set; }
        string SubSectionUniqueId { get; set; }
        string TemplateFieldName { get; set; }
        int? SeqNo { get; set; }
       
        string TemplateId { get; set; }
        List<string> Tag { get; set; }
        string NextServiceEvent { get; set; }
        string NextText { get; set; }
        string BackText { get; set; }
        string BackServiceEvent { get; set; }
        string UserName { get; set; }
        
        string FirstName { get; set; }
        FooterLink[] FooterLinks { get; set; }
        FooterInput[] FooterInputs { get; set; }
        List<string> DeleteTemplates { get; set; }
        bool isDefault { get; set; }
        bool isDisplay { get; set; } 
    }
}