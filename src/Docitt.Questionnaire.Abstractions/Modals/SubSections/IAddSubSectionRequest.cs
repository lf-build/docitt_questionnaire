﻿namespace Docitt.Questionnaire
{
    public interface IAddSubSectionRequest
    {
        string TemporaryApplicationNumber { get; set; }
        SubSection SubSection { get; set; }
        int SectionId { get; set; }
    }
}