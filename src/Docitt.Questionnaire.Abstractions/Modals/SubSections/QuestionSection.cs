﻿using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public class QuestionSection :  Aggregate,IQuestionSection
    {
        //Questions is a list of questions that will appear on the UI in the order of Seq no.
        //The Questions either will be shown one at a time, or it will be shown vertically one beneath the other.
        public string ApplicationNumber { get; set; }
        public int SectionId { get; set; }
        public string SubSectionId { get; set; }
        public string QuestionSectionName { get; set; }
        public string QuestionSectionId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IQuestion, Question>))]
        public List<IQuestion> QuestionList { get; set; }
        public bool LRUI { get; set; }
        public int SeqNo { get; set; }
        public string QuestionSectionTemplateId { get; set; }
    }
}