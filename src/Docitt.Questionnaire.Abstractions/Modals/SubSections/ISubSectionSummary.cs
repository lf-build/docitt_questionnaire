﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface IUserSubSectionSummary 
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.
        List<ISubSectionSummary> SubSectionSummary { get; set; }
        string UserName { get; set; }
        string SubSectionType { get; set; }
    }

    public interface ISubSectionSummary
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.
        ISubSection SubSection { get; set; }
        int SectionId { get; set; }
        string SubSectionId { get; set; }
        string SubSectionName { get; set; }
        string SubSectionType { get; set; }
        string SubSectionTypeInfo { get; set; }
        int HighestSectionReached { get; set; }
        string HighestSubSectionReached { get; set; }
    }
}