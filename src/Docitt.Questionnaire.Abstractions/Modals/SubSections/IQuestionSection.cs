﻿using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public interface IQuestionSection : IAggregate
    {
        //Questions is a list of questions that will appear on the UI in the order of Seq no.
        //The Questions either will be shown one at a time, or it will be shown vertically one beneath the other.

        string ApplicationNumber { get; set; }
        int SectionId { get; set; }
        string SubSectionId { get; set; }

        string QuestionSectionName { get; set; }
        string QuestionSectionId { get; set; }
        bool LRUI { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IQuestion, Question>))]
        List<IQuestion> QuestionList { get; set; }
        int SeqNo { get; set; }
        string QuestionSectionTemplateId { get;  set;}
    }
}