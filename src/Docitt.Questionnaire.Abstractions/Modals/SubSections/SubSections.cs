﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;


namespace Docitt.Questionnaire
{
    public class SubSection :  Aggregate,ISubSection
    {
        //A section is made up of multiple Sub Sections. 
        //At a given point of time, all that needs to be shown on the screen should be made part of the sub sections.
        //In other words, one sub screen is shown at a time. Sub screen is not broken down in seperate screens.
        //On clicking on the 'Next' button, the user will be shown the next Sub sections.
        public string ApplicationNumber { get; set; }
        public int SectionId { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionTextBorrower { get; set; }
        public string SubSectionTextSpouse { get; set; } 
        public string SubSectionApplicationSummary {get; set;}
        public string SubSectionId { get; set; }
        public string SubSectionUniqueId { get; set; }
        public string TemplateFieldName { get; set; }

        //This collection will hold both the UIQuestions and the Questions 
        [JsonConverter(typeof(InterfaceListConverter<IQuestionSection, QuestionSection>))]
        public List<IQuestionSection> QuestionSections { get; set; }
        public int? SeqNo { get; set; }

       
        public string TemplateId { get; set; }
        public string NextText { get; set; }
        public string BackText { get; set; }
        public List<string> Tag { get; set; }
        public string NextServiceEvent { get; set; }
        public string BackServiceEvent { get; set; }
        public string UserName { get; set; } //Name of the user whose information this section contains. These are actually applicants.
        public string FirstName { get; set; }
      
        public FooterLink[] FooterLinks { get; set; }
        public FooterInput[] FooterInputs { get; set; }
        public bool isDefault { get; set; }
        public bool isDisplay { get; set; }
        
        public List<string> DeleteTemplates { get; set; } = null;
       
    }
}