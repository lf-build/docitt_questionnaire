﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class SubSectionIdList : ISubSectionIdList
    {
        public List<string> SubSectionIds { get; set; }
    }
}