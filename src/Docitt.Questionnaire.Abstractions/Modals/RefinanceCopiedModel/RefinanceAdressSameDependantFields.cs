using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class RefinanceAdressSameDependantFields
    {
        public RequiredFields Field {get;set;}

        public List<RequiredFields> FieldCopies { get; set; }
    }

     public class RequiredFields
    {
        public int SectionId { get; set; }

        public string SubSectionId { get; set; }

        public int QuestionSequence { get; set; }

        public string QuestionId { get; set; }
        
    }
}