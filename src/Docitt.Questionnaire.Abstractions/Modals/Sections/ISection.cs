﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
namespace Docitt.Questionnaire
{
    public interface ISection : IAggregate,ISectionBasic
    {
        string TemplateId { get; set; }
        List<ISubSection> SubSections { get; set; }
        Dictionary<string, SubSection> Templates { get; set; }
        Dictionary<string, string> PlaceHolders { get; set; }
        ISectionCompletion SectionCompletion { get; set; }
        Dictionary<string, QuestionSection> QuestionSectionTemplates { get; set; }
        SectionType Type { get; set; }
        bool isDefault { get; set; }
    }

    public interface ISectionBasic
    {
         string TemporaryApplicationNumber { get; set; }
        int SectionId { get; set; }
        string SectionName { get; set; }
        bool IsCompleted { get; set; }
        int SeqNo { get; set; }
        string UserName { get; set; }
    }

    public class SectionCompletion : ISectionCompletion
    {
        public string[] MessageHeader { get; set; }
        public string MessageBody { get; set; }
        public string ButtonLabel { get; set; }
    }

    public interface ISectionCompletion
    {
        string[] MessageHeader { get; set; }
        string MessageBody { get; set; }
        string ButtonLabel { get; set; }
    }
}