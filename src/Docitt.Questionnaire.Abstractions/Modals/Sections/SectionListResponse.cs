﻿using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class SectionListResponse: ISectionListResponse 
    {
        public string TemporaryApplicationNumber { get; set; }      
        public string ReferenceApplicationNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISectionGroup, SectionGroup>))]
        public List<ISectionGroup> GroupOfSections { get; set; }
        public int LastAccessedSectionId { get; set; }
        public string LastAccessedSubSectionId { get; set; }
        public int HighestSectionReached { get; set; }
        public string HighestSubSectionReached { get; set; }
    }
}