﻿using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public class SectionResponse : ISectionResponse
    {
        public string TemporaryApplicationNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISection, Section>))]
        public ISection Section { get; set; }
        public Dictionary<string, string> PlaceHolders { get; set; }
    }

    public class LinkTemplate : ILinkTemplate
    {
        public string Text { get; set; }
        public string TemplateName { get; set; }
        public string Version { get; set; }
    }

    public interface ILinkTemplate
    {
        string Text { get; set; }
        string TemplateName { get; set; }
        string Version { get; set; }
    }
}