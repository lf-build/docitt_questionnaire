﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface ISectionListResponse 
    {
        string TemporaryApplicationNumber { get; set; }
        string ReferenceApplicationNumber { get; set; }
        List<ISectionGroup> GroupOfSections { get; set; }
        int LastAccessedSectionId { get; set; }
        string LastAccessedSubSectionId { get; set; }
        int HighestSectionReached { get; set; }
        string HighestSubSectionReached { get; set; }
    }
}