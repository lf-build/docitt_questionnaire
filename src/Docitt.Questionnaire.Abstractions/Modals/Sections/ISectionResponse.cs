﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface ISectionResponse 
    {
        string TemporaryApplicationNumber { get; set; }     
        [JsonConverter(typeof(InterfaceConverter<ISection, Section>))]
        ISection Section { get; set; }
        [JsonProperty(ItemConverterType = typeof(InterfaceTypeConverter<string>))]
        Dictionary<string, string> PlaceHolders { get; set; }
    }    
}