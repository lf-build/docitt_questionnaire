﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire
{
    public class Section : Aggregate,ISection
    {
        public Section() { }
         public string TemporaryApplicationNumber { get; set; }
        public string TemplateId { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubSection, SubSection>))]
        public List<ISubSection> SubSections { get; set; }
        public int SeqNo { get; set; }
        public bool IsCompleted { get; set; }               
        public Dictionary<string, SubSection> Templates { get; set; }
        public Dictionary<string, string> PlaceHolders { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISectionCompletion, SectionCompletion>))]
        public ISectionCompletion SectionCompletion { get; set; }
        public Dictionary<string, QuestionSection> QuestionSectionTemplates { get; set; }
        public SectionType Type { get; set; }
        public string UserName { get; set; }
        public bool isDefault { get; set; }
    }

    public class SectionBasic : ISectionBasic
    {
         public string TemporaryApplicationNumber { get; set; }
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public bool IsCompleted { get; set; }
        public int SeqNo { get; set; }
        public string UserName { get; set; }
    }    
}