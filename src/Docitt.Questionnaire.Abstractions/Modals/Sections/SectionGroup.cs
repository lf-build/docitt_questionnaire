﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public class SectionGroup : ISectionGroup
    {
        [JsonConverter(typeof(InterfaceListConverter<ISection, Section>))]
        public List<ISectionBasic> Sections { get; set; }
        public FormStatus Type { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}