﻿using System.Collections.Generic;

namespace Docitt.Questionnaire
{
    public interface ISectionGroup
    {
        List<ISectionBasic> Sections { get; set; }
        FormStatus Type { get; set; }
        string Name { get; set; }
        string Email { get; set; }
    }
}