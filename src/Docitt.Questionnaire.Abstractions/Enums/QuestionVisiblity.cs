namespace Docitt.Questionnaire
{
    public enum QuestionVisiblity
    {
        Hide = 0,
        Show = 1,
        AlwaysHide = 2
    }
}