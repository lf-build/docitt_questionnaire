﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum ApplicantType
    {
        Borrower,
        [Description("Spouse")]
        Spouse,
        [Description("Co-Borrower")]
        CoBorrower,
        [Description("CoBorrowerSpouse")]
        CoBorrowerSpouse
    }
}
