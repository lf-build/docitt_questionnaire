﻿namespace Docitt.Questionnaire
{
    public enum dataType
    {
        System_Boolean = 0,
        System_Int32 = 1,
        System_Int64 = 2,
        System_Double = 3,
        System_DateTime = 4,
        System_String = 5,
        System_Email = 6
    }
}