﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum DefaultApplicationStatusLabel
    {
        Null = 0,
        [Description("Review")]
        Review = 1
    }
}