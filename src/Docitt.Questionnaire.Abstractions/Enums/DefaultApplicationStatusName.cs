﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum DefaultApplicationStatusName
    {
        Null = 0,
        [Description("Review")]
        Review = 1
    }
}