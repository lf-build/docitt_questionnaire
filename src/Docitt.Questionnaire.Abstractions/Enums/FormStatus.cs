﻿namespace Docitt.Questionnaire
{
    public enum FormStatus
    {
        Open = 1, //Questionnaire
        Closed = 2, //Closed in between
        OnHold = 3, //Onhold in between
        Application = 4, //Application is work in progress
        ApplicationClosed = 5,
        PostApplication = 6, // Post Application 
        WelcomePostApplication = 7,
        WelcomePersonalDashboard = 8
    }    
}