﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum ScopeType
    {
        Null = 0,
        [Description("back-office")]        
        BackOffice = 1,
        [Description("borrower")]
        Borrower = 2,
        [Description("affinity-partner")]
        AffinityPortal=3
    }
}