﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum DefaultApplicationStatusCode
    {
        Null = 0,
        [Description("200.10")]
        Review = 1
    }
}