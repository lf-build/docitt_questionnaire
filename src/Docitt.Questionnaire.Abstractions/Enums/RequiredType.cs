﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum RequiredType
    {
        [Description("false")]
        False = 0,
        [Description("true")]
        True = 1,
        [Description("skippable")]
        Skippable = 2
    }
}
