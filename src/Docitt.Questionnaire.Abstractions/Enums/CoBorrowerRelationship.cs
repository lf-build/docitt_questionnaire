namespace Docitt.Questionnaire
{
    public enum CoBorowerRelationship
    {
        Spouse,
        Parent,
        Sibling,
        OtherRelative,
        CoWorker,
        Other
    }
}
