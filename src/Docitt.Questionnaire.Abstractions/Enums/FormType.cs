﻿namespace Docitt.Questionnaire
{
    public enum FormType
    {
        Null = 0,
        Full = 1,
        Short = 2
    }
}