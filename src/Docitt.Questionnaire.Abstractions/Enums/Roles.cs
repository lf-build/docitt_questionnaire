﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum Roles
    {
        Null = 0,
        
        [Description("Back-Office")]
        BackOffice =1 ,
        
        [Description("Loan Officer")]
        LoanOfficer =2,
        
        [Description("Loan Officer Assistance")]
        LoanOfficerAssistance=3,
        
        [Description("Processor")]
        Processor =4,
        
        [Description("Underwriter")]
        Underwriter =5,
        
        [Description("Branch Manager")]
        BranchManager =6,
        
        [Description("Regional Manager")]
        RegionalManager =7,
        
        [Description("Admin")]
        Admin = 8 ,
        
        [Description("Executive")]
        Executive =9, 
        
        [Description("Affinity-Partner")]
        AffinityPartner =10,
        
        [Description("Affinity Partner Admin")]
        AffinityPartnerAdmin =11,
        
        [Description("Affinity Partner Manager")]
        AffinityPartnerManager =12,
        
        [Description("Affinity Partner Agent")]
        AffinityPartnerAgent = 13,
        
        [Description("Affinity Partner Assistant")]
        AffinityPartnerAssistant =14,
        
        [Description("Affinity Partner Coordinator")]
        AffinityPartnerCoordinator = 15,
        
        [Description("Affinity Partner Other")]
        AffinityPartnerOther =16,
        
        [Description("Realtors")]
        Realtors =17,
        
        [Description("Builders")]
        Builders=18,
        
        [Description("Accountants")]
        Accountants =19,
        
        [Description("Brokers")]
        Brokers = 20,
        
        [Description("Insurance Agents")]
        InsuranceAgents = 21,
        
        [Description("Selling Agent")]
        SellingAgent = 22,
        
        [Description("Assistant Selling Agent")]
        AssistantSellingAgent = 23,
        
        [Description("Buying Agent")]
        BuyingAgent = 24,
        
        [Description("Assistant Buying Agent")]
        AssistantBuyingAgent = 25,
        
        [Description("Escrow Officer")]
        EscrowOfficer = 26,
        
        [Description("Assistant Escrow Officer")]
        AssistantEscrowOfficer = 27,
        
        [Description("Title Agent")]
        TitleAgent = 28,
        
        [Description("Borrower")]
        Borrower = 29,
        
        [Description("Co-Borrower")]
        CoBorrower =30
    }
}