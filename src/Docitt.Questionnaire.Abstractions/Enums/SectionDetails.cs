﻿using System.ComponentModel;

namespace Docitt.Questionnaire
{
    public enum SectionDetails
    {
        Profile = 1,
        Assets = 2,
        Income = 3,
        Declarations = 4,
        Credit = 5,
        Summary = 6,
        [Description("Tax Returns")]
        TaxReturns = 7,
        [Description("W-2's & Pay Stubs")]
        W2AndPayStubs = 8
    }
}