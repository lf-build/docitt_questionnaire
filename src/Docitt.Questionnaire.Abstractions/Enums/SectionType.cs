﻿namespace Docitt.Questionnaire
{
    public enum SectionType
    {
        Questionnaire = 1, //before Tax
        PostApplication = 2 //Tax and PayStub
    }
}
