﻿namespace Docitt.Questionnaire
{
    public enum LoanPurpose
    {
        Purchase,
        Refinance
    }
}