﻿using Docitt.Applicant;
using Docitt.Application;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;
using Docitt.Questionnaire.Abstractions;
using LendFoundry.Security.Tokens;
using Docitt.Questionnaire.Events;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;


namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        /// <summary>
        /// This function gets the personal details of the applicants like the First Name, Middle Name, Last Name, User Name, Email, Phone etc, Applicant Type.
        /// This function based on the question and the parameter passed uses fetches the information for the applicant.
        /// These parameters are fetched from configuration
        /// </summary>
        /// <param name="request"></param>
        /// <param name="questionAnswer"></param>
        /// <param name="frm"></param>
        /// <param name="item"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        private async Task<ITransitionFormExtended> GetBorrowerDetail(IQuestionnaire request, IQuestion questionAnswer, ITransitionFormExtended frm, IApplicationFieldMap item, string userName = null)
        {
            if (item.TargetProperty.ToLower() == "address")
            {
                if (questionAnswer.Answer != "")
                {
                    IAddressInfo currentAddress;
                    try
                    {
                        currentAddress = JsonConvert.DeserializeObject<AddressInfo>(questionAnswer.Answer);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Unable to deserialize answer :" + questionAnswer.Answer, ex);
                    }
                    
                    frm.Address = currentAddress.Address + currentAddress.Addressline2 + currentAddress.Line3;
                    frm.Line1 = currentAddress.Address;
                    frm.City = currentAddress.City;

                    if (string.IsNullOrEmpty(frm.Address))
                        frm.Address = !String.IsNullOrEmpty(currentAddress.Address) ? currentAddress.Address : String.Empty;

                    if (string.IsNullOrEmpty(frm.Line1))
                        frm.Line1 = currentAddress.Address;

                    frm.State = currentAddress.State;
                    frm.Zip = currentAddress.Zip;
                }
            }
            else if (item.TargetProperty.ToLower() == "spouseaddress")
            {
                if (questionAnswer.Answer != "" && item.QuestionFieldNameBorrower == questionAnswer.QuestionFieldName)
                {
                    try
                    {                        
                        frm.SpouseResidenceAddress = JsonConvert.DeserializeObject<AddressInfo>(questionAnswer.Answer);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Unable to deserialize answer :" + questionAnswer.Answer, ex);
                    }
                                   
                }
            }
            else if (item.TargetProperty.ToLower() == "borrowerresidence")
            {
                IAddressInfo borrowerResidenceAddress;
                try
                {
                    borrowerResidenceAddress = JsonConvert.DeserializeObject<AddressInfo>(questionAnswer.Answer);
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to deserialize answer :" + questionAnswer.Answer, ex);
                }
                
                if (borrowerResidenceAddress != null)
                {
                    frm.BorrowerResidenceAddress = borrowerResidenceAddress;
                }
            }
            else if (item.TargetProperty.ToLower() == "propertyaddress")
            {
                IAddressInfo currentAddress;
                try
                {
                    currentAddress = JsonConvert.DeserializeObject<AddressInfo>(questionAnswer.Answer);
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to deserialize answer :" + questionAnswer.Answer, ex);
                }
                if (currentAddress != null)
                {
                    frm.PropertyAddress = currentAddress;
                }
            }
            else if (item.TargetProperty.ToLower() == "propertycity")
            {
                if (string.IsNullOrEmpty(frm.PropertyCity))
                {
                    frm.PropertyCity = questionAnswer.Answer;
                }
            }
            else if (item.TargetProperty.ToLower() == "propertycounty")
            {
                if (string.IsNullOrEmpty(frm.PropertyCounty))
                {
                    frm.PropertyCounty = questionAnswer.Answer;
                }
            }
            else if (item.TargetProperty.ToLower() == "propertystate")
            {
                if (string.IsNullOrEmpty(frm.PropertyState))
                {
                    frm.PropertyState = questionAnswer.Answer;
                }
            }
            else if (item.TargetProperty.ToLower() == "propertyselected")
            {
                if (questionAnswer.Answer.ToLower() == "true")
                {
                    frm.PropertySelected = true;
                }
            }
            else if (item.TargetProperty.ToLower() == "loanamount")
            {
                ILoanCalculationInfo loanInfo;
                try
                {
                    loanInfo = JsonConvert.DeserializeObject<LoanCalculationInfo>(questionAnswer.Answer);
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to deserialize answer :" + questionAnswer.Answer, ex);
                }
                if (loanInfo != null)
                {
                    frm.LoanAmount = loanInfo.LoanAmount;
                    frm.DownPayment = loanInfo.DownPayment;
                    frm.PurchasePrice = string.IsNullOrEmpty(loanInfo.PurchasePrice) ? 0 : Convert.ToDouble(loanInfo.PurchasePrice);
                }
            }
            else if (item.TargetProperty.ToLower() == "refinancepurchase")
            {
                if (!string.IsNullOrEmpty(questionAnswer.Answer))
                {
                    frm.PurchasePrice = string.IsNullOrEmpty(questionAnswer.Answer) ? 0 : Convert.ToDouble(questionAnswer.Answer);
                }

            }

            else if (item.TargetProperty.ToLower() == "firstname")
            {
                frm.FirstName = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "lastname")
            {
                frm.LastName = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "email")
            {
                frm.Email = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "homeownership")
            {
                frm.HomeOwnership = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "loanamount")
            {
                frm.LoanAmount = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "loanpurpose")
            {
                frm.LoanPurpose = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "phone")
            {
                frm.Phone = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "birthdate")
            {
                if (DateTime.TryParse(questionAnswer.Answer, out var birthdate))
                {
                    frm.DobYear = Convert.ToString(birthdate.Year);
                    frm.DobMonth = Convert.ToString(birthdate.Month);
                    frm.DobDay = Convert.ToString(birthdate.Day);
                }
            }
            else if (item.TargetProperty.ToLower() == "ssn")
            {
                frm.Ssn = questionAnswer.Answer;
            }
            else if (item.TargetProperty.ToLower() == "sameaddress")
            {
                frm.ResidenceSameAsSpouse = questionAnswer.Answer.ToLower() == "true";
            }
            else if (item.TargetProperty.ToLower() == "salary" || item.TargetProperty.ToLower() == "bonus" || item.TargetProperty.ToLower() == "commission" || item.TargetProperty.ToLower() == "overtime")
            {
                frm.EmploymentIncome = frm.EmploymentIncome + await GetTotalAmount(request, item, userName);
            }
            else if (item.TargetProperty.ToLower() == "businessemployment")
            {
                frm.BusinessEmploymentIncome = frm.BusinessEmploymentIncome + await GetTotalAmount(request, item, userName);
            }
            else if (item.TargetProperty.ToLower() == "alimonypermonth" || item.TargetProperty.ToLower() == "alimonyperchild")
            {
                frm.AlimonyAmount = frm.AlimonyAmount + await GetTotalAmount(request, item, userName);
            }
            else if (item.TargetProperty.ToLower() == "militaryamount")
            {
                frm.MilitaryPayAmount = frm.MilitaryPayAmount + await GetTotalAmount(request, item, userName);
            }
            else if (item.TargetProperty.ToLower() == "isborrowervaeligible")
            {
                if (questionAnswer.Answer.ToLower() == "true")
                {
                    frm.IsBorrowerVAEligible = true;
                }
            }
            else if (item.TargetProperty.ToLower() == "ssnamount")
            {
                frm.SocialSecurityIncome = frm.SocialSecurityIncome + await GetTotalAmount(request, item);
            }
            else if (item.TargetProperty.ToLower() == "isbankrupt")
            {
                frm.IsBankrupt = await GetOnlyBorrowerInfo(request, item) == "true" ? "Yes" : "No";
            }
            else if (item.TargetProperty.ToLower() == "isforeclosed")
            {
                frm.isTransactionClosingStatement = await GetOnlyBorrowerInfo(request, item) == "true" ? "Yes" : "No";
            }
            else if (item.TargetProperty.ToLower() == "gender")
            {
                frm.Gender = await GetOnlyBorrowerInfo(request, item);
            }
            else if (item.TargetProperty.ToLower() == "propertytype")
            {
                frm.PropertyType = await GetOnlyBorrowerInfo(request, item);
            }
            else if (item.TargetProperty.ToLower() == "isincontract")
            {
                frm.IsInContract = await GetOnlyBorrowerInfo(request, item);
            }
            else if (item.TargetProperty.ToLower() == "residencytype")
            {
                frm.ResidencyType = await GetOnlyBorrowerInfo(request, item);
            }
            else if (item.TargetProperty.ToLower() == "uscitizen")
            {
                frm.UsCitizen = await GetOnlyBorrowerInfo(request, item);
            }
            else if (item.TargetProperty.ToLower() == "1strefinance" || item.TargetProperty.ToLower() == "2ndrefinance" || item.TargetProperty.ToLower() == "otherrefinance" || item.TargetProperty.ToLower() == "cashoutamount")
            {
                frm.LoanAmount = string.IsNullOrEmpty(frm.LoanAmount) ? "0" : frm.LoanAmount;
                questionAnswer.Answer = string.IsNullOrEmpty(questionAnswer.Answer) ? "0" : questionAnswer.Answer;
                frm.LoanAmount = Convert.ToString(Convert.ToDouble(frm.LoanAmount) + Convert.ToDouble(questionAnswer.Answer), CultureInfo.InvariantCulture);
            }
            else if (item.TargetProperty.ToLower() == "companystacklt25per")
            {
                frm.companyStackLessThan25Percentage = questionAnswer.Answer.ToLower() != "true";
            }
            else if (item.TargetProperty.ToLower() == Constant.TragetPropForDischargeFromActiveDuty.ToLower())
            {
                var answerResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(questionAnswer.Answer);
                if(answerResponse != null)
                {
                    var isCurrentlyRetired = string.Empty;
                    frm.DischargeFromActiveDuty = answerResponse.TryGetValue(Constant.EnumVACurrentlyRetired, out isCurrentlyRetired);
                }
            }           
            return frm;
        }

        /// <summary>
        /// This function provides the Borrower specific information like name, email, phone, user name, applicant type
        /// </summary>
        /// <param name="request"></param>
        /// <param name="userName"></param>
        /// <param name="borrowerInfo"></param>
        /// <returns></returns>
        private async Task<ITransitionFormExtended> GetUserWiseBorrowerInformationFromQuestionnaire(IQuestionnaire request, string userName, IBorrowerCoBorrowerInfo borrowerInfo) //GenerateApplicationFromQuestionnaire()public async Task<ITransitionFormExtended> GenerateApplicationFromQuestionnaire()
        {
            //This function fetches the Questionnaire and maps the value to Application field.
            //It then calls the Application creation API.

            ITransitionFormExtended applicantInfo = new TransitionForm();

            var appFieldMap = QuestionnaireConfigurations.ApplicationFieldMap; //GetApplicationConfiguration("application-field-map");
            var wfQuestion = request.ApplicationForm.Sections.Where(s => s.SectionId == Convert.ToInt32(SectionDetails.Profile))
                            .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                            .SelectMany(ss => ss.QuestionSections)
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerTransactionType);
            var refinanceFlag = false;
            if(wfQuestion != null &&  wfQuestion.Answer == Constant.RefinanceWorkflow)
            {
                refinanceFlag = true;
            }

            foreach (IApplicationFieldMap item in appFieldMap.Map)
            {
                IQuestion questionAnswer = null;

                if (borrowerInfo.ApplicantType == ApplicantType.Borrower)
                {                                                                     
                    if (item.TargetProperty.ToLower() == "spouseaddress")
                    {
                        if(refinanceFlag){
                            questionAnswer =  BorrowerCurrentStreetAddress(request, userName);
                        }
                        else
                        {
                             questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                            .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                            .SelectMany(ss => ss.QuestionSections)
                            .SelectMany(qs => qs.QuestionList)
                            .Where(q => q.QuestionFieldName != "BorrowerFirstMortgagePI")
                            .FirstOrDefault(q => q.QuestionFieldName == "BorrowerCurrentStreetAddress");
                        }
                       
                    }
                    else if(item.TargetProperty.ToLower() == "borrowerresidence")
                    {
                         if(refinanceFlag){
                            questionAnswer =  BorrowerCurrentStreetAddress(request, userName);
                        }
                        else
                        {
                            questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                                .SelectMany(ss => ss.QuestionSections)
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionFieldName == "BorrowerCurrentStreetAddress");
                        }
                    }
                    else if (item.TargetProperty.ToLower() == "propertycity")
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName)
                                .SelectMany(ss => ss.QuestionSections)
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionFieldName == "BorrowerCity");
                    }
                    else if (item.TargetProperty.ToLower() == "propertycounty")
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName)
                                .SelectMany(ss => ss.QuestionSections)
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionFieldName == "BorrowerCounty");
                    }
                    else if (item.TargetProperty.ToLower() == "propertystate")
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName)
                                .SelectMany(ss => ss.QuestionSections)
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionFieldName == "BorrowerState");
                    }
                    else
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                        .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                            .SelectMany(ss => ss.QuestionSections)
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => 
                                    q.QuestionId == item.QuestionId &&
                                    (string.IsNullOrEmpty(item.QuestionFieldNameBorrower) || q.QuestionFieldName == item.QuestionFieldNameBorrower)
                            );
                    }
                }
                else
                {                    
                    if (borrowerInfo.ApplicantType == ApplicantType.CoBorrower && item.TargetProperty.ToLower() == "phone")
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                            .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName && ss.TemplateFieldName != "coBorrowerInfo")
                            .SelectMany(ss => ss.QuestionSections)
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == item.QuestionId);
                    }
                    else if (borrowerInfo.ApplicantType == ApplicantType.Spouse && item.TargetProperty.ToLower() == "spouseaddress")
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId)
                                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName && ss.TemplateFieldName == "spouseResidenceDetail")
                                .SelectMany(ss => ss.QuestionSections)
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionFieldName == "BorrowerCurrentStreetAddress");
                    }
                    else
                    {
                        questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId).SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName)
                            .SelectMany(ss => ss.QuestionSections)
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == item.QuestionId &&
                                    (string.IsNullOrEmpty(item.QuestionFieldNameCoBorrower) || q.QuestionFieldName == item.QuestionFieldNameCoBorrower)
                            );
                    }
                }

                if (questionAnswer != null)
                {
                    try
                    {
                        applicantInfo = await GetBorrowerDetail(request, questionAnswer, applicantInfo, item, userName);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message, ex);
                    }
                }
            }

            applicantInfo.UserName = userName;
            applicantInfo.ApplicantType = borrowerInfo.ApplicantType;
            applicantInfo.WorkflowId = "1"; //hardcoding
            applicantInfo.AnnualGrossIncome = applicantInfo.EmploymentIncome;
            applicantInfo.Channel = "Merchant"; //hardcoding
            applicantInfo.HomeOwnership = "";

            ////
            applicantInfo.FirstName = borrowerInfo.FirstName;
            applicantInfo.LastName = borrowerInfo.LastName;
            applicantInfo.Email = borrowerInfo.Email;

            return await Task.Run(() => applicantInfo);
        }

        private IQuestion BorrowerCurrentStreetAddress(IQuestionnaire request, string userName)
        {
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
            var refinanceAddressSamequestionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId )
                    .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                    .SelectMany(ss => ss.QuestionSections)
                    .SelectMany(qs => qs.QuestionList)
                    .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerCurrentAddressSame);
            
            IQuestion questionAnswer = null;

            if(refinanceAddressSamequestionAnswer.Answer == "true"){
                questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                .SelectMany(ss => ss.QuestionSections)
                .SelectMany(qs => qs.QuestionList)
                .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerCurrentStreetAddressCheckBoxTrue);
            }
            else
            {
                questionAnswer = request.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName || ss.UserName == "[UserBased]")
                .SelectMany(ss => ss.QuestionSections)
                .SelectMany(qs => qs.QuestionList)
                .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerCurrentStreetAddressCheckBoxFalse);
            }

            return questionAnswer;
        }

        /// <summary>
        /// This function populates the request object needed by Application service to create the Docitt Application.
        /// </summary>
        /// <param name="form"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        private async Task<IApplicationRequest> GetApplicantRequest(ITransitionFormExtended form, IQuestionnaire questionnaire) //string temporaryApplicationNumber)
        {
            var applicationRequest = new ApplicationRequest();
            var temporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
            var applicants = new List<Docitt.Application.IApplicantRequest>();

            //This function collects information about all the applicants. It includes their First Name, Last Name, emaoil address, applicant type etc.
            var borrowerList = await GetBorrowersAndCoBorrowersAndSpouse(temporaryApplicationNumber, false);

            IAddress borrowerAddress = new Address(); //If the borrower and spouse have the same residence then we need to copy the address.
            var residenceSameAsSpouse = false;
            IAddress spouseBorrowerAddress = new Address();
            double propertyValue = 0;

            foreach (var applicantInList in borrowerList)
            {
                var applicantInfo = await GetUserWiseBorrowerInformationFromQuestionnaire(questionnaire, applicantInList.UserName, applicantInList);
                var addresses = new List<IAddress>();
                IAddress applicantAddress = null;

                if (applicantInfo != null)
                {
                    if (applicantInfo.PurchasePrice > 0)
                    {
                        propertyValue = applicantInfo.PurchasePrice;
                    }

                    Docitt.Application.IApplicantRequest applicant = new Docitt.Application.ApplicantRequest();

                    //Address is captured only for Borrower and the Spouse co borrower. 
                    //It is not captured for non spouse co borrower. So we are checking for the borrower address.
                    if (applicantInfo.BorrowerResidenceAddress != null)
                    {
                        borrowerAddress.Line1 = applicantInfo.BorrowerResidenceAddress.Address;
                        borrowerAddress.Line2 = applicantInfo.BorrowerResidenceAddress.Addressline2;
                        borrowerAddress.Line3 = applicantInfo.BorrowerResidenceAddress.Line3;
                        borrowerAddress.City = applicantInfo.BorrowerResidenceAddress.City;
                        borrowerAddress.State = applicantInfo.BorrowerResidenceAddress.State;
                        borrowerAddress.ZipCode = applicantInfo.BorrowerResidenceAddress.Zip;
                        borrowerAddress.Type = AddressType.Home;

                        applicantAddress = borrowerAddress;
                    }

                    if (applicantInfo.PropertySelected == false)
                    {
                        if (!string.IsNullOrEmpty(applicantInfo.PropertyCity) && 
                            !string.IsNullOrEmpty(applicantInfo.PropertyCounty))
                        {
                            applicantInfo.PropertyAddress = new AddressInfo();
                            applicantInfo.PropertyAddress.City = applicantInfo.PropertyCity;
                            applicantInfo.PropertyAddress.County = applicantInfo.PropertyCounty;
                            applicantInfo.PropertyAddress.State = applicantInfo.PropertyState; 
                        }
                    }

                    if (applicationRequest.PropertyAddress == null && applicantInfo.PropertyAddress!=null)
                    {
                        applicationRequest.PropertyAddress = new Address();
                        applicationRequest.PropertyAddress.Line1 = applicantInfo.PropertyAddress.Address;
                        applicationRequest.PropertyAddress.Line2 = applicantInfo.PropertyAddress.Addressline2;
                        applicationRequest.PropertyAddress.Line3 = applicantInfo.PropertyAddress.Line3;
                        applicationRequest.PropertyAddress.ZipCode = applicantInfo.PropertyAddress.Zip;
                        applicationRequest.PropertyAddress.City = applicantInfo.PropertyAddress.City;
                        applicationRequest.PropertyAddress.State = applicantInfo.PropertyAddress.State;
                        applicationRequest.PropertyAddress.County = applicantInfo.PropertyAddress.County;
                    }

                    applicationRequest.Channel = applicantInfo.Channel;
                    applicationRequest.WorkflowId = applicantInfo.WorkflowId;

                    if (applicantInfo.ResidenceSameAsSpouse == true)
                    {
                        residenceSameAsSpouse = true;
                    }

                    if (string.IsNullOrEmpty(applicantInfo.LoanPurpose) == false)
                    {
                        applicationRequest.Purpose = applicantInfo.LoanPurpose;
                    }

                    if (string.IsNullOrEmpty(applicantInfo.LoanAmount) == false || Convert.ToDouble(applicantInfo.LoanAmount) > 0)
                    {
                        applicationRequest.Amount = Convert.ToDouble(applicantInfo.LoanAmount);
                    }

                    if (applicantInList.ApplicantType == ApplicantType.Borrower || applicantInList.ApplicantType == ApplicantType.Spouse || 
                       applicantInList.ApplicantType == ApplicantType.CoBorrower || applicantInList.ApplicantType == ApplicantType.CoBorrowerSpouse) //capture residence address of the Borrower only, co-borrower residence address is not captured.
                    {
                        applicantAddress = borrowerAddress;

                        if (applicantInfo.SpouseResidenceAddress != null)
                        {
                            spouseBorrowerAddress.City = applicantInfo.SpouseResidenceAddress.City;
                            spouseBorrowerAddress.Line1 = applicantInfo.SpouseResidenceAddress.Address;
                            spouseBorrowerAddress.Line2 = applicantInfo.SpouseResidenceAddress.Addressline2;
                            spouseBorrowerAddress.Line3 = applicantInfo.SpouseResidenceAddress.Line3;
                            spouseBorrowerAddress.State = applicantInfo.SpouseResidenceAddress.State;
                            spouseBorrowerAddress.ZipCode = applicantInfo.SpouseResidenceAddress.Zip;
                            spouseBorrowerAddress.Type = AddressType.Home;
                        }
                    }

                    if (applicantInList.ApplicantType == ApplicantType.Spouse) //capture residence address of the Borrower only, co-borrower residence address is not captured.
                    {
                        if (residenceSameAsSpouse == true)
                        {
                            applicantAddress = new Address();
                            applicantAddress = borrowerAddress;
                        }
                        else
                        {
                            applicantAddress = new Address();
                            applicantAddress = spouseBorrowerAddress;
                        }
                    }

                    if (applicantAddress != null) addresses.Add(applicantAddress);

                    applicant.Addresses = addresses;
                    applicant.FirstName = applicantInfo.FirstName;
                    applicant.LastName = applicantInfo.LastName;
                    applicant.Ssn = applicantInfo.Ssn;
                    // bool validApplicant = true;

                    // try
                    // {
                    //     applicant.DateOfBirth = new DateTime(Convert.ToInt16(applicantInfo.DobYear), Convert.ToInt16(applicantInfo.DobMonth), Convert.ToInt16(applicantInfo.DobDay));
                    // }
                    // catch (Exception)
                    // {
                    //     //If there is no birthdate , then the user has been invited, but his information is not filled in. So this user information will not be saved when creating application.
                    //     validApplicant = false;
                    // }

                    applicant.Email = applicantInfo.Email;
                    applicant.HomeOwnership = HomeOwnershipType.Own;
                    var phonenumbers = new List<IPhoneNumber>();

                    IPhoneNumber phoneNumber = new PhoneNumber();
                    phoneNumber.Number = applicantInfo.Phone;
                    phoneNumber.Type = "Home";
                    phoneNumber.IsPrimary = true;
                    phonenumbers.Add(phoneNumber);
                    applicant.PhoneNumbers = phonenumbers;
                    applicant.StatedGrossAnnualIncome = applicantInfo.AnnualGrossIncome;
                    applicant.EmploymentStatus = EmploymentStatus.Employed;
                    var identities = new List<IIdentity>();

                    IIdentity identity = new Identity();
                    identity.Name = "ssn";
                    identity.Number = applicantInfo.Ssn; ;
                    identities.Add(identity);
                    applicant.Identities = identities;

                    var userIdentity = new LendFoundry.Security.Identity.UserIdentity();
                    userIdentity.UserId = applicantInfo.UserName;
                    userIdentity.Token = null;
                    applicant.UserIdentity = userIdentity;
                    switch (applicantInfo.ApplicantType)
                    {
                        case ApplicantType.Borrower:
                            applicant.ApplicantType = Docitt.Application.ApplicantType.Borrower;
                            break;
                        case ApplicantType.Spouse:
                            applicant.ApplicantType = Docitt.Application.ApplicantType.Spouse;
                            break;
                        case ApplicantType.CoBorrower:
                            applicant.ApplicantType = Docitt.Application.ApplicantType.CoBorrower;
                            break;
                        case ApplicantType.CoBorrowerSpouse:
                            applicant.ApplicantType = Docitt.Application.ApplicantType.CoBorrowerSpouse;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    //if (validApplicant == true)
                     applicants.Add(applicant);
                }
            }

            var source = new Source();
            source.Id = "1";
            source.Type = SourceType.Affiliate;

            applicationRequest.Source = source;
            applicationRequest.PropertyValue = propertyValue;
            applicationRequest.Applicants = applicants;
            return applicationRequest;
        }

        /// <summary>
        /// This fucntion fetches the application status
        /// </summary>
        /// <returns></returns>
        public async Task<IApplicationOverAllStatus> GetApplicationStatus()
        {

            var questionnaire = await GetQuestionnaire();

            if (questionnaire == null)
            {
                throw new QuestionnaireNotFoundException();
            }

            if (questionnaire.ApplicationNumber == null)
            {
                throw new ApplicationNotAvailableForThisQuestionnaireException("No Application available for the Questionnaire");
            }
            var status = GetApplicationStatusFromStatusManagement(questionnaire.ApplicationNumber);

            ///////////////////////////////////////////////////////////////////////////////////////
            //There is hardcoding in the below function like 200.01 etc.
            //We might need to send the code in the response and the browser-javascript should take care of the status based on the value.
            string reviewStatus = null;
            string processingStatus = null;
            string underwritingStatus = null;
            string documentStatus = null;
            string fundingStatus = null;

            switch (status.Result.Code)
            {
                case "200.00":
                    break;
                case "200.10":
                    reviewStatus = "WIP";
                    break;
                case "200.11":
                    reviewStatus = "Done";
                    break;
                case "200.20":
                    reviewStatus = "Done";
                    processingStatus = "WIP";
                    break;
                case "200.21":
                    reviewStatus = processingStatus = "Done";
                    break;
                case "200.30":
                    reviewStatus = processingStatus = "Done";
                    underwritingStatus = "WIP";
                    break;
                case "200.31":
                    reviewStatus = processingStatus = underwritingStatus = "Done";
                    break;
                case "200.40":
                    reviewStatus = "Done"; processingStatus = "Done"; underwritingStatus = "Done";
                    documentStatus = "WIP";
                    break;
                case "200.41":
                    reviewStatus = processingStatus = underwritingStatus = documentStatus = "Done";
                    break;
                case "200.50":
                    reviewStatus = processingStatus = underwritingStatus = documentStatus = "Done";
                    fundingStatus = "WIP";
                    break;
                case "200.51":
                    reviewStatus = processingStatus = underwritingStatus = documentStatus = fundingStatus = "Done";
                    break;
                default:
                    break;
            }

            var application = ApplicationService.GetByApplicationNumber(questionnaire.ApplicationNumber);

            if (application == null)
            {
                throw new Exception("Application '{questionnaire.ApplicationNumber}' does not exit");
            }

            IApplicationStatus tabStatus = new ApplicationStatus { ApplicationNumber = application.ApplicationNumber, Documents = documentStatus, Funding = fundingStatus, Processing = processingStatus, Review = reviewStatus, Underwriting = underwritingStatus };

            var applicationExpirationDate = application.ExpirationDate.Hour;
            var arrayExpirationDate = applicationExpirationDate.Split('-');
            var dtExpirationDate = new DateTime(Convert.ToInt16(arrayExpirationDate[0]), Convert.ToInt16(arrayExpirationDate[1]), Convert.ToInt16(arrayExpirationDate[2]));

            var daysLeft = (int)(dtExpirationDate - DateTime.Now).TotalDays;
            IAddressInfo propertyAddress = new AddressInfo();
            propertyAddress.Zip = application.PropertyAddress.ZipCode;
            propertyAddress.Address = application.PropertyAddress.Line1;
            propertyAddress.Addressline2 = application.PropertyAddress.Line2;
            propertyAddress.State = application.PropertyAddress.State;
            propertyAddress.City = application.PropertyAddress.City;

            IApplicationOverAllStatus applicationstatus = new ApplicationOverAllStatus { PropertyAddressInfo = propertyAddress, BorrowerFirstName = application.Applicants[0].FirstName, BorrowerLastName = application.Applicants[0].LastName, DaysToClose = "5", LoanAmount = application.Amount.ToString(), LoanDuration = "30 years", LoanNumber = application.ApplicationNumber, Percentage = "2.35", PropertyAddress = application.PropertyAddress.Line1 + ", " + application.PropertyAddress.City + ", " + application.PropertyAddress.State + ", " + application.PropertyAddress.ZipCode, DaysLeft = daysLeft, Address = propertyAddress };
            applicationstatus.TabStatus = tabStatus;
            return applicationstatus;
        }

        /// <summary>
        /// This function fetches the information of the Questionnaire summary, that is then displays in the borrower portal
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        public async Task<List<Dictionary<string, ISummary>>> GetApplicationSummary(string applicationNumber)
        {
            var questionnaire = await QuestionnaireRepository.GetQuestionnaireFromApplicationNumber(applicationNumber);

            if (questionnaire == null)
            {
                throw new ApplicationDoesNotExistsException($"Application '{applicationNumber}' does not exists exception.");
            }

            return await Task.Run(() => GetSummary(questionnaire.TemporaryApplicationNumber, questionnaire, true));
        }

        public async Task<IApplication> GetApplication(string temporaryApplicationNumber)
        {
            IApplication application = new Application();

            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire == null)
                throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");

            var applicants = new List<IBorrowerCoBorrowerInfo>();
            var sections = new List<ISectionBasic>();

            foreach (var item in questionnaire.ApplicationForm.Sections)
            {
                sections.Add(new SectionBasic
                {
                    SeqNo = item.SeqNo,
                    SectionId = item.SectionId,
                    SectionName = item.SectionName,
                    IsCompleted = item.IsCompleted
                });

                foreach (var subsection in item.SubSections)
                {
                    if (subsection.Tag == null)
                        break;

                    var subSections = GetUserInfoAndContactInfoSubsections(questionnaire.ApplicationForm.Sections.FirstOrDefault(i=>i.SectionId==item.SectionId), subsection.SubSectionId);

                    if (subsection.Tag.Contains("Borrower"))
                    {
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.Borrower);

                        if (borrowerInfo != null && !applicants.Any(x => x.UserName == borrowerInfo.UserName))
                        {
                            borrowerInfo.ApplicantType = ApplicantType.Borrower;
                            borrowerInfo.ActualApplicantType = ApplicantType.Borrower;
                            applicants.Add(borrowerInfo);
                        }
                    }

                    if (subsection.Tag.Contains("CoBorrower") && !subsection.Tag.Contains("Spouse") && subsection.TemplateFieldName != "coBorrowerMaritalStatusInfo")
                    {
                        //If the subsection is of the borrower then fetch the borrower information
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.CoBorrower);
                        if (borrowerInfo != null && !applicants.Any(x => x.UserName == borrowerInfo.UserName))
                        {
                            borrowerInfo.ApplicantType = ApplicantType.CoBorrower;
                            borrowerInfo.ActualApplicantType = ApplicantType.CoBorrower;
                            applicants.Add(borrowerInfo);
                        }
                    }

                    if (subsection.Tag.Contains("Spouse") && !subsection.Tag.Contains("Borrower"))
                    {
                        //If the subsection is of the borrower then fetch the borrower information
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.Spouse);
                        if (borrowerInfo != null && !applicants.Any(x => x.UserName == borrowerInfo.UserName))
                        {
                            borrowerInfo.ApplicantType = ApplicantType.Spouse;
                            borrowerInfo.ActualApplicantType = ApplicantType.Spouse;
                            applicants.Add(borrowerInfo);
                        }
                            
                    } 

                    if (subsection.Tag.Contains("CoBorrowerSpouse"))
                    {
                        //If the subsection is of the borrower then fetch the borrower information
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.CoBorrowerSpouse);
                        if (borrowerInfo != null && !applicants.Any(x => x.UserName == borrowerInfo.UserName))
                        {
                            borrowerInfo.ApplicantType = ApplicantType.CoBorrowerSpouse;
                            borrowerInfo.ActualApplicantType = ApplicantType.CoBorrowerSpouse;
                            applicants.Add(borrowerInfo);
                        }
                    }
                }
            }

            application.BorrowerCoBorrowerList = applicants;
            application.Sections = sections;
            application.CreatedDate = questionnaire.CreatedDate;
            application.Status = questionnaire.Status;

            var appFieldMap = QuestionnaireConfigurations.ApplicationFieldMap; //GetApplicationConfiguration("application-field-map");

            foreach (IApplicationFieldMap item in appFieldMap.Map)
            {
                var section = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId);

                if(section == null)
                    continue;

                var questionAnswer =section.SelectMany(s => s.SubSections).SelectMany(ss => ss.QuestionSections)
                                                                          .SelectMany(qs => qs.QuestionList)
                                                                          .FirstOrDefault(q => q.QuestionId == item.QuestionId);
                if (questionAnswer != null)
                {
                    if(item.TargetProperty == "loanamount")
                    {
                        var loanpurposeAnswer = GetAnswerToFielMap(questionnaire, appFieldMap, "loanpurpose");
                        if(loanpurposeAnswer.Equals("Refinance"))
                        {
                            var sumOf = new List<string>();
                            sumOf.Add(GetAnswerToFielMap(questionnaire, appFieldMap, "refinanceowe1"));
                            sumOf.Add(GetAnswerToFielMap(questionnaire, appFieldMap, "refinanceowe2"));
                            sumOf.Add(GetAnswerToFielMap(questionnaire, appFieldMap, "refinanceoweother"));
                            var cashoutIndicator = GetAnswerToFielMap(questionnaire, appFieldMap, "cashoutindicator");
                            if(cashoutIndicator == "true")
                            {
                                sumOf.Add(GetAnswerToFielMap(questionnaire, appFieldMap, "cashoutamount"));
                            }
                                
                            application.LoanAmount = ComputeSum(sumOf);
                        }
                        else
                        {
                            application.SetPropertyValue(item.TargetProperty, questionAnswer.Answer);
                        }
                    }
                    else
                    {
                        application.SetPropertyValue(item.TargetProperty, questionAnswer.Answer);
                    }
                }  
            }

            application.TemporaryApplicationNumber = temporaryApplicationNumber;

            return application;
        }

        private string ComputeSum(List<string> list)
        {
            var loanAmount = 0.0m;

            foreach(var item in list)
            {
                var val = 0.0m;
                Decimal.TryParse(item,out val);
                loanAmount = loanAmount + val;
            }
            return String.Format("{0:0.##}", loanAmount); 
        }

        private string GetAnswerToFielMap(IQuestionnaire questionnaire, ApplicationFields appFieldList, string appFieldTarget)
        {
            var returnAnswerVal = string.Empty;
            var appField = appFieldList.Map.FirstOrDefault(x=>x.TargetProperty.Equals(appFieldTarget));

            if(appField != null)
            {
                var answer = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == appField.SectionId)
                                                                               .SelectMany(s => s.SubSections)
                                                                               .SelectMany(ss => ss.QuestionSections)
                                                                               .SelectMany(qs => qs.QuestionList)
                                                                               .FirstOrDefault(q => q.QuestionId == appField.QuestionId);
                if(answer != null)
                {
                    returnAnswerVal = answer.Answer;
                }
            }
            
            return returnAnswerVal;
        }

        /// <summary>
        /// This function fetches the loan detail
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<ITransitionFormExtended> GetApplicationLoanDetail(string temporaryApplicationNumber, string userName)
        {
            Logger.Info($"GetApplicationLoanDetail:  Temporary Application Number: {temporaryApplicationNumber}");
            Logger.Info($"GetApplicationLoanDetail: Username: {userName}");
            var loanInformation = await GetQuestionnaireDetails(temporaryApplicationNumber, userName);

            return loanInformation;
        }

        /// <summary>
        /// Assign application number to the "ApplicationNumber" in the Questionnaire
        /// Also update the Questionnaire status for the logged in user
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        public async Task<IApplicationResponse> CreateApplication(IQuestionnaire questionnaire,IBorrowerCoBorrowerSpouseInfo borrowerInfo) //string temporaryApplicationNumber)
        {
                //var res = await GetApplicantRequest(null, questionnaire);
                var applicantRequest = await GetApplicantRequest(null, questionnaire); // temporaryApplicationNumber);
                applicantRequest.ApplicationNumber = questionnaire.TemporaryApplicationNumber;
                applicantRequest.Status = new Status();
                applicantRequest.Status.Code = Extensions.GetDescription(DefaultApplicationStatusCode.Review);
                applicantRequest.Status.Label = Extensions.GetDescription(DefaultApplicationStatusLabel.Review);
                applicantRequest.Status.Name = Extensions.GetDescription(DefaultApplicationStatusName.Review);
                Docitt.Application.IApplicationResponse response = null;                
                response = ApplicationService.Add(applicantRequest);
               
                

                return response;
        }


        private async Task PublishApplicationSubmittedEvent(IQuestionnaire questionnaire,IBorrowerCoBorrowerSpouseInfo borrowerInfo)
        {
            #region "Publish the event for submit application"
               Logger.Info($"ApplicationSubmitted Event Called");
                // get the borrower info
                var applicationSubmittedData = new ApplicationSubmitted(questionnaire.TemporaryApplicationNumber);
                 applicationSubmittedData.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                if(borrowerInfo != null)
                {  
                     applicationSubmittedData.email = borrowerInfo.Email;
                     applicationSubmittedData.borrowerName = borrowerInfo.FirstName + " " + borrowerInfo.LastName;
                }
               
               
                applicationSubmittedData.borrowerTemplateName = QuestionnaireConfigurations.BorrowerTemplateName;
                applicationSubmittedData.borrowerTemplateVersion = QuestionnaireConfigurations.BorrowerTemplateVersion;
                applicationSubmittedData.borrowerLoginURL = QuestionnaireConfigurations.BorrowerLoginURL;
                
                if(!string.IsNullOrEmpty(questionnaire.AssignedLoanOfficerUserName))
                {
                    Docitt.UserProfile.UserProfile userProfile =null; 
                    try
                    {
                        userProfile = (Docitt.UserProfile.UserProfile)await UserProfileService.GetProfile(questionnaire.AssignedLoanOfficerUserName);
                    }
                    catch(Exception ex)
                    {
                        Logger.Info($"Could not get User Profile information of the user {questionnaire.AssignedLoanOfficerUserName} : Error " + ex.Message);                            
                    }   
                        applicationSubmittedData.loanOfficerEmail = questionnaire.AssignedLoanOfficerUserName;

                        if(userProfile != null)
                        {
                            applicationSubmittedData.loanOfficerName = userProfile.FirstName+ " " + userProfile.LastName;
                            applicationSubmittedData.loanOfficerNMLS = userProfile.NmlsNumber;
                            applicationSubmittedData.loanOfficerPhoneNumber = !string.IsNullOrEmpty(userProfile.MobileNumber) ? GetFormattedPhoneNumber(userProfile.MobileNumber) : GetFormattedPhoneNumber(userProfile.Phone);
                        }
                        else                         
                        {
                            Logger.Info($"Could not get User Profile information of the user {questionnaire.AssignedLoanOfficerUserName}");                            
                        }   
                        applicationSubmittedData.loanOfficerTemplateName = QuestionnaireConfigurations.LoanOfficerTemplateName;
                        applicationSubmittedData.loanOfficerTemplateVersion = QuestionnaireConfigurations.LoanOfficerTemplateVersion;
                        applicationSubmittedData.loanOfficerLoginURL = QuestionnaireConfigurations.LenderLoginURL;
                }        
                //await EventHubClient.Publish(new ApplicationSubmitted(questionnaire.TemporaryApplicationNumber));
                //if(borrowerInfo.ActualApplicantType == ApplicantType.Borrower)
                
                if(borrowerInfo.ApplicantType == ApplicantType.Borrower || borrowerInfo.ApplicantType== ApplicantType.Spouse)
                {
                    await EventHubClient.Publish(applicationSubmittedData);
                }
                else  //for other co-borrower we have to send CoBorrowerApplicationSubmitted event
                {
                    CoBorrowerApplicationSubmitted coBorrowerApplicationSubmittedData = new CoBorrowerApplicationSubmitted(applicationSubmittedData.TemporaryApplicationNumber,applicationSubmittedData);
                    
                    await EventHubClient.Publish(coBorrowerApplicationSubmittedData);
                }
                Logger.Info($"ApplicationSubmitted Event End");

                #endregion
        }


        /// <summary>
        /// This function is used to create the Application from the Questionnaire
        /// </summary>
        /// <returns></returns>
        public async Task<IApplicationCreationResponse> Confirm(string temporaryApplicationNumber)
        {
            //string temporaryApplicationNumber = await GetTemporaryApplicationNumber();
            // ITransitionFormExtended BorrowerQuestionnaireDetail = new TransitionForm();
            Logger.Debug($"Started getting questionnaire for {temporaryApplicationNumber}");
            var questionnaire = await GetDecryptedQuestionnaire(await GetQuestionnaire(temporaryApplicationNumber));
            Logger.Debug($"Ended getting questionnaire for {temporaryApplicationNumber}");

            var status = false;

            var userName = await GetCurrentUser();
            var currentUserStateData = questionnaire.UserState.FirstOrDefault(x => x.UserName == userName);
            if (currentUserStateData == null)
            {
                throw new NotFoundException("Invalid userName for the given application");
            }

            IUserAccessedStatus spouseUserStateData = null;
            if (!string.IsNullOrWhiteSpace(currentUserStateData.SpouseUserName))
            {
                spouseUserStateData = questionnaire.UserState.FirstOrDefault(x =>
                    x.UserName == currentUserStateData.SpouseUserName);                
            }

            ////////////////Update User Profile
            Logger.Debug($"Started getting borrower information with spouse information for {temporaryApplicationNumber}");
            var borrowerInfo = await GetBorrowerWithSpouseInfo(temporaryApplicationNumber, userName, questionnaire);
            Logger.Debug($"Ended getting borrower information with spouse information for {temporaryApplicationNumber}");
            // update the user profile of the logged-in user
            await UpdateUserProfile(temporaryApplicationNumber, userName, borrowerInfo);

            
            if (questionnaire?.ApplicationNumber != null)
            {
                //user can submit application only once from end point
                //application already submitted and submitting second time
                //Current user already submitted application then just return
                if (currentUserStateData.Status != FormStatus.Open)
                {
                    IApplicationCreationResponse appStatusData = new ApplicationCreationResponse
                    {
                        Status = true,
                        ApplicationNumber = temporaryApplicationNumber
                    };
                    return appStatusData;
                }
                //Check if Spouse also submitted application                    
                if (spouseUserStateData != null && spouseUserStateData.Status != FormStatus.Open)
                {
                    //Return if Spouse has submitted application
                    IApplicationCreationResponse appStatusData = new ApplicationCreationResponse
                    {
                        Status = true,
                        ApplicationNumber = temporaryApplicationNumber
                    };
                    return appStatusData;
                }
                Logger.Debug($"UpdateApplicationNumber started for {temporaryApplicationNumber}");
                status = await QuestionnaireRepository.UpdateApplicationNumber(temporaryApplicationNumber,
                    questionnaire.ApplicationNumber, userName);

                //Update the spouse application as well.
                //TODO: By Nayan: Refactor it to have single call for Borrower and Spouse or CoBorrower and CoBorrowerSpouse
                if (spouseUserStateData != null)
                {
                    status = await QuestionnaireRepository.UpdateApplicationNumber(temporaryApplicationNumber,
                        questionnaire.ApplicationNumber, spouseUserStateData.UserName);
                }
                Logger.Debug($"UpdateApplicationNumber ended for {temporaryApplicationNumber}");
                await UpdateApplicationAndApplicantData(questionnaire);
                Logger.Debug($"UpdateApplicationAndApplicantData ended for {temporaryApplicationNumber}");
            }
            else
            {
                // Create application, if the ApplicationNumber is null
                Logger.Debug($"Create application started for {temporaryApplicationNumber}");
                var res = await CreateApplication(questionnaire,borrowerInfo);

                if (res != null)
                {
                    Logger.Info($"Application {res.ApplicationNumber} is created. for the Questionnaire application Number {temporaryApplicationNumber}..");
                    status = SetStatus(res.ApplicationNumber);

                    if (status)
                    {
                        Logger.Info($"Updated the status for Application {res.ApplicationNumber}.");

                        if (temporaryApplicationNumber == null)
                        {
                            throw new QuestionnaireNotFoundException();
                        }
                        Logger.Debug($"UpdateSectionCompletionStatus started for {temporaryApplicationNumber}");
                        await UpdateSectionCompletionStatus(temporaryApplicationNumber, Convert.ToInt32(SectionDetails.Summary), true);
                        
                        status = await QuestionnaireRepository.UpdateApplicationNumber(temporaryApplicationNumber,
                            res.ApplicationNumber, userName);

                        //Update the spouse application as well.
                        //TODO: By Nayan: Refactor it to have single call for Borrower and Spouse
                        if (spouseUserStateData != null)
                        {
                            status = await QuestionnaireRepository.UpdateApplicationNumber(temporaryApplicationNumber,
                                res.ApplicationNumber, spouseUserStateData.UserName);
                        }
                        Logger.Debug($"UpdateApplicationNumber ended for {temporaryApplicationNumber}");
                    }
                    else
                    {
                        //TODO: By Nayan: What will happen if status is not got updated or retrieved ? Just log?
                        Logger.Info($"Status could not get updated for Application {res.ApplicationNumber}.");
                    }
                }
                else
                {
                    Logger.Info($"Application could not get created for the Questionnaire application Number {temporaryApplicationNumber}..");
                    throw new Exception($"Application could not get created for the Questionnaire application Number {temporaryApplicationNumber}..");
                }
            }

            if(currentUserStateData.Status == FormStatus.Open)
            {
                await PublishApplicationSubmittedEvent(questionnaire,borrowerInfo);
            }

            // Logic to add conditions for borrower and spouse.
            var users = new List<string> {currentUserStateData.UserName, currentUserStateData.SpouseUserName};
            var sourceBy = userName;
            Logger.Debug($"AddRequiredConditions started for {temporaryApplicationNumber}");
            Parallel.ForEach(users, user => 
            {
                if (!string.IsNullOrWhiteSpace(user))
                {                   
                     AddRequiredConditions(temporaryApplicationNumber, user, questionnaire, borrowerInfo,
                        sourceBy).GetAwaiter().GetResult();
                }
            });
            Logger.Debug($"AddRequiredConditions ended for {temporaryApplicationNumber}");
            IApplicationCreationResponse appStatus = new ApplicationCreationResponse
            {
                Status = status,
                ApplicationNumber = temporaryApplicationNumber
            };

            return appStatus;
        }
        private async Task AddRequiredConditions(string temporaryApplicationNumber, string userName, IQuestionnaire questionnaire,
            IBorrowerCoBorrowerSpouseInfo borrowerInfo, string sourceBy)
        {
            var isSpouseUser = false;
            var spouseUserName = (borrowerInfo != null) ? borrowerInfo.SpouseUserName : string.Empty;
            var borrowerQuestionnaireDetail = await GetQuestionnaireDetails(temporaryApplicationNumber, userName);           

            if (borrowerQuestionnaireDetail != null)
            {
                if (borrowerQuestionnaireDetail.ApplicantType == ApplicantType.Spouse || borrowerQuestionnaireDetail.ApplicantType == ApplicantType.CoBorrowerSpouse)
                {
                    isSpouseUser = true;
                }

            }

            IRequiredConditionRuleParam requiredConditionParam = new RequiredConditionRuleParam();
            IApplicationData applicationData = new ApplicationData();
            requiredConditionParam.isCustom = false;
            requiredConditionParam.priority = "Normal";
            requiredConditionParam.role = borrowerQuestionnaireDetail != null ? borrowerQuestionnaireDetail.ApplicantType.ToString() : string.Empty;
            requiredConditionParam.sendTo = new List<string>() { userName };
            requiredConditionParam.sourceBy = sourceBy;
            requiredConditionParam.UserName = userName;
            requiredConditionParam.dueDate = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            requiredConditionParam.isSpouseUser = isSpouseUser;
            requiredConditionParam.spouseUserName = spouseUserName;
            applicationData.alimonyorChildSupportAmount = borrowerQuestionnaireDetail.AlimonyAmount;
            applicationData.businessSelfEmploymentIncome = borrowerQuestionnaireDetail.BusinessEmploymentIncome;
            applicationData.employmentIncome = borrowerQuestionnaireDetail.EmploymentIncome;
            applicationData.gender = string.IsNullOrEmpty(borrowerQuestionnaireDetail.Gender) ? "" : borrowerQuestionnaireDetail.Gender;

            var currentUserStateData = questionnaire.UserState.FirstOrDefault(x => x.UserName == userName);

            //user can submit application only once from end point
            //application already submitted and submitting second time
            applicationData.IsAcknowledgement = true;
            if (currentUserStateData != null && currentUserStateData.Status == FormStatus.Open && currentUserStateData.UserName!= sourceBy)
            {
                applicationData.IsAcknowledgement = false;
            }
           
            applicationData.isBankBankrupt = string.IsNullOrEmpty(borrowerQuestionnaireDetail.IsBankrupt) ? "" : borrowerQuestionnaireDetail.IsBankrupt; //BorrowerQuestionnaireDetail.IsBankrupt;
            applicationData.isInContract = string.IsNullOrEmpty(borrowerQuestionnaireDetail.IsInContract) ? "" : borrowerQuestionnaireDetail.IsInContract;
            applicationData.militaryPayAmount = borrowerQuestionnaireDetail.MilitaryPayAmount;
            applicationData.isBorrowerVAEligible = borrowerQuestionnaireDetail.IsBorrowerVAEligible;
            applicationData.propertyType = string.IsNullOrEmpty(borrowerQuestionnaireDetail.PropertyType) ? "" : borrowerQuestionnaireDetail.PropertyType;
            applicationData.purposeLoan = string.IsNullOrEmpty(borrowerQuestionnaireDetail.LoanPurpose) ? "" : borrowerQuestionnaireDetail.LoanPurpose;
            applicationData.residencyType = string.IsNullOrEmpty(borrowerQuestionnaireDetail.ResidencyType) ? "" : borrowerQuestionnaireDetail.ResidencyType;
            applicationData.socialSecurityIncome = borrowerQuestionnaireDetail.SocialSecurityIncome;
            applicationData.transactionClosingStatement = string.IsNullOrEmpty(borrowerQuestionnaireDetail.isTransactionClosingStatement) ? "" : borrowerQuestionnaireDetail.isTransactionClosingStatement;
            applicationData.usCitizen = string.IsNullOrEmpty(borrowerQuestionnaireDetail.UsCitizen) ? "" : borrowerQuestionnaireDetail.UsCitizen;
            applicationData.companyStackLT25Per = borrowerQuestionnaireDetail.companyStackLessThan25Percentage;

            applicationData.DischargeFromActiveDuty = borrowerQuestionnaireDetail.DischargeFromActiveDuty;

            var largeDepositPayload = new LargeDepositPayload();
            largeDepositPayload.applicationNumber = temporaryApplicationNumber;
            //Set customer id as main borrower if logged in user is spouse            
            largeDepositPayload.customerId = userName;           
            var largeDeposits = "No";
            var token = TokenParser.Parse(TokenReader.Read());
            var reader = new StaticTokenReader(token.Value);
            var decisionEngine = DecisionEngineFactory.Create(reader);
            Logger.Debug($"Checklargedeposits started for {temporaryApplicationNumber}");
            var result = await Task.Run(() => decisionEngine.Execute<dynamic, JObject>("checklargedeposits", largeDepositPayload));
            Logger.Debug($"Checklargedeposits ended for {temporaryApplicationNumber}");
            if (result != null)
            {
                var statusResult = result.GetValue("status").ToString();

                if (statusResult.ToLower() == "success")
                {
                    try
                    {
                        var dataResult = result.GetValue("data").ToString();
                        if (dataResult.ToLower() == "true")
                        {
                            // has large deposits
                            largeDeposits = "Yes";
                            Logger.Info($"Large Deposit rule ran successfully for the Application {temporaryApplicationNumber}.");
                        }
                        else
                        {
                            // does not have large deposits
                            largeDeposits = "No";
                            Logger.Info($"Large Deposit rule failed for the Application {temporaryApplicationNumber}.");
                        }
                    }
                    catch (Exception ex)
                    {
                        largeDeposits = "No";
                        Logger.Error($"Large deposit rules failed for the application {temporaryApplicationNumber}", ex);
                    }
                }
            }

            var incomeSection = questionnaire.ApplicationForm.Sections.
                    Where(s => s.SectionId == Convert.ToInt32(SectionDetails.Income)).FirstOrDefault();
            if (incomeSection != null && incomeSection.SubSections.Where(sub => sub.UserName == userName).Count() > 0)
            {
                var employmentGap = incomeSection.SubSections.Where(sub => sub.UserName == userName)
                                    .SelectMany(d => d.QuestionSections).Where(x => x.QuestionList != null)
                                    .SelectMany(qs => qs.QuestionList)
                                    .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerEmploymentGap);
                if (employmentGap != null && employmentGap.Answer.ToLower() == "yes")
                    applicationData.IsEmploymentGap = true;
                else
                    applicationData.IsEmploymentGap = false;

                var selfEmploymentGap = incomeSection.SubSections.Where(sub => sub.UserName == userName)
                                        .SelectMany(d => d.QuestionSections).Where(x => x.QuestionList != null)
                                        .SelectMany(qs => qs.QuestionList)
                                        .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerSelfEmploymentGap);
                if (selfEmploymentGap != null && selfEmploymentGap.Answer.ToLower() == "yes")
                    applicationData.IsSelfEmploymentGap = true;
                else
                    applicationData.IsSelfEmploymentGap = false;
            }


            // Core logic for add condition if do you want to add spouse as a coborrower NO.
            if (borrowerQuestionnaireDetail.ApplicantType == ApplicantType.Borrower || borrowerQuestionnaireDetail.ApplicantType == ApplicantType.CoBorrower)
            {
                var profileSectionQuestionList = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == Convert.ToInt32(SectionDetails.Profile))
                    .SelectMany(s => s.SubSections).Where(ss => ss.UserName == userName)
                    .SelectMany(ss => ss.QuestionSections).Where(x => x.QuestionList != null)
                    .SelectMany(qs => qs.QuestionList).ToList();
                
                var individualOrJointQuestion = profileSectionQuestionList.FirstOrDefault(q =>
                        string.Equals(q.QuestionFieldName, ConstantUtils.BorrowerIndividualOrJointCredit, StringComparison.CurrentCultureIgnoreCase));

                Logger.Debug($"Verifying if applicant is individual or joint:{individualOrJointQuestion?.Answer} - {temporaryApplicationNumber}");

                applicationData.SpouseAsNotACoBorrower = GetSpouseAsNotACoBorrowerStatus(profileSectionQuestionList, individualOrJointQuestion, borrowerQuestionnaireDetail.ApplicantType, temporaryApplicationNumber);

                var spouseAsNotACoBorrowerResult =
                    profileSectionQuestionList.FirstOrDefault(q => string.Equals(q.QuestionFieldName, Constant.BorrowerAddSpouseAsCoBorrower, StringComparison.OrdinalIgnoreCase));
                if(spouseAsNotACoBorrowerResult!=null && string.Equals(spouseAsNotACoBorrowerResult.Answer, "false", StringComparison.OrdinalIgnoreCase))
                {
                    applicationData.SpouseAsNotACoBorrower = true;
                }
            }

            //largeDeposits = "Yes";
            borrowerQuestionnaireDetail.isLargeDeposit = largeDeposits;
            applicationData.isLargeDeposit = largeDeposits;
            requiredConditionParam.applicationData = applicationData;
            IRequiredConditionRequest requiredConditionrequest = new RequiredConditionRequest();
            requiredConditionrequest.entityType = "application";
            requiredConditionrequest.entityId = temporaryApplicationNumber;
            requiredConditionrequest.payload = requiredConditionParam;
            Logger.Debug($"AddDefaultCondition started for {temporaryApplicationNumber}");
            var requiredConditionAdditionResult = await Task.Run(() =>
                    decisionEngine.Execute<dynamic, JObject>("addDefaultCondition", requiredConditionrequest)
                    );
            Logger.Debug($"AddDefaultCondition ended for {temporaryApplicationNumber}");
            if (requiredConditionAdditionResult != null)
            {
                var statusResult = requiredConditionAdditionResult.GetValue("status").ToString();

                if (statusResult.ToLower() == "success")
                {
                    Logger.Info($"Required condition for the Application {temporaryApplicationNumber} have been added.");
                }
            }
        }
        public bool GetSpouseAsNotACoBorrowerStatus(List<IQuestion> profileSectionQuestionList, IQuestion individualOrJointQuestion, ApplicantType applicantType, string temporaryApplicationNumber) {
            if (applicantType == ApplicantType.CoBorrower || individualOrJointQuestion != null)
            {
                string questionFieldNameToCompare = "";
                if(individualOrJointQuestion != null)
                {
                    if(individualOrJointQuestion.Answer == ConstantUtils.IndividualCredit) questionFieldNameToCompare = ConstantUtils.BorrowerMaritalStatus;
                    else if(individualOrJointQuestion.Answer == ConstantUtils.JointCredit) questionFieldNameToCompare = ConstantUtils.BorrowerMaritalStatusMarried;
                }
                else if (applicantType == ApplicantType.CoBorrower) questionFieldNameToCompare = ConstantUtils.BorrowerMaritalStatus;
                
                 var isMarriedQuestion = profileSectionQuestionList.FirstOrDefault(q => string.Equals(q.QuestionFieldName, questionFieldNameToCompare, StringComparison.OrdinalIgnoreCase));
                        
                  Logger.Debug($"Verifying if applicant is married :{isMarriedQuestion?.Answer} - {temporaryApplicationNumber}");

                    if(individualOrJointQuestion != null && individualOrJointQuestion.Answer == ConstantUtils.IndividualCredit){
                        if (isMarriedQuestion != null && isMarriedQuestion.Answer != ConstantUtils.Unmarried)
                           return true;    
                    } 
                    else if (isMarriedQuestion != null && isMarriedQuestion.Answer == ConstantUtils.Separated)
                           return true;
            }
            return false;
        }
        public async Task UpdateApplicationAndApplicantData(IQuestionnaire questionnaire)
        {
            var applicantRequest = await GetApplicantRequest(null, questionnaire);

            if(applicantRequest != null)
            {
                var applicationResponse = ApplicationService.GetByApplicationNumber(questionnaire.ApplicationNumber);
                
                if(applicationResponse != null)
                {
                    foreach(var applicant in applicantRequest.Applicants)
                    {
                        var savedApplicant = ApplicantService.GetByUserId(applicant.UserIdentity.UserId);
                        
                        if(savedApplicant != null)
                        {
                            var processApplicant = applicationResponse.Applicants.Where(a => a.ApplicantId == savedApplicant.Id).FirstOrDefault();
                            if(processApplicant != null)
                            {
                                applicant.ApplicantId = processApplicant.ApplicantId;
                                applicant.IsPrimary = processApplicant.IsPrimary;
                            }
                            else
                            {
                                Logger.Debug($"applicant not found in application response for :" + savedApplicant.Id);
                            }
                        }                
                    }

                    var response = ApplicationService.AddOrUpdateApplicants(questionnaire.ApplicationNumber, applicantRequest.Applicants);
                }
            }
        }

        public async Task<bool> CreatePostApplication(string temporaryApplicationNumber)
        {
            var status = false;
            if (temporaryApplicationNumber != null)
            {
                var userName = await GetCurrentUser();
                var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
                var lastAccessedSection = questionnaire.ApplicationForm.Sections.Where(s => s.Type == SectionType.PostApplication).OrderBy(s => s.SeqNo).Select(s => s.SectionId).FirstOrDefault();

                var tasks = new List<Task>();
                tasks.Add(QuestionnaireRepository.UpdateStatus(
                            temporaryApplicationNumber, 
                            userName, 
                            FormStatus.WelcomePostApplication, 
                            lastAccessedSection, null));
                
                // Update Spouse UserState to WelcomePostApplication if they are on PostApplication.
                var userState = questionnaire.UserState.FirstOrDefault(x => x.UserName == userName);
                if (userState != null && !string.IsNullOrEmpty(userState.SpouseUserName))
                {
                    var spouseUserState = questionnaire.UserState.FirstOrDefault(u => u.UserName == userState.SpouseUserName);

                    if(spouseUserState != null && (spouseUserState.Status == FormStatus.PostApplication || spouseUserState.Status == FormStatus.Open))
                    {
                        tasks.Add(QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber, 
                                                               userState.SpouseUserName, 
                                                               FormStatus.WelcomePostApplication, 
                                                               lastAccessedSection, 
                                                               null));
                    }
                }
                await Task.WhenAll(tasks);
                status = true;
            }
            else
            {
                throw new QuestionnaireNotFoundException();
            }

            return status;
        }
        public async Task<bool> UpdateApplicationStatus(string temporaryApplicationNumber, string applicationStatus)
        {
            var status = false;

            if (!string.IsNullOrEmpty(applicationStatus))
            {
                if (applicationStatus.ToLower().Equals(FormStatus.WelcomePostApplication.ToString().ToLower()))
                {
                    if (temporaryApplicationNumber != null)
                    {
                        var userName = await GetCurrentUser();
                        Logger.Debug($"GetQuestionnaire started for {temporaryApplicationNumber}");
                        var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                        var lastAccessedSection = questionnaire.ApplicationForm.Sections.Where(s => s.Type == SectionType.PostApplication).OrderBy(s => s.SeqNo).Select(s => s.SectionId).FirstOrDefault();
                        Logger.Debug($"UpdateStatus started for {temporaryApplicationNumber}");
                        await QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber, userName, FormStatus.PostApplication, lastAccessedSection, null);
                        Logger.Debug($"UpdateStatus ended for {temporaryApplicationNumber}");
                        status = true;
                    }
                    else
                    {
                        throw new QuestionnaireNotFoundException();
                    }
                }
                else if (applicationStatus.ToLower().Equals(FormStatus.WelcomePersonalDashboard.ToString().ToLower()))
                {
                    if (string.IsNullOrEmpty(temporaryApplicationNumber))
                        throw new QuestionnaireNotFoundException();

                    var userName = await GetCurrentUser();
                    Logger.Debug($"GetBorrowersAndCoBorrowersAndSpouse started for {temporaryApplicationNumber}");
                    var allUserInfo = await GetBorrowersAndCoBorrowersAndSpouse(temporaryApplicationNumber);
                    Logger.Debug($"GetBorrowersAndCoBorrowersAndSpouse ended for {temporaryApplicationNumber}");
                    var userInfo = allUserInfo.First(s => s.UserName == userName);

                    if (userInfo.ActualApplicantType == ApplicantType.Borrower || userInfo.ActualApplicantType == ApplicantType.Spouse)
                    {
                        var spouseUser = allUserInfo.Where(s => s.ActualApplicantType == ApplicantType.Spouse).Select(s => s.UserName).FirstOrDefault();
                        Logger.Debug($"UpdateStatus started for {temporaryApplicationNumber}");
                        await QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber, FormStatus.Application, allUserInfo.Where(s => s.ActualApplicantType == ApplicantType.Borrower).Select(s => s.UserName).First());

                        if (spouseUser != null)
                        {
                            await QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber,FormStatus.Application, spouseUser);
                        }
                        Logger.Debug($"UpdateStatus ended for {temporaryApplicationNumber}");
                        status = true;
                    }
                    else if (userInfo.ActualApplicantType == ApplicantType.CoBorrower || userInfo.ActualApplicantType == ApplicantType.CoBorrowerSpouse)
                    {
                        var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                        var coBorrowerSpouseUser = await GetCoupleUserName(questionnaire.UserState, userName);

                        await QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber, FormStatus.Application, userName);

                        if (coBorrowerSpouseUser != null)
                        {
                            await QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber, FormStatus.Application, coBorrowerSpouseUser);
                        }

                        status = true;
                    }
                    else
                    {
                        //If user is neither primary borrower or spouse co-borrower
                        if (userName != null)
                        {
                            await QuestionnaireRepository.UpdateStatus(temporaryApplicationNumber, FormStatus.Application, userName);
                        }

                        status = true;
                    }
                }
            }

            return status;
        }

        public async Task<bool> CompletePostApplication(string applicationNumber)
        {
            if (string.IsNullOrEmpty(applicationNumber))
                throw new QuestionnaireNotFoundException();


            //If user is Primary Borrower then update the status for spouse coborrower as well. Vice versa.

            var userName = await GetCurrentUser();
            Logger.Debug($"GetBorrowersAndCoBorrowersAndSpouse started for {applicationNumber}");
            var allUserInfo = await GetBorrowersAndCoBorrowersAndSpouse(applicationNumber);
            Logger.Debug($"GetBorrowersAndCoBorrowersAndSpouse ended for {applicationNumber}");
            var userInfo = allUserInfo.First(s => s.UserName == userName);

            if (userInfo.ActualApplicantType == ApplicantType.Borrower || userInfo.ActualApplicantType == ApplicantType.Spouse)
            {

                var spouseUser = allUserInfo.Where(s => s.ActualApplicantType == ApplicantType.Spouse).Select(s => s.UserName).FirstOrDefault();
                Logger.Debug($"UpdateStatus started for {applicationNumber}");
                await QuestionnaireRepository.UpdateStatus(applicationNumber, FormStatus.WelcomePersonalDashboard, allUserInfo.Where(s => s.ActualApplicantType == ApplicantType.Borrower).Select(s => s.UserName).First());

                if (spouseUser != null)
                {
                    await QuestionnaireRepository.UpdateStatus(applicationNumber, FormStatus.WelcomePersonalDashboard, spouseUser);
                }
                Logger.Debug($"UpdateStatus ended for {applicationNumber}");
                return true;
            }
            else
            {
                //If user is neither primary borrower or spouse co-borrower
                if (userName != null)
                {
                    Logger.Debug($"UpdateStatus started for {applicationNumber}");
                    await QuestionnaireRepository.UpdateStatus(applicationNumber, FormStatus.WelcomePersonalDashboard, userName);
                    Logger.Debug($"UpdateStatus ended for {applicationNumber}");
                }

                return true;
            }

        }
        public string GetFormattedPhoneNumber(string phone)
        {
            if (!string.IsNullOrWhiteSpace(phone) && phone.Trim().Length == 10)
                return string.Format("({0}) {1}-{2}", phone.Substring(0, 3), phone.Substring(3, 3), phone.Substring(6, 4));
            return phone;
        }
    }
}
