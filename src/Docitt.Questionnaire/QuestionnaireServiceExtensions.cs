﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;


namespace Docitt.Questionnaire
{
    public static class Extensions
    {
        public static T Clone<T>(T source)
        {
            var serialized = JsonConvert.SerializeObject(source, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
            });
            
            return JsonConvert.DeserializeObject<T>(serialized, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects
            });
        }

        public static void SetPropertyValue(this Object obj, string propName, object value)
        {

            var prop = obj.GetType()
                                   .GetProperty(propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);

            if (null != prop && prop.CanWrite)
            {
                if (propName == "loanamount")
                {
                    ILoanCalculationInfo loanInfo = JsonConvert.DeserializeObject<LoanCalculationInfo>(value.ToString());
                    if (loanInfo != null)
                        prop.SetValue(obj, loanInfo.LoanAmount, null);
                }
                else
                {
                    prop.SetValue(obj, value, null);
                }
            }
        }

        public static string GetDescription<T>(this T enumerationValue) where T : struct
        {
            var type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", nameof(enumerationValue));
            }

            //Tries to find a DescriptionAttribute for a potential friendly name
            //for the enum
            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo.Length > 0)
            {
                var attrs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);

                if (attrs.Length > 0)
                {
                    //Pull out the description value
                    return ((System.ComponentModel.DescriptionAttribute)attrs[0]).Description;
                }
            }
            //If we have no description attribute, just return the ToString of the enum
            return enumerationValue.ToString();
        }

        public static IList<DateTime> GetMissingMonths(IList<DateTime> currentList, DateTime startDate, DateTime endDate)
        {
            // Create a list for the missing months
            IList<DateTime> missingList = new List<DateTime>();

            // Select a startdate
            var testingDate = startDate;

            // Begin by the month of startDate and ends with the month of endDate
            // month of startDate and endDate included
            while (testingDate.Date <= endDate.Date)
            {
                if (currentList.Count(m => m.Month == testingDate.Month && m.Year == testingDate.Year) == 0)
                {
                    missingList.Add(new DateTime(testingDate.Year, testingDate.Month, 1));
                }
                testingDate = testingDate.AddMonths(1);
            }
            return missingList;
        }

        public static int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            var monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }
    }
}