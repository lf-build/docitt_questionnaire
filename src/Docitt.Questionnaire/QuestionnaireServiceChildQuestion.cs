using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        /// <summary>
        /// This method returns true, if any parent question wants to show child question which may belong to multiple parent 
        /// </summary>
        /// <param name="childSubSections">SubSection to while child belongs</param>
        /// <param name="childQ">child question</param>
        /// <returns></returns>
        private bool AnyResetObjectionFromOtherParentsOfChild(IEnumerable<ISubSection> childSubSections, IChildConditionalQuestion childQ)
        {
            bool returnVal = false;

            var allQuestions = childSubSections.SelectMany(x => x.QuestionSections).SelectMany(x => x.QuestionList);

            foreach(var _question in allQuestions)
            {
                var childQuestion = _question?.ChildQuestions?
                                     .FirstOrDefault(x => x.QuestionId == childQ?.QuestionId && x.SubSectionId == childQ?.SubSectionId);

                if(childQuestion != null)
                {
                    // If this parent's answer is matching the child's condition 
                    if(string.Equals(_question?.Answer, childQuestion?.Answer, StringComparison.InvariantCultureIgnoreCase))
                    {
                        Logger.Debug($"ParentQuestionFieldName '{_question.QuestionFieldName}' ParentQuestionId '{_question.QuestionId}' " +
                                     $" objected to reset ChildQuestionId '{childQ.QuestionId}'");
                        returnVal = true; 
                        break;
                    }
                }

                if(returnVal)
                    break; //skip if any parent objects
            }
            return returnVal;
        }

        private bool IsQuestionToUpdateAnswerMatchesChild(string parentQuestionAnswer, string childQuestionAnswer)
        {
            // Handle for Parent with simple answer like "Investment Property"
            if(String.Equals(parentQuestionAnswer, childQuestionAnswer, StringComparison.InvariantCultureIgnoreCase))
                return true;

            // Handle for the Parent is an array like ["\"bonus"\","\"commission"\"]
            if(parentQuestionAnswer.Length > 0 && childQuestionAnswer.Length > 0 && string.Equals(parentQuestionAnswer.Substring(0,1),"["))
            {
                var childAnswerAsElement = string.Concat("\"",childQuestionAnswer,"\"");
                if(parentQuestionAnswer.IndexOf(childAnswerAsElement,StringComparison.InvariantCultureIgnoreCase) > 0)
                    return true;        
            }
            return false;
        }

        /// <summary>
        /// This method resets the child and grandchild questionanswer when parent questionanswer is updated
        /// </summary>
        /// <param name="questionRequest">parent questionrequest model</param>
        /// <param name="questionToUpdate">parent question</param>
        /// <param name="sectionToProcess">section</param>
        /// <param name="childSections">child sections</param>
        /// <returns></returns>
        private async Task ResetChildQuestionAnswer(IAnswerUpdateRequest questionRequest, IQuestion questionToUpdate, ISection sectionToProcess, Dictionary<int, ISection> childSections)
        {
            if (questionToUpdate == null || questionToUpdate.ChildQuestions == null)
                return;
            
            // TODO this logic should be refactored. It should make a single call to resetChildQuestion for a given questionId
            //      e.g. If we have to show questionId 100 for "Single-Family" and "Multi-Family", below logic should be modified.
            foreach(var childQuestion in questionToUpdate.ChildQuestions)
            {
                if (childQuestion.Answer != null)
                {
                    bool resetChildQuestion =  !IsQuestionToUpdateAnswerMatchesChild( questionToUpdate.Answer, childQuestion.Answer);
                    
                    if(resetChildQuestion)
                    {
                        // Check values of other parents of this child question.
                        // If any parent question wants to show, then don't reset the child question
                        var childSubSections =  sectionToProcess.SubSections
                                .Where(x => x.SubSectionId == childQuestion?.SubSectionId); // same subsection
                        resetChildQuestion = !AnyResetObjectionFromOtherParentsOfChild(childSubSections, childQuestion);
                    }

                    if (resetChildQuestion)
                    {
                        Logger.Debug($"Reseting ChildQuestion {childQuestion.QuestionId} for application {questionRequest.TemporaryApplicationNumber}" + 
                                        $" as parent question {questionToUpdate.QuestionFieldName} {questionToUpdate.QuestionId} is updated");
                        
                        // Delete Child Questions
                        await DeleteChildQuestionAnswer(questionRequest, childQuestion, childSections);
                    }
                }
                else
                {
                    // For Alimony Child support for Child Name & Chlld DOB
                    int answer = 0;
                    if(!string.IsNullOrEmpty(questionToUpdate.Answer) && Int32.TryParse(questionToUpdate.Answer, out answer) && answer <=0)
                    {
                        var sectionId = Convert.ToInt32(childQuestion.SectionId);
                        var childSubSectionId = childQuestion.SubSectionId;
                        var childquestionId = childQuestion.QuestionId;
                        
                        var childSubSection = sectionToProcess?.SubSections.FirstOrDefault(ss => ss.SubSectionId == childSubSectionId);
                        
                        if(childSubSection == null)
                            return;

                        var childQuestionQuestion = childSubSection.QuestionSections
                                                            .FirstOrDefault(x => x.QuestionSectionTemplateId != null && 
                                                                x.QuestionSectionTemplateId.ToLower() == Constant.ChildQuestionSectionTemplateId.ToLower());

                        if(childQuestionQuestion == null)
                            return;

                        var question = childQuestionQuestion.QuestionList.FirstOrDefault(ql => ql.QuestionId == childquestionId);
                        if (question == null)
                            return;

                        IQuestionnaireQuestionAnswer questionAnswerDeleted = new QuestionnaireQuestionAnswer();
                        questionAnswerDeleted.TemporaryApplicationNumber = questionRequest.TemporaryApplicationNumber;
                        questionAnswerDeleted.SectionId = sectionId;
                        questionAnswerDeleted.SubSectionId = childSubSectionId;
                        questionAnswerDeleted.QuestionSectionSeqNo = childQuestionQuestion.SeqNo;
                        questionAnswerDeleted.QuestionId = question.QuestionId;                                           
                        await QuestionnaireAnswerRepository.DeleteQuestionByQuestionId(questionAnswerDeleted);   
                    }  
                }
            }
        }

        /// <summary>
        /// This method deletes the child and grand child questionanswer
        /// </summary>
        /// <param name="questionRequest"></param>
        /// <param name="childQuestion"></param>
        /// <param name="childSections"></param>
        /// <returns></returns>
        private async Task DeleteChildQuestionAnswer(IAnswerUpdateRequest questionRequest, IChildConditionalQuestion childQuestion, Dictionary<int, ISection> childSections)
        {
            bool isRealonlyQuestion = false;
            bool doesHaveChildQuestions = false;
            var childQSectionSeqNo = childQuestion.QuestionSectionSeqNo;
            ISection childQSection = await GetChildSection(childSections, questionRequest.TemporaryApplicationNumber, childQuestion.SectionId);
            
            IAnswerUpdateRequest childQuestionRequest = new AnswerUpdateRequest();
            childQuestionRequest.QuestionId = childQuestion.QuestionId;
            childQuestionRequest.SectionId = childQuestion.SectionId;
            childQuestionRequest.SubSectionId = childQuestion.SubSectionId;

            if (childQSectionSeqNo == 0)
            {
                var details = GetChildQuestionDetails(childQSection, childQuestion);
                isRealonlyQuestion = details.isReadOnlyQuestion;
                doesHaveChildQuestions = details.doesHaveChildQuestions;
                childQuestion.QuestionSectionSeqNo = details.childQSectionSeqNo;
                childQSectionSeqNo  = details.childQSectionSeqNo;
            }

            childQuestionRequest.QuestionSectionSeqNo = childQSectionSeqNo;
            childQuestionRequest.TemporaryApplicationNumber = questionRequest.TemporaryApplicationNumber;
            childQuestionRequest.Answer = string.Empty;

            if (!isRealonlyQuestion)
            {
                IQuestionnaireQuestionAnswer questionnaireQuestionAnswer = new QuestionnaireQuestionAnswer();
                questionnaireQuestionAnswer.TemporaryApplicationNumber = childQuestionRequest.TemporaryApplicationNumber;
                questionnaireQuestionAnswer.SectionId = childQuestionRequest.SectionId;
                questionnaireQuestionAnswer.SubSectionId = childQuestionRequest.SubSectionId;
                questionnaireQuestionAnswer.QuestionSectionSeqNo = childQuestionRequest.QuestionSectionSeqNo;
                questionnaireQuestionAnswer.QuestionId = childQuestionRequest.QuestionId;
                questionnaireQuestionAnswer.Answer = childQuestionRequest.Answer;

                await QuestionnaireAnswerRepository.DeleteQuestionByQuestionId(questionnaireQuestionAnswer);

                if (doesHaveChildQuestions)
                    await UpdateGrandChildQuestions(childQSection, childQuestion, childQuestionRequest);
            }
        }

        /// <summary>
        /// This method returns section of child questionanswer given its sectionId
        /// </summary>
        /// <param name="childSections"></param>
        /// <param name="appNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        private async Task<ISection> GetChildSection(Dictionary<int, ISection> childSections, string appNumber, int sectionId)
        {
            ISection childQuestionSection = null;
            if (!childSections.TryGetValue(sectionId, out childQuestionSection))
            {
                childQuestionSection = await QuestionnaireRepository.GetSectionById(appNumber, sectionId);
                if (childQuestionSection != null)
                {
                    childSections.Add(sectionId, childQuestionSection);
                }
            }
            return childQuestionSection;
        }

        /// <summary>
        /// This method returns the child questionsection seqno, and properties like isReadOnlyQuestion, doesHaveChildQuestion
        /// </summary>
        /// <param name="childQSection">section</param>
        /// <param name="childQuestion">question</param>
        /// <returns></returns>
        private (int childQSectionSeqNo, bool isReadOnlyQuestion, bool doesHaveChildQuestions) GetChildQuestionDetails(ISection childQSection, IChildConditionalQuestion childQuestion)
        {
            int childQSectionSeqNo = 0;
            bool isRealonlyQuestion = false, doesHaveChildQuestions = false;


            var childQuestionList = childQSection?
                    .SubSections?.FirstOrDefault(ss => ss.SubSectionId == childQuestion.SubSectionId)?
                    .QuestionSections;

            if (childQuestionList != null)
            {
                foreach (var questionSection in childQuestionList)
                {
                    var question = questionSection.QuestionList.FirstOrDefault(ql => ql.QuestionId == childQuestion.QuestionId);
                    if (question == null)
                        continue; //continue search for child question

                    childQSectionSeqNo = questionSection.SeqNo;

                    if (question.IsReadOnly)
                        isRealonlyQuestion = true;
                    else
                    {
                        isRealonlyQuestion = false;
                        if (question.ChildQuestions != null && question.ChildQuestions.Count > 0)
                            doesHaveChildQuestions = true;
                    }
                    break;
                }
            }

            return (childQSectionSeqNo, isRealonlyQuestion, doesHaveChildQuestions);
        }

        /// <summary>
        /// Supports upto three level of child questions for update/reset
        /// </summary>
        /// <param name="childQSection"></param>
        /// <param name="childQuestion"></param>
        /// <param name="childQuestionRequest"></param>
        /// <returns></returns>
        private async Task UpdateGrandChildQuestions(ISection childQSection, IChildConditionalQuestion childQuestion, IAnswerUpdateRequest childQuestionRequest)
        {
            var childQuestionList = childQSection?
                        .SubSections?.FirstOrDefault(ss => ss.SubSectionId == childQuestion.SubSectionId)?
                        .QuestionSections?.FirstOrDefault(ss => ss.SeqNo == childQuestionRequest.QuestionSectionSeqNo)
                        .QuestionList?.FirstOrDefault(ss => ss.QuestionId == childQuestionRequest.QuestionId);

            if (childQuestionList == null || childQuestionList.ChildQuestions == null || childQuestionList.ChildQuestions.Count <= 0)
                return;

            foreach (var itemQuestion in childQuestionList.ChildQuestions)
            {
                if (itemQuestion.QuestionSectionSeqNo != 0)
                    continue;

                var questionSections = childQSection?
                        .SubSections?.FirstOrDefault(ss => ss.SubSectionId == childQuestionRequest.SubSectionId)?
                        .QuestionSections?.ToList();

                if (questionSections == null)
                    continue;
                
                foreach (var questionSection in questionSections)
                {
                    var question = questionSection.QuestionList.FirstOrDefault(ql =>
                            ql.QuestionId == itemQuestion.QuestionId);

                    if (question == null)
                        continue;
                    
                    IQuestionnaireQuestionAnswer questionAnswer = new QuestionnaireQuestionAnswer();
                    questionAnswer.TemporaryApplicationNumber = childQuestionRequest.TemporaryApplicationNumber;
                    questionAnswer.SectionId = childQuestionRequest.SectionId;
                    questionAnswer.SubSectionId = childQuestionRequest.SubSectionId;
                    questionAnswer.QuestionSectionSeqNo = questionSection.SeqNo;
                    questionAnswer.QuestionId = question.QuestionId;
                    questionAnswer.Answer = question.Answer;

                    await QuestionnaireAnswerRepository.DeleteQuestionByQuestionId(questionAnswer);
                    break;
                }
            }
        }
    }
}