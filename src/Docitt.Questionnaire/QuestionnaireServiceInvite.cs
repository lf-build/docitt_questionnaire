using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Security.Tokens;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Docitt.Questionnaire.Abstractions;
using Newtonsoft.Json.Linq;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Services;

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        private async Task<bool> NewSendInviteWorkflow(List<AddCoBorrowerModal> param, string temporaryApplicationNumber, ISection profileSection, List<IUserAccessedStatus> userStats, string applicantType)
        {
            Logger.Debug($"NewSendInviteWorkflow- Temporary Application {temporaryApplicationNumber}");
            
            var status = false;
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
            
            var tasks = new List<Task>();

            Logger.Debug($"Updating coborrower information");
            
            foreach(var coborrower in param)
            {
                tasks.Add(UpdatePersonalInformation(coborrower, temporaryApplicationNumber, profileSection));
            }

            await Task.WhenAll(tasks);
            
            Logger.Debug($"Completed updating coborrower information");
            
            var spouseUpdateResendInviteVisibilityTd = new List<Task<bool>>();

            // Send Email Invite & Add Dynamic sections
            foreach(var coborrower in param)
            {
                if(coborrower.Relationship == CoBorowerRelationship.Spouse.ToString())
                {
                    IAnswerUpdateRequest updateRequest = new AnswerUpdateRequest();
                    updateRequest.TemporaryApplicationNumber =  temporaryApplicationNumber;
                    updateRequest.SectionId = Convert.ToInt32(SectionDetails.Profile);
                    updateRequest.QuestionSectionName = "SpouseResendInvite";
                    spouseUpdateResendInviteVisibilityTd.Add(
                        UpdateResendInviteVisibility(updateRequest, QuestionVisiblity.Show.ToString())
                    );
                }

                Logger.Debug($"Starting SendEmailAndDynamicSection for {coborrower.EmailAddress}");
                try
                {
                    status = await SendEmailAndAddDynamicSection(coborrower, profileSectionId,profileSection, userStats, temporaryApplicationNumber, applicantType);
                }
                catch (Exception e)
                {
                    Logger.Error($"Failed SendEmailAndDynamicSection for {coborrower.EmailAddress}", e);
                    throw e;
                }
                Logger.Debug($"Completed SendEmailAndDynamicSection for {coborrower.EmailAddress}");   
            }

            await Task.WhenAll(spouseUpdateResendInviteVisibilityTd);
            
            return status;
        }

        private async Task UpdatePersonalInformation(IAddCoBorrowerModal coborrower, string temporaryApplicationNumber,ISection profileSection)
        {
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
            var subSectionId = coborrower.QuestionId.Substring(2, coborrower.QuestionId.Length-2); //Remove "Q-"
            List<IQuestion> jointCreditQuestionList = null;
            var infoSubSectionId = $"ID-{subSectionId}";
            if(coborrower.Relationship != "Spouse")
            {
                infoSubSectionId = $"{infoSubSectionId}-1";
            }
            else
            {
                var jointCreditSubsectionInfo = profileSection.SubSections.FirstOrDefault(x=>x.SubSectionId == ConstantUtils.JointCreditSubSectionId);
                if (jointCreditSubsectionInfo != null)
                    jointCreditQuestionList = jointCreditSubsectionInfo.QuestionSections.SelectMany(x => x.QuestionList)
                        .ToList();
                
                infoSubSectionId = coborrower.QuestionId;
            }

            var infoSubSection = profileSection.SubSections
                .FirstOrDefault(x => x.SubSectionId.Equals(infoSubSectionId));

            var questionList = infoSubSection?.QuestionSections.SelectMany(x => x.QuestionList).ToList();

            if(questionList != null)
            {
                //FirstName
                var questionFirstName = questionList.FirstOrDefault(x => x.QuestionFieldName == "BorrowerFirstName");
                if (questionFirstName != null) questionFirstName.Answer = coborrower.FirstName;

                //LastName
                var questionLastName = questionList.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerLastName");
                if (questionLastName != null) questionLastName.Answer = coborrower.LastName;

                if(coborrower.Relationship == "Spouse")
                {
                    if(jointCreditQuestionList != null)
                    {
                        var middleNameQuestion = jointCreditQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseMiddleNameMarried");
                        var suffixQuestion  = jointCreditQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseSuffixMarried");
                        var birthDateQuestion = jointCreditQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseBirthDate");
                        var ssnQuestion = jointCreditQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseSSN");

                        //MiddleName
                        var questionMiddleName = questionList.FirstOrDefault(x => x.QuestionFieldName == "BorrowerMiddleName");
                        if (questionMiddleName != null)
                            questionMiddleName.Answer = middleNameQuestion?.Answer;

                        //Suffix
                        var questionSuffix = questionList.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerSuffix");
                        if (questionSuffix != null)
                            questionSuffix.Answer = suffixQuestion?.Answer;

                        //BirthDate
                        var questionBirthDate = questionList.FirstOrDefault(x => x.QuestionFieldName == "BorrowerBirthDate");
                        if (questionBirthDate != null)
                            questionBirthDate.Answer = birthDateQuestion?.Answer;

                        //SSN
                        var questionSsn = questionList.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerSSN");
                        if (questionSsn != null) questionSsn.Answer = ssnQuestion?.Answer;
                    }
                }

                IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                answerRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
                answerRequest.SectionId = profileSectionId;
                answerRequest.SubSectionId = infoSubSectionId;
                //TODO: By Nayan We should be calling Questionnaire Answer Repository directly instead of replacing full object
                await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest,  infoSubSection);

            }

            string communicationSubSectionId;
            if(string.Equals(coborrower.Relationship, "Spouse", StringComparison.OrdinalIgnoreCase))
            {
                  communicationSubSectionId = coborrower.QuestionId + "-1";
            }
            else
            {
                communicationSubSectionId =  "ID-" + subSectionId + "-2";
            }
                   
            var communicationSubSection = profileSection.SubSections.FirstOrDefault(x => x.SubSectionId.Equals(communicationSubSectionId));

            var questionCommunication = communicationSubSection?.QuestionSections.SelectMany(x => x.QuestionList).ToList();

            if(questionCommunication != null)
            {
                //Email
                var questionEmail = questionCommunication.FirstOrDefault(x => x.QuestionFieldName == "BorrowerPreferredEmail");
                if (questionEmail != null) questionEmail.Answer = coborrower.EmailAddress;

                if(string.Equals(coborrower.Relationship, "Spouse", StringComparison.OrdinalIgnoreCase))
                {
                    if(jointCreditQuestionList != null)
                    {
                        var phoneQuestion = jointCreditQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseCellPhone");

                        //Phone
                        var questionPhone = questionCommunication.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerCellPhone");
                        if (questionPhone != null)
                            questionPhone.Answer = phoneQuestion?.Answer;
                    }
                }

                IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                answerRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
                answerRequest.SectionId = profileSectionId;
                answerRequest.SubSectionId = communicationSubSectionId;

                //TODO: By Nayan We should be calling Questionnaire Answer Repository directly instead of replacing full object
                await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest,  communicationSubSection);

            }

        }

        private async Task<bool> SendEmailAndAddDynamicSection(AddCoBorrowerModal coborrower, int sectionId, ISection profileSection, List<IUserAccessedStatus> userStats, string temporaryApplicationNumber, string applicantType)
        {
            var status = false;
            var subSectionId = string.Empty;

             Logger.Info($"Inside SendEmailAndAddDynamicSection for {coborrower.EmailAddress}");
            if(coborrower.Relationship == ApplicantType.Spouse.ToString())
            {
                subSectionId = coborrower.QuestionId;

                IAnswerUpdateRequest updateRequestReadOnly = new AnswerUpdateRequest();
                updateRequestReadOnly.TemporaryApplicationNumber =  temporaryApplicationNumber;
                updateRequestReadOnly.SectionId = Convert.ToInt32(SectionDetails.Profile);
                updateRequestReadOnly.SubSectionId = ConstantUtils.JointCreditSubSectionId;
                updateRequestReadOnly.QuestionFieldName = ConstantUtils.SpousePreferredEmail;
                await UpdateQuestionReadOnly(updateRequestReadOnly, true);
            }
            else
            {
                var guidId = coborrower.QuestionId.Substring(2,coborrower.QuestionId.Length-2);
                subSectionId = "ID-" + guidId + "-1";
            }
            
            var subSections = GetUserInfoAndContactInfoSubsections(profileSection, subSectionId);
            Logger.Info($"Started ExecuteSendInvitationEventNew for {coborrower.EmailAddress}");
            var strInvitationId = await this.ExecuteSendInvitationEventNew(temporaryApplicationNumber, sectionId, subSections);
                        
            // if (status == true)
            if (!string.IsNullOrEmpty(strInvitationId))
            {
                 Logger.Info($"Success ExecuteSendInvitationEventNew & invitationID : {strInvitationId}" );
                //If the invitation has been sent to all the applicant then create SubSections for the user belonging to 'SubSectionId'
                //status = await AddSubSectionForTheApplicant(questionnaireNew.TemporaryApplicationNumber, sectionId, subSectionId, subSections);
                Logger.Debug($"Starting AssignUserNameToApplicantPersonalInfoSectionNew");
                await UpdateQuestionIdForInviteQuestion(temporaryApplicationNumber,profileSection, coborrower, strInvitationId);
                await UpdateUserNameForInfoSections(temporaryApplicationNumber, coborrower.QuestionId, strInvitationId, coborrower.Relationship.ToString());
                await AssignUserNameToApplicantPersonalInfoSectionNew(temporaryApplicationNumber, coborrower, strInvitationId);
                Logger.Debug($"Completed  AssignUserNameToApplicantPersonalInfoSectionNew");
                Logger.Debug($"Starting   AddSubSectionForTheApplicantNew");
                status = await AddSubSectionForTheApplicantNew(temporaryApplicationNumber,userStats, sectionId, subSections, strInvitationId, applicantType);
                 Logger.Debug($"Completed   AddSubSectionForTheApplicantNew");
            }

            Logger.Info($"ExecuteSendInvitationEventNew : Completed with Unsuccessful status with invitationID is null or empty for {coborrower.EmailAddress} & Status : {status}" );
            return status;
        }

        /// <summary>
        /// This function sends the invitation email to the applicant.
        /// It fetches the email address from the sub section using the subSectionId
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        private async Task<string> ExecuteSendInvitationEventNew(string applicationNumber, int sectionId, List<ISubSection> subSections)
        {
             Logger.Info($"Inside ExecuteSendInvitationEventNew for {applicationNumber}");
            var strInvitationId = string.Empty;
            
            var borrowerInfo = await GetBorrowerInfoFromTheMapping(applicationNumber,sectionId, subSections);
            
            if(borrowerInfo == null)
            {
              Logger.Info($"ExecuteSendInvitationEventNew : Borrower(spouse) information is empty  so returing");
                return string.Empty;            
            }
            
            if (string.IsNullOrEmpty(borrowerInfo.FirstName) == true || string.IsNullOrEmpty(borrowerInfo.LastName) == true)
            {
                Logger.Warn($"First Name or Last Name is not provided to update the UserProfile for the user {borrowerInfo.Email}");
            }
           
            var updateStatus = await UpdateUserProfile(applicationNumber, borrowerInfo.Email, borrowerInfo);
            Logger.Info($"ExecuteSendInvitationEventNew : UpdateUserProfile completed");
            var jResult = await this.TriggerRuleToSendEmail(applicationNumber, borrowerInfo);
            if (jResult == null)
            {
                Logger.Info($"jResult : null.");
            }

            if (jResult != null)
            {
                var statusResult = jResult.GetValue("status").ToString();
                if (statusResult == "success")
                {
                    var data = jResult.GetValue("data");
                    if (data != null && data.Children().Count() > 0)
                    {
                        strInvitationId = data.Value<string>("id").ToString();
                    }

                    Logger.Info($"TriggerRuleToSendEmail : Success InvitationId,{strInvitationId}.");
                }
                else
                {
                    if (jResult.GetValue("error").ToString().Contains("User already invited."))
                    {
                        //Even if it is an error, if the invitation has already been sent then Do Not consider it as an error.
                         Logger.Info($"TriggerRuleToSendEmail : Inviration response : User already invited");
                    }
                    else
                    {
                        Logger.Info($"TriggerRuleToSendEmail : Invitation not sent.");
                        throw new Exception("Error occured in Send invitationEvent");
                    }
                }
            }                    

            return strInvitationId;
        }

        private async Task<Newtonsoft.Json.Linq.JObject> TriggerRuleToSendEmail(string applicationNumber, IBorrowerCoBorrowerInfo borrowerInfo, bool allowEmail = true)
        {

             Logger.Info($"TriggerRuleToSendEmail : Started");
            var token = TokenParser.Parse(TokenReader.Read());
            var reader = new StaticTokenReader(token.Value);
            var decisionEngine = DecisionEngineFactory.Create(reader);
            var userName = await GetCurrentUser();

            IInviteRequestPayload invitePayload = new InviteRequestPayload();
            invitePayload.InvitedBy = userName;
            invitePayload.InviteEmail = borrowerInfo.Email;
            invitePayload.InviteFirstName = borrowerInfo.FirstName;
            invitePayload.InviteLastName = borrowerInfo.LastName;
            invitePayload.InviteMobileNumber = borrowerInfo.Phone;
            var rolename = "";
            /*if (borrowerInfo.ApplicantType.ToString().ToLower() != "borrower")
            {
               rolename = "Co-Borrower";
           }*/
          
            if (borrowerInfo.ApplicantType.ToString().ToLower() == "coborrower")
            {
                rolename = "Co-Borrower";
            }
            else{
                rolename =  borrowerInfo.ApplicantType.ToString();
            }

           invitePayload.Role = rolename;
           invitePayload.Team = "contributor";
           IInviteRequest inviteRequest = new InviteRequest();
           inviteRequest.entityType = "application";
            inviteRequest.entityId = applicationNumber;
            inviteRequest.payload = invitePayload;
            inviteRequest.allowEmail = allowEmail;
            //TODO: Update this logic to use invite service directly instead of using through decision engine
            var jResult = await Task.Run(() => decisionEngine.Execute<dynamic, Newtonsoft.Json.Linq.JObject>("dtmSendInvite", inviteRequest));
              Logger.Info($"TriggerRuleToSendEmail : Ended ");
            return jResult;
        }

        // Allocate invite id to AddCoborrower question section to handle delete as userid is not available before signup.
        private async Task<bool> UpdateQuestionIdForInviteQuestion(string temporaryApplicationNumber,ISection profileSection, AddCoBorrowerModal borrower, string invitationId)
        {
            if(borrower.Relationship == ApplicantType.Spouse.ToString()){
                return true;
            }
            
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);

            // Get question section by questionid
            var questionSections = profileSection.SubSections
                            .FirstOrDefault(ss => string.Equals(ss.TemplateFieldName, "coBorrowerInvitationInfo",
                                StringComparison.OrdinalIgnoreCase))
                            ?.QuestionSections;

            var coBorrowerInvitationInfoSubSectionId = profileSection
                                .SubSections.Where(ss => ss.TemplateFieldName == "coBorrowerInvitationInfo")
                                .Select(ss => ss.SubSectionId).FirstOrDefault();

            if (questionSections != null)
            {
                foreach (var questionSection in questionSections)
                {
                    var questionToUpdate =
                        questionSection.QuestionList.FirstOrDefault(q => q.QuestionId == borrower.QuestionId);
                    if (questionToUpdate != null)
                    {
                        IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                        answerRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
                        answerRequest.SectionId = profileSectionId;
                        answerRequest.SubSectionId = coBorrowerInvitationInfoSubSectionId;
                        answerRequest.QuestionSectionSeqNo = questionSection.SeqNo;
                        answerRequest.QuestionId = borrower.QuestionId;
                        answerRequest.Answer = invitationId;

                        await QuestionSectionsRepository.UpdateInviteQuestionSection(answerRequest);
                    }
                }
            }
                
            return true;
        }

        private async Task UpdateUserNameForInfoSections(string temporaryApplicationNumber, string questionId, string invitationId, string applicantType = null)
        {
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
            string subSectionId;
            string coBorrowerMaritalStatusSubSectionId;
            string coBorrowerInfoSubSectionId;
            var spouseReoInfoSubSectionId = $"{questionId}-2";

            if(string.Equals(ApplicantType.Spouse.ToString(), applicantType, StringComparison.OrdinalIgnoreCase))
            {
                subSectionId = questionId;
                coBorrowerMaritalStatusSubSectionId = subSectionId;
                coBorrowerInfoSubSectionId = questionId + "-1";
            }
            else
            {
                subSectionId = questionId.Substring(2, questionId.Length-2);
                coBorrowerMaritalStatusSubSectionId = "ID-" + subSectionId;
                coBorrowerInfoSubSectionId = "ID-" + subSectionId + "-1";    
            }
            
            var coBorrowerCommunicationSubSectionId = $"ID-{subSectionId}-2";
            var coBorrowerREOInfoSubSectionId = "ID-" + subSectionId + "-3";

            var t1 = QuestionSubSectionsRepository.UpdateUserName(temporaryApplicationNumber, profileSectionId,
                coBorrowerMaritalStatusSubSectionId, invitationId);
            var t2=QuestionSubSectionsRepository.UpdateUserName(temporaryApplicationNumber, profileSectionId,
                coBorrowerInfoSubSectionId, invitationId);
            var t3 = QuestionSubSectionsRepository.UpdateUserName(temporaryApplicationNumber, profileSectionId,
                spouseReoInfoSubSectionId, invitationId);
            var t4=QuestionSubSectionsRepository.UpdateUserName(temporaryApplicationNumber, profileSectionId,
                coBorrowerCommunicationSubSectionId, invitationId);
            var t5=QuestionSubSectionsRepository.UpdateUserName(temporaryApplicationNumber, profileSectionId,
                coBorrowerREOInfoSubSectionId, invitationId);

            await Task.WhenAll(t1, t2, t3, t4, t5);

        }

        private async Task AssignUserNameToApplicantPersonalInfoSectionNew(string temporaryApplicationNumber, AddCoBorrowerModal coborrower, string invitationId)
        {
            var currentUserName = await GetCurrentUser();

            var questionnaireWithoutDetail =
                await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
            
            if (!questionnaireWithoutDetail.UserState.Any(u => u.UserName == invitationId) && !string.IsNullOrEmpty(coborrower.EmailAddress))
            {
                IUserAccessedStatus applicantUserState = new UserAccessedStatus();
                applicantUserState.UserName = invitationId; // This will be replaced after invitee signsup
                applicantUserState.InviteId = invitationId; // This is added to know the invite id
                applicantUserState.UserEmailId = coborrower.EmailAddress; // This is added to keep track of UserEmailId
                if(string.Equals(coborrower.Relationship, "Spouse", StringComparison.OrdinalIgnoreCase))
                {
                    applicantUserState.SpouseUserName = currentUserName;
                    applicantUserState.ApplicantType = ApplicantType.Spouse.ToString();
                }
                else
                {
                    applicantUserState.ApplicantType = ApplicantType.CoBorrower.ToString();
                }        
                applicantUserState.LastAccessedSection = 1;
                applicantUserState.LastAccessedSectionSeqNo = 1;
                applicantUserState.LastAccessedSubSectionSeqNo = 1;
                applicantUserState.HighestSectionReached = 1;
                applicantUserState.Status = FormStatus.Open;
                questionnaireWithoutDetail.UserState.Add(applicantUserState);
                await QuestionnaireRepository.PushUserState(temporaryApplicationNumber, applicantUserState);
            }
            if(string.Equals(coborrower.Relationship, "Spouse", StringComparison.OrdinalIgnoreCase))
            {
                var userState = questionnaireWithoutDetail.UserState.FirstOrDefault(u => u.UserName == currentUserName);
                if (userState != null)
                {
                    userState.SpouseUserName = invitationId;
                    await QuestionnaireRepository.UpdateUserStateWithSpouseUsername(temporaryApplicationNumber, currentUserName, invitationId);
                }
            }            
        }

        private async Task<bool> AddSubSectionForTheApplicantNew(string temporaryApplicationNumber, List<IUserAccessedStatus> userStats,
                                                                    int sectionId, 
                                                                    List<ISubSection> subSections,
                                                                    string invitationId,
                                                                    string applicantType)
        {
            //List<int> destSectionIds = new List<int>();
            //List<string> destSubSectionIds = new List<string>();
            Logger.Debug($"Getting  GetBorrowerInfoFromTheMapping");

            var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,sectionId, subSections);
            // subSections = GetUserInfoAndContactInfoSubsections(questionnaire, sectionId, subSectionId);
             Logger.Debug($"Completed  GetBorrowerInfoFromTheMapping");
            var status = false;

            if (borrowerInfo != null)
            {
                 Logger.Debug($"Getting  borrowerInfo.UserName :- " + borrowerInfo.UserName);

                 if (borrowerInfo.UserName == SPOUSEUSERBASED || borrowerInfo.UserName == borrowerInfo.Email 
                    || borrowerInfo.UserName == invitationId)
                {
                    var userName = await GetCurrentUser();
                    // var inviteEmail = borrowerInfo.Email;

                    var appFieldMap = QuestionnaireConfigurations.ApplicantDynamicSections;
                    

                    var seqNumberRange = appFieldMap.SeqNumberRange.GetValueOrDefault();

                    foreach (IApplicantSubSectionFieldMap item in appFieldMap.Map)
                    {
                        Logger.Debug($"Adding dynamic template id : {item.TemplateId}");

                        //Discard
                        if (item.ExcludeUserType != null && item.ExcludeUserType.Contains(Extensions.GetDescription(borrowerInfo.ApplicantType)))
                        {
                            Logger.Debug($"Discarding as Extensions.GetDescription(borrowerInfo.ApplicantType) is in exclude list");   
                            continue;
                        }

                        var sectionToBeUpdated =
                            await QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, item.SectionId);
                        
                        if(sectionToBeUpdated == null)
                            continue;

                        var newSubSectionId = await GenerateSubSectionIdNew(invitationId, item.TemplateId);
                                                
                        //Get the subSection template to create a new subSection
                        ISubSection subSectionFromTemplate = Extensions.Clone<SubSection>(sectionToBeUpdated.Templates[item.TemplateId]);
                        if (subSectionFromTemplate == null)
                        {
                            throw new Exception($"Template {item.TemplateId} does not exist.");
                        }

                        // Validate SubSection Id need to be unique
                        if (sectionToBeUpdated.SubSections.FindAll(ss => ss.SubSectionId == newSubSectionId).Count > 0)
                        {
                            //throw new Exception($"Subsection having the Section Id '{newSubSectionId}' already exists in Section {item.SectionId}. SubSectionId has to be unique.");
                            Logger.Warn($"Subsection having the Section Id '{newSubSectionId}' already exists in Section {item.SectionId}. SubSectionId has to be unique.");
                            continue;
                        }

                        // foreach (var subSection in subSections)
                        // {
                            // if (item.TemplateId.Equals("coborrowerResidenceDetail", StringComparison.InvariantCultureIgnoreCase) && !subSection.TemplateId.Equals("coBorrowerInfo", StringComparison.InvariantCultureIgnoreCase))
                            // {
                            //     continue;
                            // }

                            //Get new seq no
                            var subSectionCount = sectionToBeUpdated.SubSections.Count; //.FindAll(ss => ss.SubSectionName == subSection.SubSectionName).Count;
                            var newSeqNo = 1000;

                            if (item.SeqNumberRange.HasValue)
                            {
                                // int userCount = sectionToBeUpdated.SubSections.Select(ss => ss.UserName).Distinct().Count();
                                var userCount = userStats.Count();
                                
                                if(item.TemplateId == "coBorrowerIncome")
                                {
                                    newSeqNo = (userCount * item.SeqNumberRange.Value) + 1000;
                                    if(applicantType.Equals(ApplicantType.CoBorrower.ToString()))
                                    {
                                        // When adding dynamic subsections in income, we need to identify in FE 
                                        // whether the request is for Spouse or CoBorrower Section.
                                        // TemplateId value null or string.Empty indicates Borrower/CoBorrower
                                        subSectionFromTemplate.TemplateId = string.Empty;
                                    }
                                }else
                                    newSeqNo = (userCount * item.SeqNumberRange.Value) + subSectionFromTemplate.SeqNo.GetValueOrDefault();
                            }
                            else
                            {
                                newSeqNo = seqNumberRange * (subSectionCount + 1);
                            }

                            subSectionFromTemplate.UserName = invitationId;
                            subSectionFromTemplate.SubSectionId = newSubSectionId;
                            subSectionFromTemplate.SeqNo = newSeqNo;

                            //Assign newly generated SubSectionId to the child question info in the ChilQuestions array
                            subSectionFromTemplate = await UpdateSubSectionId(subSectionFromTemplate, subSectionFromTemplate.SubSectionId);

                            var addSubSectionRequest = new AddSubSectionRequest();
                            addSubSectionRequest.SubSection = (SubSection)subSectionFromTemplate;
                            addSubSectionRequest.SectionId = item.SectionId;
                            addSubSectionRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
                            Logger.Debug($"Calling AddSubSectionNew for Section {item.SectionId} SubSectionId {subSectionFromTemplate.SubSectionId}");
                             await AddSubSectionNew(temporaryApplicationNumber,sectionToBeUpdated, addSubSectionRequest);
                            Logger.Debug($"Completed AddSubSectionNew");
                        // }                        
                    }
                }
                status = true;
            }
            else{
                 Logger.Debug($"BorrowefInfo is null :  AddSubSectionForTheApplicantNew");
            }
            
            return status;
        }        

        private async Task<string> GenerateSubSectionIdNew(string userName, string templateName)
        {
            return await Task.Run(() => templateName + "_" + userName.Replace('@', '_').Replace('#', '_').Replace('%', '_').Replace('.', '_'));
        }        

        private async Task<bool> AddSubSectionNew(IQuestionnaire questionnaire,IAddSubSectionRequest subSectionRequest) 
        {
            Logger.Debug($"Inside AddSubSectionNew");

            try
            {
                if (string.IsNullOrEmpty(subSectionRequest.TemporaryApplicationNumber))
                {
                    subSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                    if (subSectionRequest.TemporaryApplicationNumber == null)
                    {
                        throw new QuestionnaireNotFoundException("No Questionnaire exists for the user to add a new Sub Section.");
                    }
                }
                var status = false;
                if (questionnaire != null)
                {
                    Logger.Debug($"subSectionRequest.TemporaryApplicationNumber {subSectionRequest.TemporaryApplicationNumber} Section {subSectionRequest.SectionId}");
                    var temporaryApplicationNumber = subSectionRequest.TemporaryApplicationNumber;
                    ISubSection subSection = subSectionRequest.SubSection;
                    var sectionId = subSectionRequest.SectionId;

                    ISection sectiontoUpdate = null;

                    var sections = questionnaire.ApplicationForm.Sections;
                    sectiontoUpdate = sections.FirstOrDefault(s => s.SectionId == sectionId);

                    if (sectiontoUpdate == null)
                    {
                        throw new SectionDoesNotExistsException($"Section '{subSectionRequest.SectionId}' does not exist.");
                    }
         
                    // Update subsection id in child question
                    foreach (var queSection in subSection.QuestionSections)
                    {
                        foreach (var question in queSection.QuestionList)
                        {
                            if (question.ChildQuestions != null)
                            {
                                foreach (var child in question.ChildQuestions)
                                {
                                    if(string.IsNullOrEmpty(child.SubSectionId))
                                        child.SubSectionId = subSection.SubSectionId;
                                }
                            }
                        }
                    }

                    Logger.Debug($"Get the section to add the new subsection {subSectionRequest.SubSection.SubSectionId}");
                    if (!sectiontoUpdate.SubSections.Any(ss => ss.SubSectionId.Equals(subSectionRequest.SubSection.SubSectionId)))
                    {
                        //status = await QuestionnaireRepository.AddSubSection(subSectionRequest.TemporaryApplicationNumber, Sections, subSectionRequest.SubSection.SubSectionId);
                        var newSubSectionList = new List<ISubSection>();
                        newSubSectionList.Add(subSectionRequest.SubSection);
                        sectiontoUpdate.SubSections.Add(subSectionRequest.SubSection);
                        Logger.Debug($"Adding SubSection {temporaryApplicationNumber} {sectionId} {subSectionRequest.SubSection.SubSectionId}");
                        await QuestionSubSectionsRepository.AddSubSections(temporaryApplicationNumber, sectionId, newSubSectionList);
                        status = true;
                        Logger.Debug("Adding Done");
                    }
                    else
                    {
                        Logger.Debug($"Already exists in cached questionniare {subSectionRequest.SubSection.SubSectionId}");
                        //Take fresh pull and check again
                        var questionnaireRefresh = await GetQuestionnaire(temporaryApplicationNumber);
                        if (!questionnaireRefresh.ApplicationForm.Sections.FirstOrDefault(Section => Section.SectionId == sectionId).SubSections.Any(ss => ss.SubSectionId.Equals(subSectionRequest.SubSection.SubSectionId)))
                        {
                             Logger.Debug($"Refereshed questionniare was different so adding {subSectionRequest.SubSection.SubSectionId}");
                            var newSubSectionList = new List<ISubSection>();
                            newSubSectionList.Add(subSectionRequest.SubSection);
                            sectiontoUpdate.SubSections.Add(subSectionRequest.SubSection);
                            Logger.Debug($"Adding SubSection {temporaryApplicationNumber} {sectionId} {subSectionRequest.SubSection.SubSectionId}");
                            await QuestionSubSectionsRepository.AddSubSections(temporaryApplicationNumber, sectionId, newSubSectionList);
                            status = true;
                            Logger.Debug("Adding Done");
                        }
                    }
                }

                return await Task.Run(() => status);
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                throw new SubSectionDoesNotExistsException(ex.Message);
            }

        }

        private async Task<bool> AddSubSectionNew(string applicationNumber, ISection sectionUpdate,IAddSubSectionRequest subSectionRequest) 
        {
            Logger.Debug($"Inside AddSubSectionNew");
            if (sectionUpdate == null)
            {
                throw new SectionDoesNotExistsException($"Section '{subSectionRequest.SectionId}' does not exist.");
            }

            try
            {
                if (string.IsNullOrEmpty(subSectionRequest.TemporaryApplicationNumber))
                {
                    subSectionRequest.TemporaryApplicationNumber = applicationNumber;
                    if (subSectionRequest.TemporaryApplicationNumber == null)
                    {
                        throw new QuestionnaireNotFoundException("No Questionnaire exists for the user to add a new Sub Section.");
                    }
                }
                var status = false;
                Logger.Debug(
                    $"subSectionRequest.TemporaryApplicationNumber {subSectionRequest.TemporaryApplicationNumber} Section {subSectionRequest.SectionId}");
                var temporaryApplicationNumber = subSectionRequest.TemporaryApplicationNumber;
                ISubSection subSection = subSectionRequest.SubSection;
                var sectionId = subSectionRequest.SectionId;



                // Update subsection id in child question
                foreach (var queSection in subSection.QuestionSections)
                {
                    foreach (var question in queSection.QuestionList)
                    {
                        if (question.ChildQuestions != null)
                        {
                            foreach (var child in question.ChildQuestions)
                            {
                                if (string.IsNullOrEmpty(child.SubSectionId))
                                    child.SubSectionId = subSection.SubSectionId;
                            }
                        }
                    }
                }

                Logger.Debug($"Get the section to add the new subsection {subSectionRequest.SubSection.SubSectionId}");
                if (!sectionUpdate.SubSections.Any(ss =>
                    ss.SubSectionId.Equals(subSectionRequest.SubSection.SubSectionId)))
                {
                    //status = await QuestionnaireRepository.AddSubSection(subSectionRequest.TemporaryApplicationNumber, Sections, subSectionRequest.SubSection.SubSectionId);
                    var newSubSectionList = new List<ISubSection>();
                    newSubSectionList.Add(subSectionRequest.SubSection);
                    sectionUpdate.SubSections.Add(subSectionRequest.SubSection);
                    Logger.Debug(
                        $"Adding SubSection {temporaryApplicationNumber} {sectionId} {subSectionRequest.SubSection.SubSectionId}");
                    await QuestionSubSectionsRepository.AddSubSections(temporaryApplicationNumber, sectionId,
                        newSubSectionList);
                    status = true;
                    Logger.Debug("Adding Done");
                }
                else
                {
                    Logger.Debug($"Already exists in cached questionniare {subSectionRequest.SubSection.SubSectionId}");
                    //Take fresh pull and check again
                    var questionnaireRefresh = await GetQuestionnaire(temporaryApplicationNumber);
                    if (!questionnaireRefresh.ApplicationForm.Sections
                        .FirstOrDefault(s => s.SectionId == sectionId).SubSections.Any(ss =>
                            ss.SubSectionId.Equals(subSectionRequest.SubSection.SubSectionId)))
                    {
                        Logger.Debug(
                            $"Refereshed questionniare was different so adding {subSectionRequest.SubSection.SubSectionId}");
                        var newSubSectionList = new List<ISubSection>();
                        newSubSectionList.Add(subSectionRequest.SubSection);
                        sectionUpdate.SubSections.Add(subSectionRequest.SubSection);
                        Logger.Debug(
                            $"Adding SubSection {temporaryApplicationNumber} {sectionId} {subSectionRequest.SubSection.SubSectionId}");
                        await QuestionSubSectionsRepository.AddSubSections(temporaryApplicationNumber, sectionId,
                            newSubSectionList);
                        status = true;
                        Logger.Debug("Adding Done");
                    }
                }

                return await Task.Run(() => status);
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                throw new SubSectionDoesNotExistsException(ex.Message);
            }
            
        }

        
        public async Task<bool> EventReminderInvite(ReminderInviteRequest reminderInviteRequest, string temporaryApplicationNumber, 
            int sectionId, string subSectionId, int questionSectionSeqNo, string questionId, string questionType, string eventName)
        {
            var isInviteTriggered = false;
            var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
            var userName = await GetCurrentUser();

            switch(eventName)
            {
                case "spouse-reinvite" :
                    var spouseUserName = GetSpouseUserNameGivenInviteeUserName(questionnaire.UserState, userName);
                    if(string.IsNullOrEmpty(spouseUserName))
                    {
                        return false;
                    }

                    var spouseInviteId = GetInviteIdGivenUserName(questionnaire.UserState, spouseUserName);
                    if(string.IsNullOrEmpty(spouseInviteId))
                    {
                        return false;
                    }

                    isInviteTriggered = await SendInviteReminderGivenInviteId(spouseInviteId);
                    break;
                case "nonspouse-reinvite":
                    if(reminderInviteRequest == null)
                    {
                        throw new ArgumentException("Payload is missing");
                    }

                    if(string.IsNullOrEmpty(reminderInviteRequest.Email))
                    {
                         throw new ArgumentException("Payload is missing EmailId");
                    }

                    var coBorrowerInviteId = GetInviteIdGivenEmailId(questionnaire.UserState, reminderInviteRequest.Email);
                    if(string.IsNullOrEmpty(coBorrowerInviteId))
                    {
                        return false;
                    }

                    isInviteTriggered = await SendInviteReminderGivenInviteId(coBorrowerInviteId);
                    break;
                case "coborrower-spouse-reinvite":
                    var cospouseUserName = GetSpouseUserNameGivenInviteeUserName(questionnaire.UserState, userName);
                    if(string.IsNullOrEmpty(cospouseUserName))
                    {
                        return false;
                    }

                    var cospouseInviteId = GetInviteIdGivenUserName(questionnaire.UserState, cospouseUserName);
                    if(string.IsNullOrEmpty(cospouseInviteId))
                    {
                        return false;
                    }

                    isInviteTriggered = await SendInviteReminderGivenInviteId(cospouseInviteId);
                    break;
            }

            return isInviteTriggered;
        }

         public async Task<bool> SendInviteToCoBorrower(SendInviteRequest InviteRequest, string temporaryApplicationNumber, 
            int sectionId, string subSectionId, int questionSectionSeqNo, string questionId)
        {
                Logger.Debug($"Started SendInviteToCoBorrower- Temporary Application {temporaryApplicationNumber}");

                ValidateInviteRequest(InviteRequest);
            
                var addCoBorrowerModalList = new  List<AddCoBorrowerModal>();
                ISection profileSection = null;

                var userName = await GetCurrentUser();
                var status = true;
                var questionnaireWithoutDetail = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
                if (questionnaireWithoutDetail == null)
                    throw new NotFoundException($"Questionnaire not found for application number{temporaryApplicationNumber}");

                var objAddCoborrowerModel = new AddCoBorrowerModal();

                objAddCoborrowerModel.FirstName = InviteRequest.FirstName;
                objAddCoborrowerModel.LastName = InviteRequest.LastName;
                objAddCoborrowerModel.Relationship = InviteRequest.Relationship;
                objAddCoborrowerModel.EmailAddress = InviteRequest.Email.ToLower();
                objAddCoborrowerModel.QuestionId = questionId;
                addCoBorrowerModalList.Add(objAddCoborrowerModel);

                       if(addCoBorrowerModalList.Count == 0)
                        {
                           status = false;
                        }
                        else
                        {
                            foreach (var addCoBorrowerModal in addCoBorrowerModalList)
                            {
                                var guidId = addCoBorrowerModal.QuestionId.Substring(2,addCoBorrowerModal.QuestionId.Length-2);
                                var subSectionIdNew = $"ID-{guidId}-1";
                                FaultRetry.RunWithAlwaysRetry(() =>
                                {
                                    try
                                    {
                                        GetUserInfoAndContactInfoSubsections(profileSection, subSectionIdNew);
                                    }
                                    catch (Exception )
                                    {
                                        profileSection = QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, sectionId).Result;
                                        throw;
                                    }
                                }, maxRetry: 5, sleepSeconds: 1);
                                
                            }
                             Logger.Debug($"SendInviteToCoBorrower- Inside : NewSendInviteWorkflow.");
                            return await NewSendInviteWorkflow(addCoBorrowerModalList, temporaryApplicationNumber,
                                profileSection, questionnaireWithoutDetail.UserState, ApplicantType.CoBorrower.ToString());
                        }

                return status;
        }

        private void ValidateInviteRequest(SendInviteRequest InviteRequest)
        {
            if(InviteRequest == null)
                 throw new ArgumentNullException($"Invite request data not found.");
            
            if((InviteRequest.FirstName == null) || (string.IsNullOrEmpty(InviteRequest.FirstName.Trim())))
            {              
                throw new ArgumentException("firstname is required.");
            }

            if((InviteRequest.LastName == null) || (string.IsNullOrEmpty(InviteRequest.LastName.Trim())))
            {              
                throw new ArgumentException("lastname is required.");
            }

            if((InviteRequest.Relationship == null) || (string.IsNullOrEmpty(InviteRequest.Relationship.Trim())))
            {              
                throw new ArgumentException("relationship is required.");
            }

            if((InviteRequest.Email == null) || (string.IsNullOrEmpty(InviteRequest.Email.Trim())))
            {              
                throw new ArgumentException("email is required.");
            }
        }

        public async Task<IApplicantSignUpStatus> HasUserSignedUp(string temporaryApplicationNumber, string applicantEmailId)
        {
            var userName = await GetCurrentUser(); // Need to discuss if the request can be only made by applicant

            var applicantStatus = new ApplicantSignUpStatus();
            applicantStatus.ApplicationNumber = temporaryApplicationNumber;
            applicantStatus.ApplicantEmailId = applicantEmailId;
            applicantStatus.HasSignedUp = false;

            if(string.IsNullOrEmpty(temporaryApplicationNumber) || string.IsNullOrEmpty(applicantEmailId))
            {
                return applicantStatus;
            }

            var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
            if(questionnaire == null)
            {
                return applicantStatus;
            }
            
            var applicantUserState = questionnaire.UserState
                                        .FirstOrDefault(us => us.UserEmailId == applicantEmailId);

            if(applicantUserState == null)
            {
                return applicantStatus;
            }
             
            if(applicantUserState.UserName == applicantUserState.InviteId)
                applicantStatus.HasSignedUp = false;
            else    
                applicantStatus.HasSignedUp = true;
            
            return applicantStatus;
        }

        private string GetSpouseUserNameGivenInviteeUserName(IEnumerable<IUserAccessedStatus> userState, string inviteeUserName)
        {
            var user = userState.FirstOrDefault(x => x.UserName == inviteeUserName);

            if(user == null)
                return string.Empty;

            return string.IsNullOrEmpty(user.SpouseUserName) ? string.Empty : user.SpouseUserName;
        }

        private string GetInviteIdGivenEmailId(IEnumerable<IUserAccessedStatus> userState, string emailId)
        {
            var inviteId = string.Empty;
            var user = userState.FirstOrDefault(x => x.UserEmailId == emailId);

            if(user?.InviteId == null || user.UserName == null)
                return string.Empty;
            
            if(user.InviteId.Equals(user.UserName))
                inviteId = user.InviteId;

            return inviteId;
        }

        private string GetInviteIdGivenUserName(IEnumerable<IUserAccessedStatus> userState, string userName)
        {
            var inviteId = string.Empty;
            var user = userState.FirstOrDefault(x => x.UserName == userName);

            if(user?.InviteId == null || user.UserName == null)
                return string.Empty;
            
            if(user.InviteId.Equals(user.UserName))
                inviteId = user.InviteId;
            
            return inviteId;
        }

        private async Task<bool> UpdateResendInviteVisibility(IAnswerUpdateRequest updateRequest, string status)
        {
            try
            {
                return await QuestionSectionsRepository.UpdateResendInviteVisibility(updateRequest, status);
            }
            catch(Exception ex)
            {
                Logger.Error($"Error while updating ResendInviteVisibility for {updateRequest.TemporaryApplicationNumber}",ex);
            }
            return false;
        }

        private async Task<bool> UpdateQuestionReadOnly(IAnswerUpdateRequest updateRequest, bool value)
        {
            try
            {
                return await QuestionSectionsRepository.UpdateQuestionReadOnly(updateRequest, value);
            }
            catch(Exception ex)
            {
                Logger.Error($"Error while updating UpdateQuestionReadOnly for {updateRequest.TemporaryApplicationNumber}",ex);
            }
            return false;
        }

        private async Task<bool> SendInviteReminderGivenInviteId(string inviteId, string entityId = "")
        {
            try
            {
                await InviteService.SendReminder(inviteId, true, entityId);
            }
            catch (ClientException ex)
            {
                Logger.Error($"Error occured in SendInviteReminderGivenInviteId for {inviteId}", ex);
                return false;
            }
            return true;
        }

        private async Task<bool> ResetSendReminderForSpouse(string temporaryApplicationNumber, string subSectionId)
        {
            //Email should not be readonly
            IAnswerUpdateRequest updateRequestReadOnly = new AnswerUpdateRequest();
            updateRequestReadOnly.TemporaryApplicationNumber =  temporaryApplicationNumber;
            updateRequestReadOnly.SectionId = Convert.ToInt32(SectionDetails.Profile);
            updateRequestReadOnly.SubSectionId = subSectionId;
            updateRequestReadOnly.QuestionFieldName = ConstantUtils.SpousePreferredEmail;
            await UpdateQuestionReadOnly(updateRequestReadOnly, false);

            //Hide 
            IAnswerUpdateRequest updateRequest = new AnswerUpdateRequest();
            updateRequest.TemporaryApplicationNumber =  temporaryApplicationNumber;
            updateRequest.SectionId = Convert.ToInt32(SectionDetails.Profile);
            updateRequest.SubSectionId = subSectionId;
            updateRequest.QuestionSectionName = "SpouseResendInvite";
            await UpdateResendInviteVisibility(updateRequest, QuestionVisiblity.Hide.ToString());

            return true;
        }

        private async Task<bool> ResetSendReminderForCoBorrowerSpouse(string applicationNumber, string userName)
        {
            Logger.Debug($"ResetSendReminderForCoBorrowerSpouse applicationNumber {applicationNumber} username {userName}");
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
            var subSectionId = await QuestionSubSectionsRepository.GetMaritalSubSectionId(
                applicationNumber, 
                userName, 
                ConstantUtils.CoBorrowerMaritalStatusInfoTemplate, 
                profileSectionId);
            
            Logger.Debug($"ResetSendReminderForCoBorrowerSpouse SubSectionId {subSectionId}");

            if(!string.IsNullOrEmpty(subSectionId))
                return await ResetSendReminderForSpouse(applicationNumber, subSectionId);

            return false;
        }
    }
}
