using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Docitt.Questionnaire.Abstractions;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire 
{
    public partial class QuestionnaireService
    {
        private async Task<bool> InviteCoborrowerSpouse(string temporaryApplicationNumber, int sectionId, string subSectionId)
        { 
            var status = false;
            Logger.Debug($"Start: InviteCoborrowerSpouse - Temporary Application No: {temporaryApplicationNumber}");
            
            var currentUser = await GetCurrentUser();

            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            Logger.Debug($"'{temporaryApplicationNumber}' Validation step - CoBorrowerSpouse invite should alway be done by user by Coborrower ApplicantType");
            
            var loggedInUser = questionnaire.UserState.FirstOrDefault(x=>x.UserName == currentUser);
            if(loggedInUser != null && !string.Equals(loggedInUser.ApplicantType, ApplicantType.CoBorrower.ToString(),
                   StringComparison.OrdinalIgnoreCase))
            {
                Logger.Info($"Terminating InviteCoborrowerSpouse. Received request from '{loggedInUser.UserName}' with role as '{loggedInUser.ApplicantType}'");
                return false;
            }
            
            Logger.Debug($"Starting SendEmailAndAddSectionsForCoborrowerSpouse");

            var isBorrowerSpouseAlreadySigned = InviteValidationService.IsBorrowerSpouseAlreadySignedup(questionnaire.UserState, currentUser);
            if (!isBorrowerSpouseAlreadySigned)
            {
                Logger.Debug("Updating SpousePreferredEmail to readonly");
                IAnswerUpdateRequest updateRequestReadOnly = new AnswerUpdateRequest();
                updateRequestReadOnly.TemporaryApplicationNumber =  temporaryApplicationNumber;
                updateRequestReadOnly.SectionId = Convert.ToInt32(SectionDetails.Profile);
                updateRequestReadOnly.SubSectionId = subSectionId;
                updateRequestReadOnly.QuestionFieldName = ConstantUtils.SpousePreferredEmail;
                await UpdateQuestionReadOnly(updateRequestReadOnly, true);

                Logger.Debug("Updating CoBorrowerSpouseResendInvite to show");
                IAnswerUpdateRequest updateRequest = new AnswerUpdateRequest();
                updateRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
                updateRequest.SectionId = Convert.ToInt32(SectionDetails.Profile);
                updateRequest.QuestionSectionName = ConstantUtils.QuestionSectionNameCoBorrowerSpouseResendInvite;
                await UpdateResendInviteVisibility(updateRequest, QuestionVisiblity.Show.ToString());

            }
            
            status = await SendEmailAndAddSectionsForCoborrowerSpouse(questionnaire, sectionId, subSectionId);
            
            Logger.Debug($"Completed SendEmailAndAddSectionsForCoborrowerSpouse");

            Logger.Debug($"End: CoborrowerSpouseInvitationWorkflow - Temporary Application {temporaryApplicationNumber}");
            
            return status;
        }

        private async Task<string> GetQuestionAnswerFromSubSection(ISubSection subSection, string questionFieldName)
        {
            if(subSection == null)
                return string.Empty;

            var questions = subSection.QuestionSections.SelectMany(x=>x.QuestionList);

            if(questions == null)
                return string.Empty;
            
            var question = questions.FirstOrDefault(x=>x.QuestionFieldName==questionFieldName);
            
            if(question == null)
                return string.Empty;
        
            return await Task.Run(()=>question.Answer); 
        }

        private bool UpdateQuestionAnswerInSubSection(List<ISubSection> subSections, string questionFieldName,
            string answer)
        {
            var returnVal = false;
            foreach (var subSection in subSections)
            {
                var questions = subSection?.QuestionSections.SelectMany(x => x.QuestionList);

                var question = questions?.FirstOrDefault(x => x.QuestionFieldName == questionFieldName);

                if (question == null)
                    continue;

                question.Answer = answer;
                returnVal = true;
                //TODO Update QuestionAnswer Database
            }

            return returnVal;
        }

        private async Task<bool> SendEmailAndAddSectionsForCoborrowerSpouse(IQuestionnaire questionnaire, int sectionId, string subSectionId)
        {
            var status = false;
            var currentUserName = GetCurrentUser().Result;
            var coborrowerSpouseTemplates = new[] {"coBorrowerSpouseInfo", "coBorrowerSpouseCommunicationInfo", "coboSpouseREOInfo" };

            var subSections = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == sectionId)
                .SelectMany(s => s.SubSections)
                .Where(ss => coborrowerSpouseTemplates.Contains(ss.TemplateId)
                             && ss.UserName == currentUserName).ToList();

            if (subSections.Count == 0)
            {
                Logger.Debug($"Invitation already sent, no further operation required.");
                try
                {
                    status = await RegenerateInvite(questionnaire.TemporaryApplicationNumber,questionnaire.UserState, questionnaire.ApplicationForm.Sections.FirstOrDefault(s=>s.SectionId==sectionId) , subSectionId);
                }
                catch (Exception ex)
                {
                    Logger.Warn(
                        $"Error occurred while reinviting coborrower spouse {currentUserName} subsection {subSectionId}",
                        ex);
                }

                return status;
            }

            var subSectionCoborrowerSpouseInvite = questionnaire.ApplicationForm
                .Sections.Where(s => s.SectionId == sectionId)
                .SelectMany(s => s.SubSections)
                .FirstOrDefault(x => x.SubSectionId == subSectionId);

            // check for BorrowerAddSpouseAsCoBorrower
            var answerBorrowerAddSpouseAsCoBorrower =
                await GetQuestionAnswerFromSubSection(subSectionCoborrowerSpouseInvite,
                    "BorrowerAddSpouseAsCoBorrower");
            if (!answerBorrowerAddSpouseAsCoBorrower.ToLower().Equals("true"))
            {
                Logger.Debug(
                    $"Answer of BorrowerAddSpouseAsCoBorrower is ${answerBorrowerAddSpouseAsCoBorrower} so not sending invite");
                return status;
            }

            string[] fieldsTobeUpdate =
            {
                "FirstName",
                "MiddleName",
                "Suffix",
                "BirthDate",
                "SSN",
                "LastName",
                "PreferredEmail",
                "CellPhone"
            };

            foreach (var fieldName in fieldsTobeUpdate)
            {
                var answer =
                    await GetQuestionAnswerFromSubSection(subSectionCoborrowerSpouseInvite, "Spouse" + fieldName);
                UpdateQuestionAnswerInSubSection(subSections, "Borrower" + fieldName, answer);
            }

            var strInvitationId =
                await this.ExecuteSendInvitationEventNew(questionnaire.TemporaryApplicationNumber, sectionId,
                    subSections);

            if (!string.IsNullOrEmpty(strInvitationId))
            {
                //If the invitation has been sent to all the applicant then create SubSections for the user belonging to 'SubSectionId'
                Logger.Debug($"Starting AssignUserNameToApplicantPersonalInfoSectionNew");
                await AssignUserNameToInfoSubSections(questionnaire, strInvitationId, sectionId, subSections);

                var borrowerInfo = await GetBorrowerInfoFromTheMapping(questionnaire.TemporaryApplicationNumber,
                    sectionId, subSections);

                if (!questionnaire.UserState.Any(u => u.UserName == strInvitationId))
                {
                    IUserAccessedStatus _applicantUserState = new UserAccessedStatus();
                    _applicantUserState.UserName = strInvitationId;
                    _applicantUserState.SpouseUserName = currentUserName;
                    _applicantUserState.LastAccessedSection = 1;
                    _applicantUserState.LastAccessedSectionSeqNo = 1;
                    _applicantUserState.LastAccessedSubSectionSeqNo = 1;
                    _applicantUserState.HighestSectionReached = 1;
                    _applicantUserState.Status = FormStatus.Open;
                    _applicantUserState.InviteId = strInvitationId;
                    if (!string.IsNullOrEmpty(borrowerInfo.Email))
                    {
                        _applicantUserState.UserEmailId = borrowerInfo.Email;
                    }

                    _applicantUserState.ApplicantType = ApplicantType.CoBorrowerSpouse.ToString();

                    questionnaire.UserState.Add(_applicantUserState);
                    await QuestionnaireRepository.PushUserState(questionnaire.TemporaryApplicationNumber, _applicantUserState);
                }

                var userState = questionnaire.UserState.FirstOrDefault(u => u.UserName == currentUserName);
                if (userState != null)
                {
                    userState.SpouseUserName = strInvitationId;
                    await QuestionnaireRepository.UpdateUserStateWithSpouseUsername(questionnaire.TemporaryApplicationNumber, currentUserName, strInvitationId);
                }
                status = await AddDynamicSubSection(questionnaire, borrowerInfo, strInvitationId);
            }

            return status;
        }

        private async Task<bool> AssignUserNameToInfoSubSections(IQuestionnaire questionnaire, string invitationId, int sectionId, List<ISubSection> subSections)
        {
            var status = false;
            
            if(subSections != null)
            {
                IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
                var sectionIndex = questionnaire.ApplicationForm.Sections.FindIndex(x => x.SectionId == profileSectionId);

                foreach(var subSection in subSections)
                {
                    subSection.UserName = invitationId;

                    answerRequest = new AnswerUpdateRequest();
                    answerRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                    answerRequest.SectionId = profileSectionId;
                    answerRequest.SubSectionId = subSection.SubSectionId;

                    await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest, subSection);
                }
                
                status = true;
            }

            return status;
        }

        private async Task<bool> AddDynamicSubSection(IQuestionnaire questionnaire, 
                                                                    IBorrowerCoBorrowerInfo borrowerInfo,
                                                                    string invitationId)
        {
            //IBorrowerCoBorrowerInfo borrowerInfo = await GetBorrowerInfoFromTheMapping(sectionId, subSections);
            var status = false;
            
            if (borrowerInfo != null)
            {
                if (borrowerInfo.UserName == COBORROWERSPOUSEUSERBASED ||  borrowerInfo.UserName == invitationId)
                {
                    var appFieldMap = QuestionnaireConfigurations.ApplicantDynamicSections;
                    var seqNumberRange = (int)appFieldMap.SeqNumberRange;

                    foreach (IApplicantSubSectionFieldMap item in appFieldMap.Map)
                    {
                        Logger.Debug($"Adding dynamic template id : {item.TemplateId}");

                        if (item.TemplateId.Equals("coborrowerResidenceDetail", StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }

                        //Discard
                        if (item != null && item.ExcludeUserType != null && item.ExcludeUserType.Contains(Extensions.GetDescription(borrowerInfo.ApplicantType)))
                        {
                            Logger.Debug($"Discarding as Extensions.GetDescription(borrowerInfo.ApplicantType) is in exclude list");   
                            continue;
                        }

                        var sectionToBeUpdated = questionnaire.ApplicationForm.Sections.Find(s => s.SectionId == item.SectionId);
                        
                          if(sectionToBeUpdated == null)
                            continue;
                        var newSubSectionId = await GenerateSubSectionIdNew(invitationId, item.TemplateId);
                        
                        //Get the subSection template to create a new subSection
                        ISubSection subSectionFromTemplate = sectionToBeUpdated.Templates[item.TemplateId];
                        if (subSectionFromTemplate == null)
                        {
                            throw new Exception($"Template {item.TemplateId} does not exist.");
                        }

                        // Validate SubSection Id need to be unique
                        if (sectionToBeUpdated.SubSections.FindAll(ss => ss.SubSectionId == newSubSectionId).Count > 0)
                        {
                            //throw new Exception($"Subsection having the Section Id '{newSubSectionId}' already exists in Section {item.SectionId}. SubSectionId has to be unique.");
                            Logger.Warn($"Subsection having the Section Id '{newSubSectionId}' already exists in Section {item.SectionId}. SubSectionId has to be unique.");
                            continue;
                        }                        
                        
                        //Get new seq no
                        var subSectionCount = sectionToBeUpdated.SubSections.Count; //.FindAll(ss => ss.SubSectionName == subSection.SubSectionName).Count;
                        var newSeqNo = 1000;

                        if (item.SeqNumberRange.HasValue)
                        {
                            // int userCount = sectionToBeUpdated.SubSections.Select(ss => ss.UserName).Distinct().Count();
                            var userCount = questionnaire.UserState.Count();
                            
                            if(item.TemplateId == "coBorrowerIncome")
                                newSeqNo = (userCount * item.SeqNumberRange.Value) + 1000;
                            else
                                newSeqNo = (userCount * item.SeqNumberRange.Value) + subSectionFromTemplate.SeqNo.Value;
                        }
                        else
                        {
                            newSeqNo = seqNumberRange * (subSectionCount + 1);
                        }

                        subSectionFromTemplate.UserName = invitationId;
                        subSectionFromTemplate.SubSectionId = newSubSectionId;
                        subSectionFromTemplate.SeqNo = newSeqNo;

                        //Assign newly generated SubSectionId to the child question info in the ChilQuestions array
                        subSectionFromTemplate = await UpdateSubSectionId(subSectionFromTemplate, subSectionFromTemplate.SubSectionId);

                        var addSubSectionRequest = new AddSubSectionRequest
                        {
                            SubSection = (SubSection) subSectionFromTemplate,
                            SectionId = item.SectionId,
                            TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber
                        };
                        Logger.Debug($"Calling AddSubSectionNew for Section {item.SectionId} SubSectionId {subSectionFromTemplate.SubSectionId}");
                        status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                        Logger.Debug($"Completed AddSubSectionNew");
                    }
                }
                else
                {
                    status = true;
                }
            }
            
            return status;
        } 
    
        private async Task<bool> RegenerateInvite(string temporaryApplicationNumber,List<IUserAccessedStatus> userStats,ISection section, string subSectionId)
        {
            Logger.Debug($"Get the Spouse Email that Coborrower wish to invite");
            var spouseInviteSubSection = section.SubSections.FirstOrDefault(x=>x.SubSectionId == subSectionId);
            var answer = await GetQuestionAnswerFromSubSection(spouseInviteSubSection, ConstantUtils.SpousePreferredEmail);
            
            Logger.Debug($"Get the data from UserState");
            var currentUser = await GetCurrentUser();
            var coBorrowerUserState = userStats.FirstOrDefault(x=>x.UserName == currentUser);
            var spouseUserName = coBorrowerUserState != null ? coBorrowerUserState.SpouseUserName : string.Empty;
            var coBorrowerSpouseUserState = userStats.FirstOrDefault(x=>x.UserName == spouseUserName);

            if(coBorrowerSpouseUserState != null && coBorrowerSpouseUserState.InviteId == coBorrowerSpouseUserState.UserName)
            {
                if(coBorrowerSpouseUserState.UserEmailId == answer)
                    return false;

                //Update Invite                
                try
                {
                    var result = await InviteService.RegenerateInviteToken(coBorrowerSpouseUserState.InviteId, answer);

                    if (result != null)
                    {
                        Logger.Debug($"Updating CoBorrower Spouse EmailId to {answer} for application {temporaryApplicationNumber}");
                        //TODO: It's not doing anything
                        //var resBorrowerPreferredEmail = UpdateQuestionAnswerInSubSection(subSections,"BorrowerPreferredEmail", answer);

                        Logger.Debug($"Update UserState {temporaryApplicationNumber} {coBorrowerSpouseUserState.InviteId} {answer}");
                        var userStateUpdateStatus = await QuestionnaireRepository.UpdateUserEmailIdInUserState(
                            temporaryApplicationNumber,
                                        coBorrowerSpouseUserState.InviteId,
                                        answer
                                    );
                    }
                    else
                    {
                        Logger.Warn($"There was a problem in resending the invite for coborrower {currentUser} inviteId {coBorrowerSpouseUserState.InviteId}");
                        return false;
                    }
                }
                catch(ClientException ex)
                {
                    Logger.Warn($"There was a problem in resending the invite for coborrower {currentUser} inviteId {coBorrowerSpouseUserState.InviteId}");
                    return false;
                }
            }

            return true;
        }
    
    }
}
