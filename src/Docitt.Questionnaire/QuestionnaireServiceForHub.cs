using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Docitt.Questionnaire.Abstractions;
using LendFoundry.Security.Tokens;

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        static Regex ValidEmailRegex = CreateValidEmailRegex();

        /// <summary>
        /// For Email Validation 
        /// </summary>
        /// <returns></returns>
        private static Regex CreateValidEmailRegex()
        {
            //TODO Move to Helper common class 
            var validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// For Relationship Validation 
        /// </summary>
        /// <returns></returns>
        private bool ValidateRelationship(string data)
        {
            //TODO Move to configuration or const
            var relationship = new string[] {"Spouse","Parent","Sibling","Other Relative","Co-Worker","Other","CoBorrowerSpouse"};
            if(!relationship.Contains(data))
            {
                return false;      
            }
            return true;
        }

        private InviteHubResponse ValidateDuplicates(IQuestionnaire questionnaire, AddCoBorrowerModal borrower)
        {
            var returnResult = new InviteHubResponse();
            returnResult.status = true;

            var loggedInUser = GetCurrentUser().Result;
            if(loggedInUser.ToLower().Equals(borrower.EmailAddress.ToLower()))
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400",Message="EmailAddress is already used for primary borrower."};
                return returnResult; 
            }
            
            var profileSection = questionnaire.ApplicationForm.Sections.FirstOrDefault(x=>x.SectionId == Convert.ToInt32(SectionDetails.Profile));
            
            var userState = questionnaire.UserState.FirstOrDefault(x => x.UserEmailId.ToLower() == borrower.EmailAddress.ToLower());

            //Validate for Spouse
            var individualOrJointCreditSubSection = profileSection.SubSections.FirstOrDefault(x=>x.SubSectionId == ConstantUtils.JointCreditSubSectionId);
            if(individualOrJointCreditSubSection != null)
            {
                var individualOrJointCreditSubSectionQuestions = individualOrJointCreditSubSection.QuestionSections.SelectMany(x=>x.QuestionList);
                var SpouseEmailQuestion = individualOrJointCreditSubSectionQuestions
                                            .FirstOrDefault(x=>x.QuestionFieldName == "SpousePreferredEmail");
                if(SpouseEmailQuestion != null)
                {   
                    
                        //if(SpouseEmailQuestion.Answer.ToLower().Equals(borrower.EmailAddress.ToLower()))
                    if(userState != null && userState.ApplicantType.ToLower() == ApplicantType.Spouse.ToString().ToLower())
                    {
                        returnResult.status = false;
                        returnResult.error = new InviteErrorResponse() {Code="400",Message="EmailAddress is already used for spouse."};
                        return returnResult; 
                    }
                    else if(userState != null && userState.ApplicantType.ToLower() == ApplicantType.CoBorrowerSpouse.ToString().ToLower())
                    {
                        returnResult.status = false;
                        returnResult.error = new InviteErrorResponse() {Code="400",Message="EmailAddress is already used for co-borrower spouse."};
                        return returnResult; 
                    }
                } 
            }

            //validate for borrower email address
             if(userState != null && userState.ApplicantType.ToLower() == ApplicantType.Borrower.ToString().ToLower())
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400",Message="EmailAddress is already used for primary borrower."};
                return returnResult;  
            }
            
            //Validate for Coborrower
            var coBorrowerInvitationSubSection = profileSection.SubSections.FirstOrDefault(x => x.TemplateFieldName != null && x.TemplateFieldName.Equals(SectionConstants.CoBorrowerInvitationTemplateName));
            if(coBorrowerInvitationSubSection == null)
            {
                // This may be the case of first entry
                returnResult.status = true;
                return returnResult;
            }

            var questions = coBorrowerInvitationSubSection.QuestionSections.Where(x=>x.QuestionSectionName == "AddCoBorrower")
                               .SelectMany(x=>x.QuestionList);

            if(questions == null)
            {
                // This may be the case of first entry
                returnResult.status = true;
                return returnResult; 
            }
            else
            {
                foreach(var item in questions.ToList())
                {
                    if(!string.IsNullOrEmpty(item.Answer))
                    {
                        var value = JsonConvert.DeserializeObject<AddCoBorrowerModal>(item.Answer);
                        
                        // This validation to not allow shared email id in applicants will be added back 
                        // after username and emailid are updated as one field on sign-up page 
                        
                        if(value.EmailAddress.ToLower().Equals(borrower.EmailAddress.ToLower()))
                        {
                           returnResult.status = false;
                           returnResult.error = new InviteErrorResponse() {Code="400",Message="EmailAddress is already used for co-borrower."};
                           return returnResult; 
                        }
                        

                        // Validation to check for duplicate applicants
                        if(value.EmailAddress.ToLower().Equals(borrower.EmailAddress.ToLower()) && 
                           value.FirstName.ToLower().Equals(borrower.FirstName.ToLower()) && 
                           value.LastName.ToLower().Equals(borrower.LastName.ToLower())
                          )
                        {
                            returnResult.status = false;
                            returnResult.error = new InviteErrorResponse() {Code="400",Message=$"Applicant {value.EmailAddress.ToLower()} is already added."};
                            return returnResult; 
                        }

                        //Validation to check for Spouse applicant duplicate
                        if(borrower.Relationship.Equals("Spouse") && value.Relationship.Equals("Spouse"))
                        {
                            returnResult.status = false;
                            returnResult.error = new InviteErrorResponse() {Code="400",Message="Spouse co-borrower is already added."};
                            return returnResult;
                        }
                    }
                }
            }

            return returnResult;
        }

        /// <summary>
        /// Validate input AddCoBorrowerModal
        /// </summary>
        /// <param name="borrower"></param>
        /// <returns></returns>
        private InviteHubResponse ValidateAddCoBorrowerModal(AddCoBorrowerModal borrower)
        {
            var returnResult = new InviteHubResponse();
            returnResult.status = true;

            if(string.IsNullOrEmpty(borrower.FirstName))
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400",Message="FirstName is required."};
                return returnResult; 
            }

            if(string.IsNullOrEmpty(borrower.LastName))
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400",Message="LastName is required."};
                return returnResult; 
            }

            if(string.IsNullOrEmpty(borrower.EmailAddress))
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400",Message="EmailAddress is required."};
                return returnResult; 
            }

            if(string.IsNullOrEmpty(borrower.Relationship))
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400", Message="Relationship is required."};
                return returnResult; 
            }

            var isValidEmail = ValidEmailRegex.IsMatch(borrower.EmailAddress);
            if(!isValidEmail)
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400", Message="EmailAddress is not valid."};
                return returnResult; 
            }

            var isValidRelationship = ValidateRelationship(borrower.Relationship);
            if(!isValidRelationship)
            {
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400", Message="Relationship is not valid."};
                return returnResult; 
            }

            return returnResult;
        }

        /// <summary>
        /// SetApplicationAsJointCredit will enable the Joint Credit
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        private async Task<bool> SetApplicationAsJointCredit(IQuestionnaire questionnaire, bool IsSpouse)
        {
            var sectionId = Convert.ToInt32(SectionDetails.Profile);
            var jointCreditSectionId = ConstantUtils.JointCreditSubSectionId;
            var coBorrowerInvitationSubSectionSeqNo = SectionConstants.CoBorrowerInvitationSubSectionSeqNo;
            var coborrowerInvitationSubSectionId = SectionConstants.CoborrowerInvitationSubSectionId;           
            var addSubSectionRequest = new AddSubSectionRequest();

            var profileSection = questionnaire.ApplicationForm.Sections.FirstOrDefault(x=>x.SectionId == sectionId);

            var jointCreditSection = profileSection.SubSections.Where(x=>x.SubSectionId.Equals(jointCreditSectionId))
                .FirstOrDefault();
            
            if(jointCreditSection == null)
                return false;

            var questionlist = jointCreditSection.QuestionSections.SelectMany(x=>x.QuestionList);

            if(questionlist == null)
                return false;
            
            var questionBorrowerIndividualOrJointCredit = questionlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerIndividualOrJointCredit");

            if(questionBorrowerIndividualOrJointCredit == null)
                return false;

            if(questionBorrowerIndividualOrJointCredit.Answer != SectionConstants.JointCreditAnswerOption)
            {
                //Set Answer as JointCredit
                questionBorrowerIndividualOrJointCredit.Answer = SectionConstants.JointCreditAnswerOption;

                //Save Answer
                IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                answerRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                answerRequest.SectionId = sectionId;
                answerRequest.SubSectionId = jointCreditSectionId;
                 
                await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest, jointCreditSection);

            }

            var coBorrowerInvitationTemplateName = SectionConstants.CoBorrowerInvitationTemplateName;
            var coBorrowerInvitationSubSection = profileSection.SubSections
                                                  .FirstOrDefault(x => x.TemplateFieldName != null && 
                                                  x.TemplateFieldName.Equals(coBorrowerInvitationTemplateName));
            
            // If its a coborrower applicant and coBorrowerInvitationSubSection is not available then add a subsection
            if(!IsSpouse && coBorrowerInvitationSubSection == null)
            {
                //Add Template coBorrowerInvitationInfo
                var coBorrowerInvitationTemplate = profileSection.Templates.FirstOrDefault(x=>x.Key == SectionConstants.CoBorrowerInvitationTemplateName);
                if(coBorrowerInvitationTemplate.Key == null)
                    return false;
                else
                {
                    var subSectionFromTemplate = coBorrowerInvitationTemplate.Value;

                    subSectionFromTemplate.UserName = questionnaire.UserName;
                    subSectionFromTemplate.SeqNo = coBorrowerInvitationSubSectionSeqNo;
                    subSectionFromTemplate.SubSectionId = coborrowerInvitationSubSectionId;
                    subSectionFromTemplate.DeleteTemplates = new List<string>(){coborrowerInvitationSubSectionId};
                    addSubSectionRequest = new AddSubSectionRequest();
                    addSubSectionRequest.SubSection = (SubSection)subSectionFromTemplate;
                    addSubSectionRequest.SectionId = sectionId;
                    addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                    Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {subSectionFromTemplate.SubSectionId}");
                    await AddSubSectionNew(questionnaire, addSubSectionRequest);
                    Logger.Debug($"Completed AddSubSectionNew");
                                      
                }
            }
            return true;
        }

        private async Task UpdateMaritalStatus(string applicationNumber, ISubSection individualOrJointCreditSubSection)
        {
            //Marital Status
            var maritalStatus = individualOrJointCreditSubSection.QuestionSections
                                            .Where(x=>x.QuestionSectionName == "Married")
                                            .SelectMany(x=>x.QuestionList)
                                            .FirstOrDefault(x=>x.QuestionFieldName == "BorrowerMaritalStatus");

            if(maritalStatus != null)
            {
                maritalStatus.Answer = "Married";

                IAnswerUpdateRequest maritalStatusAnswerUpdateRequest = new AnswerUpdateRequest();
                maritalStatusAnswerUpdateRequest.QuestionId = maritalStatus.QuestionId;
                maritalStatusAnswerUpdateRequest.SectionId = 1;
                maritalStatusAnswerUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                maritalStatusAnswerUpdateRequest.QuestionSectionSeqNo = 2;
                maritalStatusAnswerUpdateRequest.TemporaryApplicationNumber = applicationNumber;//questionnaire.TemporaryApplicationNumber;
                maritalStatusAnswerUpdateRequest.Answer = maritalStatus.Answer;

                if (maritalStatusAnswerUpdateRequest.QuestionSectionSeqNo > 0)
                {
                    var result = await QuestionnaireRepository.UpdateQuestion(maritalStatusAnswerUpdateRequest, null);
                }
            }    

            var borrowerMaritalStatusMarried = individualOrJointCreditSubSection.QuestionSections
                                            .Where(x=>x.QuestionSectionName == "Married")
                                            .SelectMany(x=>x.QuestionList)
                                            .FirstOrDefault(x=>x.QuestionFieldName == "BorrowerMaritalStatusMarried");
            
            if(borrowerMaritalStatusMarried != null)
            {
                 borrowerMaritalStatusMarried.Answer = "Married";

                IAnswerUpdateRequest maritalStatusAnswerUpdateRequest = new AnswerUpdateRequest();
                maritalStatusAnswerUpdateRequest.QuestionId = borrowerMaritalStatusMarried.QuestionId;
                maritalStatusAnswerUpdateRequest.SectionId = 1;
                maritalStatusAnswerUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                maritalStatusAnswerUpdateRequest.QuestionSectionSeqNo = 10;
                maritalStatusAnswerUpdateRequest.TemporaryApplicationNumber = applicationNumber; 
                maritalStatusAnswerUpdateRequest.Answer = borrowerMaritalStatusMarried.Answer; 

                if (maritalStatusAnswerUpdateRequest.QuestionSectionSeqNo > 0)
                {
                    var result = await QuestionnaireRepository.UpdateQuestion(maritalStatusAnswerUpdateRequest, null);
                }
            }
        }

        private async Task UpdateAddSpouseAsCoborrower(string applicationNumber, ISubSection individualOrJointCreditSubSection)
        {
            //Spouse co-borrower
            var borrowerAddSpouseAsCoBorrower = individualOrJointCreditSubSection.QuestionSections
                                            .Where(x=>x.QuestionSectionName == "Spouse co-borrower")
                                            .SelectMany(x=>x.QuestionList)
                                            .FirstOrDefault(x=>x.QuestionFieldName == "BorrowerAddSpouseAsCoBorrower");

            if(borrowerAddSpouseAsCoBorrower != null)
            {
                borrowerAddSpouseAsCoBorrower.Answer = "true";

                IAnswerUpdateRequest borrowerAddSpouseAsCoBorrowerUpdateRequest = new AnswerUpdateRequest();
                borrowerAddSpouseAsCoBorrowerUpdateRequest.QuestionId = borrowerAddSpouseAsCoBorrower.QuestionId;
                borrowerAddSpouseAsCoBorrowerUpdateRequest.SectionId = 1;
                borrowerAddSpouseAsCoBorrowerUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                borrowerAddSpouseAsCoBorrowerUpdateRequest.QuestionSectionSeqNo = 12;
                borrowerAddSpouseAsCoBorrowerUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                borrowerAddSpouseAsCoBorrowerUpdateRequest.Answer = borrowerAddSpouseAsCoBorrower.Answer;

                if (borrowerAddSpouseAsCoBorrowerUpdateRequest.QuestionSectionSeqNo > 0)
                {
                    var result = await QuestionnaireRepository.UpdateQuestion(borrowerAddSpouseAsCoBorrowerUpdateRequest, null);
                }
            }    
        }

        private async Task UpdateBorrowerFirstName(string applicationNumber, string firstName, ISubSection individualOrJointCreditSubSection)
        {
            // Spouse First Name
            Logger.Debug($"...Adding {firstName} to 'SpouseFirstName'");
            var questionslistFirstName = individualOrJointCreditSubSection.QuestionSections.Where(x=>x.QuestionSectionName == "First Name")
                               .SelectMany(x=>x.QuestionList);

            if(questionslistFirstName != null)
            {
                var questionFirstNameJointCredit = questionslistFirstName.FirstOrDefault(x=>x.QuestionFieldName == "SpouseFirstName");
                if(questionFirstNameJointCredit != null)
                {
                    questionFirstNameJointCredit.Answer = firstName; 

                    IAnswerUpdateRequest spouseFirstNameUpdateRequest = new AnswerUpdateRequest();
                    spouseFirstNameUpdateRequest.QuestionId = questionFirstNameJointCredit.QuestionId;
                    spouseFirstNameUpdateRequest.SectionId = 1;
                    spouseFirstNameUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                    spouseFirstNameUpdateRequest.QuestionSectionSeqNo = 15;
                    spouseFirstNameUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                    spouseFirstNameUpdateRequest.Answer = questionFirstNameJointCredit.Answer;

                    if (spouseFirstNameUpdateRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(spouseFirstNameUpdateRequest, null);
                    }
                }
                else
                    Logger.Debug("Question 'SpouseFirstName' not found!... ");

                questionFirstNameJointCredit = questionslistFirstName.FirstOrDefault(x=>x.QuestionFieldName == "SpouseFirstNameMarried");
                if(questionFirstNameJointCredit != null)
                {
                    questionFirstNameJointCredit.Answer = firstName; 

                    IAnswerUpdateRequest spouseFirstNameUpdateRequest = new AnswerUpdateRequest();
                    spouseFirstNameUpdateRequest.QuestionId = questionFirstNameJointCredit.QuestionId;
                    spouseFirstNameUpdateRequest.SectionId = 1;
                    spouseFirstNameUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                    spouseFirstNameUpdateRequest.QuestionSectionSeqNo = 25;
                    spouseFirstNameUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                    spouseFirstNameUpdateRequest.Answer = questionFirstNameJointCredit.Answer;

                    if (spouseFirstNameUpdateRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(spouseFirstNameUpdateRequest, null);
                    }
                }
                else
                    Logger.Debug("Question 'SpouseFirstNameMarried' not found!... ");
            }
            else
                Logger.Debug("Questionlist 'First Name' not found!... ");
        }

        private async Task UpdateBorrowerLastName(string applicationNumber, string lastName, ISubSection individualOrJointCreditSubSection)
        {
            // Spouse LastName
            Logger.Debug($"...Adding {lastName} to 'SpouseLastName'");
            var questionslistLastName = individualOrJointCreditSubSection.QuestionSections
                                            .Where(x=>x.QuestionSectionName == "Last Name")
                                            .SelectMany(x=>x.QuestionList);
            if(questionslistLastName != null)
            {
                var questionLastNameJointCredit = questionslistLastName.FirstOrDefault(x=>x.QuestionFieldName == "SpouseLastName");

                if(questionLastNameJointCredit != null)
                {
                    questionLastNameJointCredit.Answer = lastName;

                    IAnswerUpdateRequest spouseLastNameUpdateRequest = new AnswerUpdateRequest();
                    spouseLastNameUpdateRequest.QuestionId = questionLastNameJointCredit.QuestionId;
                    spouseLastNameUpdateRequest.SectionId = 1;
                    spouseLastNameUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                    spouseLastNameUpdateRequest.QuestionSectionSeqNo = 17;
                    spouseLastNameUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                    spouseLastNameUpdateRequest.Answer = questionLastNameJointCredit.Answer;

                    if (spouseLastNameUpdateRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(spouseLastNameUpdateRequest, null);
                    }
                }  
                else
                    Logger.Debug("Question 'SpouseLastName' not found!... ");

                var questionLastNameMarriedJointCredit = questionslistLastName.FirstOrDefault(x=>x.QuestionFieldName == "SpouseLastNameMarried");

                if(questionLastNameMarriedJointCredit != null){
                    
                    questionLastNameMarriedJointCredit.Answer = lastName;

                    IAnswerUpdateRequest spouseLastNameUpdateRequest = new AnswerUpdateRequest();
                    spouseLastNameUpdateRequest.QuestionId = questionLastNameMarriedJointCredit.QuestionId;
                    spouseLastNameUpdateRequest.SectionId = 1;
                    spouseLastNameUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                    spouseLastNameUpdateRequest.QuestionSectionSeqNo = 27;
                    spouseLastNameUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                    spouseLastNameUpdateRequest.Answer = questionLastNameMarriedJointCredit.Answer;

                    if (spouseLastNameUpdateRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(spouseLastNameUpdateRequest, null);
                    }
                }   
                else
                    Logger.Debug("Question 'SpouseLastNameMarried' not found!... ");
            }
            else
                Logger.Debug("Questionlist 'Last Name' not found!... ");
        }

        private async Task UpdateBorrowerEmailId(string applicationNumber, string emailId, ISubSection individualOrJointCreditSubSection)
        {
            // Spouse LastName
            Logger.Debug($"...Adding {emailId} to 'BorrowerPreferredEmail'");
            var questionslistEmailId = individualOrJointCreditSubSection.QuestionSections
                                            .Where(x=>x.QuestionSectionName == "Email")
                                            .SelectMany(x=>x.QuestionList);
            if(questionslistEmailId != null)
            {
                var questionEmailIdJointCredit = questionslistEmailId.FirstOrDefault(x=>x.QuestionFieldName == "SpousePreferredEmail");

                if(questionEmailIdJointCredit != null)
                {
                    questionEmailIdJointCredit.Answer = emailId;

                    IAnswerUpdateRequest spouseEmailIdUpdateRequest = new AnswerUpdateRequest();
                    spouseEmailIdUpdateRequest.QuestionId = questionEmailIdJointCredit.QuestionId;
                    spouseEmailIdUpdateRequest.SectionId = 1;
                    spouseEmailIdUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                    spouseEmailIdUpdateRequest.QuestionSectionSeqNo = 35;
                    spouseEmailIdUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                    spouseEmailIdUpdateRequest.Answer = questionEmailIdJointCredit.Answer;

                    if (spouseEmailIdUpdateRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(spouseEmailIdUpdateRequest, null);
                    }
                }  
                else
                    Logger.Debug("Question 'BorrowerPreferredEmail' not found!... ");
            }
            else
                Logger.Debug("Questionlist 'Last Name' not found!... ");
        }

        private async Task UpdateAddAnotherCoBorrower(string applicationNumber, ISubSection individualOrJointCreditSubSection)
        {
            // Spouse LastName
            Logger.Debug("...UpdateAddAnotherCoBorrower");
            var questionslistAddAnotherCoBorrowerId = individualOrJointCreditSubSection.QuestionSections
                                            .Where(x=>x.QuestionSectionName == "Spouse co-borrower")
                                            .SelectMany(x=>x.QuestionList);
            if(questionslistAddAnotherCoBorrowerId != null)
            {
                var questionAddAnotherCoBorrowerJointCredit = questionslistAddAnotherCoBorrowerId.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerAddAsCoBorrower");

                if(questionAddAnotherCoBorrowerJointCredit != null)
                {
                    questionAddAnotherCoBorrowerJointCredit.Answer = "true";

                    IAnswerUpdateRequest spouseAddAnotherCoBorrowerUpdateRequest = new AnswerUpdateRequest();
                    spouseAddAnotherCoBorrowerUpdateRequest.QuestionId = questionAddAnotherCoBorrowerJointCredit.QuestionId;
                    spouseAddAnotherCoBorrowerUpdateRequest.SectionId = 1;
                    spouseAddAnotherCoBorrowerUpdateRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
                    spouseAddAnotherCoBorrowerUpdateRequest.QuestionSectionSeqNo = 37;
                    spouseAddAnotherCoBorrowerUpdateRequest.TemporaryApplicationNumber = applicationNumber;
                    spouseAddAnotherCoBorrowerUpdateRequest.Answer = questionAddAnotherCoBorrowerJointCredit.Answer;

                    if (spouseAddAnotherCoBorrowerUpdateRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(spouseAddAnotherCoBorrowerUpdateRequest, null);
                    }
                }  
                else
                    Logger.Debug("Question 'BorrowerPreferredEmail' not found!... ");
            }
            else
                Logger.Debug("Questionlist 'Last Name' not found!... ");
        }

        private async Task<AddCoBorrowerModal> FillSpouseBorrowerInfo(IQuestionnaire questionnaire, AddCoBorrowerModal borrower)
        {
            var sectionId = Convert.ToInt32(SectionDetails.Profile);          
            string individualOrJointCreditSubSectionId = ConstantUtils.JointCreditSubSectionId;

            var profileSection = questionnaire.ApplicationForm.Sections.FirstOrDefault(x=>x.SectionId == sectionId);
            var individualOrJointCreditSubSection = profileSection.SubSections.FirstOrDefault(x => x.SubSectionId != null && x.SubSectionId.Equals(individualOrJointCreditSubSectionId));

            if(individualOrJointCreditSubSection == null)
            {
                Logger.Debug("SubSection 'IndividualOrJoinCreditSubSection' not found!...");
                return borrower;
            }

            //Marital Status
            await UpdateMaritalStatus(questionnaire.TemporaryApplicationNumber, individualOrJointCreditSubSection);
 
            await UpdateAddSpouseAsCoborrower(questionnaire.TemporaryApplicationNumber, individualOrJointCreditSubSection);
            
            await UpdateBorrowerFirstName(questionnaire.TemporaryApplicationNumber, borrower.FirstName, individualOrJointCreditSubSection);

            await UpdateBorrowerLastName(questionnaire.TemporaryApplicationNumber, borrower.LastName, individualOrJointCreditSubSection);

            await UpdateBorrowerEmailId(questionnaire.TemporaryApplicationNumber, borrower.EmailAddress, individualOrJointCreditSubSection);
            
            Logger.Debug("Update spouse data in joint credit...");
            var sectionIndex = questionnaire.ApplicationForm.Sections.FindIndex(x => x.SectionId == sectionId);
            var subSectionIndex = questionnaire.ApplicationForm.Sections[0].
                                                SubSections.FindIndex(x => x.SubSectionId.ToLower() == individualOrJointCreditSubSection.SubSectionId.ToLower());
            Logger.Debug($"sectionIndex {sectionIndex} & subSectionIndex {subSectionIndex} & subsectionId {individualOrJointCreditSubSection.SubSectionId}");
            IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
            answerRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
            answerRequest.SectionId = sectionId;
            answerRequest.SubSectionId = individualOrJointCreditSubSection.SubSectionId;
            await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest, individualOrJointCreditSubSection);

            Logger.Debug("Adding dynamic sub-sections for spouse");
            var sequenceNo = 4000;
            var questionGuid = Guid.NewGuid().ToString();
            borrower.QuestionId = "Q-" + questionGuid;
            
            //Add template for spouse
            Logger.Debug("Adding spouseInfo subsection...");
            var spouseInfo = profileSection.Templates.FirstOrDefault(x=>x.Key == "spouseInfo");
            if(spouseInfo.Value != null)
            {
                var spouseInfoSection = spouseInfo.Value;
                spouseInfoSection.SubSectionId = "ID-" + questionGuid;
                spouseInfoSection.SeqNo = sequenceNo;
                spouseInfoSection.DeleteTemplates = new List<string>(){spouseInfoSection.SubSectionId , spouseInfoSection.SubSectionId + "-1", spouseInfoSection.SubSectionId + "-2" };
                //Update UserName in SubSection 
                spouseInfoSection.UserName = borrower.EmailAddress.ToLower();

                var questionlist = spouseInfoSection.QuestionSections.SelectMany(x=>x.QuestionList);
                if(questionlist != null)
                {
                    //FirstName
                    var questionFirstName = questionlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerFirstName");
                    questionFirstName.Answer = borrower.FirstName;

                    //LastName
                    var questionLastName = questionlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerLastName");
                    questionLastName.Answer =  borrower.LastName;
                }

                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)spouseInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {spouseInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }
                
            Logger.Debug("Adding spouseCommunicationInfo subsection...");
            var spouseCommunicationInfo = profileSection.Templates.FirstOrDefault(x=>x.Key == "spouseCommunicationInfo");
            if(spouseCommunicationInfo.Value != null)
            {
                var spouseCommunicationSection = spouseCommunicationInfo.Value;
                spouseCommunicationSection.SubSectionId = "ID-" + questionGuid + "-1";
                spouseCommunicationSection.SeqNo = sequenceNo + 1; 
                spouseCommunicationSection.DeleteTemplates = new List<string>() { "ID-" + questionGuid, spouseCommunicationSection.SubSectionId, $"ID-{questionGuid }-2" };
                spouseCommunicationSection.UserName = borrower.EmailAddress.ToLower();

                var questionCommunicationlist = spouseCommunicationSection.QuestionSections.SelectMany(x=>x.QuestionList);

                if(questionCommunicationlist != null)
                {
                    //Email
                    var questionEmail = questionCommunicationlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerPreferredEmail");
                    questionEmail.Answer = borrower.EmailAddress.ToLower();
                    //Update Phone Number TODO discuss with Client
                }   
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)spouseCommunicationSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {spouseCommunicationSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }

            Logger.Debug("Adding spouseReoInfo subsection...");
            var spouseReoInfo = profileSection.Templates.FirstOrDefault(x => x.Key.ToLower() == ConstantUtils.SpouseReoInfoTemplate.ToLower());
            if (spouseReoInfo.Value != null)
            {
                var spouseReoInfoSection = spouseReoInfo.Value;
                spouseReoInfoSection.SubSectionId = $"ID-{questionGuid}-2";
                spouseReoInfoSection.SeqNo = 20000;
                spouseReoInfoSection.DeleteTemplates = new List<string>() { $"ID-{questionGuid}", $"ID-{questionGuid }-1", spouseReoInfoSection.SubSectionId };
                spouseReoInfoSection.UserName = borrower.EmailAddress.ToLower();
                
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)spouseReoInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {spouseReoInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }
            return borrower;
            //throw new NotImplementedException("Error");
        }

        /// <summary>
        /// FillBorrowerInfo will fill coborrower details
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <param name="borrower"></param>
        /// <returns></returns>
        private async Task<AddCoBorrowerModal> FillBorrowerInfo(IQuestionnaire questionnaire, AddCoBorrowerModal borrower)
        {
            var sectionId = Convert.ToInt32(SectionDetails.Profile);
            string individualOrJointCreditSubSectionId = ConstantUtils.JointCreditSubSectionId;   
            var coBorrowerInvitationTemplateName = SectionConstants.CoBorrowerInvitationTemplateName;

            // If the data is for spouse process it seperately and return
            if(borrower.Relationship == CoBorowerRelationship.Spouse.ToString())
                return await FillSpouseBorrowerInfo(questionnaire, borrower);

            var profileSection = questionnaire.ApplicationForm.Sections.FirstOrDefault(x=>x.SectionId == sectionId);
            var individualOrJointCreditSubSection = profileSection.SubSections
                .FirstOrDefault(x => x.SubSectionId != null && x.SubSectionId.Equals(individualOrJointCreditSubSectionId));

            if(individualOrJointCreditSubSection == null)
            {
                Logger.Debug("SubSection 'IndividualOrJoinCreditSubSection' not found!...");
                return borrower;
            }

            await UpdateAddAnotherCoBorrower(questionnaire.TemporaryApplicationNumber, individualOrJointCreditSubSection);
            
            var coBorrowerInvitationSubSection = profileSection.SubSections.FirstOrDefault(x => x.TemplateFieldName != null && x.TemplateFieldName.Equals(coBorrowerInvitationTemplateName));
            
            if(coBorrowerInvitationSubSection == null)
                return borrower;

            var questions = coBorrowerInvitationSubSection.QuestionSections.Where(x=>x.QuestionSectionName == "AddCoBorrower")
                               .SelectMany(x=>x.QuestionList);

            if(questions == null)
                return borrower;

            var question = questions.FirstOrDefault(x=> string.IsNullOrEmpty(x.Answer));
            var questionGuid = Guid.NewGuid().ToString();
            if(question != null)
            {
                
                // Question is already added
                borrower.QuestionId = question.QuestionId;
                question.Answer = JsonConvert.SerializeObject(borrower); 
                //Update questionGuid
                questionGuid = borrower.QuestionId.Substring(2,borrower.QuestionId.Length-2);

                //Save coBorrowerInvitationSubSection TODO
                IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                answerRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                answerRequest.SectionId = sectionId;
                answerRequest.SubSectionId = coBorrowerInvitationSubSection.SubSectionId;

                await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest,  coBorrowerInvitationSubSection);
            }
            else
            {
                //Add new question
                var newQuestionTemplate = profileSection.QuestionSectionTemplates.FirstOrDefault(x=>x.Key.Equals("coBorrowerInvitationQuestions"));
                if(newQuestionTemplate.Value == null)
                    return borrower;
                
                var newQuestion = newQuestionTemplate.Value;
                newQuestion.QuestionList[0].QuestionId = "Q-" + questionGuid;
                newQuestion.QuestionList[0].SeqNo = questions.ToList().Count;
                newQuestion.SeqNo = 2000 + questions.ToList().Count;
                newQuestion.QuestionList[0].Answer = JsonConvert.SerializeObject(borrower);
                
                newQuestion.ApplicationNumber = questionnaire.TemporaryApplicationNumber;
                newQuestion.SectionId = sectionId;
                newQuestion.SubSectionId = coBorrowerInvitationSubSection.SubSectionId;

                coBorrowerInvitationSubSection.QuestionSections.Add(newQuestion);
                await QuestionSectionsRepository.Add(newQuestion);
                borrower.QuestionId = newQuestion.QuestionList[0].QuestionId;   
            }

            var sequenceNo = 4000 + (questions.ToList().Count * 2);
            
            //Add template for co-borrower
            var coBorrowerMaritalStatusInfoTemplate = profileSection.Templates.FirstOrDefault(x=>x.Key == "coBorrowerMaritalStatusInfo");
            if(coBorrowerMaritalStatusInfoTemplate.Value != null)
            {
                var coBorrowerMaritalStatusInfoSection = coBorrowerMaritalStatusInfoTemplate.Value;
                
                coBorrowerMaritalStatusInfoSection.SubSectionId = "ID-" + questionGuid;
                coBorrowerMaritalStatusInfoSection.SeqNo = sequenceNo;
                coBorrowerMaritalStatusInfoSection.DeleteTemplates = new List<string>(){coBorrowerMaritalStatusInfoSection.SubSectionId, coBorrowerMaritalStatusInfoSection.SubSectionId + "-1", coBorrowerMaritalStatusInfoSection.SubSectionId + "-2"};
                coBorrowerMaritalStatusInfoSection.UserName = borrower.EmailAddress.ToLower();
                
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)coBorrowerMaritalStatusInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {coBorrowerMaritalStatusInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }

            var coBorrowerInfoTemplate = profileSection.Templates.FirstOrDefault(x=>x.Key == "coBorrowerInfo");
            if(coBorrowerInfoTemplate.Value != null)
            {
                var coBorrowerInfoSection = coBorrowerInfoTemplate.Value;
                
                coBorrowerInfoSection.SubSectionId = "ID-" + questionGuid + "-1";
                coBorrowerInfoSection.SeqNo = sequenceNo + 1;
                coBorrowerInfoSection.DeleteTemplates = new List<string>(){coBorrowerInfoSection.SubSectionId, coBorrowerInfoSection.SubSectionId + "-1", coBorrowerInfoSection.SubSectionId + "-2"};
                coBorrowerInfoSection.UserName = borrower.EmailAddress.ToLower();
                var questionlist = coBorrowerInfoSection.QuestionSections.SelectMany(x=>x.QuestionList);

                if(questionlist != null)
                {
                    //FirstName
                    var questionFirstName = questionlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerFirstName");
                    questionFirstName.Answer = borrower.FirstName;

                    //LastName
                    var questionLastName = questionlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerLastName");
                    questionLastName.Answer =  borrower.LastName;
                }
                
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)coBorrowerInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {coBorrowerInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }

            var coBorrowerCommunicationInfoTemplate = profileSection.Templates.FirstOrDefault(x=>x.Key == "coBorrowerCommunicationInfo");
            if(coBorrowerCommunicationInfoTemplate.Value != null)
            {
                var coBorrowerCommunicationSection = coBorrowerCommunicationInfoTemplate.Value;

                coBorrowerCommunicationSection.SubSectionId = "ID-" + questionGuid + "-2";
                coBorrowerCommunicationSection.SeqNo = sequenceNo + 2;
                coBorrowerCommunicationSection.DeleteTemplates = new List<string>(){"ID-" + questionGuid + "-1", "ID-" + questionGuid + "-2"};
                coBorrowerCommunicationSection.UserName = borrower.EmailAddress.ToLower();
                var questionCommunicationlist = coBorrowerCommunicationSection.QuestionSections.SelectMany(x=>x.QuestionList);

                if(questionCommunicationlist != null)
                {
                    //Email
                    var questionEmail = questionCommunicationlist.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerPreferredEmail");
                    questionEmail.Answer = borrower.EmailAddress.ToLower();
                }

                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)coBorrowerCommunicationSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {coBorrowerCommunicationSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }

            var coBorrowerREOInfoTemplate = profileSection.Templates.FirstOrDefault(x=>x.Key == "coBorrowerREOInfo");
            if(coBorrowerREOInfoTemplate.Value != null)
            {
                var coBorrowerREOInfoSection = coBorrowerREOInfoTemplate.Value;

                coBorrowerREOInfoSection.SubSectionId = "ID-" + questionGuid + "-3";
                coBorrowerREOInfoSection.SeqNo = sequenceNo + 11001; //Starts with 15001
                coBorrowerREOInfoSection.DeleteTemplates = new List<string>(){"ID-" + questionGuid, "ID-" + questionGuid + "-1", "ID-" + questionGuid + "-2", "ID-" + questionGuid + "-3"};
                coBorrowerREOInfoSection.UserName = borrower.EmailAddress.ToLower();
                
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)coBorrowerREOInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for Section {sectionId} SubSectionId {coBorrowerREOInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew");
            }

            return borrower;
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="coborrower"></param>
        /// <param name="questionnaire"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        private async Task<InviteHubResponse> SendEmailAndAddDynamicSectionForHub(AddCoBorrowerModal coborrower, 
                                                                                  IQuestionnaire questionnaire, 
                                                                                  int sectionId, 
                                                                                  bool allowEmail)
        {
            var temporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
            var returnResult = new InviteHubResponse();
            var status = false;
            var guidId = coborrower.QuestionId.Substring(2,coborrower.QuestionId.Length-2);
            var subSectionId = "ID-" + guidId + "-1";
            questionnaire =  await QuestionnaireRepository.GetQuestionnaire(temporaryApplicationNumber);
            var subSections = GetUserInfoAndContactInfoSubsections(questionnaire.ApplicationForm.Sections.FirstOrDefault(i=>i.SectionId==sectionId), subSectionId);
            Logger.Debug($"Calling SendEmailAndAddDynamicSectionForHub.ExecuteSendInvitationEventNewForHub {temporaryApplicationNumber}");
            var jResult = await this.ExecuteSendInvitationEventNewForHub(temporaryApplicationNumber, coborrower, allowEmail);
            if (jResult != null)
            {
                var statusResult = jResult.GetValue("status").ToString();
                if (statusResult == "success")
                {
                    status = true;
                    returnResult.status = true;
                    var inviteResult = jResult.GetValue("data").ToString();
                    returnResult.invite = JsonConvert.DeserializeObject<Invite>(inviteResult);
                }
                else
                {
                    if (jResult.GetValue("error").ToString().Contains("User already invited."))
                    {
                        //Even if it is an error, if the invitation has already been sent then Do Not consider it as an error.
                        status = true;
                        returnResult.status = false;
                        returnResult.error =  new InviteErrorResponse() {Code="400",Message="User already invited."};
                    }
                    else
                    {
                        status = false;
                        returnResult.status = false;
                        returnResult.error =  new InviteErrorResponse() {Code="400",Message="Error occured in Send invitationEvent"};
                        //throw new Exception("Error occured in Send invitationEvent");
                    }
                }
            }

            if(status == true && returnResult.invite != null)
            {
                Logger.Debug($"Calling AddSubSectionForTheApplicantNew {temporaryApplicationNumber}"); 
                await UpdateQuestionIdForInviteQuestion(temporaryApplicationNumber,questionnaire.ApplicationForm.Sections.First(a=>a.SectionId==1), coborrower, returnResult.invite.Id);
                await UpdateUserNameForInfoSections(temporaryApplicationNumber, coborrower.QuestionId, returnResult.invite.Id);
                await AssignUserNameToApplicantPersonalInfoSectionNew(temporaryApplicationNumber, coborrower, returnResult.invite.Id);

                var applicantType = ApplicantType.CoBorrower.ToString();
                if(coborrower.Relationship == "Spouse")
                {
                    applicantType = ApplicantType.Spouse.ToString();
                }
                    
                await AddSubSectionForTheApplicantNew(temporaryApplicationNumber,questionnaire.UserState, sectionId, subSections, returnResult.invite.Id, applicantType);    
            }
                
            return returnResult;
        }

        /// <summary>
        /// This function sends the invitation email to the applicant.
        /// It fetches the email address from the sub section using the subSectionId
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        private async Task<Newtonsoft.Json.Linq.JObject> ExecuteSendInvitationEventNewForHub(string applicationNumber, AddCoBorrowerModal borrower, bool allowEmail)
        {
            IBorrowerCoBorrowerInfo borrowerInfo = new BorrowerCoBorrowerInfo(){
                FirstName = borrower.FirstName,
                LastName = borrower.LastName,
                Email = borrower.EmailAddress.ToLower(),
                ApplicantType = ApplicantType.CoBorrower,
                Phone = string.Empty,
                ActualApplicantType = ApplicantType.CoBorrower                
            };

            //FreshTicket 141 : we have to pass spouse insted on coborrower as role for borrower spouse            
            if(borrower.Relationship.ToString() == CoBorowerRelationship.Spouse.ToString())
            {
                borrowerInfo.ApplicantType = ApplicantType.Spouse;
                borrowerInfo. ActualApplicantType = ApplicantType.Spouse;
            }
          
            var updateStatus = await UpdateUserProfile(applicationNumber, borrowerInfo.Email.ToLower(), borrowerInfo);

            return await this.TriggerRuleToSendEmail(applicationNumber, borrowerInfo, allowEmail);
        }

        private void UpdateInviteHubErrorResponse(InviteHubResponse inviteHubRequest,string errorCode , string errorMessage)
        {
            inviteHubRequest.status = false;
            inviteHubRequest.error = new InviteErrorResponse() {Code=errorCode,Message=errorMessage};
        }

        /// <summary>
        /// This AddNewCoBorrowerForHub method is used to create a co-borrower and will be used by HUB
        /// </summary>
        /// <param name="temporaryApplicationNumber">application number</param>
        /// <param name="borrower">borrower modal</param>
        /// <returns>InviteHubResponse</returns>
        public async Task<InviteHubResponse> AddNewCoborrowerForHub(string temporaryApplicationNumber, AddCoBorrowerModal borrower, bool allowEmail)
        {
            var returnResult = new InviteHubResponse();            
            
            Logger.Debug($"Validate AddCoBorrowerModal {temporaryApplicationNumber}"); 
            returnResult = ValidateAddCoBorrowerModal(borrower);

            if(!returnResult.status)
                return returnResult;

            Logger.Debug($"Read Questionnaire {temporaryApplicationNumber}"); 
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if(questionnaire == null || questionnaire.ApplicationForm ==null)
            {
                Logger.Debug($"Questionnaire not found {temporaryApplicationNumber}"); 
                returnResult.status = false;
                returnResult.error = new InviteErrorResponse() {Code="400",Message="Questionnaire not found"};
                return returnResult;
            }

            Logger.Debug($"ValidateDuplicates {temporaryApplicationNumber}"); 
            returnResult = ValidateDuplicates(questionnaire, borrower);
            
            if(!returnResult.status)
                return returnResult;

            
            if(borrower.Relationship.ToString() == ApplicantType.CoBorrowerSpouse.ToString())
            {
                //we have to get the "coBorrowerMaritalStatusInfo" for current coborrower user            

                var loggedInUserName = await GetCurrentUser();
                var profileSectionId = Convert.ToInt32(SectionDetails.Profile);

                if(!string.IsNullOrEmpty(loggedInUserName)){
                        var profileSection = questionnaire.ApplicationForm.Sections.FirstOrDefault(x=>x.SectionId == profileSectionId);
                        if(profileSection != null)
                        {   
         
                        var coborrowerSpouseInvitataionSubSection = profileSection.SubSections.FirstOrDefault(x => x.TemplateFieldName.ToLower() == ConstantUtils.CoBorrowerMaritalStatusInfoTemplate.ToLower()
                                && x.UserName == loggedInUserName);
                                         
                                if(coborrowerSpouseInvitataionSubSection != null)
                                {
                                    //now check emailaddress
                                     var coBorrowerMaritalStatusQuestion = coborrowerSpouseInvitataionSubSection.QuestionSections.SelectMany(x=>x.QuestionList);
                                     var SpouseEmailQuestion = coBorrowerMaritalStatusQuestion.FirstOrDefault(x=>x.QuestionFieldName == ConstantUtils.SpousePreferredEmail);
                                     if(SpouseEmailQuestion != null)
                                     {                       
                                        if(!SpouseEmailQuestion.Answer.ToLower().Equals(borrower.EmailAddress.ToLower()))
                                        {
                                            string errorMessage = "Email address is different than the one updated into coborrower profile section for spouse.";                                                                    
                                            Logger.Debug(errorMessage);                                                                
                                            UpdateInviteHubErrorResponse(returnResult,"400",errorMessage);
                                            return returnResult;
                                        }
                                      }
                                    
                                    var subsectionId = coborrowerSpouseInvitataionSubSection.SubSectionId;

                                    await AddCoBorrowerSpouseSubSectionForHub(loggedInUserName,questionnaire,profileSectionId);
                                    var status = await InviteCoborrowerSpouse(temporaryApplicationNumber, profileSectionId, subsectionId);  
                                    if(status)
                                    {
                                        //get the invited user invitation id from user-State of questionnaire.
                                        //call the get invite by invite id using rule

                                        //get the updated questionnaire usign applicationNumber
                                        var updatedQuestionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
                                        if(updatedQuestionnaire != null)
                                        {
                                            if(updatedQuestionnaire.UserState != null)
                                            {
                                                //get the invited user invite id

                                                var currentUser = updatedQuestionnaire.UserState.FirstOrDefault(x => x.UserEmailId == borrower.EmailAddress);
                                                if(currentUser != null)
                                                {
                                                    var inviteId = currentUser.InviteId;
                                                    if(!string.IsNullOrEmpty(inviteId))
                                                    {
                                                         var getInviteInfoByIdRule = QuestionnaireConfigurations.GetInviteInfoByIdRule;
                                                        if(string.IsNullOrEmpty(getInviteInfoByIdRule))
                                                            {
                                                                string errorMessage = "...getInviteInfoByIdRule rule not configured. Invitation process success but unable to get invite information.";
                                                                Logger.Debug(errorMessage);                                                                
                                                                UpdateInviteHubErrorResponse(returnResult,"400",errorMessage);
                                                                return returnResult;
                                                            }

                                                        var token = TokenParser.Parse(TokenReader.Read());
                                                        var reader = new StaticTokenReader(token.Value);
                                                        var decisionEngine = DecisionEngineFactory.Create(reader);
                                                        var jResult = await Task.Run(() => decisionEngine.Execute<dynamic, Newtonsoft.Json.Linq.JObject>(getInviteInfoByIdRule,  
                                                                    new
                                                                    {
                                                                        invitationId = inviteId
                                                                    }));
                                                        if (jResult != null)
                                                            {
                                                                var statusResult = jResult.GetValue("status").ToString();
                                                                if (statusResult == "success")
                                                                {
                                                                    status = true;
                                                                    returnResult.status = true;
                                                                    var inviteResult = jResult.GetValue("data").ToString();
                                                                    returnResult.invite = JsonConvert.DeserializeObject<Invite>(inviteResult);
                                                                }
                                                            }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        string errorMessage = "Error during CoBorrower Spouse invitation process.";
                                        Logger.Debug(errorMessage);
                                        UpdateInviteHubErrorResponse(returnResult,"400",errorMessage);
                                        return returnResult;
                                    }
                                }
                                else
                                    {
                                         string errorMessage = "CoBorrower Marital sub section(coBorrowerMaritalStatusInfo) is not found for user :" + loggedInUserName;
                                        Logger.Debug(errorMessage);
                                        UpdateInviteHubErrorResponse(returnResult,"400",errorMessage);
                                        return returnResult;
                                    }
                        }
                } 
                else
                {
                   
                   string errorMessage = "Please use the proper coborrower token to invite coborrower spouse.";
                   Logger.Debug(errorMessage);
                   UpdateInviteHubErrorResponse(returnResult,"400",errorMessage);
                   return returnResult;                                        
                }
            }
            else
            {
                Logger.Debug($"SetApplicationAsJointCredit {temporaryApplicationNumber}");     
                var result = await SetApplicationAsJointCredit(questionnaire, (borrower.Relationship.ToString() == CoBorowerRelationship.Spouse.ToString()));

                if(result==false)
                {
                   string errorMessage = "Error in adding joint credit section " + temporaryApplicationNumber;
                   Logger.Debug(errorMessage);
                   UpdateInviteHubErrorResponse(returnResult,"400",errorMessage);
                   return returnResult;
                }
                    
                // Logger.Debug($"FillBorrowerInfo {temporaryApplicationNumber}"); 
                //need to update the questionnaire as new sub section are added into setapplicationasjointcredit
                questionnaire =  await QuestionnaireRepository.GetQuestionnaire(questionnaire.TemporaryApplicationNumber);

                var coBorrower = await FillBorrowerInfo(questionnaire, borrower);
                
                // Logger.Debug($"Starting AssignUserNameToApplicantPersonalInfoSectionNew {temporaryApplicationNumber}");
                // questionnaire = await GetQuestionnaire(temporaryApplicationNumber); //refresh questionnaire
                // List<AddCoBorrowerModal> param = new List<AddCoBorrowerModal>();
                // param.Add(coBorrower);
                // var userStateStatus = await AssignUserNameToApplicantPersonalInfoSectionNew(questionnaire, param);

                // Send Email Invite & Add Dynamic sections
                Logger.Debug($"Starting RemoveDisabledSectionFromConfiguration {temporaryApplicationNumber}");
                Logger.Debug($"Starting SendEmailAndDynamicSection for {borrower.EmailAddress}");
                returnResult = await SendEmailAndAddDynamicSectionForHub(coBorrower, questionnaire, 1, allowEmail);
                Logger.Debug($"Completed SendEmailAndDynamicSection for {borrower.EmailAddress}");
            }

            return returnResult;
        }

        private async Task  AddCoBorrowerSpouseSubSectionForHub(string currentUserName,IQuestionnaire questionnaire,int sectionId)
        {
            //

            

           var subSections = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == sectionId)
                                .SelectMany(s => s.SubSections)
                                .Where(ss => (ss.TemplateId == ConstantUtils.CoBorrowerSpouseInfoSubSection 
                                || ss.TemplateId == ConstantUtils.CoBorrowerSpouseCommunicationInfoSubSection
                                || ss.TemplateId == ConstantUtils.CoBorrowerSpouseReoInfoSubSection
                                ) 
                                    && ss.UserName == currentUserName).ToList();

             if(subSections != null)
            {
                if(subSections.Count > 0) 
                {
                    //section already added so no need to add again.
                        return;
                }
            }
            var sequenceNo = 4010;
            var questionGuid = Guid.NewGuid().ToString();
            var section = questionnaire.ApplicationForm.Sections.FirstOrDefault(x=>x.SectionId == sectionId);
            //Add template for spouse
            //"coBorrowerSpouseInfo" || ss.TemplateId == "coBorrowerSpouseCommunicationInfo"
            Logger.Debug("Adding coBorrowerSpouseInfo subsection...");
            var spouseInfo = section.Templates.FirstOrDefault(x=>x.Key == ConstantUtils.CoBorrowerSpouseInfoSubSection);
            if(spouseInfo.Value != null)
            {
                var spouseInfoSection = spouseInfo.Value;
                spouseInfoSection.SubSectionId = "ID-" + questionGuid;
                spouseInfoSection.SeqNo = sequenceNo;
                spouseInfoSection.DeleteTemplates = new List<string>(){spouseInfoSection.SubSectionId , spouseInfoSection.SubSectionId + "-1", spouseInfoSection.SubSectionId + "-2" };
                //Update UserName in SubSection 
                spouseInfoSection.UserName = currentUserName;
               
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)spouseInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for CoBorrower Spouse info Section {sectionId} SubSectionId {spouseInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew CoBorrower Spouse info");
            }
                
            Logger.Debug("Adding coBorrowerSpouseCommunicationInfo subsection...");
            var spouseCommunicationInfo = section.Templates.FirstOrDefault(x=>x.Key == ConstantUtils.CoBorrowerSpouseCommunicationInfoSubSection);
            if(spouseCommunicationInfo.Value != null)
            {
                var spouseCommunicationSection = spouseCommunicationInfo.Value;
                spouseCommunicationSection.SubSectionId = "ID-" + questionGuid + "-1";
                spouseCommunicationSection.SeqNo = sequenceNo + 1; 
                spouseCommunicationSection.DeleteTemplates = new List<string>(){"ID-" + questionGuid , spouseCommunicationSection.SubSectionId, $"ID-{questionGuid}-2" };
                spouseCommunicationSection.UserName = currentUserName;
                               
                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)spouseCommunicationSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for CoBorrower spouse communication info Section {sectionId} SubSectionId {spouseCommunicationSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew CoBorrower spouse communication info");
            }

            Logger.Debug("Adding coBorrowerSpouseReoInfo subsection...");
            var spouseReoInfo = section.Templates.FirstOrDefault(x => x.Key == ConstantUtils.CoBorrowerSpouseReoInfoSubSection);
            if (spouseReoInfo.Value != null)
            {
                var spouseReoInfoSection = spouseReoInfo.Value;
                spouseReoInfoSection.SubSectionId = "ID-" + questionGuid + "-2";
                spouseReoInfoSection.SeqNo = 20000;
                spouseReoInfoSection.DeleteTemplates = new List<string>() { $"ID-{questionGuid}", $"{spouseReoInfoSection.SubSectionId }-1", $"ID-{questionGuid}-2" };
                //Update UserName in SubSection 
                spouseReoInfoSection.UserName = currentUserName;

                //Save Templates in database
                var addSubSectionRequest = new AddSubSectionRequest();
                addSubSectionRequest.SubSection = (SubSection)spouseReoInfoSection;
                addSubSectionRequest.SectionId = sectionId;
                addSubSectionRequest.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
                Logger.Debug($"Calling AddSubSectionNew for CoBorrower Spouse REO info Section {sectionId} SubSectionId {spouseReoInfoSection.SubSectionId}");
                var status = await AddSubSectionNew(questionnaire, addSubSectionRequest);
                Logger.Debug($"Completed AddSubSectionNew CoBorrower Spouse REO info");
            }

        }
    }
}