﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Security.Tokens;
using Docitt.Questionnaire.Abstractions;

namespace Docitt.Questionnaire
{
    public class QuestionnaireDataService : IQuestionnaireDataService
    {
        public QuestionnaireDataService(ILogger logger,
            QuestionnaireConfiguration configuration,
            IQuestionnaireRepository questionnaireRepository, ITokenReader tokenReader, ITokenHandler tokenParser,
            IDecisionEngineService decisionEngineService)
        {
            Log = logger ?? throw new ArgumentException($"{nameof(logger)} is mandatory");
            Configuration = configuration ?? throw new ArgumentException($"{nameof(configuration)} is mandatory");
            QuestionnaireRepository = questionnaireRepository ??
                                      throw new ArgumentException($"{nameof(questionnaireRepository)} is mandatory");
            TokenReader = tokenReader ?? throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            TokenParser = tokenParser ?? throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            DecisionEngineService = decisionEngineService ??
                                    throw new ArgumentException($"{nameof(decisionEngineService)} is mandatory");
        }

        #region "Private Property"

        /// <summary>
        /// Decision Engine Service
        /// </summary>
        private IDecisionEngineService DecisionEngineService { get; }

        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }

        /// <summary>
        /// Logger
        /// </summary>
        private ILogger Log { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private QuestionnaireConfiguration Configuration { get; }


        private IQuestionnaireRepository QuestionnaireRepository { get; }

        #endregion "Private Property"

        #region "Private Method"

        /// <summary>
        /// Get question list for subsection for section
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="sectionId">The section id</param>
        /// <param name="userName">The user name</param>
        /// <returns>List of question with key value pair</returns>
        private async Task<Dictionary<string, object>> GetQuestionListForSubSectionForSection(string applicationId,
            int sectionId, string userName)
        {
            //var sectionData = await QuestionnaireSection.GetRawSection(applicationId, sectionId);
            var sectionData = await QuestionnaireRepository.GetSectionById(applicationId, sectionId);
            var questionListTo = new Dictionary<string, dynamic>();
            var borrowerReoInfoData = new List<Dictionary<string, dynamic>>();

            var borrowerAdditionalAddressData = new List<Dictionary<string, dynamic>>();

            var isBorrowerReo = false;
            if (sectionData != null)
            {
                Log.Debug($"Processing sectionId '{sectionData.SectionId}' of '{applicationId}'... ");

                var questionSubSection = sectionData.SubSections
                    .Where(s => s.UserName == userName && s.QuestionSections != null).ToList();
                var loanPurposeQuestion = questionSubSection.SelectMany(x => x.QuestionSections)
                    .SelectMany(x => x.QuestionList)
                    .FirstOrDefault(x => x.QuestionFieldName == ConstantUtils.BorrowerTransactionType);
                var jointCreditQuestion = questionSubSection.SelectMany(x => x.QuestionSections)
                    .SelectMany(x => x.QuestionList)
                    .FirstOrDefault(x => x.QuestionFieldName == ConstantUtils.BorrowerIndividualOrJointCredit);
                var coborrowerMaritalStatus = questionSubSection
                    .Where(x => x.TemplateFieldName == ConstantUtils.CoBorrowerMaritalStatusInfoTemplate)
                    .SelectMany(x => x.QuestionSections).SelectMany(x => x.QuestionList)
                    .FirstOrDefault(x => x.QuestionFieldName == ConstantUtils.BorrowerMaritalStatus);
                var marriedSpouseQuestionList = Configuration.SpouseInfoQuestionList;
                foreach (var qSubSection in questionSubSection)
                {
                    if (!string.IsNullOrEmpty(qSubSection.SubSectionId))
                        Log.Debug($"Processing subsectionId '{qSubSection.SubSectionId}'...");

                    var questionListFrom = GetQuestionList(qSubSection);

                    var originalDataKeys = questionListFrom.Keys.ToList();
                    if (loanPurposeQuestion != null && loanPurposeQuestion.Answer == ConstantUtils.RefinanceWorkflow)
                    {
                        RemoveDuplicateIdsRefinanceAddressFeatures(questionListFrom, originalDataKeys);
                    }
                    else
                    {
                        if (originalDataKeys.Any(x => x == ConstantUtils.BorrowerCurrentAddressSame))
                        {
                            questionListFrom.Remove(ConstantUtils.BorrowerCurrentAddressSame);

                            foreach (var key in originalDataKeys)
                            {
                                if (key.Contains(ConstantUtils.PostFixCheckBoxFalse) ||
                                    key.Contains(ConstantUtils.PostFixCheckBoxTrue))
                                {
                                    questionListFrom.Remove(key);
                                }
                            }
                        }
                    }

                    //// Remove Duplicate QuestionIds
                    if (jointCreditQuestion != null && jointCreditQuestion.Answer == ConstantUtils.JointCredit)
                    {
                        RemoveJointCreditMarriedFeatures(questionListFrom, originalDataKeys, marriedSpouseQuestionList);
                    }
                    else if (jointCreditQuestion != null &&
                             jointCreditQuestion.Answer == ConstantUtils.IndividualCredit)
                    {
                        RemoveIndividualFeatures(questionListFrom, originalDataKeys, marriedSpouseQuestionList);
                    }

                    if (coborrowerMaritalStatus != null &&
                        (coborrowerMaritalStatus.Answer == ConstantUtils.Unmarried ||
                         coborrowerMaritalStatus.Answer == ConstantUtils.Separated))
                    {
                        if (originalDataKeys.Any(x => x == ConstantUtils.BorrowerMaritalStatus))
                        {
                            RemoveChildQuestionsForMarried(questionListFrom);

                            foreach (var key in marriedSpouseQuestionList)
                            {
                                questionListFrom.Remove(key);
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(qSubSection.TemplateFieldName) &&
                        (string.Equals(qSubSection.TemplateFieldName, ConstantUtils.BorrowerReoInfoKey, StringComparison.CurrentCultureIgnoreCase) ||
                         string.Equals(qSubSection.TemplateFieldName, ConstantUtils.CoBorrowerReoInfoKey, StringComparison.CurrentCultureIgnoreCase))
                    )
                    {
                        if (questionListFrom.Any(x => x.Key == ConstantUtils.Calculator))
                        {
                            questionListFrom.Remove(ConstantUtils.Calculator);
                        }

                        if (!isBorrowerReo)
                            isBorrowerReo = AddDoYouHaveAdditionalProperty(questionListFrom, questionListTo);

                        if (isBorrowerReo)
                            borrowerReoInfoData.Add(questionListFrom);
                    }
                    else if (!string.IsNullOrWhiteSpace(qSubSection.TemplateFieldName) &&
                             qSubSection.TemplateFieldName.ToLower() ==
                             ConstantUtils.AdditionalResidenceTemplate.ToLower())
                    {
                        borrowerAdditionalAddressData.Add(questionListFrom);
                    }
                    else
                    {
                        try
                        {
                            questionListFrom.ToList().ForEach(x => questionListTo.Add(x.Key, x.Value));
                        }
                        catch (Exception ex)
                        {
                            Log.Warn(
                                $"Error in GetQuestionListForSubSectionForSection while adding to questionListTo: {ex.Message}");
                            Log.Error(ex.Message, ex);
                        }
                    }
                }

                if (borrowerReoInfoData.Any())
                    questionListTo.Add(ConstantUtils.BorrowerReoInfoKey, borrowerReoInfoData);

                if (borrowerAdditionalAddressData.Any())
                    questionListTo.Add(ConstantUtils.BorrowerAdditionalAddressList, borrowerAdditionalAddressData);
            }

            return questionListTo;
        }

        private void RemoveIndividualFeatures(Dictionary<string, dynamic> questionListFrom,
            List<string> originalDataKeys, string[] marriedSpouseQuestionList)
        {
            if (questionListFrom.Any(x => x.Key == ConstantUtils.BorrowerMaritalStatus))
            {
                var borrowerMarriedStatus =
                    questionListFrom.FirstOrDefault(x => x.Key == ConstantUtils.BorrowerMaritalStatus);

                if (borrowerMarriedStatus.Value == ConstantUtils.Unmarried)
                {
                    foreach (var key in marriedSpouseQuestionList)
                    {
                        questionListFrom.Remove(key);
                    }
                }

                foreach (var key in originalDataKeys)
                {
                    if (key.Contains(ConstantUtils.PostFixMarried))
                    {
                        questionListFrom.Remove(key);
                    }
                }

                questionListFrom.Remove(ConstantUtils.BorrowerAddSpouseAsCoBorrower);
                questionListFrom.Remove(ConstantUtils.BorrowerAddAsNewCoBorrower);

                RemoveChildQuestionsForMarried(questionListFrom);
            }
        }

        private void RemoveDuplicateIdsRefinanceAddressFeatures(Dictionary<string, dynamic> questionListFrom,
            List<string> originalDataKeys)
        {
            if (questionListFrom.Any(x => x.Key == ConstantUtils.BorrowerCurrentAddressSame))
            {
                var refinanceAddressSameList = Configuration.RefinanceAddressSameList;
                var borrowerCurrentAddressSame =
                    questionListFrom.FirstOrDefault(x => x.Key == ConstantUtils.BorrowerCurrentAddressSame);
                if (borrowerCurrentAddressSame.Value == "true")
                {
                    foreach (var key in originalDataKeys)
                    {
                        if (key.Contains(ConstantUtils.PostFixCheckBoxFalse))
                        {
                            questionListFrom.Remove(key);
                        }

                        foreach (var item in refinanceAddressSameList)
                        {
                            if (item + ConstantUtils.PostFixCheckBoxTrue == key)
                            {
                                questionListFrom[item] = questionListFrom[key];
                                questionListFrom.Remove(key);
                            }
                        }
                    }
                }
                else
                {
                    foreach (var key in originalDataKeys)
                    {
                        if (key.Contains(ConstantUtils.PostFixCheckBoxTrue))
                        {
                            questionListFrom.Remove(key);
                        }

                        foreach (var item in refinanceAddressSameList)
                        {
                            if (item + ConstantUtils.PostFixCheckBoxFalse == key)
                            {
                                questionListFrom[item] = questionListFrom[key];
                                questionListFrom.Remove(key);
                            }
                        }
                    }
                }
            }
        }

        private void RemoveJointCreditMarriedFeatures(Dictionary<string, dynamic> questionListFrom,
            List<string> originalDataKeys, string[] marriedSpouseQuestionList)
        {
            if (questionListFrom.Any(x => x.Key == ConstantUtils.BorrowerMaritalStatusMarried))
            {
                var borrowerMarriedStatus =
                    questionListFrom.FirstOrDefault(x => x.Key == ConstantUtils.BorrowerMaritalStatusMarried);
                if (borrowerMarriedStatus.Value == ConstantUtils.Married ||
                    borrowerMarriedStatus.Value == ConstantUtils.Separated)
                {
                    foreach (var key in originalDataKeys)
                    {
                        foreach (var item in marriedSpouseQuestionList)
                        {
                            if (item + ConstantUtils.PostFixMarried == key)
                            {
                                questionListFrom[item] = questionListFrom[key];
                                questionListFrom.Remove(key);
                            }
                        }
                    }

                    if (borrowerMarriedStatus.Value == ConstantUtils.Separated)
                    {
                        RemoveChildQuestionsForMarried(questionListFrom);
                        //questionListFrom.Remove(ConstantUtils.BorrowerAddAsNewCoBorrower);
                    }

                    questionListFrom[ConstantUtils.BorrowerMaritalStatus] =
                        questionListFrom[ConstantUtils.BorrowerMaritalStatusMarried];
                    questionListFrom.Remove(ConstantUtils.BorrowerMaritalStatusMarried);
                }
                else
                {
                    questionListFrom[ConstantUtils.BorrowerMaritalStatus] =
                        questionListFrom[ConstantUtils.BorrowerMaritalStatusMarried];
                    RemoveChildQuestionsForMarried(questionListFrom);
                    foreach (var key in originalDataKeys)
                    {
                        if (key.Contains(ConstantUtils.PostFixMarried))
                        {
                            questionListFrom.Remove(key);
                        }
                    }

                    foreach (var key in marriedSpouseQuestionList)
                    {
                        questionListFrom.Remove(key);
                    }

                    questionListFrom.Remove(ConstantUtils.BorrowerAddSpouseAsCoBorrower);
                }
            }
        }


        private void RemoveChildQuestionsForMarried(Dictionary<string, dynamic> questionListFrom)
        {
            questionListFrom.Remove(ConstantUtils.SpouseBirthDate);
            questionListFrom.Remove(ConstantUtils.SpouseSSN);
            questionListFrom.Remove(ConstantUtils.SpousePreferredEmail);
            questionListFrom.Remove(ConstantUtils.SpouseCellPhone);
        }

        private bool AddDoYouHaveAdditionalProperty(Dictionary<string, dynamic> questionListFrom,
            Dictionary<string, dynamic> questionListTo)
        {
            var hasBorrowerReo = false;
            // To check Do you currently own any properties?
            var isAdditionalProperties = questionListFrom.FirstOrDefault(p => p.Key == ConstantUtils.BorrowerReoKey);
            if (!string.IsNullOrWhiteSpace(isAdditionalProperties.Key))
            {
                bool.TryParse(isAdditionalProperties.Value, out hasBorrowerReo);
                questionListFrom.Remove(isAdditionalProperties.Key);
                questionListTo.Add(isAdditionalProperties.Key, isAdditionalProperties.Value);
            }

            return hasBorrowerReo;
        }


        private IEnumerable<IQuestion> ExcludeBorrowerPropertyUse(ISubSection qSubSection,
            IEnumerable<IQuestion> questionList)
        {
            if (qSubSection != null &&
                qSubSection.SubSectionId != null &&
                qSubSection.SubSectionId == "SS2" &&
                questionList != null)
            {
                var propertySelected = questionList
                    .FirstOrDefault(x => x.QuestionFieldName.Equals("BorrowerSelectedProperty"));

                if (propertySelected != null && propertySelected.Answer == "false")
                {
                    //exclude property use with question id 12
                    questionList = questionList.Where(x => !(x.QuestionFieldName.Equals("BorrowerPropertyUse") &&
                                                             x.QuestionId == "12"));
                }
            }

            return questionList;
        }

        #region "Income section"

        /// <summary>
        /// Get the question list from Sub Section
        /// </summary>
        /// <param name="qSubSection">Sub section object</param>
        /// <returns>Key value pair</returns>
        private Dictionary<string, dynamic> GetQuestionList(ISubSection qSubSection)
        {
            Dictionary<string, dynamic> objQuestionData = new Dictionary<string, dynamic>();
            IEnumerable<IQuestion> questionList = null;

            questionList = qSubSection.QuestionSections.Where(x => x.QuestionList != null)
                .SelectMany(qs => qs.QuestionList).ToList();

            questionList = ExcludeBorrowerPropertyUse(qSubSection, questionList);

            if (questionList != null)
            {
                var questionListUnique = (from qs in questionList
                        where (qs.QuestionFieldName != null &&
                               qs.QuestionFieldName.Length > 0 & qs.QuestionType != QuestionType.Null &&
                               qs.QuestionType != QuestionType.Label)
                        select new
 {
                            selectedField = qs.QuestionFieldName, Answer = qs.Answer, fieldType = qs.QuestionType
                        })
                    .ToList();
                string answer;

                foreach (var data in questionListUnique)
                {
                    if (!objQuestionData.ContainsKey(data.selectedField))
                    {
                        if (data.fieldType == QuestionType.LoanCalculator)
                        {
                            if (!string.IsNullOrEmpty(data.Answer))
                            {
                                dynamic obj = JsonConvert.DeserializeObject(data.Answer);
                                dynamic objLoandetail = new ExpandoObject();
                                objLoandetail.PurchasePrice = obj.purchasePrice;
                                objLoandetail.DownPayment = obj.downPayment;
                                objLoandetail.LoanPercentage = obj.loanPercentage;
                                objLoandetail.LoanAmount = obj.loanAmount;
                                objQuestionData.Add(data.selectedField, objLoandetail);
                            }
                        }
                        else if (data.fieldType == QuestionType.AddCoBorrower)
                        {
                            if (!string.IsNullOrEmpty(data.Answer))
                            {
                                dynamic obj = JsonConvert.DeserializeObject(data.Answer);
                                dynamic objCoBorrower = new ExpandoObject();
                                objCoBorrower.FirstName = obj.firstName;
                                objCoBorrower.LastName = obj.lastName;
                                objCoBorrower.Relationship = obj.relationship;
                                objCoBorrower.Email = obj.email;

                                if (objQuestionData.Count > 0)
                                {
                                    var coborrowerlist = objQuestionData[ConstantUtils.AddCoBorrower];
                                    coborrowerlist.Add(objCoBorrower);
                                }
                                else
                                {
                                    var coborrowerlist = new List<dynamic>();
                                    coborrowerlist.Add(objCoBorrower);
                                    objQuestionData.Add(ConstantUtils.AddCoBorrower, coborrowerlist);
                                }
                            }
                        }

                        else if (data.fieldType == QuestionType.BusinessAddress)
                        {
                            if (!string.IsNullOrEmpty(data.Answer))
                            {
                                dynamic obj = JsonConvert.DeserializeObject(data.Answer);

                                dynamic objLoandetail = new ExpandoObject();
                                objLoandetail.Address = obj.address;
                                objLoandetail.Address2 = obj.addressLine2;
                                objLoandetail.AddressCity = obj.city;
                                objLoandetail.AddressState = obj.state;
                                objLoandetail.AddressZip = obj.zip;

                                objQuestionData.Add(data.selectedField, objLoandetail);
                            }
                        }
                        else if (data.fieldType == QuestionType.MultiToggle)
                        {
                            if (!string.IsNullOrEmpty(data.Answer))
                            {
                                dynamic obj = JsonConvert.DeserializeObject(data.Answer);

                                if (obj != null && obj.Count > 0)
                                {
                                    List<string> objCom = new List<string>();

                                    foreach (var item in obj)
                                    {
                                        try
                                        {
                                            // CorrectElements
                                            objCom.Add(item.Value);
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Error("Error while parsing MultiToggle field " + data.selectedField +
                                                      " , message " + ex.Message);
                                        }
                                    }

                                    objQuestionData.Add(data.selectedField, objCom);
                                }
                            }
                        }
                        else if (data.fieldType == QuestionType.LoanOfficer)
                        {
                            if (!string.IsNullOrEmpty(data.Answer))
                            {
                                dynamic obj = JsonConvert.DeserializeObject(data.Answer);
                                string loanofficer = obj.loanOfficerName;
                                objQuestionData.Add(data.selectedField, loanofficer);
                            }
                        }
                        else if (data.fieldType == QuestionType.TreeView)
                        {
                            try
                            {
                                var treeViewTextBoxControlList = Configuration.TreeViewTextBoxControl;
                                JObject x = Newtonsoft.Json.JsonConvert.DeserializeObject(data.Answer) as JObject;
                                List<string> objCom = new List<string>();
                                Dictionary<string, string> txtObject = new Dictionary<string, string>();
                                if (x != null)
                                {
                                    //objQuestionData.Add(((Newtonsoft.Json.Linq.JProperty)x.First).Name, ((Newtonsoft.Json.Linq.JProperty)x.First).Value);
                                    if (treeViewTextBoxControlList.Contains(((Newtonsoft.Json.Linq.JProperty) x.First)
                                        .Name))
                                    {
                                        txtObject.Add(((Newtonsoft.Json.Linq.JProperty) x.First).Name,
                                            ((Newtonsoft.Json.Linq.JProperty) x.First).Value.ToString());
                                    }
                                    else
                                    {
                                        objCom.Add(((Newtonsoft.Json.Linq.JProperty) x.First).Value.ToString());
                                        AddChildAnswer(x, objCom, treeViewTextBoxControlList, txtObject);
                                    }
                                }

                                objQuestionData.Add(data.selectedField, objCom);

                                foreach (var item in txtObject)
                                {
                                    objQuestionData.Add(item.Key, item.Value);
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Warn($"Data corrupt for {data.fieldType}. Error :" + ex.Message);
                            }
                        }
                        else if (data.selectedField == Constant.BorrowerIncomeAlimonySupport)
                        {
                            var obj = JsonConvert.DeserializeObject<Dictionary<string, string>>(data.Answer);
                            long objAmount = 0;
                            string objStartDate = string.Empty;
                            if (obj.ContainsKey(Constant.AlimonyAmount))
                            {
                                var amount = obj[Constant.AlimonyAmount];
                                if (!string.IsNullOrEmpty(amount))
                                {
                                    objAmount = Convert.ToInt64(amount);
                                }
                            }

                            if (obj.ContainsKey(Constant.AlimonyStartDate))
                            {
                                var startDateVAlue = obj[Constant.AlimonyStartDate];
                                if (!string.IsNullOrEmpty(startDateVAlue))
                                {
                                    objStartDate = startDateVAlue;
                                }
                            }

                            objQuestionData.Add(Constant.BorrowerIncomeAlimonySupportAmount, objAmount);
                            objQuestionData.Add(Constant.BorrowerIncomeAlimonySupportStartDate, objStartDate);
                        }
                        else if (data.fieldType == QuestionType.RadioTreeView)
                        {
                            var treeViewTextBoxControlList = Configuration.TreeViewTextBoxControl;
                            JObject x = Newtonsoft.Json.JsonConvert.DeserializeObject(data.Answer) as JObject;
                            List<string> objCom = new List<string>();
                            if (x != null)
                            {
                                objCom.Add(((Newtonsoft.Json.Linq.JProperty) x.First).Value.ToString());

                                objQuestionData.Add(data.selectedField, objCom);

                                var answerString =
                                    JsonConvert.DeserializeObject<Dictionary<string, string>>(data.Answer);

                                foreach (var item in answerString)
                                {
                                    if (treeViewTextBoxControlList.Contains(item.Key))
                                    {
                                        var textboxValue = !string.IsNullOrWhiteSpace(item.Value)
                                            ? ($"{item.Value.Substring(0, 2)}/{item.Value.Substring(2, 4)}")
                                            : null;
                                        objQuestionData.Add(item.Key, textboxValue);
                                    }
                                }
                            }
                        }
                        else if (data.selectedField.ToLower() ==
                            ConstantUtils.BorrowerAlimonyChildName || data.selectedField.ToLower() ==
                                                                   ConstantUtils.BorrowerAlimonyChildDOB
                                                                   || data.selectedField.ToLower() ==
                                                                   ConstantUtils.BorrowerAlimonyChildSupportName ||
                                                                   data.selectedField.ToLower() ==
                                                                   ConstantUtils.BorrowerAlimonyChildSupportDOB
                        )
                        {
                            var childlist = new List<dynamic>();

                            if (objQuestionData.Count > 0 &&
                                objQuestionData.ContainsKey(ConstantUtils.BorrowerAlimonyChildKey))
                            {
                                childlist = objQuestionData[ConstantUtils.BorrowerAlimonyChildKey];

                                if (data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildDOB ||
                                    data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildSupportDOB)
                                {
                                    var objChildDetails = childlist[childlist.Count - 1];
                                    objChildDetails.BorrowerAlimonyChildDOB = data.Answer;
                                }
                                else if (data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildName ||
                                         data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildSupportName)
                                {
                                    dynamic objChildDetails = new ExpandoObject();
                                    objChildDetails.BorrowerAlimonyChildName = data.Answer;
                                    childlist.Add(objChildDetails);
                                }

                                objQuestionData[ConstantUtils.BorrowerAlimonyChildKey] = childlist;
                            }
                            else
                            {
                                dynamic objChildDetails = new ExpandoObject();
                                if (data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildName ||
                                    data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildSupportName)
                                    objChildDetails.BorrowerAlimonyChildName = data.Answer;
                                else if (data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildDOB ||
                                         data.selectedField.ToLower() == ConstantUtils.BorrowerAlimonyChildSupportDOB)
                                    objChildDetails.BorrowerAlimonyChildDOB = data.Answer;

                                childlist.Add(objChildDetails);
                                objQuestionData.Add(ConstantUtils.BorrowerAlimonyChildKey, childlist);
                            }
                        }
                        else
                        {
                            if (data.Answer == null)
                                answer = string.Empty;
                            else if (data.Answer.Contains("\""))
                                answer = data.Answer.Replace("\"", "'");
                            else
                                answer = data.Answer;

                            objQuestionData.Add(data.selectedField, answer);
                        }
                    }
                }
            }

            return objQuestionData;
        }

        private void AddChildAnswer(JObject parent, List<string> objCom, string[] treeViewTextBoxControlList,
            Dictionary<string, string> txtObject)
        {
            JArray child = parent.GetValue("child") as JArray;
            if (child != null)
            {
                foreach (JObject c in child)
                {
                    //Console.WriteLine(c.GetValue("EnumBorrowerHispanicOrLatinoTypeMX"));
                    //Console.WriteLine(((Newtonsoft.Json.Linq.JProperty)c.First).Name);
                    //Console.WriteLine(((Newtonsoft.Json.Linq.JProperty)c.First).Value);
                    //objQuestionData.Add(((Newtonsoft.Json.Linq.JProperty)c.First).Name, ((Newtonsoft.Json.Linq.JProperty)c.First).Value);
                    if (treeViewTextBoxControlList.Contains(((Newtonsoft.Json.Linq.JProperty) c.First).Name))
                    {
                        txtObject.Add(((Newtonsoft.Json.Linq.JProperty) c.First).Name,
                            ((Newtonsoft.Json.Linq.JProperty) c.First).Value.ToString());
                    }
                    else
                    {
                        var childAnswer = ((Newtonsoft.Json.Linq.JProperty) c.First).Value.ToString();
                        if (!string.IsNullOrEmpty(childAnswer))
                        {
                            objCom.Add(childAnswer);
                        }

                        AddChildAnswer(c, objCom, treeViewTextBoxControlList, txtObject);
                    }
                }
            }
        }

        /// <summary>
        ///  Get the list of question for subsection
        /// </summary>
        /// <param name="objQuestionDataList">Question data list object</param>
        /// <param name="objSubSection">Questionnaire sub section object</param>
        /// <param name="employmentSectionName">Sub section name</param>
        /// <param name="employmentContactSectionName">Dependent sub section name</param>
        /// <param name="isCurrentEmployment">is current employer(true/false)</param>
        private void GetEmploymentInfo(List<Dictionary<string, dynamic>> objQuestionDataList,
            List<ISubSection> objSubSection, string employmentSectionName, string employmentContactSectionName,
            string employmentContactKey, bool isCurrentEmployment = false)
        {
            //get current employer
            Dictionary<string, dynamic> questionList = null;
            var questionSubSectionList = objSubSection.Where(x =>
                    x.TemplateFieldName != null && x.TemplateFieldName.ToLower() == employmentSectionName.ToLower())
                .ToList();

            if (questionSubSectionList != null)
            {
                foreach (var qSubSection in questionSubSectionList)
                {
                    questionList = GetQuestionList(qSubSection);

                    StartEndDateQuestionTypeHandling(questionList, ConstantUtils.Employment);

                    //get current employment location or contact section

                    GetDependnetQuestionInfo(questionList, objSubSection, qSubSection, employmentContactSectionName,
                        employmentContactKey);

                    #region "Old region"

                    /*
                    var questionSubSectionContactList = objSubSection.Where(x => x.SeqNo == qSubSection.SeqNo + 1 && x.TemplateFieldName != null && x.TemplateFieldName.ToLower() == employmentContactSectionName.ToLower()).ToList();
                    if (questionSubSectionContactList != null)
                    {
                        foreach (var qSubContactSection in questionSubSectionContactList)
                        {
                            var questionContactList = GetQuestionList(qSubContactSection);
                            //questionList.Add("BorrowerEmployerContact", questionContactList);
                            questionList.Add(employmentContactKey, questionContactList);
                        }
                    }
                    */

                    #endregion "Old region"

                    objQuestionDataList.Add(questionList);
                    questionList = null;
                }
            }
        }

        /// <summary>
        ///  Get the list of question for subsection
        /// </summary>
        /// <param name="objQuestionDataList">Question data list object</param>
        /// <param name="objSubSection">Questionnaire sub section object</param>
        /// <param name="businessSectionName">Sub section name</param>
        /// <param name="businessdependentSectionName">Dependent sub section name</param>
        /// <param name="dependentKeyName">dependent sub section key name</param>
        private void GetBusinessInfo(List<Dictionary<string, dynamic>> objQuestionDataList,
            List<ISubSection> objSubSection, string businessSectionName, string businessdependentSectionName,
            string dependentKeyName)
        {
            //get current employer
            Dictionary<string, dynamic> questionList = null;
            var questionSubSectionList = objSubSection.Where(x =>
                x.TemplateFieldName != null && x.TemplateFieldName.ToLower() == businessSectionName.ToLower()).ToList();

            if (questionSubSectionList != null)
            {
                foreach (var qSubSection in questionSubSectionList)
                {
                    questionList = GetQuestionList(qSubSection);

                    StartEndDateQuestionTypeHandling(questionList, ConstantUtils.Business);

                    if (questionList.Any(x => x.Key == ConstantUtils.Calculator))
                    {
                        questionList.Remove(ConstantUtils.Calculator);
                    }

                    GetDependnetQuestionInfo(questionList, objSubSection, qSubSection, businessdependentSectionName,
                        dependentKeyName);

                    objQuestionDataList.Add(questionList);
                    questionList = null;
                }
            }
        }

        private void StartEndDateQuestionTypeHandling(Dictionary<string, dynamic> questionList, string sectionName)
        {
            var currentlyWorkHereAnswer =
                questionList.FirstOrDefault(x => x.Key == ConstantUtils.BorrowerCurrentlyWork).Value;
            var startEndDateTypeAnswer = string.Empty;
            if (!string.IsNullOrWhiteSpace(currentlyWorkHereAnswer)) //&& currentlyWorkHereAnswer.ToLower() == "false")
            {
                switch (sectionName)
                {
                    case ConstantUtils.Employment:
                        startEndDateTypeAnswer = questionList[ConstantUtils.BorrowerEmployerEndDateFalse];
                        if (!string.IsNullOrWhiteSpace(startEndDateTypeAnswer))
                        {
                            var startEndDateObject =
                                JsonConvert.DeserializeObject<StartEndDate>(startEndDateTypeAnswer);
                            ;
                            questionList[ConstantUtils.BorrowerEmployerStartDate] = startEndDateObject != null
                                ? startEndDateObject.StartDate
                                : string.Empty;
                            questionList[ConstantUtils.BorrowerEmployerEndDate] = startEndDateObject != null
                                ? startEndDateObject.EndDate
                                : string.Empty;
                        }

                        questionList.Remove(ConstantUtils.BorrowerEmployerEndDateFalse);
                        break;
                    case ConstantUtils.Business:
                        startEndDateTypeAnswer = questionList[ConstantUtils.BorrowerBusinessEndDateFalse];
                        if (!string.IsNullOrWhiteSpace(startEndDateTypeAnswer))
                        {
                            var startEndDateObject =
                                JsonConvert.DeserializeObject<StartEndDate>(startEndDateTypeAnswer);
                            ;
                            questionList[ConstantUtils.BorrowerBusinessStartDate] = startEndDateObject != null
                                ? startEndDateObject.StartDate
                                : string.Empty;
                            questionList[ConstantUtils.BorrowerBusinessEndDate] = startEndDateObject != null
                                ? startEndDateObject.EndDate
                                : string.Empty;
                        }

                        questionList.Remove(ConstantUtils.BorrowerBusinessEndDateFalse);
                        break;
                }
            }
        }

        /// <summary>
        /// Process the dependent section question list
        /// </summary>
        /// <param name="questionList">Questionnaire list</param>
        /// <param name="objSubSection">List of Questionnaire sub section object</param>
        /// <param name="qSubSection">Questionnaire sub section</param>
        /// <param name="businessdependentSectionName">Dependent sub section name</param>
        /// <param name="dependentKeyName">dependent sub section key name</param>
        private void GetDependnetQuestionInfo(Dictionary<string, dynamic> questionList, List<ISubSection> objSubSection,
            ISubSection qSubSection, string businessdependentSectionName, string dependentKeyName)
        {
            if (!string.IsNullOrEmpty(businessdependentSectionName))
            {
                //get current employment location or contact section
                var questionSubSectionContactList = objSubSection.Where(x =>
                    x.SeqNo == qSubSection.SeqNo + 1 && x.TemplateFieldName != null &&
                    x.TemplateFieldName.ToLower() == businessdependentSectionName.ToLower()).ToList();
                if (questionSubSectionContactList != null)
                {
                    foreach (var qSubContactSection in questionSubSectionContactList)
                    {
                        var questionContactList = GetQuestionList(qSubContactSection);
                        questionList.Add(dependentKeyName, questionContactList);
                    }
                }
            }
        }

        /// <summary>
        ///  Get the list of question for subsection
        /// </summary>
        /// <param name="objSubSection">Questionnaire sub section object</param>
        /// <param name="subSectionName">Sub section name</param>
        /// <param name="dependentSectionName">Child or dependentSub section name</param>
        /// <param name="dependentKeyName">Unique key name for sub section</param>
        private Dictionary<string, dynamic> GetQuestionListInfo(List<ISubSection> objSubSection, string subSectionName,
            string dependentSectionName, string dependentKeyName)
        {
            //get current employer
            Dictionary<string, dynamic> questionList = null;
            var questionSubSectionList = objSubSection.Where(x =>
                x.TemplateFieldName != null && x.TemplateFieldName.ToLower() == subSectionName.ToLower()).ToList();

            if (questionSubSectionList != null)
            {
                foreach (var qSubSection in questionSubSectionList)
                {
                    questionList = GetQuestionList(qSubSection);

                    GetDependnetQuestionInfo(questionList, objSubSection, qSubSection, dependentSectionName,
                        dependentKeyName);

                    #region "Old Code TODO : Will remove later"

                    /*
                    if (!string.IsNullOrEmpty(dependentSectionName))
                    {
                        //get current employment location or contact section
                        var questionSubSectionContactList = objSubSection.Where(x => x.SeqNo == qSubSection.SeqNo + 1 && x.TemplateFieldName != null && x.TemplateFieldName.ToLower() == dependentSectionName.ToLower()).ToList();
                        if (questionSubSectionContactList != null)
                        {
                            foreach (var qSubContactSection in questionSubSectionContactList)
                            {
                                var questionContactList = GetQuestionList(qSubContactSection);
                                questionList.Add(dependentKeyName, questionContactList);
                            }
                        }
                    }
                    */

                    #endregion "Old Code TODO : Will remove later"
                }
            }

            return questionList;
        }

        /// <summary>
        /// Get borrower employment income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private List<Dictionary<string, dynamic>> GetBorrowerEmployer(List<ISubSection> objSubSection)
        {
            var objQuestionDataList = new List<Dictionary<string, dynamic>>();

            //current employment
            GetEmploymentInfo(objQuestionDataList, objSubSection, ConstantUtils.CurrentEmployment,
                ConstantUtils.CurrentEmploymentContact, ConstantUtils.BorrowerEmployerContactKey, true);

            //past or other employment
            GetEmploymentInfo(objQuestionDataList, objSubSection, ConstantUtils.Employment,
                ConstantUtils.EmploymentContact, ConstantUtils.BorrowerEmployerContactKey, false);
            //objQuestionDataList.Add(employmentQuestionList);

            return objQuestionDataList;
        }

        /// <summary>
        /// Get borrower business income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private List<Dictionary<string, dynamic>> GetBorrowerBusiness(List<ISubSection> objSubSection)
        {
            var objQuestionDataList = new List<Dictionary<string, dynamic>>();

            //business
            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils.Business, "", "");

            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils.BusinessAnother, "", "");

            return objQuestionDataList;
        }

        /// <summary>
        /// Get borrower interest/dividend income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private List<Dictionary<string, dynamic>> GetBorrowerInterestDividend(List<ISubSection> objSubSection)
        {
            var objQuestionDataList = new List<Dictionary<string, dynamic>>();

            //business
            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils.InterestDividend, "", "");

            //GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils._InterestDividend, "", "");

            return objQuestionDataList;
        }

        /// <summary>
        /// Get borrower rental income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private List<Dictionary<string, dynamic>> GetBorrowerRental(List<ISubSection> objSubSection)
        {
            var objQuestionDataList = new List<Dictionary<string, dynamic>>();

            //rentalIncome
            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils.RentalIncome, "", "");
            //_rentalIncome
            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils._RentalIncome, "", "");

            return objQuestionDataList;
        }

        /// <summary>
        /// Get borrower other income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private List<Dictionary<string, dynamic>> GetBorrowerOthers(List<ISubSection> objSubSection)
        {
            var objQuestionDataList = new List<Dictionary<string, dynamic>>();

            //business
            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils.OtherIncome, "", "");

            GetBusinessInfo(objQuestionDataList, objSubSection, ConstantUtils._OtherIncome, "", "");

            return objQuestionDataList;
        }

        /// <summary>
        /// Get borrower alimony income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private Dictionary<string, dynamic> GetBorrowerAlimony(List<ISubSection> objSubSection)
        {
            var questionList = GetQuestionListInfo(objSubSection, ConstantUtils.Alimony, ConstantUtils.AlimonySupport,
                ConstantUtils.BorrowerIncomeTypeAlimonySupportKey);
            return questionList;
        }

        /// <summary>
        /// Get borrower militry pay income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private Dictionary<string, dynamic> GetBorrowerMilitryPay(List<ISubSection> objSubSection)
        {
            var questionList = GetQuestionListInfo(objSubSection, ConstantUtils.MilitaryPay, "", "");
            return questionList;
        }

        /// <summary>
        /// Get borrower social security income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private Dictionary<string, dynamic> GetBorrowerDOBSSN(List<ISubSection> objSubSection)
        {
            var questionList = GetQuestionListInfo(objSubSection, ConstantUtils.IncomeSearch, "", "");
            return questionList;
        }

        /// <summary>
        /// Get borrower social security income
        /// </summary>
        /// <param name="objSubSection">Borrower income sub section object</param>
        /// <returns>List of key value pair</returns>
        private Dictionary<string, dynamic> GetBorrowerSocialSecurity(List<ISubSection> objSubSection)
        {
            var questionList = GetQuestionListInfo(objSubSection, ConstantUtils.SocialSecurity, "", "");
            return questionList;
        }

        #endregion "Income section"

        /// <summary>
        /// This function fetches the information of the logged in user
        /// </summary>
        /// <returns></returns>
        private async Task<string> GetCurrentUser()
        {
            try
            {
                //TODO:By Nayan: Need to remove async, it should return string 
                var token = TokenParser.Parse(TokenReader.Read());
                var username = token?.Subject;
                if (!string.IsNullOrEmpty(username))
                {
                    username = username.ToLower();
                }

                return username;
            }
            catch (InvalidUserException exception)
            {
                throw new InvalidUserException(exception.Message, exception);
            }
        }

        #endregion "Private Method"

        #region "Public Method"

        public async Task<object> GetQuestionnaireSectionData(string applicationId, string borrowerId,
            string sectionName)
        {
            if (string.IsNullOrEmpty(borrowerId))
                borrowerId = await GetCurrentUser();
            if (string.Equals(sectionName, SectionDetails.Profile.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return await GetProfileOriginalData(applicationId, borrowerId);
            }

            if (string.Equals(sectionName, SectionDetails.Assets.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return await GetAssetOriginalData(applicationId, borrowerId);
            }

            if (string.Equals(sectionName, SectionDetails.Income.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return await GetIncomeOriginalData(applicationId, borrowerId);
            }

            return await GetDeclarationOriginalData(applicationId, borrowerId);
            //if(sectionName == SectionDetails.Declarations.ToString())
        }

        /// <summary>
        /// Get the profile original data
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>key value pair object</returns>
        private async Task<Dictionary<string, object>> GetProfileOriginalData(string applicationId, string borrowerId)
        {
            Dictionary<string, dynamic> questionListTo = null;

            try
            {
                questionListTo =
                    await GetQuestionListForSubSectionForSection(applicationId, (int) SectionDetails.Profile,
                        borrowerId);
            }
            catch (Exception e)
            {
                var message =
                    $"Failed to process request for Profile data of '{borrowerId}' for temporary application number '{applicationId}'. Error Message : " +
                    e.Message;
                Log.Error(message, e);
                throw new Exception(message, e);
            }

            return questionListTo;
        }

        /// <summary>
        /// Get the asset original data
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>key value pair object</returns>
        private async Task<List<object>> GetAssetOriginalData(string applicationId, string borrowerId)
        {
            var objDetails = new List<object>();
            try
            {
                if (!string.IsNullOrWhiteSpace(borrowerId))
                {
                    Log.Info($"...Execute DecisionEngine({Configuration.GetBankInfoRule})");

                    var getBankInfoRule = string.IsNullOrEmpty(Configuration.GetBankInfoRule)
                        ? "getbankinfo"
                        : Configuration.GetBankInfoRule;

                    var jResult = await Task.Run(() =>
                        DecisionEngineService.Execute<dynamic, JObject>(getBankInfoRule,
                            new {customerId = borrowerId, entityId = applicationId}));

                    if (jResult == null)
                    {
                        jResult = await Task.Run(() =>
                            DecisionEngineService.Execute<dynamic, JObject>(getBankInfoRule,
                                new {customerId = borrowerId}));
                    }

                    if (jResult != null)
                    {
                        dynamic bankInfo = JsonConvert.DeserializeObject(jResult.ToString());
                        string status = bankInfo.status != null ? Convert.ToString(bankInfo.status) : string.Empty;

                        if (status != "success")
                            throw new Exception(
                                $"Error occured in getting information from DCC : {jResult.GetValue("error").ToString()}");

                        if (bankInfo.data != null && bankInfo.data.result != null)
                        {
                            foreach (var institution in bankInfo.data.result)
                            {
                                var name = institution.Name;
                                foreach (var account in institution.Accounts)
                                {
                                    dynamic objBankdetail = new ExpandoObject();
                                    objBankdetail.BorrowerInstitutionName = name;
                                    objBankdetail.BorrowerAccountMask = account.Mask;
                                    objBankdetail.BorrowerBalance = account.Balances.Available != null
                                        ? account.Balances.Available
                                        : account.Balances.Current;
                                    //var isManualUpload = Convert.ToBoolean(account.IsManualUpload);
                                    objDetails.Add(objBankdetail);
                                    objBankdetail = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var message =
                    $"Failed to process request for Asset data of '{borrowerId}' for temporary application number '{applicationId}'. Error Message : " +
                    e.Message;
                Log.Error(message, e);
                throw new Exception(message, e);
            }

            return objDetails;
        }

        /// <summary>
        /// Get the profile original data
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>key value pair object</returns>
        private async Task<Dictionary<string, object>> GetDeclarationOriginalData(string applicationId,
            string borrowerId)
        {
            Dictionary<string, dynamic> questionListTo = null;
            try
            {
                questionListTo =
                    await GetQuestionListForSubSectionForSection(applicationId, (int) SectionDetails.Declarations,
                        borrowerId);
            }
            catch (Exception e)
            {
                var message =
                    $"Failed to process request for Declaration data of '{borrowerId}' for temporary application number '{applicationId}'. Error Message : " +
                    e.Message;
                Log.Error(message, e);
                throw new Exception(message, e);
            }

            return questionListTo;
        }


        /// <summary>
        /// Get the income original data
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <param name="borrowerId">The borrower id or user name</param>
        /// <returns>key value pair object</returns>
        private async Task<Dictionary<string, dynamic>> GetIncomeOriginalData(string applicationId, string borrowerId)
        {
            var objData = new Dictionary<string, dynamic>();
            try
            {
                //var sectionData = await QuestionnaireSection.GetRawSection(applicationId, (int)SectionDetails.Income);

                var sectionData =
                    await QuestionnaireRepository.GetSectionById(applicationId, (int) SectionDetails.Income);

                if (sectionData != null)
                {
                    var questionSubSection = sectionData.SubSections
                        .Where(s => s.UserName == borrowerId && s.QuestionSections != null).ToList();

                    //get borrower income search(birthdate / ssn)
                    var objBirthDateSsnQuestionData = GetBorrowerDOBSSN(questionSubSection);
                    if (objBirthDateSsnQuestionData != null)
                    {
                        foreach (var item in objBirthDateSsnQuestionData)
                        {
                            objData.Add(item.Key, item.Value);
                        }
                    }

                    //Borrower current employer * other employer details with contact address
                    var objEmploymentQuestionData = GetBorrowerEmployer(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeEmployerKey, objEmploymentQuestionData);

                    //Borrower business
                    var objBusinessQuestionData = GetBorrowerBusiness(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeBusinessKey, objBusinessQuestionData);

                    //Borrower Military pay
                    var objMilitryQuestionData = GetBorrowerMilitryPay(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeMilitaryPayKey, objMilitryQuestionData);

                    //interest and dividend
                    var objInterestDividendQuestionData = GetBorrowerInterestDividend(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeInterestDividentKey, objInterestDividendQuestionData);

                    //rental income
                    //interest and dividend
                    var objRentalQuestionData = GetBorrowerRental(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeRentalKey, objRentalQuestionData);

                    //alimony income
                    var objAlimonyQuestionData = GetBorrowerAlimony(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeAlimonyChildKey, objAlimonyQuestionData);

                    //socialSecurity
                    var objSocialSecurityQuestionData = GetBorrowerSocialSecurity(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeSocialSecurityKey, objSocialSecurityQuestionData);

                    //others
                    var objOthersQuestionData = GetBorrowerOthers(questionSubSection);
                    objData.Add(ConstantUtils.BorrowerIncomeTypeOtherKey, objOthersQuestionData);

                    //incomeOriginalData.Add(objData);
                }
            }
            catch (Exception e)
            {
                Log.Error("Unable to get Income data : " + e.Message, e);
            }

            return objData;
        }


        public async Task<Dictionary<string, object>> GetAllSectionOriginalData(string applicationId, string borrowerId)
        {
            var originalData = new Dictionary<string, dynamic>();

            if (string.IsNullOrEmpty(borrowerId))
                borrowerId = await GetCurrentUser();

            var originalProfileData = await GetProfileOriginalData(applicationId, borrowerId);
            var originalAssetData = await GetAssetOriginalData(applicationId, borrowerId);
            var originalIncomeData = await GetIncomeOriginalData(applicationId, borrowerId);
            var originalDeclarationData = await GetDeclarationOriginalData(applicationId, borrowerId);

            originalData.Add(SectionDetails.Profile.ToString(), originalProfileData);
            originalData.Add(SectionDetails.Assets.ToString(), originalAssetData);
            originalData.Add(SectionDetails.Income.ToString(), originalIncomeData);
            originalData.Add(SectionDetails.Declarations.ToString(), originalDeclarationData);

            return originalData;
        }

        #endregion "Public Method"
    }
}