﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Newtonsoft.Json.Linq;
using Docitt.Questionnaire.Abstractions;
#if DOTNET2

#else
using Microsoft.AspNet.Http;
#endif

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        // This method is used to get the summary of questionnaire.
        public async Task<List<Dictionary<string, ISummary>>> GetSummary(string temporaryApplicationNumber, IQuestionnaire questionnaire = null, bool isFromLenderDashboard = false, bool isAllowBlankFieldData = false)
        {
            //List<IQuestionnaireSummaryConfig> summary = new List<IQuestionnaireSummaryConfig>();
            var questionnaireCopy = await GetDecryptedQuestionnaire(await GetQuestionnaire(temporaryApplicationNumber));

            if (questionnaire == null) // if questionnaire is null then fetch it from DB.
            {
                questionnaire = Extensions.Clone(questionnaireCopy);
            }

            var questionnaireSummaryFieldMap = Extensions.Clone<List<QuestionnaireSummaryConfig>>(QuestionnaireConfigurations.ApplicationSummary.Map);

            // Questionnaire Field Map contains information about the question that are to be shown on Summary.
            // Loop through the Questionnaire.
            // Select SubSections that are mentioned in the Questionnaire. 
            // Keep the Questions that are needed. Remove the Questions that are not needed.
            // Prepare an array of SubSections.
            if (IsAffinityPartnerUser())
            {
                foreach (var item in questionnaireSummaryFieldMap.Where(ss => ss.IsHiddenForAffinityPartner))
                {
                    item.IsVisible = false;
                }
            }

            ISubSection subSectionFromTemplate = new SubSection();

            var question = await GetLoanPurposeQuestion(temporaryApplicationNumber,null,questionnaire);

            if (question != null)
            {
                if (question.Answer == loanPurpose.Purchase.ToString())
                    questionnaireSummaryFieldMap = questionnaireSummaryFieldMap.Where(x => x.SubSectionType != "About Loan Refinance").ToList();
                else if (question.Answer == loanPurpose.Refinance.ToString())
                    questionnaireSummaryFieldMap =  questionnaireSummaryFieldMap.Where(x => x.SubSectionType != "About Loan Purchase").ToList();
            }

            var userSummary = new Dictionary<string, List<ISubSectionSummary>>();
            // List<IBorrowerCoBorrowerInfo> borrowerList = await GetBorrowersAndCoBorrowers(temporaryApplicationNumber, false, questionnaire);            
            var borrowerList = await GetBorrowersAndCoBorrowersAndSpouseName(temporaryApplicationNumber, false, questionnaireCopy);
            var userName = string.Empty;

            var spouseResidence = questionnaire.ApplicationForm.Sections
                                               .Where(x=>x.SectionId == 1)
                                               .SelectMany(x=>x.SubSections)
                                               .Where(x=> x.TemplateFieldName == "coBorrowerSpouseResidenceDetail" || x.TemplateFieldName =="coBorrowerSpouseAdditionalResidence");

            foreach(var spouseSubSection in spouseResidence)
            {
                // Get the role of the user. 
                var sectionUser = borrowerList.Where(x=>x.UserName ==  spouseSubSection.UserName).FirstOrDefault();
                if(sectionUser != null && sectionUser.ActualApplicantType == ApplicantType.CoBorrower)
                {
                    var spouseUserName = questionnaire.UserState.FirstOrDefault(x=>x.UserName == sectionUser.UserName);
                    if(spouseUserName != null)
                        spouseSubSection.UserName = spouseUserName.SpouseUserName;
                }
            }

            foreach (IQuestionnaireSummaryConfig item in questionnaireSummaryFieldMap.Where(s => s.IsVisible).OrderBy(x => x.SectionId))
            {    
                // Fix: Dynamically generated question ids for section 3 and 4 have to be consistant in summary to display for spouse and co-borrower
                if (item.SectionId == Convert.ToInt32(SectionDetails.Income))
                {
                    var tempQuestionIds = new List<string>();
                    foreach (var questionId in item.QuestionIds)
                        tempQuestionIds.Add("-1" + questionId);

                    if (tempQuestionIds.Count > 0)
                        item.QuestionIds = item.QuestionIds.Union(tempQuestionIds.ToArray()).ToArray();
                }
                else if (item.SectionId == Convert.ToInt32(SectionDetails.Declarations))
                {
                    var tempQuestionIds = new List<string>();
                    foreach (var questionId in item.QuestionIds)
                    {
                        int intQuestionId;
                        if (int.TryParse(questionId, out intQuestionId))
                        {
                            if (intQuestionId > 0 && intQuestionId < 10)
                                tempQuestionIds.Add("100" + questionId);
                            else if (intQuestionId >= 10)
                                tempQuestionIds.Add("10" + questionId);
                        }
                    }
                    if (tempQuestionIds.Count > 0)
                        item.QuestionIds = item.QuestionIds.Union(tempQuestionIds.ToArray()).ToArray();
                }
                // End Fix:

                var _originalSubSectionName = item.SubSectionName;
                var coBorrowerIndex = 1;

                do
                {
                    #region "Generate Result for Summary"

                    var subSectionSummaries = new List<ISubSectionSummary>();
                    subSectionSummaries = await GetSubSectionInfo(questionnaire, item, questionnaire,
                        isAllowBlankFieldData, isFromLenderDashboard, borrowers: borrowerList);

                    foreach (SubSectionSummary tempSubSectionSummary in subSectionSummaries)
                    {
                        var subSectionSummary = Extensions.Clone<SubSectionSummary>(tempSubSectionSummary);
                        if (subSectionSummary != null)
                        {
                            subSectionSummary.HighestSectionReached = questionnaire.HighestSectionReached;
                            subSectionSummary.HighestSubSectionReached = questionnaire.HighestSubSectionReached;
                            subSectionSummary.SubSectionType = item.SubSectionType;
                            subSectionSummary.SubSectionTypeInfo = item.SubSectionTypeInfo;

                            userName = subSectionSummary.SubSection.UserName;

                            if (userName == null || userName == "")
                            {
                                //If there is no username assigned to the section, then it will be a common subsection that will be visible to all the applicants
                                userName = "ALL";
                            }

                            if (userSummary.ContainsKey(userName) == false)
                            {
                                //If the key does not exist then add a collection
                                userSummary.Add(userName, new List<ISubSectionSummary>());
                            }

                            ISubSectionSummary subSectionSummaryCopy = new SubSectionSummary();
                            subSectionSummaryCopy = subSectionSummary;
                            userSummary[userName].Add(subSectionSummaryCopy);
                            subSectionSummaryCopy = null;
                        }
                    }

                    #endregion

                    if (!string.IsNullOrWhiteSpace(item.BorrowerNamePlaceHolder) && !string.IsNullOrWhiteSpace(item.CoBorrowerNamePlaceHolder)
                            && borrowerList.Count > 1)
                    {
                        item.SubSectionName = _originalSubSectionName.Replace(item.BorrowerNamePlaceHolder, string.Format(item.CoBorrowerNamePlaceHolder, coBorrowerIndex));
                    }

                    if (coBorrowerIndex < borrowerList.Count)
                        coBorrowerIndex++;
                    else
                        break;

                } while (!string.IsNullOrWhiteSpace(item.BorrowerNamePlaceHolder) && !string.IsNullOrWhiteSpace(item.CoBorrowerNamePlaceHolder));
            }

            var requiredQuestions = new Dictionary<string, List<IRequiredQuestionDetails>>();

            if (!isFromLenderDashboard)
            {
                requiredQuestions = await GetValidationsOnSummary(temporaryApplicationNumber, borrowerList, questionnaireCopy);
                var sortedUserSummary = new Dictionary<string, List<ISubSectionSummary>>();
                foreach (var user in borrowerList)
                {
                    if (!userSummary.ContainsKey(user.UserName))
                        userSummary.Add(user.UserName, new List<ISubSectionSummary>());

                    if (!sortedUserSummary.ContainsKey(user.UserName))
                    {
                        sortedUserSummary.Add(user.UserName, new List<ISubSectionSummary>());
                        var sortedSummaryList = userSummary[user.UserName].ToList();
                        foreach(var sortedSummary in sortedSummaryList)
                            sortedUserSummary[user.UserName].Add(sortedSummary);
                    }                        
                }

                if(sortedUserSummary.Keys.Count() == userSummary.Keys.Count())
                    userSummary = sortedUserSummary;


                #region "Hide unwanted user's summary based on applicant role"

                var loggedInUser = await GetCurrentUser();
                
                if (borrowerList.Any() && !string.IsNullOrWhiteSpace(loggedInUser))
                {
                    var isBorrowerOrSpouse = borrowerList.Any(x => x.UserName == loggedInUser
                                                               && (x.ApplicantType == ApplicantType.Borrower || x.ApplicantType == ApplicantType.Spouse));

                    if (!isBorrowerOrSpouse)
                    {
                        var loggedBorrower = borrowerList
                                                            .Where(x=>x.UserName == loggedInUser)
                                                            .FirstOrDefault();

                        if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrower || loggedBorrower.ActualApplicantType == ApplicantType.CoBorrowerSpouse)
                        {
                            var loggedInUserSpouse = await GetCoupleUserName(questionnaire.UserState, loggedBorrower.UserName);

                            if(!string.IsNullOrEmpty(loggedInUserSpouse))
                            {
                                userSummary = userSummary.Where(x => x.Key == loggedInUser || x.Key == loggedInUserSpouse).ToDictionary(i => i.Key, i => i.Value);
                            }
                            else
                            {
                                userSummary = userSummary.Where(x => x.Key == loggedInUser).ToDictionary(i => i.Key, i => i.Value);   
                            }
                        }
                    }
                    else
                    {
                        var borrowerOrSpouseList = borrowerList.Where(x => x.ApplicantType == ApplicantType.Borrower
                                                                                 || x.ApplicantType == ApplicantType.Spouse).ToList();

                        userSummary = userSummary.Where(x => borrowerOrSpouseList.Any(y => y.UserName == x.Key)).ToDictionary(i => i.Key, i => i.Value);
                    }
                }

                #endregion
            }

            var userSummaryList = new List<Dictionary<string, ISummary>>();

            for (var i = 0; i < userSummary.Count; i++)
            {
                if (userSummary.ElementAt(i).Key != "ALL")
                {
                    var userDictionary = new Dictionary<string, ISummary>();
                    Summary userDetails = null;

                    if (requiredQuestions.ContainsKey(userSummary.ElementAt(i).Key))
                        userDetails = new Summary
                        {
                            SubSectionSummary = userSummary.ElementAt(i).Value,
                            RequiredQuestions = requiredQuestions[userSummary.ElementAt(i).Key]
                        };
                    else
                        userDetails = new Summary
                        {
                            SubSectionSummary = userSummary.ElementAt(i).Value,
                            RequiredQuestions = null
                        };

                    var applicantRole = borrowerList.FirstOrDefault(x => x.UserName == userSummary.ElementAt(i).Key);
                    if(applicantRole != null)
                    {
                        userDetails.ApplicantType = applicantRole.ApplicantType.ToString();
                        userDetails.SpouseUserName = applicantRole.SpouseUserName;
                    }
                        
                    userDictionary.Add(userSummary.ElementAt(i).Key, userDetails);
                    userSummaryList.Add(userDictionary);
                }
            }

            return userSummaryList;
        }

        // This function fetches the information of the Questionnaire summary, that is then display in the Lender portal at Application Tab.
        public async Task<List<Dictionary<string, ISummary>>> GetQuestionnaireSummary(string temporaryApplicationNumber, bool isAllowBlankAnswerField)
        {
            return await Task.Run(() => GetSummary(temporaryApplicationNumber, null, true, isAllowBlankAnswerField));
        }

        // This function fetches the information of sub sections needed to show on the Summary.
        private async Task<List<ISubSectionSummary>> GetSubSectionInfo(IQuestionnaire questionnaire, IQuestionnaireSummaryConfig item, IQuestionnaire questionnaireCopy, bool isAllowBlankFieldData = false, bool isFromLenderDashboard = false,List<IBorrowerCoBorrowerSpouseInfo> borrowers=null)
        {
            var subSections = new List<ISubSection>();
            var loggedInUser = await GetCurrentUser();
            var sectionId = item.SectionId;

            subSections = questionnaire.ApplicationForm
                                       .Sections.Where(s => s.SectionId == item.SectionId)
                                       .SelectMany(ss => ss.SubSections)
                                       .Where(ss => 
                                           (ss.SubSectionApplicationSummary != null && ss.SubSectionApplicationSummary == item.SubSectionName) ||
                                           (ss.SubSectionApplicationSummary == null && ss.SubSectionName == item.SubSectionName)
                                        )
                                       .ToList();
                  
            

            if (subSections == null)
            {
                return null;
            }

            var temporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;

            var subSectionSummaries = new List<ISubSectionSummary>();

            foreach (var subSection in subSections)
            {
                var processedSubSection = await ProcessSubSection(subSection, item);

                ISubSectionSummary subSectionSummary = new SubSectionSummary();
                subSectionSummary.SubSection = processedSubSection;
                subSectionSummary.SectionId = item.SectionId;
                subSectionSummary.SubSectionId = processedSubSection.SubSectionId;
                subSectionSummary.SubSectionName = processedSubSection.SubSectionName;
                subSectionSummary.SubSectionType = item.SubSectionType;
                subSectionSummary.SubSectionTypeInfo = item.SubSectionTypeInfo;

                subSectionSummary = await ProcessSubSectionSummary(temporaryApplicationNumber, subSectionSummary, item, questionnaireCopy, isAllowBlankFieldData, isFromLenderDashboard);

                //// borrower and coborrower spouse section club
                if(subSection.TemplateFieldName == "spouseInfo" || subSection.TemplateFieldName == "spouseCommunicationInfo")
                {
                    if (borrowers == null)
                    {
                        borrowers = await GetBorrowersAndCoBorrowersAndSpouseName(questionnaire.TemporaryApplicationNumber, false);
                    }
                    foreach (var user in borrowers)
                    {
                        if(user.UserName == loggedInUser && user.ApplicantType == ApplicantType.Borrower)
                        {
                            subSectionSummary.SubSectionId = ConstantUtils.JointCreditSubSectionId;
                        }
                    } 
                }
                else if(subSection.TemplateFieldName == "coBorrowerSpouseInfo" || subSection.TemplateFieldName == "coBorrowerSpouseCommunicationInfo")
                {
                    if (borrowers == null)
                    {
                        borrowers = await GetBorrowersAndCoBorrowersAndSpouseName(questionnaire.TemporaryApplicationNumber, false);
                    }
                    foreach (var user in borrowers)
                    {
                        if(user.UserName == loggedInUser && user.ApplicantType == ApplicantType.CoBorrower)
                        {
                            var coBorrowerMaritalStatusSection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == Convert.ToInt32(SectionDetails.Profile))
                                .SelectMany(ss => ss.SubSections).FirstOrDefault(ss => ss.TemplateFieldName == "coBorrowerMaritalStatusInfo" && ss.UserName == loggedInUser);
                            if(coBorrowerMaritalStatusSection != null)
                            {
                                subSectionSummary.SubSectionId = coBorrowerMaritalStatusSection.SubSectionId;
                            }
                        }
                    } 
                }

                //Below commented code is working but too much time consuming.
                //subSectionSummaries.Add(JSON.Deserialize<ISubSectionSummary>(JSON.Serialize(subSectionSummary)));
                if (subSectionSummary.SubSection.QuestionSections.Count > 0)
                    subSectionSummaries.Add(subSectionSummary);
                
                if (item.subSectionTemplateFieldName == ConstantUtils.ssTemplateFieldNameBorrowerResidenceDetail || 
                    item.subSectionTemplateFieldName == ConstantUtils.ssTemplateFieldNameCoBorrowerResidenceDetail)
                {
                    var userstateInfo = questionnaire.UserState.Where(x=>x.UserName == subSectionSummary.SubSection.UserName).FirstOrDefault();
                    var spouseUserName = string.Empty;

                    spouseUserName = userstateInfo?.SpouseUserName;

                    if (!string.IsNullOrEmpty((spouseUserName)))
                    {
                        var IsResidenceSameQuestion = await GetQuestionFromQuestionTextOrQuestionId(
                            subSectionSummary, 
                            null,
                            item.QuestionInformation["ResidenseSameOrSpouse"].DisplayQuestionText);
                        
                        if (IsResidenceSameQuestion?.Answer == "true")
                        {
                            var spouseSubSectionSummary = Extensions.Clone(subSectionSummary);
                            spouseSubSectionSummary.SubSection.UserName = spouseUserName;

                            if (spouseSubSectionSummary.SubSection.QuestionSections.Count > 0)
                                subSectionSummaries.Add(spouseSubSectionSummary);
                        }
                    }                    
                }
            }

            subSections = null;

            return subSectionSummaries;
        }

        // This function processes the sub section
        private async Task<ISubSection> ProcessSubSection(ISubSection subSection, IQuestionnaireSummaryConfig item)
        {
            var itemQuestionIds = item.QuestionIds;

            if (item.SubSectionType == "Alimony/Child's support" && Array.Exists(itemQuestionIds, x => x == "childSupportQuestions"))
            {
                var templateQuestionIds = subSection.QuestionSections
                                                    .SelectMany(qs => qs.QuestionList)
                                                    .Where(qt => !string.IsNullOrWhiteSpace(qt.QuestionText))
                                                    .Select(q => q.QuestionId).ToList();

                itemQuestionIds = itemQuestionIds.Union(templateQuestionIds.ToArray()).ToArray();
            }

            if (item.SubSectionType == "Co-Borrower Information")
            {
                var templateQuestionIds = subSection.QuestionSections
                                                    .Where(qs => qs.QuestionSectionTemplateId == "coBorrowerInvitationQuestions")
                                                    .SelectMany(qs => qs.QuestionList)
                                                    .Select(q => q.QuestionId).ToList();

                itemQuestionIds = itemQuestionIds.Union(templateQuestionIds.ToArray()).ToArray();
            }
 
            if(item.SubSectionType == "Purchase Property Details")
            {
                var propertySelectedQuestionId = "12";
                var propertyNotSelectedQuestionId = "1212";
                var borrowerSelectedProperty = "BorrowerSelectedProperty";
                var borrowerSelectedPropertyQuestion = subSection.QuestionSections
                                                    .SelectMany(qs => qs.QuestionList)
                                                    .FirstOrDefault(x => x.QuestionFieldName == borrowerSelectedProperty);

                if(borrowerSelectedPropertyQuestion != null)
                {
                    if(borrowerSelectedPropertyQuestion.Answer!=null && borrowerSelectedPropertyQuestion.Answer.ToLower() == "false")
                    {
                        for (var i = 0; i < itemQuestionIds.Length; i++)
                        {
                            if(itemQuestionIds[i] == propertySelectedQuestionId)
                            {
                                itemQuestionIds[i] = propertyNotSelectedQuestionId;
                            }
                        }
                    }
                }
            }
            var AllQuestionIds = subSection.QuestionSections.SelectMany(qs => qs.QuestionList).Select(q => q.QuestionId).ToList();
            
            var QuestionIdToRemove = AllQuestionIds.Except(itemQuestionIds).ToList();

            var questionFound = false;
            for (var j = 0; j < QuestionIdToRemove.Count; j++)
            {
                questionFound = false;
                foreach (QuestionSection qs in subSection.QuestionSections)
                {
                    for (var qi = 0; qi < qs.QuestionList.Count; qi++)
                    {
                        if (qs.QuestionList[qi].QuestionId == QuestionIdToRemove[j])
                        {
                            qs.QuestionList.Remove(qs.QuestionList[qi]);
                            questionFound = true;
                            break;
                        }
                    }
                    if (questionFound == true)
                    {
                        break;
                    }
                }
            }

            subSection.QuestionSections = subSection.QuestionSections.Where(qs => qs.QuestionList.Count != 0).ToList();

            return await Task.Run(() => subSection);
        }

        /// <summary>
        /// This is used to remove special character from answer and concat with comma
        /// Ex : ["Phone","Email"]  To Phone,Email
        /// </summary>
        /// <param name="questionAnswer"></param>
        /// <returns>return string of concat answer</returns>
        private string removeSpecialCharacterAndConcatAnswer(string answer)
        {  
                if(!string.IsNullOrEmpty(answer))
                {
                    var answerList = JsonConvert.DeserializeObject<List<string>>(answer);
                    if(answerList != null)
                    {
                     answer = (string.Join(",", answerList));
                    }       
                }

            return answer;
        }

        // This function processes the questionnaire data for Summary
        private async Task<ISubSectionSummary> ProcessSubSectionSummary(string temporaryApplicationNumber, ISubSectionSummary subSectionSummary, IQuestionnaireSummaryConfig summaryItem, IQuestionnaire questionnaire, bool isAllowBlankFieldData = false, bool isFromLenderDashboard = false)
        {
            var newQuestionSequence = new List<IQuestion>();
            var purchaseAboutLoanSection = "About Loan Purchase";
            var refinanceAboutLoanSection = "About Loan Refinance";
            var alimonychildSection = "Alimony/Child's support";
            var employmentSection = "Employment";
            var purchasePropTitle = "Purchase Property Details";
            var businessSelfEmployment = "Business/Self Employment";
            var vaternSection = "VA/Military Loan";
            var realEstateSectionType = "Real Estate Agent";
            var loanOfficerSectionType = "Loan Officer";
            var questionsToRemove = new List<IQuestion>();

            if (subSectionSummary.SubSectionTypeInfo == "Account Information")
            {
                //Merge First Name, Middle Name and Last Name as Full Name. 
                var firstName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].QuestionId); // "First Name", null);
                var middleName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["MiddleName"].QuestionText, summaryItem.QuestionInformation["MiddleName"].QuestionId); // "Middle Name", null);
                var lastName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["LastName"].QuestionText, summaryItem.QuestionInformation["LastName"].QuestionId); // "Last Name", null);
                var suffix = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["Suffix"].QuestionText, summaryItem.QuestionInformation["Suffix"].QuestionId); // "Suffix", null);
                LoanPurpose = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LoanPurpose"].QuestionId); // "Suffix", null);

                var fullName = "";

                if (string.IsNullOrEmpty(middleName))
                {
                    fullName = firstName + " " + lastName + " " + suffix;
                }
                else
                {
                    fullName = firstName + " " + middleName + " " + lastName + " " + suffix;
                }

                var dob = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["DOB"].QuestionText, summaryItem.QuestionInformation["DOB"].QuestionId);
                var ssn = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["SSN"].QuestionText, summaryItem.QuestionInformation["SSN"].QuestionId);

                // Mask ssn if from lender 
                if (isFromLenderDashboard && !string.IsNullOrEmpty(ssn))
                {
                     ssn = "00000" + ssn.Substring(ssn.Length - 4);
                }

                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText, fullName); //Full Name
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["DOB"].QuestionText, summaryItem.QuestionInformation["DOB"].DisplayQuestionText, dob);
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["SSN"].QuestionText, summaryItem.QuestionInformation["SSN"].DisplayQuestionText, ssn);

                //Remove these questions, as they are not be shown on the summary screen
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["LastName"].QuestionText); // "Last Name");
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["LoanPurpose"].QuestionText); // Loan purpose

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText)); // "Full Name"));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["DOB"].DisplayQuestionText));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["SSN"].DisplayQuestionText)); 

                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);

                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if(subSectionSummary.SubSectionType == vaternSection)
            {
                var vaScreenQuestion = subSectionSummary.SubSection.QuestionSections
                                   .SelectMany(qs => qs.QuestionList)
                                   .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["VA"].QuestionId);

                if (vaScreenQuestion != null)
                {
                    if (!string.IsNullOrWhiteSpace(vaScreenQuestion.Answer))
                    {
                        var selectedQuestionIds = vaScreenQuestion.ChildQuestions.Where(c => c.Answer.ToLower() != vaScreenQuestion.Answer.ToLower()).ToList().Select(x => x.QuestionId);
                        if (selectedQuestionIds != null)
                        {
                            foreach(var questionId in selectedQuestionIds)
                            {
                                subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, questionId);
                            }                        
                        }
                    }
                }

                var monthlyChildCareQuestion = subSectionSummary.SubSection.QuestionSections
                                   .SelectMany(qs => qs.QuestionList)
                                   .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["MonthlyChildCare"].QuestionId);

                if (monthlyChildCareQuestion != null)
                {
                    if (!string.IsNullOrWhiteSpace(monthlyChildCareQuestion.Answer))
                    {
                        var selectedQuestionIds = monthlyChildCareQuestion.ChildQuestions.Where(c => c.Answer.ToLower() != monthlyChildCareQuestion.Answer.ToLower()).ToList().Select(x => x.QuestionId);
                        if (selectedQuestionIds != null)
                        {
                            foreach(var questionId in selectedQuestionIds)
                            {
                                subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, questionId);
                            }                        
                        }
                    }
                }
            }
            else if (subSectionSummary.SubSectionTypeInfo == "Communication Information")
            {
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["Phone"].QuestionText, summaryItem.QuestionInformation["Phone"].DisplayQuestionText, null,true); //"Phone Number"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["CellPhone"].QuestionText, summaryItem.QuestionInformation["CellPhone"].DisplayQuestionText, null,true); //"Cell Phone Number"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["Email"].QuestionText, summaryItem.QuestionInformation["Email"].DisplayQuestionText, null,true); //"Email Address"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MethodofCommunication"].QuestionText, summaryItem.QuestionInformation["MethodofCommunication"].DisplayQuestionText, null,true); //"Method of communication"

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["Email"].DisplayQuestionText)); // "Email Address"));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["Phone"].DisplayQuestionText)); // "Phone Number"));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["CellPhone"].DisplayQuestionText)); // "Cell Phone Number"));
              
                 IQuestion questionAnswer = new Question();

                if (summaryItem.QuestionInformation["MethodofCommunication"].DisplayQuestionText  != null)
                {
                    questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["MethodofCommunication"].QuestionId);
                        if(questionAnswer != null)
                        {
                           if(!string.IsNullOrEmpty(questionAnswer.Answer))
                           {
                               /* var answerList = JsonConvert.DeserializeObject<List<string>>(questionAnswer.Answer);
                                var answerstring = (string.Join(",", answerList));
                                 questionAnswer.Answer = answerstring; */
                                 questionAnswer.Answer =   removeSpecialCharacterAndConcatAnswer(questionAnswer.Answer);        
                           }
                        }
                            
                }                

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MethodofCommunication"].DisplayQuestionText)); // "Method of communication"));

                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);

                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if (subSectionSummary.SubSectionTypeInfo == "Coborrower Account Information")
            {
                //Merge First Name, Middle Name and Last Name as Full Name. 
                var firstName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].QuestionId); // "First Name", null);
                var middleName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["MiddleName"].QuestionText, summaryItem.QuestionInformation["MiddleName"].QuestionId); // "Middle Name", null);
                var lastName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["LastName"].QuestionText, summaryItem.QuestionInformation["LastName"].QuestionId); // "Last Name", null);
                var suffix = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["Suffix"].QuestionText, summaryItem.QuestionInformation["Suffix"].QuestionId); // "Suffix", null);

                var fullName = "";

                if (string.IsNullOrEmpty(middleName))
                {
                    fullName = firstName + " " + lastName + " " + suffix;
                }
                else
                {
                    fullName = firstName + " " + middleName + " " + lastName + " " + suffix;
                }

                var dob = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["DOB"].QuestionText, summaryItem.QuestionInformation["DOB"].QuestionId);
                var ssn = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["SSN"].QuestionText, summaryItem.QuestionInformation["SSN"].QuestionId);

                // Mask ssn if from lender 
                if (isFromLenderDashboard && !string.IsNullOrEmpty(ssn))
                {
                     ssn = "00000" + ssn.Substring(ssn.Length - 4);
                }

                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText, fullName); //Full Name
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["DOB"].QuestionText, summaryItem.QuestionInformation["DOB"].DisplayQuestionText, dob);
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["SSN"].QuestionText, summaryItem.QuestionInformation["SSN"].DisplayQuestionText, ssn);

                //Remove these questions, as they are not be shown on the summary screen
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["LastName"].QuestionText); // "Last Name");
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["MiddleName"].QuestionText); // "Middle Name");
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["Suffix"].QuestionText); //Suffix

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText)); // "Full Name"));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["DOB"].DisplayQuestionText));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["SSN"].DisplayQuestionText));
                
                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);
                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if (subSectionSummary.SubSectionTypeInfo == "Coborrower Communication Information")
            {
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["Phone"].QuestionText, summaryItem.QuestionInformation["Phone"].DisplayQuestionText, null,true); //"Phone Number"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["CellPhone"].QuestionText, summaryItem.QuestionInformation["CellPhone"].DisplayQuestionText, null,true); //"Phone Number"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["Email"].QuestionText, summaryItem.QuestionInformation["Email"].DisplayQuestionText, null,true); //"Email Address"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MethodofCommunication"].QuestionText, summaryItem.QuestionInformation["MethodofCommunication"].DisplayQuestionText, null,true); //"Method of communication"

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["Email"].DisplayQuestionText)); // "Email Address"));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["Phone"].DisplayQuestionText)); // "Phone Number"));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["CellPhone"].DisplayQuestionText)); // "Phone Number"));
                //remove special character

                 IQuestion questionAnswer = new Question();

                if (summaryItem.QuestionInformation["MethodofCommunication"].QuestionId  != null)
                {
                        questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["MethodofCommunication"].QuestionId);
                        if(questionAnswer != null)
                        {
                           if(!string.IsNullOrEmpty(questionAnswer.Answer))
                           {
                              /*  var answerList = JsonConvert.DeserializeObject<List<string>>(questionAnswer.Answer);
                                var answerstring = (string.Join(",", answerList));
                                 questionAnswer.Answer = answerstring; */

                                questionAnswer.Answer =   removeSpecialCharacterAndConcatAnswer(questionAnswer.Answer);
                           }
                        }   
                }                

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MethodofCommunication"].DisplayQuestionText)); // "Method of communication"));


                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);
                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if(subSectionSummary.SubSectionType == "Co-Borrower Marital Status")
            {
                //Merge First Name, Middle Name and Last Name as Full Name. 
                var addSpouse = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["AddSpouse"].QuestionText, summaryItem.QuestionInformation["AddSpouse"].QuestionId); // "AddSpouse", null);
                var firstName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].QuestionId); // "First Name", null);
                var middleName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["MiddleName"].QuestionText, summaryItem.QuestionInformation["MiddleName"].QuestionId); // "Middle Name", null);
                var lastName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["LastName"].QuestionText, summaryItem.QuestionInformation["LastName"].QuestionId); // "Last Name", null);
                var suffix = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["Suffix"].QuestionText, summaryItem.QuestionInformation["Suffix"].QuestionId); // "Suffix", null);

                var fullName = "";

                if (string.IsNullOrEmpty(middleName))
                {
                    fullName = firstName + " " + lastName + " " + suffix;
                }
                else
                {
                    fullName = firstName + " " + middleName + " " + lastName + " " + suffix;
                }

                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText, fullName); //Full Name
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["Email"].QuestionText, summaryItem.QuestionInformation["Email"].DisplayQuestionText, null, true);
                
                //Remove these questions, as they are not be shown on the summary screen
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["LastName"].QuestionText); // "Last Name");
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["MiddleName"].QuestionText); // "Middle Name");
                subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["Suffix"].QuestionText); //Suffix
                if (string.IsNullOrEmpty(addSpouse) || addSpouse == "false")
                {
                     //Remove these questions, as they are not be shown on the summary screen
                     subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["BirthDate"].QuestionText); // "BirthDate");
                     subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["SSN"].QuestionText); // "SSN");
                     subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["Email"].QuestionText); // "Email");
                     subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["Phone"].QuestionText); // "Phone");
                }
                
            }
            else if (subSectionSummary.SubSectionType == "Who will be on the loan")
            {
                var strMaritalStatus = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["MaritalStatus"].QuestionText, summaryItem.QuestionInformation["MaritalStatus"].QuestionId);
                var strMaritalStatusJoint = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MaritalStatusJoint"].QuestionId);
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MaritalStatus"].QuestionText, summaryItem.QuestionInformation["MaritalStatus"].DisplayQuestionText, strMaritalStatus);
                var typeOfCredit = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["TypeOfCredit"].QuestionId); // "Suffix", null);

                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["TypeOfCredit"].QuestionText, summaryItem.QuestionInformation["TypeOfCredit"].DisplayQuestionText, null);
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["Email"].QuestionText, summaryItem.QuestionInformation["Email"].DisplayQuestionText, null);
                
                if (typeOfCredit == "Individual Credit" && (strMaritalStatus.ToLower() == "married" || strMaritalStatus.ToLower() == "separated"))
                {
                    //Merge First Name, Middle Name and Last Name as Full Name. 
                    var firstName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].QuestionId); // "First Name", null);
                    var middleName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["MiddleName"].QuestionText, summaryItem.QuestionInformation["MiddleName"].QuestionId); // "Middle Name", null);
                    var lastName = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["LastName"].QuestionText, summaryItem.QuestionInformation["LastName"].QuestionId); // "Last Name", null);
                    var suffix = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["Suffix"].QuestionText, summaryItem.QuestionInformation["Suffix"].QuestionId); // "Suffix", null);
                    var fullName = string.Empty;

                    if (string.IsNullOrEmpty(middleName))
                        fullName = firstName + " " + lastName + " " + suffix;
                    else
                        fullName = firstName + " " + middleName + " " + lastName + " " + suffix;

                    subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText, fullName); //Full Name
                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText));
                    
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["LastName"].QuestionText);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["MiddleName"].QuestionText); // "Middle Name");
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["Suffix"].QuestionText); //Suffix
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["AddSpouseAsCoBorrower"].QuestionText); //BorrowerAddSpouseAsCoBorrower
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["AddAsCoBorrower"].QuestionText); //AddAsCoBorrower
                }
                else if(typeOfCredit == "Joint Credit" && (strMaritalStatusJoint.ToLower() == "married" || strMaritalStatusJoint.ToLower() == "separated"))
                {
                     //Merge First Name, Middle Name and Last Name as Full Name. 
                    var firstName = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["FirstNameMarried"].QuestionId); // "First Name", null);
                    var middleName = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MiddleNameMarried"].QuestionId); // "Middle Name", null);
                    var lastName = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LastNameMarried"].QuestionId); // "Last Name", null);
                    var suffix = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["SuffixMarried"].QuestionId); // "Suffix", null);

                    var addSpouseAsCoBorrower =await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["AddSpouseAsCoBorrower"].QuestionId); // "Suffix", null);
                    var fullName = string.Empty;

                    if (string.IsNullOrEmpty(middleName))
                        fullName = firstName + " " + lastName + " " + suffix;
                    else
                        fullName = firstName + " " + middleName + " " + lastName + " " + suffix;
                        
                    subSectionSummary = await UpdateQuestionText(subSectionSummary, summaryItem.QuestionInformation["FirstNameMarried"].QuestionId, summaryItem.QuestionInformation["FirstName"].QuestionText, summaryItem.QuestionInformation["FirstName"].DisplayQuestionText, fullName); //Full Name
                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["FirstNameMarried"].QuestionId, null));
                    
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null ,summaryItem.QuestionInformation["MiddleNameMarried"].QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null ,summaryItem.QuestionInformation["LastNameMarried"].QuestionId); // "Middle Name");
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null,summaryItem.QuestionInformation["SuffixMarried"].QuestionId); //Suffix

                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["FirstNameMarried"].QuestionId, null));

                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["AddSpouseAsCoBorrower"].QuestionId, null));
                    if(addSpouseAsCoBorrower.ToLower() == "true"){
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BirthDate"].QuestionId, null));
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["SSN"].QuestionId, null));
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["Email"].QuestionId, null));
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["Phone"].QuestionId, null));
                    }

                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["AddAsCoBorrower"].QuestionId, null));
                }
                else
                {
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["FirstName"].QuestionText);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["LastName"].QuestionText);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["MiddleName"].QuestionText); // "Middle Name");
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, summaryItem.QuestionInformation["Suffix"].QuestionText); //Suffix

                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null,summaryItem.QuestionInformation["BirthDate"].QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null,summaryItem.QuestionInformation["SSN"].QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null,summaryItem.QuestionInformation["Email"].QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null,summaryItem.QuestionInformation["AddAsCoBorrower"].QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, null,summaryItem.QuestionInformation["AddSpouseAsCoBorrower"].QuestionId);
                }
            }
            else if (subSectionSummary.SubSectionType == "Co-Borrower Information")
            {
                var coBorrowerInfoQuestions = subSectionSummary.SubSection.QuestionSections.Where(x => x.QuestionList != null)
                                .SelectMany(qs => qs.QuestionList)
                                .Where(q => !string.IsNullOrEmpty(q.Answer));
                if (coBorrowerInfoQuestions != null)
                {
                    IQuestion objNumberOfCoBorrowers = new Question();
                    objNumberOfCoBorrowers.QuestionType = QuestionType.Text;
                    objNumberOfCoBorrowers.QuestionText = $"Number of Co-Borrowers";
                    objNumberOfCoBorrowers.Answer = coBorrowerInfoQuestions.Count().ToString();
                    newQuestionSequence.Add(objNumberOfCoBorrowers);

                    var counter = 1;
                    foreach (var coBorrowerInfoQuestion in coBorrowerInfoQuestions)
                    {
                        if(!string.IsNullOrEmpty(coBorrowerInfoQuestion.Answer))
                        {
                            var objAddCoBorrowerModal = JsonConvert.DeserializeObject<AddCoBorrowerModal>(coBorrowerInfoQuestion.Answer);

                            if(objAddCoBorrowerModal != null)
                            {
                                IQuestion objBorrowerFullNameLabel = new Question();
                                objBorrowerFullNameLabel.QuestionType = QuestionType.Text;
                                objBorrowerFullNameLabel.QuestionText = $"Co-Borrower {counter} Full Name";
                                objBorrowerFullNameLabel.Answer = objAddCoBorrowerModal.FirstName + " " + objAddCoBorrowerModal.LastName;
                                newQuestionSequence.Add(objBorrowerFullNameLabel);
                                
                                IQuestion objCoBorrowerEmailLabel = new Question(); 
                                objCoBorrowerEmailLabel.QuestionType = QuestionType.Text;
                                objCoBorrowerEmailLabel.QuestionText = "Email";
                                objCoBorrowerEmailLabel.Answer = objAddCoBorrowerModal.EmailAddress;
                                newQuestionSequence.Add(objCoBorrowerEmailLabel);

                                IQuestion objCoBorrowerRelationLabel = new Question();
                                objCoBorrowerRelationLabel.QuestionType = QuestionType.Text;
                                objCoBorrowerRelationLabel.QuestionText = "Relationship";
                                objCoBorrowerRelationLabel.Answer = objAddCoBorrowerModal.Relationship.ToString();
                                newQuestionSequence.Add(objCoBorrowerRelationLabel);                        

                                counter++;
                            }
                        }
                    }

                    var questionSections = new List<IQuestionSection>();
                    IQuestionSection questionSection = new QuestionSection();
                    questionSection.QuestionList = new List<IQuestion>();
                    questionSection.QuestionList.AddRange(newQuestionSequence);
                    questionSections.Add(questionSection);
                    subSectionSummary.SubSection.QuestionSections = questionSections;
                }
            }
            else if (subSectionSummary.SubSectionType == purchaseAboutLoanSection || subSectionSummary.SubSectionType == refinanceAboutLoanSection)
            {
                if (subSectionSummary.SubSectionType == refinanceAboutLoanSection)
                {
                    var answer = await GetAnswerFromQuestionText(subSectionSummary, null, "85");

                    if (answer == "false")
                    {
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, "86");
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, "87");
                    }
                }
                subSectionSummary.SubSectionType = summaryItem.SubSectionType = "About Loan";
            }
            else if (subSectionSummary.SubSectionType == "Current Residence Address" && subSectionSummary.SubSectionTypeInfo != "Spouse Current Residence Address" && subSectionSummary.SubSectionTypeInfo != "CoBorrower Current Residence Address")
            {
                ////Question Added for the summary display
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["ResidenseSameOrSpouse"].DisplayQuestionText));
                if (!string.IsNullOrEmpty(LoanPurpose) && LoanPurpose.ToLower() == REFINANCE.ToLower())
                {
                    var borrowerCurrentAddressSame = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerCurrentAddressSame"].QuestionId, null);
                    newQuestionSequence.Add(borrowerCurrentAddressSame);
                    var appendString = string.Empty;
                    if(borrowerCurrentAddressSame != null && borrowerCurrentAddressSame.Answer == "true")
                    {
                        var housingExpense = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["HousingExpenseCheckBoxTrue"].QuestionId ,null);
                        appendString = Constant.RefinanceRentOrOwnCheckBoxTruePostFix;
                        newQuestionSequence.Add(housingExpense);
                        subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].QuestionText, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText, null, true); // "When did you start living here (MM/YYYY)"
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary,summaryItem.QuestionInformation["CurrentAddressCheckBoxTrue"].QuestionId,null));
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText));
                        try
                        {
                            var sameMailingAddressQuestion = subSectionSummary.SubSection
                                            .QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionFieldName == "BorrowerDifferentMailingAddress").FirstOrDefault();
                            if (sameMailingAddressQuestion!= null && sameMailingAddressQuestion.Answer == "true")
                            {
                                newQuestionSequence.Add(sameMailingAddressQuestion);
                                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].QuestionText, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText, null); // "Mailing Address"
                                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText));
                            }
                        }
                        catch { }
                        if (housingExpense != null)
                        {
                            subSectionSummary =  await ProcessPropertyInfo(subSectionSummary, summaryItem, newQuestionSequence,appendString);
                        }
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["SellCurrentHome"].DisplayQuestionText));
                    }
                    else
                    {
                        var housingExpense = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary,summaryItem.QuestionInformation["HousingExpenseCheckBoxFalse"].QuestionId,null);
                        appendString = Constant.RefinanceRentOrOwnCheckBoxFalsePostFix;
                        newQuestionSequence.Add(housingExpense);
                        subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].QuestionText, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText, null, true); // "When did you start living here (MM/YYYY)"
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["CurrentAddressCheckBoxFalse"].QuestionId,null));
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText));
                        try
                        {
                            var sameMailingAddressQuestion = subSectionSummary.SubSection
                                            .QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionFieldName == "BorrowerDifferentMailingAddress").FirstOrDefault();
                            if (sameMailingAddressQuestion!= null && sameMailingAddressQuestion.Answer == "true")
                            {
                                newQuestionSequence.Add(sameMailingAddressQuestion);
                                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].QuestionText, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText, null); // "Mailing Address"
                                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText));
                            }
                        }
                        catch { }
                        if (housingExpense != null && housingExpense.Answer == "Own")
                        {
                            subSectionSummary =  await ProcessPropertyInfo(subSectionSummary, summaryItem, newQuestionSequence,appendString);
                        }

                        if (housingExpense != null && housingExpense.Answer == Constant.Rent)
                        {
                            newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation[Constant.MonthlyRentalAmountCheckBoxFalse].QuestionId,null));
                        }

                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["SellCurrentHome"].DisplayQuestionText));
                    }
                }
                else
                {
                    var housingExpense = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["HousingExpense"].QuestionId, null);
                    newQuestionSequence.Add(housingExpense);

                    subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["CurrentAddress"].QuestionText, summaryItem.QuestionInformation["CurrentAddress"].DisplayQuestionText, null); // "Current Address"
                    subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].QuestionText, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText, null, true); // "When did you start living here (MM/YYYY)"
                    
                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["CurrentAddress"].DisplayQuestionText));
                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText));
                    try
                    {
                        var sameMailingAddressQuestion = subSectionSummary.SubSection
                                        .QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionFieldName == "BorrowerDifferentMailingAddress").FirstOrDefault();
                        if (sameMailingAddressQuestion!= null && sameMailingAddressQuestion.Answer == "true")
                        {
                            newQuestionSequence.Add(sameMailingAddressQuestion);
                            subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].QuestionText, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText, null); // "Mailing Address"
                            newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText));
                        }
                    }
                    catch { }
                
                    if (housingExpense != null && housingExpense.Answer == "Own")
                    {
                        subSectionSummary =  await ProcessPropertyInfo(subSectionSummary, summaryItem, newQuestionSequence,null);
                    }

                    if (housingExpense != null && housingExpense.Answer == Constant.Rent)
                    {
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation[Constant.MonthlyRentalAmount].QuestionId,null));
                    }

                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["SellCurrentHome"].DisplayQuestionText));
                }                

                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);

                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if (subSectionSummary.SubSectionTypeInfo == "Spouse Current Residence Address")
            {
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["CurrentAddress"].QuestionText, summaryItem.QuestionInformation["CurrentAddress"].DisplayQuestionText, null); // "Current Address"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].QuestionText, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText, null, true); // "When did you start living here (MM/YYYY)"

                var housingExpense = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["HousingExpense"].QuestionId, null);
                newQuestionSequence.Add(housingExpense);

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["CurrentAddress"].DisplayQuestionText));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText));

                try
                {
                    var sameMailingAddressQuestion = subSectionSummary.SubSection
                                    .QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionFieldName == "BorrowerDifferentMailingAddress").FirstOrDefault();
                    if (sameMailingAddressQuestion!= null && sameMailingAddressQuestion.Answer == "true")
                    {
                        newQuestionSequence.Add(sameMailingAddressQuestion);
                        subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].QuestionText, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText, null); // "Mailing Address"
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText));
                    }
                }
                catch { }

                if (housingExpense != null && housingExpense.Answer == "Own")
                {
                    subSectionSummary = await ProcessPropertyInfo(subSectionSummary, summaryItem, newQuestionSequence, null);
                }

                if (housingExpense != null && housingExpense.Answer == Constant.Rent)
                {
                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation[Constant.MonthlyRentalAmount].QuestionId ,null));
                }

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["SellCurrentHome"].DisplayQuestionText));

                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);

                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if (subSectionSummary.SubSectionTypeInfo == "CoBorrower Current Residence Address")
            {
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["ResidenseSameOrSpouse"].DisplayQuestionText));

                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["CurrentAddress"].QuestionText, summaryItem.QuestionInformation["CurrentAddress"].DisplayQuestionText, null); // "Current Address"
                subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].QuestionText, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText, null,true); // "When did you start living here (MM/YYYY)"

                var housingExpense = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["HousingExpense"].QuestionId, null);
                newQuestionSequence.Add(housingExpense);

                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["CurrentAddress"].DisplayQuestionText));
                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["LivingSince"].DisplayQuestionText));

                try
                {
                    var sameMailingAddressQuestion = subSectionSummary.SubSection
                                    .QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionFieldName == "BorrowerDifferentMailingAddress").FirstOrDefault();
                    if (sameMailingAddressQuestion!= null && sameMailingAddressQuestion.Answer == "true")
                    {
                        newQuestionSequence.Add(sameMailingAddressQuestion);
                        subSectionSummary = await UpdateQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].QuestionText, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText, null); // "Mailing Address"
                        newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, null, summaryItem.QuestionInformation["MailingAddress"].DisplayQuestionText));
                    }
                }
                catch { }
                
                if (housingExpense != null && housingExpense.Answer == "Own")
                {
                    subSectionSummary = await ProcessPropertyInfo(subSectionSummary, summaryItem, newQuestionSequence,null);
                }

                if (housingExpense != null && housingExpense.Answer == Constant.Rent)
                {
                    newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation[Constant.MonthlyRentalAmount].QuestionId, null));
                }


                newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["SellCurrentHome"].QuestionId, null));

                var questionSections = new List<IQuestionSection>();
                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);
                questionSections.Add(questionSection);

                subSectionSummary.SubSection.QuestionSections = questionSections;
            }
            else if (subSectionSummary.SubSectionType == alimonychildSection)
            {
                var isSupport = summaryItem.QuestionInformation["IsChildAlimonySupport"];
                var perMonth = summaryItem.QuestionInformation["PerMonth"];
                var startDate = summaryItem.QuestionInformation["StartDate"];

                var perMonthQn = subSectionSummary.SubSection.QuestionSections
                                                           .SelectMany(qs => qs.QuestionList)
                                                           .FirstOrDefault(q => q.QuestionText.Equals(perMonth.QuestionText));
                var startDateQn = subSectionSummary.SubSection.QuestionSections
                                                           .SelectMany(qs => qs.QuestionList)
                                                           .FirstOrDefault(q => q.QuestionText.Equals(startDate.QuestionText));

                if (perMonthQn != null && startDateQn != null)
                {
                    if ((string.IsNullOrWhiteSpace(perMonthQn.Answer) || perMonthQn.Answer == "0") &&
                        string.IsNullOrWhiteSpace(startDateQn.Answer) || startDateQn.Answer == "0")
                    {
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, isSupport.QuestionId);
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, perMonth.QuestionId);
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, startDate.QuestionId);
                    }
                }
                else if(perMonthQn != null && perMonthQn.QuestionFieldName == Constant.BorrowerIncomeAlimonySupport)
                {
                     //This is for new changes
                    if(string.IsNullOrWhiteSpace(perMonthQn.Answer) || perMonthQn.Answer == "0")
                    {
                       subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, isSupport.QuestionId);
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, perMonth.QuestionId);
                    }
                    else {
                        //need to parse the respose and based on amount need to remove the qeustion.
                        try
                        {
                            
                                var obj = JsonConvert.DeserializeObject<Dictionary<string,string>>(perMonthQn.Answer);
                                Int64 objAmount = 0;
                                    string  objStartDate = string.Empty;
                                if(obj.ContainsKey(Constant.AlimonyAmount))
                                {
                                    var amount = obj[Constant.AlimonyAmount];
                                
                                        if(!string.IsNullOrEmpty(amount))
                                        {
                                            objAmount = Convert.ToInt64(amount);                                                                        
                                        }
                                    }

                                    if(obj.ContainsKey(Constant.AlimonyStartDate))
                                {
                                        var startDateVAlue = obj[Constant.AlimonyStartDate];
                                            
                                        if(!string.IsNullOrEmpty(startDateVAlue))
                                        {
                                            objStartDate = startDateVAlue;
                                        }
                                    }

                                if(objAmount == 0 || string.IsNullOrEmpty(objStartDate))
                                {
                                    subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, isSupport.QuestionId);
                                    subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, perMonth.QuestionId);
                                }
                            
                        }
                        catch
                        {
                            //answer is not in proper format
                            subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, isSupport.QuestionId);
                            subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, perMonth.QuestionId);
                        }

                    } 
                }               
               
                var childName = summaryItem.QuestionInformation["ChildName"].QuestionText;
                var childDOB = summaryItem.QuestionInformation["ChildDOB"].QuestionText;

                foreach (var qs in subSectionSummary.SubSection.QuestionSections.Where(x => x.QuestionSectionTemplateId == "childSupportQuestions"))
                {
                    var nameQn = qs.QuestionList.First(q => q.QuestionText.Equals(childName));
                    var dobQn = qs.QuestionList.First(q => q.QuestionText.Equals(childDOB));

                    if (string.IsNullOrWhiteSpace(nameQn.Answer))
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, nameQn.QuestionId);
                    if (string.IsNullOrWhiteSpace(dobQn.Answer))
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, dobQn.QuestionId);
                }
            }
            else if (subSectionSummary.SubSectionType == employmentSection)
            {
                var optionalIncomeQnId = summaryItem.QuestionInformation["OptionalIncome"].QuestionId;
                var optionalIncomeBonus = summaryItem.QuestionInformation["Bonus"];
                var optionalIncomeCommission = summaryItem.QuestionInformation["Commission"];
                var optionalIncomeOvertime = summaryItem.QuestionInformation["Overtime"];

                var currentlyWorkHereQuestion = subSectionSummary.SubSection.QuestionSections
                                   .SelectMany(qs => qs.QuestionList)
                                   .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["CurrentlyWorkHere"].QuestionId);

                if (currentlyWorkHereQuestion != null)
                {
                    if (!string.IsNullOrWhiteSpace(currentlyWorkHereQuestion.Answer))
                    {
                        var selectedQuestionIds = currentlyWorkHereQuestion.ChildQuestions.Where(c => c.Answer.ToLower() != currentlyWorkHereQuestion.Answer.ToLower()).ToList().Select(x => x.QuestionId);
                        if (selectedQuestionIds != null)
                        {
                            foreach(var questionId in selectedQuestionIds)
                            {
                                subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, questionId);
                            }                        
                        }

                        if(currentlyWorkHereQuestion.Answer.ToLower() == "true")
                        {
                            currentlyWorkHereQuestion.Answer = "Yes";
                        }
                        else
                        {
                            currentlyWorkHereQuestion.Answer = "No";
                        }
                    }
                }

                if(summaryItem.QuestionInformation["EmployedFamily"] != null)
                {
                    var EmployedFamilyQuestion = subSectionSummary.SubSection.QuestionSections
                                    .SelectMany(qs => qs.QuestionList)
                                    .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["EmployedFamily"].QuestionId);

                    if (EmployedFamilyQuestion != null)
                    {
                        subSectionSummary = await UpdateQuestionText(subSectionSummary, summaryItem.QuestionInformation["EmployedFamily"].QuestionId, summaryItem.QuestionInformation["EmployedFamily"].QuestionText, summaryItem.QuestionInformation["EmployedFamily"].DisplayQuestionText, null,true); 
                    
                        if (!string.IsNullOrWhiteSpace(EmployedFamilyQuestion.Answer))
                        {   
                            EmployedFamilyQuestion.Mask=null;                     
                            if(EmployedFamilyQuestion.Answer.ToLower() == "true")
                            {
                                EmployedFamilyQuestion.Answer = "Yes";
                            }
                            else
                            {
                                EmployedFamilyQuestion.Answer = "No";
                            }
                        }
                    }
                }

                var answer = await GetAnswerFromQuestionText(subSectionSummary, null, optionalIncomeQnId);
                //subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeQnId);

                if (!string.IsNullOrWhiteSpace(answer))
                {
                    subSectionSummary = await UpdateQuestionText(subSectionSummary, summaryItem.QuestionInformation["OptionalIncome"].QuestionId, summaryItem.QuestionInformation["OptionalIncome"].QuestionText, summaryItem.QuestionInformation["OptionalIncome"].DisplayQuestionText, null,true); //"Optional Income"
                   
                    var answerList = JsonConvert.DeserializeObject<List<string>>(answer);

                    if (summaryItem.QuestionInformation["OptionalIncome"].QuestionId  != null)
                    {
                       var answerstring = (string.Join(",", answerList));                       
                       var questionAnswer = subSectionSummary.SubSection.QuestionSections
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["OptionalIncome"].QuestionId);
                        if(questionAnswer != null)
                            questionAnswer.Answer = answerstring;
                    }      

                    if (!answerList.Contains(optionalIncomeBonus.QuestionText))
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeBonus.QuestionId);
                    if (!answerList.Contains(optionalIncomeCommission.QuestionText))
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeCommission.QuestionId);
                    if (!answerList.Contains(optionalIncomeOvertime.QuestionText))
                        subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeOvertime.QuestionId);
                }
                else
                {
                    subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeBonus.QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeCommission.QuestionId);
                    subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, optionalIncomeOvertime.QuestionId);
                }
            }
            else if (subSectionSummary.SubSectionType == businessSelfEmployment)
            {
                var currentlyWorkHereQuestion = subSectionSummary.SubSection.QuestionSections
                                   .SelectMany(qs => qs.QuestionList)
                                   .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["CurrentlyWorkHere"].QuestionId);

                if (currentlyWorkHereQuestion != null)
                {
                    if (!string.IsNullOrWhiteSpace(currentlyWorkHereQuestion.Answer))
                    {
                        var selectedQuestionIds = currentlyWorkHereQuestion.ChildQuestions.Where(c => c.Answer.ToLower() != currentlyWorkHereQuestion.Answer.ToLower()).ToList().Select(x => x.QuestionId);
                        if (selectedQuestionIds != null)
                        {
                            foreach(var questionId in selectedQuestionIds)
                            {
                                subSectionSummary.SubSection = await RemoveQuestionByIdFromSubSection(subSectionSummary.SubSection, questionId);
                            }                        
                        }

                        if(currentlyWorkHereQuestion.Answer.ToLower() == "true")
                        {
                            currentlyWorkHereQuestion.Answer = "Yes";
                        }
                        else
                        {
                            currentlyWorkHereQuestion.Answer = "No";
                        }    
                    }
                }
            }
            else if (subSectionSummary.SubSectionType == purchasePropTitle)
            {
                //Thesse below values will come from config.
                ISubSection subSectionAddress = new SubSection();

                var propertySelected = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["PropertySelected"].QuestionId);
                var addressAnswer = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["PurchasingAddress"].QuestionId);
                
                var addressRefinanceAnswer = await GetAnswerFromQuestionText(subSectionSummary, null, summaryItem.QuestionInformation["RefinanceAddress"].QuestionId);
                
                var cityAnswer = ""; var zipAnswer = ""; var stateAnswer = "";

                if (string.IsNullOrEmpty(LoanPurpose) || LoanPurpose.ToLower() == PURCHASE.ToLower())
                {
                    cityAnswer = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["City"].QuestionText, null);
                    zipAnswer = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["ZipCode"].QuestionText, null);
                    stateAnswer = await GetAnswerFromQuestionText(subSectionSummary, summaryItem.QuestionInformation["State"].QuestionText, null);

                    var propertySelectedQuestion = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).First(q => q.QuestionId == summaryItem.QuestionInformation["PropertySelected"].QuestionId);
                    var contractSelectedQuestion = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).First(q => q.QuestionId == summaryItem.QuestionInformation["ContractSelected"].QuestionId);

                    subSectionSummary.SubSection = await RemoveQuestionFromSubSection(subSectionSummary.SubSection, propertySelectedQuestion.QuestionText);

                    if (propertySelected.ToLower() == "true")
                    {
                        newQuestionSequence.Add(propertySelectedQuestion);
                        newQuestionSequence.Add(contractSelectedQuestion);
                        
                        if (addressAnswer != "" && addressAnswer != null)
                        {
                            var questionAnswer = subSectionSummary.SubSection.QuestionSections
                                   .SelectMany(qs => qs.QuestionList)
                                   .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["PurchasingAddress"].QuestionId);

                            //The business address was to be set only for the Income section.
                            //questionAnswer.QuestionType = QuestionType.Address;

                            var questionList = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionId != summaryItem.QuestionInformation["PurchasingAddress"].QuestionId).ToList();
                            questionAnswer.QuestionText = summaryItem.QuestionInformation["PurchasingAddress"].DisplayQuestionText;
                            newQuestionSequence.Add(questionAnswer);
                        }
                    }
                    else
                    {
                        newQuestionSequence.Add(propertySelectedQuestion);
                        if (cityAnswer != "" && cityAnswer != null)
                        {
                            var questionAnswer = subSectionSummary.SubSection.QuestionSections
                                   .SelectMany(qs => qs.QuestionList)
                                   .FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["City"].QuestionText);                            
                            newQuestionSequence.Add(questionAnswer);
                        }

                        if(stateAnswer != "" && stateAnswer != null)
                        {
                            var questionAnswer = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["State"].QuestionText);

                            newQuestionSequence.Add(questionAnswer);
                        }

                        if (zipAnswer != "" && zipAnswer != null)
                        {
                            var questionAnswer = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["ZipCode"].QuestionText);

                            newQuestionSequence.Add(questionAnswer);
                        }

                        
                    }
                }
                else if (LoanPurpose.ToLower() == REFINANCE.ToLower())
                {
                    if (addressRefinanceAnswer != "" && addressRefinanceAnswer != null)
                    {
                        var questionAnswer = subSectionSummary.SubSection.QuestionSections
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["RefinanceAddress"].QuestionId);

                        var questionList = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionId != summaryItem.QuestionInformation["PurchasingAddress"].QuestionId).ToList();

                        questionAnswer.QuestionText = "Refinance Address";

                        newQuestionSequence.Add(questionAnswer);
                    }
                }

                var propertyTypeQuestionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["PropertyType"].QuestionText);
	            if (LoanPurpose.ToLower() == REFINANCE.ToLower())
                {
                    newQuestionSequence.Add(propertyTypeQuestionAnswer);
                }
                else if(propertySelected.ToLower() == "true")
                {
                    newQuestionSequence.Add(propertyTypeQuestionAnswer);
                }

                var propertySelectedQuestionId = "12";
                var propertyNotSelectedQuestionId = "1212";
                var propertyUseQuestionId = propertySelectedQuestionId;
                if (propertySelected.ToLower() == "false")
                {
                    propertyUseQuestionId = propertyNotSelectedQuestionId;
                }
                var propertyUseQuestionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["PropertyUse"].QuestionText
                            && q.QuestionId == propertyUseQuestionId);
                
                newQuestionSequence.Add(propertyUseQuestionAnswer);
          
                try
                {
                    if (propertySelected.ToLower() == "true" || LoanPurpose.ToLower() == REFINANCE.ToLower())
                    {
                        // TODO multi-family should be from configuration or constant
                        // Show Number of units only if its Multi-Family
                        if (propertyTypeQuestionAnswer != null && propertyTypeQuestionAnswer.Answer == "Multi-Family")
                        {
                            // PropertyTypeUnits
                            var propertyTypeUnitsQuestionAnswer = subSectionSummary.SubSection.QuestionSections
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["PropertyTypeUnits"].QuestionText);

                            newQuestionSequence.Add(propertyTypeUnitsQuestionAnswer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //For summary data if some issue is there in retrival, it should not break the flow
                    Logger.Error("Error occcured while processing Number Of Units",ex);
                }

                try
                {
                    //To show only for Purchase
                    if (LoanPurpose.ToLower() == PURCHASE.ToLower() && propertySelected.ToLower() == "true")
                    {                        
                        // TODO multi-family & Investment Property should be from configuration or constant
                        // borrowerExpectedMonthlyRentalIncome
                        // is shown when property type is multi-family or propertyuse is investment property
                        if ((propertyTypeQuestionAnswer != null && propertyTypeQuestionAnswer.Answer == "Multi-Family") || 
                            (propertyUseQuestionAnswer != null && propertyUseQuestionAnswer.Answer.Equals("Investment Property")))
                        {
                            var borrowerExpectedMonthlyRentalIncome = subSectionSummary.SubSection.QuestionSections
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionText == summaryItem.QuestionInformation["BorrowerExpectedMonthlyRentalIncome"].QuestionText);
                    
                            newQuestionSequence.Add(borrowerExpectedMonthlyRentalIncome);    
                        }
                    }
                }
                catch (Exception ex)
                {
                    //For summary data if some issue is there in retrival, it should not break the flow
                    Logger.Error("Error occcured while processing BorrowerExceptedMontlyRentalIncome",ex);
                }
                

                IQuestion questionToMerge = new Question();

                IQuestionSection questionSection = new QuestionSection();
                questionSection.QuestionList = new List<IQuestion>();
                questionSection.QuestionList.AddRange(newQuestionSequence);

                subSectionSummary.SubSection.QuestionSections = new List<IQuestionSection>();
                subSectionSummary.SubSection.QuestionSections.Add(questionSection);
            }
            else if (subSectionSummary.SubSectionType == "Rental Income")
            {
                try
                {
                    if (subSectionSummary.SubSection.QuestionSections != null)
                    {
                        var qsPropertyType = subSectionSummary.SubSection.QuestionSections.FirstOrDefault(x =>
                            x.QuestionSectionName == "property type");

                        var questions = new List<IQuestion>();

                        if (qsPropertyType != null)
                        {
                            var questionPropertyType = qsPropertyType.QuestionList.Where(x=>x.QuestionFieldName =="BorrowerIncomeRentPropertyType").FirstOrDefault();
                            if (questionPropertyType != null)
                            {
                                questions.Add(questionPropertyType);

                                if (questionPropertyType.Answer == "Multi-Family")
                                {
                                    var questionPropertyTypeUnits = qsPropertyType.QuestionList.Where(x=>x.QuestionFieldName =="BorrowerIncomeRentUnits").FirstOrDefault();
                                    questions.Add(questionPropertyTypeUnits);
                                }

                                qsPropertyType.QuestionList = questions;
                            }
                        }
                    }

                    await calculateNetRentalMonthlyIncome(subSectionSummary,summaryItem);
                }
                catch (Exception ex)
                {
                    Logger.Error("Error occurred while processing rental income in ProcessSubSectionSummary", ex);
                }
            }
            else if (subSectionSummary.SubSectionType == "Current Properties Own")
            {
                try
                {
                    if (subSectionSummary.SubSection.QuestionSections != null)
                    {
                        var qsPropertyType = subSectionSummary.SubSection.QuestionSections.FirstOrDefault(x =>
                            x.QuestionSectionName == "property type");

                        var questions = new List<IQuestion>();

                        if (qsPropertyType != null)
                        {
                            var questionPropertyType = qsPropertyType.QuestionList.Where(x => x.QuestionFieldName == "BorrowerREOPropertyType").FirstOrDefault();
                            if (questionPropertyType != null)
                            {
                                questions.Add(questionPropertyType);

                                if (questionPropertyType.Answer == "Multi-Family")
                                {
                                    var questionPropertyTypeUnits = qsPropertyType.QuestionList.Where(x => x.QuestionFieldName == "BorrowerREOPropertyTypeUnits").FirstOrDefault();
                                    questions.Add(questionPropertyTypeUnits);
                                }

                                qsPropertyType.QuestionList = questions;
                            }
                        }
                    }

                    //Need to calculate net monthly rental income 
                    await calculateNetRentalMonthlyIncome(subSectionSummary,summaryItem);                 
                   
                }
                catch (Exception ex)
                {
                    Logger.Error("Error occurred while processing Current Properties Own in ProcessSubSectionSummary", ex);
                }
            }
            //Below implementation is to handle Declaration section
            else if (subSectionSummary.SubSectionType.ToLower() == "declarations")
            {
                IQuestion previousQuestion = new Question();
                var ParentAns = "";
                var includeChildQuestions = false;
                var ParentQuestionIds = new List<string>();
                var childQuestions = new List<IChildConditionalQuestion>();
                var childQuestionIds = new List<string>();
                var excludeChildQuestionIds = new List<string>();
                // bool isIDoNotWishToFurnishQuestion;

                // ParentQuestionIds.Add(summaryItem.QuestionInformation["IdoNotFurnish"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["OwnershipInterest"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["PrimaryResidence"].QuestionId);    
                ParentQuestionIds.Add(summaryItem.QuestionInformation["Ethnicity"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["Race"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["Sex"].QuestionId);
                
                ParentQuestionIds.Add(summaryItem.QuestionInformation["EthnicityOther"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["RaceOther"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["SexOther"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["OwnershipInterestOther"].QuestionId);
                ParentQuestionIds.Add(summaryItem.QuestionInformation["PrimaryResidenceOther"].QuestionId);
                
                foreach (var questionAnswer in subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList))
                {

                     //added statt
                    var originalQuestionText = string.Empty;
                     //added end
                    
                    // isIDoNotWishToFurnishQuestion = false;
                    if (ParentQuestionIds.Contains(questionAnswer.QuestionId)) // == IDoNotNeedToFurnishQuestionId)
                    {
                        //Get Display Question Text
                        foreach (var questionInfo in summaryItem.QuestionInformation)
                        {
                            var questionValue = questionInfo.Value;
                            if (questionAnswer.QuestionId == questionValue.QuestionId)
                            {
                                 //added start
                                originalQuestionText = questionAnswer.QuestionText;
                                 //added end

                                // isIDoNotWishToFurnishQuestion = true;
                                questionAnswer.QuestionText = string.IsNullOrEmpty(questionValue.DisplayQuestionText) ? questionAnswer.QuestionText : questionValue.DisplayQuestionText;
                            }
                        }

                        //Get a new instance of same Questionnaire as it will have all the questions, the present questionnaire does not have all the questions.

                        //ParentAns = string.IsNullOrEmpty(questionAnswer.Answer) ? await GetChildAnswers(subSectionSummary, questionAnswer.ChildQuestions, questionAnswer.Answer) : questionAnswer.Answer;
                        //ParentAns = await GetChildAnswers(subSectionSummary, questionAnswer.ChildQuestions, questionAnswer.Answer, questionnaire);
                        ParentAns = questionAnswer.Answer;
                        // questionAnswer.Answer = string.IsNullOrEmpty(questionAnswer.Answer) == false || string.IsNullOrEmpty(ParentAns) == false ? questionAnswer.Answer + " > " + ParentAns : questionAnswer.Answer + " " + ParentAns;
                        childQuestions = questionAnswer.ChildQuestions;
                        if (childQuestions != null) 
                        {
                               var isMultiAnswer = false;
                            childQuestionIds = childQuestions.Select(q => q.QuestionId).ToList();

                            //added  start
                            if(questionAnswer.QuestionId == summaryItem.QuestionInformation["Ethnicity"].QuestionId 
                            || questionAnswer.QuestionId == summaryItem.QuestionInformation["Race"].QuestionId
                            || questionAnswer.QuestionId == summaryItem.QuestionInformation["Sex"].QuestionId
                            || questionAnswer.QuestionId == summaryItem.QuestionInformation["EthnicityOther"].QuestionId
                            || questionAnswer.QuestionId == summaryItem.QuestionInformation["RaceOther"].QuestionId
                            ||  questionAnswer.QuestionId == summaryItem.QuestionInformation["SexOther"].QuestionId                         
                            )
                            {
                                includeChildQuestions = false;
                                if(ParentAns.ToLower() == "true")
                                {
                                    questionAnswer.Answer = originalQuestionText;
                                }
                                else
                                {      
                                    var childQuestionAnswer = string.Empty;
                                 
                                    if(questionAnswer.QuestionId == summaryItem.QuestionInformation["Ethnicity"].QuestionId 
                                        || questionAnswer.QuestionId == summaryItem.QuestionInformation["Race"].QuestionId                            
                                        || questionAnswer.QuestionId == summaryItem.QuestionInformation["EthnicityOther"].QuestionId
                                        || questionAnswer.QuestionId == summaryItem.QuestionInformation["RaceOther"].QuestionId                         
                                        )
                                        {
                                            isMultiAnswer = true;
                                        }
                                    //now need to merge both the child question into answer array.
                                    foreach(var questionId in childQuestionIds)
                                    {
                                       var childquestion =  subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.QuestionId == questionId).FirstOrDefault();
                                       if(!string.IsNullOrWhiteSpace(childquestion.Answer.Trim()))
                                        {
                                            if(string.IsNullOrEmpty(childQuestionAnswer))
                                            {
                                                if( questionAnswer.QuestionId == summaryItem.QuestionInformation["Sex"].QuestionId || questionAnswer.QuestionId == summaryItem.QuestionInformation["SexOther"].QuestionId)
                                                {
                                                    try
                                                    {
                                                        if(!string.IsNullOrEmpty(childquestion.Answer))
                                                        {
                                                            var token = JToken.Parse(childquestion.Answer);   
                                                            IEnumerable<String> maleFemale;
                                                            if (token is JArray)
                                                            {
                                                                maleFemale = token.ToObject<List<String>>();
                                                                 childQuestionAnswer = string.Join( ",", maleFemale.ToArray() );
                                                            }
                                                        }
                                                    }
                                                    catch(Exception)
                                                    {
                                                            //For backward compatiblity. 
                                                             childQuestionAnswer = childquestion.Answer;                    
                                                    }
                                                }
                                                else                                             
                                                    childQuestionAnswer = childquestion.Answer;

                                            }
                                            else
                                            {                                               
                                                childQuestionAnswer = childQuestionAnswer + "," + childquestion.Answer;
                                            }
                                        }                                         
                                         questionsToRemove.Add(childquestion);
                                    }

                                    if(!string.IsNullOrEmpty(childQuestionAnswer))
                                    {                                    
                                        if(isMultiAnswer)
                                        {
                                                questionAnswer.QuestionType = QuestionType.TreeView;
                                            questionAnswer.Answer = "[" + childQuestionAnswer + "]";
                                        }
                                        else
                                        {
                                                questionAnswer.Answer = childQuestionAnswer;
                                        }
                                    }
                                    else
                                    {
                                        questionAnswer.Answer = "[[na]]";
                                    }
                                }

                            }
                              //added  end
                            //Added to remove the 2nd level child questions
                            else if(questionAnswer.QuestionId == summaryItem.QuestionInformation["PrimaryResidence"].QuestionId 
                                    || questionAnswer.QuestionId == summaryItem.QuestionInformation["OwnershipInterest"].QuestionId 
                                    || questionAnswer.QuestionId == summaryItem.QuestionInformation["PrimaryResidenceOther"].QuestionId 
                                    || questionAnswer.QuestionId == summaryItem.QuestionInformation["OwnershipInterestOther"].QuestionId)
                            {
                                if(ParentAns.ToLower() == "false" || string.IsNullOrWhiteSpace(ParentAns))
                                {
                                    excludeChildQuestionIds.AddRange(questionAnswer.ChildQuestions.Select(q => q.QuestionId).ToList());
                                }
                                else
                                {
                                    includeChildQuestions = true;
                                }
                            }
                        }
                    }
                    
                    if (childQuestionIds != null && childQuestionIds.Contains(questionAnswer.QuestionId) && ParentAns.ToLower() == "true" && !includeChildQuestions)
                    {
                        questionsToRemove.Add(questionAnswer);
                    }
                   else if((questionAnswer.QuestionId == summaryItem.QuestionInformation["OwnershipInterest"].QuestionId 
                            && (ParentAns.ToLower() == "false" || string.IsNullOrEmpty(ParentAns)))
                            || (questionAnswer.QuestionId == summaryItem.QuestionInformation["OwnershipInterestOther"].QuestionId 
                            && (ParentAns.ToLower() == "false" || string.IsNullOrEmpty(ParentAns))))
                    {
                        //Dont add to questionsToRemove, Show the question (m.) in the list, if parent is answered as true
                        if(!(previousQuestion.ChildQuestions != null && previousQuestion.ChildQuestions.Any(x=>x.QuestionId == questionAnswer.QuestionId) && 
                             previousQuestion.Answer.Equals("true",StringComparison.InvariantCultureIgnoreCase))) 
                        {
                            questionsToRemove.Add(questionAnswer);
                        }
                        
                    }
                    else
                    {
                        if (questionAnswer.QuestionType == QuestionType.TextArea)
                        {
                            if(previousQuestion.Answer == "false")
                            {
                                questionAnswer.Answer = "[[false]]";
                            }
                            else if (previousQuestion.Answer == "") //if previous answer is blank then dont show answer in the child question.
                            {
                                //If the Answer is "No", then we need not show the text. At present if the Answer is 'No' we are hiding the child question but we are not making the answr field blank. 
                                //The answer was entered by the user for Yes. So for No the answer should get removed as well, at present the answer is not getting removed when it is No.                         
                                questionAnswer.Answer = "";
                            }

                            if(previousQuestion.Answer == "true" && questionAnswer.Answer == string.Empty)
                            {
                                questionAnswer.Answer = "[[na]]";
                            }

                            questionAnswer.QuestionText = previousQuestion.QuestionText;
                            questionAnswer.QuestionType = QuestionType.DeclarationSelectBool;
                            questionAnswer.QuestionItemNo = previousQuestion.QuestionItemNo;
                            questionAnswer.HelpText = "[[merged]]"; //This question is a merged question.
                                                                    //We are using this property in Summary to manage a condition. There is no other Use of it.                                                                    //If there are no child conditions then treat this question as Yes/No. Else use it as Yes/No + Explanation if the answer is Yes.
                            questionAnswer.ChildQuestions = previousQuestion.ChildQuestions;
                            questionAnswer.Required = previousQuestion.Required;
                            questionsToRemove.Add(previousQuestion);
                        }

                        if (
                            (questionAnswer.QuestionType == QuestionType.SingleToggle || questionAnswer.QuestionType == QuestionType.SingleToggleLoan || questionAnswer.QuestionType == QuestionType.Select) 
                            & previousQuestion.QuestionId != summaryItem.QuestionInformation["Sex"].QuestionId
                            & previousQuestion.QuestionId != summaryItem.QuestionInformation["SexOther"].QuestionId)
                        {
                            //questionAnswer.QuestionText = isIDoNotWishToFurnishQuestion == false ? previousQuestion.QuestionText : questionAnswer.QuestionText;
                            questionAnswer.QuestionText = previousQuestion.QuestionText;
                            questionAnswer.QuestionType = QuestionType.Select;
                            questionsToRemove.Add(previousQuestion);
                        }

                        previousQuestion = questionAnswer;
                    }
                }

                var questionList = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).ToList();

                var result = questionList.Except(questionsToRemove).ToList();

                //Now check whether the question id is there in the config
                for (var i = 0; i < questionsToRemove.Count; i++)
                {
                    var q = questionsToRemove[i]; new Question();
                    q = questionsToRemove[i];
                    questionList.Remove(q);
                }
                //Added to remove the 2nd level child questions.
                for (var i = 0; i < excludeChildQuestionIds.Count; i++)
                {
                    var q = questionList.Where(x=>x.QuestionId == excludeChildQuestionIds[i]).FirstOrDefault();
                    questionList.Remove(q);
                }
                
                ISubSection newSubSection = new SubSection();

                var questionSection = new List<IQuestionSection>();

                newSubSection.QuestionSections = questionSection; //
                newSubSection.QuestionSections.Add(new QuestionSection());
                newSubSection.QuestionSections[0].QuestionList = questionList;
                newSubSection.UserName = subSectionSummary.SubSection.UserName;
                newSubSection.SubSectionId = subSectionSummary.SubSection.SubSectionId;
                newSubSection.SubSectionName = subSectionSummary.SubSection.SubSectionName;
                newSubSection.SeqNo = subSectionSummary.SubSection.SeqNo;
                subSectionSummary.SubSection = newSubSection;
            }
            else if(subSectionSummary.SubSectionType.ToLower() == realEstateSectionType.ToLower())
            {
                var childQuestions = new List<IChildConditionalQuestion>();
                var childQuestionIds = new List<string>();

                var questionList = subSectionSummary.SubSection.QuestionSections.SelectMany(qs => qs.QuestionList).ToList();

                if(questionList != null)
                {
                   
                    if(summaryItem.QuestionInformation != null &&  summaryItem.QuestionInformation.ContainsKey("RealEstateAgent"))
                    {
                       
                        var parentQuestionId = summaryItem.QuestionInformation["RealEstateAgent"].QuestionId;

                        var parentQuestion = questionList.Where(x => x.QuestionId == parentQuestionId).FirstOrDefault();

                        childQuestions = parentQuestion.ChildQuestions;
                        childQuestionIds = childQuestions.Select(q => q.QuestionId).ToList();

                        if(parentQuestion.Answer.ToLower() == "true")
                        {
                            var questionToRemoved = childQuestions.Where(x => x.Answer.ToLower() != parentQuestion.Answer.ToLower()).ToList();
                            var questionIdToRemoved = questionToRemoved.Select(q => q.QuestionId).ToList();
                            questionsToRemove = questionList.Where( x => questionIdToRemoved.Contains(x.QuestionId)).ToList();
                        }
                        else
                        {
                            var questionToRemoved = childQuestions.Where(x => x.Answer.ToLower() != parentQuestion.Answer.ToLower()).ToList();
                            var questionIdToRemoved = questionToRemoved.Select(q => q.QuestionId).ToList();
                            questionsToRemove = questionList.Where( x => questionIdToRemoved.Contains(x.QuestionId)).ToList();
                        }

                        for (var i = 0; i < questionsToRemove.Count; i++)
                        {
                            var q = questionsToRemove[i];                            
                            questionList.Remove(q);
                        }

                        ISubSection newSubSection = new SubSection();

                        var questionSection = new List<IQuestionSection>();

                        newSubSection.QuestionSections = questionSection; //
                        newSubSection.QuestionSections.Add(new QuestionSection());
                        newSubSection.QuestionSections[0].QuestionList = questionList;
                        newSubSection.UserName = subSectionSummary.SubSection.UserName;
                        newSubSection.SubSectionId = subSectionSummary.SubSection.SubSectionId;
                        newSubSection.SubSectionName = subSectionSummary.SubSection.SubSectionName;
                        newSubSection.SeqNo = subSectionSummary.SubSection.SeqNo;
                        subSectionSummary.SubSection = newSubSection;
                    }
                     else
                        {
                        Logger.Debug("QuestionInformation not found for Real Estate Agent in ApplicationSummary Configuration");
                                
                        }
                }
               
            }
            else if (subSectionSummary.SubSectionType == loanOfficerSectionType)
            {
                subSectionSummary = await UpdateQuestionText(subSectionSummary, summaryItem.QuestionInformation["LoanOfficer"].QuestionId, summaryItem.QuestionInformation["LoanOfficer"].QuestionText, summaryItem.QuestionInformation["LoanOfficer"].DisplayQuestionText, null,true); 
                

                 if (summaryItem.QuestionInformation["LoanOfficer"].DisplayQuestionText  != null)
                {
                  var  questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["LoanOfficer"].QuestionId);
                        if(questionAnswer != null)
                        {
                           if(!string.IsNullOrEmpty(questionAnswer.Answer))
                           {                             
                               dynamic obj = JsonConvert.DeserializeObject(questionAnswer.Answer);
                                if(obj != null)
                                {
                                 questionAnswer.Answer = obj.loanOfficerName;         
                                }
                           }
                        }   
                }               
            }

            if (!isAllowBlankFieldData)
            {
                return await Task.Run(() => RemoveNullEmptyOrZeroAnswerQuestions(subSectionSummary));
            }
            else
            {
                return await Task.Run(() => subSectionSummary);
            }
        }

            /// <summary>
            /// This is used to calculate the net monthly rental income for Current Own Property and Rental Income
            /// </summary>
            /// <param name="subSectionSummary"></param>
            /// <param name="summaryItem"></param>
            /// <returns>void</returns>
        private async Task calculateNetRentalMonthlyIncome(ISubSectionSummary subSectionSummary, IQuestionnaireSummaryConfig summaryItem)
        {
            int MonthlyRentalIncome = 0;
            int MonthlyInsuranceTaxes = 0;

            if(summaryItem.QuestionInformation["MonthlyRentalIncome"] != null)
            {
                var MonthlyRentalIncomeQuestion = subSectionSummary.SubSection.QuestionSections
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["MonthlyRentalIncome"].QuestionId);

                if(MonthlyRentalIncomeQuestion != null)
                {
                    if(!string.IsNullOrEmpty(MonthlyRentalIncomeQuestion.Answer))
                    {
                        MonthlyRentalIncome = ConvertStringExpenseToIntValue(MonthlyRentalIncomeQuestion.Answer);
                    }
                }
            }

            if(summaryItem.QuestionInformation["MonthlyInsuranceTaxesAssociationDues"] != null)
            {
                var MonthlyInsuranceTaxesQuestion = subSectionSummary.SubSection.QuestionSections
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["MonthlyInsuranceTaxesAssociationDues"].QuestionId);

                if(MonthlyInsuranceTaxesQuestion != null)
                {
                    if(!string.IsNullOrEmpty(MonthlyInsuranceTaxesQuestion.Answer))
                    {
                        MonthlyInsuranceTaxes = ConvertStringExpenseToIntValue(MonthlyInsuranceTaxesQuestion.Answer);
                    }
                }
            }

            int netMonthlyIncome = MonthlyRentalIncome - MonthlyInsuranceTaxes;

            if(summaryItem.QuestionInformation["NetMonthlyIncome"] != null)
            {
                    
                subSectionSummary = await UpdateQuestionText(subSectionSummary, summaryItem.QuestionInformation["NetMonthlyIncome"].QuestionId, summaryItem.QuestionInformation["NetMonthlyIncome"].QuestionText, summaryItem.QuestionInformation["NetMonthlyIncome"].DisplayQuestionText, null,true);                 
        
                var NetMonthlyIncomeQuestion = subSectionSummary.SubSection.QuestionSections
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["NetMonthlyIncome"].QuestionId);
                if(NetMonthlyIncomeQuestion !=null)
                {
                    NetMonthlyIncomeQuestion.QuestionType = QuestionType.Text;
                    
                    if(netMonthlyIncome < 0)
                    {                                
                        NetMonthlyIncomeQuestion.Answer= $"($ {System.Math.Abs(netMonthlyIncome)})";
                    }
                    else
                    {
                        NetMonthlyIncomeQuestion.Answer= "$ " + Convert.ToString(netMonthlyIncome);
                    }
                }

            }
        }

        private async Task<ISubSectionSummary> ProcessPropertyInfo(ISubSectionSummary subSectionSummary, IQuestionnaireSummaryConfig summaryItem, List<IQuestion> newQuestionSequence, string appendString) 
        {
            if (subSectionSummary == null)
                throw new Exception($" {subSectionSummary} Not found");

            newQuestionSequence.Add(await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerCombineMonthlyHousingExpense"].QuestionId, null));

            var borrowerFirstMortgagePI = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerFirstMortgagePI"].QuestionId, null);
            var borrowerFirstMortgagePIValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, 
                                                                GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerFirstMortgagePIValue"].QuestionId,appendString),
                                                                 null);
            borrowerFirstMortgagePIValue.QuestionText = borrowerFirstMortgagePI.QuestionText;
            borrowerFirstMortgagePIValue.Answer = string.IsNullOrWhiteSpace(borrowerFirstMortgagePIValue.Answer) ? "0" : borrowerFirstMortgagePIValue.Answer;
            newQuestionSequence.Add(borrowerFirstMortgagePIValue);

            var borrowerOtherFinancingPI = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerOtherFinancingPI"].QuestionId, null);
            var borrowerOtherFinancingPIValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerOtherFinancingPIValue"].QuestionId,appendString), null);
            borrowerOtherFinancingPIValue.QuestionText = borrowerOtherFinancingPI.QuestionText;
            borrowerOtherFinancingPIValue.Answer = string.IsNullOrWhiteSpace(borrowerOtherFinancingPIValue.Answer) ? "0" : borrowerOtherFinancingPIValue.Answer;
            newQuestionSequence.Add(borrowerOtherFinancingPIValue);

            var borrowerHazardInsurance = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerHazardInsurance"].QuestionId, null);
            var borrowerHazardInsuranceValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerHazardInsuranceValue"].QuestionId,appendString), null);
            borrowerHazardInsuranceValue.QuestionText = borrowerHazardInsurance.QuestionText;
            borrowerHazardInsuranceValue.Answer = string.IsNullOrWhiteSpace(borrowerHazardInsuranceValue.Answer) ? "0" : borrowerHazardInsuranceValue.Answer;
            newQuestionSequence.Add(borrowerHazardInsuranceValue);

            var borrowerRealEstateTaxes = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerRealEstateTaxes"].QuestionId, null);
            var borrowerRealEstateTaxesValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerRealEstateTaxesValue"].QuestionId,appendString), null);
            borrowerRealEstateTaxesValue.QuestionText = borrowerRealEstateTaxes.QuestionText;
            borrowerRealEstateTaxesValue.Answer = string.IsNullOrWhiteSpace(borrowerRealEstateTaxesValue.Answer) ? "0" : borrowerRealEstateTaxesValue.Answer;
            newQuestionSequence.Add(borrowerRealEstateTaxesValue);

            var borrowerMortgageInsurance = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerMortgageInsurance"].QuestionId, null);
            var borrowerMortgageInsuranceValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerMortgageInsuranceValue"].QuestionId,appendString), null);
            borrowerMortgageInsuranceValue.QuestionText = borrowerMortgageInsurance.QuestionText;
            borrowerMortgageInsuranceValue.Answer = string.IsNullOrWhiteSpace(borrowerMortgageInsuranceValue.Answer) ? "0" : borrowerMortgageInsuranceValue.Answer;
            newQuestionSequence.Add(borrowerMortgageInsuranceValue);

            var borrowerHomeOwnerAssocDues = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerHomeOwnerAssocDues"].QuestionId, null);
            var borrowerHomeOwnerAssocDuesValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerHomeOwnerAssocDuesValue"].QuestionId,appendString), null);
            borrowerHomeOwnerAssocDuesValue.QuestionText = borrowerHomeOwnerAssocDues.QuestionText;
            borrowerHomeOwnerAssocDuesValue.Answer = string.IsNullOrWhiteSpace(borrowerHomeOwnerAssocDuesValue.Answer) ? "0" : borrowerHomeOwnerAssocDuesValue.Answer;
            newQuestionSequence.Add(borrowerHomeOwnerAssocDuesValue);

            var borrowerCombineMonthlyExpenseOther = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerCombineMonthlyExpenseOther"].QuestionId, null);
            var borrowerCombineMonthlyExpenseOtherValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerCombineMonthlyExpenseOtherValue"].QuestionId,appendString), null);
            borrowerCombineMonthlyExpenseOtherValue.QuestionText = borrowerCombineMonthlyExpenseOther.QuestionText;
            borrowerCombineMonthlyExpenseOtherValue.Answer = string.IsNullOrWhiteSpace(borrowerCombineMonthlyExpenseOtherValue.Answer) ? "0" : borrowerCombineMonthlyExpenseOtherValue.Answer;
            newQuestionSequence.Add(borrowerCombineMonthlyExpenseOtherValue);

            var borrowerCombineMonthlyExpenseTotal = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, summaryItem.QuestionInformation["BorrowerCombineMonthlyExpenseTotal"].QuestionId, null);
            var borrowerCombineMonthlyExpenseTotalValue = await GetQuestionFromQuestionTextOrQuestionId(subSectionSummary, GetQuetionIdForRentalScreen(summaryItem.QuestionInformation["BorrowerCombineMonthlyExpenseTotalValue"].QuestionId,appendString), null);
            borrowerCombineMonthlyExpenseTotalValue.QuestionText = borrowerCombineMonthlyExpenseTotal.QuestionText;
            
            borrowerCombineMonthlyExpenseTotalValue.Answer = 
                Convert.ToString(
                    ConvertStringExpenseToIntValue(borrowerFirstMortgagePIValue.Answer) + 
                    ConvertStringExpenseToIntValue(borrowerOtherFinancingPIValue.Answer) +
                    ConvertStringExpenseToIntValue(borrowerHazardInsuranceValue.Answer) + 
                    ConvertStringExpenseToIntValue(borrowerRealEstateTaxesValue.Answer) +
                    ConvertStringExpenseToIntValue(borrowerMortgageInsuranceValue.Answer) + 
                    ConvertStringExpenseToIntValue(borrowerHomeOwnerAssocDuesValue.Answer) + 
                    ConvertStringExpenseToIntValue(borrowerCombineMonthlyExpenseOtherValue.Answer));

            newQuestionSequence.Add(borrowerCombineMonthlyExpenseTotalValue);

            return await Task.Run(() => subSectionSummary);
        }

        private int ConvertStringExpenseToIntValue(string answer)
        {
            if (answer == string.Empty) 
                return 0;

            if(Double.TryParse(answer, out double dVal))
            {
                 return (int)Math.Floor(dVal);
            }

            return 0;
        }

        private string GetQuetionIdForRentalScreen(string questionId, string appendString)
        {
            if(!string.IsNullOrWhiteSpace(appendString))
            {
                return questionId + appendString;
            }

            return questionId;
        }              
    }
}