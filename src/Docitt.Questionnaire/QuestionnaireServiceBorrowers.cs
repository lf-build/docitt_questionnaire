﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Docitt.Questionnaire.Abstractions;
using LendFoundry.Security.Tokens;

#if DOTNET2

#else
using Microsoft.AspNet.Http;
#endif

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService 
    {
        #region "Users"

        /// <summary>
        /// This function fetches the borrower information based on the configuration
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="subSection"></param>
        /// <returns></returns>
        public async Task<IBorrowerCoBorrowerInfo> GetBorrowerInfoFromTheMapping(string applicationNumber,int sectionId, List<ISubSection> subSections)
        {
            IBorrowerCoBorrowerInfo frm = new BorrowerCoBorrowerInfo();
            if (subSections != null)
            {
                if (subSections[0].Tag != null)
                {
                    if (subSections[0].Tag.Contains("Borrower"))
                    {
                        var applicantType = ApplicantType.Borrower;
                        frm = await GetBorrowerInfoFromTheMapping(applicationNumber,sectionId, subSections, applicantType);
                    }
                    else if (subSections[0].Tag.Contains("Spouse"))
                    {
                        var applicantType = ApplicantType.Spouse;
                        frm = await GetBorrowerInfoFromTheMapping(applicationNumber,sectionId, subSections, applicantType);
                    }
                    else if (subSections[0].Tag.Contains("CoBorrower"))
                    {
                        var applicantType = ApplicantType.CoBorrower;
                        frm = await GetBorrowerInfoFromTheMapping(applicationNumber,sectionId, subSections, applicantType);
                    }
                    else if (subSections[0].Tag.Contains("CoBorrowerSpouse"))
                    {
                        var applicantType = ApplicantType.CoBorrowerSpouse;
                        frm = await GetBorrowerInfoFromTheMapping(applicationNumber,sectionId, subSections, applicantType);
                    } 
                }
            }

            return frm;
        }

        /// <summary>
        /// This function fetches the borrower information based on the configuration
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="subSections"></param>
        /// <param name="applicantType"></param>
        /// <returns></returns>
        private async Task<IBorrowerCoBorrowerInfo> GetBorrowerInfoFromTheMapping(string applicationNumber,int sectionId, List<ISubSection> subSections, ApplicantType applicantType)
        {
            //This function fetches the questions answer form the sub section for a borrower and co-borrower.            

            IBorrowerCoBorrowerInfo frm = new BorrowerCoBorrowerInfo();

            var appFieldMap = new ApplicationFields(); 
            switch (applicantType)
            {
                case ApplicantType.Borrower:
                    appFieldMap = QuestionnaireConfigurations.BorrowerSubsectionFieldMap;
                    break;
                case ApplicantType.Spouse:
                    appFieldMap = QuestionnaireConfigurations.SpouseSubsectionFieldMap;
                    break;
                case ApplicantType.CoBorrower:
                    appFieldMap = QuestionnaireConfigurations.CoborrowerSubsectionFieldMap;
                    break;
                case ApplicantType.CoBorrowerSpouse:
                    appFieldMap = QuestionnaireConfigurations.CoborrowerSubsectionFieldMap;
                    break;
                default:
                    break;
            }
            foreach (var subSection in subSections)
            {
                foreach (IApplicationFieldMap item in appFieldMap.Map)
                {
                    var questionAnswer = subSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionId == item.QuestionId);

                    //TODO: refactor to get all the question from mapping
                    var questionAnswer1 = (await QuestionnaireAnswerRepository.GetByQuestionId(applicationNumber,
                        sectionId, subSection.SubSectionId, item.QuestionId)).FirstOrDefault();


                    if (questionAnswer != null || questionAnswer1!=null)
                    {
                        if (item.TargetProperty.ToLower() == "firstname")
                        {
                            frm.FirstName = string.IsNullOrWhiteSpace(questionAnswer1?.Answer) ? questionAnswer?.Answer: questionAnswer1?.Answer;
                            frm.SubSectionId = subSection.SubSectionId;
                        }
                        else if (item.TargetProperty.ToLower() == "lastname")
                        {
                            frm.LastName = string.IsNullOrWhiteSpace(questionAnswer1?.Answer) ? questionAnswer?.Answer: questionAnswer1?.Answer;
                            frm.SubSectionId = subSection.SubSectionId;
                        }
                        else if (item.TargetProperty.ToLower() == "email")
                        {
                            frm.Email = string.IsNullOrWhiteSpace(questionAnswer1?.Answer) ? questionAnswer?.Answer: questionAnswer1?.Answer;
                        }
                        else if (item.TargetProperty.ToLower() == "phone")
                        {
                            frm.Phone = string.IsNullOrWhiteSpace(questionAnswer1?.Answer) ? questionAnswer?.Answer: questionAnswer1?.Answer;                            
                        }

                        //if (applicantType.Equals(ApplicantType.CoBorrower)) { frm.SubSectionId = subSection.SubSectionId; }

                        frm.ApplicantType = applicantType;
                        frm.SectionId = sectionId;

                        if (string.IsNullOrEmpty(subSection.UserName) || subSection.UserName == "[UserBased]")
                        {
                            frm.UserName = frm.Email;
                        }
                        else
                        {
                            frm.UserName = subSection.UserName;
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(frm.Email))
            {
                return null;
            }

            return await Task.Run(() => frm);
        }

        /// <summary>
        /// This section will look for CoBorrower List from Joint Credit Section
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="ForApplicantScreen"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        public async Task<List<IBorrowerCoBorrowerInfo>> GetCoBorrowersJointCredit(string temporaryApplicationNumber, bool ForApplicantScreen = false, IQuestionnaire questionnaire = null)
        {
            if (questionnaire == null)
            {
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                if (questionnaire == null)
                {
                    throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");
                }
            }

            var borrowerAndCoBorrowerList = new List<IBorrowerCoBorrowerInfo>();
            var sections = new List<ISectionBasic>();
            ISectionListResponse sectionListResponse = new SectionListResponse();
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);

            var profileSection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId && s.UserName != SPOUSEUSERBASED);
            var jointCreditSubSection = profileSection.SelectMany(x=>x.SubSections).FirstOrDefault(x=>x.TemplateFieldName == "coBorrowerInvitationInfo");

            if(jointCreditSubSection == null)
                return borrowerAndCoBorrowerList;

            var subSectionsNew = GetUserInfoAndContactInfoSubsections(questionnaire.ApplicationForm.Sections.FirstOrDefault(i=>i.SectionId==1), jointCreditSubSection.SubSectionId);
            if(subSectionsNew != null && subSectionsNew.Count >0)
            {
                var questionSectionList = subSectionsNew[0].QuestionSections.Where(x=>x.QuestionSectionName=="AddCoBorrower");

                foreach(var qSection in questionSectionList)
                {
                    if(qSection.QuestionList != null && qSection.QuestionList.Count>0)
                    {
                        var question = qSection.QuestionList[0];
                        var questionId = question.QuestionId;
                        var answer = question.Answer;

                        var obj = JsonConvert.DeserializeObject<AddCoBorrowerModal>(answer);
                        if(obj != null)
                        {
                            var borrowerAndCoBorrowerItem = new BorrowerCoBorrowerInfo();
                            borrowerAndCoBorrowerItem.FirstName = obj.FirstName;
                            borrowerAndCoBorrowerItem.LastName = obj.LastName;
                            borrowerAndCoBorrowerItem.Phone = string.Empty;
                            borrowerAndCoBorrowerItem.Email = obj.EmailAddress;
                            
                            borrowerAndCoBorrowerItem.SectionId = profileSectionId;

                            if(!string.IsNullOrEmpty(questionId))
                            {
                                var sectionId = questionId.Substring(2,questionId.Length-2);
                                var infoSectionID = "ID-" + sectionId;
                                
                                if(obj.Relationship != "Spouse")
                                {
                                    infoSectionID = infoSectionID + "-1";
                                }
                                
                                borrowerAndCoBorrowerItem.SubSectionId = infoSectionID;
                            }
                            else
                            {
                                borrowerAndCoBorrowerItem.SubSectionId = string.Empty;
                            }
                            
                            borrowerAndCoBorrowerItem.UserName = obj.EmailAddress;

                            if(obj.Relationship == "Spouse")
                            {
                                borrowerAndCoBorrowerItem.ActualApplicantType = ApplicantType.Spouse;
                                borrowerAndCoBorrowerItem.ApplicantType = ApplicantType.CoBorrower;
                                borrowerAndCoBorrowerItem.Tag = new List<string>(){"Applicant","Spouse","CoBorrower"};
                            }else
                            {
                                borrowerAndCoBorrowerItem.ActualApplicantType = ApplicantType.CoBorrower;
                                borrowerAndCoBorrowerItem.ApplicantType = ApplicantType.CoBorrower;
                                borrowerAndCoBorrowerItem.Tag = new List<string>(){"Applicant","CoBorrower"};
                            }

                            borrowerAndCoBorrowerList.Add(borrowerAndCoBorrowerItem);
                        }
                    }
                }
            }
            return borrowerAndCoBorrowerList;
        }
       
        /// <summary>
        /// This function provides the list of applicants with their information.
        /// It provides first name, last name, email, phone, applicant type, email and user name
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="ForApplicantScreen"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        public async Task<List<IBorrowerCoBorrowerInfo>> GetBorrowersAndCoBorrowers(string temporaryApplicationNumber, bool ForApplicantScreen = false, IQuestionnaire questionnaire = null, bool isSpouseFromJointCredit = true, bool getCoborrowerSpouses = false)
        {
            // IQuestionnaire questionnaire = new Questionnaire();

            if (questionnaire == null) // if questionnaire is null then fetch it from DB.
            {
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                if (questionnaire == null)
                {
                    throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");
                }
            }

            //JointCredit async Task
            var jointCreditTask = GetCoBorrowersJointCredit(temporaryApplicationNumber,ForApplicantScreen, questionnaire);
            var spouseJointCreditTask = GetSpouseInfoFromJoinCredit(temporaryApplicationNumber, questionnaire);

            var borrowerAndCoBorrowerList = new List<IBorrowerCoBorrowerInfo>();

            var sections = new List<ISectionBasic>();

            ISectionListResponse sectionListResponse = new SectionListResponse();

            foreach (var item in questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == Convert.ToInt32(SectionDetails.Profile) && s.UserName != SPOUSEUSERBASED))
            {
                if(item.SubSections != null)
                {
                    foreach (var subsection in item.SubSections.OrderBy(x => x.SeqNo))
                    {
                        if (subsection.Tag == null || subsection.Tag.Count == 0) { continue; }

                        if (subsection.SubSectionId.Equals("SSS1")) { continue; }
                        if (subsection.TemplateId != null)
                        {
                            if (subsection.TemplateId.Equals("spouseCommunicationInfo")) { continue; }
                            if (subsection.TemplateId.Equals("coBorrowerCommunicationInfo")) { continue; }
                            if (subsection.TemplateId.Equals("coBorrowerSpouseCommunicationInfo")) { continue; }
                            //Fetch coborrower's spouse email if getCoborrowerSpouses is true
                            if (getCoborrowerSpouses && subsection.TemplateId.Equals("coBorrowerMaritalStatusInfo")) 
                            {
                                var coborrowerSpouseEmail = subsection.QuestionSections.Where(q => q.QuestionSectionName == "Email").FirstOrDefault();
                                if (coborrowerSpouseEmail != null)
                                {
                                    var emailQuestion = coborrowerSpouseEmail.QuestionList.Where(q => q.QuestionFieldName == "SpousePreferredEmail").FirstOrDefault();
                                    if (emailQuestion != null && !string.IsNullOrWhiteSpace(emailQuestion.Answer))
                                    {
                                        IBorrowerCoBorrowerInfo coBorrowerSpouseInfo = new BorrowerCoBorrowerInfo();
                                        coBorrowerSpouseInfo.Email = emailQuestion.Answer;
                                        coBorrowerSpouseInfo.ActualApplicantType = ApplicantType.CoBorrowerSpouse;
                                        borrowerAndCoBorrowerList.Add(coBorrowerSpouseInfo);
                                    }                                                                
                                }
                            }
                        }

                        if (subsection.Tag.Contains("Borrower"))
                        {
                            //If the subsection is of the borrower then fetch the borrower information
                            var subSections = GetUserInfoAndContactInfoSubsections(questionnaire.ApplicationForm.Sections.FirstOrDefault(i=>i.SectionId==item.SectionId), subsection.SubSectionId);
                            var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.Borrower);

                            if (borrowerInfo != null)
                            {
                                borrowerInfo.Tag = subsection.Tag;
                                borrowerInfo.ActualApplicantType = borrowerInfo.ApplicantType;
                                borrowerAndCoBorrowerList.Add(borrowerInfo);
                            }
                        }

                        if (subsection.Tag.Contains("CoBorrower") && !subsection.Tag.Contains("Spouse") && subsection.TemplateFieldName != "coBorrowerMaritalStatusInfo")
                        {
                            //If the subsection is of the coborrower who is not a Spouse co borrower then fetch the coborrower information
                            var subSections = GetUserInfoAndContactInfoSubsections(questionnaire.ApplicationForm.Sections.FirstOrDefault(i=>i.SectionId==item.SectionId), subsection.SubSectionId);
                            var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.CoBorrower);

                            if (borrowerInfo != null)
                            {
                                borrowerInfo.Tag = subsection.Tag;
                                borrowerInfo.ActualApplicantType = borrowerInfo.ApplicantType;
                                borrowerAndCoBorrowerList.Add(borrowerInfo);
                            }
                        }

                        if (subsection.Tag.Contains("Spouse") && !subsection.Tag.Contains("Borrower"))
                        {
                            //If the subsection is of the coborrower who is not a Spouse co borrower then fetch the coborrower information
                            var subSections = GetUserInfoAndContactInfoSubsections(questionnaire.ApplicationForm.Sections.FirstOrDefault(i=>i.SectionId==item.SectionId), subsection.SubSectionId);
                            var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.Spouse);

                            if (borrowerInfo != null)
                            {
                                borrowerInfo.Tag = subsection.Tag;

                                if (ForApplicantScreen)
                                {
                                    //For applicant screen, we want to show coborrower if it is 'spouse'. ForApplicantScreen value is passed from controller as true
                                    borrowerInfo.ApplicantType = ApplicantType.CoBorrower;
                                }
                                borrowerInfo.ApplicantType = ApplicantType.Spouse;
                                borrowerInfo.ActualApplicantType = ApplicantType.Spouse;
                                borrowerAndCoBorrowerList.Add(borrowerInfo);
                            }
                        }
                    }
                }
            }

            // await for JointCredit Section result
            // if new coborrowers are found add them to list
            var jointCreditList = await jointCreditTask;
            IBorrowerCoBorrowerInfo spouseJointCreditSectionData = null;

            // For some scenarios we dont want to get spouse info untill invite process is complete
            if(isSpouseFromJointCredit)
            {
                spouseJointCreditSectionData = await spouseJointCreditTask;
            }

            if(spouseJointCreditSectionData != null && !string.IsNullOrWhiteSpace(spouseJointCreditSectionData.Email))
            {
                if(!borrowerAndCoBorrowerList.Any(x => x.Email == spouseJointCreditSectionData.Email) && 
                    !borrowerAndCoBorrowerList.Any(x => x.SubSectionId == spouseJointCreditSectionData.SubSectionId))
                {
                    var spouseData = borrowerAndCoBorrowerList.FirstOrDefault(x=>x.ActualApplicantType == ApplicantType.Spouse);
                    if(spouseData != null){
                        spouseData.FirstName = spouseJointCreditSectionData.FirstName;
                        spouseData.LastName = spouseJointCreditSectionData.LastName;
                        spouseData.Email = spouseJointCreditSectionData.Email;
                    }
                    else
                    {
                         borrowerAndCoBorrowerList.Add(spouseJointCreditSectionData);
                    }
                }
                else
                {
                    var coBorrowerExist = borrowerAndCoBorrowerList.FirstOrDefault(x=>x.Email == spouseJointCreditSectionData.Email);
                    if(!string.IsNullOrEmpty(spouseJointCreditSectionData.FirstName)) coBorrowerExist.FirstName = spouseJointCreditSectionData.FirstName;
                    if(!string.IsNullOrEmpty(spouseJointCreditSectionData.LastName)) coBorrowerExist.LastName = spouseJointCreditSectionData.LastName;
                    if(!string.IsNullOrEmpty(spouseJointCreditSectionData.Email)) coBorrowerExist.Email = spouseJointCreditSectionData.Email;
                }
            }

            if(jointCreditList != null && jointCreditList.Count > 0)
            {
                if(borrowerAndCoBorrowerList.Count != jointCreditList.Count + 2) // 2 is for borrower & spouse user
                {
                    foreach(var coBo in jointCreditList)
                    {
                        if(!borrowerAndCoBorrowerList.Any(x => x.SubSectionId == coBo.SubSectionId))
                        {
                            borrowerAndCoBorrowerList.Add(coBo);
                        }else{
                            var coBorrowerExist = borrowerAndCoBorrowerList.FirstOrDefault(x=>x.SubSectionId == coBo.SubSectionId);
                            if(!string.IsNullOrEmpty(coBo.FirstName)) coBorrowerExist.FirstName = coBo.FirstName;
                            if(!string.IsNullOrEmpty(coBo.LastName)) coBorrowerExist.LastName = coBo.LastName;
                            if(!string.IsNullOrEmpty(coBo.Email)) coBorrowerExist.Email = coBo.Email;
                        }
                    }
                }
            }
            else
            {
                var coborrowerList = borrowerAndCoBorrowerList.Where(x=>x.ActualApplicantType == ApplicantType.CoBorrower).ToList();
                if(coborrowerList.Any()){
                    foreach (var coborrower in coborrowerList)
                    {
                        borrowerAndCoBorrowerList.Remove(coborrower);
                    }
                }
            }
            /// joint credit coborrower processing complete

            return borrowerAndCoBorrowerList; // await Task.Run(() => sectionListResponse);
        }

        private async Task<IBorrowerCoBorrowerInfo> GetSpouseInfoFromJoinCredit(string temporaryApplicationNumber, IQuestionnaire questionnaire = null)
        {
            if (questionnaire == null)
            {
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                if (questionnaire == null)
                {
                    throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");
                }
            }

            var borrowerAndCoBorrower = new BorrowerCoBorrowerInfo();
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);

            var profileSection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId && s.UserName != SPOUSEUSERBASED);
            var jointCreditSubSection = profileSection.SelectMany(x=>x.SubSections).FirstOrDefault(x=>x.SubSectionId == ConstantUtils.JointCreditSubSectionId);

            if(jointCreditSubSection == null)
                return borrowerAndCoBorrower;

            var jointCreditSectionQuestionList = jointCreditSubSection.QuestionSections.SelectMany(x=>x.QuestionList);
            var spouseAddAsCoborrower = jointCreditSectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == ConstantUtils.BorrowerAddSpouseAsCoBorrower);     
                                                                                                                
            if(spouseAddAsCoborrower != null && spouseAddAsCoborrower.Answer == "true")
            {
                var spouseFirstName =jointCreditSectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == ConstantUtils.SpouseFirstNameMarried);
                var spouseLastName =jointCreditSectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == ConstantUtils.SpouseLastNameMarried);
                var spouseEmail =jointCreditSectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == ConstantUtils.SpousePreferredEmail);

                borrowerAndCoBorrower.FirstName = spouseFirstName != null ? spouseFirstName.Answer : string.Empty;
                borrowerAndCoBorrower.LastName = spouseLastName != null ? spouseLastName.Answer : string.Empty;
                borrowerAndCoBorrower.Email = spouseEmail != null ? spouseEmail.Answer : string.Empty;

                borrowerAndCoBorrower.Phone = string.Empty;
                borrowerAndCoBorrower.SectionId = profileSectionId;
                borrowerAndCoBorrower.SubSectionId = ConstantUtils.JointCreditSubSectionId;
                borrowerAndCoBorrower.UserName =  borrowerAndCoBorrower.Email;

                borrowerAndCoBorrower.ActualApplicantType = ApplicantType.Spouse;
                borrowerAndCoBorrower.ApplicantType = ApplicantType.CoBorrower;
                borrowerAndCoBorrower.Tag = new List<string>(){"Applicant","Spouse","CoBorrower"};

                return borrowerAndCoBorrower;
            }

            return null;
        }

        /// <summary>
        /// This function provides the list of applicants with their information.
        /// It provides first name, last name, email, phone, applicant type, email and user name
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="forApplicantScreen"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        public async Task<List<IBorrowerCoBorrowerInfo>> GetBorrowersAndCoBorrowersAndSpouse(string temporaryApplicationNumber, bool forApplicantScreen = false)
        {
            // IQuestionnaire questionnaire = new Questionnaire();           

            var borrowerAndCoBorrowerList = new List<IBorrowerCoBorrowerInfo>();
            
            var item= await QuestionSectionRepository.GetApplicationSection(temporaryApplicationNumber,Convert.ToInt32(SectionDetails.Profile));
            if(item == null)
                    return null;

            if (item.UserName != SPOUSEUSERBASED)
            {
                var sectionsToQuery = new List<ISection>() {item};
                foreach (var subsection in item.SubSections)
                {
                    if (subsection.Tag == null || subsection.Tag.Count == 0) { continue; }

                    if (subsection.SubSectionId.Equals("SSS1")) { continue; }
                    if (subsection.TemplateId != null)
                    {
                        if (subsection.TemplateId.Equals("spouseCommunicationInfo")) { continue; }
                        if (subsection.TemplateId.Equals("coBorrowerCommunicationInfo")) { continue; }
                        if (subsection.TemplateId.Equals("coBorrowerMaritalStatusInfo")) { continue; }
                        if (subsection.TemplateId.Equals("coBorrowerSpouseCommunicationInfo")) { continue; }                        
                    }

                    if (subsection.Tag.Contains("Borrower"))
                    {
                        //If the subsection is of the borrower then fetch the borrower information
                        var subSections = GetUserInfoAndContactInfoSubsections(sectionsToQuery, item.SectionId, subsection.SubSectionId);
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.Borrower);

                        if (borrowerInfo != null)
                        {
                            borrowerInfo.Tag = subsection.Tag;
                            borrowerInfo.ActualApplicantType = borrowerInfo.ApplicantType;
                            borrowerAndCoBorrowerList.Add(borrowerInfo);
                        }
                    }

                    if (subsection.Tag.Contains("CoBorrower") && !subsection.Tag.Contains("Spouse") && subsection.TemplateFieldName != "coBorrowerMaritalStatusInfo")
                    {
                        //If the subsection is of the coborrower who is not a Spouse co borrower then fetch the coborrower information
                        var subSections = GetUserInfoAndContactInfoSubsections(sectionsToQuery, item.SectionId, subsection.SubSectionId);
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.CoBorrower);

                        if (borrowerInfo != null)
                        {
                            borrowerInfo.Tag = subsection.Tag;
                            borrowerInfo.ActualApplicantType = borrowerInfo.ApplicantType;
                            borrowerAndCoBorrowerList.Add(borrowerInfo);
                        }
                    }

                    if (subsection.Tag.Contains("Spouse") && !subsection.Tag.Contains("Borrower"))
                    {
                        //If the subsection is of the coborrower who is not a Spouse co borrower then fetch the coborrower information
                        var subSections = GetUserInfoAndContactInfoSubsections(sectionsToQuery, item.SectionId, subsection.SubSectionId);
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.Spouse);

                        if (borrowerInfo != null)
                        {
                            borrowerInfo.Tag = subsection.Tag;

                            if (forApplicantScreen)
                            {
                                //For applicant screen, we want to show coborrower if it is 'spouse'. ForApplicantScreen value is passed from controller as true
                                borrowerInfo.ApplicantType = ApplicantType.CoBorrower;
                            }
                            borrowerInfo.ApplicantType = ApplicantType.Spouse;
                            borrowerInfo.ActualApplicantType = ApplicantType.Spouse;
                            borrowerAndCoBorrowerList.Add(borrowerInfo);
                        }
                    }

                    if (subsection.Tag.Contains("CoBorrowerSpouse"))
                    {
                        //If the subsection is of the coborrower who is not a Spouse co borrower then fetch the coborrower information
                        var subSections = GetUserInfoAndContactInfoSubsections(sectionsToQuery, item.SectionId, subsection.SubSectionId);
                        var borrowerInfo = await GetBorrowerInfoFromTheMapping(temporaryApplicationNumber,item.SectionId, subSections, ApplicantType.CoBorrowerSpouse);

                        if (borrowerInfo != null)
                        {
                            borrowerInfo.Tag = subsection.Tag;
                            borrowerInfo.ActualApplicantType = borrowerInfo.ApplicantType;
                            borrowerAndCoBorrowerList.Add(borrowerInfo);
                        }
                    }

                }
            }

            return borrowerAndCoBorrowerList;
        }

        /// <summary>
        /// This function provides the list of applicants with their information.
        /// It provides first name, last name, email, phone, applicant type, email and user name
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="ForApplicantScreen"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        public async Task<List<IBorrowerCoBorrowerSpouseInfo>> GetBorrowersAndCoBorrowersAndSpouseName(string temporaryApplicationNumber, bool ForApplicantScreen = false, IQuestionnaire questionnaire = null)
        {
            var borrowerAndCoBorrowerSpouseList = new List<IBorrowerCoBorrowerSpouseInfo>();

            if (questionnaire == null) // if questionnaire is null then fetch it from DB.
            {
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                if (questionnaire == null)
                {
                    throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");
                }
            }

            var list = await GetBorrowersAndCoBorrowersAndSpouse(temporaryApplicationNumber, ForApplicantScreen);

            foreach(var co in list)
            {
                IBorrowerCoBorrowerSpouseInfo borrower = new BorrowerCoBorrowerSpouseInfo();
                borrower.FirstName = co.FirstName;
                borrower.LastName = co.LastName;
                borrower.ApplicantType = co.ApplicantType;
                borrower.Email = co.Email;
                borrower.Phone = co.Phone;
                borrower.SubSectionId = co.SubSectionId;
                borrower.SectionId = co.SectionId;
                borrower.UserName = co.UserName;
                borrower.ActualApplicantType = co.ActualApplicantType;
                borrower.Tag = co.Tag;
                
                var userState = questionnaire.UserState.FirstOrDefault(x => x.UserName == co.UserName);
                if(userState != null && !string.IsNullOrEmpty(userState.SpouseUserName))
                {
                    borrower.SpouseUserName = userState.SpouseUserName;
                }

                borrowerAndCoBorrowerSpouseList.Add(borrower);
            }
            
            return borrowerAndCoBorrowerSpouseList; // await Task.Run(() => sectionListResponse);
        }

        /// <summary>
        /// This function assigns primary borrower
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        public async Task<bool> AssignPrimaryBorrower(string temporaryApplicationNumber, int sectionId, string subSectionId)
        {

            ////////////////////////////

            var Sections = new List<ISection>();
            IQuestionnaire questionnaire = new Questionnaire();
            ISubSection initialBorrowerSubsection = new SubSection();
            ISubSection newBorrowerSubsection = new SubSection();

            //Logic
            //First find the Subsection having the Borrower
            //Update this Borrower to co-borrower
            //Then use the subsectionId passed to the function. This applicant is to be set as 'Borrower'

            //Get Questionnaire 
            questionnaire = await GetQuestionnaire(temporaryApplicationNumber);//GetByApplicationIdFilter(questionRequest.ApplicantId);

            initialBorrowerSubsection = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections).FirstOrDefault(ss => ss.Tag.Contains("Borrower"));
            initialBorrowerSubsection.Tag.Remove(ApplicantType.Borrower.ToString());
            initialBorrowerSubsection.Tag.Add(ApplicantType.CoBorrower.ToString());

            //Now assign the new applicant as Borrower
            newBorrowerSubsection = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections).FirstOrDefault(ss => ss.SubSectionId == subSectionId);

            if (newBorrowerSubsection.Tag.Contains(ApplicantType.CoBorrower.ToString()) == true)
            {
                newBorrowerSubsection.Tag.Remove(ApplicantType.CoBorrower.ToString());
            }

            newBorrowerSubsection.Tag.Add(ApplicantType.Borrower.ToString());

            await QuestionSectionRepository.UpdateSections(temporaryApplicationNumber, questionnaire.ApplicationForm.Sections);

            return true;
        }

        /// <summary>
        /// GetBasicInformation
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        public async Task<IUserBasicInfo> GetBasicInformation(string applicationNumber)
        {
            var questionnaire = await QuestionnaireRepository.GetQuestionnaire(applicationNumber);

            if (questionnaire == null)
            {
                throw new Exception($"questionnaire data not found for this application : {applicationNumber}");
            }
            IUserBasicInfo result = new UserBasicInfo();

            result.ApplicationNumber = applicationNumber;
            IAddressInfo currentAddress = new AddressInfo();
            var loanAmount = string.Empty;
            var questionnaireSummaryFieldMap = Extensions.Clone<List<QuestionnaireSummaryConfig>>(QuestionnaireConfigurations.ApplicationSummary.Map);
            if (questionnaireSummaryFieldMap != null)
            {
                var accountInfo = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == "Account Information");
                var communicationInfo = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == "Communication Information");

                if (accountInfo == null)
                {
                    throw new Exception($"Account Information doest not exist in questionnaireSummaryFieldMap configuration.");
                }
                var sectionId = accountInfo.SectionId;
                var subSectionId = accountInfo.SubSectionId;
                var communicationSubSectionId = communicationInfo.SubSectionId;
                var emailQuestionId = communicationInfo.QuestionInformation.ContainsKey("Email") ? communicationInfo.QuestionInformation["Email"].QuestionId : "";
                var phoneQuestionId = communicationInfo.QuestionInformation.ContainsKey("Phone") ? communicationInfo.QuestionInformation["Phone"].QuestionId : "";
                var cellPhoneQuestionId = communicationInfo.QuestionInformation.ContainsKey("CellPhone") ? communicationInfo.QuestionInformation["CellPhone"].QuestionId : "";
                var loanPurposeId = accountInfo.QuestionInformation.ContainsKey("LoanPurpose") ? accountInfo.QuestionInformation["LoanPurpose"].QuestionId : "";
                var firstNameQuestionId = accountInfo.QuestionInformation.ContainsKey("FirstName") ? accountInfo.QuestionInformation["FirstName"].QuestionId : "";
                var lastNameQuestionId = accountInfo.QuestionInformation.ContainsKey("LastName") ? accountInfo.QuestionInformation["LastName"].QuestionId : "";
                
                var questionList = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == sectionId).SelectMany(ss => ss.SubSections)
                    .Where(ss => ss.SubSectionId == subSectionId || ss.SubSectionId == communicationSubSectionId).SelectMany(qs => qs.QuestionSections)
                    .SelectMany(qs => qs.QuestionList);
                    
                if (questionList != null && questionList.Count() < 0)
                {
                    throw new Exception($"No questionnaire found");
                }

                var emailQuestion = questionList.FirstOrDefault(ql => ql.QuestionId == emailQuestionId);
                var firstNameQuestion = questionList.FirstOrDefault(ql => ql.QuestionId == firstNameQuestionId);
                var lastNameQuestion = questionList.FirstOrDefault(ql => ql.QuestionId == lastNameQuestionId);
                var phoneQuestion = questionList.FirstOrDefault(ql => ql.QuestionId == phoneQuestionId);
                var cellPhoneQuestion = questionList.FirstOrDefault(ql => ql.QuestionId == cellPhoneQuestionId);
                var loanPurposeQuestion = questionList.FirstOrDefault(ql => ql.QuestionId == loanPurposeId);

                result.FirstName = firstNameQuestion != null ? firstNameQuestion.Answer : string.Empty;
                result.Email = emailQuestion != null ? emailQuestion.Answer : string.Empty;
                result.LastName = lastNameQuestion != null ? lastNameQuestion.Answer : string.Empty;
                result.PhoneNumber = phoneQuestion != null ? phoneQuestion.Answer : string.Empty;
                result.LoanPurpose = loanPurposeQuestion != null ? loanPurposeQuestion.Answer : string.Empty;
                result.CellPhoneNumber = cellPhoneQuestion != null ? cellPhoneQuestion.Answer : string.Empty;
            }


            var appFieldMap = QuestionnaireConfigurations.ApplicationFieldMap;

            var propertyAddressdata = appFieldMap.Map.FirstOrDefault(x => x.TargetProperty.ToLower() == "propertyaddress");
            var loanAmountdata = appFieldMap.Map.FirstOrDefault(x => x.TargetProperty.ToLower() == "loanamount");

            var propertyAddressAnswer = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == propertyAddressdata.SectionId)
                        .SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == propertyAddressdata.QuestionId);

            var loanAmountAnswer = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == loanAmountdata.SectionId)
                        .SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == loanAmountdata.QuestionId);

            result.PropertyAddress = JsonConvert.DeserializeObject<AddressInfo>(propertyAddressAnswer.Answer);
            ILoanCalculationInfo loanInfo = JsonConvert.DeserializeObject<LoanCalculationInfo>(loanAmountAnswer.Answer);
            if (loanInfo != null)
            {
                result.LoanAmount = loanInfo.LoanAmount;
            }

            return result;
        }

        private async Task<bool> UpdateEmailVisiblity(IQuestionnaire questionnaire)
        {
            var result = false;
            var applicationNumber = questionnaire.TemporaryApplicationNumber;
            var userName = await GetCurrentUser();
            var userState = questionnaire.UserState.Where(x=>x.UserEmailId == userName).FirstOrDefault();

            if(userState.ApplicantType == ApplicantType.Spouse.ToString())
            {
                try
                {
                    IAnswerUpdateRequest updateRequest = new AnswerUpdateRequest();
                    updateRequest.TemporaryApplicationNumber = applicationNumber;
                    updateRequest.QuestionSectionName = "Email";
                    updateRequest.QuestionFieldName = ConstantUtils.SpousePreferredEmail;
                    updateRequest.SectionId = System.Convert.ToInt32(SectionDetails.Profile);
                    result = await QuestionSectionsRepository.UpdateQuestionReadOnly(updateRequest, true);
                }catch(Exception ex)
                {
                    Logger.Error($"Error while UpdateResendReminderVisiblity {applicationNumber} for {userName}", ex);
                }
            }

            return result;
        }

        private async Task<bool> UpdateResendReminderVisiblityAfterSignUp(IQuestionnaire questionnaire, string userName)
        {
            var result = false;
            var applicationNumber = questionnaire.TemporaryApplicationNumber;
            var userState = questionnaire.UserState.Where(x=>x.UserEmailId == userName).FirstOrDefault();

            IAnswerUpdateRequest updateRequest = new AnswerUpdateRequest();
            updateRequest.TemporaryApplicationNumber = applicationNumber;
            updateRequest.SectionId = System.Convert.ToInt32(SectionDetails.Profile);

            if(userState.ApplicantType == ApplicantType.Spouse.ToString())
            {
                updateRequest.QuestionSectionName = ConstantUtils.QuestionSectionNameSpouseResendInvite;
                updateRequest.QuestionFieldName = ConstantUtils.QuestionSectionNameSpouseResendInvite;
            }else if(userState.ApplicantType == ApplicantType.CoBorrowerSpouse.ToString())
            {
                updateRequest.QuestionSectionName = ConstantUtils.QuestionSectionNameCoBorrowerSpouseResendInvite;
                updateRequest.QuestionFieldName = ConstantUtils.QuestionSectionNameCoBorrowerSpouseResendInvite;
            }else if(userState.ApplicantType == ApplicantType.CoBorrower.ToString())
            {
                updateRequest.QuestionSectionName = ConstantUtils.QuestionSectionNameAddCoBorrower;
                updateRequest.QuestionFieldName = ConstantUtils.QuestionSectionNameAddCoBorrower;
                updateRequest.UserName = userState.InviteId;
            }

            try
            {   
                result = await QuestionSectionsRepository.UpdateResendInviteVisibility(updateRequest, QuestionVisiblity.AlwaysHide.ToString());
            }catch(Exception ex)
            {
                Logger.Error($"Error while UpdateResendReminderVisiblityAfterSignUp {applicationNumber} for {userName}", ex);
            }

            return result;
        }

        private async Task<bool> UpdateSignupEmailEditVisiblityAfterSignUp(IQuestionnaire questionnaire, string userName)
        {
            var result = false;
            var applicationNumber = questionnaire.TemporaryApplicationNumber;
            var userState = questionnaire.UserState.Where(x=>x.UserEmailId == userName).FirstOrDefault();

            IAnswerUpdateRequest updateRequest = new AnswerUpdateRequest();
            updateRequest.TemporaryApplicationNumber = applicationNumber;
            updateRequest.SectionId = System.Convert.ToInt32(SectionDetails.Profile);

            if(userState.ApplicantType == ApplicantType.Spouse.ToString())
            {
                updateRequest.QuestionSectionName = ConstantUtils.QuestionSectionNameSpouseResendInvite;
                updateRequest.QuestionFieldName = ConstantUtils.SpousePreferredEmail; 
            }
            else if(userState.ApplicantType == ApplicantType.CoBorrower.ToString())
            {
                updateRequest.QuestionSectionName = ConstantUtils.QuestionSectionNameAddCoBorrower;
                updateRequest.QuestionFieldName = ConstantUtils.SpousePreferredEmail;
                updateRequest.UserName = userState.InviteId;
            }

            try
            {   
                if(!string.IsNullOrEmpty(updateRequest.QuestionFieldName))
                    result = await QuestionSectionsRepository
                                          .UpdateQuestionVisibilityStatus(updateRequest, QuestionVisiblity.AlwaysHide.ToString());
            }
            catch(Exception ex)
            {
                // Allow to Coborrower to signup even if the email is not editable
                Logger.Error($"Error while UpdateSignupEmailEditVisiblityAfterSignUp {applicationNumber} for {userName}", ex);
            }

            return result;
        }

        public async Task<bool> UpdateUsernameField(string inviteId)
        {
            var status = true;
            var resultsData = await QuestionnaireRepository.GetMultipleTemporaryApplicationNumberByUserName(inviteId);
            
            if (resultsData == null)
            {
                status = false;
            }
            else
            {
                var userName = await GetCurrentUser();
                foreach (var temporaryApplicationNumber in resultsData )
                {
                    var questionnaire = await QuestionnaireRepository.GetQuestionnaire(temporaryApplicationNumber);

                    if (questionnaire == null)
                    {
                        status = false;
                    }
                    else
                    {
                        var updateResendReminderVisility = await UpdateResendReminderVisiblityAfterSignUp(questionnaire, userName);
                        var UpdateSignupEmailEditVisiblity = await UpdateSignupEmailEditVisiblityAfterSignUp(questionnaire, userName);
                        
                        Logger.Debug($"Start Update Username for subsections - {temporaryApplicationNumber} in inviteId - {inviteId} by actual username - {userName}");

                        await MapInviteIdToUsername(inviteId, temporaryApplicationNumber, userName);

                        foreach (var section in questionnaire.ApplicationForm.Sections)
                        {
                            var invitedUserSubSections = section.SubSections.Where(ss => ss.UserName == inviteId).ToList();
                            foreach (var invitedUserSubSection in invitedUserSubSections)
                            {
                                IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                                answerRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
                                answerRequest.SectionId = section.SectionId;
                                answerRequest.SubSectionId = invitedUserSubSection.SubSectionId;
                                answerRequest.UserName = userName;
                                invitedUserSubSection.UserName = userName;

                                await QuestionSubSectionsRepository.UpdateSubSectionWithSubSectionId(answerRequest, invitedUserSubSection);
                            }
                        }
                        Logger.Debug($"End Update Username for subsections - {temporaryApplicationNumber} in inviteId by actual username");

                        Logger.Debug($"Start Update Username for {temporaryApplicationNumber} in UserState - inviteId - {inviteId} by actual username - {userName}");

                        var userState = questionnaire.UserState.FirstOrDefault(u => u.UserName == inviteId);
                        if (userState != null)
                        {
                            userState.UserName = userName;
                        }
                        var coboSpouseuserState = questionnaire.UserState.FirstOrDefault(u => u.SpouseUserName == inviteId);
                        if (coboSpouseuserState != null)
                        {
                            coboSpouseuserState.SpouseUserName = userName;
                        }

                        await QuestionnaireRepository.UpdateUserStates(temporaryApplicationNumber, questionnaire.UserState);
                        status = true;
                        Logger.Debug($"End Update Username for UserState - inviteId by actual username");

                        if (questionnaire.ApplicationNumber != null)
                        {
                            Logger.Debug($"UpdateApplicationAndApplicantData method started for the application Number : {questionnaire.ApplicationNumber}");

                            await UpdateApplicationAndApplicantData(questionnaire);

                            Logger.Debug($"UpdateApplicationAndApplicantData method ended");
                        }
                        //await updateResendReminderVisility;
                    }
                };
               
            }
            return status;
        }
        
        private async Task<bool> MapInviteIdToUsername(string inviteId, string temporaryApplicationNumber, string username=null) 
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var reader = new StaticTokenReader(token.Value);
            
            if(string.IsNullOrEmpty(username))
                username = await GetCurrentUser();
            //TODO Why do we need to use DecisionEngineFactory. We can directly use the client.
            var decisionEngine = DecisionEngineFactory.Create(reader);

            var payload = new {
                inviteId,
                temporaryApplicationNumber,
                username
            };

            var tasks = new List<Task>();
            tasks.Add(Task.Run(() => decisionEngine.Execute<dynamic, Newtonsoft.Json.Linq.JObject>
                                                                    ("mapInviteIdToUsername", payload)));

            tasks.Add(Task.Run(() => decisionEngine.Execute<dynamic, Newtonsoft.Json.Linq.JObject>
                                                                    ("mapInviteIdToUsernameForAsset", payload)));

            tasks.Add(Task.Run(() => decisionEngine.Execute<dynamic, Newtonsoft.Json.Linq.JObject>
                                                                   ("updateInviteIdToUsernameForRequiredCondition", payload)));
            await Task.WhenAll(tasks);
            return true;
        }

        #endregion
    }
}
