﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Foundation.Services;
using Docitt.Questionnaire.Events;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Docitt.Questionnaire.Abstractions;
using Newtonsoft.Json;


namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        /// <summary>
        /// This function is used to get the JSON of 1 section at a time.
        /// Based on the section id, section json is fetched. It includes the collection of Subsections and templates.
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<ISection> GetSection(int sectionId)
        {
            var temporaryApplicationNumber = await GetTemporaryApplicationNumber();

            var section = await GetSection(temporaryApplicationNumber, sectionId);

            if (section != null)
            {
                return section;
            }
            else
            {
                throw new SectionNotFoundException($"Section having the Section Id {sectionId} for the Temporary Application Number '{temporaryApplicationNumber}' cannot be found.");
            }
        }

        private async Task CopiedRentalSectionDataForIncome(List<IUserAccessedStatus> users, string temporaryApplicationNumber)
        {
            Logger.Debug($"Copied Rental Section Started for {temporaryApplicationNumber}");
            var incomeSectionId = Convert.ToInt32(SectionDetails.Income);

            //// Copy Model Data
            var loggedInUser = await GetCurrentUser();

            var loggedBorrower = users.FirstOrDefault(x => x.UserName == loggedInUser);
            if (loggedBorrower == null)
            {
                throw new Exception("User not found");
            }

            
            //if(loggedInUser)
            //TODO: by Nayan: Move to configuration, instead of hardcoding
            var questionnaireSummaryFieldMap = Extensions.Clone<List<QuestionnaireSummaryConfig>>(QuestionnaireConfigurations.ApplicationSummary.Map);
            var reoSectionName = "Current Properties Own"; //// [From Config file]
            var rentalSectionName = "Rental Income";//// [From Config file]
            var ownPropertyQuestionFieldName =  "BorrowerREO";
            var rentalProperties = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == reoSectionName);
            var rentalPropertiesSectionData = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == rentalSectionName);

            var profileSectionTask = QuestionnaireRepository.GetSectionById(temporaryApplicationNumber,
                    Convert.ToInt32(SectionDetails.Profile));

            var incomeSectionTask = QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, incomeSectionId);

            await Task.WhenAll(profileSectionTask, incomeSectionTask);

            var profileSection = profileSectionTask.Result;
            var incomeSection = incomeSectionTask.Result;

            if (profileSection != null)
            {
                //TODO: By Nayan: Why we have different name for additional info for spouse REO? 
                var borrowerReoInfo = profileSection.SubSections.Where(x =>
                    x.UserName == loggedBorrower.UserName || x.UserName == loggedBorrower.SpouseUserName).Where(x =>
                    (
                        string.Equals(x.TemplateFieldName, TemplateConstant.BorrowerREOInfo, StringComparison.OrdinalIgnoreCase)
                     || string.Equals(x.TemplateFieldName, TemplateConstant.CoBorrowerREOInfo, StringComparison.OrdinalIgnoreCase)
                     || string.Equals(x.TemplateFieldName, TemplateConstant.SpouseBorrowerREOAdditionalInfo, StringComparison.OrdinalIgnoreCase)
                     || string.Equals(x.TemplateFieldName, TemplateConstant.SpouseBorrowerREOInfo, StringComparison.OrdinalIgnoreCase)
                     || string.Equals(x.TemplateFieldName, TemplateConstant.CoborrowerSpouseBorrowerREOInfo, StringComparison.OrdinalIgnoreCase)
                     || string.Equals(x.TemplateFieldName, TemplateConstant.CoborrowerSpouseBorrowerREOAdditionalInfo, StringComparison.OrdinalIgnoreCase)
                    )).ToList();

                Logger.Debug(
                    $"Copied Rental Section for {temporaryApplicationNumber} found {borrowerReoInfo.Count} sections");

                var reoPropertyList = borrowerReoInfo.SelectMany(x => x.QuestionSections)
                    .SelectMany(x => x.QuestionList)
                    .Where(x => x.QuestionFieldName == ownPropertyQuestionFieldName).ToList();

                string reoPropertyExist = "false";

                if (reoPropertyList.Any(x => x.Answer == "true"))                
                    reoPropertyExist = "true";

                var rentalDetailList =
                    new List<(string Username, string SubSectionId, Dictionary<string, string> Properties)>();
                if (rentalProperties != null && reoPropertyExist == "true")
                {
                    foreach (var subSection in borrowerReoInfo)
                    {
                        var rentalDetail = new Dictionary<string, string>();
                        rentalDetail["PropertyAddress"] = subSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["PropertyAddress"].QuestionId)
                            ?.Answer;

                        rentalDetail["PropertyType"] = subSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["PropertyType"].QuestionId)
                            ?.Answer;

                        rentalDetail["PropertyTypeUnits"] = subSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["PropertyTypeUnits"].QuestionId)
                            ?.Answer; //need to check
                        rentalDetail["PropertyStatus"] = subSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["PropertyStatus"].QuestionId)
                            ?.Answer;
                        rentalDetail["PropertyUse"] = subSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["PropertyUse"].QuestionId)
                            ?.Answer;
                        rentalDetail["PropertyValue"] = subSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["PropertyValue"].QuestionId)
                            ?.Answer;
                        rentalDetail["MonthlyRentalIncome"] = subSection.QuestionSections
                            .SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties.QuestionInformation["MonthlyRentalIncome"].QuestionId)
                            ?.Answer;
                        rentalDetail["MonthlyInsuranceTaxesAssociationDues"] = subSection.QuestionSections
                            .SelectMany(x => x.QuestionList)
                            .FirstOrDefault(x =>
                                x.QuestionId == rentalProperties
                                    .QuestionInformation["MonthlyInsuranceTaxesAssociationDues"].QuestionId)
                            ?.Answer;

                        // Copy property to income section only if rental income is available

                        if (int.TryParse(rentalDetail["MonthlyRentalIncome"], out var rentalIncomeValue) &&
                            rentalIncomeValue > 0)
                            rentalDetailList.Add((subSection.UserName, subSection.SubSectionId, rentalDetail));
                    }
                }

                if (!rentalDetailList.Any())
                {
                    Logger.Debug(
                        "CopiedRentalSectionDataForIncome - profileData != null && rentalProperties != null && reoPropertyExist == true",
                        new
                        {
                            temporaryApplicationNumber, sectionId = incomeSectionId, rentalProperties, reoPropertyExist
                        });
                }

                Logger.Debug(
                    $"Copied Rental Section for {temporaryApplicationNumber} found {rentalDetailList.Count} sections with monthly rental income");

                //// Template add to sub section

                var rentalTemplateName = "rentalIncome";
                var rentalWithDeleteTemplateName = "_rentalIncome";
                var borrowerIncomeQuestionType = "BorrowerIncomeType";
                
                foreach (var item in rentalDetailList.GroupBy(r => r.Username))
                {
                    Logger.Debug($"Copied Rental Section for {temporaryApplicationNumber} processing for {item.Key} with total {item.Count()}");
                    var answerUpdateList = new List<IQuestionnaireQuestionAnswer>();
                    var subSectionList = new List<SubSection>();

                    var count = 0;
                    var seqNo = 0;

                    //Get the sequence number for the current user's income logo sub section
                    var incomeLogoSubSection =
                        incomeSection.SubSections.FirstOrDefault(x => x.UserName == item.Key);
                    IQuestion questionInfo = null;
                    if (incomeLogoSubSection != null)
                    {
                        seqNo = incomeLogoSubSection.SeqNo.GetValueOrDefault() + 5500 + 1;
                        
                        questionInfo = incomeLogoSubSection.QuestionSections.SelectMany(x => x.QuestionList)
                            .FirstOrDefault(y => y.QuestionFieldName == borrowerIncomeQuestionType);

                        if (questionInfo == null)
                        {
                            throw new Exception($"IncomeLogo Question not found for {temporaryApplicationNumber}");
                        }
                        if (!string.IsNullOrWhiteSpace(questionInfo.Answer))
                        {
                            Logger.Debug($"Copied Rental Section for {temporaryApplicationNumber} processing terminated {item.Key} as section is already copied");
                            return;
                        }
                    }
                    else
                    {
                        throw new Exception($"IncomeLogo Section not found for {temporaryApplicationNumber}");
                    }    
                    

                    foreach (var tuple in item)
                    {
                        KeyValuePair<string, SubSection> rentalIncomeTemplateData;
                        SubSection newTemplate;

                        if (count == 0)
                        {
                            rentalIncomeTemplateData =
                                incomeSection.Templates.FirstOrDefault(x => x.Key == rentalTemplateName);
                            newTemplate = Extensions.Clone<SubSection>(rentalIncomeTemplateData.Value);
                        }
                        else
                        {
                            rentalIncomeTemplateData =
                                incomeSection.Templates.FirstOrDefault(x => x.Key == rentalWithDeleteTemplateName);
                            newTemplate = Extensions.Clone<SubSection>(rentalIncomeTemplateData.Value);
                        }
                        var user = users.First(u => string.Equals(u.UserName, tuple.Username, StringComparison.OrdinalIgnoreCase));
                        if (!string.Equals(user.ApplicantType, ApplicantType.Borrower.ToString(), StringComparison.OrdinalIgnoreCase))
                        {
                            newTemplate.TemplateId = null;
                        }
                        newTemplate.SeqNo = seqNo + count;
                        newTemplate.SubSectionId = $"ID-{Guid.NewGuid()}";
                        newTemplate.UserName = tuple.Username;
                        newTemplate.DeleteTemplates = new List<string>()
                        {
                            newTemplate.SubSectionId
                        };

                        if (rentalPropertiesSectionData != null)
                        {
                            foreach (var dicData in rentalPropertiesSectionData.QuestionInformation)
                            {
                                var answerUpdateRequestObj = new QuestionnaireQuestionAnswer();
                                answerUpdateRequestObj.SubSectionId = newTemplate.SubSectionId;
                                answerUpdateRequestObj.SectionId = Convert.ToInt32(SectionDetails.Income);
                                answerUpdateRequestObj.QuestionId = dicData.Value.QuestionId;
                                var seqNumber = newTemplate.QuestionSections
                                    .FirstOrDefault(x =>
                                        x.QuestionList.Any(y => y.QuestionId == dicData.Value.QuestionId))
                                    ?.SeqNo;
                                answerUpdateRequestObj.QuestionSectionSeqNo = seqNumber.GetValueOrDefault();

                                if (tuple.Properties.TryGetValue(dicData.Key, out var answer))
                                {
                                    answerUpdateRequestObj.Answer = answer;
                                }

                                answerUpdateRequestObj.TemporaryApplicationNumber = temporaryApplicationNumber;
                                answerUpdateList.Add(answerUpdateRequestObj);
                            }
                        }

                        subSectionList.Add(newTemplate);
                        //section.SubSections.Add(_newTemplate);
                        count++;
                    }

                    Logger.Debug($"Copied Rental Section for {temporaryApplicationNumber} processing {subSectionList.Count} are created");
                    if (subSectionList.Any())
                    {
                        var addSubSectionModel = new AddSubSectionsRequest();
                        addSubSectionModel.TemporaryApplicationNumber = temporaryApplicationNumber;
                        addSubSectionModel.SubSections = subSectionList;
                        addSubSectionModel.SectionId = Convert.ToInt32(SectionDetails.Income);

                        //// Append Sub section for Rental
                        await AppendSubSections(addSubSectionModel, temporaryApplicationNumber,
                            incomeSectionId, true);

                        //// Answer update to database
                        await QuestionnaireAnswerRepository.InsertAnswers(answerUpdateList);

                        //// For Rental section by default selected - One Time.

                        var sequenceNumberForLogo = (incomeLogoSubSection.QuestionSections
                            .FirstOrDefault(x => x.QuestionList.Any(y => y.QuestionId == questionInfo.QuestionId))
                            ?.SeqNo).GetValueOrDefault();

                        await UpdateIncomeLogoSelectedForRental(temporaryApplicationNumber,
                            incomeLogoSubSection.SubSectionId, questionInfo.QuestionId, sequenceNumberForLogo,
                            questionInfo.Answer);

                        Logger.Debug($"Copied Rental Section for {temporaryApplicationNumber} updated logo answer for {item.Key}");
                    }
                    else
                    {
                        Logger.Debug("CopiedRentalSectionDataForIncome - No Subsections to add", new
                        {
                            temporaryApplicationNumber, sectionId = incomeSectionId, rentalProperties,
                            reoPropertyExist
                        });
                    }
                }

            }
            else
            {
                Logger.Error($"Copied Rental Section failed for {temporaryApplicationNumber} as profile section not found");
            }
        }


        private async Task UpdateIncomeLogoSelectedForRental(string temporaryApplicationNumber, string subSectionId,
            string qId, int seqNo, string existingAnswer)
        {
            string answer;
            if (!string.IsNullOrWhiteSpace(existingAnswer))
            {
                var answerArray = JsonConvert.DeserializeObject<List<string>>(existingAnswer);
                if (!answerArray.Contains("rental"))
                    answerArray.Add("rental");
                answer = JsonConvert.SerializeObject(answerArray);
            }
            else
            {
                answer = "[\"rental\"]";
            }

            //// For Rental section by default selected - One Time.
            var answerUpdateRequestForRentalSection = new QuestionnaireQuestionAnswer();
            answerUpdateRequestForRentalSection.SubSectionId = subSectionId;
            answerUpdateRequestForRentalSection.SectionId = Convert.ToInt32(SectionDetails.Income);
            answerUpdateRequestForRentalSection.QuestionId = qId;
            answerUpdateRequestForRentalSection.QuestionSectionSeqNo = seqNo;
            answerUpdateRequestForRentalSection.TemporaryApplicationNumber = temporaryApplicationNumber;
            answerUpdateRequestForRentalSection.Answer = answer;
            await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(answerUpdateRequestForRentalSection);
        }

        /// <summary>
        /// This function is used to get the JSON of 1 section at a time.
        /// Based on the section id, section json is fetched. It includes the collection of Subsections and templates.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<ISection> GetSection(string temporaryApplicationNumber, int sectionId)
        {
            var loggedInUser = await GetCurrentUser();
            var loggedInUserSpouse = await GetCurrentUser();
            
            //Validate input params
            await ValidateGetSectionRequest(temporaryApplicationNumber, sectionId);

            //Get Questionnaire
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
            
            if (questionnaire == null)
            {
                Logger.Error($"Questionnaire not found for {temporaryApplicationNumber}");
                throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");
            }
            //TODO Get ApplicantList from QuestionnaireUserState
            var borrowerList = await GetBorrowersAndCoBorrowersAndSpouseName(temporaryApplicationNumber,false);
            
            IBorrowerCoBorrowerSpouseInfo loggedInUserCoBorrower = null;

            var loggedBorrower = borrowerList.FirstOrDefault(x => x.UserName == loggedInUser);

            var isBorrowerOrSpouse = borrowerList
                                        .Any(x => x.UserName == loggedInUser && 
                                                 (x.ApplicantType == ApplicantType.Borrower || x.ApplicantType == ApplicantType.Spouse));
            
            if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrower || loggedBorrower.ActualApplicantType == ApplicantType.CoBorrowerSpouse)
            {
                var username = await GetCoupleUserName(questionnaire.UserState, loggedBorrower.UserName);

                if(!string.IsNullOrEmpty(username))
                {
                    loggedInUserSpouse = username;
                
                    loggedInUserCoBorrower = borrowerList.FirstOrDefault(x => x.UserName == username);
                }
            }

            if(loggedBorrower.ActualApplicantType == ApplicantType.Borrower || loggedBorrower.ActualApplicantType == ApplicantType.Spouse)
            {
                var username = await GetCoupleUserName(questionnaire.UserState, loggedBorrower.UserName);

                if(!string.IsNullOrEmpty(username))
                {
                    loggedInUserSpouse = username;
                
                    loggedInUserCoBorrower = borrowerList.FirstOrDefault(x => x.UserName == username);
                }
            }

            var section = questionnaire.ApplicationForm.Sections
                                            .FirstOrDefault(Section => Section.SectionId == sectionId);
            
            if (section == null)
                throw new SectionNotFoundException($"Section having the Section Id {sectionId} for the Temporary Application Number '{temporaryApplicationNumber}' cannot be found.");
            
            // Decrypt Sensitive Answers
            var sectionResponse = await ProcessSensitiveAnswerAndFillPlaceholders(questionnaire, section);

            sectionResponse.PlaceHolders["<#applicantType#>"] = loggedBorrower.ActualApplicantType.ToString();
            
            if(loggedBorrower.ActualApplicantType != ApplicantType.Borrower && loggedBorrower.ActualApplicantType != ApplicantType.Spouse)
            {
                var coborrowerSpouseData = loggedBorrower;
                var coborrowerData = loggedBorrower;

                if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrowerSpouse)
                {
                    coborrowerSpouseData = loggedBorrower;
                    coborrowerData = loggedInUserCoBorrower!= null ? loggedInUserCoBorrower : null;
                }

                if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrower)
                {
                    coborrowerSpouseData = loggedInUserCoBorrower!= null ? loggedInUserCoBorrower : null;
                    coborrowerData = loggedBorrower;
                }

                sectionResponse.PlaceHolders["<#fName#>"] = coborrowerData.FirstName;

                if(loggedInUserCoBorrower!= null && !string.IsNullOrEmpty(loggedInUserCoBorrower.FirstName))
                {
                    sectionResponse.PlaceHolders["<#spouseFirstName#>"] = coborrowerSpouseData.FirstName;
                    sectionResponse.PlaceHolders["<#spouseLastName#>"] = coborrowerSpouseData.LastName;
                    sectionResponse.PlaceHolders["<#spouseEmail#>"] = coborrowerSpouseData.Email;
                }
                else
                {
                    sectionResponse.PlaceHolders["<#spouseFirstName#>"] = string.Empty;
                    sectionResponse.PlaceHolders["<#spouseLastName#>"] = string.Empty;
                    sectionResponse.PlaceHolders["<#spouseEmail#>"] = string.Empty;
                    sectionResponse.PlaceHolders["<#spouseDetails#>"] = string.Empty;
                }
            }

            if(loggedBorrower.ActualApplicantType == ApplicantType.Borrower || loggedBorrower.ActualApplicantType == ApplicantType.Spouse)
            {
                var borrowerSpouseData = loggedBorrower;
                var borrowerData = loggedBorrower;

                if(loggedBorrower.ActualApplicantType == ApplicantType.Spouse)
                {
                    borrowerSpouseData = loggedBorrower;
                    borrowerData = loggedInUserCoBorrower!= null ? loggedInUserCoBorrower : null;
                }

                if(loggedBorrower.ActualApplicantType == ApplicantType.Borrower)
                {
                    borrowerSpouseData = loggedInUserCoBorrower!= null ? loggedInUserCoBorrower : null;
                    borrowerData = loggedBorrower;
                }

                sectionResponse.PlaceHolders["<#fName#>"] = borrowerData.FirstName;

                if(loggedInUserCoBorrower!= null && !string.IsNullOrEmpty(loggedInUserCoBorrower.FirstName))
                {
                    sectionResponse.PlaceHolders["<#spouseFirstName#>"] = borrowerSpouseData.FirstName;
                    sectionResponse.PlaceHolders["<#spouseLastName#>"] = borrowerSpouseData.LastName;
                    sectionResponse.PlaceHolders["<#spouseEmail#>"] = borrowerSpouseData.Email;
                }
                else
                {
                    
                    sectionResponse.PlaceHolders["<#spouseFirstName#>"] = await GetSpouseDataForIndividualApplication(section,"SpouseFirstName");
                    sectionResponse.PlaceHolders["<#spouseLastName#>"] = await GetSpouseDataForIndividualApplication(section,"SpouseLastName");
                    sectionResponse.PlaceHolders["<#spouseEmail#>"] = string.Empty;
                    sectionResponse.PlaceHolders["<#spouseDetails#>"] = string.Empty;
                }
            }

            if (section.SectionId == Convert.ToInt32(SectionDetails.Profile))
            {
                // Process ProfileSection
                await ProcessProfileSection(section, loggedBorrower, loggedInUserCoBorrower);

                await ProcessPlaceHolders(section);
            }

            if (sectionId == Convert.ToInt32(SectionDetails.Income)) // Income section
            {
                // List<IBorrowerCoBorrowerInfo> applicantSpouseList = null;
                // string applicantSpouse = await GetCoupleUserName(questionnaire, loggedBorrower.UserName);
                // if(loggedBorrower.ActualApplicantType == ApplicantType.Borrower || loggedBorrower.ActualApplicantType == ApplicantType.CoBorrower)
                //     applicantSpouseList = borrowerList.Where(b => b.UserName == applicantSpouse).ToList();
                // else
                //     applicantSpouseList = borrowerList.Where(b => b.UserName == loggedInUser).ToList();
               

                ProcessIncomeSection(temporaryApplicationNumber,section, borrowerList, sectionId);
                //GenerateDynamicRentalIncomeTemplate(questionnaire, section);

                // applicantSpouseList = borrowerList.Where(b => b.UserName == loggedInUser || b.UserName == applicantSpouse).ToList();
                // Autofill ssn and dob from profile section for borrower
                await PrefillDOBandSSN(questionnaire, section, borrowerList);

            }

            await ReplaceText(section, loggedBorrower, loggedInUserCoBorrower, borrowerList);
            if (section.SectionId != Convert.ToInt32(SectionDetails.Profile))
            {
             // Replace #FName PlaceHolders
                await ReplaceFNamePlaceholder(section, borrowerList);
            }
            if (borrowerList.Any() && !string.IsNullOrWhiteSpace(loggedInUser))
            {
                if (!isBorrowerOrSpouse)
                {
                     section.SubSections = section.SubSections
                                                     .Where(x => x.UserName == loggedInUser || 
                                                                 x.UserName == loggedInUserSpouse || 
                                                                 x.isDefault)
                                                     .ToList();
                }
                else
                {
                    var borrowerAndSpouse = questionnaire.UserState
                        .Where(x => x.ApplicantType == ApplicantType.Borrower.ToString() || x.ApplicantType == ApplicantType.Spouse.ToString())
                        .Select(x=>x.UserName).ToList();

                    section.SubSections = section.SubSections.Where(x => borrowerAndSpouse.Any(y => y == x.UserName)
                                                                            || x.isDefault 
                                                                            || x.TemplateId == "spouseInfo"
                                                                            || x.TemplateId == "spouseCommunicationInfo"
                                                                             ).ToList();
                }
            }

            var nonSpouseCoborrower = borrowerList.Where(x => x.ApplicantType == ApplicantType.CoBorrower).ToList();

            if (section.SectionId == Convert.ToInt32(SectionDetails.Profile) && !isBorrowerOrSpouse)
            {
                var questionList = section.SubSections.Where(x => x.UserName != loggedInUser && x.UserName != loggedInUserSpouse)
                                    .SelectMany(x => x.QuestionSections)
                                    .SelectMany(x => x.QuestionList);

                foreach (var question in questionList)
                {
                    question.IsReadOnly = true;
                    question.Required = Extensions.GetDescription(RequiredType.False);
                }

                nonSpouseCoborrower = nonSpouseCoborrower.Where(x => x.UserName != loggedInUser && x.UserName != loggedInUserSpouse).ToList();
                
                section.SubSections = section.SubSections
                                            .Where(s => !nonSpouseCoborrower.Any(y => y.UserName == s.UserName && "coborrowerResidenceDetail".Equals(s.TemplateId, StringComparison.InvariantCultureIgnoreCase)))
                                            .ToList();
            }
            else
            {
                section.SubSections = section.SubSections
                                             .Where(s => !nonSpouseCoborrower.Any(y => y.UserName == s.UserName && "coborrowerResidenceDetail".Equals(s.TemplateId, StringComparison.InvariantCultureIgnoreCase)))
                                             .ToList();
            }

            section = await MakeRequiredQuestionSkipable(section);

            if (sectionId == Convert.ToInt32(SectionDetails.Summary)) //this should come from config
            {
                var requiredQuestions = await GetValidationsOnSummary(temporaryApplicationNumber, borrowerList, questionnaire);
                var totalRequiredQuestions = 0;
                List<string> userList; 
                if (!isBorrowerOrSpouse)
                {
                    userList = new List<string>(){ loggedInUser, loggedInUserSpouse };
                    totalRequiredQuestions = requiredQuestions.Where(ss => userList.Contains(ss.Key)).ToList().Count;
                }
                else
                {
                    userList = await GetCoupleUserNames(borrowerList);
                    totalRequiredQuestions = requiredQuestions.Where(ss => userList.Contains(ss.Key)).ToList().Count;
                }
                
                if (totalRequiredQuestions > 0)
                {
                    //Disable the checkbox for all the applicant. If it is Borrower then do it for the spouse as well, vice versa.
                    var questionCheckbox = section.SubSections
                                            .Where(us => userList.Contains(us.UserName))
                                            .SelectMany(qs => qs.QuestionSections)
                                            .SelectMany(ql => ql.QuestionList)
                                            .Where(q => q.QuestionId == "5"); //this should come from config.

                    foreach (var question in questionCheckbox)
                    {
                        if (question != null)
                        {
                            question.Answer = string.Empty;
                            question.IsReadOnly = true;
                        }
                    }
                }
            }

            //Next if manual work flow is set to true then update the section
            if(QuestionnaireConfigurations.IsManualAssetWorkFlow)
            {
                await ProcessForManualAssetWorkflow(section);
            }

            return await Task.Run(() => section);
        }

        private async Task<string> GetSpouseDataForIndividualApplication(ISection section,string fieldName)
        {
            string data = string.Empty;
            var questions = section.SubSections
                                            .SelectMany(ss => ss.QuestionSections)
                                            .Where(x => x.QuestionList != null)
                                            .SelectMany(qs => qs.QuestionList)
                                            .Where(x=>x.QuestionFieldName == fieldName);

            foreach(var question in questions)
            {
                if(!string.IsNullOrEmpty(question.Answer))
                {
                        data = question.Answer;
                        return data;
                }
            }

            return await Task.Run(() => data);
        }

        private async Task<bool> ReplaceText(ISection section, IBorrowerCoBorrowerSpouseInfo loggedInUser, IBorrowerCoBorrowerSpouseInfo loggedInUserSpouse, List<IBorrowerCoBorrowerSpouseInfo> borrowerList)
        {
            var isLoggedUserBorrowerOrCoBorrower = loggedInUser.ActualApplicantType == ApplicantType.Borrower || loggedInUser.ActualApplicantType == ApplicantType.CoBorrower;
            string loggerInUserSpouseFirstName = string.Empty; string loggerInUserFirstName = string.Empty;

            if(loggedInUser != null &&  !string.IsNullOrEmpty(loggedInUser.FirstName))
                loggerInUserFirstName = loggedInUser.FirstName;

            if(loggedInUserSpouse != null && !string.IsNullOrEmpty(loggedInUserSpouse.FirstName))
                 loggerInUserSpouseFirstName = loggedInUserSpouse.FirstName;

            List<string> coBorrowerSpouseSectionList = QuestionnaireConfigurations.CoBorrowerSpouseSectionList; //new List<string>(){"coBorrowerSpouseAdditionalResidence","coBorrowerSpouseResidenceDetail"};

            // Loop through each user's section
            foreach (var applicantInList in borrowerList)
            {
                // process only for logged-in user and logged-in user spouse
                if((loggedInUser != null && loggedInUser.UserName != applicantInList.UserName) && 
                   (loggedInUserSpouse != null && loggedInUserSpouse.UserName != applicantInList.UserName))
                    continue;

                // isApplicationBorrowerOrCoBorrower will be false for spouse and coborrowerspouse
                var isApplicationBorrowerOrCoBorrower = applicantInList.ActualApplicantType == ApplicantType.Borrower || applicantInList.ActualApplicantType == ApplicantType.CoBorrower;

                //Get sections of each user
                var subSections = section.SubSections
                                            .Where(s => s.UserName == applicantInList.UserName)
                                            .ToList();

                if(isApplicationBorrowerOrCoBorrower)
                {
                    foreach(var subSection in subSections)
                    {
                        // CoBorrowerSpouse Template are updated with coborrower username
                        var isCoBorrowerSpouseTemplate = coBorrowerSpouseSectionList.Any(x => x == subSection.TemplateFieldName);  
                        
                        if(!isCoBorrowerSpouseTemplate)
                        {   
                            if(isLoggedUserBorrowerOrCoBorrower)
                            {
                                if(!string.IsNullOrEmpty(subSection.SubSectionTextBorrower))
                                    subSection.SubSectionName = subSection.SubSectionTextBorrower;
                            }
                            else
                            {
                                if(!string.IsNullOrEmpty(subSection.SubSectionTextSpouse))
                                    subSection.SubSectionName = subSection.SubSectionTextSpouse;
                            }
                        }else{
                            if(isLoggedUserBorrowerOrCoBorrower)
                            {
                                if(!string.IsNullOrEmpty(subSection.SubSectionTextSpouse))
                                    subSection.SubSectionName = subSection.SubSectionTextSpouse;
                            }
                            else
                            {
                                if(!string.IsNullOrEmpty(subSection.SubSectionTextBorrower))
                                    subSection.SubSectionName = subSection.SubSectionTextBorrower;
                            }
                        }
                        
                        
                    if (section.SectionId != Convert.ToInt32(SectionDetails.Profile))
                        {
                        subSection.SubSectionName = subSection.SubSectionName.Replace("<#spouseFirstName#>",loggerInUserSpouseFirstName);
                        subSection.SubSectionName = subSection.SubSectionName.Replace("<#loggedFirstName#>",loggerInUserFirstName);
                         }

                        var tempQuestions = subSection.QuestionSections
                                                      .Where(x => x.QuestionList !=     null)
                                                      .SelectMany(qs => qs.QuestionList);
                        foreach(var question in tempQuestions)
                        {
                            if(!isCoBorrowerSpouseTemplate)
                            {
                                if(isLoggedUserBorrowerOrCoBorrower)
                                {
                                    if(!string.IsNullOrEmpty(question.QuestionTextBorrower))
                                        question.QuestionText = question.QuestionTextBorrower;
                                }
                                else
                                {
                                    if(!string.IsNullOrEmpty(question.QuestionTextSpouse))
                                        question.QuestionText = question.QuestionTextSpouse;
                                }
                            }else{
                                if(isLoggedUserBorrowerOrCoBorrower)
                                {
                                    if(!string.IsNullOrEmpty(question.QuestionTextSpouse))
                                        question.QuestionText = question.QuestionTextSpouse;
                                }
                                else
                                {
                                    if(!string.IsNullOrEmpty(question.QuestionTextBorrower))
                                        question.QuestionText = question.QuestionTextBorrower;
                                }
                            }
                             if (section.SectionId != Convert.ToInt32(SectionDetails.Profile))
                        {
                            //Replace Placeholder value
                            question.QuestionText = question.QuestionText.Replace("<#spouseFirstName#>",loggerInUserSpouseFirstName);
                        }
                        }
                    }   
                }
                else
                {
                    foreach(var subSection in subSections)
                    {
                        if(isLoggedUserBorrowerOrCoBorrower)
                        {
                            if(!string.IsNullOrEmpty(subSection.SubSectionTextSpouse))
                                subSection.SubSectionName = subSection.SubSectionTextSpouse;
                        }
                        else
                        {
                            if(!string.IsNullOrEmpty(subSection.SubSectionTextBorrower))
                                subSection.SubSectionName = subSection.SubSectionTextBorrower;
                        }
                         if (section.SectionId != Convert.ToInt32(SectionDetails.Profile))
                        {
                         subSection.SubSectionName = subSection.SubSectionName.Replace("<#spouseFirstName#>",loggerInUserSpouseFirstName);
                          subSection.SubSectionName = subSection.SubSectionName.Replace("<#loggedFirstName#>",loggerInUserFirstName);
                        }
                        
                        var tempQuestions = subSection.QuestionSections
                                                      .Where(x => x.QuestionList != null)
                                                      .SelectMany(qs => qs.QuestionList);
                        foreach(var question in tempQuestions)
                        {
                            if(isLoggedUserBorrowerOrCoBorrower)
                            {
                                if(!string.IsNullOrEmpty(question.QuestionTextSpouse))
                                    question.QuestionText = question.QuestionTextSpouse;
                            }
                            else
                            {
                                if(!string.IsNullOrEmpty(question.QuestionTextBorrower))
                                    question.QuestionText = question.QuestionTextBorrower;
                            }
                             if (section.SectionId != Convert.ToInt32(SectionDetails.Profile))
                        {
                            //Replace Placeholder value
                        question.QuestionText = question.QuestionText.Replace("<#spouseFirstName#>",loggerInUserSpouseFirstName);
                        }

                        }
                    }   
                }
            }

            return await Task.Run(() => true);
        }

        private async Task<ISectionResponse> ProcessSensitiveAnswerAndFillPlaceholders(IQuestionnaire questionnaire, ISection section)
        {
            ISectionResponse sectionResponse = new SectionResponse();
            var placeholders = section.PlaceHolders;

            var questions = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections)
                                                                           .SelectMany(ss => ss.QuestionSections)
                                                                           .Where(x => x.QuestionList != null)
                                                                           .SelectMany(qs => qs.QuestionList);

            // TODO: <#fname#> must be replaced by loggedin user firstname while it is assigning first occurance of firstname value 
            // i.e. primary borrowers firstname

            foreach (var question in questions)
            {
                if (question != null)
                {
                    //Loop through all the questions and see if it is "IsSensitive". Decrypt if it is IsSensitive.
                    question.Answer = DecryptAnswer(question);

                    if (question.PlaceHolder != null)
                    {
                        if (!placeholders.ContainsKey(question.PlaceHolder))
                        {
                            placeholders.Add(question.PlaceHolder, question.Answer);
                        }
                        else
                        {
                            placeholders[question.PlaceHolder] = question.Answer;
                        }
                    }
                }
            }

            sectionResponse.Section = section;
            sectionResponse.PlaceHolders = placeholders;

            return await Task.Run(() => sectionResponse);
        }

        private async Task<bool> ReplaceFNamePlaceholder(ISection section, List<IBorrowerCoBorrowerSpouseInfo> borrowerList)
        {
            // replace placeholders with actual values
            foreach (var applicantInList in borrowerList)
            {
                var subSections = section.SubSections
                                            .Where(s => s.UserName == applicantInList.UserName)
                                            .ToList();
                if(string.IsNullOrEmpty(applicantInList.FirstName))
                    continue;

                for (var i = 0; i < subSections.Count; i++)
                {
                    subSections[i].SubSectionName = subSections[i].SubSectionName.Replace("<#fName#>", applicantInList.FirstName);
                    subSections[i].FirstName = applicantInList.FirstName;

                    var tempQuestions = subSections[i].QuestionSections
                                                                    .Where(x => x.QuestionList != null)
                                                                    .SelectMany(qs => qs.QuestionList);
                    foreach (var question in tempQuestions)
                    {
                        if (question != null)
                        {
                            question.QuestionText = question.QuestionText.Replace("<#fName#>", applicantInList.FirstName);
                        }
                    }
                }
            }

            return await Task.Run(() => true);
        }

        private void ProcessIncomeSection(string temporaryApplicationNumber, ISection section, List<IBorrowerCoBorrowerSpouseInfo> borrowerList, int sectionId)
        {
            var fNamePlaceHolder = "<#fName#>";
            var coBorrowerFirstNamePlaceHolder = "cobo";

            var appFieldMap = QuestionnaireConfigurations.ApplicantDynamicSections;

            var currentSectionTemplatesFromMap = appFieldMap.Map.Where(m => m.SectionId == sectionId)
                                                                 .Select(t => t.TemplateId).ToList();

            var newTemplates = new Dictionary<string, SubSection>();
            var coBorrowerPlaceHolders = new Dictionary<string, string>();
    
            // Get list of templates to be created for coborrower
            var templatesToCopyForBorrower = section.Templates.Where(t => !currentSectionTemplatesFromMap.Contains(t.Key));

            var coBorrowerUserNames = new List<string>();

            // for each templates from which sub sections are created                        
            foreach (var _templateFromMap in currentSectionTemplatesFromMap)
            {
                // get subSections which are created from template
                var coBorrowerSubSections = section.SubSections
                    .Where(ss => ss.SubSectionId.Contains(_templateFromMap)
                    && borrowerList.Any(b => b.UserName == ss.UserName));

                // for each subSections created from templates for coBorrowers
                foreach (var _subSection in coBorrowerSubSections)
                {
                    if (!coBorrowerUserNames.Contains(_subSection.UserName))
                    {
                        coBorrowerUserNames.Add(_subSection.UserName);

                        var _fName = borrowerList.Where(b => b.UserName == _subSection.UserName).Select(b => b.FirstName).FirstOrDefault();
                        var coBorrowerNamePlaceHolder = "<#" + coBorrowerFirstNamePlaceHolder + coBorrowerUserNames.Count.ToString() + "#>";

                        // Set copy rental template Id if not set
                        SetTemplateIdForCopiedRentalIncome(temporaryApplicationNumber,$"{ConstantUtils.RentalIncome}{coBorrowerUserNames.Count.ToString()}", _subSection.UserName,section.SubSections);

                        foreach (var template in templatesToCopyForBorrower)
                        {
                            // var _templateJson = JSON.Serialize(template.Value);

                            // create new templates for coBorrower
                            var _newTemplate = new SubSection();
                            // _newTemplate = JSON.Deserialize<SubSection>(_templateJson);
                            _newTemplate = Extensions.Clone<SubSection>(template.Value);
                            var newTemplateId = template.Key + coBorrowerUserNames.Count.ToString();
                            if (_newTemplate.SeqNo.HasValue)
                            {
                                if (_newTemplate.SeqNo.Value > 0)
                                {
                                    _newTemplate.SeqNo = _newTemplate.SeqNo + _subSection.SeqNo;
                                }
                            }

                            _newTemplate.TemplateId = template.Value.TemplateId + coBorrowerUserNames.Count.ToString();
                            _newTemplate.SubSectionName = _newTemplate.SubSectionName.Replace(fNamePlaceHolder, coBorrowerNamePlaceHolder);
                            var tempQuestions = _newTemplate.QuestionSections
                                                                .Where(x => x.QuestionList != null)
                                                                .SelectMany(qs => qs.QuestionList);

                            foreach (var question in tempQuestions)
                            {
                                if (question != null)
                                {
                                    if (template.Key.ToLower() == "alimony" || template.Key.ToLower() == "alimonysupport")
                                    {
                                        if (question.ChildQuestions != null)
                                        {
                                            foreach (var child in question.ChildQuestions)
                                            {
                                                child.QuestionId = coBorrowerUserNames.Count.ToString() + "_" + child.QuestionId;
                                            }
                                        }

                                        question.QuestionId = coBorrowerUserNames.Count.ToString() + "_" + question.QuestionId;
                                    }

                                    question.QuestionText = question.QuestionText.Replace(fNamePlaceHolder, coBorrowerNamePlaceHolder);
                                }
                            }

                            foreach (var _footerInput in _newTemplate.FooterInputs)
                            {
                                // Update templateIds for coborrower in Footer inputs
                                for (var templateIndex = 0; templateIndex < _footerInput.dynamicTemplate.TemplateIds.Count; templateIndex++)
                                {
                                    _footerInput.dynamicTemplate.TemplateIds[templateIndex] = _footerInput.dynamicTemplate.TemplateIds[templateIndex] + Convert.ToString(coBorrowerUserNames.Count);
                                }
                            }

                            // Get dynamicTemplate to be updated by new templateId
                            var dynamicTemplatesToUpdate = _newTemplate.QuestionSections.SelectMany(q => q.QuestionList).Where(q => q.DynamicTemplate != null).SelectMany(q => q.DynamicTemplate).Where(t => t.TemplateIds != null);

                            foreach (var _dynamicTemplate in dynamicTemplatesToUpdate)
                            {
                                for (var templateIndex = 0; templateIndex < _dynamicTemplate.TemplateIds.Count; templateIndex++)
                                {
                                    _dynamicTemplate.TemplateIds[templateIndex] = _dynamicTemplate.TemplateIds[templateIndex] + Convert.ToString(coBorrowerUserNames.Count);
                                }
                            }
                                    
                            newTemplates.Add(newTemplateId, _newTemplate);
                        }

                                // create new placeholder with value for each coBorrower
                        coBorrowerPlaceHolders.Add(coBorrowerNamePlaceHolder, _fName);
                    }

                    // Get dynamicTemplate to be updated by new templateId
                    var dynamicTemplateToUpdate = _subSection.QuestionSections.SelectMany(q => q.QuestionList).Where(q => q.DynamicTemplate != null).SelectMany(q => q.DynamicTemplate);

                    foreach (var _dynamicTemplate in dynamicTemplateToUpdate)
                    {
                        for (var templateIndex = 0; templateIndex < _dynamicTemplate.TemplateIds.Count; templateIndex++)
                        {
                            _dynamicTemplate.TemplateIds[templateIndex] = _dynamicTemplate.TemplateIds[templateIndex] + Convert.ToString(coBorrowerUserNames.IndexOf(_subSection.UserName) + 1);
                        }
                    }
                }

            }
     
            //// DOC-2798 : Title not coming for spouse in self employment another company section - Update dynamic template
            
            var spouseUserName = borrowerList
                                        .Where(x => x.ActualApplicantType == ApplicantType.Spouse || x.ActualApplicantType == ApplicantType.CoBorrowerSpouse)
                                        .Select(s => s.UserName)
                                        .FirstOrDefault();
            
            var businessAnotherTemplate = "businessAnother";

            var businessAnotherSubSections = section.SubSections
                                                    .Where(ss => ss.UserName == spouseUserName && ss.TemplateId == businessAnotherTemplate)
                                                    .ToList();

            var dynamicTemplatesToUpdateResult = businessAnotherSubSections.SelectMany(x => x.FooterInputs).Select(y => y.dynamicTemplate);

            foreach (var _dynamicTemplate in dynamicTemplatesToUpdateResult)
            {
                for (var templateIndex = 0; templateIndex < _dynamicTemplate.TemplateIds.Count; templateIndex++)
                {
                    _dynamicTemplate.TemplateIds[templateIndex] =
                        _dynamicTemplate.TemplateIds[templateIndex] +
                        Convert.ToString(coBorrowerUserNames.Count);
                }
            }
            //// END

            foreach (var item in newTemplates)
            {
                section.Templates.Add(item.Key, item.Value);
            }

            foreach (var _placeHolder in coBorrowerPlaceHolders)
            {
                // Update subsection names for coBorrower
                var subSections = section.SubSections.Where(s => s.SubSectionName.Contains(_placeHolder.Key)).ToList();
                for (var i = 0; i < subSections.Count; i++)
                {
                    subSections[i].SubSectionName = subSections[i].SubSectionName.Replace(_placeHolder.Key, _placeHolder.Value);
                }

                section.PlaceHolders.Add(_placeHolder.Key, _placeHolder.Value);
            }
        }


        private void SetTemplateIdForCopiedRentalIncome(string temporaryApplicationNumber, string newTemplateId,string userName, List<ISubSection> subSections)
        {
            if (subSections != null)
            {
                var subSectionRentalIncomeData = subSections
                      .Where(ss => (ss.TemplateFieldName != null && (ss.TemplateFieldName.ToLower().Equals(ConstantUtils.RentalIncome.ToLower())
                      || ss.TemplateFieldName.ToLower().Equals(ConstantUtils._RentalIncome.ToLower()))
                      && ss.UserName == userName && ss.TemplateId==null)).ToList();
                if (subSectionRentalIncomeData.Any())
                {
                    List<string> subSectionIds = new List<string>();
                    foreach (var subSection in subSectionRentalIncomeData)
                    {
                        subSectionIds.Add(subSection.SubSectionId);
                        subSection.TemplateId = newTemplateId;
                    }

                   bool IsAcknowledged =  QuestionSubSectionsRepository.UpdateSubSectionWithTemplateId(temporaryApplicationNumber, Convert.ToInt32(SectionDetails.Income), subSectionIds, userName, newTemplateId).Result;
                }
            }
        }
        private async Task<bool> ValidateGetSectionRequest(string temporaryApplicationNumber, int sectionId)
        {
            if(string.IsNullOrEmpty(temporaryApplicationNumber))
            {
                throw new ArgumentException("temporaryApplicationNumber is required");
            }

            return await Task.Run(() => true);
        }

        private async Task<ISection> HideSubSectionFromSection(ISection section, string subSectionId)
        {
            var hideSection = section.SubSections
                                         .Where(ss => ss.SubSectionId == subSectionId)
                                         .FirstOrDefault();

            if(hideSection != null) 
                hideSection.isDisplay = false;
            
            return await Task.Run(() => section);
        }

        private async Task<ISection> RemoveSubSectionFromSection(ISection section, string subSectionId)
        {
            
            var removeSection = section.SubSections
                                        .Where(ss => ss.SubSectionId == subSectionId)
                                        .FirstOrDefault();

            if(removeSection != null) 
                section.SubSections.Remove(removeSection);

            return await Task.Run(() => section);
        }


        public async Task ProcessProfileSection(ISection section, IBorrowerCoBorrowerInfo loggedBorrower, IBorrowerCoBorrowerInfo loggedBorrowerCoborrower)
        {
            var isBorrowerOrSpouse = (loggedBorrower.ApplicantType == ApplicantType.Borrower) ||
                                      (loggedBorrower.ApplicantType == ApplicantType.Spouse); 
            
            var firstNameValue = loggedBorrower.FirstName;
            var firstNameValueCoborrower = loggedBorrower.FirstName;

            var loggedBorrowerCoborrowerUserName = loggedBorrower.UserName;
            if(loggedBorrowerCoborrower != null && !string.IsNullOrEmpty(loggedBorrowerCoborrower.UserName))
            {
                loggedBorrowerCoborrowerUserName = loggedBorrowerCoborrower.UserName;
                firstNameValueCoborrower = loggedBorrowerCoborrower.FirstName;
            }

            if(loggedBorrower.ApplicantType == ApplicantType.Spouse)
            {
                /// Not to show "who is on loan" to spouse 
                section = await HideSubSectionFromSection(section, ConstantUtils.JointCreditSubSectionId);

                /// Not to show "invitation info" to spouse 
                var s = section.SubSections
                                .FirstOrDefault(x=>x.TemplateFieldName == "coBorrowerInvitationInfo");
                if(s!=null)
                {
                    section = await RemoveSubSectionFromSection(section,s.SubSectionId);
                }

                var subsectionsToDisplay = new List<string>(){"spouseInfo","spouseCommunicationInfo"};

                foreach(var templateName in subsectionsToDisplay)
                {
                    var subSectionInfo = section.SubSections
                                .FirstOrDefault(x=>x.TemplateFieldName == templateName);
                    if(subSectionInfo!=null)
                    {
                        subSectionInfo.isDisplay = true;
                    }
                }
            }

            if(loggedBorrower.ApplicantType == ApplicantType.CoBorrowerSpouse)
            {
                /// Not to show "who is on loan" to spouse 
                var ssCoborrowerSpouseInvitation = section.SubSections.FirstOrDefault(x=>x.TemplateFieldName=="coBorrowerMaritalStatusInfo");
                if(ssCoborrowerSpouseInvitation != null && !string.IsNullOrEmpty(ssCoborrowerSpouseInvitation.SubSectionId))
                    section = await HideSubSectionFromSection(section, ssCoborrowerSpouseInvitation.SubSectionId);

                var subsectionsToDisplay = new List<string>(){"coBorrowerSpouseInfo","coBorrowerSpouseCommunicationInfo"};

                foreach(var templateName in subsectionsToDisplay)
                {
                    var subSectionInfo = section.SubSections
                                .FirstOrDefault(x=>x.TemplateFieldName == templateName && 
                                                    (x.UserName == loggedBorrower.UserName || x.UserName == loggedBorrowerCoborrowerUserName));
                    if(subSectionInfo!=null)
                    {
                        subSectionInfo.isDisplay = true;
                    }
                }
            }

            if(loggedBorrower.ApplicantType == ApplicantType.Borrower)
            {
                /// Not to show "spouseInfo"  "spousecommunicationinfo" to primary borrower
                var subsectionsToRemove = new List<string>(){"spouseInfo","spouseCommunicationInfo"};
                
                foreach(var templateName in subsectionsToRemove)
                {
                    var s = section.SubSections
                                .FirstOrDefault(x=>x.TemplateFieldName == templateName);
                    if(s!=null)
                    {
                        section = await RemoveSubSectionFromSection(section,s.SubSectionId);
                    }
                }
            }

            if (!isBorrowerOrSpouse)
            {
                // hide SS1 
                section = await HideSubSectionFromSection(section, "SS1");

                // remove SS12
                section = await RemoveSubSectionFromSection(section, "SS12");

                if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrower)
                {
                    var coBorrowerMaritalStatusInfo = section.SubSections
                                                   .Where(ss => ss.TemplateId == "coBorrowerMaritalStatusInfo" && ss.UserName == loggedBorrower.UserName) 
                                                   .FirstOrDefault();

                    if(coBorrowerMaritalStatusInfo != null)
                    {
                        coBorrowerMaritalStatusInfo.isDisplay = true;

                        var questionList = coBorrowerMaritalStatusInfo
                                            .QuestionSections
                                            .SelectMany(x => x.QuestionList);

                        
                        var coborrowerResidenceDetail = section.SubSections
                                                   .Where(ss => ss.TemplateId == "coborrowerResidenceDetail" 
                                                                && (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName)) 
                                                   .FirstOrDefault();

                        if(coborrowerResidenceDetail != null)
                        {
                            var subSectionId = coborrowerResidenceDetail.SubSectionId;

                            foreach (var question in questionList)
                            {
                                if(question.QuestionFieldName == "BorrowerAddSpouseAsCoBorrower")
                                {
                                    var childquestion = question.ChildQuestions;
                                    if(childquestion != null)
                                    {
                                        foreach(var c in childquestion)
                                        {
                                            if(c.QuestionId == "44") // only required for add spouse as same resident 
                                                c.SubSectionId = subSectionId;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                else if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrowerSpouse && loggedBorrowerCoborrower != null)
                {
                    var coBorrowerMaritalStatusInfo = section.SubSections
                                                   .Where(ss => ss.TemplateId == "coBorrowerMaritalStatusInfo" 
                                                                && (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName)) 
                                                   .FirstOrDefault();

                    if(coBorrowerMaritalStatusInfo != null)
                    {
                        //coBorrowerMaritalStatusInfo.isDisplay = true;
                        
                        var questionList = coBorrowerMaritalStatusInfo
                                            .QuestionSections
                                            .SelectMany(x => x.QuestionList);

                        foreach (var question in questionList)
                        {
                            question.IsReadOnly = true;
                            question.Required = Extensions.GetDescription(RequiredType.False);
                        }

                        //Set child subsection

                        var coborrowerResidenceDetail = section.SubSections
                                                   .Where(ss => ss.TemplateId == "coborrowerResidenceDetail" 
                                                                && (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName)) 
                                                   .FirstOrDefault();

                        if(coborrowerResidenceDetail != null)
                        {
                            var subSectionId = coborrowerResidenceDetail.SubSectionId;

                            foreach (var question in questionList)
                            {
                               if(question.QuestionFieldName == "BorrowerAddSpouseAsCoBorrower")
                                {
                                    var childquestion = question.ChildQuestions;
                                    if(childquestion != null)
                                    {
                                        foreach(var c in childquestion)
                                        {
                                            c.SubSectionId = subSectionId;
                                        }
                                    }
                                }
                            }

                        }

                    }
                }
                
                var firstSeqNumber = section.SubSections.OrderBy(x => x.SeqNo)
                                            .FirstOrDefault()
                                            .SeqNo;

                var coBorrowerInfoSection = section.SubSections
                                                   .Where(ss => ss.TemplateId == "coBorrowerInfo" && 
                                                   (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName))
                                                   .FirstOrDefault();
                
                if(coBorrowerInfoSection != null) 
                {
                    coBorrowerInfoSection.SeqNo = firstSeqNumber - 2;
                    // required for back button
                    coBorrowerInfoSection.isDisplay = true;
                    
                    var coBorrowerCommunicationInfoSection = section.SubSections
                                                            .Where(ss => ss.TemplateId == "coBorrowerCommunicationInfo" && 
                                                            (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName))
                                                            .FirstOrDefault();

                    if(coBorrowerCommunicationInfoSection != null)
                    {
                        coBorrowerCommunicationInfoSection.SeqNo = firstSeqNumber - 1;
                        coBorrowerCommunicationInfoSection.isDisplay = true;
                    }

                    var coBorrowerREOInfoSubSection = section.SubSections
                                                             .Where(ss => ss.TemplateId == "coBorrowerREOInfo" && 
                                                                          (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName))
                                                             .FirstOrDefault();
                        
                    var coborrowerResidenceDetailSubSection = section.SubSections
                                                             .Where(ss => ss.TemplateId == "coborrowerResidenceDetail" && 
                                                                         (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName))
                                                             .FirstOrDefault();
                    
                }

                 var coBorrowerSpouseInfoSection = section.SubSections
                                                   .Where(ss => ss.TemplateId == "coBorrowerSpouseInfo" && 
                                                   (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName))
                                                   .FirstOrDefault();
                
                if(coBorrowerSpouseInfoSection != null)
                {
                    if(loggedBorrower.ActualApplicantType == ApplicantType.CoBorrowerSpouse && loggedBorrowerCoborrower != null)
                    {
                        firstNameValue = loggedBorrowerCoborrower.FirstName;

                        //Update SSN & DOB of coborrower spouse should be skippable for co-borrower 
                        var questionListSpouseInfo = coBorrowerSpouseInfoSection.QuestionSections
                                                                 .SelectMany(x=>x.QuestionList)
                                                                 .Where(x=> x.QuestionFieldName == "BorrowerSSN" || x.QuestionFieldName == "BorrowerBirthDate" );

                        foreach(var question in questionListSpouseInfo)
                        {
                            question.Required = Extensions.GetDescription(RequiredType.True);
                        }
                    }
                    else
                    {
                        firstNameValue =  loggedBorrower.FirstName;
                    }

                    coBorrowerSpouseInfoSection.SubSectionName = coBorrowerSpouseInfoSection
                                                                                 .SubSectionName
                                                                                 .Replace("<#fName#>", firstNameValue);
                    
                    var coBorrowerSpouseCommunicationInfo = section.SubSections
                                                             .Where(ss => ss.TemplateId == "coBorrowerSpouseCommunicationInfo" && 
                                                                         (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName))
                                                             .FirstOrDefault();
                    if(coBorrowerSpouseCommunicationInfo != null)
                    {
                         coBorrowerSpouseCommunicationInfo.SubSectionName = coBorrowerSpouseCommunicationInfo
                                                                                 .SubSectionName
                                                                                 .Replace("<#fName#>", firstNameValue);
                    }
                }

                // Purchase Property Details
                var purchasePropertyDetailsSection = section.SubSections
                                                            .Where(ss => ss.SubSectionId == "SS2")
                                                            .FirstOrDefault();

                if (purchasePropertyDetailsSection != null) purchasePropertyDetailsSection.SubSectionName = "Purchase property details";

                // SS3 Loan Details
                var loanDetailsSection = section.SubSections
                                                .Where(ss => ss.SubSectionId == "SS3")
                                                .FirstOrDefault();
                
                if (loanDetailsSection != null) loanDetailsSection.SubSectionName = "Loan details";

                // SS9 RealEstate
                var realEstateSection = section.SubSections
                                               .Where(ss => ss.SubSectionId == "SS9")
                                               .FirstOrDefault();

                if (realEstateSection != null) realEstateSection.SubSectionName = "Real estate agent details";

                // SS11 Loan Officer
                var loanOfficerSection = section.SubSections
                                                .Where(ss => ss.SubSectionId == "SS11")
                                                .FirstOrDefault();

                if (loanOfficerSection != null) loanOfficerSection.SubSectionName = "Loan officer details";
            }
            
            if(isBorrowerOrSpouse)
            {
                if(loggedBorrower.ActualApplicantType == ApplicantType.Spouse && !string.IsNullOrEmpty(section.PlaceHolders["<#fName#>"]))
                {
                    firstNameValue = section.PlaceHolders["<#fName#>"];
                }
                else
                {
                    firstNameValue =  loggedBorrower.FirstName;
                }

                //spouseInfo
                var spouseInfo = section.SubSections
                                                   .Where(ss => ss.TemplateId == "spouseInfo" 
                                                   && (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName)) 
                                                   .FirstOrDefault();
                if(spouseInfo != null)
                {
                    spouseInfo.SubSectionName = spouseInfo
                                                    .SubSectionName
                                                    .Replace("<#fName#>", firstNameValue);
                        
                }

                //spouseInfo
                var spouseCommunicationInfo = section.SubSections
                                                   .Where(ss => ss.TemplateId == "spouseCommunicationInfo" 
                                                   && (ss.UserName == loggedBorrower.UserName || ss.UserName == loggedBorrowerCoborrowerUserName)) 
                                                   .FirstOrDefault();
                if(spouseCommunicationInfo != null)
                {
                    spouseCommunicationInfo.SubSectionName = spouseCommunicationInfo
                                                    .SubSectionName
                                                    .Replace("<#fName#>", firstNameValue);
                        
                }
            }
        }

        private async Task<bool> ProcessPlaceHolders(ISection section)
        {
            const string strPleaseListAllProp = "Please, list all the properties you currently own.";
            const string strRefinancingFIRST = "Please list the property you are refinancing FIRST.";
            const string strDoYouOwnRentNoPrimary = "Do you own, rent or no primary housing expense?";

            var placeholders = section.PlaceHolders;

            if(placeholders == null && placeholders.Count() < 1)
            {
                return await Task.Run(()=>false);
            }

            if (section.SectionCompletion != null)
            {
                var sectionCompletion = section.SectionCompletion.MessageHeader;

                if (!string.IsNullOrEmpty(placeholders["<#fName#>"]))
                {
                    for (var i = 0; i < sectionCompletion.Count(); i++)
                    {
                        sectionCompletion[i] = sectionCompletion[i].Replace("<#fName#>", placeholders["<#fName#>"]);
                    }
                }
            }

            var subSections = section.SubSections;
            
            for (var i = 0; i < subSections.Count; i++)
            {
                //if (!string.IsNullOrEmpty(placeholders["<#fName#>"]))
                    //subSections[i].SubSectionName = subSections[i].SubSectionName.Replace("<#fName#>", placeholders["<#fName#>"]);

                
                if (subSections[i].SubSectionId == "SS2")
                {
                    if (!string.IsNullOrEmpty(placeholders["<#loanpurpose#>"]))
                    {
                        if (placeholders["<#loanpurpose#>"].ToLower().Equals("purchase"))
                        {
                            subSections[i].SubSectionName = subSections[i].SubSectionName.Replace("<#loanpurpose#>", "buying");
                        }
                        else if (placeholders["<#loanpurpose#>"].ToLower().Equals("refinance"))
                        {
                            subSections[i].SubSectionName = subSections[i].SubSectionName.Replace("<#loanpurpose#>", "looking to refinance");

                            var propertySelectedQuestionId = "12";
                            var borrowerPropertyType = "BorrowerPropertyType";
                            var borrowerPropertyUse = "BorrowerPropertyUse";
                            var childBorrowerExpectedMonthlyRentalIncomeId = "313";
                            var borrowerExpectedMonthlyRentalIncome = "BorrowerExpectedMonthlyRentalIncome";
                            var propertyTypeQuestionAnswer = subSections[i].QuestionSections
                                        .SelectMany(qs => qs.QuestionList)
                                        .FirstOrDefault(q => q.QuestionFieldName == borrowerPropertyType);

                            if(propertyTypeQuestionAnswer != null)
                            {
                                propertyTypeQuestionAnswer.IsParentQuestion = true;
                            }

                            var propertyUseQuestionAnswer = subSections[i].QuestionSections
                                        .SelectMany(qs => qs.QuestionList)
                                        .FirstOrDefault(q => q.QuestionFieldName == borrowerPropertyUse
                                        && q.QuestionId == propertySelectedQuestionId);

                            if (propertyUseQuestionAnswer != null)
                            {
                                propertyUseQuestionAnswer.IsParentQuestion = true;
                            }

                            var tempQuestionSection = subSections[i].QuestionSections
                                                        .Where(x => x.QuestionList != null).ToList();
                            
                            for (var j = tempQuestionSection.Count - 1; j >= 0; j--)
                            {
                                var tempQuestion = tempQuestionSection[j].QuestionList.FirstOrDefault(x => x.QuestionFieldName == borrowerExpectedMonthlyRentalIncome);
                                if (tempQuestion != null)
                                {
                                    subSections[i].QuestionSections.Remove(tempQuestionSection[j]);
                                }

                                var tempChildQuestion = tempQuestionSection[j].QuestionList.FirstOrDefault(x => x.QuestionFieldName == borrowerPropertyType);
                                if (tempChildQuestion != null)
                                {
                                    var tempChildAnswer = tempChildQuestion.ChildQuestions.FirstOrDefault(x => x.QuestionId == childBorrowerExpectedMonthlyRentalIncomeId);
                                    if (tempChildAnswer != null)
                                    {
                                        subSections[i].QuestionSections[j].QuestionList.FirstOrDefault(x => x.QuestionFieldName == borrowerPropertyType).ChildQuestions.Remove(tempChildAnswer);
                                    }
                                }

                                var tempChildQuestion2 = tempQuestionSection[j].QuestionList.FirstOrDefault(x => x.QuestionFieldName == borrowerPropertyUse);
                                if (tempChildQuestion2 != null)
                                {
                                    var tempChildAnswer = tempChildQuestion2.ChildQuestions.FirstOrDefault(x => x.QuestionId == childBorrowerExpectedMonthlyRentalIncomeId);
                                    if (tempChildAnswer != null)
                                    {
                                        subSections[i].QuestionSections[j].QuestionList.FirstOrDefault(x => x.QuestionFieldName == borrowerPropertyUse).ChildQuestions.Remove(tempChildAnswer);
                                    }
                                }
                            }
                        }
                    }
                }

                if (subSections[i].SubSectionId == "SS10")
                {
                    if (placeholders["<#loanpurpose#>"].ToLower().Equals(REFINANCE))
                    {
                        var tempQuestionSection = subSections[i].QuestionSections
                                .Where(x => x.QuestionList != null).ToList();

                        for (var j = tempQuestionSection.Count - 1; j >= 0; j--)
                        {
                            var tempChildQuestion = tempQuestionSection[j].QuestionList.FirstOrDefault(x => x.QuestionText == strDoYouOwnRentNoPrimary);
                            if (tempChildQuestion != null)
                            {
                                var tempChildAnswer = tempChildQuestion.ChildQuestions.FirstOrDefault(x => x.Answer == "Own");
                                if(tempChildAnswer != null)
                                {
                                    subSections[i].QuestionSections[j].QuestionList.FirstOrDefault(x => x.QuestionText == strDoYouOwnRentNoPrimary).ChildQuestions.Remove(tempChildAnswer);
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(placeholders["<#fName#>"]))
                {
                    var tempQuestions = subSections[i].QuestionSections
                                                                    .Where(x => x.QuestionList != null)
                                                                    .SelectMany(qs => qs.QuestionList);
                    foreach (var question in tempQuestions)
                    {
                        if (question != null)
                        {
                            //question.QuestionText = question.QuestionText.Replace("<#fName#>", placeholders["<#fName#>"]);
                            if (question.QuestionText == strPleaseListAllProp && placeholders["<#loanpurpose#>"].ToLower().Equals(REFINANCE))
                            {
                                question.QuestionText = strRefinancingFIRST;
                            }
                        }
                    }
                }
            }
                
            var templates = section.Templates;
            foreach (var template in templates)
            {
                var templateValue = template.Value;
                if (templateValue.TemplateFieldName == "borrowerREOInfo" || templateValue.TemplateFieldName == "coBorrowerREOInfo")
                {
                    var tempQuestions = templateValue.QuestionSections
                        .Where(x => x.QuestionList != null)
                        .SelectMany(qs => qs.QuestionList);
                    
                    foreach (var question in tempQuestions)
                    {
                        if (question != null)
                        {
                            if (question.QuestionText == strPleaseListAllProp && placeholders["<#loanpurpose#>"].ToLower().Equals(REFINANCE))
                            {
                                question.QuestionText = strRefinancingFIRST;
                            }
                        }
                    }
                }

                if (templateValue.TemplateFieldName == "spouseResidenceDetail" || templateValue.TemplateFieldName == "coborrowerResidenceDetail")
                {
                    if (placeholders["<#loanpurpose#>"].ToLower().Equals(REFINANCE))
                    {
                        var tempQuestionSection = templateValue.QuestionSections
                                .Where(x => x.QuestionList != null).ToList();

                        for (var i = tempQuestionSection.Count - 1; i >= 0; i--)
                        {
                            var tempChildQuestion = tempQuestionSection[i].QuestionList.FirstOrDefault(x => x.QuestionText == strDoYouOwnRentNoPrimary);
                            if (tempChildQuestion != null)
                            {
                                var tempChildAnswer = tempChildQuestion.ChildQuestions.FirstOrDefault(x => x.Answer == "Own");
                                if (tempChildAnswer != null)
                                {
                                    templateValue.QuestionSections[i].QuestionList.FirstOrDefault(x => x.QuestionText == strDoYouOwnRentNoPrimary).ChildQuestions.Remove(tempChildAnswer);
                                }
                            }
                        }
                    }
                }
            }

            return await Task.Run(()=>true);
        }

        private async Task<bool> ProcessForManualAssetWorkflow(ISection section)
        {
            var manualAssetWorkFlowChanges = QuestionnaireConfigurations.ManualAssetWorkFlowChangeList;

            if(manualAssetWorkFlowChanges == null)
            {
                return await Task.Run(()=>false);
            }

            var manualAssetWorkFlow = manualAssetWorkFlowChanges
                                      .Where(x => x.SectionId == section.SectionId)
                                      .FirstOrDefault();

            if(manualAssetWorkFlow == null)
            {
                return await Task.Run(()=>false);
            }

            if(manualAssetWorkFlow.UpdatedMessageHeader != null)
            {
                var sectionCompletion = section.SectionCompletion;
                sectionCompletion.MessageHeader = manualAssetWorkFlow.UpdatedMessageHeader;
            }

            if(manualAssetWorkFlow.RemoveDynamicTemplateList != null)
            {
                foreach(var dynamicTemplate in manualAssetWorkFlow.RemoveDynamicTemplateList)
                {
                    var subSectionId = dynamicTemplate.subSectionId;
                    var questionId = dynamicTemplate.QuestionId;
                    var questionList = section.SubSections.SelectMany(qs => qs.QuestionSections).SelectMany(qs => qs.QuestionList);
                    
                    if(questionList != null)
                    {
                        var question =  questionList
                                            .Where(x => x.QuestionId == questionId)
                                            .ToList();
                        
                        if(question != null)
                        {
                            foreach(var que in question)
                            {
                                var questionDynamicTemplate = que.DynamicTemplate;
                                        
                                if(questionDynamicTemplate != null && dynamicTemplate.TemplateIds !=null)
                                {
                                    foreach(var dTemplate in questionDynamicTemplate)
                                    {
                                            foreach(var template in dynamicTemplate.TemplateIds)
                                            {
                                                dTemplate.TemplateIds.RemoveAll(x => x.StartsWith(template));
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return await Task.Run(()=>true);
        }

        private async Task<ISubSection> GetIncomePersonalSection(IQuestionnaire questionnaire, string templateKey, string userName)
        {
            // const string TEMPLATE_INCOMESEARCH = "INCOMESEARCH";
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
            var srcPersonalInfoSection = questionnaire.ApplicationForm
                                                .Sections
                                                .Find(s => s.SectionId == profileSectionId)
                                                .SubSections
                                                .Find(ss => ss.SubSectionId.ToLower().Equals("ss1") && ss.UserName == userName);

            if(srcPersonalInfoSection == null)
            {
                srcPersonalInfoSection = questionnaire.ApplicationForm
                            .Sections.Where(s => s.SectionId == profileSectionId)
                            .SelectMany(s => s.SubSections).Where(ss => !string.IsNullOrEmpty(ss.TemplateId))
                            .Where(ss => ss.UserName == userName)
                            .Where(ss => ss.TemplateId.Equals("spouseInfo") || 
                                         ss.TemplateId.Equals("coBorrowerInfo") || 
                                         ss.TemplateId.Equals("coBorrowerSpouseInfo"))
                            .FirstOrDefault();
            }   
            
            return await Task.Run(()=> srcPersonalInfoSection);
        } 
        
        
        private async Task<ISubSection> GetIncomePersonalSection(ISection profileSection,  string userName)
        {
            var srcPersonalInfoSection = profileSection.SubSections.Find(ss => ss.SubSectionId.Equals("ss1", StringComparison.OrdinalIgnoreCase) && ss.UserName == userName);

            if(srcPersonalInfoSection == null)
            {
                srcPersonalInfoSection = profileSection.SubSections
                    .Where(ss => !string.IsNullOrEmpty(ss.TemplateId))
                    .Where(ss => ss.UserName == userName)
                    .FirstOrDefault(ss => ss.TemplateId.Equals("spouseInfo") || 
                                          ss.TemplateId.Equals("coBorrowerInfo") || 
                                          ss.TemplateId.Equals("coBorrowerSpouseInfo"));
            }   
            
            return await Task.Run(()=> srcPersonalInfoSection);
        } 

        private async Task<bool> PrefillDOBandSSN(IQuestionnaire questionnaire, ISection section, List<IBorrowerCoBorrowerSpouseInfo> borrowerList)
        {
            const string TEMPLATE_INCOMESEARCH = "INCOMESEARCH";
            var templatesToCopyForBorrower = section.Templates.Where(t => t.Key.ToLower().Contains(TEMPLATE_INCOMESEARCH.ToLower()));
            
            int userCounter = 1, templateCounter = 1;
            foreach(var borrower in borrowerList)
            {
                foreach (var template in templatesToCopyForBorrower)
                {
                    if(userCounter != templateCounter) 
                    {
                        templateCounter++;
                        continue;
                    }
                    var incomeTemplate = template.Value;

                    var incomeQuestions = incomeTemplate.QuestionSections
                                                        .SelectMany(qs => qs.QuestionList)
                                                        .Where(q => q.QuestionType == QuestionType.Date || q.QuestionType == QuestionType.TextSSN);

                    var srcPersonalInfoSection = await GetIncomePersonalSection(questionnaire, template.Key.ToLower(), borrower.UserName);
                    
                    if (srcPersonalInfoSection != null)
                    {
                        var personalInfoQuestions = srcPersonalInfoSection
                                                        .QuestionSections
                                                        .SelectMany(qs => qs.QuestionList)
                                                        .Where(q => q.QuestionType == QuestionType.Date || q.QuestionType == QuestionType.TextSSN)
                                                        .ToList();

                        foreach (var personalInfoQuestion in personalInfoQuestions)
                        {
                            foreach (var incomeQuestion in incomeQuestions)
                            {
                                if (personalInfoQuestion.QuestionType == QuestionType.Date && incomeQuestion.QuestionType == QuestionType.Date)
                                {
                                    incomeQuestion.Answer = personalInfoQuestion.Answer;
                                }
                                
                                if (personalInfoQuestion.QuestionType == QuestionType.TextSSN && incomeQuestion.QuestionType == QuestionType.TextSSN)
                                {
                                    incomeQuestion.Answer = personalInfoQuestion.Answer;
                                }
                            }
                        }
                    }
                    templateCounter++;
                }
                userCounter++;
                templateCounter = 1;
            }

            return await Task.Run(() => true);
        }

        /// <summary>
        /// This function is used to get the JSON of 1 raw section without any process at a time.
        /// Based on the section id, section json is fetched. It includes the collection of Subsections and templates.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<ISection> GetRawSection(string temporaryApplicationNumber, int sectionId)
        {
            //ISectionResponse sectionResponse = new SectionResponse();
            //Need to fetch the form using TemporaryApplicationID as well.

            var section = await QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, sectionId);

            if (section == null)
            {
                throw new SectionNotFoundException(
                    $"Section having the Section Id {sectionId} for the Temporary Application Number '{temporaryApplicationNumber}' cannot be found.");
            }

            return section;
        }

        /// <summary>
        /// This function provides the list of sections.
        /// It is used to draw left hand navigation
        /// </summary>
        /// <param name="formType"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        public async Task<ISectionListResponse> GetSectionList(string formType, string temporaryApplicationNumber = "")
        {
            IQuestionnaire questionnaire = null;
            //////
            //Add a Questionnaire if it is not added
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber))
            {
                questionnaire = await GetStarted(formType, false);
            }
            else
            {
                // TODO It can be improved. We dont require to pull entire questionnaire
                questionnaire = await QuestionnaireRepository.GetQuestionnaire(temporaryApplicationNumber);
            }

            if (questionnaire == null)
            {
                throw new Exception("Questionnaire could not get created in GetSectionList");
            }
            //////

            ISectionListResponse _sectionList = new SectionListResponse();

            var sectionGroups = new List<ISectionGroup>();
            var sections = new List<ISectionBasic>();

            temporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
            //This function collects information about all the applicants. It includes their First Name, Last Name, emaoil address, applicant type etc.
            var borrowerList = await GetBorrowersAndCoBorrowers(temporaryApplicationNumber, false, questionnaire);

            var userName = await GetCurrentUser();

            var _userState = questionnaire.UserState.Where(u => u.UserName == userName).FirstOrDefault();

            var userStatus = _userState.Status;

            var sectionType = SectionType.Questionnaire;
            switch (_userState.Status)
            {
                case FormStatus.Open:
                    sectionType = SectionType.Questionnaire;
                    break;
                case FormStatus.PostApplication:
                    sectionType = SectionType.PostApplication;
                    break;
            }

            foreach (ISectionBasic item in questionnaire.ApplicationForm.Sections.Where(s => s.Type == sectionType))
            {
                ISectionBasic sectionBasic = new SectionBasic();
                sectionBasic.SectionId = item.SectionId;
                sectionBasic.SectionName = item.SectionName;
                sectionBasic.SeqNo = item.SeqNo;
                sectionBasic.IsCompleted = (item.SectionId <= _userState?.HighestSectionReached) ? true : false; 
                sectionBasic.UserName = _userState?.UserName;
                sections.Add(sectionBasic);
            }

            if (borrowerList.Any() &&
                borrowerList.Any(x => x.ApplicantType == ApplicantType.CoBorrower && x.UserName == userName))
            {
                foreach (var item in sections)
                {
                    if (item.SectionId < _userState.HighestSectionReached)
                    {
                        item.IsCompleted = true;
                    }
                    else
                    {
                        item.IsCompleted = false;
                    }
                }
            }

            switch (userStatus)
            {
                case FormStatus.Open:
                    ISectionGroup _sectionGroup = new SectionGroup();
                    _sectionGroup.Name = QuestionnaireConfigurations.DefaultSectionGroupName;
                    _sectionGroup.Sections = sections;
                    _sectionGroup.Type = FormStatus.Open;

                    sectionGroups.Add(_sectionGroup);
                    break;

                case FormStatus.PostApplication:
                    foreach (var borrower in borrowerList)
                    {
                        _sectionGroup = new SectionGroup();
                        _sectionGroup.Name = string.Format("{0} {1}", borrower.FirstName, borrower.LastName);
                        _sectionGroup.Email = borrower.Email;
                        _sectionGroup.Type = questionnaire.Status;
                        _sectionGroup.Sections = sections.Where(s =>
                            s.UserName == borrower.UserName || string.IsNullOrEmpty(s.UserName)).ToList();

                        sectionGroups.Add(_sectionGroup);
                    }

                    break;
            }

            #region Update user state               

            _sectionList.LastAccessedSectionId = _userState.LastAccessedSection;
            _sectionList.LastAccessedSubSectionId = _userState.LastAccessedSubSectionId;
            _sectionList.HighestSectionReached = _userState.HighestSectionReached;
            _sectionList.HighestSubSectionReached = _userState.HighestSubSectionReached;

            #endregion

            _sectionList.GroupOfSections = sectionGroups;
            _sectionList.TemporaryApplicationNumber = questionnaire.TemporaryApplicationNumber;
            _sectionList.ReferenceApplicationNumber = questionnaire.ReferenceApplicationNumber;
            return await Task.Run(() => _sectionList);

            throw new SectionNotFoundException(
                $"No section present for the Temporary Application Number '{temporaryApplicationNumber}'.");
        }

        public async Task<bool> UpdateSectionCompletionStatus(string temporaryApplicationNumber, int sectionId, bool isCompleted)
        {
            ISection sectionFound = new Section();
            try
            {
                if (string.IsNullOrEmpty(temporaryApplicationNumber))
                {
                    //TODO: By Nayan: What if there is multiple applications
                    //If no TemporaryApplicationNumber was sent in the request, then get the TemporaryApplicationNumber of the logged in user
                    temporaryApplicationNumber = await GetTemporaryApplicationNumber();

                    if (temporaryApplicationNumber == null)
                    {
                        throw new QuestionnaireNotFoundException("No Questionnaire exists for the user.");
                    }
                }

                #region Update IsCompleted status of Section

                //Get Questionnaire for the applicant
                var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);

                if (questionnaire == null)
                {
                    throw new NotFoundException( $"Questionnaire for the applicant '{temporaryApplicationNumber}' not found");
                }

                var allSections=await QuestionSectionRepository.GetAllApplicationSectionWithoutDetail(temporaryApplicationNumber);
                if (allSections == null || !allSections.Any())
                {
                    throw new NotFoundException(
                        $"Questionnaire sections for the applicant '{temporaryApplicationNumber}' not found");
                }
                
                var nextSectionId = allSections.Where(s => s.SectionId > sectionId)
                    .Select(s => s.SectionId).FirstOrDefault();
                //Get the section that needs to be updated.
                
                var sectionToUpdate = allSections.FirstOrDefault(section =>section.SectionId == sectionId);

                if (sectionToUpdate == null)
                {
                    throw new SectionDoesNotExistsException($"Section '{sectionId}' does not exist.");
                }

                if (questionnaire.HighestSectionReached <= sectionId)
                {
                    // questionnaire.HighestSectionReached = sectionId + 1;
                    questionnaire.HighestSectionReached = nextSectionId;
                }

                var userName = await GetCurrentUser();
                var userStatus = questionnaire.UserState.FirstOrDefault(u => u.UserName == userName);
                 var flagForHigherSection = 0;
                if (nextSectionId == 0)
                {
                    //if NextSectionId = 0, then use the SectionId
                    nextSectionId = sectionId;
                }

                if (userStatus != null)
                {
                    userStatus.LastAccessedSection = nextSectionId;
                    userStatus.LastAccessedSubSectionId = null;
                    flagForHigherSection = userStatus.HighestSectionReached;

                    if (userStatus.HighestSectionReached <= sectionId && sectionId != 6)
                    {
                        userStatus.HighestSectionReached = nextSectionId;
                        // userStatus.HighestSectionReached = sectionId + 1;
                    }
                }

                await QuestionnaireRepository.UpdateQuestionnaireStatus(questionnaire.TemporaryApplicationNumber,
                    questionnaire.HighestSectionReached, questionnaire.HighestSubSectionReached, userStatus,
                    sectionToUpdate.SectionId,isCompleted);
                
                var result = true;
                try
                {
                    Logger.Info("CopiedRentalSectionDataForIncome - Started", new {temporaryApplicationNumber, sectionId,isCompleted, flagForHigherSection });
                    if(flagForHigherSection != 0 && flagForHigherSection < Convert.ToInt32(SectionDetails.Assets))
                    {
                        //TODO: By Nayan, Need to add validation if answer is already set by Copy. We should not overwrite it. We can get the specific question and if the answer is found for the income logo then skip this.                         
                        await CopiedRentalSectionDataForIncome(questionnaire.UserState, temporaryApplicationNumber);
                        Logger.Info("CopiedRentalSectionDataForIncome - Ended", new {temporaryApplicationNumber, sectionId,isCompleted,flagForHigherSection });
                    }
                    else
                    {
                        Logger.Info("CopiedRentalSectionDataForIncome - Not done", new {temporaryApplicationNumber, sectionId,isCompleted ,flagForHigherSection});
                    }                    
                }
                catch(Exception ex)
                {
                    Logger.Error("... CopiedRentalSectionDataForIncome - Error", ex,new {temporaryApplicationNumber, sectionId, isCompleted, flagForHigherSection});
                }

                if (result == true)
                {
                    Logger.Info("... publish SectionCompleted Event - Started");
                    await EventHubClient.Publish("SectionCompleted", new SectionCompleted(temporaryApplicationNumber, sectionId.ToString(), sectionToUpdate.SectionName, userName));
                    Logger.Info("... publish SectionCompleted Event - Completed");
                    return await Task.Run(() => true);
                }
                else
                {
                    throw new ArgumentException($"isCompleted property for the Section '{sectionId}' could not be updated");
                }

                #endregion
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                throw new SubSectionDoesNotExistsException(ex.Message, ex);
            }
            catch (AnswerNotProvidedException ex)
            {
                throw new AnswerNotProvidedException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}
