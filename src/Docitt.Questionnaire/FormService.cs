using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;

namespace Docitt.Questionnaire
{
    public class FormService : IFormService
    {
        private IFormRepository FormRepository { get; }

        public FormService(IFormRepository formRepository)
        {
            FormRepository = formRepository;
        }

        public async Task<IForm> GetActive(string formName)
        {
            if (string.IsNullOrWhiteSpace(formName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(formName));

            var form = await FormRepository.GetActive(formName);
            if (form == null)
            {
                throw new NotFoundException($"Unable to find active form for {formName}");
            }

            return form;
        }

        public async Task<IForm> Get(string formName, string version)
        {
            if (formName == null) throw new ArgumentNullException(nameof(formName));
            if (version == null) throw new ArgumentNullException(nameof(version));

            var form = await FormRepository.Get(formName, version);
            if (form == null)
            {
                throw new NotFoundException($"Unable to find form for {formName} version {version}");
            }

            return form;
        }

        public async Task<IForm> Add(IForm form)
        {
            if (form == null) throw new ArgumentNullException(nameof(form));
            if (string.IsNullOrWhiteSpace(form.Version)) throw new ArgumentNullException(nameof(form.Version));
            if (string.IsNullOrWhiteSpace(form.FormName)) throw new ArgumentNullException(nameof(form.FormName));

            var existing = await FormRepository.Get(form.FormName, form.Version);
            if (existing != null)
            {
                throw new ArgumentException($"Form already exist for {form.FormName} version {form.Version}");
            }
            
            FormRepository.Add(form);

            return form;
        }

        public async Task<IForm> Update(string formName, string version, IForm form)
        {
            if (form == null) throw new ArgumentNullException(nameof(form));
            if (string.IsNullOrWhiteSpace(form.Version)) throw new ArgumentNullException(nameof(form.Version));
            if (string.IsNullOrWhiteSpace(form.FormName)) throw new ArgumentNullException(nameof(form.FormName));

            var existing = await FormRepository.Get(formName, version);
            if (existing == null)
            {
                throw new NotFoundException($"Form not found for {form.FormName} version {form.Version}");
            }

            if (existing.IsActive == true || existing.WasEverActive == true)
            {
                throw new ArgumentException("Update is not allowed as form is active or was active in past");
            }
            
            form.Id = existing.Id;
            form.FormName = formName;
            form.Version = version;
            form.WasEverActive = existing.WasEverActive;

            FormRepository.Update(form);

            return form;
        }

        public async Task<List<IForm>> GetAll(string formName)
        {
            if (string.IsNullOrWhiteSpace(formName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(formName));

            return await FormRepository.GetAll(formName);
        }

        public async Task<List<IForm>> GetAll()
        {
            return await FormRepository.GetAll();
        }

        public async Task Activate(string formName, string version)
        {
            if (string.IsNullOrWhiteSpace(formName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(formName));
            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(version));

            var form = await FormRepository.Get(formName, version);
            if (form == null)
            {
                throw new NotFoundException($"Unable to find {formName} with version {version} ");
            }

            await FormRepository.Activate(formName, version);
        }

        public async Task Deactivate(string formName, string version)
        {
            if (string.IsNullOrWhiteSpace(formName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(formName));
            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(version));

            var form = await FormRepository.Get(formName, version);
            if (form == null)
            {
                throw new NotFoundException($"Unable to find {formName} with version {version} ");
            }

            await FormRepository.Deactivate(formName, version);
        }
    }
}