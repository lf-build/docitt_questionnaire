﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using LendFoundry.Security.Tokens;
using System;
using Docitt.Questionnaire.Abstractions;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        /// <summary>
        /// This function executes different events.
        /// These events are called on and "Next" and "Back" button click.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        public async Task<bool> ExecuteEvent(string temporaryApplicationNumber, int sectionId, string subSectionId, string eventName)
        {
            var userName = await GetCurrentUser();
            var status = true;
            var questionnaireWithoutDetail = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
            if (questionnaireWithoutDetail == null)
                throw new NotFoundException($"Questionnaire not found for application number{temporaryApplicationNumber}");

            await QuestionnaireRepository.UpdateLastAccessedStatus(temporaryApplicationNumber, userName, sectionId, subSectionId);


            
            try
            {
                List<AddCoBorrowerModal> addCoBorrowerModalList;
                switch (eventName)
                {
                    case "NewSendInvitation":

                        if (sectionId != Convert.ToInt32(SectionDetails.Profile))
                        {
                            throw new Exception("Invalid section id for event");
                        }
                        List<ISubSection> userInformationSubSections = null;
                        ISection profileSection = null;
                        FaultRetry.RunWithAlwaysRetry(() =>
                        {
                            profileSection=QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, sectionId).Result;
                            userInformationSubSections = GetUserInfoAndContactInfoSubsections(profileSection, subSectionId);
                        }, maxRetry: 5, sleepSeconds: 1);



                        addCoBorrowerModalList = new  List<AddCoBorrowerModal>();

                        if(userInformationSubSections != null && userInformationSubSections.Count >0)
                        {
                            //TODO: Why it's checking for first element?
                            var questionSectionList = userInformationSubSections[0].QuestionSections.Where(x=>x.QuestionSectionName=="AddCoBorrower");

                            foreach(var qSection in questionSectionList)
                            {
                                if (qSection.QuestionList == null || qSection.QuestionList.Count <= 0) continue;
                                var question = qSection.QuestionList[0];
                                if (!string.IsNullOrEmpty(question.ResultQuestionId)) continue;
                                var questionId = question.QuestionId;
                                var answer = question.Answer;

                                var obj = JsonConvert.DeserializeObject<AddCoBorrowerModal>(answer);
                                if (obj == null) continue;
                                obj.EmailAddress = obj.EmailAddress.ToLower();
                                obj.QuestionId = questionId;
                                addCoBorrowerModalList.Add(obj);
                            }
                        }

                        if(addCoBorrowerModalList.Count == 0)
                        {
                            return false;
                        }
                        else
                        {
                            foreach (var addCoBorrowerModal in addCoBorrowerModalList)
                            {
                                var guidId = addCoBorrowerModal.QuestionId.Substring(2,addCoBorrowerModal.QuestionId.Length-2);
                                var subSectionIdNew = $"ID-{guidId}-1";
                                FaultRetry.RunWithAlwaysRetry(() =>
                                {
                                    try
                                    {
                                        GetUserInfoAndContactInfoSubsections(profileSection, subSectionIdNew);
                                    }
                                    catch (Exception )
                                    {
                                        profileSection = QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, sectionId).Result;
                                        throw;
                                    }
                                }, maxRetry: 5, sleepSeconds: 1);
                                
                            }

                            return await NewSendInviteWorkflow(addCoBorrowerModalList, temporaryApplicationNumber,
                                profileSection, questionnaireWithoutDetail.UserState, ApplicantType.CoBorrower.ToString());
                        }
                     case "BorrowerSpouseInvitation":
                         if (sectionId != Convert.ToInt32(SectionDetails.Profile))
                         {
                             throw new Exception("Invalid section id for event");
                         }

                        //NOTE: This delay is added for the issue with phone number not getting saved and the event is getting published first
                         await Task.Delay(1000);
                         
                        Logger.Debug($" Start BorrowerSpouseInvitation temporaryApplicationNumber: {temporaryApplicationNumber}");
                        
                        List<ISubSection> profileSubSectionsForSpouse = null;

                        addCoBorrowerModalList = null;

                        ISection profileSectionForSpouse = null;
                         FaultRetry.RunWithAlwaysRetry(() =>
                         {
                             profileSectionForSpouse = QuestionnaireRepository
                                 .GetSectionById(temporaryApplicationNumber, sectionId).Result;
                             profileSubSectionsForSpouse =
                                 GetUserInfoAndContactInfoSubsections(profileSectionForSpouse, subSectionId);
                             addCoBorrowerModalList = GetSpouseInviteModal(profileSectionForSpouse,
                                 profileSubSectionsForSpouse, sectionId);
                         }, maxRetry: 5, sleepSeconds: 1);              

                        if(addCoBorrowerModalList.Count == 0)
                        {
                            Logger.Debug("No Spouse found in the list to invite");
                            return false;
                        }
                        else
                        {
                            Logger.Debug($" waiting for Spouse Template to update saved");
                            IsSpouseTemplateAvailable(temporaryApplicationNumber, profileSectionForSpouse);
                            var isSpouseAlreadyInvited = IsBorrowerSpouseAlreadyInvited(profileSectionForSpouse);
                           
                            var isBorrowerSpouseAlreadySigned = InviteValidationService.IsBorrowerSpouseAlreadySignedup(questionnaireWithoutDetail.UserState,userName);
                            if(isBorrowerSpouseAlreadySigned)
                            {
                                Logger.Debug("Spouse has already signed up");
                                return false;
                            }

                            if(isSpouseAlreadyInvited)
                            {
                                Logger.Debug("Invite is already send");                               
                                var resendInvitationRequest = InviteValidationService.GetBorrowerSpouseInviteDetails(questionnaireWithoutDetail.UserState, addCoBorrowerModalList[0], userName);
                                
                                if(resendInvitationRequest != null && !string.IsNullOrEmpty(resendInvitationRequest.invitationId))
                                {
                                    try
                                    {
                                        var response = InviteService.RegenerateInviteToken(resendInvitationRequest.invitationId, resendInvitationRequest.inviteEmailId);

                                        if (response != null)
                                        {
                                            var userStateUpdateStatus = await QuestionnaireRepository.UpdateUserEmailIdInUserState(
                                                temporaryApplicationNumber,
                                                resendInvitationRequest.invitationId,
                                                resendInvitationRequest.inviteEmailId
                                            );

                                            var subsectionId = await QuestionSubSectionsRepository.GetMaritalSubSectionId(
                                                temporaryApplicationNumber,
                                                resendInvitationRequest.invitationId,
                                                ConstantUtils.SpouseCommunicationInfoTemplate, // "spouseCommunicationInfo",
                                                Convert.ToInt32(SectionDetails.Profile)
                                            );

                                            var communicationSubSection = profileSectionForSpouse.SubSections
                                                .FirstOrDefault(x => x.SubSectionId.Equals(subsectionId));

                                            var questionList = communicationSubSection.QuestionSections
                                                                    .FirstOrDefault(x => x.QuestionList.Any(y => y.QuestionFieldName == ConstantUtils.QuestionBorrowerPreferredEmail));

                                            var questionId = communicationSubSection
                                                                .QuestionSections
                                                                .SelectMany(x => x.QuestionList)
                                                                .FirstOrDefault(x => x.QuestionFieldName == ConstantUtils.QuestionBorrowerPreferredEmail);

                                            var questionnaireQuestionAnswer = new QuestionnaireQuestionAnswer
                                            {
                                                TemporaryApplicationNumber = temporaryApplicationNumber,
                                                SectionId = Convert.ToInt32(SectionDetails.Profile),
                                                SubSectionId = subsectionId,
                                                QuestionSectionSeqNo = questionList.SeqNo, //19
                                                QuestionId = questionId.QuestionId, // "7"
                                                Answer = resendInvitationRequest.inviteEmailId
                                            };

                                            await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionnaireQuestionAnswer);
                                        }
                                        else
                                        {
                                            Logger.Warn("There was a problem in resending the invite");
                                            return false;
                                        }
                                    }
                                    catch(ClientException ex)
                                    {
                                        Logger.Warn("There was a problem in resending the invite");
                                        return false;
                                    }
                                    return true;
                                }                                
                                return false;
                            }
                        }                     
                        Logger.Debug("Starting NewSendInviteWorkflow");
                        status = await NewSendInviteWorkflow(addCoBorrowerModalList, temporaryApplicationNumber, profileSectionForSpouse, questionnaireWithoutDetail.UserState, ApplicantType.CoBorrowerSpouse.ToString());
                        Logger.Debug($"Completed NewSendInviteWorkflow for TemporaryApplicationNumber: {temporaryApplicationNumber}, Status - {status.ToString()}");

                        return status;
                        
                    case "CoborrowerSpouseInvitation":
                        //NOTE: This delay is added for the issue with phone number not getting saved and the event is getting published first
                        await Task.Delay(1000);

                        Logger.Debug($"Start CoborrowerSpouseInvitation temporaryApplicationNumber: {temporaryApplicationNumber}");
                        status = await InviteCoborrowerSpouse(temporaryApplicationNumber, sectionId, subSectionId);  
                        Logger.Debug($"End CoborrowerSpouseInvitation temporaryApplicationNumber: {temporaryApplicationNumber}, Status - {status.ToString()}");
                        return status;
                    
                    case "SameAsCurrentAddress":
                        return await ExecuteSameAsCurrentAddressEvent(temporaryApplicationNumber, sectionId, subSectionId, eventName);

                    case "LogConsent":

                        return await ExecuteLogConsentEvent(temporaryApplicationNumber, sectionId, subSectionId);
                    case "SetPurchaseRefinanceSection":

                        return await ShowHidePurchaseRefinanceSection(temporaryApplicationNumber);
                    case "SameAsRefinanceAddress":

                        return await PopulatingAddressForRefinance(temporaryApplicationNumber, sectionId, subSectionId);
                    case "PrimaryPropertyUse":
                    
                        return await PrimaryPropertyUseTrigger(temporaryApplicationNumber, sectionId, subSectionId);
                
                    case "UpdateUserProfile":
                        return await UpdateUserProfile(temporaryApplicationNumber);
                }
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                throw new SubSectionDoesNotExistsException(ex.Message);
            }
            catch (UserEmailAlreadyExistsException ex)
            {
                throw new UserEmailAlreadyExistsException(ex.Message);
            }
            return status;
        }

        private bool IsSpouseTemplateAvailable(string temporaryApplicationNumber,ISection profileSection)
        {
            Logger.Debug("Check if the spouseInfo and spouseCommunicationInfo templates are added");
            FaultRetry.RunWithAlwaysRetry(() =>
            {
                var isSpouseTemplateAvailable = InviteValidationService.IsSpouseTemplateAvailable(profileSection);
                if (isSpouseTemplateAvailable) return;
                profileSection = QuestionnaireRepository.GetSectionById(temporaryApplicationNumber,1).Result;
                throw new SubSectionDoesNotExistsException("SpouseCommunicationInfo SubSection not found");
            }, maxRetry: 5, sleepSeconds: 1);
            return true;
        }

        private bool IsBorrowerSpouseAlreadyInvited(ISection profileSection)
        {
            Logger.Debug("Check if the spouse is already invited");
            return InviteValidationService.IsBorrowerSpouseAlreadyInvited(profileSection);
        }

        
        private List<AddCoBorrowerModal> GetSpouseInviteModal(ISection section, List<ISubSection> subSectionsSpouseNew, int sectionId)
        {
            Logger.Debug("Inside GetSpouseInviteModal...");
            var addCoBorrowerModalList = new List<AddCoBorrowerModal>();

            Logger.Debug("Check if the application is JointApplication");
            var isSpouseCoApplicant = InviteValidationService.IsBorrowerSpouseCoApplicant(subSectionsSpouseNew);

            if(!isSpouseCoApplicant)
            {
                //TODO Delete spouse templates if any
                Logger.Debug("Exiting spouse invitation as its not a joint application");
                return addCoBorrowerModalList;
            }
            
            if(subSectionsSpouseNew != null && subSectionsSpouseNew.Count > 0)
            {
                var jointCreditSubsectionQuestionList = subSectionsSpouseNew[0].QuestionSections.SelectMany(x=>x.QuestionList);
                var borrowerAddSpouseAsCoBorrower =  jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "BorrowerAddSpouseAsCoBorrower");

                if(borrowerAddSpouseAsCoBorrower != null && borrowerAddSpouseAsCoBorrower.Answer == "true")
                {
                    var spouseFirstName =  jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseFirstNameMarried");
                    var spouseLastName =  jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpouseLastNameMarried");
                    var spouseEmail = jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == "SpousePreferredEmail");
                    
                    if(spouseEmail == null || string.IsNullOrEmpty(spouseEmail.Answer))
                    {
                        Logger.Debug("Email Address is mandatory for Inviting Spouse");
                        throw new QuestionNotFoundException("Spouse Email address not found");
                    }

                    var spouseInfoSubSection = section.SubSections
                                                        .Where(ss => !string.IsNullOrEmpty(ss.TemplateId))
                                                        .FirstOrDefault(ss => ss.TemplateId.Equals("spouseInfo"));

                    if(spouseInfoSubSection == null){
                        return addCoBorrowerModalList;
                    }

                    var spouseSubsectionId = spouseInfoSubSection.SubSectionId;

                    if(spouseEmail != null && !string.IsNullOrWhiteSpace(spouseEmail.Answer))
                    {
                        var addCoBorrowerModalObj = new  AddCoBorrowerModal{
                            FirstName =  spouseFirstName.Answer,
                            LastName = spouseLastName.Answer,
                            EmailAddress = spouseEmail.Answer.ToLower(),
                            QuestionId = spouseSubsectionId, //// As subsectionId pass
                            Relationship = ApplicantType.Spouse.ToString()
                        };

                        Logger.Debug($"Invite Modal created SpouseFirstName - {addCoBorrowerModalObj.FirstName}, " +
                                     $"SpouseLastName : {addCoBorrowerModalObj.LastName}," + 
                                     $"SpouseEmail : {addCoBorrowerModalObj.EmailAddress}");

                        addCoBorrowerModalList.Add(addCoBorrowerModalObj);
                    }
                    else
                    {
                        return addCoBorrowerModalList;
                    }
                }
            }

            return addCoBorrowerModalList;
        }
    }
}
