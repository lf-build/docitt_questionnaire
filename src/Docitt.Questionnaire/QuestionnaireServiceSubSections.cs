﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Docitt.Questionnaire.Events;
using LendFoundry.Foundation.Services;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
#if DOTNET2

#else
using Microsoft.AspNet.Http;
#endif

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        private static readonly Dictionary<string, string[]> TemplateMappingForSeqNo = new Dictionary<string, string[]>()
        {
            {"borrowerREOInfo", new[] {"borrowerREOInfo", "coBorrowerREOInfo"}},
            {"spouseREOInfo", new[] {"borrowerREOInfo", "spouseREOInfo", "spouseREOAdditionalInfo"}},
            {"spouseREOAdditionalInfo", new[] {"spouseREOInfo", "spouseREOAdditionalInfo"}},
            {"coBorrowerREOInfo", new[] {"coBorrowerREOInfo"}},
            {"coboSpouseREOInfo", new[] {"coBorrowerREOInfo", "coboSpouseREOInfo", "coboSpouseREOAdditionalInfo"}},
            {"coboSpouseREOAdditionalInfo", new[] {"coboSpouseREOInfo", "coboSpouseREOAdditionalInfo"}}
        };
        private static readonly string[] SpecialUserNamePlaceHolders =
        {
            "[UserBased]", "[SpouseUserBased]"
        };

        #region "SubSections"

        /// <summary>
        /// This function appends the sub sections
        /// </summary>
        /// <param name="subSectionRequest"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public async Task<bool> AppendSubSections(AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, int sectionId) //, string temporaryapplicationNumber)
        {
            return await AppendSubSections(subSectionRequest, temporaryApplicationNumber, sectionId, false);
        }

        private async Task<bool> AppendSubSections(AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, int sectionId, bool isCalledFromCopy) //, string temporaryapplicationNumber)
        {
            if (subSectionRequest == null) throw new ArgumentNullException(nameof(subSectionRequest));
            if (temporaryApplicationNumber == null) throw new ArgumentNullException(nameof(temporaryApplicationNumber));
            if (subSectionRequest.SubSections == null || !subSectionRequest.SubSections.Any())
                throw new ArgumentNullException(nameof(subSectionRequest), "At least one subsection is required");
            

            subSectionRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
            
            var currentUserName = await GetCurrentUser();
            var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
            if (questionnaire == null)
            {
                throw new NotFoundException($"Questionnaire not found for application number:{temporaryApplicationNumber}");
            }
            
            var userStatus = questionnaire.UserState.FirstOrDefault(u => u.UserName == currentUserName);
            if (userStatus == null)
            {
                throw new UnauthorizedAccessException($"User {currentUserName} not authorized for {temporaryApplicationNumber}");
            }
            
            var profileSectionTask= QuestionnaireRepository.GetSectionById(temporaryApplicationNumber,
                Convert.ToInt32(SectionDetails.Profile));
            
            var creditSectionTask=QuestionnaireRepository.GetSectionById(temporaryApplicationNumber,
                Convert.ToInt32(SectionDetails.Credit));

            ISection currentSection;
            if (sectionId != Convert.ToInt32(SectionDetails.Profile))
            {
                var currentSectionTask = QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, sectionId);
                await Task.WhenAll(profileSectionTask, creditSectionTask, currentSectionTask);
                currentSection = currentSectionTask.Result;
            }
            else
            {
                await Task.WhenAll(profileSectionTask, creditSectionTask);
                currentSection = profileSectionTask.Result;
            }
            
            var profileSection=profileSectionTask.Result;
            var creditSection = creditSectionTask.Result;



            var isCoBorrowerInvitation = false;
            var isCoBorrowerSpouseInvitation = false;


            if (sectionId == Convert.ToInt32(SectionDetails.Profile) && subSectionRequest.SubSections.Any())
            {
                isCoBorrowerSpouseInvitation =
                    subSectionRequest.SubSections.Any(r =>
                        string.Equals(r.TemplateId, "coBorrowerSpouseInfo", StringComparison.OrdinalIgnoreCase));

                isCoBorrowerInvitation =
                    subSectionRequest.SubSections.Any(r =>
                        string.Equals(r.TemplateId, "coBorrowerInfo", StringComparison.OrdinalIgnoreCase));

                if (isCoBorrowerSpouseInvitation)
                {
                    //Do nothing for spouse invitation
                }
                else if (isCoBorrowerInvitation)
                {
                    //Note: Add REO Section when CoBorrower is invited
                    var coBorrowerREOInfoTemplate =
                        currentSection.Templates.FirstOrDefault(x => x.Key == "coBorrowerREOInfo");
                    var borrowerREOInfoSubSectionSeqNo = currentSection.SubSections
                        .FirstOrDefault(
                            ss => string.Equals(ss.SubSectionId, "SS12", StringComparison.OrdinalIgnoreCase))?.SeqNo;
                    var coBorrowerREOInfoSubSection = coBorrowerREOInfoTemplate.Value;
                    //NOTE: 0th element contain the pure subsection id without -1
                    coBorrowerREOInfoSubSection.SubSectionId = subSectionRequest.SubSections[0].SubSectionId + "-3";
                    coBorrowerREOInfoSubSection.SeqNo = borrowerREOInfoSubSectionSeqNo.GetValueOrDefault() + 1;
                    //TODO: Not sure what this code is doing, hence commented oode.
                    //AddSectionForCoborrowerREOSection(coBorrowerREOInfoSubSection);
                    subSectionRequest.SubSections.Add(coBorrowerREOInfoSubSection);
                }
                else
                {
                    //Note: If it is not invitation, generate seq number for dynamic subsections for REO
                    foreach (var subSectionRequestSubSection in subSectionRequest.SubSections)
                    {
                        if (subSectionRequestSubSection.TemplateFieldName == null) continue;

                        if (!TemplateMappingForSeqNo.TryGetValue(subSectionRequestSubSection.TemplateFieldName,
                            out var subSectionsForSeqNo)) continue;

                        var borrowerREOInfoSubSection = currentSection.SubSections.LastOrDefault(ss =>
                            subSectionsForSeqNo.Contains(ss.TemplateFieldName,
                                StringComparer.InvariantCultureIgnoreCase));

                        if (borrowerREOInfoSubSection != null)
                        {
                            var borrowerREOInfoSubSectionSeqNo =
                                borrowerREOInfoSubSection.SeqNo.GetValueOrDefault();

                            if (string.Equals(subSectionRequestSubSection.TemplateFieldName, "spouseREOInfo",
                                    StringComparison.OrdinalIgnoreCase) && string.Equals(
                                    borrowerREOInfoSubSection.TemplateFieldName, "borrowerREOInfo",
                                    StringComparison.OrdinalIgnoreCase))
                            {
                                //Note: jump the SeqNo in case of the first SpouseREOInfo
                                borrowerREOInfoSubSectionSeqNo = borrowerREOInfoSubSectionSeqNo + 600;
                            }
                            else if (string.Equals(subSectionRequestSubSection.TemplateFieldName, "coboSpouseREOInfo",
                                         StringComparison.OrdinalIgnoreCase) && string.Equals(
                                         borrowerREOInfoSubSection.TemplateFieldName, "coBorrowerREOInfo",
                                         StringComparison.OrdinalIgnoreCase))
                            {
                                //Note: jump the SeqNo in case of the first CoBoSpouseREOInfo
                                borrowerREOInfoSubSectionSeqNo = borrowerREOInfoSubSectionSeqNo + 600;
                            }

                            subSectionRequestSubSection.SeqNo = borrowerREOInfoSubSectionSeqNo + 1;
                        }
                        else
                        {
                            throw new Exception(
                                $"Mapping not found for {subSectionRequestSubSection.TemplateFieldName}");
                        }
                    }
                }
            }

            var subSectionIds = subSectionRequest.SubSections.Select(q => q.SubSectionId).ToList();

            if (currentSection.SubSections.Any(i => subSectionIds.Contains(i.SubSectionId, StringComparer.OrdinalIgnoreCase)))
            {
                throw new Exception($"Some of subsectionIds already exist {string.Join(",", subSectionIds)} in application {temporaryApplicationNumber}");
            }

            // Set username from request in case of income subsection, to resolve the issue of wrong userName getting updated against the income subsection selected, specially for spouse and co-borrower spouse
            string userNameFromRequest = "";
            var firstSubSection = subSectionRequest.SubSections[0];
             if(firstSubSection.SubSectionUniqueId.ToLower() == "incomesubsection" && firstSubSection.UserName != null && firstSubSection.UserName != ""){
                userNameFromRequest = firstSubSection.UserName;
            }

            var subSections =  subSectionRequest.SubSections;
            for (var i = 0; i < subSections.Count; i++)
            {
                var subsectionTemplateId = subSections[i].TemplateFieldName;
                KeyValuePair<string, SubSection> sectionTemplates;
                
                if(isCalledFromCopy)
                {
                    //Note: When it is copied at that it will have to use from the request object otherwise will use from the template
                    sectionTemplates = new KeyValuePair<string, SubSection>(subsectionTemplateId,subSections[i]);
                }
//                else if(subsectionTemplateId.Contains("copiedRentalIncome")) //eg. copiedRentalIncome1, copiedRentalIncome2 
//                {
//                    subsectionTemplateId = "_rentalIncome";
//                    subSections[i].TemplateFieldName = subsectionTemplateId;
//                    subSections[i].TemplateId = subsectionTemplateId;
//                    sectionTemplates = new KeyValuePair<string, SubSection>(subsectionTemplateId,subSections[i]);
//                }
                else
                {
                    sectionTemplates = currentSection.Templates.FirstOrDefault(x => x.Key == subsectionTemplateId);
                }
                
                if (sectionTemplates.Value != null)
                {
                    if (subSections[i].DeleteTemplates != null)
                    {
                        sectionTemplates.Value.DeleteTemplates = subSections[i].DeleteTemplates;
                    }

                    if(subSections[i].Tag != null && subSections[i].Tag.Any())
                    {
                        sectionTemplates.Value.Tag = subSections[i].Tag;
                    }
                    
                    var subSection = Extensions.Clone<SubSection>(sectionTemplates.Value);
                    subSection.SubSectionId = subSections[i].SubSectionId;
                    subSection.SeqNo = subSections[i].SeqNo;
                    subSection.SubSectionName = subSections[i].SubSectionName;
                    subSection.TemplateId = subSections[i].TemplateId;

                    foreach (var queSection in subSection.QuestionSections)
                    {
                        foreach (var question in queSection.QuestionList)
                        {
                            if (question.ChildQuestions != null)
                            {
                                foreach (var child in question.ChildQuestions)
                                {
                                    child.SubSectionId = subSections[i].SubSectionId;
                                }
                            }

                            if(question.DynamicRequired != null)
                            {
                                 foreach (var child in question.DynamicRequired)
                                {
                                    child.SubSectionId = subSections[i].SubSectionId;
                                }
                            }
                        }
                    }

                    subSections[i] = subSection;
                }
                else
                {
                    throw new Exception($"Template not found for {subsectionTemplateId}");
                }
            }

            //var currentUserName = await GetCurrentUser();
            foreach (ISubSection subSection in subSections)
            {

                if (isCoBorrowerInvitation)
                {
                    //Note: For CoBorrowerInvitation Template we will keep [UserBased] as is, that will be replaced once the co-borrower invitation is sent
                }
                else if (isCoBorrowerSpouseInvitation)
                {
                    //Note: When Inviting CoBorrower Spouse, need to use the username of coborrower
                    subSection.UserName = currentUserName;
                }
                else if(string.IsNullOrWhiteSpace(subSection.UserName))
                {
                    // GetRootSectionSeqNo is getting worng username when seq no generated for income section is same for two users(e.g. coborrower and spouse)
                    var userName = "";
                    if(!string.IsNullOrEmpty(userNameFromRequest)) {
                        userName = userNameFromRequest;
                        subSection.UserName = userName;
                    }
                    else {
                        //This will work for the Income section                
                        // For some scenario we do not want to assign the SeqNo, based on the previous subsection seq no, but for others we want to, so we will use this field.
                        // If SubSectionBased (not 'UserBased') then get the previous seqNo and gets its username.
                        var presentSeqNo = subSection.SeqNo; // From UI
                        var rootSubSection = GetRootSectionSeqNo(currentSection, (int)presentSeqNo);
                        if (rootSubSection != null)
                        {
                            userName = rootSubSection.UserName.ToLower();
                            subSection.UserName = userName;
                        }
                    }

                    //TODO: By Nayan: What this code is doing?
                    if (sectionId ==  Convert.ToInt32(SectionDetails.Income)  && subSection.TemplateId.ToLower().Contains("incomesearch") && currentUserName != userName)
                    {
                        var section = creditSection;

                        if(section != null)
                        {
                            var srcSubSection = section.SubSections.Find(ss => ss.SubSectionId.ToLower() != "ss1");
                            var autoFillSourceQuestions = subSection.QuestionSections.SelectMany(qs => qs.QuestionList).Where(q => q.AutoFillQuestions != null);

                            foreach (var question in autoFillSourceQuestions)
                            {
                                foreach (var autoFillQuestion in question.AutoFillQuestions)
                                {
                                    autoFillQuestion.SubSectionId = srcSubSection.SubSectionId;
                                }
                            }
                        }
                    }

                    // Added: 22/04/2018 - Autofill income section dob and ssn from profile for borrower and spouse
                    if (sectionId ==  Convert.ToInt32(SectionDetails.Income)  && subSection.TemplateId.ToLower().Contains("incomesearch"))
                    {
                        var incomeQuestions = subSection.QuestionSections.SelectMany(qs => qs.QuestionList)
                                .Where(q => q.QuestionType == QuestionType.Date || q.QuestionType == QuestionType.TextSSN).ToList();

                        ISubSection srcPersonalInfoSection;
                        if (currentUserName == userName)
                        {
                            srcPersonalInfoSection = await GetIncomePersonalSection(profileSection, currentUserName);
                        }
                        else
                        {
                            srcPersonalInfoSection = await GetIncomePersonalSection(profileSection, userName);                          
                        }
                        
                        if (srcPersonalInfoSection != null)
                        {
                            var personalInfoQuestions = srcPersonalInfoSection.QuestionSections.SelectMany(qs => qs.QuestionList)
                                    .Where(q => q.QuestionType == QuestionType.Date || q.QuestionType == QuestionType.TextSSN).ToList();

                            foreach (var personalInfoQuestion in personalInfoQuestions)
                            {
                                foreach (var incomeQuestion in incomeQuestions)
                                {
                                    if (personalInfoQuestion.QuestionType == QuestionType.Date && incomeQuestion.QuestionType == QuestionType.Date)
                                    {
                                        incomeQuestion.Answer = personalInfoQuestion.Answer;
                                    }
                                    if (personalInfoQuestion.QuestionType == QuestionType.TextSSN && incomeQuestion.QuestionType == QuestionType.TextSSN)
                                    {
                                        incomeQuestion.Answer = personalInfoQuestion.Answer;
                                    }
                                }
                            }
                        }
                    }
                    // End Added: 22/04/2018 - Autofill income section dob and ssn from profile                  

                    foreach (var queSection in subSection.QuestionSections)
                    {
                        foreach (var question in queSection.QuestionList)
                        {
                            if (question.ChildQuestions != null)
                            {
                                foreach (var child in question.ChildQuestions)
                                {
                                    child.SubSectionId = subSection.SubSectionId;
                                }
                            }
                            
                            if(question.DynamicRequired != null)
                            {
                                 foreach (var child in question.DynamicRequired)
                                {
                                    child.SubSectionId = subSection.SubSectionId;
                                }
                            }
                        }
                    }
                }
                else if (SpecialUserNamePlaceHolders.All(s=>subSection.UserName!=s))
                {
                    //Note: When user name is coming from request and it does not contain any place holder                    
                    //Note: This is used when we are copying REO to Income section
                }
                else if(SpecialUserNamePlaceHolders.Any(s=>subSection.UserName!=s))
                {
                    //Note: If the username is place holders                    
                    if (string.Equals(userStatus.ApplicantType, ApplicantType.Spouse.ToString(),
                            StringComparison.OrdinalIgnoreCase) ||
                        string.Equals(userStatus.ApplicantType,
                            ApplicantType.CoBorrowerSpouse.ToString(), StringComparison.OrdinalIgnoreCase))
                    {                        
                        if (string.Equals(subSection.UserName, "[SpouseUserBased]", StringComparison.OrdinalIgnoreCase))
                        {
                            //Spouse is adding section and username is spouse user based
                            subSection.UserName = userStatus.UserName;
                        }
                        else if(!string.IsNullOrWhiteSpace(userStatus.SpouseUserName))
                        {
                            //Spouse is adding section for borrower then username is spouse 
                            subSection.UserName = userStatus.SpouseUserName;
                        }
                    }
                    else
                    {
                        if (string.Equals(subSection.UserName, "[UserBased]", StringComparison.OrdinalIgnoreCase))
                        {
                            //Borrower is adding section for himself
                            subSection.UserName = userStatus.UserName;
                        }
                        else if(!string.IsNullOrWhiteSpace(userStatus.SpouseUserName))
                        {
                            //Borrower is adding section for spouse and spouse invitation already sent.
                            //if invitation is not sent then it will remain as is and replace after invitation
                            subSection.UserName = userStatus.SpouseUserName;
                        }
                    }
                }
            }

            var tasks=new List<Task>();
            
            tasks.Add(QuestionSubSectionsRepository.AddSubSections(subSectionRequest.TemporaryApplicationNumber, sectionId, new List<ISubSection>(subSections)));            
                
            foreach (ISubSection subSection in subSections)
            {
                tasks.Add(EventHubClient.Publish(new QuestionSubSectionAdded(subSectionRequest.TemporaryApplicationNumber, sectionId, subSection.SubSectionId, subSection.SubSectionName)));
            }

            await Task.WhenAll(tasks);

            return true;
        }

/*
        private void AddSectionForCoborrowerREOSection(SubSection coBorrowerREOInfoSubSection)
        {
            IQuestion addAnotherQuestion = null;
            var deleteSectionString = "deleteSection";
            var calculationSectionString = "calculation";
            addAnotherQuestion = coBorrowerREOInfoSubSection.QuestionSections.Where(x=>x.QuestionSectionName.ToLower() == deleteSectionString.ToLower())
                                                            .SelectMany(y=>y.QuestionList).FirstOrDefault(z=>z.QuestionId == "80");
            var deleteSectionRemove = coBorrowerREOInfoSubSection.QuestionSections.FirstOrDefault(x=>x.QuestionSectionName.ToLower() == deleteSectionString.ToLower());
            if(deleteSectionRemove != null)
            {
                coBorrowerREOInfoSubSection.QuestionSections.Remove(deleteSectionRemove);
            }
            
            var calculationSubsection = coBorrowerREOInfoSubSection.QuestionSections.FirstOrDefault(x=>x.QuestionSectionName.ToLower() == calculationSectionString.ToLower());
            if(addAnotherQuestion != null){  
                calculationSubsection.QuestionList.Add(addAnotherQuestion);
            }
        }
*/

        /// <summary>
        /// This function removes collection of Sub sections from the JSON based on the temporay application number, Section Id and Sub Section Ids. 
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionIds"></param>
        /// <returns></returns>
        public async Task<bool> RemoveSubSections(string temporaryApplicationNumber, int sectionId, SubSectionIdList subSectionIds)
        {
            try
            {
                if (sectionId == Convert.ToInt32(SectionDetails.Profile) && subSectionIds.SubSectionIds.Count() >= 1)
                {
                    IQuestionnaire questionnaire = new Questionnaire();
                    questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                    var subSection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == sectionId)
                        .SelectMany(s => s.SubSections)
                        .Where(ss => ss.SubSectionId == subSectionIds.SubSectionIds[0]).FirstOrDefault();

                    if(subSection != null)
                    {

                        if (subSection.TemplateId == "coBorrowerMaritalStatusInfo" || subSection.TemplateId == "coBorrowerInfo" ||
                            subSection.TemplateId == "coBorrowerCommunicationInfo")
                        {
                            var coBorrowerREOInfoSubSectionId = subSection.SubSectionId + "-3";
                            subSectionIds.SubSectionIds.Add(coBorrowerREOInfoSubSectionId);
                        }
                    }
                }

                var status = false;
                await QuestionSubSectionsRepository.RemoveSubSections(temporaryApplicationNumber, sectionId, subSectionIds.SubSectionIds);
                return status;
            }
            catch (SubSectionDoesNotExistsException exception)
            {
                throw new SubSectionDoesNotExistsException(exception.Message);
            }
            catch (SectionDoesNotExistsException exception)
            {
                throw new SectionDoesNotExistsException(exception.Message);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }

        /// <summary>
        /// This function deletes the Sub section from the JSON based on the temporay application number, Section Id and Sub Section Id. It removes one sub section at a time 
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSubSection(string temporaryApplicationNumber, int sectionId, string subSectionId, IQuestionnaire questionnaire = null)
        {
            var Sections = new List<ISection>();
            ISection sectionFound = new Section();
            ISection sectiontoUpdate = null;

            var status = false;

            //This function assigns Application Number to the Questionnaire.
            //Get Questionnaire for the applicant.
            // IQuestionnaire questionnaire = new Questionnaire();
            if (questionnaire == null)
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire != null)
            {
                Sections = questionnaire.ApplicationForm.Sections;
                sectiontoUpdate = Sections.FirstOrDefault(Section => Section.SectionId == sectionId);
                if (sectiontoUpdate == null)
                {
                    // throw new SectionDoesNotExistsException($"Section '{sectionId}' does not exist.");
                    status = false;
                }
                else
                {
                    //Get the section to add the new subsection.
                    //ISection section = questionnaire.ApplicationForm.Sections.FirstOrDefault(Section => Section.SectionId == ISubSection subSection = null;
                    var subSectionToRemove = sectiontoUpdate.SubSections.FirstOrDefault(Subsection => Subsection.SubSectionId == subSectionId);

                    if (subSectionToRemove == null)
                    {
                        // throw new SubSectionDoesNotExistsException($"Subsection having the subSectionId '{subSectionId}' does not exist.");
                        status = false;
                    }
                    else
                    {
                        sectiontoUpdate.SubSections.Remove(subSectionToRemove);                        
                        //status = await QuestionnaireRepository.DeleteSubSection(temporaryApplicationNumber, Sections);

                         var subSectionIds = new List<string>{
                            subSectionToRemove.SubSectionId
                        };
                        
                        await QuestionSubSectionsRepository.RemoveSubSections(temporaryApplicationNumber,sectionId,subSectionIds);
                        status = true;
                        await EventHubClient.Publish(new QuestionSubSectionRemoved(temporaryApplicationNumber, sectionId, subSectionId, subSectionToRemove.SubSectionName));

                    }
                }
            }

            return status;
        }

        #endregion
    }
}
