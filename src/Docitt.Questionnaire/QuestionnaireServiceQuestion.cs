﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Security;
using Jil;
using Docitt.Questionnaire.Events;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Docitt.Questionnaire.Abstractions;
#if DOTNET2

#else
using Microsoft.AspNet.Http;
#endif

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        /// <summary>
        /// This fucntion provides the question JSON, based on section id, sub section id, question id
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<IQuestion> GetQuestion(string temporaryApplicationNumber, int sectionId, string subSectionId, string questionId, IQuestionnaire questionnaire=null)
        {
            if (questionnaire == null)
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire == null)
                throw new QuestionnaireNotFoundException($"Questionnaire having the temporary application number {temporaryApplicationNumber} does not exist");
            
            var question = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == questionId);

            if (question != null)
                return await Task.Run(() => question);
            else
                throw new QuestionNotFoundException($"Question having the Question Id {questionId} for the Temporary Application Number '{temporaryApplicationNumber}, Section '{sectionId}', Subsection '{subSectionId}' cannot be found.");
        }

        /// <summary>
        /// This function provides the Question JSON of the Loan purpose question
        /// It consists of the Answer whether user selected 'Purchase' or 'Refinance'
        /// This helps in deciding whether we need to show the "Purchase" and "Refinance" screen
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        public async Task<IQuestion> GetLoanPurposeQuestion(string temporaryApplicationNumber)
        {
            return await Task.Run(() => GetLoanPurposeQuestion(temporaryApplicationNumber, null, null));
        }

//        /// <summary>
//        /// This is to be used when creating a form template by the admin user.
//        /// </summary>
//        /// <param name="question"></param>
//        /// <param name="formId"></param>
//        /// <param name="sectionId"></param>
//        /// <returns></returns>
//        public async Task<bool> AddQuestion(IQuestion question, string formId, string sectionId)
//        {
//            //This function will be used by Tenant Admin
//            var status = await FormRepository.AddQuestion(question, formId, sectionId);
//            if (status == true)
//            {
//                return await Task.Run(() => true);
//            }
//            else
//            {
//                throw new ArgumentException($"Question could not be added to the Form '{formId}' and section '{sectionId}'.");
//            }
//        }

        /// <summary>
        /// This function is used to get the bank info of the logged in user. This functionality is now not is use.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, List<IBankInfo>>> GetBankInfo(string temporaryApplicationNumber)
        {
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            var allBankInfo = new Dictionary<string, List<IBankInfo>>();


            if (questionnaire == null)
            {
                throw new QuestionnaireNotFoundException($"Questionnaire having TemporaryApplicatioNumber {temporaryApplicationNumber}.");
            }

            //The below values should come from configuration. Will be done later.
            var sectionId =  Convert.ToInt32(SectionDetails.Assets) ;
            var subSectionId = "SS1";
            var questionSectionName = "BankInfo";

            var subSection = questionnaire.ApplicationForm.Sections.Find(s => s.SectionId == sectionId).SubSections.Find(ss => ss.SubSectionId == subSectionId);

            if (subSection != null)
            {
                var questions = new List<IQuestion>();
                var questionSection = subSection.QuestionSections.FirstOrDefault(qs => qs.QuestionSectionName == questionSectionName);

                if (questionSection == null)
                {
                    questionSection = new QuestionSection();
                    questionSection.QuestionSectionName = questionSectionName;
                }

                if (questionSection.QuestionList == null)
                {
                    questionSection.QuestionList = questions;
                }
                else
                {
                    questions = questionSection.QuestionList;
                }


                foreach (var question in questions)
                {
                    var bankInfo = JsonConvert.DeserializeObject<BankInfo>(question.Answer);

                    var bankNameForGrouping = question.QuestionText;

                    if (allBankInfo.ContainsKey(bankNameForGrouping))
                    {

                    }
                    else
                    {
                        var bankInfoList = new List<IBankInfo>();

                        allBankInfo.Add(bankNameForGrouping, bankInfoList);
                    }
                    allBankInfo[bankNameForGrouping].Add(bankInfo);
                }
            }
            else
            {
                throw new SubSectionDoesNotExistsException($"Subsection {subSectionId} or Section having SectionId {sectionId} does not exists.");
            }

            return allBankInfo;
        }

        /// <summary>
        /// This function will provide the information about the Bank Details to be shown in yellow section.
        /// It internally calls the DCC service.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>        
        public async Task<IBankAlert> GetBankAlertInfo(string temporaryApplicationNumber, string customerId)
        {
            var loanInformation = await GetInformationFromQuestionnaire(temporaryApplicationNumber);

            // Total Current Bank Balance from DCC
            double totalBankBalance = 0;
            double verifiedBankBalance = 0;
            double disclosedBankBalance = 0;
            JObject jResult = null;

            if (!string.IsNullOrWhiteSpace(customerId)) 
                jResult = await Task.Run(() => DecisionEngine.Execute<dynamic, JObject>("getbankinfo", new {customerId, entityId = temporaryApplicationNumber }));

            if(jResult == null)
                jResult = await Task.Run(() => DecisionEngine.Execute<dynamic, JObject>("getbankinfo", new { customerId }));

            if (jResult != null)
            {
                dynamic bankInfo = JsonConvert.DeserializeObject(jResult.ToString());
                string status = bankInfo.status != null ? Convert.ToString(bankInfo.status) : string.Empty;

                if (status != "success")
                    throw new Exception($"Error occured in getting information from DCC : {jResult.GetValue("error").ToString()}");

                if (bankInfo.data != null && bankInfo.data.result != null)
                {
                    foreach (var institution in bankInfo.data.result)
                    {
                        foreach (var account in institution.Accounts)
                        {
                            var isManualUpload = Convert.ToBoolean(account.IsManualUpload);

                            // we are getting two balances Current Balance & Available Balance in account details
                            // here for calculate total bank balance, we have taken current balance if Available Balance is null
                            if (!isManualUpload)
                            {
                                if(account.Balances != null)
                                {
                                    verifiedBankBalance += account.Balances.Available != null ? Convert.ToDouble(account.Balances.Available) : Convert.ToDouble(account.Balances.Current);
                                }
                            }
                            else
                            {
                                disclosedBankBalance += account.Balances.Available != null ? Convert.ToDouble(account.Balances.Available) : Convert.ToDouble(account.Balances.Current);
                            }
                            if(account.Balances != null)
                            {
                                totalBankBalance += account.Balances.Available != null ? Convert.ToDouble(account.Balances.Available) : Convert.ToDouble(account.Balances.Current);
                            }
                        }
                    }
                }
            }

            IBankAlert bankInformation = new BankAlert();

            if (loanInformation != null)
            {
                bankInformation.LoanAmount = string.IsNullOrEmpty(loanInformation.LoanAmount) ? 0 : Convert.ToDouble(loanInformation.LoanAmount);
                bankInformation.PurchasePrice = loanInformation.PurchasePrice;
                bankInformation.DownPayment = string.IsNullOrEmpty(loanInformation.DownPayment) ? 0 : Convert.ToDouble(loanInformation.DownPayment);
            }

            bankInformation.TotalBankBalance = totalBankBalance;
            bankInformation.VerifiedBankBalance = verifiedBankBalance;
            bankInformation.DisclosedBankBalance = disclosedBankBalance;
            return bankInformation;
        }

        /// <summary>
        /// This function adds bank info the Questionnaire. This functionality is now removed.
        /// </summary>
        /// <param name="bankInfo"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        public async Task<bool> AddBankInfo(BankInfo bankInfo, string temporaryApplicationNumber)
        {
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire == null)
            {
                throw new QuestionnaireNotFoundException($"Questionnaire having TemporaryApplicatioNumber {temporaryApplicationNumber}.");
            }

            var sectionId =  Convert.ToInt32(SectionDetails.Assets) ;
            var subSectionId = "SS1";
            var questionSectionName = "BankInfo";

            var subSection = questionnaire.ApplicationForm.Sections.Find(s => s.SectionId == sectionId).SubSections.Find(ss => ss.SubSectionId == subSectionId);
            bool status;

            IQuestionSection questionSection = new QuestionSection();
            if (subSection != null)
            {
                IQuestion question = new Question();

                questionSection =
                    subSection.QuestionSections.FirstOrDefault(qs => qs.QuestionSectionName == questionSectionName);

                if (questionSection == null)
                {
                    questionSection = new QuestionSection {QuestionSectionName = questionSectionName};


                    subSection.QuestionSections.Add(questionSection);
                }

                questionSection =
                    subSection.QuestionSections.FirstOrDefault(qs => qs.QuestionSectionName == questionSectionName);

                bankInfo.BankInfoId = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +
                                      DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() +
                                      DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString();

                question.Answer = JSON.Serialize(bankInfo);

                question.QuestionText = bankInfo.BankName.ToLower();

                if (questionSection.QuestionList == null)
                {
                    questionSection.QuestionList = new List<IQuestion>();
                }

                questionSection.QuestionList.Add(question);

                await QuestionSectionRepository.UpdateSections(temporaryApplicationNumber,
                    questionnaire.ApplicationForm.Sections); //AssignUserNameToSubSection

                // Update section status
                IQuestionnaire _questionnaire = new Questionnaire();
                IUserAccessedStatus userStatus = new UserAccessedStatus();

                _questionnaire.TemporaryApplicationNumber = temporaryApplicationNumber;
                _questionnaire.HighestSectionReached = sectionId;

                //Set the status
                userStatus.LastAccessedSection = sectionId;
                userStatus.LastAccessedSubSection = subSection.SubSectionName;
                userStatus.LastAccessedSubSectionId = subSectionId;
                userStatus.HighestSectionReached = sectionId;

                userStatus.UserName = await GetCurrentUser();

                await QuestionnaireRepository.UpdateQuestionnaireStatus(_questionnaire.TemporaryApplicationNumber,
                    _questionnaire.HighestSectionReached, _questionnaire.HighestSubSectionReached, userStatus);
                
                status = true;
            }
            else
            {

                throw new SubSectionDoesNotExistsException($"Subsection {subSectionId} or Section having SectionId {sectionId} does not exists.");

            }

            return status;

        }
        public async Task<bool> UpdateQuestion(IAnswerUpdateRequest questionRequest) //, string temporaryapplicationNumber)
        {
            IQuestion questionResponse = new Question();

            try
            {
                var userName = await GetCurrentUser();

                questionRequest.UserName = userName;

                if (questionRequest.TemporaryApplicationNumber == null || questionRequest.TemporaryApplicationNumber == "")
                {
                    //If no TemporaryApplicationNumber was sent in the request, then get the TemporaryApplicationNumber of the logged in user
                    questionRequest.TemporaryApplicationNumber = await GetTemporaryApplicationNumber();

                    if (questionRequest.TemporaryApplicationNumber == null)
                    {
                        throw new QuestionnaireNotFoundException("No Questionnaire exists for the user.");
                    }
                }

                var result = await UpdateQuestionSectionAndStatus(questionRequest);

                if (result == true)
                {
                    return await Task.Run(() => true);
                }
                else
                {
                    throw new ArgumentException($"Answer for the Question '{questionRequest.QuestionId}' could not be updated");
                }
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                throw new SubSectionDoesNotExistsException(ex.Message);
            }
            catch (AnswerNotProvidedException ex)
            {
                throw new AnswerNotProvidedException(ex.Message);
            }
        }

        /// <summary>
        /// This function updates the purpose of loan question
        /// A seperate end point is created to update this question, because this question is not shown like the other Questionnaire questions are shown
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="questionRequest"></param>
        /// <returns></returns>
        public async Task<bool> UpdatePurposeOfLoanQuestion(string temporaryApplicationNumber, AnswerUpdateRequest questionRequest)
        {
            Logger.Debug($"Updating purposeofloan question for {temporaryApplicationNumber}");
            var appFieldMap = QuestionnaireConfigurations.QuestionnaireLoanPurpose;
            var profileSectionId = Convert.ToInt32(SectionDetails.Profile);

            var status = false;
            if (questionRequest != null)
            {            
                questionRequest.TemporaryApplicationNumber = temporaryApplicationNumber;

                if (appFieldMap.Map != null)
                {
                    questionRequest.SectionId = appFieldMap.Map.SectionId;
                    questionRequest.SubSectionId = appFieldMap.Map.SubSectionId;
                    questionRequest.QuestionId = appFieldMap.Map.QuestionId;
                    Logger.Debug($"Updating LoanPurpose as {questionRequest.Answer} for {temporaryApplicationNumber} with values ({questionRequest.SectionId}, {questionRequest.SubSectionId}, {questionRequest.QuestionId})");
                    status = await UpdateQuestion(questionRequest);

                    var affinityPartnerAdmin = await AssignmentService.GetByRole("application", temporaryApplicationNumber, Roles.AffinityPartnerAdmin.GetDescription());
                    if (affinityPartnerAdmin != null || string.Equals(questionRequest.Answer, loanPurpose.Refinance.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        var realEstateId = new List<string>
                        {
                            ConstantUtils.RealEstateSectionId
                        };

                        await QuestionSubSectionsRepository.RemoveSubSections(temporaryApplicationNumber, profileSectionId, realEstateId);
                    }

                    await EventHubClient.Publish(new QuestionnairePurposeSelected(temporaryApplicationNumber,questionRequest.Answer, status));
                }
            }
            else
            {
                 Logger.Error($"AnswerUpdateRequest received as 'null' for {temporaryApplicationNumber}");
            }

            Logger.Debug($"return status as {status} for {temporaryApplicationNumber}");
            return status;
        }

        public async Task<bool> IsEmailAlreadyExists(string temporaryApplicationNumber, string emailId)
        {
            if(!string.IsNullOrWhiteSpace(temporaryApplicationNumber) &&  !string.IsNullOrWhiteSpace(emailId))
            {
                 var borrowersInfo = new List<IBorrowerCoBorrowerInfo>();
                borrowersInfo = await GetBorrowersAndCoBorrowers(temporaryApplicationNumber, getCoborrowerSpouses:true);
                foreach (var borrower in borrowersInfo)
                {
                    if (borrower.Email == emailId)
                    {
                       Logger.Debug($"Applicant having the email address '{emailId}' already exists. Please update the Email address.");
                       return true;
                    }
                }
            }

            return false;
        }

        private async Task ValidateApplicationUser(string applicationNumber)
        {
            var userName = await GetCurrentUser();

            FaultRetry.RunWithAlwaysRetry((() =>
            {
                var questionnaire = QuestionnaireRepository.GetQuestionnaireWithoutDetail(applicationNumber).Result;
                var userStatus = questionnaire.UserState.FirstOrDefault(a => a.UserName == userName);
                if (userStatus == null){
                    Logger.Warn($" User {userName} does not belong to {applicationNumber}");
                    throw new InvalidUserException($"Invalid user {userName} for application number {applicationNumber}");
                }   
                if (userStatus.Status != FormStatus.Open)
                {
                    Logger.Warn($"{applicationNumber} User {userName} status is not open. It's {userStatus.Status}");
                    throw new InvalidUserException(
                        $"Application {applicationNumber} is already submitted for user {userName}");
                }
            }),3,1);
        }

        public async Task<bool> UpdateQuestionAndStatus(IAnswerUpdateRequest questionRequest)
        {
            IQuestion questionToUpdate=null;
            ISection questionnaireSectionToProcess = null;

            if (questionRequest.QuestionId.IndexOf("_", StringComparison.Ordinal) > 0)
            {
                questionRequest.QuestionId = questionRequest.QuestionId.Split('_')[1];
            }
            var childSections = new Dictionary<int, ISection>();

            await ValidateApplicationUser(questionRequest.TemporaryApplicationNumber);
            
            FaultRetry.RunWithAlwaysRetry((() =>
            {
                questionnaireSectionToProcess = QuestionnaireRepository
                    .GetSectionById(questionRequest.TemporaryApplicationNumber, questionRequest.SectionId).Result;

                questionToUpdate = questionnaireSectionToProcess?.SubSections
                    .FirstOrDefault(ss => ss.SubSectionId == questionRequest.SubSectionId)?
                    .QuestionSections.FirstOrDefault(qs => qs.SeqNo == questionRequest.QuestionSectionSeqNo)?
                    .QuestionList.FirstOrDefault(q => q.QuestionId == questionRequest.QuestionId);

                if (questionToUpdate == null)
                {
                    throw new QuestionDoesNotExistsException(
                        $"The Question having questionId  '{questionRequest.QuestionId}' does not exist in QuestionSection for Sequence Number '{questionRequest.QuestionSectionSeqNo}' subsection {questionRequest.SubSectionId} sectionId {questionRequest.SectionId} .");
                }
                else
                {
                    childSections.Add(questionRequest.SectionId, questionnaireSectionToProcess);
                }

            }), maxRetry: 5, sleepSeconds: 1);
            
            var isAnswerValid = ValidateAnswer(questionToUpdate, questionRequest.Answer);

            if (isAnswerValid == false)
            {
                throw new InvalidAnswerException($"The answer '{questionRequest.Answer}' is invalid for the question '{questionToUpdate.QuestionText}'.");  
            }

            if (questionToUpdate.IsSensitive == true)
            {
                //if sensitive then encrypt the data
                questionRequest.Answer = EncryptionService.Encrypt<string>(questionRequest.Answer);
            }

             //////////////////////////////////////////////////////////////////
            if (questionToUpdate.QuestionType == QuestionType.SignUpEmail || questionToUpdate.QuestionType == QuestionType.SignUpEmailEdit)
            {
                //If the email field is a signup email then verify that the email address is unique for this application. 
                var borrowersInfo = new List<IBorrowerCoBorrowerInfo>();
                borrowersInfo = await GetBorrowersAndCoBorrowers(questionRequest.TemporaryApplicationNumber);
                //List<string> userList = new List<string>();

                //if ( borrowersInfo.Any(binfo => binfo.Email == questionRequest.Answer && binfo.SubSectionId != questionRequest.SubSectionId))
                if (borrowersInfo.Any(binfo => binfo.UserName == questionRequest.Answer && binfo.SubSectionId != questionRequest.SubSectionId))
                {
                    throw new UserEmailAlreadyExistsException($"Applicant having the email address '{questionRequest.Answer}' already exists. This email address is used as Sign Up email address. Please enter a different the email address for sign up.");
                }

            }

            var oldAnswer = questionToUpdate.Answer; 
            questionToUpdate = await QuestionnaireRepository.UpdateQuestion(questionRequest, questionToUpdate);

            if (!questionToUpdate.IsSensitive)
            {
                // If information is not sensitive then log the information.

                #region "Publish Question Answer updated event"
                var questionAnswerUpdatedEvent = new QuestionAnswerUpdated()
                {
                    EntityId = questionRequest.TemporaryApplicationNumber,
                    SectionId = questionRequest.SectionId,
                    SubSectionId = questionRequest.SubSectionId,
                    QuestionId = questionToUpdate.QuestionId,
                    QuestionFieldName = questionToUpdate.QuestionFieldName,
                    QuestionText = !string.IsNullOrEmpty(questionToUpdate.QuestionText.Trim()) ? questionToUpdate.QuestionText.Trim() : questionToUpdate.QuestionFieldName,
                    OldAnswer = oldAnswer,
                    NewAnswer = questionRequest.Answer
                };

                await EventHubClient.Publish(questionAnswerUpdatedEvent);
                #endregion
            }

            // Moved child answer reset login to ResetChildQuestionAnswer method
            await ResetChildQuestionAnswer(questionRequest, questionToUpdate, questionnaireSectionToProcess, childSections);

            ///////////////////////////////////////////////////////////////////
            if (questionToUpdate != null && !string.IsNullOrEmpty(questionToUpdate.QuestionId))
            {
                // Update autofill question's answers
                if (questionToUpdate.AutoFillQuestions != null)
                {
                    var autoQuestionCount = questionToUpdate.AutoFillQuestions.Count();
                    for (var questionIndex = 0; questionIndex < autoQuestionCount; questionIndex++)
                    {
                        var autoFillSectionId = Convert.ToInt32(questionToUpdate.AutoFillQuestions[questionIndex].SectionId); //questionSectionId = Convert.ToInt32(questionToUpdate.AutoFillQuestions[questionIndex].SectionId);
                        var autoFillQuestionSubSectionId = questionToUpdate.AutoFillQuestions[questionIndex].SubSectionId;
                        if (autoFillQuestionSubSectionId == "[username]")
                        { // If sub sectionId is not set then no need to update autofill question's answer.
                            continue;
                        }
                        var autoFillQuestionId = questionToUpdate.AutoFillQuestions[questionIndex].QuestionId;
                        var autoFillQuestionSectionSeqNo = questionToUpdate.AutoFillQuestions[questionIndex].QuestionSectionSeqNo;

                        IAnswerUpdateRequest autoFillQuestionRequest = new AnswerUpdateRequest();
                        autoFillQuestionRequest.QuestionId = autoFillQuestionId;
                        autoFillQuestionRequest.SectionId = autoFillSectionId;
                        autoFillQuestionRequest.SubSectionId = autoFillQuestionSubSectionId;
                        autoFillQuestionRequest.QuestionSectionSeqNo = autoFillQuestionSectionSeqNo;
                        autoFillQuestionRequest.TemporaryApplicationNumber = questionRequest.TemporaryApplicationNumber;
                        autoFillQuestionRequest.Answer = questionRequest.Answer; //No need to encrypt, as the value is already encrypted.

                        if (autoFillQuestionRequest.QuestionSectionSeqNo > 0)
                        {
                            var result = await QuestionnaireRepository.UpdateQuestion(autoFillQuestionRequest, null);
                        }
                    }
                }

                // Update section status
                IUserAccessedStatus userStatus = new UserAccessedStatus();

                //Set the status
                userStatus.LastAccessedSection = questionRequest.SectionId;
                userStatus.LastAccessedSubSection = questionRequest.SubSectionName;
                userStatus.LastAccessedSubSectionId = questionRequest.SubSectionId;

                if (userStatus.HighestSectionReached <= questionRequest.SectionId)
                {
                    userStatus.HighestSectionReached = questionRequest.SectionId;
                    userStatus.HighestSubSectionReached = questionRequest.SubSectionId;
                }

                userStatus.UserName = await GetCurrentUser();

                await QuestionnaireRepository.UpdateQuestionnaireStatus(questionRequest.TemporaryApplicationNumber,
                    questionRequest.SectionId, questionRequest.SubSectionId, userStatus);

            }

            var autoSyncQuestionAnswerConfig = QuestionnaireConfigurations.AutoSyncQuestionAnswer;
            if (questionToUpdate != null && autoSyncQuestionAnswerConfig != null)
            {
                var subSectionTemplateFieldName = 
                    questionnaireSectionToProcess?.SubSections
                        .FirstOrDefault(ss => ss.SubSectionId == questionRequest.SubSectionId)?
                        .TemplateFieldName;

                var autoSyncQuestion = autoSyncQuestionAnswerConfig
                           .Map
                           .FirstOrDefault(x => 
                            x.CopyFrom.QuestionFieldName == questionToUpdate.QuestionFieldName &&
                            x.CopyFrom.SectionId == questionRequest.SectionId &&
                            (
                                (!string.IsNullOrEmpty(x.CopyFrom.SubSectionId) && x.CopyFrom.SubSectionId == questionRequest.SubSectionId) || 
                                (!string.IsNullOrEmpty(x.CopyFrom.SubSectionTemplateFieldName) && x.CopyFrom.SubSectionTemplateFieldName == subSectionTemplateFieldName)
                            ) &&
                            x.CopyFrom.QuestionSectionSeqNo == questionRequest.QuestionSectionSeqNo &&
                            x.CopyFrom.QuestionId == questionToUpdate.QuestionId
                           );

                if(autoSyncQuestion != null)
                {
                    string copyToSubSectionId = autoSyncQuestion.CopyTo.SubSectionId;

                    if(string.IsNullOrEmpty(copyToSubSectionId))
                    {
                        var currentUser = await GetCurrentUser();
                        var questionnaireWithoutDetail = await QuestionnaireRepository
                            .GetQuestionnaireWithoutDetail(questionRequest.TemporaryApplicationNumber);
                        var currentUserUserState = questionnaireWithoutDetail
                                .UserState
                                .FirstOrDefault(x => x.UserName == currentUser);
                        

                        var subSection = 
                            questionnaireSectionToProcess?.SubSections
                                .FirstOrDefault(ss => 
                                ss.TemplateFieldName == autoSyncQuestion.CopyTo.SubSectionTemplateFieldName && 
                                ss.UserName == currentUserUserState.UserName || ss.UserName == currentUserUserState.SpouseUserName);
                        
                        if(subSection != null)
                        {
                            copyToSubSectionId = subSection.SubSectionId;
                        }
                    }

                    Logger.Debug("Inside AutoSyncQuestionAnswer to sync answers");
                    IAnswerUpdateRequest autoFillQuestionRequest = new AnswerUpdateRequest();
                    autoFillQuestionRequest.QuestionId = autoSyncQuestion.CopyTo.QuestionId;
                    autoFillQuestionRequest.SectionId = autoSyncQuestion.CopyTo.SectionId;
                    autoFillQuestionRequest.SubSectionId = copyToSubSectionId;
                    autoFillQuestionRequest.QuestionSectionSeqNo = autoSyncQuestion.CopyTo.QuestionSectionSeqNo;
                    autoFillQuestionRequest.TemporaryApplicationNumber = questionRequest.TemporaryApplicationNumber;
                    autoFillQuestionRequest.Answer = questionRequest.Answer;
                    if (!string.IsNullOrEmpty(autoFillQuestionRequest.SubSectionId) && autoFillQuestionRequest.QuestionSectionSeqNo > 0)
                    {
                        var result = await QuestionnaireRepository.UpdateQuestion(autoFillQuestionRequest, null);
                    }
                }
            }
            
            return true;
        }

        /// <summary>
        /// This function removes the bank info of the user. This functionality is now not is use.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="bankInfoId"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, List<IBankInfo>>> DeleteBankInfo(string temporaryApplicationNumber, string bankInfoId)
        {

            try
            {
                var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                var bankSummary = new List<Dictionary<string, List<IBankInfo>>>();

                if (questionnaire == null)
                {
                    throw new QuestionnaireNotFoundException($"Questionnaire having TemporaryApplicatioNumber {temporaryApplicationNumber}.");
                }

                //The below values should come from configuration. Will be done later.
                var sectionId =  Convert.ToInt32(SectionDetails.Assets) ;
                var subSectionId = "SS1";
                var questionSectionName = "BankInfo";

                var subSection = questionnaire.ApplicationForm.Sections.Find(s => s.SectionId == sectionId).SubSections.Find(ss => ss.SubSectionId == subSectionId);

                if (subSection != null)
                {
                    var questions = subSection.QuestionSections.FirstOrDefault(qs => qs.QuestionSectionName == questionSectionName)?.QuestionList;

                    //string BankNameForGrouping = "";

                    var bankInfoList = new List<IBankInfo>();
                    IQuestion questionToDelete = new Question();

                    for (var i = 0; i < questions.Count; i++)
                    {
                        var bankInfo = JsonConvert.DeserializeObject<BankInfo>(questions[i].Answer);

                        if (bankInfo.BankInfoId == bankInfoId)
                        {
                            questionToDelete = questions[i];
                            break;
                        }
                    }

                    if (questionToDelete != null)
                    {
                        questions.Remove(questionToDelete);
                        QuestionnaireRepository.Update(questionnaire);
                    }

                    return await GetBankInfo(temporaryApplicationNumber);
                }
                else
                {
                    throw new SubSectionDoesNotExistsException($"Subsection {subSectionId} or Section having SectionId {sectionId} does not exists.");
                }
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                throw new SubSectionDoesNotExistsException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
