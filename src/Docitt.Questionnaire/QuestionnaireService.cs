﻿using Docitt.Applicant;
using Docitt.Application;
using LendFoundry.Foundation.Date;
using LendFoundry.NumberGenerator;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using System.Text.RegularExpressions;
using Docitt.Questionnaire.Events;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
using Docitt.UserProfile;
using LendFoundry.EventHub;
using LendFoundry.Tenant.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Identity.Client;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using Docitt.Questionnaire.Abstractions;
using Docitt.AssignmentEngine;
using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService : IQuestionnaireService
    {
        #region "Constants"

        const string USERBASED = "[UserBased]";
        const string SPOUSEUSERBASED = "[SpouseUserBased]";
        const string COBORROWERSPOUSEUSERBASED = "[CoboSpouseUserBased]";
        private const string REFINANCE = "refinance";
        private const string PURCHASE = "purchase";
        private const string CURRENTEMPLOYMENT = "currentEmployment";
        private const string EMPLOYMENT = "employment";
        private const string BUSINESS = "business";
        private const string BUSINESSANOTHER = "businessAnother";
        
        #endregion

        #region "Constructor"

        public QuestionnaireService
        (
            IQuestionnaireRepository questionnaireRepository,
            IGeneratorService temporaryApplicationNumberGenerator, IEventHubClient eventHubClient,
            QuestionnaireConfiguration questionnaireConfiguration, ITenantTime tenantTime, IFormRepository formRepository,
            ITokenReader tokenReader, ITokenHandler tokenParser, IApplicationService applicationService, IApplicantService applicantService,
            ITenantService tenantService, IDecisionEngineClientFactory decisionEngineFactory, IStatusManagementServiceFactory statusManagementFactory,
            IHttpContextAccessor httpAccessor, IDecisionEngineService decisionEngine, IIdentityService identityService,
            IUserProfileService userProfileService, IEncryptionService encryptionService, ILogger logger,
            IQuestionnaireAnswerRepository questionnaireAnswerRepository,
            IInviteValidationService inviteValidationService,
            IQuestionSectionsRepository questionSectionsRepository,
            IQuestionSubSectionRepository questionSubSectionsRepository,IQuestionSectionRepository questionSectionRepository,
            IAssignmentService assignmentService, IInviteService inviteService
        )
        {
            QuestionnaireRepository = questionnaireRepository;
            TemporaryApplicationNumberGenerator = temporaryApplicationNumberGenerator;
            EventHubClient = eventHubClient;
            QuestionnaireConfigurations = questionnaireConfiguration;
            TenantTime = tenantTime;
            FormRepository = formRepository;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            ApplicationService = applicationService;
            ApplicantService = applicantService;
            TenantService = tenantService;
            DecisionEngineFactory = decisionEngineFactory;
            StatusManagementFactory = statusManagementFactory;
            HttpAccessor = httpAccessor;
            DecisionEngine = decisionEngine;
            IdentityService = identityService;
            UserProfileService = userProfileService;
            EncryptionService = encryptionService;
            Logger = logger;
            QuestionnaireAnswerRepository = questionnaireAnswerRepository;
            InviteValidationService = inviteValidationService;
            QuestionSectionsRepository = questionSectionsRepository;            
            QuestionSubSectionsRepository = questionSubSectionsRepository;
            QuestionSectionRepository = questionSectionRepository;
            AssignmentService = assignmentService;
            InviteService = inviteService;
        }

        #endregion

        #region "Private Properties"
        private IQuestionnaireRepository QuestionnaireRepository { get; }
        private IGeneratorService TemporaryApplicationNumberGenerator { get; }
        private IEventHubClient EventHubClient { get; }
        private QuestionnaireConfiguration QuestionnaireConfigurations { get; }
        private ITenantTime TenantTime { get; }
        private IFormRepository FormRepository { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private IApplicationService ApplicationService { get; }
        private IApplicantService ApplicantService { get; }
        private ITenantService TenantService { get; }
        private IDecisionEngineClientFactory DecisionEngineFactory { get; }
        private IStatusManagementServiceFactory StatusManagementFactory { get; }
        private IHttpContextAccessor HttpAccessor { get; }
        private IDecisionEngineService DecisionEngine { get; }
        private IIdentityService IdentityService { get; }
        private IUserProfileService UserProfileService { get; }
        private IEncryptionService EncryptionService { get; }
        private ILogger Logger { get; }
        private IQuestionnaireAnswerRepository QuestionnaireAnswerRepository { get; }
        private string LoanPurpose { get; set; }
        private IInviteValidationService InviteValidationService {get; }
        public IQuestionSectionsRepository QuestionSectionsRepository { get;  }
        public IQuestionSubSectionRepository QuestionSubSectionsRepository { get; }
        private IQuestionSectionRepository QuestionSectionRepository { get; }
        private IAssignmentService AssignmentService { get; }
        private IInviteService InviteService { get; }

        #endregion

        #region "Private Methods"

        /// <summary>
        /// This function loops through the Form and prefills the value in the question based on the configuration.
        /// Prefill functionality assigns the answer to the same question it is asked in other subsections.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        private async Task<IForm> PrefillValues(IForm form)
        {
            int sectionId;
            var emailQuestionId = "";
            var phoneQuestionId = "";
            var firstNameQuestionId = string.Empty;
            var lastNameQuestionId = string.Empty;
            var userId = await GetCurrentUser();
            var emailAddress = string.Empty;
            var firstName = string.Empty;
            var lastName = string.Empty;
            var mobileNumber = string.Empty;

            //// start - Invite Information for First ,Last Name and Mobile number From Assignment Service
            if (!string.IsNullOrWhiteSpace(userId))
            {
                try
                {
                    var inviteResponse = await InviteService.GetInviteByUser(userId);

                    if (inviteResponse != null)
                    {
                        firstName = inviteResponse.InviteFirstName;
                        lastName = inviteResponse.InviteLastName;
                        mobileNumber = inviteResponse.InviteMobileNumber;
                        emailAddress = inviteResponse.InviteEmail;
                    }
                    else
                    {
                        Logger.Info($"Could not find invite information for the user {userId}");
                    }
                }
                catch(ClientException ex)
                {
                    Logger.Info($"Could not find invite information for the user {userId}");
                }
            }
            //// End

            /////////////////////////////////////

            var questionnaireSummaryFieldMap = Extensions.Clone<List<QuestionnaireSummaryConfig>>(QuestionnaireConfigurations.ApplicationSummary.Map);

            //GetQuestionnaireSummaryConfiguration(applicantSummaryQuestionList);
            if (questionnaireSummaryFieldMap != null)
            {
                var accountInfo = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == "Account Information");
                var communicationInfo = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == "Communication Information");
                if (accountInfo == null)
                {
                    throw new Exception($"Account Information doest not exist in questionnaireSummaryFieldMap configuration.");
                }

                if (communicationInfo == null)
                {
                    throw new Exception($"Communication Information doest not exist in questionnaireSummaryFieldMap configuration.");
                }

                sectionId = accountInfo.SectionId;

                emailQuestionId = communicationInfo.QuestionInformation.ContainsKey("Email") ? communicationInfo.QuestionInformation["Email"].QuestionId : string.Empty;
                phoneQuestionId = communicationInfo.QuestionInformation.ContainsKey("CellPhone") ? communicationInfo.QuestionInformation["CellPhone"].QuestionId : string.Empty;

                firstNameQuestionId = accountInfo.QuestionInformation.ContainsKey("FirstName") ? accountInfo.QuestionInformation["FirstName"].QuestionId : string.Empty;
                lastNameQuestionId = accountInfo.QuestionInformation.ContainsKey("LastName") ? accountInfo.QuestionInformation["LastName"].QuestionId : string.Empty;

                var questionAccountInfoList = form.Sections.FirstOrDefault(s => s.SectionId == sectionId)
                    ?.SubSections.FirstOrDefault(ss => ss.SubSectionId == accountInfo.SubSectionId)
                    ?.QuestionSections
                                    .SelectMany(qs => qs.QuestionList);
                var questionCommunicationInfoList = form.Sections.FirstOrDefault(s => s.SectionId == sectionId)
                    ?.SubSections.FirstOrDefault(ss => ss.SubSectionId == communicationInfo.SubSectionId)
                    ?.QuestionSections
                                    .SelectMany(qs => qs.QuestionList);
                if (questionAccountInfoList != null && questionAccountInfoList.Count() < 0)
                {
                    throw new Exception($"No questionnaire found");
                }

                var firstNameQuestion = questionAccountInfoList.FirstOrDefault(ql => ql.QuestionId == firstNameQuestionId);
                var lastNameQuestion = questionAccountInfoList.FirstOrDefault(ql => ql.QuestionId == lastNameQuestionId);
                var emailQuestion = questionCommunicationInfoList.FirstOrDefault(ql => ql.QuestionId == emailQuestionId);
                var phoneQuestion = questionCommunicationInfoList.FirstOrDefault(ql => ql.QuestionId == phoneQuestionId);

                if (emailQuestion == null)
                {
                    throw new Exception($"No question exists having questionid '{emailQuestionId}'");
                }


                firstNameQuestion.Answer = string.IsNullOrEmpty(firstName) ? string.Empty : firstName;
                lastNameQuestion.Answer = string.IsNullOrEmpty(lastName) ? string.Empty : lastName;
                emailQuestion.Answer =  string.IsNullOrEmpty(emailAddress) ? string.Empty : emailAddress;

                #region Prefill Mobile number

                var currentUser = await IdentityService.GetUser(userId);
                if(currentUser != null)
                {
                    if(phoneQuestion!=null && string.IsNullOrEmpty(phoneQuestion.Answer))
                    {
                        phoneQuestion.Answer = string.IsNullOrEmpty(currentUser.MobileNumber) ? mobileNumber : currentUser.MobileNumber;
                    }

                    if(emailQuestion!=null && string.IsNullOrEmpty(emailQuestion.Answer) && !string.IsNullOrEmpty(currentUser.Email))
                    {
                        emailQuestion.Answer = string.IsNullOrEmpty(currentUser.Email) ? string.Empty : currentUser.Email;
                    }   
                }
                else
                {
                    if(phoneQuestion!=null){
                        phoneQuestion.Answer = mobileNumber;
                    }                    
                }

                #endregion

                #region Prefill Mobile number on credit section

                var creditInfo = questionnaireSummaryFieldMap.FirstOrDefault(a => a.SubSectionType == "Credit Review");
                if (creditInfo != null)
                {
                    // throw new Exception($"Credit Review doest not exist in questionnaireSummaryFieldMap configuration.");
                    sectionId = creditInfo.SectionId;
                    phoneQuestionId = creditInfo.QuestionInformation.ContainsKey("Phone") ? creditInfo.QuestionInformation["Phone"].QuestionId : "";

                    var creditSection = form.Sections.FirstOrDefault(s => s.SectionId == sectionId);
                    if (creditSection != null)
                    {
                        var creditPhoneQuestion = creditSection.SubSections.FirstOrDefault(ss => ss.SubSectionId == creditInfo.SubSectionId).QuestionSections.SelectMany(qs => qs.QuestionList).FirstOrDefault(ql => ql.QuestionId == phoneQuestionId);
                        creditPhoneQuestion.Answer = string.IsNullOrEmpty(currentUser.MobileNumber) ? mobileNumber : currentUser.MobileNumber;
                    }
                }               

                #endregion
            }

            //Assign username to all the Predefined section

            var subSections = new List<ISubSection>();
            var userName = await GetCurrentUser();

            subSections = form.Sections.SelectMany(s => s.SubSections).ToList();
            foreach (var subSection in subSections)
            {
                if (string.IsNullOrEmpty(subSection.UserName))
                {
                    subSection.UserName = userName;
                }

            }

            foreach (var _section in form.Sections)
            {
                _section.UserName = userName;
            }

            return form;
        }

        /// <summary>
        /// This function provides the list of all the skippable questions
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private async Task<ISection> MakeRequiredQuestionSkipable(ISection section)
        {
            var tenantConfiguration = QuestionnaireConfigurations.TenantConfiguration;
            var summarySectionId = Convert.ToInt32(SectionDetails.Summary);

            if (section.SectionId < summarySectionId && tenantConfiguration.IsRequiredQuestionSkipable)
            {
                var subSectionquestions = section.SubSections
                                       .SelectMany(qs => qs.QuestionSections)
                                       .SelectMany(ql => ql.QuestionList)
                                       .Where(q => q.Required.ToLower() != "true")
                                       .ToList();

                if (subSectionquestions.Any())
                {
                    subSectionquestions.ForEach(c => c.Required = Extensions.GetDescription(RequiredType.False).ToLower());
                }

                var templates = section.Templates;

                foreach (var key in templates.Keys)
                {
                    var templateQuestions = section.Templates
                                            .Where(s => s.Key == key)
                                            .Select(ss => ss.Value)
                                            .SelectMany(qs => qs.QuestionSections)
                                            .SelectMany(q => q.QuestionList)
                                            .Where(q => q.Required.ToLower() != "true")
                                            .ToList();

                    if (templateQuestions.Any())
                    {
                        templateQuestions.ForEach(c => c.Required = Extensions.GetDescription(RequiredType.False).ToLower());
                    }

                }

            }

            return await Task.Run(() => section);
        }

        /// <summary>
        /// This function provides a collection of the child questions from the questionniare
        /// </summary>
        /// <param name="childQnList"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        private async Task<List<IChildConditionalQuestion>> GetChildQuestions(List<IChildConditionalQuestion> childQnList, IQuestionnaire questionnaire)
        {
            var requiredChildQnList = new List<IChildConditionalQuestion>();

            foreach (var childQn in childQnList)
            {
                var childQuestion = questionnaire.ApplicationForm.Sections.Where(x => x.SectionId == childQn.SectionId)
                                                            .SelectMany(x => x.SubSections).Where(x => x.SubSectionId == childQn.SubSectionId)
                                                            .SelectMany(x => x.QuestionSections)
                                                            .SelectMany(x => x.QuestionList).Where(x => x.QuestionId == childQn.QuestionId
                                                                                                           && x.QuestionType != QuestionType.Null
                                                                                                           && x.QuestionType != QuestionType.Label
                                                                                                           && x.ChildQuestions != null
                                                                                                           && x.ChildQuestions.Any()).FirstOrDefault();

                if (childQuestion != null && childQuestion.ChildQuestions != null && childQuestion.ChildQuestions.Any())
                {
                    var parentAnswer = childQuestion != null ? childQuestion.Answer.ToLower() : null;
                    var childQns = childQuestion.ChildQuestions.Where(x => x.Answer == childQuestion.Answer
                                                                           || (x.Answer != null && x.Answer.ToLower() == parentAnswer)).ToList();
                    requiredChildQnList.AddRange(childQns);
                }
            }

            return await Task.Run(() => requiredChildQnList);
        }

        /// <summary>
        /// This function validates whether the logged in user is a Lender user or not
        /// </summary>
        /// <returns></returns>
        private bool IsLenderUser()
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                return token.Scope.Contains(Extensions.GetDescription(ScopeType.BackOffice));

            }
            catch (InvalidUserException exception)
            {
                throw new InvalidUserException(exception.Message);
            }
            catch (Exception)
            {
                throw new Exception("User not authenticated.");
            }
        }

        /// <summary>
        /// This function is used for removing null, empty or 0 answer questions 
        /// also checks if question is label and have child questions then if all childs answer is null, empty or 0 than removes label also
        /// </summary>
        /// <param name="subSectionSummary"></param>
        /// <returns></returns>
        private async Task<ISubSectionSummary> RemoveNullEmptyOrZeroAnswerQuestions(ISubSectionSummary subSectionSummary)
        {
            if (subSectionSummary == null)
                throw new Exception($" {subSectionSummary} Not found");

            if (subSectionSummary.SectionId != Convert.ToInt32(SectionDetails.Declarations) && subSectionSummary.SectionId != Convert.ToInt32(SectionDetails.Assets))
            {
                var questionsToRemove = new List<IQuestion>();
                var nullableQuestionTypes = Enum.GetValues(typeof(QuestionType)).Cast<QuestionType>();

                foreach (QuestionSection qs in subSectionSummary.SubSection.QuestionSections)
                {
                    if (subSectionSummary.SubSectionId == "SS10")
                    {
                        qs.QuestionList = qs.QuestionList.Where(x => x != null).Where(x => !(nullableQuestionTypes.Any(y => y == x.QuestionType)
                                            && (x.QuestionType != QuestionType.TextCurrRight && x.QuestionType != QuestionType.TotalAmountCalculator && x.Answer == "0" 
                                            || x.QuestionType != QuestionType.TextCurrRight && x.QuestionType != QuestionType.TotalAmountCalculator && string.IsNullOrWhiteSpace(x.Answer)))).ToList();
                    }
                    else
                    {
                        qs.QuestionList = qs.QuestionList.Where(x => x != null).Where(x => !(nullableQuestionTypes.Any(y => y == x.QuestionType)
                                            && (x.Answer == "0" || string.IsNullOrWhiteSpace(x.Answer)))).ToList();
                    }

                    var questions = qs.QuestionList.Where(x => x.QuestionType == QuestionType.Label && x.ChildQuestions != null && x.ChildQuestions.Any());

                    if (questions != null)
                    {
                        foreach (var pQn in questions)
                        {
                            var removeParentQn = true;
                            if(pQn.QuestionType == QuestionType.TextCurrRight || pQn.QuestionType ==  QuestionType.TotalAmountCalculator)
                            {
                                removeParentQn = false;
                            }
                            foreach (var cQn in pQn.ChildQuestions)
                            {
                                var questionAnswer = subSectionSummary.SubSection.QuestionSections
                                                                                 .SelectMany(p => p.QuestionList)
                                                                                 .FirstOrDefault(q => q.QuestionId == cQn.QuestionId);

                                if (!(nullableQuestionTypes.Any(y => y == questionAnswer.QuestionType)
                                    && (questionAnswer.Answer == "0" || string.IsNullOrWhiteSpace(questionAnswer.Answer))))
                                {
                                    removeParentQn = false;
                                    pQn.ChildQuestions = null;
                                }
                            }

                            if (removeParentQn)
                                questionsToRemove.Add(pQn);
                        }
                    }
                }

                foreach (QuestionSection qs in subSectionSummary.SubSection.QuestionSections)
                {
                    foreach (var item in questionsToRemove)
                        qs.QuestionList.Remove(item);
                }
            }

            subSectionSummary.SubSection.QuestionSections = subSectionSummary.SubSection.QuestionSections.Where(qs => qs.QuestionList.Count != 0).ToList();

            return await Task.Run(() => subSectionSummary);
        }

        /// <summary>
        /// This function gets the address info.
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="subSection"></param>
        /// <param name="applicantType"></param>
        /// <returns></returns>
        private async Task<Dictionary<string, IQuestion>> GetAddressInfo(int sectionId, ISubSection subSection, ApplicantType applicantType)
        {
            //This function fetches the questions answer from the sub section for a borrower and co-borrower.            

            IBorrowerCoBorrowerInfo frm = new BorrowerCoBorrowerInfo();

            var appFieldMap = new ApplicationFields(); // GetApplicationConfiguration(mappingInfoName); // "borrower-subsection-field-map");

            if (applicantType == ApplicantType.Borrower)
            {
                appFieldMap = QuestionnaireConfigurations.BorrowerSubsectionFieldMap;
            }
            else if (applicantType == ApplicantType.Spouse)
            {
                appFieldMap = QuestionnaireConfigurations.SpouseSubsectionFieldMap;
            }
            else if (applicantType == ApplicantType.CoBorrower)
            {
                appFieldMap = QuestionnaireConfigurations.CoborrowerSubsectionFieldMap;
            }
            var Addresses = new Dictionary<string, IQuestion>();

            foreach (IApplicationFieldMap item in appFieldMap.Map)
            {

                var questionAnswer = subSection.QuestionSections
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == item.QuestionId);

                if (questionAnswer != null)
                {
                    if (item.TargetProperty.ToLower() == "currentaddress" || item.TargetProperty.ToLower() == "mailingaddress" || item.TargetProperty.ToLower() == "sameascurrentaddress")
                    {
                        if (Addresses.ContainsKey(item.TargetProperty.ToLower()) == false)
                        {
                            Addresses.Add(item.TargetProperty.ToLower(), questionAnswer);
                        }
                        else
                        {
                            Addresses[item.TargetProperty.ToLower()] = questionAnswer;
                        }

                    }
                }
            }

            return await Task.Run(() => Addresses);
        }

        /// <summary>
        /// This function is used to populate spouse residential address from borrower resiential address if Spouse resides with the borrower.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        private async Task<bool> ExecuteSameAsCurrentAddressEvent(string temporaryApplicationNumber, int sectionId, string subSectionId, string eventName)
        {
            //This function copies answer from the Current Address to Mailing Address

            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
            var subSection = questionnaire.ApplicationForm.Sections.Find(s => s.SectionId == sectionId).SubSections.Find(ss => ss.SubSectionId == subSectionId);
            var Addresses = new Dictionary<string, IQuestion>();

            Addresses = await GetAddressInfo(sectionId, subSection, ApplicantType.Borrower);

            ValidateCollection(Addresses, "currentaddress");
            ValidateCollection(Addresses, "mailingaddress");
            ValidateCollection(Addresses, "sameascurrentaddress");

            if (Addresses["sameascurrentaddress"].Answer.ToLower() == "false")
            {
                //if the answer is false, then no need to copy, just return True
                return true;
            }

            //After getting the address copy the Current Address into Mailing Address
            IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
            answerRequest.SectionId = sectionId;
            answerRequest.SubSectionId = subSectionId;
            answerRequest.TemporaryApplicationNumber = temporaryApplicationNumber;
            answerRequest.QuestionId = Addresses["mailingaddress"].QuestionId;  //Get the Question Id of Mailing Address
            answerRequest.Answer = Addresses["currentaddress"].Answer;          //But get the answer of current Address

            var status = false;

            //Now update the question
            status = await UpdateQuestion(answerRequest);
            return status;
        }

        /// <summary>
        /// This function gets the client IP
        /// </summary>
        /// <returns></returns>
        private string GetClientIp()
        {
            const string message = "Cannot determine client IP";
            try
            {
                if (HttpAccessor == null)
                    throw new ArgumentNullException(message);

                if (HttpAccessor.HttpContext == null)
                    throw new ArgumentNullException(message);

                if (HttpAccessor.HttpContext.Request == null)
                    throw new ArgumentNullException(message);

                if (!HttpAccessor.HttpContext.Request.Headers.Any(h => h.Key.ToLower() == "x-client-ip"))
                    throw new ArgumentNullException(message);

                return HttpAccessor.HttpContext.Request.Headers["X-Client-IP"];
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetClientId('{message}') raised an error\n: {ex.Message}");
            }
            return null;
        }
       
        private async Task<ISubSection> UpdateSubSectionId(ISubSection subSection, string subSectionId)
        {
            var questions = subSection.QuestionSections.SelectMany(qs => qs.QuestionList)
                       .Where(q => q.ChildQuestions != null).ToList();

            if (questions != null)
            {
                foreach (var question in questions)
                {
                    foreach (var childQuestion in question.ChildQuestions)
                    {
                        childQuestion.SubSectionId = subSectionId;
                    }
                }
            }

            return await Task.Run(() => subSection);
        }

        /// <summary>
        /// This function encrypts the answer if
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        private string EncryptAnswer(IQuestion question)
        {
            if (question.IsSensitive && !string.IsNullOrEmpty(question.Answer))
            {
                //if sensitive then encrypt the data
                question.Answer = this.EncryptionService.Encrypt<string>(question.Answer);
            }

            return question.Answer;
        }

        /// <summary>
        /// Fetches the question section using the Question Section seq number
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <param name="SectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="questionSectionSeqNo"></param>
        /// <returns></returns>
        private async Task<IQuestionSection> GetQuestionSectionForSectionSubSection(IQuestionnaire questionnaire, int SectionId, string subSectionId, int questionSectionSeqNo)
        {
            IQuestionSection questionSection = new QuestionSection();

            try
            {
                questionSection = questionnaire.ApplicationForm.Sections.First(s => s.SectionId == SectionId).SubSections.First(ss => ss.SubSectionId == subSectionId).QuestionSections.First(qs => qs.SeqNo == questionSectionSeqNo);
            }
            catch (Exception)
            {

            }

            return await Task.Run(() => questionSection);
        }

        

        /// <summary>
        /// Update the status using the Status management service
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        private bool SetStatus(string applicationNumber)
        {
            //TODO: By Nayan : Why factory is used? It should be using service directly
            Logger.Debug($"Started SetStatus for {applicationNumber}");
            var statusManagementService = StatusManagementFactory.Create(TokenReader);
            if (statusManagementService != null)
            {
                statusManagementService.ChangeStatus("application", applicationNumber, "200.10",null).Wait();
                Logger.Debug($"Ended SetStatus for {applicationNumber}");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get the status of the Application using the status management
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        private async Task<LendFoundry.StatusManagement.IStatusResponse> GetApplicationStatusFromStatusManagement(string applicationNumber)
        {
            var statusManagementService = StatusManagementFactory.Create(TokenReader);
            LendFoundry.StatusManagement.IStatusResponse statusResponse = new LendFoundry.StatusManagement.StatusResponse();
            if (statusManagementService != null)
            {
                statusResponse = await statusManagementService.GetStatusByEntity("application", applicationNumber);
            }

            return await Task.Run(() => statusResponse);
        }

        /// <summary>
        /// This fucntion fetches the credit information using the configuration
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="subSection"></param>
        /// <returns></returns>
        private async Task<ICreditInfo> GetCreditInfoFromTheMapping(int sectionId, ISubSection subSection)
        {
            //This function fetches the questions answer form the sub section for a borrower and co-borrower.            

            ICreditInfo frm = new CreditInfo();

            var appFieldMap = QuestionnaireConfigurations.CreditFieldMap; //GetApplicationConfiguration(mappingInfoName); // "borrower-subsection-field-map");

            foreach (IApplicationFieldMap item in appFieldMap.Map)
            {
                var questionAnswer = subSection.QuestionSections
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == item.QuestionId);

                if (questionAnswer != null)
                {

                    if (item.TargetProperty.ToLower() == "dateofbirth")
                    {
                        frm.DateOfBirth = questionAnswer.Answer;
                    }
                    else if (item.TargetProperty.ToLower() == "ssn")
                    {
                        frm.Ssn = questionAnswer.Answer;
                    }
                    else if (item.TargetProperty.ToLower() == "phone")
                    {
                        frm.Phone = questionAnswer.Answer;
                    }

                    frm.SubSectionId = subSection.SubSectionId;
                    frm.SectionId = sectionId;
                }
            }


            return await Task.Run(() => frm);
        }

        /// <summary>
        /// This function executes the log consent event
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        private async Task<bool> ExecuteLogConsentEvent(string temporaryApplicationNumber, int sectionId, string subSectionId)
        {

            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
            var subSection = questionnaire.ApplicationForm.Sections.Find(s => s.SectionId == sectionId).SubSections.Find(ss => ss.SubSectionId == subSectionId);
            ICreditInfo creditInfo = new CreditInfo();

            var status = false;
            var token = TokenParser.Parse(TokenReader.Read());
            var reader = new StaticTokenReader(token.Value);
            var decisionEngine = DecisionEngineFactory.Create(reader);

            creditInfo = await GetCreditInfoFromTheMapping(sectionId, subSection);
            creditInfo.UserName = await GetCurrentUser();

            var ipAddress = GetClientIp();

            IConsentRequestPayload consentPayload = new ConsentRequestPayload();

            consentPayload.ApplicationNumber = temporaryApplicationNumber;
            consentPayload.DateOfBirth = creditInfo.DateOfBirth;
            consentPayload.FullName = creditInfo.UserName;
            consentPayload.PhoneNumber = creditInfo.Phone;
            consentPayload.Ssn = creditInfo.Ssn;
            consentPayload.ipAddress = ipAddress;
            consentPayload.metaData = new ConsentMetaData { createdBy = creditInfo.UserName };

            IConsentRequest consentRequest = new ConsentRequest();
            consentRequest.entityType = "application";
            consentRequest.entityId = temporaryApplicationNumber;
            consentRequest.payload = consentPayload;
            consentRequest.consentName = "CreditCommunicationConsent";

            var jResult = await Task.Run(() => decisionEngine.Execute<dynamic, Newtonsoft.Json.Linq.JObject>("consentacknowledge", consentRequest));

            if (jResult != null)
            {
                var statusResult = jResult.GetValue("status").ToString();
                if (statusResult == "success")
                {
                    status = true;
                }
                else
                {
                    if (jResult.GetValue("error").ToString() == "User already invited.")
                    {
                        //Even if it is an error, if the invitation has already been sent then Do Not consider it as an error.
                        status = true;

                    }
                    else
                    {
                        status = false;
                        throw new Exception("Error occured in Saving consent information.");
                    }
                }
            }

            return await Task.Run(() => status);
        }

        private async Task<bool> UpdateUserProfile(string temporaryApplicationNumber)
        {
            Logger.Debug("Updating UserProfile...");
            var userName = await GetCurrentUser();
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
            Logger.Debug("...GetBorrowerInfo");
            var borrowerInfo = await GetBorrowerInfo(temporaryApplicationNumber, userName, questionnaire);
            if(borrowerInfo!= null)
            {
                Logger.Debug("Requesting UserProfile service");
                var updateStatus = await UpdateUserProfile(temporaryApplicationNumber, userName, borrowerInfo);
                return updateStatus;
            }
            Logger.Debug($"BorrowerInfo not found for {userName}");
            return false;
        }

        /// <summary>
        /// This function, hides or creates a new Sub section when user selects "Purchase" and "Refinance"
        /// This event is also called on SS1 subsection NEXT button event
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        private async Task<bool> ShowHidePurchaseRefinanceSection(string temporaryApplicationNumber)
        {
            //Update UserProfile
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
            var userName = await GetCurrentUser();
            var borrowerInfo = await GetBorrowerInfo(temporaryApplicationNumber, userName, questionnaire);
            if (string.IsNullOrEmpty(borrowerInfo.FirstName) || string.IsNullOrEmpty(borrowerInfo.LastName))
            {
                Logger.Warn($"First Name or Last Name is not provided to update the UserProfile for the user {userName}");
            }
            else{
                Logger.Debug($"Updating userprofile with {userName}");
                var updateStatus = await UpdateUserProfile(temporaryApplicationNumber, userName, borrowerInfo);
            }
            
            var appFieldMap = QuestionnaireConfigurations.QuestionnaireLoanPurpose; 
            var question = await GetLoanPurposeQuestion(temporaryApplicationNumber, appFieldMap, questionnaire);

            if (question.Answer.ToLower() == REFINANCE.ToLower())
            {
                await DeleteSubSection(temporaryApplicationNumber, appFieldMap.Map.RealEstateSectionId, appFieldMap.Map.RealEstateAgentSubSectionId);
                return true;
            }

            return false;
        }

        private async Task<bool> PrimaryPropertyUseTrigger(string temporaryApplicationNumber, int sectionId, string subSectionId)
         {
             var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire != null)
            {
                var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
                Logger.Info("... PrimaryPropertyUseTrigger - Started");
                 var updatedFlag = false;
                  var question = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerTransactionType);
                
                 var refinanceAddressPrepopulateSubsection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                                    .SelectMany(s => s.SubSections)
                                    .FirstOrDefault(x=>x.TemplateId == Constant.TemplateIdForRefinanceAddressPrepopulate);

                if (question.Answer.ToLower() == REFINANCE.ToLower())
                {
                    QuestionnaireQuestionAnswer questionnaireQuestionAnswer = null;

                    var propertyUseQuestion = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                        .SelectMany(s => s.SubSections)
                        .SelectMany(ss => ss.QuestionSections)
                        .SelectMany(qs => qs.QuestionList)
                        .FirstOrDefault(q => q.QuestionId == Constant.BorrowerPropertyUseQuestionId);

                    //// update answer for checkbox value for refinance address same on primary trigger
                    if(propertyUseQuestion != null)
                    {
                        var answer = "false";
                        if(propertyUseQuestion.Answer == "Primary"){
                            answer = "true";
                        }
                        var sameAddressAsRefinance = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                                    .SelectMany(s => s.SubSections)
                                    .SelectMany(ss => ss.QuestionSections)
                                    .SelectMany(qs => qs.QuestionList)
                                    .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerCurrentAddressSame);

                        questionnaireQuestionAnswer = new QuestionnaireQuestionAnswer{
                            TemporaryApplicationNumber = temporaryApplicationNumber,
                            SectionId = profileSectionId,
                            SubSectionId = refinanceAddressPrepopulateSubsection.SubSectionId,
                            QuestionSectionSeqNo = GetSeqNumberForQuestionList(questionnaire,profileSectionId,refinanceAddressPrepopulateSubsection.SubSectionId, sameAddressAsRefinance.QuestionId),
                            QuestionId = sameAddressAsRefinance.QuestionId,
                            Answer = answer
                        };

                        await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionnaireQuestionAnswer);
                        updatedFlag = true;
                        Logger.Info($"... Updated Answer PrimaryPropertyUseTrigger - {updatedFlag}");

                    }
                   
                    
                    Logger.Info("... PrimaryPropertyUseTrigger - Ended");
                    return updatedFlag;
                }
            }
            Logger.Info("... PrimaryPropertyUseTrigger in questionnaire not found");
            return false;
        }
         private async Task<bool> PopulatingAddressForRefinance(string temporaryApplicationNumber, int sectionId, string subSectionId)
         {
             var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire != null)
            {
                var profileSectionId = Convert.ToInt32(SectionDetails.Profile);
                Logger.Info("... PopulatingAddressForRefinance - Started");
                var updatedFlag = false;
                  var question = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerTransactionType);
                
                 var refinanceAddressPrepopulateSubsection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                                    .SelectMany(s => s.SubSections)
                                    .FirstOrDefault(x=>x.TemplateId == Constant.TemplateIdForRefinanceAddressPrepopulate);

                if (question.Answer.ToLower() == REFINANCE.ToLower())
                {
                    QuestionnaireQuestionAnswer questionnaireQuestionAnswer = null;
                    var addressQuestion = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                        .SelectMany(s => s.SubSections)
                        .SelectMany(ss => ss.QuestionSections)
                        .SelectMany(qs => qs.QuestionList)
                        .FirstOrDefault(q => q.QuestionId == Constant.AddressPrepopulateQuestionId);

                    //// update answer on readonly question 
                    if(addressQuestion != null &&  !string.IsNullOrWhiteSpace(addressQuestion.Answer))
                    {
                        var readOnlyAddress = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == profileSectionId)
                                    .SelectMany(s => s.SubSections)
                                    .SelectMany(ss => ss.QuestionSections)
                                    .SelectMany(qs => qs.QuestionList)
                                    .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerCurrentStreetAddressCheckBoxTrue);

                        questionnaireQuestionAnswer = new QuestionnaireQuestionAnswer{
                            TemporaryApplicationNumber = temporaryApplicationNumber,
                            SectionId = profileSectionId,
                            SubSectionId = refinanceAddressPrepopulateSubsection.SubSectionId,
                            QuestionSectionSeqNo = GetSeqNumberForQuestionList(questionnaire,profileSectionId,refinanceAddressPrepopulateSubsection.SubSectionId, readOnlyAddress.QuestionId),
                            QuestionId = readOnlyAddress.QuestionId,
                            Answer = addressQuestion.Answer
                        };

                        await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionnaireQuestionAnswer);
                        updatedFlag = true;
                        Logger.Info($"... Updated Answer PopulatingAddressForRefinance - {updatedFlag}");
                    }
                    
                    Logger.Info("... PopulatingAddressForRefinance - Ended");
                    return updatedFlag;
                }
            }
            Logger.Info("... PopulatingAddressForRefinance in questionnaire not found");
            return false;
        }

        public int GetSeqNumberForQuestionList(IQuestionnaire questionnaire, int sectionId, string subsectionId,string questionId)
        {
             var questionListObject = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == sectionId)
                                    .SelectMany(s => s.SubSections).Where(s => s.SubSectionId == subsectionId)
                                    .SelectMany(ss => ss.QuestionSections)
                                    .Where(x => x.QuestionList.Any(y=>y.QuestionId == questionId))
                                    .FirstOrDefault();

            if(questionListObject != null)
            {
                return questionListObject.SeqNo;
            }
                                    
            return 0;
        }

        /// <summary>
        /// This function validates the email address
        /// </summary>
        /// <param name="InputEmail"></param>
        /// <returns></returns>
        private static bool IsValidEmailId(string InputEmail)
        {
            var regex = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");
            var match = regex.Match(InputEmail);
            if (match.Success)
                return true;
            else
                return false;
        }

        private async Task<bool> UpdateQuestionSectionAndStatus(IAnswerUpdateRequest questionRequest)
        {
            var Sections = new List<ISection>();
            IQuestion questionToUpdate = new Question();
            IQuestionnaire questionnaire = new Questionnaire();
            ISection sectionFound = new Section();

            //Get Questionnaire for the applicant
            questionnaire = await GetQuestionnaire(questionRequest.TemporaryApplicationNumber);//GetByApplicationIdFilter(questionRequest.ApplicantId);


            //If the user updating the questionnaire is different than the user who created it, and do not allow the user to update the answer.

            ISubSection subSection = null;
            ISection sectiontoUpdate = null;
            if (questionnaire != null)
            {
                //Get the section that needs to be updated.
                Sections = questionnaire.ApplicationForm.Sections;
                sectiontoUpdate = Sections.FirstOrDefault(Section => Section.SectionId == questionRequest.SectionId);
                if (sectiontoUpdate == null)
                {
                    throw new SectionDoesNotExistsException($"Section '{questionRequest.SectionId}' does not exist.");
                }

                //Validate Question Exists in the Section
                //Get the SubSectionName and the QuestionSection Name from the parameter

                subSection = sectiontoUpdate.SubSections.FirstOrDefault(subsection => subsection.SubSectionId == questionRequest.SubSectionId);

                if (subSection == null)
                {
                    throw new SubSectionDoesNotExistsException($"Sub Section '{questionRequest.SubSectionId}' does not exist.");
                }


                ////////////////////////////////////////////
                //code to fetch the questionSection

                var questionSectionSeqNo = GetQuestionSectionSeqNoForAQuestion(subSection, questionRequest.QuestionId);

                questionRequest.QuestionSectionSeqNo = questionSectionSeqNo;
                IQuestionSection questionSectionToUpdate = new QuestionSection();
                questionSectionToUpdate = subSection.QuestionSections.First(qs => qs.SeqNo == questionSectionSeqNo);
                if (questionSectionToUpdate == null)
                {
                    throw new QuestionSectionDoesNotExistsException($"Question Section having SeqNo = '{questionRequest.QuestionSectionSeqNo}' does not exist, in the Sub Section Id = '{questionRequest.SubSectionId}', in the Section Id = '{questionRequest.SectionId}'.");
                }


                ///////////////////////////////////////////////////////////////////////////
                questionToUpdate = ValidateQuestionExistenceInTheQuestionSection(questionSectionToUpdate, questionRequest.QuestionId);

                if (questionToUpdate == null)
                {
                    throw new QuestionNotFoundException($"Question having the question Id {questionRequest.QuestionId} does not exist.");
                }


                if (questionToUpdate.Required.ToLower() == Extensions.GetDescription(RequiredType.True).ToLower())
                {
                    if (questionRequest.Answer == "" || questionRequest.Answer == null)
                    {
                        throw new AnswerNotProvidedException($"An answer is required for the question '{ questionToUpdate.QuestionText}' having question Id '{questionRequest.QuestionId}'. Please provide the answer.");

                    }
                }

                if (questionToUpdate == null)
                {
                    throw new QuestionNotFoundException($"Question having question Id '{questionRequest.QuestionId}' for the applicant '{questionnaire.ApplicantId}' having the section Id '{questionRequest.SectionId}', does not exist.");
                }

                var isAnswerValid = ValidateAnswer(questionToUpdate, questionRequest.Answer);

                if (isAnswerValid == false)
                {
                    throw new InvalidAnswerException($"The answer '{questionRequest.Answer}' is invalid for the question '{questionToUpdate.QuestionText}'");
                }

                //If the Questionnaire exist and the Question to update exists then update the question.

                //Update the answer
                questionToUpdate.Answer = questionRequest.Answer;
                questionToUpdate.Answer = questionRequest.Answer = EncryptAnswer(questionToUpdate);

                if (questionnaire.HighestSectionReached < questionRequest.SectionId)
                {
                    questionnaire.HighestSectionReached = questionRequest.SectionId;
                    questionnaire.HighestSubSectionReached = questionRequest.SubSectionId;
                }
                bool result = false;

                //var result = await QuestionnaireRepository.UpdateQuestionSection(questionRequest, questionSectionToUpdate);
                var questionData = await  QuestionnaireRepository.UpdateQuestion(questionRequest,questionToUpdate);

               // if (result == true)
               if(questionData != null)
                {
                    result = true;
                    // Update autofill question's answers
                    if (questionToUpdate.AutoFillQuestions != null)
                    {
                        var autoQuestionCount = questionToUpdate.AutoFillQuestions.Count();
                        for (var questionIndex = 0; questionIndex < autoQuestionCount; questionIndex++)
                        {
                            var autoFillSectionId = Convert.ToInt32(questionToUpdate.AutoFillQuestions[questionIndex].SectionId); //questionSectionId = Convert.ToInt32(questionToUpdate.AutoFillQuestions[questionIndex].SectionId);
                            var autoFillQuestionSubSectionId = questionToUpdate.AutoFillQuestions[questionIndex].SubSectionId;
                            var autoFillQuestionId = questionToUpdate.AutoFillQuestions[questionIndex].QuestionId;
                            var autoFillSubSection = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == autoFillSectionId).FirstOrDefault().SubSections.Where(ss => ss.SubSectionId == questionToUpdate.AutoFillQuestions[questionIndex].SubSectionId).FirstOrDefault();
                            var autoFillQuestionSectionSeqNo = GetQuestionSectionSeqNoForAQuestion(autoFillSubSection, autoFillQuestionId);
                            var autoFillQuestionSectionToUpdate = await GetQuestionSectionForSectionSubSection(questionnaire, autoFillSectionId, autoFillQuestionSubSectionId, autoFillQuestionSectionSeqNo);

                            IAnswerUpdateRequest autoFillQuestionRequest = new AnswerUpdateRequest();
                            autoFillQuestionRequest.QuestionId = autoFillQuestionId;
                            autoFillQuestionRequest.SectionId = autoFillSectionId;
                            autoFillQuestionRequest.SubSectionId = autoFillQuestionSubSectionId;
                            autoFillQuestionRequest.QuestionSectionSeqNo = autoFillQuestionSectionSeqNo;
                            autoFillQuestionRequest.TemporaryApplicationNumber = questionRequest.TemporaryApplicationNumber;
                            autoFillQuestionRequest.Answer = questionRequest.Answer;

                            var autoFillQuestionToUpdate = ValidateQuestionExistenceInTheQuestionSection(autoFillQuestionSectionToUpdate, autoFillQuestionId);


                            if (autoFillQuestionToUpdate != null)
                            {
                                autoFillQuestionToUpdate.Answer = questionRequest.Answer;
                            }

                            if (autoFillQuestionRequest.QuestionSectionSeqNo > 0)
                            {
                                await QuestionSectionsRepository.UpdateQuestionSection(autoFillQuestionRequest);
                            }

                        }
                    }
                }

                if (result == true)
                {
                    return await Task.Run(() => true);
                }
                else
                {
                    throw new ArgumentException($"Answer for the Question '{questionRequest.QuestionId}' could not be updated");
                }
            }
            else
            {
                throw new NotFoundException($"Questionnaire for the applicant '{questionRequest.TemporaryApplicationNumber}' not found");
            }
        }

        /// <summary>
        /// This function fetches the values of specific fields from the questionnaire 
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        private async Task<ITransitionFormExtended> GetQuestionnaireDetails(string temporaryApplicationNumber, string userName = null) //GenerateApplicationFromQuestionnaire()public async Task<ITransitionFormExtended> GenerateApplicationFromQuestionnaire()
        {
            //This function fetches the Questionnaire and maps the value to the Application field.
            //It then calls the Application creation API.

            Logger.Info(
                $"GetQuestionnaireDetails:  Temporary Application Number: {temporaryApplicationNumber} for user:{userName}");

            var request = await GetQuestionnaire(temporaryApplicationNumber, userName);

            ITransitionFormExtended applicantInfo = new TransitionForm();

            applicantInfo.companyStackLessThan25Percentage = true;

            var appFieldMap = QuestionnaireConfigurations.ApplicationFieldMap; // GetApplicationConfiguration("application-field-map");

            foreach (IApplicationFieldMap item in appFieldMap.Map)
            {
                var section = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId);
                if(section == null)
                    continue;
                    
                var questionAnswer = section.SelectMany(s => s.SubSections)
                       .Where(ss => (ss.UserName == userName) || (ss.isDefault == true && ss.Tag.Count == 0))
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == item.QuestionId);

                if (questionAnswer != null)
                {
                    try
                    {
                        applicantInfo = await GetBorrowerDetail(request, questionAnswer, applicantInfo, item, userName);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message, ex);
                    }
                }
            }

            var borrowerInfo = await GetBorrowerInfo(temporaryApplicationNumber, userName);
            applicantInfo.ApplicantType = borrowerInfo.ApplicantType;

            applicantInfo.WorkflowId = "1";
            applicantInfo.AnnualGrossIncome = applicantInfo.EmploymentIncome;
            applicantInfo.Channel = "Merchant";
            applicantInfo.HomeOwnership = "";

            return applicantInfo;
        }

        /// <summary>
        /// Income is one such section, where we create the Sub sections on the client. As new sub sections gets created, user name needs to be assigned to those sub sections.
        /// To get the username, we get the root sequence number to fetch that section.
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <param name="sectionId"></param>
        /// <param name="rootSequenceNumber"></param>
        /// <returns></returns>
        private async Task<ISubSection> GetRootSectionSeqNo(IQuestionnaire questionnaire, int sectionId, int rootSequenceNumber)
        {
            var subSection = questionnaire.ApplicationForm.Sections.FirstOrDefault(s => s.SectionId == sectionId).SubSections.Where(x => x.SeqNo < rootSequenceNumber).OrderByDescending(d => d.SeqNo).FirstOrDefault();
            return await Task.Run(() => subSection);
            //Root means the subsection having the email address in the UserName.            
        }
        
        private ISubSection GetRootSectionSeqNo(ISection section, int rootSequenceNumber)
        {
            return section.SubSections.Where(x => x.SeqNo < rootSequenceNumber).OrderByDescending(d => d.SeqNo).FirstOrDefault();
        }

        /// <summary>
        /// This function fetches the couple from the list of applicants.
        /// </summary>
        /// <param name="borrowersInfo"></param>
        /// <returns></returns>
        private async Task<List<string>> GetCoupleUserNames(List<IBorrowerCoBorrowerSpouseInfo> borrowersInfo)
        {
            var userName = await GetCurrentUser();
            var coupleInfo = new List<string>();
            coupleInfo.Add(userName);

            // Check if logged in user is either spouse or borrower
            if (borrowersInfo.FirstOrDefault(x => x.UserName == userName && x.ActualApplicantType != ApplicantType.CoBorrower && x.ActualApplicantType != ApplicantType.CoBorrowerSpouse) != null)
            {
                // get other user which is not logged user but spouse or primary borrower
                var spouseOrBorrowerUser = borrowersInfo.Where(x => x.ActualApplicantType != ApplicantType.CoBorrower && x.ActualApplicantType != ApplicantType.CoBorrowerSpouse && !coupleInfo.Contains(x.UserName))
                    .Select(s => s.UserName).ToList();
                coupleInfo.AddRange(spouseOrBorrowerUser);
            }

            return coupleInfo;
        }

        /// <summary>
        /// This function fetches the username of coBorrowerSpouse
        /// </summary>
        /// <param name="userStates"></param>
        /// <param name="coBorrowerSpouseUsername"></param>
        /// <returns></returns>
        private async Task<string> GetCoupleUserName(List<IUserAccessedStatus> userStates, string coBorrowerSpouseUsername)
        {
            return await Task.Run(() => {

                var coBorrowerUserName = userStates.FirstOrDefault(x=>x.SpouseUserName == coBorrowerSpouseUsername);

                if(coBorrowerUserName != null)
                    return coBorrowerUserName.UserName;

                return string.Empty;

            });   
        }

        /// <summary>
        /// This function provides the Loan purpose question using the configuration
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="appFieldMap"></param>
        /// <returns></returns>
        private async Task<IQuestion> GetLoanPurposeQuestion(string temporaryApplicationNumber, QuestionnaireConfig appFieldMap = null, IQuestionnaire questionnaire = null)
        {
            if (appFieldMap == null)
            {
                appFieldMap = QuestionnaireConfigurations.QuestionnaireLoanPurpose;
            }

            if (appFieldMap.Map != null)
            {
                if (appFieldMap.Map != null)
                {
                    var sectionId = appFieldMap.Map.SectionId;
                    var subSectionId = appFieldMap.Map.SubSectionId;
                    var questionId = appFieldMap.Map.QuestionId;
                    var question = await GetQuestion(temporaryApplicationNumber, sectionId, subSectionId, questionId, questionnaire);
                    return question;
                }
            }
            return null;
        }

        /// <summary>
        /// This function validates whether the logged in user is an Affinity partner user or not
        /// </summary>
        /// <returns></returns>
        private bool IsAffinityPartnerUser()
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                return token.Scope.Contains(Extensions.GetDescription(ScopeType.AffinityPortal));

            }
            catch (InvalidUserException exception)
            {
                throw new InvalidUserException(exception.Message);
            }
            catch (Exception)
            {
                throw new Exception("User not authenticated.");
            }
        }

        /// <summary>
        /// This function decrypts the answer
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        private string DecryptAnswer(IQuestion question)
        {
            if (question.IsSensitive == true && string.IsNullOrEmpty(question.Answer) == false)
            {
                question.Answer = EncryptionService.Decrypt<string>(question.Answer);
            }

            return question.Answer;
        }


        /// <summary>
        /// This function will just loop through all the QuestionSections. If it finds the question in any of the QuestionSection it will return the sequence number.
        /// </summary>
        /// <param name="subSectionToUpdate"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        private int GetQuestionSectionSeqNoForAQuestion(ISubSection subSectionToUpdate, string questionId)
        {
            var questionSectionSeqNo = 0;

            var questionSections = subSectionToUpdate.QuestionSections;
            IQuestion questionToUpdate = new Question();

            for (var i = 0; i < questionSections.Count; i++)
            {
                //fetch the question section of the question
                questionToUpdate = ValidateQuestionExistenceInTheQuestionSection(questionSections[i], questionId);
                if (questionToUpdate != null)
                {
                    questionSectionSeqNo = questionSections[i].SeqNo;
                    break;
                }
            }
            return questionSectionSeqNo;
        }

        /// <summary>
        /// This function validates the answer, based on the question type, Available answer list.
        /// </summary>
        /// <param name="questionToUpdate"></param>
        /// <param name="answer"></param>
        /// <returns></returns>

        private bool ValidateAnswer(IQuestion questionToUpdate, string answer)
        {
            //Here answer will be validated.
            //It will be validated agianst the Data type and the 'AvailableAns'

            //Validate the Data Type
            var answerDataType = questionToUpdate.QuestionType;

            //Validate the answer if it is a Date type
            if (answerDataType == QuestionType.Date && ParseString(answer) == dataType.System_DateTime)
            {
                return true;
            }

            if (answerDataType == QuestionType.Date && string.IsNullOrEmpty(answer))
            {
                //if the answer is passed as blank then also it is a valid answer.
                return true;
            }

            if (answerDataType == QuestionType.TextCurr && (ParseString(answer) == dataType.System_Int32 || ParseString(answer) == dataType.System_Int64 || ParseString(answer) == dataType.System_Double))
            {
                return true;
            }

            if (answerDataType == QuestionType.TextCurrIncome && (ParseString(answer) == dataType.System_Int32 || ParseString(answer) == dataType.System_Int64 || ParseString(answer) == dataType.System_Double))
            {
                return true;
            }

            if ((answerDataType == QuestionType.TextCurrIncome || answerDataType == QuestionType.TextCurr || answerDataType == QuestionType.TextNum || answerDataType == QuestionType.TextNum2 || answerDataType == QuestionType.TextNum) && (ParseString(answer) == dataType.System_Int32 || ParseString(answer) == dataType.System_Int64))
            {
                return true;
            }

            if (answerDataType == QuestionType.TextNum && (ParseString(answer) == dataType.System_Int32 || ParseString(answer) == dataType.System_Int64))
            {
                return true;
            }

            if (answerDataType == QuestionType.TextNum2 && (ParseString(answer) == dataType.System_Int32 || ParseString(answer) == dataType.System_Int64))
            {
                return true;
            }

            if (answerDataType == QuestionType.TextEmail && ParseString(answer) == dataType.System_Email)
            {
                return true;
            }

            if (answerDataType == QuestionType.SignUpEmail && ParseString(answer) == dataType.System_Email)
            {
                return true;
            }

            if (answerDataType == QuestionType.SignUpEmailEdit && ParseString(answer) == dataType.System_Email)
            {
                return true;
            }

            if (answerDataType == QuestionType.Text || answerDataType == QuestionType.TextPhone || 
                answerDataType == QuestionType.TextSSN || answerDataType == QuestionType.TextYear || 
                answerDataType == QuestionType.TextMonth || answerDataType == QuestionType.TextDay || 
                answerDataType == QuestionType.Address || answerDataType == QuestionType.BusinessAddress || 
                answerDataType == QuestionType.Null || answerDataType == QuestionType.TextPerc || 
                answerDataType == QuestionType.LoanCalculator || answerDataType == QuestionType.FinancialInstitution || 
                answerDataType == QuestionType.SingleToggle || answerDataType == QuestionType.SingleToggleLoan || answerDataType == QuestionType.MultiToggle  || answerDataType == QuestionType.MultiToggleButton ||
                answerDataType == QuestionType.SelectOne || answerDataType == QuestionType.CheckBox || 
                answerDataType == QuestionType.TextArea || answerDataType == QuestionType.Validator || 
                answerDataType == QuestionType.IncomeSelect || answerDataType == QuestionType.LoanOfficer || 
                answerDataType == QuestionType.RadioBox || answerDataType == QuestionType.RadioBoxContainer || 
                answerDataType == QuestionType.TreeView || answerDataType == QuestionType.StartEndDate || 
                answerDataType == QuestionType.RadioTreeView || answerDataType == QuestionType.SingleChoice ||
                answerDataType == QuestionType.TotalAmountCalculator || answerDataType == QuestionType.TextCurrRight ||
                answerDataType == QuestionType.SelectState ||
                answerDataType == QuestionType.AddCoBorrower || 
                answerDataType == QuestionType.MoneyMonth) //No other verification is needed if it is "Text" && ParseString(answer) == dataType.System_String)
            {
                return true;
            }

            //Validate answer if it is a Select type
            if (answerDataType == QuestionType.Select || answerDataType == QuestionType.SelectIcon || answerDataType == QuestionType.SelectBool || answerDataType == QuestionType.DeclarationSelectBool || answerDataType == QuestionType.SelectBoolMarital) 
            {
                // If the data type is "Select" then make sure the Answer given by the user should be in 'Available Ans'
                if (questionToUpdate.AvailableAns != null)
                {
                    for (var i = 0; i <= questionToUpdate.AvailableAns.Count - 1; i++)
                    {
                        if (answerDataType == QuestionType.SelectBool && questionToUpdate.AvailableAns[i].Answer.ToLower() == answer.ToLower())
                        {
                            return true;
                        }

                        if (answerDataType == QuestionType.DeclarationSelectBool && questionToUpdate.AvailableAns[i].Answer.ToLower() == answer.ToLower())
                        {
                            return true;
                        }

                        if (answerDataType == QuestionType.SelectBoolMarital && questionToUpdate.AvailableAns[i].Answer.ToLower() == answer.ToLower())
                        {
                            return true;
                        }

                        if (questionToUpdate.AvailableAns[i].Answer == answer)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private List<RequiredQuestionDetails> GetEmploymentStartEndDateValidation(ISection section, IUserAccessedStatus userStatus)
        {
            var employmentRequiredQuestionList = new List<RequiredQuestionDetails>();
            var employmentSubSections = section.SubSections
                                .Where(ss => ss.TemplateFieldName == CURRENTEMPLOYMENT || ss.TemplateFieldName == EMPLOYMENT || ss.TemplateFieldName == BUSINESS || ss.TemplateFieldName == BUSINESSANOTHER).ToList();

            var strStartDate = string.Empty;
            var strEndDate = string.Empty; 
//            bool isEmploymentSubSectionAvailable = false;
            var subSectionId = string.Empty;

            if (employmentSubSections != null)
            {
                 foreach (var employmentSubSection in employmentSubSections)
                {
                    //if (!string.IsNullOrWhiteSpace(subSectionId))
                     subSectionId = employmentSubSection.SubSectionId;

                    var borrowerCurrentlyWorkQuestion = employmentSubSection.QuestionSections.SelectMany(ss => ss.QuestionList).Where(ql => ql.QuestionFieldName == "BorrowerCurrentlyWork").FirstOrDefault();
                    if (borrowerCurrentlyWorkQuestion!=null && borrowerCurrentlyWorkQuestion.Answer!=null && borrowerCurrentlyWorkQuestion.Answer.ToLower() == "false")
                    {
                        var startEndDateTypeAnswer = employmentSubSection.QuestionSections.SelectMany(ss => ss.QuestionList).Where(ql => ql.QuestionId == "1002").FirstOrDefault().Answer;
                          //// Remove Start Date question 
                        var startDateQuestion = employmentSubSection.QuestionSections.SelectMany(x=>x.QuestionList)
                                                .FirstOrDefault(y=> (y.QuestionFieldName == "BorrowerEmployerStartDate" || y.QuestionFieldName == "BorrowerBusinessStartDate"));
                        foreach (QuestionSection qs in employmentSubSection.QuestionSections)
                        {
                            qs.QuestionList.Remove(startDateQuestion);
                        }

                        if (!string.IsNullOrWhiteSpace(startEndDateTypeAnswer))
                        {
                            var startEndDateObject =  JsonConvert.DeserializeObject<StartEndDate>(startEndDateTypeAnswer);;
                            strStartDate = startEndDateObject != null ? startEndDateObject.StartDate : string.Empty;
                            strEndDate  = startEndDateObject != null ?  startEndDateObject.EndDate : string.Empty;
                        }

                        var strValidationMessage = new System.Text.StringBuilder();
                        if(string.IsNullOrWhiteSpace(strStartDate) && string.IsNullOrWhiteSpace(strEndDate))
                        {
                            var startEndDateString = "StartDate and EndDate";
                            strValidationMessage.AppendLine(startEndDateString);
                        }
                        else if(string.IsNullOrWhiteSpace(strStartDate))
                        {
                            var startDateString = "StartDate";
                            strValidationMessage.AppendLine(startDateString);
                        }
                        else if(string.IsNullOrWhiteSpace(strEndDate))
                        {
                            var endDateString = "EndDate";
                            strValidationMessage.AppendLine(endDateString);
                        }
                        else
                        {
                            continue;
                        }
                        
                      

                        var employmentRequiredQuestion = new RequiredQuestionDetails()
                        {
                                QuestionId = "1002",
                                QuestionText = strValidationMessage.ToString(),
                                SubSectionId = subSectionId,
                                SectionId = section.SectionId,
                                HighestSectionReached = userStatus.HighestSectionReached,
                                HighestSubSectionReached = userStatus.HighestSubSectionReached
                        };

                        employmentRequiredQuestionList.Add(employmentRequiredQuestion);
                        subSectionId = string.Empty; 
                    }
                }
                return employmentRequiredQuestionList;    
            }
            return null;
        }

         private RequiredQuestionDetails GetMissingCoBorrowerQustionList(List<QuestionnaireSummaryConfig> questionnaireSummaryFieldMap,ISection section, IUserAccessedStatus userStatus)
         {     
                 RequiredQuestionDetails missingCoBorrowerRequiredQuestion = null;
                var summaryItem = questionnaireSummaryFieldMap.Where(x => x.SectionId == section.SectionId && x.SubSectionId == ConstantUtils.JointCreditSubSectionId).FirstOrDefault();
                if(summaryItem != null)
                {
                    var subSections = section.SubSections.FirstOrDefault(x => x.SubSectionId == ConstantUtils.JointCreditSubSectionId);
                    if(subSections != null)
                    {
                        string maritalStatusAnswer = string.Empty ,AddSpouseAsCoBorrowerAnswer= string.Empty,AddAnotherCoBorrowerAnswer = string.Empty;
                         bool isMissingCoBorrowerPopUpDisplay = false,isOnlyCoborrower = false;
                        var maritalStatusQuestion = subSections.QuestionSections
                            .SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["MaritalStatusJoint"].QuestionId);            

                        if (maritalStatusQuestion != null)
                        {
                            maritalStatusAnswer = maritalStatusQuestion.Answer;
                        }
                        
                        if(!string.IsNullOrEmpty(maritalStatusAnswer))
                        {
                            //get add spouse as a co-borrower question
                            var AddSpouseAsCoBorrowerQuestion = subSections.QuestionSections
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["AddSpouseAsCoBorrower"].QuestionId);                

                            if (AddSpouseAsCoBorrowerQuestion != null)
                            {
                                AddSpouseAsCoBorrowerAnswer = AddSpouseAsCoBorrowerQuestion.Answer;
                            }

                            //get add another as co-borrower question
                            var AddAnotherCoBorrowerQuestion = subSections.QuestionSections
                                .SelectMany(qs => qs.QuestionList)
                                .FirstOrDefault(q => q.QuestionId == summaryItem.QuestionInformation["AddAsCoBorrower"].QuestionId);                

                            if (AddAnotherCoBorrowerQuestion != null)
                            {
                                AddAnotherCoBorrowerAnswer = AddAnotherCoBorrowerQuestion.Answer;
                            }

                            if(maritalStatusAnswer.ToLower() == ConstantUtils.Married.ToLower() && AddSpouseAsCoBorrowerAnswer == "true" && AddAnotherCoBorrowerAnswer == "true")
                            {
                               isMissingCoBorrowerPopUpDisplay = true;
                               isOnlyCoborrower = false;
                               missingCoBorrowerRequiredQuestion =  ValidateAddCoBorrowerSection(section,isMissingCoBorrowerPopUpDisplay,userStatus,isOnlyCoborrower);
                            }
                            else  if(maritalStatusAnswer.ToLower() == ConstantUtils.Married.ToLower() && AddSpouseAsCoBorrowerAnswer == "false")
                            {
                               isMissingCoBorrowerPopUpDisplay = true;
                               isOnlyCoborrower = true;
                               missingCoBorrowerRequiredQuestion =  ValidateAddCoBorrowerSection(section,isMissingCoBorrowerPopUpDisplay,userStatus,isOnlyCoborrower);
                            }
                            else if(maritalStatusAnswer.ToLower() == ConstantUtils.Unmarried.ToLower() || maritalStatusAnswer.ToLower() == ConstantUtils.Separated.ToLower())
                            {
                               isMissingCoBorrowerPopUpDisplay = true;
                               isOnlyCoborrower = true;
                               missingCoBorrowerRequiredQuestion = ValidateAddCoBorrowerSection(section,isMissingCoBorrowerPopUpDisplay,userStatus,isOnlyCoborrower);
                            }
                        }
                    } 
                }
                
                return missingCoBorrowerRequiredQuestion;
         }


         private RequiredQuestionDetails ValidateAddCoBorrowerSection(ISection section,bool isMissingCoBorrowerPopUpDisplay,IUserAccessedStatus userStatus,bool isOnlyCoborrower)
         {
             //get add co-borrower sub section //Validate for Coborrower

            RequiredQuestionDetails missingCoBorrowerRequiredQuestion = null;
            var coBorrowerInvitationSubSection = section.SubSections.FirstOrDefault(x => x.TemplateFieldName != null && x.TemplateFieldName.Equals(SectionConstants.CoBorrowerInvitationTemplateName));
            if(coBorrowerInvitationSubSection != null)
            {
                //this is the add co-borrower sub section.
                var questions = coBorrowerInvitationSubSection.QuestionSections.Where(x=>x.QuestionSectionName == ConstantUtils.QuestionSectionNameAddCoBorrower).SelectMany(x=>x.QuestionList);
                //var questionSections = coBorrowerInvitationSubSection.QuestionSections.Where(x=>x.QuestionSectionName == "AddCoBorrower").ToList();
                if(questions != null)
                {
                    //check any invitatin is sent to co-borrower.
                    //resultQuestionId                                        
                    bool isInvitationSent = questions.Any(x => x.ResultQuestionId != null);

                    if(!isInvitationSent)
                    {     
                        var question = questions.FirstOrDefault(x => x.ResultQuestionId == null);                  
                        //add missing condition
                        missingCoBorrowerRequiredQuestion = new RequiredQuestionDetails();
                        missingCoBorrowerRequiredQuestion.QuestionId = question!=null ? question.QuestionId : string.Empty;
                        missingCoBorrowerRequiredQuestion.QuestionText = QuestionnaireConfigurations.MissingCoBorrowerText;

                            missingCoBorrowerRequiredQuestion.SectionId = section.SectionId;
                            missingCoBorrowerRequiredQuestion.SubSectionId = coBorrowerInvitationSubSection.SubSectionId;
                            missingCoBorrowerRequiredQuestion.HighestSectionReached = userStatus.HighestSectionReached;
                            missingCoBorrowerRequiredQuestion.HighestSubSectionReached = userStatus.HighestSubSectionReached;
                            missingCoBorrowerRequiredQuestion.IsEmploymentGap = false;
                            if(isMissingCoBorrowerPopUpDisplay)
                            {
                                if(!isOnlyCoborrower)
                                {
                                    missingCoBorrowerRequiredQuestion.IsMissingCoBorrowerWithSpouse = isOnlyCoborrower;
                                    missingCoBorrowerRequiredQuestion.IsMissingCoBorrower = isMissingCoBorrowerPopUpDisplay;
                                }
                                else {
                                    missingCoBorrowerRequiredQuestion.IsMissingCoBorrowerWithSpouse = isMissingCoBorrowerPopUpDisplay;
                                }
                                
                                missingCoBorrowerRequiredQuestion.EmployementGapPopupText="";
                                missingCoBorrowerRequiredQuestion.EmployementGapPopupTitle = QuestionnaireConfigurations.MissingCoBorrowerTitle;
                                missingCoBorrowerRequiredQuestion.EmploymentGapPayload = null;
                                missingCoBorrowerRequiredQuestion.EmploymentGapPayload = new EmploymentGapDetails();
                                missingCoBorrowerRequiredQuestion.EmploymentGapPayload.SectionId = section.SectionId;
                                missingCoBorrowerRequiredQuestion.EmploymentGapPayload.SubSectionId = coBorrowerInvitationSubSection.SubSectionId;
                                missingCoBorrowerRequiredQuestion.EmploymentGapPayload.QuestionId = question !=null ? question.QuestionId : string.Empty;
                                missingCoBorrowerRequiredQuestion.EmploymentGapPayload.QuestionSectionSeqNo = coBorrowerInvitationSubSection.SeqNo.Value;
                            }
                    }
                }
            }

            return missingCoBorrowerRequiredQuestion;
         }
        private RequiredQuestionDetails GetEmploymentDateQuestionsList(ISection section, IUserAccessedStatus userStatus, string sectionName = null)
        {
            RequiredQuestionDetails employmentRequiredQuestion = null;
            List<ISubSection> employmentSubSections = null;
            var summaryEmpGapMessage = string.Empty;
            var empGapTitle = string.Empty;
            string questionFieldNameToMatch = string.Empty;
            string sectionFieldNameToMatch = string.Empty;

            if(sectionName == Constant.EmploymentSection)
            {
                summaryEmpGapMessage = QuestionnaireConfigurations.EmploymentGapMessage;
                empGapTitle = QuestionnaireConfigurations.EmploymentGapTitle;
                 employmentSubSections = section.SubSections
                                .Where(ss => ss.TemplateFieldName == CURRENTEMPLOYMENT || ss.TemplateFieldName == EMPLOYMENT)
                                .OrderBy(x=>x.SeqNo)
                                .ToList();
                                
                questionFieldNameToMatch  = Constant.BorrowerEmploymentGap;
                sectionFieldNameToMatch = Constant.EmploymentGapSection;
            }
            else
            {
                summaryEmpGapMessage = QuestionnaireConfigurations.SelfEmploymentGapMessage;
                empGapTitle = QuestionnaireConfigurations.SelfEmploymentGapTitle;
                employmentSubSections = section.SubSections
                                .Where(ss => ss.TemplateFieldName == BUSINESS || ss.TemplateFieldName == BUSINESSANOTHER)
                                .OrderBy(x=>x.SeqNo)
                                .ToList();
                
                questionFieldNameToMatch  = Constant.BorrowerSelfEmploymentGap;
                sectionFieldNameToMatch = Constant.SelfEmploymentGapSection;
            }

            var employmentGapQuestion = employmentSubSections.SelectMany(ss => ss.QuestionSections)
                                             .Where(x => x.QuestionList != null)
                                             .SelectMany(qs => qs.QuestionList)
                                             .FirstOrDefault(q => q.QuestionFieldName == questionFieldNameToMatch);

            int questionSectionSeqNoForGap = employmentSubSections.SelectMany(ss => ss.QuestionSections)
                                            .Where(sec => sec.QuestionSectionName == sectionFieldNameToMatch)
                                            .Select(x => x.SeqNo).FirstOrDefault();


            if (employmentSubSections != null)
            {
                var subSectionId = string.Empty;
                var questionId = string.Empty;
                var dtStartDatePeriod = DateTime.UtcNow.AddMonths(-24);
                dtStartDatePeriod = dtStartDatePeriod.AddDays(1 - dtStartDatePeriod.Day);
                var dtEndDatePeriod = DateTime.UtcNow;
                dtEndDatePeriod = new DateTime(dtEndDatePeriod.Year, dtEndDatePeriod.Month, DateTime.DaysInMonth(dtEndDatePeriod.Year, dtEndDatePeriod.Month));
                var employmentDateQuestionsList = new List<DateTime>();
                var isEmploymentSubSectionAvailable = false;
                var employmentGapIndex = 0;

                #region "Add all date range in list
                foreach (var employmentSubSection in employmentSubSections)
                {
                    if (!isEmploymentSubSectionAvailable && (employmentSubSection.TemplateFieldName == CURRENTEMPLOYMENT || employmentSubSection.TemplateFieldName == EMPLOYMENT))
                    {
                        subSectionId = employmentSubSection.SubSectionId;
                        isEmploymentSubSectionAvailable = true;
                    }

                    if (string.IsNullOrWhiteSpace(subSectionId))
                        subSectionId = employmentSubSection.SubSectionId;

                    var borrowerCurrentlyWorkQuestion = employmentSubSection.QuestionSections.SelectMany(ss => ss.QuestionList).Where(ql => ql.QuestionFieldName == "BorrowerCurrentlyWork").FirstOrDefault();

                    if (borrowerCurrentlyWorkQuestion != null)
                    {
                        var strStartDate = string.Empty;
                        var strEndDate = string.Empty;
                       
                        if (borrowerCurrentlyWorkQuestion.Answer.ToLower() == "true")
                        {
                            if(employmentGapIndex == 0)
                            {
                                if (employmentSubSection.TemplateFieldName == CURRENTEMPLOYMENT || employmentSubSection.TemplateFieldName == EMPLOYMENT)
                                    questionId = employmentSubSection.QuestionSections
                                            .Where(x => x.QuestionList != null).SelectMany(ss => ss.QuestionList)
                                            .Where(ql => ql.QuestionFieldName == Constant.BorrowerEmployerStartDate)
                                            .Select(x => x.QuestionId).FirstOrDefault();
                                else    
                                    questionId = employmentSubSection.QuestionSections
                                            .Where(x => x.QuestionList != null).SelectMany(ss => ss.QuestionList)
                                            .Where(ql => ql.QuestionFieldName == Constant.BorrowerBusinessStartDate)
                                            .Select(x => x.QuestionId).FirstOrDefault();

                            }

                            //string strStartDate = string.Empty;
                            if (employmentSubSection.TemplateFieldName == CURRENTEMPLOYMENT || employmentSubSection.TemplateFieldName == EMPLOYMENT)
                                strStartDate = employmentSubSection.QuestionSections.SelectMany(ss => ss.QuestionList).Where(ql => ql.QuestionFieldName == "BorrowerEmployerStartDate").FirstOrDefault().Answer;
                            else
                                strStartDate = employmentSubSection.QuestionSections.SelectMany(ss => ss.QuestionList).Where(ql => ql.QuestionFieldName == "BorrowerBusinessStartDate").FirstOrDefault().Answer;
                            
                            if (DateTime.UtcNow.Month < 10)
                                strEndDate = "0" + DateTime.UtcNow.Month.ToString() + DateTime.UtcNow.Year.ToString();
                            else
                                strEndDate = DateTime.UtcNow.Month.ToString() + DateTime.UtcNow.Year.ToString();
                        }
                        else
                        {
                            if(employmentGapIndex == 0)
                            {
                                if (employmentSubSection.TemplateFieldName == CURRENTEMPLOYMENT || employmentSubSection.TemplateFieldName == EMPLOYMENT)
                                     questionId = employmentSubSection.QuestionSections
                                            .Where(x => x.QuestionList != null).SelectMany(ss => ss.QuestionList)
                                            .Where(ql => ql.QuestionFieldName == Constant.BorrowerEmployerEndDateFalse)
                                            .Select(x => x.QuestionId).FirstOrDefault();
                                else
                                     questionId = employmentSubSection.QuestionSections
                                            .Where(x => x.QuestionList != null).SelectMany(ss => ss.QuestionList)
                                            .Where(ql => ql.QuestionFieldName == Constant.BorrowerBusinessEndDateFalse)
                                            .Select(x => x.QuestionId).FirstOrDefault();
                            }
                               

                            var startEndDateTypeAnswer = employmentSubSection.QuestionSections.SelectMany(ss => ss.QuestionList).Where(ql => ql.QuestionId == "1002").FirstOrDefault().Answer;

                            if (!string.IsNullOrWhiteSpace(startEndDateTypeAnswer))
                            {
                                var startEndDateObject =  JsonConvert.DeserializeObject<StartEndDate>(startEndDateTypeAnswer);;
                                strStartDate = startEndDateObject != null ? startEndDateObject.StartDate : string.Empty;
                                strEndDate  = startEndDateObject != null ?  startEndDateObject.EndDate : string.Empty;
                            }
                            
                        }

                        if (!string.IsNullOrWhiteSpace(strStartDate) && !string.IsNullOrWhiteSpace(strEndDate))
                        {
                            var startDate = new DateTime(Convert.ToInt16(strStartDate.Substring(2, 4)), Convert.ToInt16(strStartDate.Substring(0, 2)), 1);
                            var days = DateTime.DaysInMonth(Convert.ToInt16(strEndDate.Substring(2, 4)), Convert.ToInt16(strEndDate.Substring(0, 2)));
                            var endDate = new DateTime(Convert.ToInt16(strEndDate.Substring(2, 4)), Convert.ToInt16(strEndDate.Substring(0, 2)), days);

                            while (startDate.Year < endDate.Year || (startDate.Year == endDate.Year && startDate.Month <= endDate.Month))
                            {
                                if (!employmentDateQuestionsList.Any(d => d.Year == startDate.Year && d.Month == startDate.Month))
                                    if (startDate.Date >= dtStartDatePeriod.Date)
                                        employmentDateQuestionsList.Add(startDate);
                                startDate = startDate.AddMonths(1);
                            }
                        }
                        employmentGapIndex ++;
                    }
                }
                #endregion
                
                if (employmentDateQuestionsList.Count() > 0)
                {
                    var missingMonths = Extensions.GetMissingMonths(employmentDateQuestionsList, dtStartDatePeriod, dtEndDatePeriod);

                    if (missingMonths != null && missingMonths.Count > 0)
                    {
                        var strValidationMessage = new System.Text.StringBuilder();
                        strValidationMessage.AppendLine(summaryEmpGapMessage);
                        var strDateValidationsForPopup = new System.Text.StringBuilder();

                        string startDateString = string.Empty, endDateString = string.Empty;
                        var counter = 1;

                        for (var i = 0; i < missingMonths.Count; i++)
                        {
                            if (string.IsNullOrEmpty(startDateString))
                                startDateString = counter + ". " + missingMonths[i].Month + "/" + missingMonths[i].Year;

                            if (i > 0 && i < missingMonths.Count - 1)
                                if (Extensions.GetMonthDifference(missingMonths[i], missingMonths[i + 1]) > 1)
                                    endDateString = " to " + missingMonths[i].Month + "/" + missingMonths[i].Year;

                            if (i == missingMonths.Count - 1)
                                endDateString = " to " + missingMonths[i].Month + "/" + missingMonths[i].Year;

                            if (!string.IsNullOrWhiteSpace(startDateString) && !string.IsNullOrWhiteSpace(endDateString))
                            {
                                strValidationMessage.AppendLine("\t" + startDateString + endDateString);
                                strDateValidationsForPopup.AppendLine("\t" + startDateString + endDateString);
                                counter++;
                                startDateString = endDateString = string.Empty;
                            }
                        }

                        employmentRequiredQuestion = new RequiredQuestionDetails();
                        employmentRequiredQuestion.QuestionId = questionId;
                        employmentRequiredQuestion.QuestionText = strValidationMessage.ToString();
                        employmentRequiredQuestion.SubSectionId = subSectionId;
                        employmentRequiredQuestion.SectionId = section.SectionId;
                        employmentRequiredQuestion.HighestSectionReached = userStatus.HighestSectionReached;
                        employmentRequiredQuestion.HighestSubSectionReached = userStatus.HighestSubSectionReached;
                        employmentRequiredQuestion.IsEmploymentGap = true;
                        employmentRequiredQuestion.EmployementGapPopupText = strDateValidationsForPopup.ToString();
                        employmentRequiredQuestion.EmployementGapPopupTitle = empGapTitle;
                        employmentRequiredQuestion.EmploymentGapPayload = new EmploymentGapDetails();
                        employmentRequiredQuestion.EmploymentGapPayload.SectionId = section.SectionId;
                        employmentRequiredQuestion.EmploymentGapPayload.SubSectionId = subSectionId;
                        employmentRequiredQuestion.EmploymentGapPayload.QuestionId = employmentGapQuestion.QuestionId;
                        employmentRequiredQuestion.EmploymentGapPayload.QuestionSectionSeqNo = questionSectionSeqNoForGap;
                    }
                }
            }

            return employmentRequiredQuestion;
        }

        // this function returns all required questions on summary which are left blank.
        /// <summary>
        /// This function returns the list of question which were skipped from answering.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="borrowerList"></param>
        /// <returns></returns>
        private async Task<Dictionary<string, List<IRequiredQuestionDetails>>> GetValidationsOnSummary(string temporaryApplicationNumber, List<IBorrowerCoBorrowerSpouseInfo> borrowerList = null,IQuestionnaire questionnaire=null)
        {
            var userValidations = new Dictionary<string, List<IRequiredQuestionDetails>>();

            if (questionnaire == null)
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (borrowerList == null)
                borrowerList = await GetBorrowersAndCoBorrowersAndSpouseName(temporaryApplicationNumber, false);
            var loanPurposeQuestion =questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == Convert.ToInt32(SectionDetails.Profile)).FirstOrDefault()
                .SubSections.Where(ss => ss.SubSectionId == "SS1").FirstOrDefault()
                .QuestionSections.Where(q => q.QuestionSectionName.ToLower() == "purpose").FirstOrDefault()
                .QuestionList.Where(q => q.QuestionId == "-1").FirstOrDefault();
            var loanPurposeAnswer = loanPurposeQuestion.Answer;

            var loggedInUser = await GetCurrentUser();
            List<IRequiredQuestionDetails> reqQuestionDetails = null;
            RequiredQuestionDetails employmentRequiredQuestion = null;
            RequiredQuestionDetails selfEmploymentRequiredQuestion = null;
            RequiredQuestionDetails missingCoBorrowerRequiredQuestion = null;
            var alimonySupporttemplate = "alimony";
            var parentPerMonthQuestion  = "BorrowerBusinessMonthlyIncomeAmount";
            var currentEmploymentTemplate = "currentEmployment";
            var employmentTemplate = "employment";
            var parentOptionalQuestion = "BorrowerOtherOptionalIncome";

            if (borrowerList.Any())
            {
                var questionnaireCopy = Extensions.Clone(questionnaire);
                 var questionnaireSummaryFieldMap = Extensions.Clone<List<QuestionnaireSummaryConfig>>(QuestionnaireConfigurations.ApplicationSummary.Map);
                foreach (var user in borrowerList)
                {
                    var excludeSpouseValidation = false; // flag to hide validation for spouse communication and home phone
                    if(user.UserName != loggedInUser && (user.ApplicantType == ApplicantType.Spouse || user.ApplicantType == ApplicantType.CoBorrowerSpouse))
                    {
                        excludeSpouseValidation = true;
                    }

                    var userStatus = questionnaire.UserState.Where(u => u.UserName == loggedInUser).FirstOrDefault();
                    reqQuestionDetails = new List<IRequiredQuestionDetails>();

                    var summarySectionId = Convert.ToInt32(SectionDetails.Summary);

                    foreach (var section in questionnaire.ApplicationForm.Sections)
                    {
                        bool incomeSectionEvaluated = false;
                        var requiredChildQnList = new List<IChildConditionalQuestion>();
                        if (section.SectionId >= summarySectionId)
                            break;
                        
                        if (user.ApplicantType == ApplicantType.Borrower)
                            section.SubSections = section.SubSections.Where(x => x.UserName == user.UserName || x.isDefault).ToList();
                        else
                            section.SubSections = section.SubSections.Where(x => x.UserName == user.UserName).ToList();

                        #region "Start : Adding logic to check employment and self employment dates to check whether 24 months continuous experience is added or not."
                        List<RequiredQuestionDetails> employmentStartEndDateRequiredQuestionList = null;
                        if (section.SectionId == 3)
                        {   
                            incomeSectionEvaluated = true;
                            employmentRequiredQuestion = GetEmploymentDateQuestionsList(section, userStatus, Constant.EmploymentSection);
                            selfEmploymentRequiredQuestion = GetEmploymentDateQuestionsList(section, userStatus, Constant.BusineeSelfEmploymentSection);
                            employmentStartEndDateRequiredQuestionList = GetEmploymentStartEndDateValidation(section, userStatus);
                        }
                        #endregion "End : Adding logic to check employment and self employment dates to check whether 24 months continuous experience is added or not."

                        #region "Logic for catch missing co-borrower functionality"
                        if(section.SectionId == Convert.ToInt32(SectionDetails.Profile))
                        {
                           missingCoBorrowerRequiredQuestion = GetMissingCoBorrowerQustionList(questionnaireSummaryFieldMap,section,userStatus);
                        }
                        #endregion

                        #region Get the child question list which are visible in the front end based on the answer
                        var childQuestionList = section.SubSections
                                            .SelectMany(x => x.QuestionSections)
                                            .SelectMany(x => x.QuestionList)
                                            .Where(x => x.ChildQuestions != null && x.ChildQuestions.Any(a => string.Equals(a.Answer, x.Answer, StringComparison.InvariantCultureIgnoreCase)));
                        var visibleChildQuestions = new List<IChildConditionalQuestion>();
                        foreach (var question in childQuestionList)
                        {
                            visibleChildQuestions.AddRange(question.ChildQuestions.Where(a => string.Equals(a.Answer, question.Answer, StringComparison.InvariantCultureIgnoreCase)));
                        }
                        #endregion
                        foreach (var subSection in section.SubSections)
                        {
                            if(!string.IsNullOrEmpty(subSection.TemplateFieldName) && subSection.TemplateFieldName.Equals("employment"))
                            {
                                requiredChildQnList = new List<IChildConditionalQuestion>();
                            }
                            var parentQnList = subSection.QuestionSections
                                                     .SelectMany(x => x.QuestionList)
                                                     .Where(x => (x.IsParentQuestion || x.QuestionId == "-1")
                                                                 && x.QuestionType != QuestionType.Null
                                                                 && x.QuestionType != QuestionType.Label
                                                                 && x.ChildQuestions != null
                                                                 && x.ChildQuestions.Any()).ToList();

                            if (parentQnList != null && parentQnList.Any())
                            {
                                foreach (var parentQn in parentQnList)
                                {
                                    var parentAnswer = parentQn != null ? parentQn.Answer.ToLower() : null;
                                    var childQnList = parentQn.ChildQuestions.Where(x => x.Answer == parentQn.Answer ||
                                                                                        (!String.IsNullOrEmpty(x.Answer) && x.Answer.ToLower() == parentAnswer)).ToList();

                                    if(subSection.TemplateFieldName == alimonySupporttemplate && parentQn.QuestionFieldName == parentPerMonthQuestion){
                                        if((!string.IsNullOrEmpty(parentQn.Answer) && Convert.ToInt32(parentQn.Answer) > 0))
                                        {
                                                 childQnList = parentQn.ChildQuestions.Where(x => string.IsNullOrEmpty(x.Answer)).ToList();
                                        }
                                    }

                                    if(parentQn.QuestionFieldName == parentOptionalQuestion && (subSection.TemplateFieldName == employmentTemplate || subSection.TemplateFieldName == currentEmploymentTemplate))
                                    {
                                        childQnList = parentQn.ChildQuestions.Where(x => !string.IsNullOrWhiteSpace(parentAnswer) && parentAnswer.Contains(x.Answer.ToLower())).ToList();
                                    }

                                    requiredChildQnList.AddRange(childQnList);
                                    var isRootQuestionReached = false;

                                    while (!isRootQuestionReached)
                                    {
                                        var childQuestions = await GetChildQuestions(childQnList, questionnaire);

                                        if (childQuestions != null && childQuestions.Any())
                                        {
                                            requiredChildQnList.AddRange(childQuestions);
                                            childQnList = childQuestions;
                                        }
                                        else
                                            isRootQuestionReached = true;
                                    }
                                }
                            }

                        //var questionnaireSummaryFieldMap = Extensions.Clone<List<QuestionnaireSummaryConfig>>(QuestionnaireConfigurations.ApplicationSummary.Map);
           
                        ////VA select options other text value handle
                         var vaInfo = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType == "VA/Military Loan");
                        var vaQuestionId = new List<string>();

                        if(vaInfo.QuestionInformation["SelectOptions"] != null)
                            vaQuestionId.Add(vaInfo.QuestionInformation["SelectOptions"].QuestionId);

                        var employmentSectionConfig = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType.ToLower() == Constant.EmploymentSection.ToLower());

                        var rentalIncomeInfo = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType == "Rental Income");

                         var currenctlyOwnProperty = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType == "Current Properties Own");

                         var alimonyChildSupport = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType.ToLower() == Constant.AlimonyChildSupportSubSection.ToLower());
                        
                        var declarationInfo = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType == "declarations");
                        var ethnicityRaceQuestionId = new List<string>();
                        var propertyOwnAndTitleHold = new List<string>(); 

                        if(declarationInfo.QuestionInformation["Ethnicity"] != null)
                            ethnicityRaceQuestionId.Add(declarationInfo.QuestionInformation["Ethnicity"].QuestionId);

                        if(declarationInfo.QuestionInformation["Race"] != null)
                            ethnicityRaceQuestionId.Add(declarationInfo.QuestionInformation["Race"].QuestionId);

                         if(declarationInfo.QuestionInformation["Sex"] != null)
                            ethnicityRaceQuestionId.Add(declarationInfo.QuestionInformation["Sex"].QuestionId);
                       
                       
                        if(declarationInfo.QuestionInformation["EthnicityOther"] != null)
                             ethnicityRaceQuestionId.Add(declarationInfo.QuestionInformation["EthnicityOther"].QuestionId);
                       
                        if(declarationInfo.QuestionInformation["RaceOther"] != null)
                        ethnicityRaceQuestionId.Add(declarationInfo.QuestionInformation["RaceOther"].QuestionId);

                          if(declarationInfo.QuestionInformation["SexOther"] != null)
                            ethnicityRaceQuestionId.Add(declarationInfo.QuestionInformation["SexOther"].QuestionId);
                       

                          if(declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwn"] != null)
                            propertyOwnAndTitleHold.Add(declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwn"].QuestionId);
                       

                          if(declarationInfo.QuestionInformation["BorrowerHoldTitle"] != null)
                            propertyOwnAndTitleHold.Add(declarationInfo.QuestionInformation["BorrowerHoldTitle"].QuestionId);
                          
                          if(declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwnOther"] != null)
                            propertyOwnAndTitleHold.Add(declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwnOther"].QuestionId);

                         if(declarationInfo.QuestionInformation["BorrowerHoldTitleOther"] != null)
                            propertyOwnAndTitleHold.Add(declarationInfo.QuestionInformation["BorrowerHoldTitleOther"].QuestionId);
                       
                       
                            var questions = subSection.QuestionSections
                                                                     .Where(x => x.QuestionList != null)
                                                                     .SelectMany(qs => qs.QuestionList)
                                                                     .Where(x => (x.IsParentQuestion || requiredChildQnList.Any(y => y.QuestionId == x.QuestionId && y.SubSectionId == subSection.SubSectionId)
                                                                                || visibleChildQuestions.Any(z=> z.QuestionId == x.QuestionId && z.SubSectionId == subSection.SubSectionId))
                                                                                 && (x.Required.ToLower() == Extensions.GetDescription(RequiredType.True).ToLower() || x.Required.ToLower() == Extensions.GetDescription(RequiredType.Skippable).ToLower())
                                                                                 && x.QuestionType != QuestionType.Null
                                                                                 && x.QuestionType != QuestionType.Label
                                                                                 && x.QuestionType != QuestionType.LabelLink
                                                                                // && !string.IsNullOrWhiteSpace(x.QuestionText)
                                                                                // && (x.Answer == "0" || string.IsNullOrWhiteSpace(x.Answer))
                                                                                 );

                            foreach (var question in questions)
                            {
                                var flag = true;
                                IEnumerable<IQuestion> yearAndMonthQuestionList;

                                if(!string.IsNullOrWhiteSpace(question.QuestionText)  && (question.Answer == "0" || string.IsNullOrWhiteSpace(question.Answer)))
                                {
                                    if(currenctlyOwnProperty.AllowedZeroOrBlankValues != null && currenctlyOwnProperty.AllowedZeroOrBlankValues.Any() && currenctlyOwnProperty.AllowedZeroOrBlankValues.Contains(question.QuestionId))
                                    {
                                        flag = false;
                                    }
                                    else if(rentalIncomeInfo.AllowedZeroOrBlankValues != null && rentalIncomeInfo.AllowedZeroOrBlankValues.Any() && rentalIncomeInfo.AllowedZeroOrBlankValues.Contains(question.QuestionId))
                                    {
                                        flag = false;
                                    }
                                    else if(ethnicityRaceQuestionId.Contains(question.QuestionId) & section.SectionName.ToLower() == SectionDetails.Declarations.ToString().ToLower())
                                    {
                                        flag = UpdateQuestionTextForEthnicityRaceSexQuestions(questionnaire, question, flag, declarationInfo);
                                    }
                                    else if(section.SectionId == Convert.ToInt32(SectionDetails.Income) && employmentSectionConfig.AllowedZeroOrBlankValues != null && employmentSectionConfig.AllowedZeroOrBlankValues.Any() && employmentSectionConfig.AllowedZeroOrBlankValues.Contains(question.QuestionId))
                                    {
                                        flag = false;
                                    }
                                    else if(section.SectionId == Convert.ToInt32(SectionDetails.Income) && (question.QuestionFieldName == Constant.BorrowerIncomeAlimonySupport || (alimonyChildSupport.AllowedZeroOrBlankValues != null && alimonyChildSupport.AllowedZeroOrBlankValues.Any() && alimonyChildSupport.AllowedZeroOrBlankValues.Contains(question.QuestionId))))
                                    { // we have to allow zero for alimony support and null start date // for child support also
                                         flag = false;
                                    }                                  
                                    else 
                                    {
                                        flag = true;
                                    }                                    
                                }
                                else
                                {
                                    if(ethnicityRaceQuestionId.Contains(question.QuestionId) & section.SectionName.ToLower() == SectionDetails.Declarations.ToString().ToLower())
                                    {                                                                  
                                        flag = UpdateQuestionTextForEthnicityRaceSexQuestions(questionnaire, question, flag, declarationInfo);
                                    }
                                    else if(vaQuestionId.Contains(question.QuestionId) & section.SectionId == Convert.ToInt32(SectionDetails.Profile))
                                    {
                                        var isChildAnswerGiven = false; 
                                        flag = false;
                                        if(question.QuestionId == vaInfo.QuestionInformation["SelectOptions"].QuestionId)
                                        {
                                            question.QuestionText = vaInfo.QuestionInformation["SelectOptions"].QuestionText;
                                            if(question.ChildQuestions != null)
                                            {           
                                                 isChildAnswerGiven = CheckChildAnswerGiven(questionnaire,question);
                                            }

                                            var answerString = JsonConvert.DeserializeObject<Dictionary<string,string>>(question.Answer);
                                            if(answerString != null)
                                            {
                                                var currentlyServingExpirationDate = string.Empty;
                                                var exists = answerString.TryGetValue("EnumVACurrentlyServingExpirationDate", out currentlyServingExpirationDate);
                                                if(exists && !string.IsNullOrWhiteSpace(currentlyServingExpirationDate))
                                                {
                                                    var currentlyServingExpirationDateValue = string.Empty;
                                                    answerString.TryGetValue("BorrowerVACurrentlyServingExpirationDateValue", out currentlyServingExpirationDateValue);
                                                    if(string.IsNullOrWhiteSpace(currentlyServingExpirationDateValue))
                                                    {
                                                        flag = true;
                                                    }
                                                }
                                            }
                                        }                                       
                                    }
                                    else if(propertyOwnAndTitleHold.Contains(question.QuestionId) & section.SectionName.ToLower() == SectionDetails.Declarations.ToString().ToLower())
                                    {
                                         flag = UpdateQuestionTextForPropertyOwnAndTitleHold(questionnaire, question, flag, declarationInfo);
                                    }
                                    else if(question.QuestionFieldName == Constant.BorrowerIncomeAlimonySupport)
                                    {
                                       var alimonyChildSupportSubSectionConfig = questionnaireSummaryFieldMap.FirstOrDefault(dl => dl.SubSectionType == Constant.AlimonyChildSupportSubSection);

                                        try
                                        {
                                            
                                                var obj = JsonConvert.DeserializeObject<Dictionary<string,string>>(question.Answer);
                                                    Int64 objAmount = 0;
                                                    string  objStartDate = string.Empty;
                                                if(obj.ContainsKey(Constant.AlimonyAmount))
                                                {
                                                        var amount = obj[Constant.AlimonyAmount];
                                                        
                                                        if(!string.IsNullOrEmpty(amount))
                                                        {
                                                            objAmount = Convert.ToInt64(amount);
                                                        }
                                                    }

                                                if(obj.ContainsKey(Constant.AlimonyStartDate))
                                                {
                                                        var startDate = obj[Constant.AlimonyStartDate];
                                                            
                                                        if(!string.IsNullOrEmpty(startDate))
                                                        {
                                                            objStartDate = startDate;
                                                        }
                                                }

                                                    if(objAmount == 0 && !string.IsNullOrEmpty(objStartDate))
                                                    {
                                                        flag = true;
                                                    }
                                                    else if(objAmount > 0 && string.IsNullOrEmpty(objStartDate))
                                                    {
                                                        flag = true;
                                                        if(alimonyChildSupportSubSectionConfig != null)
                                                        {
                                                        if(alimonyChildSupportSubSectionConfig.QuestionInformation["StartDate"] != null)
                                                            question.QuestionText = alimonyChildSupportSubSectionConfig.QuestionInformation["StartDate"].QuestionText;
                                                        else
                                                            question.QuestionText = Constant.AlimonyStartDateDisplayText;
                                                        }
                                                    }
                                                    else
                                                        flag = false;
                                            
                                        }
                                        catch
                                        {                                                        
                                            flag = false;                 
                                        }
                                    }
                                    else
                                       flag = false;
                                }
                                

                                if (question.QuestionText.ToLower().Equals("years") || question.QuestionText.ToLower().Equals("months"))
                                {
                                    yearAndMonthQuestionList = subSection.QuestionSections.Where(qs => qs.QuestionSectionName.ToLower().Equals("yearandmonth")).SelectMany(qs => qs.QuestionList);
                                    string year = "0", month = "0";
                                    year = yearAndMonthQuestionList.Where(x => x.QuestionText.ToLower().Equals("years")).SingleOrDefault().Answer;
                                    month = yearAndMonthQuestionList.Where(x => x.QuestionText.ToLower().Equals("months")).SingleOrDefault().Answer;
                                    if ((year == "0" && month != "0") || (year != "0" && month == "0"))
                                        flag = false;
                                }

                                if (loanPurposeAnswer.ToLower() == "purchase" && (question.QuestionFieldName.ToLower().Equals("borrowerrentorowncheckboxfalse") || question.QuestionFieldName.ToLower().Equals("borrowercurrentstreetaddresscheckboxfalse")))
                                {
                                    flag = false;
                                }
                                if (loanPurposeAnswer.ToLower() == "refinance" && question.QuestionText.ToLower().Equals("address"))
                                {
                                    flag = false;
                                }

                                if (loanPurposeAnswer.ToLower() == "refinance" && question.QuestionId == Constant.AddressPrepopulateQuestionId) // QuestionId "213"
                                {
                                    if(question.Answer == string.Empty)
                                        flag = true;
                                }

                                if (question.QuestionFieldName != null && loanPurposeAnswer.ToLower() == "refinance" && question.QuestionFieldName.ToLower().Equals("borrowerlookingtosell"))
                                {
                                    flag = false;
                                }

                                try{

                                    
                                    /*If property type is not multi-family then dont make number of units mandatory */
                                    // Profile REO
                                    if(question.QuestionFieldName == "BorrowerREOPropertyTypeUnits")
                                    {
                                        var propType = questions.Where(x=>x.QuestionFieldName == "BorrowerREOPropertyType").FirstOrDefault();
                                        if(propType != null && propType.Answer != "Multi-Family")
                                        {
                                            flag = false;
                                        }
                                    }
                                    
                                    if(question.QuestionFieldName == "BorrowerIncomeRentUnits")
                                    {
                                        var propType = questions.Where(x=>x.QuestionFieldName == "BorrowerIncomeRentPropertyType").FirstOrDefault();
                                        if(propType != null && propType.Answer != "Multi-Family")
                                        {
                                            flag = false;
                                        }
                                    }

                                    //// Investment Property validation remove for Rental Income section
                                    if(question.QuestionFieldName == Constant.BorrowerIncomeRentRentalIncome)
                                    {
                                        var propType = questions.Where(x=>x.QuestionFieldName == Constant.BorrowerIncomeRentPropertyUse).FirstOrDefault();
                                        if(propType != null && propType.Answer != Constant.InvestmentProperty)
                                        {
                                            flag = false;
                                        }
                                    }
                                    
                                    //// Investment Property validation remove for Profile section
                                    if(question.QuestionFieldName == Constant.BorrowerREORentalIncome)
                                    {
                                        var propType = questions.Where(x=>x.QuestionFieldName == Constant.BorrowerREOPropertyUse).FirstOrDefault();
                                        if(propType != null && propType.Answer != Constant.InvestmentProperty)
                                        {
                                            flag = false;
                                        }
                                    }

                                    // Incase of borrower or coborrower dont mandate preferred method of communication
                                    if(question.QuestionFieldName == Constant.BorrowerCommunication && excludeSpouseValidation){
                                        flag = false;
                                    }

                                    // Incase of borrower or coborrower dont mandate homephone 
                                    if(question.QuestionFieldName == Constant.BorrowerHomePhone && excludeSpouseValidation){
                                        flag = false;
                                    }

                                    // Incase of borrower or coborrower dont mandate cellPhone 
                                    if(question.QuestionFieldName == Constant.BorrowerCellPhone && excludeSpouseValidation){
                                        flag = false;
                                    }

                                    // Incase of borrower or coborrower dont mandate BorrowerBirthDate of spouse
                                    if(question.QuestionFieldName == Constant.BorrowerBirthDate && excludeSpouseValidation){
                                        flag = false;
                                    }

                                    // Incase of borrower or coborrower dont mandate BorrowerSSN of spouse
                                    if(question.QuestionFieldName == Constant.BorrowerSSN && excludeSpouseValidation){
                                        flag = false;
                                    }

                                }catch(Exception ex)
                                {
                                    Logger.Error("Error while setting required conditions",ex);
                                }

                                if (flag)
                                {
                                    var questionDetail = new RequiredQuestionDetails()
                                    {
                                        QuestionId = question.QuestionId,
                                        QuestionText = question.QuestionText,
                                        SubSectionId = subSection.SubSectionId,
                                        SectionId = section.SectionId,
                                        HighestSectionReached = userStatus.HighestSectionReached,
                                        HighestSubSectionReached = userStatus.HighestSubSectionReached
                                    };

                                    reqQuestionDetails.Add(questionDetail);
                                }
                            }
                            
                            if (employmentRequiredQuestion != null)
                            {
                                if (subSection.SubSectionId == employmentRequiredQuestion.SubSectionId)
                                {
                                    var employmentGapQuestion = subSection.QuestionSections
                                                         .Where(x => x.QuestionList != null)
                                                         .SelectMany(qs => qs.QuestionList)
                                                         .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerEmploymentGap);
                                    if(string.IsNullOrEmpty(employmentGapQuestion?.Answer))
                                     {
                                        reqQuestionDetails.Add(employmentRequiredQuestion);
                                     }
                                }
                            }
                            else if(incomeSectionEvaluated)
                            {
                                   var checkSubSection = subSection.QuestionSections.Where(q => q.QuestionList != null).SelectMany(qs => qs.QuestionList)
                                            .Where(qq => qq.QuestionFieldName == Constant.BorrowerEmploymentGap);
                                    if(checkSubSection != null && checkSubSection.Count() > 0)
                                    {
                                         var employmentGapQuestion = subSection.QuestionSections
                                                         .Where(x => x.QuestionList != null)
                                                         .SelectMany(qs => qs.QuestionList)
                                                         .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerEmploymentGap);
                                        if(!string.IsNullOrEmpty(employmentGapQuestion.Answer) && employmentGapQuestion.Answer.ToLower() == "yes")
                                        {
                                            int questionSectionSeqNoForGap = subSection.QuestionSections
                                                            .Where(sec => sec.QuestionSectionName == Constant.EmploymentGapSection)
                                                            .Select(x => x.SeqNo).FirstOrDefault();

                                            IQuestionnaireQuestionAnswer questionAnswerData = new QuestionnaireQuestionAnswer
                                            {
                                                TemporaryApplicationNumber = temporaryApplicationNumber,
                                                SectionId = section.SectionId,
                                                QuestionSectionSeqNo = questionSectionSeqNoForGap,
                                                QuestionId = employmentGapQuestion.QuestionId,
                                                SubSectionId = subSection.SubSectionId,
                                                Answer = "No"
                                            }; 

                                            await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionAnswerData);
                                        }
                                    }
                            }
                            
                            if (selfEmploymentRequiredQuestion != null)
                            {
                                if (subSection.SubSectionId == selfEmploymentRequiredQuestion.SubSectionId)
                                {
                                    var selfEmploymentGapQuestion = subSection.QuestionSections
                                                         .Where(x => x.QuestionList != null)
                                                         .SelectMany(qs => qs.QuestionList)
                                                         .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerSelfEmploymentGap);
                                    if(selfEmploymentGapQuestion != null)
                                    {
                                        if(string.IsNullOrEmpty(selfEmploymentGapQuestion.Answer))
                                        {
                                            reqQuestionDetails.Add(selfEmploymentRequiredQuestion);
                                        }
                                    }
                                   
                                }
                            }
                            else if(incomeSectionEvaluated)
                            {
                                   var checkSubSection = subSection.QuestionSections.Where(q => q.QuestionList != null).SelectMany(qs => qs.QuestionList)
                                            .Where(qq => qq.QuestionFieldName == Constant.BorrowerSelfEmploymentGap);
                                    if(checkSubSection != null && checkSubSection.Count() > 0)
                                    {
                                         var selfEmploymentGapQuestion = subSection.QuestionSections
                                                         .Where(x => x.QuestionList != null)
                                                         .SelectMany(qs => qs.QuestionList)
                                                         .FirstOrDefault(q => q.QuestionFieldName == Constant.BorrowerSelfEmploymentGap);
                                                         
                                        if(!string.IsNullOrEmpty(selfEmploymentGapQuestion.Answer) && selfEmploymentGapQuestion.Answer.ToLower() == "yes")
                                        {
                                            int questionSectionSeqNoForGap = subSection.QuestionSections
                                                            .Where(sec => sec.QuestionSectionName == Constant.SelfEmploymentGapSection)
                                                            .Select(x => x.SeqNo).FirstOrDefault();

                                            IQuestionnaireQuestionAnswer questionAnswerData = new QuestionnaireQuestionAnswer
                                            {
                                                TemporaryApplicationNumber = temporaryApplicationNumber,
                                                SectionId = section.SectionId,
                                                QuestionSectionSeqNo = questionSectionSeqNoForGap,
                                                QuestionId = selfEmploymentGapQuestion.QuestionId,
                                                SubSectionId = subSection.SubSectionId,
                                                Answer = "No"
                                            }; 

                                            await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionAnswerData);
                                        }
                                    }
                            }

                             if (missingCoBorrowerRequiredQuestion != null)
                            {
                                if (subSection.SubSectionId == missingCoBorrowerRequiredQuestion.SubSectionId)
                                {                                    
                                    reqQuestionDetails.Add(missingCoBorrowerRequiredQuestion);
                                }
                            }
                        }

                        if(employmentStartEndDateRequiredQuestionList != null && employmentStartEndDateRequiredQuestionList.Count > 0)
                        {
                            foreach(var empQuestion in employmentStartEndDateRequiredQuestionList)
                            {
                                    reqQuestionDetails.Add(empQuestion);   
                            }
                        }

                       
                    }

                    if (reqQuestionDetails.Count > 0)
                    {
                        if(!userValidations.ContainsKey(user.UserName))
                        {
                            userValidations.Add(user.UserName, reqQuestionDetails);
                        }
                    }                        

                    questionnaire = Extensions.Clone(questionnaireCopy);
                }
            }

            return await Task.Run(() => userValidations);
        }

        private bool UpdateQuestionTextForPropertyOwnAndTitleHold(IQuestionnaire questionnaire, IQuestion question, bool flag, QuestionnaireSummaryConfig declarationInfo)
        {
          
            if(question.QuestionId == declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwn"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwn"].QuestionText;
                
                if(string.IsNullOrEmpty(question.Answer))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["BorrowerHoldTitle"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["BorrowerHoldTitle"].QuestionText;
                
                if(string.IsNullOrEmpty(question.Answer))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwnOther"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["BorrowerTypeOfPropertyOwnOther"].QuestionText;
                
                if(string.IsNullOrEmpty(question.Answer))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["BorrowerHoldTitleOther"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["BorrowerHoldTitleOther"].QuestionText;
                
                if(string.IsNullOrEmpty(question.Answer))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            return flag;
        }
        private bool UpdateQuestionTextForEthnicityRaceSexQuestions(IQuestionnaire questionnaire, IQuestion question, bool flag, QuestionnaireSummaryConfig declarationInfo)
        {
            //now need to implement logic that is parent is unchecked and
            //no child answer selected then required message to be showm
            //for ethnicity and race.
            //need to add skippable for ethnicity and race and sex.
                                      
            var isChildAnswerGiven = false; 
            if(question.QuestionId == declarationInfo.QuestionInformation["Ethnicity"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["Ethnicity"].QuestionText;
                
                if(question.ChildQuestions != null)
                {           
                        isChildAnswerGiven = CheckChildAnswerGiven(questionnaire, question);
                }

                if(!isChildAnswerGiven & (string.IsNullOrWhiteSpace(question.Answer) || question.Answer.ToLower() == "false"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["Race"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["Race"].QuestionText;

                    if(question.ChildQuestions != null)
                {           
                        isChildAnswerGiven = CheckChildAnswerGiven(questionnaire,question);
                }

                if(!isChildAnswerGiven & (string.IsNullOrWhiteSpace(question.Answer) || question.Answer.ToLower() == "false"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["EthnicityOther"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["EthnicityOther"].QuestionText;
                    if(question.ChildQuestions != null)
                {           
                        isChildAnswerGiven = CheckChildAnswerGiven(questionnaire,question);
                }

                if(!isChildAnswerGiven & (string.IsNullOrWhiteSpace(question.Answer) || question.Answer.ToLower() == "false"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["RaceOther"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["RaceOther"].QuestionText;
                if(question.ChildQuestions != null)
                {           
                        isChildAnswerGiven = CheckChildAnswerGiven(questionnaire,question);
                }

                if(!isChildAnswerGiven & (string.IsNullOrWhiteSpace(question.Answer) || question.Answer.ToLower() == "false"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["Sex"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["Sex"].QuestionText;
                if(question.ChildQuestions != null)
                {           
                        isChildAnswerGiven = CheckChildAnswerGiven(questionnaire,question);
                }

                if(!isChildAnswerGiven & (string.IsNullOrWhiteSpace(question.Answer) || question.Answer.ToLower() == "false"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            else if(question.QuestionId == declarationInfo.QuestionInformation["SexOther"].QuestionId)
            {
                question.QuestionText = declarationInfo.QuestionInformation["SexOther"].QuestionText;
                if(question.ChildQuestions != null)
                {           
                        isChildAnswerGiven = CheckChildAnswerGiven(questionnaire,question);
                }

                if(!isChildAnswerGiven & (string.IsNullOrWhiteSpace(question.Answer) || question.Answer.ToLower() == "false"))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }

            return flag;
        }

        private bool CheckChildAnswerGiven(IQuestionnaire questionnaire,IQuestion question)
        {
            var isChildAnswerGiven = false;

            if (question == null)
            {
                return isChildAnswerGiven;
            }
            
            foreach(var childQuestion in question.ChildQuestions)
            {
                    var childQuestionAnswer = questionnaire.ApplicationForm.Sections.Where(x => x.SectionId == childQuestion.SectionId)
                    .SelectMany(x => x.SubSections).Where(x => x.SubSectionId == childQuestion.SubSectionId)
                    .SelectMany(x => x.QuestionSections)
                    .SelectMany(x => x.QuestionList).Where(x => x.QuestionId == childQuestion.QuestionId).FirstOrDefault();
                if(childQuestionAnswer!=null &&  !string.IsNullOrWhiteSpace(childQuestionAnswer.Answer))
                {
                    //parse json 
                    //var obj = JsonConvert.DeserializeObject<Dictionary<string, string>>(childQuestionAnswer.Answer);
                    //dynamic da = JObject.Parse(childQuestionAnswer.Answer);
                    
                    isChildAnswerGiven  =true;
                    break;
                }
            }

            return isChildAnswerGiven;
        }
        
        private void ValidateCollection(Dictionary<string, IQuestion> addresses, string key)
        {
            if (addresses.ContainsKey(key) == false)
            {
                throw new Exception($"Key '{key}' does not exist");
            }
            else
            {
                if (addresses[key] == null)
                {
                    throw new Exception($"Object is null for the Key '{key}'");
                }
            }
        }

        /// <summary>
        /// This function loops through all the IsSensitive = true questions and decrypts the answer.
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        private async Task<IQuestionnaire> GetDecryptedQuestionnaire(IQuestionnaire questionnaire)
        {
            //Loop through all the encryptd questions and decrypt the values.
            var questions = questionnaire.ApplicationForm.Sections.SelectMany(s => s.SubSections)
                                                                           .SelectMany(ss => ss.QuestionSections)
                                                                           .Where(x => x.QuestionList != null)
                                                                           .SelectMany(qs => qs.QuestionList).Where(s => s.IsSensitive && string.IsNullOrEmpty(s.Answer) == false);

            foreach (var question in questions)
            {
                question.Answer = DecryptAnswer(question);
            }

            return await Task.Run(() => questionnaire);
        }

        private IQuestion ValidateQuestionExistenceInTheQuestionSection(IQuestionSection questionsectionToUpdate, string questionId)
        {
            var question = questionsectionToUpdate.QuestionList.FirstOrDefault(Question => Question.QuestionId == questionId);
            return question;
        }

        /// <summary>
        /// This function updates the user profile.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <param name="borrowerInfo"></param>
        /// <returns></returns>
        private async Task<bool> UpdateUserProfile(string temporaryApplicationNumber, string userName, IBorrowerCoBorrowerInfo borrowerInfo)
        {
            var status = false;
            Logger.Info($"Inside UpdateUserProfile for {userName}");
            try
            {
                var UserProfile = (Docitt.UserProfile.UserProfile)await UserProfileService.GetProfile(userName);

                if (UserProfile == null)
                {
                    Logger.Info($"Could not get User Profile information of the user {userName}");
                    return true;
                }

                if (borrowerInfo != null)
                {
                    UserProfile.FirstName = borrowerInfo.FirstName;
                    UserProfile.LastName = borrowerInfo.LastName;
                    UserProfile.Phone = borrowerInfo.Phone;
                }

                try
                {
                    var userProfile = await UserProfileService.UpdateProfile(userName, UserProfile);

                    if (userProfile != null)
                    {
                        status = true;
                    }
                    else
                    {
                        Logger.Warn($"UpdateProfile did not update information for the user {userName}");
                    }

                }
                catch (Exception ex)
                {
                    Logger.Info("Error in UpdateUserProfile "+ ex.Message);
                }

            }
            catch (Exception ex)
            {
                 Logger.Info("Error in UpdateUserProfile "+ ex.Message);
            }

            return await Task.Run(() => status);
        }

        /// <summary>
        /// This function gets the borrower info based on the logged in user
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        private async Task<IBorrowerCoBorrowerInfo> GetBorrowerInfo(string temporaryApplicationNumber, string userName, IQuestionnaire questionnaire = null)
        {

            IBorrowerCoBorrowerInfo applicant = new BorrowerCoBorrowerInfo();

            var borrowerList = await GetBorrowersAndCoBorrowersAndSpouse(temporaryApplicationNumber, false);

            foreach (var applicantInList in borrowerList)
            {
                //if (applicantInList.Email == userName)
                if (applicantInList.UserName == userName)
                {
                    applicant = applicantInList;
                    break;
                }
            }

            return applicant;
        }

        /// <summary>
        /// This function gets the borrower info based on the logged in user
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <param name="questionnaire"></param>
        /// <returns></returns>
        private async Task<IBorrowerCoBorrowerSpouseInfo> GetBorrowerWithSpouseInfo(string temporaryApplicationNumber, string userName, IQuestionnaire questionnaire = null)
        {

            IBorrowerCoBorrowerSpouseInfo applicant = new BorrowerCoBorrowerSpouseInfo();

            var borrowerList = await GetBorrowersAndCoBorrowersAndSpouseName(temporaryApplicationNumber, false, questionnaire);

            foreach (var applicantInList in borrowerList)
            {
                //if (applicantInList.Email == userName)
                if (applicantInList.UserName == userName)
                {
                    applicant = applicantInList;
                    break;
                }
            }

            return applicant;
        }

        /// <summary>
        /// Ths function gets the total amount
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <param name="item"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        private async Task<double> GetTotalAmount(IQuestionnaire questionnaire, IApplicationFieldMap item, string userName = null)
        {
            double totalAmount = 0;
            var answers = new List<string>();

            if (string.IsNullOrEmpty(userName))
            {
                answers = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId).SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .Where(
                           q => q.QuestionId == item.QuestionId
                           &&
                           (string.IsNullOrEmpty(item.QuestionFieldNameBorrower) || q.QuestionFieldName == item.QuestionFieldNameBorrower)
                        )
                        .Select(q => q.Answer).ToList();
            }
            else
            {
                answers = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId).SelectMany(s => s.SubSections)
                        .Where(ss => ss.UserName == userName)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .Where
                       (
                           q => q.QuestionId == item.QuestionId
                             &&
                           (string.IsNullOrEmpty(item.QuestionFieldNameBorrower) || q.QuestionFieldName == item.QuestionFieldNameBorrower)
                       )
                       .Select(q => q.Answer).ToList();
            }

            foreach (var answer in answers)
            {
                if (answer != null && answer != "")
                {
                    try
                    {
                        if(item.TargetProperty.ToLower() == "alimonypermonth")
                        {
                            var result = JsonConvert.DeserializeObject<dynamic>(answer);
                            var amt = result.alimonyAmount is null ? "0" : result.alimonyAmount;
                            totalAmount = totalAmount + Convert.ToDouble(amt); 
                        }
                        else
                        {
                            totalAmount = totalAmount + Convert.ToDouble(answer);
                        }
                    }
                    catch(Exception ex)
                    {
                        Logger.Warn($"Error in parsing amount for GetTotalAmount {item.TargetProperty} {answer}",ex);
                    }
                    
                }
            }

            return await Task.Run(() => totalAmount);
        }

        /// <summary>
        /// This function fetches the information of a specific section, and question id
        /// </summary>
        /// <param name="questionnaire"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private async Task<string> GetOnlyBorrowerInfo(IQuestionnaire questionnaire, IApplicationFieldMap item)
        {
            //This function just check the status of the Borrower. It does not check the bankruptsy status of other applicants.

            var question = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId).SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == item.QuestionId && 
                            (string.IsNullOrEmpty(item.QuestionFieldNameCoBorrower) || q.QuestionFieldName == item.QuestionFieldNameCoBorrower)
                       );

            if (question != null)
            {
                return question.Answer;
            }

            return await Task.Run(() => "");
        }

        /// <summary>
        /// This function get the temporary application number of the logged in user's questionnaire, if there is any
        /// </summary>
        /// <returns></returns>
        private async Task<string> GetTemporaryApplicationNumber()
        {
            var userName = await GetCurrentUser();

            return await QuestionnaireRepository.GetApplicationNumberGivenUserName(userName);
        }

        /// <summary>
        /// This function removes the question not needed to be shown on the summary screen from the sub section
        /// On the summary screen only few questions are to be shown from the sub section
        /// </summary>
        /// <param name="subSection"></param>
        /// <param name="questionText"></param>
        /// <returns></returns>
        private async Task<ISubSection> RemoveQuestionFromSubSection(ISubSection subSection, string questionText, string questionId =null)
        {
            IQuestion questionAnswer = new Question();
            if (questionText != null)
            {
                //TODO: This should be changed to get details from questionFieldName
                questionAnswer = subSection.QuestionSections
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionText == questionText || q.QuestionText == string.Concat(questionText,"*"));
            }
            else
            {
                questionAnswer = subSection.QuestionSections
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == questionId);
            }

            // var questionAnswer = subSection.QuestionSections
            //            .SelectMany(qs => qs.QuestionList)
            //            .FirstOrDefault(q => q.QuestionText == questionText);

            foreach (QuestionSection qs in subSection.QuestionSections)
            {
                qs.QuestionList.Remove(questionAnswer);
            }

            return await Task.Run(() => subSection);
        }

        /// <summary>
        /// This function fethes the answer using the QuestioId or QuestionText
        /// </summary>
        /// <param name="subSectionSummary"></param>
        /// <param name="questionId"></param>
        /// <param name="questionText"></param>
        /// <returns></returns>
        private async Task<IQuestion> GetQuestionFromQuestionTextOrQuestionId(ISubSectionSummary subSectionSummary, string questionId, string questionText)
        {

            IQuestion questionAnswer = new Question();

            if (questionText != null)
            {
                //TODO: This should be changed to get details from questionFieldName
                questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionText == questionText || q.QuestionText == string.Concat(questionText,"*"));
            }
            else
            {
                questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionId == questionId);
            }

            if (questionAnswer == null)
            {
                // throw new Exception($"No question exists having question text '{questionText}' or questionid '{questionId}'");
            }          

            return await Task.Run(() => questionAnswer);
        }

        private dataType ParseString(string str)
        {
            bool boolValue;
            Int32 intValue;
            Int64 bigintValue;
            double doubleValue;
            DateTime dateValue;

            // Place checks higher in if-else statement to give higher priority to type.

            if (IsValidEmailId(str))
                return dataType.System_Email;
            else if (bool.TryParse(str, out boolValue))
                return dataType.System_Boolean;
            else if (Int64.TryParse(str, out bigintValue))
                return dataType.System_Int64;
            else if (Int32.TryParse(str, out intValue))
                return dataType.System_Int32;
            else if (double.TryParse(str, out doubleValue))
                return dataType.System_Double;
            else if (DateTime.TryParse(str, out dateValue))
                return dataType.System_DateTime;
            else return dataType.System_String;
        }

        /// <summary>
        /// This function updates the Question Textusing the Question text or QuestionId
        /// </summary>
        /// <param name="subSectionSummary"></param>
        /// <param name="questionId"></param>
        /// <param name="oldQuestionText"></param>
        /// <param name="newQuestionText"></param>
        /// <param name="newAnswer"></param>
        /// <returns></returns>
        private async Task<ISubSectionSummary> UpdateQuestionText(ISubSectionSummary subSectionSummary, string questionId, string oldQuestionText, string newQuestionText, string newAnswer, bool optional=false)
        {
            IQuestion questionAnswer = new Question();

            if (questionId != null)
            {
                questionAnswer = subSectionSummary.SubSection.QuestionSections
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == questionId);

            }
            else
            {
                //TODO: This should be changed to get details from questionFieldName
                questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionText == oldQuestionText || q.QuestionText == string.Concat(oldQuestionText,"*"));
            }

            if (questionAnswer == null)
            {
                if (optional == true)
                {
                    return await Task.Run(() => subSectionSummary);        
                }
                throw new QuestionDoesNotExistsException($"Question having QUestion Id '{questionId}' or Question Text '{oldQuestionText}' does not exist");
            }

            questionAnswer.QuestionText = newQuestionText;

            if (newAnswer != null)
            {
                questionAnswer.Answer = newAnswer;
            }

            return await Task.Run(() => subSectionSummary);
        }

        /// <summary>
        /// This function removes the Question from the sub section using the question Id
        /// </summary>
        /// <param name="subSection"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        private async Task<ISubSection> RemoveQuestionByIdFromSubSection(ISubSection subSection, string questionId)
        {
            var questionAnswer = subSection.QuestionSections
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == questionId);

            foreach (QuestionSection qs in subSection.QuestionSections)
            {
                qs.QuestionList.Remove(questionAnswer);
            }

            if(questionAnswer != null)
            {
                if(questionAnswer.ChildQuestions != null)
                {
                    //need to update/remove the third child level for above question.
                    var childQuestionId = questionAnswer.ChildQuestions.ToList().Select(x => x.QuestionId);

                    foreach(var queId in childQuestionId)
                    {
                            var questionToRemove = subSection.QuestionSections
                                    .SelectMany(qs => qs.QuestionList)
                                    .FirstOrDefault(q => q.QuestionId == queId);
                            if(questionToRemove != null)
                            {
                                foreach (QuestionSection qs in subSection.QuestionSections)
                                {
                                    qs.QuestionList.Remove(questionToRemove);
                                }
                            }
                    }  
                }
            }

            return await Task.Run(() => subSection);
        }

        /// <summary>
        /// This function fetches the answer of the question based on the Question Text
        /// </summary>
        /// <param name="subSectionSummary"></param>
        /// <param name="questionText"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        private async Task<string> GetAnswerFromQuestionText(ISubSectionSummary subSectionSummary, string questionText, string questionId)
        {
            //string answer = "";
            IQuestion questionAnswer = new Question();

            if (questionText != null)
            {
                try
                {
                    //TODO: This should be changed to get details from questionFieldName
                    questionAnswer = subSectionSummary.SubSection.QuestionSections
                               .SelectMany(qs => qs.QuestionList)
                               .First(q => q.QuestionText == questionText ||  q.QuestionText == string.Concat(questionText,"*"));

                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message, ex);
                    //throw new Exception($"No question exists having question text '{questionText}'");
                }

            }
            else
            {
                questionAnswer = subSectionSummary.SubSection.QuestionSections
                           .SelectMany(qs => qs.QuestionList)
                           .FirstOrDefault(q => q.QuestionId == questionId);
            }

            if (questionAnswer == null)
            {
                //throw new Exception($"No question exists having question text '{questionText}' or questionid '{questionId}'");
            }

            if (questionAnswer == null)
            {
                return await Task.Run(() => string.Empty);
            }
            else
            {
                return await Task.Run(() => questionAnswer.Answer);
            }
        }

        /// <summary>
        /// GetAllSubmittedApplications
        /// </summary>
        /// <returns></returns>
        public async Task<List<Docitt.Application.IApplication>> GetAllSubmittedApplications()
        {
            var userName = await GetCurrentUser();
            if(string.IsNullOrWhiteSpace(userName)){
                return null;
            }            

            var applications = await ApplicationService.GetApplicationsByUserName(userName);
            
            return applications;
        }

        /// <summary>
        /// This function fetches the information of the logged in user
        /// </summary>
        /// <returns></returns>
        private async Task<string> GetCurrentUser()
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                var username = token?.Subject;
                if(!string.IsNullOrEmpty(username)) { username = username.ToLower(); }
                return await Task.Run(() => username);

            }
            catch (InvalidUserException exception)
            {
                throw new InvalidUserException(exception.Message);
            }
        }

        private List<ISubSection> GetUserInfoAndContactInfoSubsections(ISection section,  string subSectionId)
        {
            var subSection = section.SubSections.Find(ss => ss.SubSectionId == subSectionId);

            if(subSection == null)
                throw new SubSectionDoesNotExistsException($"Error in GetUserInfoAndContactInfoSubsections for TemporaryApplicationNumber {section.TemporaryApplicationNumber} Section {section.SectionId} Subsection {subSectionId} not found");

            var subSections = new List<ISubSection>();

            if (subSectionId.Equals("SS1",StringComparison.OrdinalIgnoreCase) || subSectionId.Equals("SSS1",StringComparison.OrdinalIgnoreCase))
            {
                subSections = section.SubSections.Where(ss => ss.SubSectionId.Equals("SS1",StringComparison.OrdinalIgnoreCase) || ss.SubSectionId.Equals("SSS1",StringComparison.OrdinalIgnoreCase)).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(subSection.TemplateId)  && (subSection.TemplateId.Equals("spouseInfo",StringComparison.OrdinalIgnoreCase) || subSection.TemplateId.Equals("spouseCommunicationInfo",StringComparison.OrdinalIgnoreCase)))
            {
                subSections = section.SubSections
                    .Where(ss => !string.IsNullOrEmpty(ss.TemplateId)).Where(ss => ss.TemplateId.Equals("spouseInfo",StringComparison.OrdinalIgnoreCase) || ss.TemplateId.Equals("spouseCommunicationInfo",StringComparison.OrdinalIgnoreCase)).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(subSection.TemplateId)  && (subSection.TemplateId.Equals("coBorrowerInfo",StringComparison.OrdinalIgnoreCase) || subSection.TemplateId.Equals("coBorrowerCommunicationInfo",StringComparison.OrdinalIgnoreCase)))
            {
                var communicationInfoSubSectionId = subSection.SubSectionId.Substring(0, subSection.SubSectionId.Length-2) + "-2";

                subSections = section.SubSections
                    .Where(ss => ss.SubSectionId.Equals(subSection.SubSectionId,StringComparison.OrdinalIgnoreCase) || ss.SubSectionId.Equals(communicationInfoSubSectionId,StringComparison.OrdinalIgnoreCase)).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(subSection.TemplateId)  && (subSection.TemplateId.Equals("coBorrowerSpouseInfo",StringComparison.OrdinalIgnoreCase)))
            {
                subSections = section.SubSections
                    .Where(ss => !string.IsNullOrEmpty(ss.TemplateId))
                    .Where(ss => (ss.TemplateId.Equals("coBorrowerSpouseInfo",StringComparison.OrdinalIgnoreCase) || ss.TemplateId.Equals("coBorrowerSpouseCommunicationInfo",StringComparison.OrdinalIgnoreCase))
                        && ss.UserName == subSection.UserName).ToList();
            }
            else
            {
                subSections.Add(subSection);
            }

            return subSections;
        }
        
        private List<ISubSection> GetUserInfoAndContactInfoSubsections(List<ISection> sections, int sectionId, string subSectionId)
        {
            var subSection = sections.Find(s => s.SectionId == sectionId).SubSections.Find(ss => ss.SubSectionId == subSectionId);
            var subSections = new List<ISubSection>();

            if(subSection == null)
                throw new SubSectionDoesNotExistsException($"Error in GetUserInfoAndContactInfoSubsections for TemporaryApplicationNumber {sections.FirstOrDefault()?.TemporaryApplicationNumber} Section {sectionId} Subsection {subSectionId} not found");
            var subSectionsToQuery = sections.Where(s => s.SectionId == sectionId).SelectMany(s => s.SubSections);
            if (subSectionId.Equals("SS1") || subSectionId.Equals("SSS1"))
            {
                subSections = subSectionsToQuery
                    .Where(ss => ss.SubSectionId.Equals("SS1") || ss.SubSectionId.Equals("SSS1")).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(subSection.TemplateId)  && (subSection.TemplateId.Equals("spouseInfo") || subSection.TemplateId.Equals("spouseCommunicationInfo")))
            {
                subSections = subSectionsToQuery
                    .Where(ss => !string.IsNullOrEmpty(ss.TemplateId)).Where(ss => ss.TemplateId.Equals("spouseInfo") || ss.TemplateId.Equals("spouseCommunicationInfo")).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(subSection.TemplateId)  && (subSection.TemplateId.Equals("coBorrowerInfo") || subSection.TemplateId.Equals("coBorrowerCommunicationInfo")))
            {
                var infoSubSectionID = subSection.SubSectionId;
                var CommunicationInfoSubSectionID = infoSubSectionID.Substring(0, infoSubSectionID.Length-2) + "-2";

                subSections = subSectionsToQuery
                    .Where(ss => ss.SubSectionId.Equals(infoSubSectionID) || ss.SubSectionId.Equals(CommunicationInfoSubSectionID)).ToList();
            }
            else if (!string.IsNullOrWhiteSpace(subSection.TemplateId)  && (subSection.TemplateId.Equals("coBorrowerSpouseInfo")))
            {
                subSections = subSectionsToQuery
                    .Where(ss => !string.IsNullOrEmpty(ss.TemplateId))
                    .Where(ss => (ss.TemplateId.Equals("coBorrowerSpouseInfo") || ss.TemplateId.Equals("coBorrowerSpouseCommunicationInfo"))
                        && ss.UserName == subSection.UserName).ToList();
            }
            else
            {
                subSections.Add(subSection);
            }

            return subSections;
        }
        
        

        #endregion

        #region Public Methods

        /// <summary>
        /// This function provides Application Type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public FormType GetFormType(string type)
        {
            switch (type)
            {
                case "Full":
                    return FormType.Full;
                case "Short":
                    return FormType.Short;
                default:
                    return FormType.Null;
            }
        }

        #region "Questionnaires"

        /// <summary>
        /// Fetch the Questionnaires
        /// </summary>
        /// <returns></returns>
        public async Task<List<IQuestionnaire>> GetQuestionnaires()
        {
            var questionnaires = new List<IQuestionnaire>();

            questionnaires = await QuestionnaireRepository.GetQuestionnaires();
            if (questionnaires != null)
            {
                return await Task.Run(() => questionnaires);
            }
            else
            {
                throw new QuestionnaireNotFoundException($"No Questionnaire available for the Tenant.");
            }
        }

        /// <summary>
        /// Fetch the questionnaire of the logged in user, or for the Lender user or affinity partner user
        /// </summary>
        /// <returns></returns>
        public async Task<IQuestionnaire> GetQuestionnaire()
        {
            //This function fetches the Questionnaire of the logged in user.
            IQuestionnaire questionnaire = new Questionnaire();

            //Get the TemporaryApplicationNumber of the logged in user
            var existingTemporaryApplicationNumber = await GetTemporaryApplicationNumber(); //QuestionnaireRepository.GetExistingQuestionnaireFormIdOfTheUser(userName);

            if (existingTemporaryApplicationNumber == null || existingTemporaryApplicationNumber == "")
            {
                throw new QuestionnaireNotFoundException("No Questionnaire exist for the user");
            }

            //Get the Questionnaire if it exists
            questionnaire = await GetQuestionnaire(existingTemporaryApplicationNumber);

            if (questionnaire != null)
            {
                return await Task.Run(() => questionnaire);
            }
            else
            {
                throw new QuestionnaireNotFoundException($"Questionnaire having the Temporary Application Number '{existingTemporaryApplicationNumber}' cannot be found.");
            }
        }

         public async Task<List<IUserAccessedStatus>> GetUserStateInformation(string applicationNumber)
         {
            var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(applicationNumber);

            if(questionnaire == null)
                throw new QuestionnaireNotFoundException($"Questionnaire data not found for {applicationNumber}");

            return questionnaire.UserState;
         }

        /// <summary>
        /// This function validates whether the form being fetched is of the logged in user if the user is neither a Lender user or Affinity partner user.
        /// There will be Lender's team which will be pulling these questionnaires which are not created by them.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<IQuestionnaire> GetQuestionnaire(string temporaryApplicationNumber, string userName = null)
        {
            IQuestionnaire questionnaire = new Questionnaire();
            userName = userName == null ? await GetCurrentUser() : userName;

            if (!string.IsNullOrWhiteSpace(userName) && !IsLenderUser() && !IsAffinityPartnerUser())
                //Fetch the questionnaire only if the logged in user have the access to the Questionnaire                
                questionnaire = await QuestionnaireRepository.GetQuestionnaire(temporaryApplicationNumber, userName);
            else
                //If the user is a Lender user or Affinity partner user accessing the Questionnaire, then we do not need to verify the user name
                //Just fetch the questionnaire, without validating it against the user having access to Questionnaire
                questionnaire = await QuestionnaireRepository.GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire != null)
            {
                //TODO by Bhushan: Need to check why we are setting the section to summary as highest.
                var appFieldMap = QuestionnaireConfigurations.ApplicantDynamicSections;

                var currentSectionTemplatesFromMap = appFieldMap.Map.Where(m => m.TemplateId == "summaryPersonalInfo").FirstOrDefault();

                if (currentSectionTemplatesFromMap != null)
                {
                    //Check whether the Highest Section Reached is greater than Summary section, if yes, then set Summary Section Id as Highest Section Reached.
                    if (questionnaire.HighestSectionReached > currentSectionTemplatesFromMap.SectionId && questionnaire.Status == FormStatus.Open)
                    {
                        questionnaire.HighestSectionReached = currentSectionTemplatesFromMap.SectionId;
                    }
                }

                return await Task.Run(() => questionnaire);
            }
            else
            {
                throw new QuestionnaireNotFoundException($"Questionnaire having the Temporary Application Number '{temporaryApplicationNumber}' cannot be found.");
            }
        }

        /// <summary>
        /// This function is called when the Questionnaire does not exist for the logged in user
        /// Here a new Questionnaire is assigned
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<IQuestionnaire> GetStarted(string type, bool allowNew=false)
        {
             Logger.Debug("GetStarted started ...");
            try
            {
                var formType = GetFormType(type);

                //Create a new Questionnaire for the user and provide the Questionnaire
                Logger.Debug("Calling AddingQuestionnaire");
                var TemporaryApplicationNumber = await AddQuestionnaire(formType, allowNew);
                Logger.Debug("Fetching questionnaire data");
                var questionnaire = await GetQuestionnaire(TemporaryApplicationNumber);
                 Logger.Debug("Fetched questionnaire data");
                return questionnaire;
            }
            catch (QuestionnaireAlreadyExistsException ex)
            {
                throw new QuestionnaireAlreadyExistsException(ex.Message);
            }
        }

        /// <summary>
        /// This function returns the Questionnaire of the logged in user.
        /// </summary>
        /// <returns></returns>
        public async Task<IQuestionnaire> GetQuestionnaireStatus()
        {

            //////
            //Get the username and the temporaryapplicationnumber
            //Now fetch the Questionnaire
            IQuestionnaire questionnaire = new Questionnaire();

            var userName = await GetCurrentUser();

            var existingTemporaryApplicationNumber = await GetTemporaryApplicationNumber(); // QuestionnaireRepository.GetExistingQuestionnaireFormIdOfTheUser(userName);

            if (existingTemporaryApplicationNumber == null || existingTemporaryApplicationNumber == "")
            {
                throw new QuestionnaireNotFoundException("No Questionnaire having the status as 'Open' exist for the user.");
            }

            questionnaire = await GetQuestionnaireStatus(existingTemporaryApplicationNumber);

            if (questionnaire == null)
            {
                throw new QuestionnaireNotFoundException($"Questionnaire does not exist.");
            }

            ////////////////Update User Profile

            var borrowerInfo = await GetBorrowerInfo(questionnaire.TemporaryApplicationNumber, userName);

            if (string.IsNullOrEmpty(borrowerInfo.FirstName) == true || string.IsNullOrEmpty(borrowerInfo.LastName) == true)
            {
                Logger.Info($"First Name or Last Name is not provided to update the UserProfile for the user {userName}");
            }
            else
            {
                var updateStatus = await UpdateUserProfile(questionnaire.TemporaryApplicationNumber, userName, borrowerInfo);
            }

            return await Task.Run(() => questionnaire);
        }

        /// <summary>
        /// This fucntion just provides the Questionnaire status. In the response it does not send the whole Application
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        public async Task<IQuestionnaire> GetQuestionnaireStatus(string temporaryApplicationNumber)
        {
            Logger.Debug($"GetQuestionnaireStatus for temporaryApplicationNumner {temporaryApplicationNumber}");
            IQuestionnaire questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(temporaryApplicationNumber);
            if (questionnaire != null)
            {
                var userName = await GetCurrentUser();
                var userState = questionnaire.UserState.FirstOrDefault(u => u.UserName == userName);
                if (userState == null)
                    throw new UnauthorizedAccessException("User is not authorized");
                questionnaire.Status = userState.Status == 0 ? FormStatus.Open : userState.Status;
                questionnaire.ApplicationForm = null;
                return await Task.Run(() => questionnaire);
            }
            else
            {
                throw new QuestionnaireNotFoundException($"Questionnaire having the Temporary Application Number '{temporaryApplicationNumber}' cannot be found.");
            }
        }

        //This function is a working function. It is commented out because a new function GetNonBorrowerInformationFromQuestionnaire is to be used
        //changed on 21st July
        public async Task<ITransitionFormExtended> GetInformationFromQuestionnaire(string temporaryApplicationNumber) //GenerateApplicationFromQuestionnaire()public async Task<ITransitionFormExtended> GenerateApplicationFromQuestionnaire()
        {
            //This function fetches the Questionnaire and maps the value to Application field.
            //It then calls the Application creation API.

            var request = await GetQuestionnaire(temporaryApplicationNumber);

            ITransitionFormExtended applicantInfo = new TransitionForm();

            var appFieldMap = QuestionnaireConfigurations.ApplicationFieldMap; //GetApplicationConfiguration("application-field-map");

            foreach (IApplicationFieldMap item in appFieldMap.Map)
            {
                var section = request.ApplicationForm.Sections.Where(s => s.SectionId == item.SectionId);
                if(section == null)
                 continue;
                
                var questionAnswer = section.SelectMany(s => s.SubSections)
                       .SelectMany(ss => ss.QuestionSections)
                       .SelectMany(qs => qs.QuestionList)
                       .FirstOrDefault(q => q.QuestionId == item.QuestionId);

                if (questionAnswer != null)
                {
                    try
                    {
                        applicantInfo = await GetBorrowerDetail(request, questionAnswer, applicantInfo, item);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message, ex);
                    }
                }
            }

            applicantInfo.WorkflowId = "1";
            applicantInfo.AnnualGrossIncome = applicantInfo.EmploymentIncome; //hardcoding this will come from a function.
            applicantInfo.Channel = "Merchant";

            applicantInfo.HomeOwnership = "";

            return applicantInfo;
        }

        /// <summary>
        /// This function creates the questionnaire and updates the status.
        /// This end point was created to get the status of existing Questionnaire. If Questionnaire does not eist, then it will create the questionnaire
        /// </summary>
        /// <param name="formType"></param>
        /// <returns></returns>
        public async Task<IQuestionnaire> CreateQuestionnaireAndGetQuestionnaireStatus(string formType)
        {

            //////
            //Add a Questionnaire if it is not added

            var questionnaire = await GetStarted(formType);

            if (questionnaire == null)
            {
                throw new Exception("Questionnaire could not get created in GetSectionList");
            }
            //////

            questionnaire.ApplicationForm = null; //since this function is just to provide the status for the progress bar, we are not sending the Questionnaire in the response.

            return await Task.Run(() => questionnaire);
        }

        /// <summary>
        /// This function assigns a new Questionnaire to the user, if it does not exist.
        /// </summary>
        /// <param name="formType"></param>
        /// <returns></returns>
        public async Task<string> AddQuestionnaire(FormType formType, bool allowNew=false) //string formId)//string userId, string formId
        {
            Logger.Debug($"AddQuestionnaire Started with allowNew flag set to '{allowNew}'");

            try
            {
                //Get the Form from the Forms Lookup based on the Form Type.
                //This function will be used to add a Form as Questionnaire for the borrower.
                //Get the 'Form' from the Lookup.
                var userName = await GetCurrentUser();

                //before getting the form check whether there exists any form for the given user
                Logger.Debug($"Checking if application already exist for user : {userName}");
                
                var temporaryApplicationNumber = await QuestionnaireRepository.GetApplicationNumberGivenUserName(userName);

                if (allowNew || string.IsNullOrEmpty(temporaryApplicationNumber))
                {
                    Logger.Debug($"Creating a new application");
                    //Generate the TemporaryApplicationNumber
                    var applicationNumberTask = TemporaryApplicationNumberGenerator.TakeNext("docApplication");

                    //if form does not exist for the user, then create a questionnaire for the user
                    var form = await FormRepository.GetActive(formType.ToString());
                    if (form == null)
                    {
                        Logger.Debug($"Form not found : {formType}");
                        throw new FormDoesNotExistsException($"Form '{ formType }' does not exist.");
                    }

                    //Prefill the values in the questionnaire. These includes the email and the phone number.
                    Logger.Debug($"Prefilling values");
                    var formTask = PrefillValues(form);
                    temporaryApplicationNumber = applicationNumberTask.Result;
                    Logger.Debug($"Creating an application with Temporary Application Number : {temporaryApplicationNumber}");
                    form = formTask.Result;
                    var questionnaireTask = QuestionnaireRepository.AddQuestionnaire(userName, form, temporaryApplicationNumber, allowNew);
                    
                    //Now assign the Loan officer to the Questionnaire                    
                    Logger.Debug($"ApplicationTeamAssignment: {userName} for temporaryApplicationNumber: {temporaryApplicationNumber} started.");
                    var userApplicationAssignmentResult = await InviteService.Assign("application", temporaryApplicationNumber, userName);
                    Logger.Debug($"ApplicationTeamAssignment: {userName} for temporaryApplicationNumber: {temporaryApplicationNumber} ended.");
                    List<Task> tasks = new List<Task>();
                    if (userApplicationAssignmentResult != null && !string.IsNullOrWhiteSpace(userApplicationAssignmentResult.Assignee))
                    {
                        Logger.Debug($"LoanOffcer assignment started for application {temporaryApplicationNumber}");
                        var assignedLoanOfficerToQuestionnaire = userApplicationAssignmentResult.Assignee;
                        questionnaireTask.Wait();
                        tasks.Add(QuestionnaireRepository.AssignLoanOfficerToQuestionnaire(temporaryApplicationNumber, assignedLoanOfficerToQuestionnaire));
                        //Remove the loan officer subsection.
                        var loanOfficerSectionId = new List<string>
                        {
                            ConstantUtils.LoanOfficerSectionId
                        };
                        tasks.Add(QuestionSubSectionsRepository.RemoveSubSections(temporaryApplicationNumber, Convert.ToInt32(SectionDetails.Profile), loanOfficerSectionId));
                    }
                    else
                    {
                        tasks.Add(questionnaireTask);
                    }
                    Logger.Debug($"Waiting for all tasks to finish for {temporaryApplicationNumber}");
                    await Task.WhenAll(tasks);
                    //we have to publish questionnaire application created event for assighment related changes.
                    Logger.Debug($"Publishing QuestionnaireApplicationCreate for ApplicationNumber {temporaryApplicationNumber}");
                    await EventHubClient.Publish(new QuestionnaireApplicationCreated(temporaryApplicationNumber));
                }
                
                return temporaryApplicationNumber;
            }
            catch (QuestionnaireAlreadyExistsException ex)
            {
                throw new QuestionnaireAlreadyExistsException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #endregion

        #region Rental Copied all dependant fields

        public async Task UpdateRefinanceAddressSameQuestions(string temporaryApplicationNumber,int sectionId, string subSectionId)
        {
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);
            Logger.Info($"... UpdateRefinanceAddressSameQuestions - started");
            var refinanceAdressSameDependantFields = QuestionnaireConfigurations.RefinanceAdressSameDependantFieldsList;

            var fieldquestionList = refinanceAdressSameDependantFields.Select(x=>x.Field).ToList().Select(y=>y.QuestionId).ToList<string>();

            var updateAnswers = new List<Task<bool>>();

            IQuestion questionData = null;
            foreach (var qId in fieldquestionList)
            {
                questionData = GetQuestionDataFromQuestionId(questionnaire,sectionId,subSectionId,qId);
                updateAnswers.Add(UpdateRefinanceDuplicateQueIds(temporaryApplicationNumber,questionData));
            }

            await Task.WhenAll(updateAnswers);
            Logger.Info($"... UpdateRefinanceAddressSameQuestions - ended");
        }

        private IQuestion GetQuestionDataFromQuestionId(IQuestionnaire  questionnaire,int sectionId, string subSectionId, string questionId)
        {
            var question = questionnaire.ApplicationForm.Sections.Where(x => x.SectionId == sectionId)
                                                            .SelectMany(x => x.SubSections).Where(x => x.SubSectionId == subSectionId)
                                                            .SelectMany(x => x.QuestionSections)
                                                            .SelectMany(x => x.QuestionList).Where(x => x.QuestionId == questionId).FirstOrDefault();
            return question;
        }
        
        private async Task<bool> UpdateRefinanceDuplicateQueIds(string  temporaryApplicationNumber,IQuestion questionToUpdate)
        {
            Logger.Info($"... UpdateRefinanceAddressSameQuestions - started");
            var refinanceAdressSameDependantFields = QuestionnaireConfigurations.RefinanceAdressSameDependantFieldsList;

            if(questionToUpdate == null)
            {
                Logger.Error($"... Error in QuestionId update for application - {temporaryApplicationNumber} and QuestionId - {questionToUpdate.QuestionId}");
                 throw new Exception("questionToUpdate not found");
            }


            var copiedFields = refinanceAdressSameDependantFields.Where(x=> x.Field.QuestionId == questionToUpdate.QuestionId).SelectMany(x=>x.FieldCopies).ToList();

            foreach (var copiedField in copiedFields)
            {
                IQuestionnaireQuestionAnswer questionAnswerData = new QuestionnaireQuestionAnswer
                {
                    TemporaryApplicationNumber = temporaryApplicationNumber,
                    SectionId = Convert.ToInt32(SectionDetails.Profile),
                    QuestionSectionSeqNo =  copiedField.QuestionSequence,
                    QuestionId = copiedField.QuestionId,
                    SubSectionId = copiedField.SubSectionId,
                    Answer =questionToUpdate.Answer
                }; 

                await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionAnswerData);
                Logger.Info($"... Update Answer for QuestionId - {copiedField.QuestionId}");
            }

            return true;
        }

        #endregion

        #region "Remove SubSection based on action Type"
            public async Task<bool> RemoveSubSectionAndAnswer(string applicationNumber,SubSectionIdList subSectionIds,int sectionId,string actionType)
            {
                List<IBorrowerCoBorrowerInfo> borrowerList = null;
                bool resultFlag = true;
                //var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);            
                borrowerList = await GetBorrowersAndCoBorrowersAndSpouse(applicationNumber, false);

                if (borrowerList == null)
                    return false;

                var loggedInUser = await GetCurrentUser();

                //get the borrower user type #endregion
                var borrower = borrowerList.FirstOrDefault(x => x.ActualApplicantType == ApplicantType.Borrower);

                if(actionType.ToLower() == ApplicantType.Spouse.ToString().ToLower())  // add spouse co-borrower from yes to no , married to unmarrired,seperated
                {
                    //only logged in borrower spouse is removed

                    //get the all the user except spouse
                        var objBorrowerList = borrowerList.Where(x => x.ActualApplicantType != ApplicantType.Spouse).ToList();

                        if(objBorrowerList != null && borrower.UserName.Equals(loggedInUser))                        
                        {
                             List<string> objUserList = new List<string>();
                            var userList = objBorrowerList.Select(x => x.UserName).ToList();
                           
                            //need to add UserBased UserName if in case primary borrower spouse is invited yet and 
                            // add another co-borrower but not invited and removing sopuse invite "Do you wnat to add spouse as co-borrower" to no"
                            // no need to remove dynamic subsection of co-borrower (AddCoBorrower Screen)
                            userList.Add(USERBASED);   //"[UserBased]"; UserName      


                            objUserList.AddRange(userList);     
                                                     

                            await RemoveSubSection(applicationNumber,objUserList,sectionId,subSectionIds);

                            //update the logged in user spouseUserName in userState
                            await QuestionnaireRepository.UpdateSpouseUserNameInUserState(applicationNumber,loggedInUser);
                        }
                        else
                        {
                            resultFlag = false;
                            //need to log or throw errror only borrowe can remvoe his spouse or co-borrower
                             Logger.Info($"... only borrowe can remvoe his spouse for actiontype - {actionType}");
                        }
                        await ResetSendReminderForSpouse(applicationNumber, ConstantUtils.JointCreditSubSectionId);
                }
                else if(actionType.ToLower() == ApplicantType.CoBorrower.ToString().ToLower()) //add other co-borrower from yes to no
                {
                    //all the co-borrower is removed except borrower and his spouse.
                    //get the all the co-borrower except borrower and spouse.

                        var objBorrowerList = borrowerList.Where(x => (x.ActualApplicantType == ApplicantType.Spouse) || 
                        (x.ActualApplicantType == ApplicantType.Borrower)).ToList();

                        if(objBorrowerList != null && borrower.UserName.Equals(loggedInUser))                          
                        {
                             List<string> objUserList = new List<string>();
                            var userList = objBorrowerList.Select(x => x.UserName).ToList();
                            //need to add spouseUserBased UserName if in case primary borrower spouse is not invited yet and selectiong
                            // do you want to add other co-borrower answer is no that time
                            userList.Add(SPOUSEUSERBASED);   //"[SpouseUserBased]"; UserName                            
                           
                            objUserList.AddRange(userList);
                            
                            await RemoveSubSection(applicationNumber,objUserList,sectionId,subSectionIds);
                        }
                        else
                        {
                             resultFlag = false;
                            //need to log or throw errror only borrowe can remvoe his spouse or co-borrower
                            Logger.Info($"... only borrower can remvoe his spouse or co-borrower for actionType - {actionType}");
                        }
                }
                else if(actionType.ToLower() == ApplicantType.CoBorrowerSpouse.ToString().ToLower())  // coborrower added spouse and married to un married,seperated
                {
                    //only logged in co-borrower spouse is removed 
                    //first get the logged in user from userState and his spouse info.

                    var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(applicationNumber);
                    if(questionnaire != null)
                    {
                        //get the userState and find the spouse userid / username.
                        var userState = questionnaire.UserState.FirstOrDefault(u => u.UserName == loggedInUser);
                        if(userState != null)
                        {
                            if(!string.IsNullOrEmpty(userState.SpouseUserName))
                            {
                                var objBorrowerList = borrowerList.Where(x => x.UserName != userState.SpouseUserName).ToList();  

                                 List<string> objUserList = new List<string>();
                                 var userList = objBorrowerList.Select(x => x.UserName).ToList();
                           
                                objUserList.AddRange(userList);
                            
                                await RemoveSubSection(applicationNumber,objUserList,sectionId,subSectionIds);
                                  //update the logged in user spouseUserName in userState
                                 await QuestionnaireRepository.UpdateSpouseUserNameInUserState(applicationNumber,loggedInUser);
                            }
                            else
                            {
                                 resultFlag = false;
                                Logger.Info($"...co borrower spouse information not found for co-borrower - {loggedInUser} and actiontype - {actionType}");
                            }
                            await ResetSendReminderForCoBorrowerSpouse(applicationNumber, loggedInUser);
                        }
                        else
                        {
                             resultFlag = false;
                             Logger.Info($"...user information not found for - {loggedInUser} and actiontype - {actionType}");
                        }

                    }
                    else
                    {
                         resultFlag = false;
                     Logger.Info($"... application not found for application number - {applicationNumber} and actiontype - {actionType}");
                    }
                }
                else  //Joint to Individual credit.
                {     
                    //except borrower all the co-borrwer and spouse sub section must be removed.

                        if(borrower != null && borrower.UserName.Equals(loggedInUser))
                        {
                            List<string> objUserList = new List<string>();
                            objUserList.Add(borrower.UserName);
                          
                            await RemoveSubSection(applicationNumber,objUserList,sectionId,subSectionIds);

                             //update the logged in user spouseUserName in userState
                           await QuestionnaireRepository.UpdateSpouseUserNameInUserState(applicationNumber,loggedInUser);

                           await ResetSendReminderForSpouse(applicationNumber, ConstantUtils.JointCreditSubSectionId);
                        }
                        else{
                            //need to log or throw errror only borrowe can remvoe his spouse or co-borrower
                            resultFlag = false;
                             Logger.Info($"... only borrower can remvoe his spouse or co-borrower for actionType - {actionType}");
                           
                        }
                }
                
                return await Task.Run( () => resultFlag);
            }


            private async Task  RemoveSubSection(string applicationNumber,List<string> objUserList,int sectionId,SubSectionIdList subSectionIds)
            {
                    //remove all the subsection ,questionSection and answer except borrower subsection.
                    //first get all the section , subsection Id for given application.


                //we have to remove all the subsction except objUserList.
                var subSectionList = await QuestionSubSectionsRepository.GetSubSectionList(applicationNumber,objUserList);

                if(subSectionIds != null)  //extra subsection which are come from UI that need to add into existing list like , ApplicantInfo etc ...
                {
                    if(subSectionIds.SubSectionIds != null)
                    {
                        var extraSubSectionid = subSectionIds.SubSectionIds.Select
                        ( x=> new  SectionWithSubSectionId() { SectionId = sectionId, SubSectionId = x}).ToList();

                        if(extraSubSectionid != null)
                        {
                            subSectionList.AddRange(extraSubSectionid);
                        
                        }
                    }
                }

                if(subSectionList != null)
                {
                    var uniquesSectionId = subSectionList.Select(x => x.SectionId).Distinct();
                    foreach(var secId in uniquesSectionId)
                    {
                        var subSections = subSectionList.Where(x => x.SectionId == secId).Select(t => t.SubSectionId).ToList();
                        
                        //remove from question answer 
                        await  QuestionnaireAnswerRepository.Remove(applicationNumber,secId,subSections);

                            //now remove subsectionas.
                        await QuestionSubSectionsRepository.RemoveSubSections(applicationNumber,secId,subSections);                                  
                    }

                    var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(applicationNumber);

                    await expireInvitation(applicationNumber,objUserList,questionnaire);

                    await RemoveAndBlockUser(applicationNumber,objUserList,questionnaire);

                    //remove User from the questionnaire userstate 
                    await QuestionnaireRepository.RemoveFromUserState(applicationNumber,objUserList);                  
                }
            }

            //we have to get the invitation id for whom we have to expire the invitation.
            private async Task expireInvitation(string applicationNumber,List<string> objUserList,IQuestionnaire questionnaire)
            {
                try
                 { 
                    if(questionnaire != null)
                    {
                        var userListToBeExpired = questionnaire.UserState.Where(x => !objUserList.Contains(x.UserName));
                        if(userListToBeExpired.Count() > 0)
                        {
                            var inviteIdList = userListToBeExpired.Select(x => x.InviteId).ToList();
                            Logger.Debug("...Executing multiExpireInvites...");
                            await InviteService.ExpireInvitations(inviteIdList);
                            Logger.Debug("...Executed multiExpireInvites...");                            
                        }
                    }
                }
                catch(Exception ex)
                {
                    Logger.Error("...Error multiExpireInvites..." + ex.Message);
                }
            }


            //we have to block to removed user to access the application
            //remove form questionnaire filters
            private async Task RemoveAndBlockUser(string applicationNumber,List<string> objUserList,IQuestionnaire questionnaire)
            {
                try
                {                 
                    if(questionnaire != null)
                    {
                        var userListToBeRemoved = questionnaire.UserState.Where(x => !objUserList.Contains(x.UserName));
                        if(userListToBeRemoved.Count() > 0)
                        {
                            var applicantList = userListToBeRemoved.Select(x => x.UserName).ToList();

                            
                            var removeApplicantFromApplicationRule = QuestionnaireConfigurations.RemoveApplicantFromApplicationRule;
                                if(string.IsNullOrEmpty(removeApplicantFromApplicationRule))
                                {
                                    Logger.Debug("...removeApplicantFromApplicationRule rule not configured...");
                                    return;
                                }

                            Logger.Debug("...Executing removeApplicantFromApplication...");
                            await Task.Run(() => DecisionEngine.Execute<dynamic, JObject>(removeApplicantFromApplicationRule,
                                new
                                {
                                    applicationNumber = applicationNumber,
                                    applicantList = applicantList
                                }));
                            Logger.Debug("...Executed removeApplicantFromApplication...");
                            
                        }
                    }
                }
                catch(Exception ex){
                        Logger.Error("...Error removeApplicantFromApplication..." + ex.Message);

                    }
            }
        #endregion


        public async Task CopyAnswer(string fromApplicationNumber, string toApplicationNumber)
        {
            var fromApplication = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(fromApplicationNumber);
            var toApplication = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(toApplicationNumber);
            if (fromApplication == null)
            {
                throw new NotFoundException("{fromApplicationNumber} not found");
            }
            if (toApplication == null)
            {
                throw new NotFoundException("{toApplicationNumber} not found");
            }
            if (fromApplication.UserName != toApplication.UserName)
            {
                throw new InvalidOperationException("Username does not match");
            }

            var sourceFlowAnswers = await QuestionnaireAnswerRepository.GetByQuestionId(fromApplicationNumber, 1, "SS1", "-1");
            var destinationFlowAnswers = await QuestionnaireAnswerRepository.GetByQuestionId(toApplicationNumber, 1, "SS1", "-1");

            var sourceFlow = sourceFlowAnswers.FirstOrDefault()?.Answer;
            var destinationFlow = destinationFlowAnswers.FirstOrDefault()?.Answer;
            var key = $"{sourceFlow}.{destinationFlow}".ToLower();

            if (QuestionnaireConfigurations.AnswerCopyConfigurations == null ||
                !QuestionnaireConfigurations.AnswerCopyConfigurations.ContainsKey("Borrower"))
            {
                throw  new Exception("AnswerCopy configuration not found");
            }

            //need to update the from application number into new questionnaire application for reference.
            await QuestionnaireRepository.UpdateReferenceApplicationNumber(fromApplicationNumber,toApplicationNumber);

            var answerCopyConfiguration = QuestionnaireConfigurations.AnswerCopyConfigurations["Borrower"];

            var userNames = new List<string>()
            {
                fromApplication.UserName
            };

            var dynamicSubSections = await CopyDynamicSubSection(fromApplicationNumber, toApplicationNumber,sourceFlow, destinationFlow, userNames,
                answerCopyConfiguration.DynamicSubSectionFilterConfigurations);

            foreach (var dynamicSubSection in answerCopyConfiguration.CopyAnswerConfigurations)
            {
                if (dynamicSubSections.ContainsKey(dynamicSubSection.Key))
                {
                    dynamicSubSections[dynamicSubSection.Key].AddRange(dynamicSubSection.Value);
                }
                else
                {
                    dynamicSubSections.Add(dynamicSubSection.Key, dynamicSubSection.Value);
                }
            }


            await CopyAnswers(fromApplicationNumber, toApplicationNumber, sourceFlow, destinationFlow,
                dynamicSubSections);

            if (answerCopyConfiguration.CustomMappingForAnswerCopies!=null && answerCopyConfiguration.CustomMappingForAnswerCopies.ContainsKey(key))
            {
                var customMappingForAnswerCopies = answerCopyConfiguration.CustomMappingForAnswerCopies[key];
                await CopyAnswerForCustomMappings(fromApplicationNumber, toApplicationNumber,
                    customMappingForAnswerCopies);
            }           
        }

        public async Task<bool> CopyAnswerForSpouse(string fromApplicationNumber, string toApplicationNumber)
        {
            var fromApplication = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(fromApplicationNumber);
            var toApplication = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(toApplicationNumber);
            if (fromApplication == null)
            {
                throw new NotFoundException("{fromApplicationNumber} not found");
            }
            if (toApplication == null)
            {
                throw new NotFoundException("{toApplicationNumber} not found");
            }
            
            if (fromApplication.UserName != toApplication.UserName)
            {
                throw new InvalidOperationException("Username does not match");
            }                        

            var sourceFlowAnswers = await QuestionnaireAnswerRepository.GetByQuestionId(fromApplicationNumber, 1, "SS1", "-1");
            var destinationFlowAnswers = await QuestionnaireAnswerRepository.GetByQuestionId(toApplicationNumber, 1, "SS1", "-1");

            var sourceFlow = sourceFlowAnswers.FirstOrDefault()?.Answer;
            var destinationFlow = destinationFlowAnswers.FirstOrDefault()?.Answer;
            var key = $"{sourceFlow}.{destinationFlow}".ToLower();
                                    
            var primaryBorrowerData =
                fromApplication.UserState.FirstOrDefault(u => u.UserName == fromApplication.UserName);
            if (primaryBorrowerData == null)
            {
                throw new InvalidOperationException("Invalid username");
            }
            if(string.IsNullOrWhiteSpace(primaryBorrowerData.SpouseUserName))
            {
                return false;
            }

                var spouseBorrowerData =
                    fromApplication.UserState.FirstOrDefault(u => u.UserName == primaryBorrowerData.SpouseUserName);
                if (spouseBorrowerData == null)
                {
                    throw new InvalidOperationException("Spouse information not found");
                }

                if (QuestionnaireConfigurations.AnswerCopyConfigurations == null ||
                    !QuestionnaireConfigurations.AnswerCopyConfigurations.ContainsKey("Spouse"))
                {
                    throw new Exception("AnswerCopy configuration not found for spouse");
                }

                var answerCopyConfiguration = QuestionnaireConfigurations.AnswerCopyConfigurations["Spouse"];

                spouseBorrowerData.LastAccessedSection = 1;
                spouseBorrowerData.LastAccessedSectionSeqNo = 1;
                spouseBorrowerData.LastAccessedSubSectionSeqNo = 1;
                spouseBorrowerData.HighestSectionReached = 1;
                spouseBorrowerData.Status = FormStatus.Open;                             
                var toUserState = toApplication.UserState;
                toUserState.Add(spouseBorrowerData);
                toUserState.First(p => p.UserName == primaryBorrowerData.UserName).SpouseUserName = spouseBorrowerData.UserName;

                await QuestionnaireRepository.UpdateUserStates(toApplicationNumber, toUserState);

                //TODO: What if spouse accepting invitation later on?
                //TODO: Do we need to send invitation again?
                var userNames = new List<string>()
            {
                primaryBorrowerData.SpouseUserName
            };

                var dynamicSubSections = await CopyDynamicSubSection(fromApplicationNumber, toApplicationNumber, sourceFlow, destinationFlow, userNames,
                    answerCopyConfiguration.DynamicSubSectionFilterConfigurations);

                foreach (var dynamicSubSection in answerCopyConfiguration.CopyAnswerConfigurations)
                {
                    if (dynamicSubSections.ContainsKey(dynamicSubSection.Key))
                    {
                        dynamicSubSections[dynamicSubSection.Key].AddRange(dynamicSubSection.Value);
                    }
                    else
                    {
                        dynamicSubSections.Add(dynamicSubSection.Key, dynamicSubSection.Value);
                    }
                }


                await CopyAnswers(fromApplicationNumber, toApplicationNumber, sourceFlow, destinationFlow,
                    dynamicSubSections);

                if (answerCopyConfiguration.CustomMappingForAnswerCopies != null && answerCopyConfiguration.CustomMappingForAnswerCopies.ContainsKey(key))
                {
                    var customMappingForAnswerCopies = answerCopyConfiguration.CustomMappingForAnswerCopies[key];
                    await CopyAnswerForCustomMappings(fromApplicationNumber, toApplicationNumber,
                        customMappingForAnswerCopies);
                }

                // To resend invitation while copy spouse data.
                await SendInviteReminderGivenInviteId(spouseBorrowerData.InviteId, toApplicationNumber);
            if (spouseBorrowerData.InviteId != spouseBorrowerData.UserName)
            {
                await EventHubClient.Publish(new SpouseCopiedSuccessFully(spouseBorrowerData.UserName, toApplicationNumber));
            }
            return true;
        }

        private async Task CopyAnswerForCustomMappings(string fromApplicationNumber, string toApplicationNumber,List<CustomMappingForAnswerCopy> customMappingForAnswerCopies)
        {
            foreach (var item in customMappingForAnswerCopies.GroupBy(i=>i.SectionId))
            {
                var answers =await QuestionnaireAnswerRepository.GetBySectionId(fromApplicationNumber, item.Key);
                var answersToCopy = new List<IQuestionnaireQuestionAnswer>();
                foreach (var customMappingForAnswerCopy in item)
                {
                    var answer=answers.FirstOrDefault(i => i.SubSectionId==customMappingForAnswerCopy.SubSectionId && i.QuestionId == customMappingForAnswerCopy.Source.QuestionId);
                    if (answer == null) continue;
                    answer.Id = null;
                    answer.TemporaryApplicationNumber = toApplicationNumber;
                    answer.QuestionSectionSeqNo = customMappingForAnswerCopy.Destination.QuestionSeqNumber;
                    answer.QuestionId = customMappingForAnswerCopy.Destination.QuestionId;
                    answersToCopy.Add(answer);
                }

                if (answersToCopy.Any())
                {
                    await QuestionnaireAnswerRepository.InsertAnswers(answersToCopy);
                }
            }   
        }
        
        private async Task<Dictionary<int,List<SectionFilterConfiguration>>> CopyDynamicSubSection(string fromApplicationNumber, string toApplicationNumber,string sourceWorkflowType, string destinationWorkflowType, List<string> filterByUserNames , Dictionary<int,DynamicSubSectionFilterConfiguration> dynamicSubSectionFilterConfigurations)
        {
            if (filterByUserNames == null) throw new ArgumentNullException(nameof(filterByUserNames));
            if (dynamicSubSectionFilterConfigurations == null)
                throw new ArgumentNullException(nameof(dynamicSubSectionFilterConfigurations));
            if (string.IsNullOrWhiteSpace(fromApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(fromApplicationNumber));
            if (string.IsNullOrWhiteSpace(toApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(toApplicationNumber));
            var dynamicAnswersToCopy = new Dictionary<int, List<SectionFilterConfiguration>>();
            
            foreach (var sectionIdWithDynamicSubSection in dynamicSubSectionFilterConfigurations)
            {
                var templateIds = sectionIdWithDynamicSubSection.Value.TemplateIds;
                if (string.Equals(sourceWorkflowType, destinationWorkflowType,StringComparison.OrdinalIgnoreCase) && sectionIdWithDynamicSubSection.Value.OnlyIfWorkflowIsSameTemplateIds!=null && sectionIdWithDynamicSubSection.Value.OnlyIfWorkflowIsSameTemplateIds.Any())
                {
                    templateIds.AddRange((sectionIdWithDynamicSubSection.Value.OnlyIfWorkflowIsSameTemplateIds));
                }
                //Get the subsection from the repository
                var sectionsToCopy =await QuestionSubSectionsRepository.GetAllBySectionIdWithoutDetail(fromApplicationNumber, sectionIdWithDynamicSubSection.Key);
                var originalSubSections =await QuestionSubSectionsRepository.GetAllBySectionIdWithoutDetail(toApplicationNumber, sectionIdWithDynamicSubSection.Key);
                
                //Copy the subsection for the given users only
                sectionsToCopy = sectionsToCopy.Where(x => filterByUserNames.Contains(x.UserName, StringComparer.OrdinalIgnoreCase)).ToList();

                bool removeExistingSubSection = false;
                //If filter set by templateId filter the same
                if (templateIds!=null && templateIds.Any())
                {
                    sectionsToCopy = sectionsToCopy.Where(x =>
                        templateIds.Contains(x.TemplateId,
                            StringComparer.OrdinalIgnoreCase)).ToList();
                    if (sectionIdWithDynamicSubSection.Value.DeleteExistingTemplates!=null && sectionIdWithDynamicSubSection.Value.DeleteExistingTemplates.Any())
                    {
                        removeExistingSubSection = true;
                    }
                }
                
                var questionSectionsToCopy= await QuestionSectionsRepository.GetBySectionId(fromApplicationNumber, sectionIdWithDynamicSubSection.Key);
                var originalQuestionSections= await QuestionSectionsRepository.GetBySectionId(toApplicationNumber, sectionIdWithDynamicSubSection.Key);
                
                
                //Note: It's filtering already added question sections
                var originalSubSectionIds = originalQuestionSections.Select(x => x.SubSectionId);
                if (removeExistingSubSection == true)
                {
                    var subSectionIdsToRemove = new List<string>();
                    foreach (var originalSubSection in originalSubSections.Where(o=>sectionIdWithDynamicSubSection.Value.DeleteExistingTemplates.Contains(o.TemplateId, StringComparer.OrdinalIgnoreCase)))
                    {
                        subSectionIdsToRemove.Add(originalSubSection.SubSectionId);
                        QuestionSubSectionsRepository.Remove(originalSubSection);
                    }
                    
                    foreach (var originalQuestionSection in originalQuestionSections.Where(o=>subSectionIdsToRemove.Contains(o.SubSectionId,StringComparer.OrdinalIgnoreCase)))
                    {
                        QuestionSectionsRepository.Remove(originalQuestionSection);
                    }
                }

                questionSectionsToCopy = questionSectionsToCopy.Where(x => !originalSubSectionIds.Contains(x.SubSectionId)).ToList();
                
                //Note: It's filtering question section for the user
                var subSectionIds = sectionsToCopy.Select(x => x.SubSectionId);
                questionSectionsToCopy = questionSectionsToCopy.Where(x => subSectionIds.Contains(x.SubSectionId,StringComparer.OrdinalIgnoreCase)).ToList();

                if (questionSectionsToCopy.Any())
                {
                    foreach (var questionSection in questionSectionsToCopy)
                    {
                        questionSection.Id = null;
                        questionSection.ApplicationNumber = toApplicationNumber;
                    }

                    await QuestionSectionsRepository.Add(questionSectionsToCopy);    
                }
                
                var subSectionConfigurations =
                    sectionsToCopy.Select(x => new SectionFilterConfiguration(x.SubSectionId)).ToList();
                
                //NOTE: Copying before filtering as it required to copy the answers
                if (dynamicAnswersToCopy.ContainsKey(sectionIdWithDynamicSubSection.Key))
                {
                    dynamicAnswersToCopy[sectionIdWithDynamicSubSection.Key].AddRange(subSectionConfigurations);
                }
                else
                {
                    dynamicAnswersToCopy.Add(sectionIdWithDynamicSubSection.Key, subSectionConfigurations);
                }

                subSectionIds=originalSubSections.Select(x => x.SubSectionId);
                
                sectionsToCopy = sectionsToCopy.Where(x => !subSectionIds.Contains(x.SubSectionId)).ToList();

                if (sectionsToCopy.Any())
                {
                    foreach (var sectionToCopy in sectionsToCopy)
                    {
                        sectionToCopy.Id = null;
                        sectionToCopy.ApplicationNumber = toApplicationNumber;
                    }

                    await QuestionSubSectionsRepository.AddSubSections(toApplicationNumber, sectionIdWithDynamicSubSection.Key, sectionsToCopy);    
                }
                
            }

            return dynamicAnswersToCopy;

        }

        private async Task CopyAnswers(string fromApplicationNumber, string toApplicationNumber, string sourceWorkflowType, string destinationWorkflowType,Dictionary<int,List<SectionFilterConfiguration>> copyAnswerConfigurations)
        {
            foreach (var section in copyAnswerConfigurations)
            {
                var answers = await QuestionnaireAnswerRepository.GetBySectionId(fromApplicationNumber, section.Key);
                var listOfSubSectionIds = new List<string>();
                var idsToExclude = new List<string>();
                foreach (var sectionFilterConfiguration in section.Value)
                {
                    if (!sectionFilterConfiguration.OnlyIfWorkflowIsSame)
                    {
                        listOfSubSectionIds.Add(sectionFilterConfiguration.SubSectionId);
                    }
                    else if (string.Equals(sourceWorkflowType, destinationWorkflowType,
                                 StringComparison.OrdinalIgnoreCase))
                    {
                        listOfSubSectionIds.Add(sectionFilterConfiguration.SubSectionId);
                    }

                    if (sectionFilterConfiguration.ExcludeQuestionIds != null && sectionFilterConfiguration.ExcludeQuestionIds.Any())
                    {
                        var tmp = answers.Where(a =>
                            a.SubSectionId == sectionFilterConfiguration.SubSectionId &&
                            sectionFilterConfiguration.ExcludeQuestionIds.Contains(a.QuestionId,
                                StringComparer.OrdinalIgnoreCase)).ToList();
                        idsToExclude.AddRange(tmp.Select(i => i.Id));
                    }
                }

                var answersToCopy = answers.Where(i =>
                    listOfSubSectionIds.Contains(i.SubSectionId, StringComparer.OrdinalIgnoreCase) &&
                    i.QuestionId != "-1").ToList();

                if (idsToExclude.Any())
                {
                    answersToCopy = answersToCopy.Where(i => !idsToExclude.Contains(i.Id)).ToList();
                }
                
                foreach (var questionnaireQuestionAnswer in answersToCopy)
                {
                    questionnaireQuestionAnswer.Id = null;
                    questionnaireQuestionAnswer.TemporaryApplicationNumber = toApplicationNumber;
                }

                if (answersToCopy.Any())
                {
                    await QuestionnaireAnswerRepository.InsertAnswers(answersToCopy);
                }

            }
        }

        public async Task<bool> VerifyApplication(string applicationNumber)
        {
            var userName = await GetCurrentUser();
            var questionnaire = await QuestionnaireRepository.GetQuestionnaireWithoutDetail(applicationNumber);
            if (questionnaire == null)
                throw new QuestionnaireNotFoundException($"Questionnaire data not found for {applicationNumber}");
            return questionnaire.UserState.Any(y => y.UserName == userName);
        }


/// <summary>
/// this method is used to set the applicaiton from joint credit to individual.
/// </summary>
/// <param name="applicationNumber"></param>
/// <returns></returns>
        public async Task<bool> JointToIndividualCredit(string applicationNumber)
        {
            bool status = false;
             var jointToIndividualFieldMap = QuestionnaireConfigurations.QuestionnaireJointToIndividual; 

                if(jointToIndividualFieldMap != null)
                {
                     if (jointToIndividualFieldMap.Map != null)
                        {
                        
                            var sectionId = jointToIndividualFieldMap.Map.SectionId;
                            var subSectionId = jointToIndividualFieldMap.Map.SubSectionId;
                            var questionId = jointToIndividualFieldMap.Map.QuestionId;
                            var questionSectionSeqNo = jointToIndividualFieldMap.Map.QuestionSectionSeqNo;

                            //now update the answer.

                            IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                            answerRequest.TemporaryApplicationNumber = applicationNumber;
                            answerRequest.SectionId = sectionId;
                            answerRequest.SubSectionId = subSectionId;
                            answerRequest.QuestionSectionSeqNo = questionSectionSeqNo;
                            answerRequest.QuestionId = questionId;
                            answerRequest.Answer = jointToIndividualFieldMap.Map.QuestionAnswer; //Individual Credit

                           status = await UpdateQuestionAndStatus(answerRequest); 
                           if(status) 
                           {
                                
                               //remove all the co-borrower subsection and section.
                                //co-borrower invitation sub-section
                                
                                var coBorrowerInvitationInfoSubSectionId = await QuestionSubSectionsRepository.GetSubSectionIdByTemplateName(
                                                applicationNumber,
                                                SectionConstants.CoBorrowerInvitationTemplateName, // "coBorrowerInvitationInfo",
                                                Convert.ToInt32(SectionDetails.Profile)
                                            );
                                            SubSectionIdList subSectionList = null;
                            if(!string.IsNullOrEmpty(coBorrowerInvitationInfoSubSectionId))
                            {   
                                 subSectionList = new SubSectionIdList();
                                 subSectionList.SubSectionIds = new List<string>();
                                 subSectionList.SubSectionIds.Add(coBorrowerInvitationInfoSubSectionId);
                            }

                            await RemoveSubSectionAndAnswer(applicationNumber,subSectionList,sectionId,"All");

                           }                         

                        }

                        return status;
                }
                return status;
        }

        /// <summary>
        /// This method is used to remove co-borrwer related information
        /// and set "Do you want to add another co-borrower?" answer to no.
        /// </summary>
        /// <param name="applicationNumber">Application number</param>
        /// <returns>return true/false</returns>
         public async Task<bool> RemoveCoBorrower(string applicationNumber)
        {
            bool status = false;
             var addCoBorrowerToNoQuestion = QuestionnaireConfigurations.QuestionnaireAddCoBorrowerToNo; 

                if(addCoBorrowerToNoQuestion != null)
                {
                     if (addCoBorrowerToNoQuestion.Map != null)
                        {
                        
                            var sectionId = addCoBorrowerToNoQuestion.Map.SectionId;
                            var subSectionId = addCoBorrowerToNoQuestion.Map.SubSectionId;
                            var questionId = addCoBorrowerToNoQuestion.Map.QuestionId;
                            var questionSectionSeqNo = addCoBorrowerToNoQuestion.Map.QuestionSectionSeqNo;

                            //now update the answer.

                            IAnswerUpdateRequest answerRequest = new AnswerUpdateRequest();
                            answerRequest.TemporaryApplicationNumber = applicationNumber;
                            answerRequest.SectionId = sectionId;
                            answerRequest.SubSectionId = subSectionId;
                            answerRequest.QuestionSectionSeqNo = questionSectionSeqNo;
                            answerRequest.QuestionId = questionId;
                            answerRequest.Answer = addCoBorrowerToNoQuestion.Map.QuestionAnswer; //Individual Credit

                           status = await UpdateQuestionAndStatus(answerRequest); 
                           if(status) 
                           {
                                
                               //remove all the co-borrower subsection and section.
                                //co-borrower invitation sub-section
                                
                                var coBorrowerInvitationInfoSubSectionId = await QuestionSubSectionsRepository.GetSubSectionIdByTemplateName(
                                                applicationNumber,
                                                SectionConstants.CoBorrowerInvitationTemplateName, // "coBorrowerInvitationInfo",
                                                Convert.ToInt32(SectionDetails.Profile)
                                            );
                                            SubSectionIdList subSectionList = null;
                            if(!string.IsNullOrEmpty(coBorrowerInvitationInfoSubSectionId))
                            {   
                                 subSectionList = new SubSectionIdList();
                                 subSectionList.SubSectionIds = new List<string>();
                                 subSectionList.SubSectionIds.Add(coBorrowerInvitationInfoSubSectionId);
                            }

                            await RemoveSubSectionAndAnswer(applicationNumber,subSectionList,sectionId,"CoBorrower");

                           }                         

                        }

                        return status;
                }
                return status;
        }
    }
}
