﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using loanPurpose = Docitt.Questionnaire.LoanPurpose;
#if DOTNET2

#else
using Microsoft.AspNet.Http;
#endif

namespace Docitt.Questionnaire
{
    public partial class QuestionnaireService
    {
        
        /// <summary>
        /// This function Appends question section
        /// </summary>
        /// <param name="questionSectionRequest"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        public async Task<bool> AppendQuestionSection(QuestionSection questionSectionRequest, string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            var section= await QuestionnaireRepository.GetSectionById(temporaryApplicationNumber, sectionId);
            if (section== null)
            {
                throw new SubSectionDoesNotExistsException($"Subsection {subSectionId} or Section having SectionId {sectionId} does not exists for {temporaryApplicationNumber}.");
            }

            var subSection = section.SubSections.Find(ss => ss.SubSectionId == subSectionId);

            if (subSection != null)
            {
                subSection.QuestionSections.Add(questionSectionRequest);
                questionSectionRequest.ApplicationNumber = temporaryApplicationNumber;
                questionSectionRequest.SectionId = sectionId;
                questionSectionRequest.SubSectionId = subSectionId;
                await QuestionSectionsRepository.Add(questionSectionRequest);                 
            }
            else
            {
                throw new SubSectionDoesNotExistsException($"Subsection {subSectionId} or Section having SectionId {sectionId} does not exists.");
            }

            return true;
        }

        /// <summary>
        /// This function deletes the Question Section based on the Section id, sub section id and question section seq no.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="questionSectionSeqNo"></param>
        /// <returns></returns>
        public async Task<bool> DeleteQuestionSection(string temporaryApplicationNumber, int sectionId, string subSectionId, int questionSectionSeqNo)
        {
            IQuestionSection questionSection = null;

            try
            {
                var status = false;

                //This function assigns Application Number to the Questionnaire.
                //Get Questionnaire for the applicant.
                IQuestionnaire questionnaire = new Questionnaire();
                questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

                if (questionnaire != null)
                {
                    var section = questionnaire.ApplicationForm.Sections.FirstOrDefault(s => s.SectionId == sectionId);
                    if (section != null)
                    {
                        var subSection = section.SubSections.FirstOrDefault(x => x.SubSectionId == subSectionId);
                        if (subSection != null)
                        {
                            questionSection = subSection.QuestionSections.FirstOrDefault(x => x.SeqNo == questionSectionSeqNo);
                            if (questionSection != null)
                            {
                                if (questionSection.QuestionSectionName == "AddCoBorrower")
                                {
                                    Logger.Debug("Inside AddCoborrower question section...");
                                    var AddCoBorrowerModal = new AddCoBorrowerModal();
                                    if (questionSection.QuestionList != null && questionSection.QuestionList.Count > 0)
                                    {
                                        var question = questionSection.QuestionList[0];
                                        
                                        if (!string.IsNullOrWhiteSpace(question.Answer))
                                        {
                                            var questionId = question.QuestionId;
                                            var answer = question.Answer;
                                            var inviteId = question.ResultQuestionId;
                                            
                                            AddCoBorrowerModal = JsonConvert.DeserializeObject<AddCoBorrowerModal>(answer);
                                            AddCoBorrowerModal.QuestionId = questionId;
                                            AddCoBorrowerModal.EmailAddress = AddCoBorrowerModal.EmailAddress.ToLower();

                                            Logger.Debug($"Start DeleteQuestionSection 1 - SubSectionId - {subSectionId}");
                                            await QuestionSectionsRepository.Remove(temporaryApplicationNumber, questionSection, sectionId, subSectionId);
                                            status = true;
                                            Logger.Debug($"Completed DeleteQuestionSection 1 - SubSectionId - {subSectionId}");

                                            var currentUser = await IdentityService.GetUser(AddCoBorrowerModal.EmailAddress);
                                            if (currentUser == null)
                                            {
                                                try
                                                {
                                                    var borrowerEmail = await GetCurrentUser();
                                                    Logger.Debug($"Expire invite started for {inviteId}");
                                                    await InviteService.ExpireInvitation(inviteId);
                                                    Logger.Debug($"Expire invite ended for {inviteId}");

                                                    Logger.Debug("...Removing from UserState...");
                                                    // var userState = questionnaire.UserState.Where(u => u.UserName == AddCoBorrowerModal.EmailAddress).FirstOrDefault();
                                                    var userState = questionnaire.UserState.Where(u => u.UserName == inviteId || u.UserName == AddCoBorrowerModal.EmailAddress).FirstOrDefault();
                                                    if(userState != null)
                                                    {
                                                        questionnaire.UserState.Remove(userState);
                                                        await QuestionnaireRepository.UpdateUserStates(temporaryApplicationNumber, questionnaire.UserState); // Update UserStates
                                                        status = true;
                                                    }

                                                    Logger.Debug("...Removed from UserState...");
                                                }
                                                catch { }
                                                
                                                Logger.Debug("...User is not registered, so we can delete added subsections...");
                                                
                                                var userSectionIds = questionnaire.ApplicationForm.Sections.Select(s => s.SectionId).ToArray();
                                                foreach (var userSectionId in userSectionIds)
                                                {
                                                    var userSubSectionIds = questionnaire.ApplicationForm.Sections.Where(s => s.SectionId == userSectionId)
                                                        .SelectMany(s => s.SubSections).Where(ss => ss.UserName == inviteId || ss.UserName == AddCoBorrowerModal.EmailAddress)
                                                        .Select(ss => ss.SubSectionId).ToArray();
                                                    if (userSubSectionIds != null)
                                                    {
                                                        foreach (var userSubSectionId in userSubSectionIds)
                                                        {
                                                            Logger.Debug("Deleting subsection -> " + userSubSectionId);
                                                            var retVal = await DeleteSubSection(temporaryApplicationNumber, userSectionId, userSubSectionId, questionnaire);
                                                            if (retVal)
                                                                Logger.Debug("Deleted subsection -> " + userSubSectionId);
                                                            else
                                                                Logger.Debug("Subsection not found -> " + userSubSectionId);
                                                        }
                                                    }
                                                }
                                                Logger.Debug($"Start DeleteQuestionSection 2 - SubSectionId - {subSectionId}");
                                                await QuestionSectionsRepository.Remove(temporaryApplicationNumber, questionSection, sectionId, subSectionId);
                                                status = true;
                                                Logger.Debug($"Complete DeleteQuestionSection 2 - SubSectionId - {subSectionId}");
                                            }
                                            else
                                            {
                                                Logger.Debug("...User already registered...");
                                            }
                                        }                               
                                        else {
                                            Logger.Debug($"Start DeleteQuestionSection 1 - SubSectionId - {subSectionId}");
                                            await QuestionSectionsRepository.Remove(temporaryApplicationNumber, questionSection, sectionId, subSectionId);
                                            status = true;
                                            Logger.Debug($"Completed DeleteQuestionSection 1 - SubSectionId - {subSectionId}");
                                        }                               
                                    }
                                    
                                }
                                else
                                {
                                    await QuestionSectionsRepository.Remove(temporaryApplicationNumber, questionSection, sectionId, subSectionId);
                                    status = true;
                                }
                                
                            }
                        }
                    }
                }

                return status;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
