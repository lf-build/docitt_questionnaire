using System;
using System.Collections.Generic;
using System.Linq;

namespace Docitt.Questionnaire
{
    public class InviteValidationService : IInviteValidationService
    {

        //TODO Move this const to questionnaire configuration for dynamic validations
        const string INDIVIDUAL_CREDIT_ANSWER = "Individual Credit";
        const string JOINT_CREDIT_ANSWER = "Joint Credit";
        const string MARRIED_ANSWER = "Married";
        const string FIELD_INDIVIDUAL_JOINT = "BorrowerIndividualOrJointCredit";
        const string FIELD_MARITAL_STATUS = "BorrowerMaritalStatusMarried";
        const string FIELD_ADD_SPOUSE_COBORROWER = "BorrowerAddSpouseAsCoBorrower";
        const string TEMPLATE_SPOUSE_INFO = "spouseInfo";
        const string TEMPLATE_SPOUSE_COMMUNICATION_INFO = "spouseCommunicationInfo";


        private static bool IsJointApplication(List<ISubSection> subSectionsSpouse)
        {
            var jointCreditSubsectionQuestionList = subSectionsSpouse[0].QuestionSections.SelectMany(x=>x.QuestionList);
            var individualOrJoint = jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == FIELD_INDIVIDUAL_JOINT);
            if (individualOrJoint == null || string.IsNullOrEmpty(individualOrJoint.Answer))
                return false;
            
            return individualOrJoint.Answer.Equals(JOINT_CREDIT_ANSWER,StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsBorrowerMarried(List<ISubSection> subSectionsSpouse)
        {
            var result = IsJointApplication(subSectionsSpouse);

            if (!result) return false;
            
            var jointCreditSubsectionQuestionList = subSectionsSpouse[0].QuestionSections.SelectMany(x=>x.QuestionList);
            var borrowerMaritalStatus = jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == FIELD_MARITAL_STATUS);
                
            return borrowerMaritalStatus != null && borrowerMaritalStatus.Answer.Equals(MARRIED_ANSWER,StringComparison.OrdinalIgnoreCase);
        }

        public bool IsBorrowerSpouseCoApplicant(List<ISubSection> subSectionsSpouse)
        {
            var result = IsBorrowerMarried(subSectionsSpouse);

            if (!result) return false;
            
            var jointCreditSubsectionQuestionList = subSectionsSpouse[0].QuestionSections.SelectMany(x=>x.QuestionList);
            var addSpouseAsCoBorrower = jointCreditSubsectionQuestionList.FirstOrDefault(x=>x.QuestionFieldName == FIELD_ADD_SPOUSE_COBORROWER);
                
            return addSpouseAsCoBorrower != null && addSpouseAsCoBorrower.Answer.Equals("true",StringComparison.OrdinalIgnoreCase);
        }

        public bool IsSpouseTemplateAvailable(ISection profileSection)
        {
            bool result;

            var spouseInfoSubSection = profileSection.SubSections
                .FirstOrDefault(x => x.TemplateFieldName == TEMPLATE_SPOUSE_INFO);

            var communicationSubSection = profileSection.SubSections
                .FirstOrDefault(x => x.TemplateFieldName == TEMPLATE_SPOUSE_COMMUNICATION_INFO);

            if(spouseInfoSubSection == null || communicationSubSection == null)
            {
                result = false;
            }
            else
            {
                result = true;
            }

            return result;
        }

        public bool IsBorrowerSpouseAlreadyInvited(ISection profileSection)
        {
            bool result;
            
            var spouseInfoSubSection = profileSection.SubSections
                .FirstOrDefault(x => x.TemplateFieldName == TEMPLATE_SPOUSE_INFO);

            var communicationSubSection = profileSection.SubSections
                .FirstOrDefault(x => x.TemplateFieldName == TEMPLATE_SPOUSE_COMMUNICATION_INFO);

            if(spouseInfoSubSection == null || communicationSubSection == null)
            {
                result = false;
            }
            else
            {
                var emailQuestion = communicationSubSection.QuestionSections.SelectMany(x=>x.QuestionList)
                                       .FirstOrDefault(x=>x.QuestionFieldName == "BorrowerPreferredEmail");

                result = !string.IsNullOrEmpty(emailQuestion?.Answer);
            }

            return result;
        }

        public bool IsBorrowerSpouseAlreadySignedup(List<IUserAccessedStatus> userStats, string borrowerUserName)
        {
            var userState = userStats
                                           .FirstOrDefault(x=>x.UserName == borrowerUserName);
            
            if(userState == null || string.IsNullOrEmpty(userState.SpouseUserName) )
                return false;
            
            var spouseUserState = userStats
                                           .FirstOrDefault(x=>x.UserName == userState.SpouseUserName);

            if(spouseUserState == null)
                return false;

            if(spouseUserState.UserName == spouseUserState.InviteId)
                return false;

            return true;
        }

        public IResendInvitationRequest GetBorrowerSpouseInviteDetails(List<IUserAccessedStatus> userStats, AddCoBorrowerModal borrowerSpouseInfo,string borrowerUserName)
        {
            var result = new ResendInvitationRequest();
            var userState = userStats
                                           .FirstOrDefault(x=>x.UserName == borrowerUserName);
            
            if(userState == null || string.IsNullOrEmpty(userState.SpouseUserName) )
                return result;
            
            var spouseUserState = userStats
                                           .FirstOrDefault(x=>x.UserName == userState.SpouseUserName);

            if(spouseUserState == null)
                return result;

            if(spouseUserState.UserName == spouseUserState.InviteId)
            {
                if(borrowerSpouseInfo.EmailAddress.ToLower().Equals(spouseUserState.UserEmailId.ToLower()))
                {
                    return result;
                }

                result.invitationId = spouseUserState.InviteId;
                result.inviteEmailId = (!string.IsNullOrEmpty(borrowerSpouseInfo.EmailAddress))?
                                            borrowerSpouseInfo.EmailAddress : spouseUserState.UserEmailId;
            }
            
            return  result;
        }
    }
}