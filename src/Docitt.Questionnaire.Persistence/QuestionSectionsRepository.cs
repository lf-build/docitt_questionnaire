﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;
using MongoDB.Driver.Linq;
using System.Linq;

namespace Docitt.Questionnaire.Persistence
{
    public class QuestionSectionsRepository : MongoRepository<IQuestionSection, QuestionSection>,
        IQuestionSectionsRepository
    {
        public QuestionSectionsRepository(LendFoundry.Tenant.Client.ITenantService tenantService,
            IMongoConfiguration configuration)
            : base(tenantService, configuration, "question-sections")
        {
            CreateIndexIfNotExists("tenant_applicationNumber_sectionId_subsectionId",
                Builders<IQuestionSection>.IndexKeys.Ascending(t => t.TenantId)
                    .Descending(i => i.ApplicationNumber).Ascending(i => i.SectionId)
                    .Ascending(i => i.SubSectionId));
            CreateIndexIfNotExists("tenant_applicationNumber_sectionId",
                Builders<IQuestionSection>.IndexKeys.Ascending(t => t.TenantId)
                    .Descending(i => i.ApplicationNumber).Ascending(i => i.SectionId));
            CreateIndexIfNotExists("tenant_applicationNumber",
                Builders<IQuestionSection>.IndexKeys.Ascending(t => t.TenantId)
                    .Descending(i => i.ApplicationNumber));
        }

        public async Task<List<IQuestionSection>> GetByApplicationNumber(
            string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            return await Query.Where(x => x.ApplicationNumber == applicationNumber).ToListAsync();
        }

        public async Task<List<IQuestionSection>> GetBySectionId(string applicationNumber,
            int sectionId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            return await Query.Where(x =>
                x.ApplicationNumber == applicationNumber && x.SectionId == sectionId).ToListAsync();
        }

        public async Task<List<IQuestionSection>> GetBySubSectionId(string applicationNumber, int sectionId,
            string subSectionId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(subSectionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(subSectionId));
            subSectionId = subSectionId?.ToLower();
            return await Query.Where(x =>
                x.ApplicationNumber == applicationNumber && x.SectionId == sectionId &&
                x.SubSectionId == subSectionId).ToListAsync();
        }
        
        public async Task Add(List<IQuestionSection> questionSections)
        {
            if (questionSections == null) throw new ArgumentNullException(nameof(questionSections));
            questionSections.ForEach(item =>
            {
                item.TenantId = TenantService.Current.Id;
                item.SubSectionId = item.SubSectionId?.ToLower();
            });
            await Collection.InsertManyAsync(questionSections);
        }
        
        public new async Task Add(IQuestionSection questionSection)
        {
            if (questionSection == null) throw new ArgumentNullException(nameof(questionSection));
            questionSection.TenantId = TenantService.Current.Id;
            questionSection.SubSectionId = questionSection.SubSectionId?.ToLower();
            await Collection.InsertOneAsync(questionSection);
        }

        public async Task UpdateQuestionSection(IAnswerUpdateRequest answerRequest)
        {
            if (answerRequest == null) throw new ArgumentNullException(nameof(answerRequest));
            var updateDefinition =
                Builders<IQuestionSection>.Update.Set(x => x.QuestionList[-1].Answer, answerRequest.Answer);
            answerRequest.SubSectionId = answerRequest.SubSectionId?.ToLower();

            await Collection.UpdateOneAsync(
                p => p.TenantId == TenantService.Current.Id &&
                     p.ApplicationNumber == answerRequest.TemporaryApplicationNumber &&
                     p.SectionId == answerRequest.SectionId &&
                     p.SubSectionId == answerRequest.SubSectionId &&
                     p.SeqNo == answerRequest.QuestionSectionSeqNo,
                updateDefinition);
        }

        public async Task<bool> UpdateQuestionReadOnly(IAnswerUpdateRequest answerRequest, bool value)
        {
            if (answerRequest == null) throw new ArgumentNullException(nameof(answerRequest));

            var filter = Builders<IQuestionSection>.Filter
                        .Where(a => a.TenantId == TenantService.Current.Id &&
                                    a.ApplicationNumber == answerRequest.TemporaryApplicationNumber &&
                                    a.SectionId == answerRequest.SectionId && 
                                    a.SubSectionId == answerRequest.SubSectionId.ToLower() && 
                                    a.QuestionList.Any(x => x.QuestionFieldName == answerRequest.QuestionFieldName));

            var update = Builders<IQuestionSection>.Update.Set(b => b.QuestionList[-1].IsReadOnly, value);
            var result = await Collection.UpdateManyAsync(filter, update);
            return result.IsAcknowledged;
        }
        
        public async Task UpdateInviteQuestionSection(IAnswerUpdateRequest answerRequest, string visibleStatus="show")
        {
            if (answerRequest == null) throw new ArgumentNullException(nameof(answerRequest));
            var updateDefinition =
                Builders<IQuestionSection>.Update
                                          .Set(x => x.QuestionList[-1].ResultQuestionId, answerRequest.Answer)
                                          .Set(x => x.QuestionList[-1].QuestionVisibleStatus, visibleStatus)
                                          .Set(x => x.QuestionList[-1].IsReadOnly, true);

            answerRequest.SubSectionId = answerRequest.SubSectionId?.ToLower();

            await Collection.UpdateOneAsync(
                p => p.TenantId == TenantService.Current.Id &&
                     p.ApplicationNumber == answerRequest.TemporaryApplicationNumber &&
                     p.SectionId == answerRequest.SectionId &&
                     p.SubSectionId == answerRequest.SubSectionId &&
                     p.SeqNo == answerRequest.QuestionSectionSeqNo &&
                     p.QuestionList.Any(x => x.QuestionId == answerRequest.QuestionId),
                updateDefinition);
        }

        public async Task Remove(string applicationNumber, IQuestionSection questionSection, int sectionId,
            string subSectionId)
        {
            if (questionSection == null) throw new ArgumentNullException(nameof(questionSection));
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(subSectionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(subSectionId));
            subSectionId = subSectionId?.ToLower();

            await Collection.DeleteOneAsync(x =>
                x.TenantId == TenantService.Current.Id &&
                x.ApplicationNumber == applicationNumber &&
                x.SectionId == sectionId &&
                x.SubSectionId == subSectionId &&
                x.SeqNo == questionSection.SeqNo
            );
        }

        public async Task Remove(string applicationNumber, int sectionId, string subSectionId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(subSectionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(subSectionId));
            subSectionId = subSectionId?.ToLower();

            await Collection.DeleteManyAsync(x =>
                x.TenantId == TenantService.Current.Id &&
                x.ApplicationNumber == applicationNumber &&
                x.SectionId == sectionId &&
                x.SubSectionId == subSectionId);
        }

        public async Task Remove(string applicationNumber, int sectionId, List<string> subSectionIds)
        {
            if (subSectionIds == null) throw new ArgumentNullException(nameof(subSectionIds));
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));
            subSectionIds = subSectionIds.ConvertAll(d => d.ToLower());
            await Collection.DeleteManyAsync(x =>
                x.TenantId == TenantService.Current.Id &&
                x.ApplicationNumber == applicationNumber &&
                x.SectionId == sectionId &&
                subSectionIds.Contains(x.SubSectionId));
        }

        public async Task<bool> UpdateQuestionVisibilityStatus(IAnswerUpdateRequest updateRequest, string status)
        {
            if (updateRequest == null) 
                throw new ArgumentNullException(nameof(updateRequest));
            if (string.IsNullOrWhiteSpace(updateRequest.TemporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(updateRequest.TemporaryApplicationNumber));
            if (string.IsNullOrWhiteSpace(updateRequest.QuestionSectionName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(updateRequest.QuestionSectionName));

            // Set value of QuestionVisibleStatus to show, hide, or alwayshide
            var update =  Builders<IQuestionSection>
                                .Update
                                .Set(x => x.QuestionList[0].QuestionVisibleStatus, status.ToLower());

            var filter = Builders<IQuestionSection>.Filter.Where(x =>
                x.TenantId == TenantService.Current.Id &&
                x.ApplicationNumber == updateRequest.TemporaryApplicationNumber &&
                x.SectionId == updateRequest.SectionId &&
                x.QuestionList.Any(q => q.QuestionFieldName == updateRequest.QuestionFieldName));

            var result = await Collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;
        }

        public async Task<bool> UpdateResendInviteVisibility(IAnswerUpdateRequest updateRequest, string status)
        {
            if (updateRequest == null) 
                throw new ArgumentNullException(nameof(updateRequest));
            if (string.IsNullOrWhiteSpace(updateRequest.TemporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(updateRequest.TemporaryApplicationNumber));
            if (string.IsNullOrWhiteSpace(updateRequest.QuestionSectionName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(updateRequest.QuestionSectionName));

            // Set value of QuestionVisibleStatus to show, hide, or alwayshide
            var update =  Builders<IQuestionSection>.Update.Set(x => x.QuestionList[0].QuestionVisibleStatus, status.ToLower());
            var filter = Builders<IQuestionSection>.Filter.Where(x =>
                x.TenantId == TenantService.Current.Id &&
                x.ApplicationNumber == updateRequest.TemporaryApplicationNumber &&
                x.SectionId == updateRequest.SectionId &&
                x.QuestionSectionName == updateRequest.QuestionSectionName);

            if(updateRequest.QuestionSectionName == "AddCoBorrower")
            {
                filter = Builders<IQuestionSection>.Filter.Where(qs =>
                qs.TenantId == TenantService.Current.Id &&
                qs.ApplicationNumber == updateRequest.TemporaryApplicationNumber &&
                qs.SectionId == updateRequest.SectionId &&
                qs.QuestionSectionName == updateRequest.QuestionSectionName && 
                qs.QuestionList.Any(q => q.ResultQuestionId == updateRequest.UserName));
            }

            var result = await Collection.UpdateOneAsync(filter, update);
            return result.IsAcknowledged;
        }
    }
}