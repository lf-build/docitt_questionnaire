﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Date;
using System;
using MongoDB.Driver.Linq;
using MongoDB.Bson;

namespace Docitt.Questionnaire.Persistence
{
    public class QuestionnaireRepository : MongoRepository<IQuestionnaire, Questionnaire>, IQuestionnaireRepository
    {
        static QuestionnaireRepository()
        {
            QuestionnaireRegisterClassMap.Initialize();
        }

        public QuestionnaireRepository(LendFoundry.Tenant.Client.ITenantService tenantService,
            IMongoConfiguration configuration,
            IQuestionnaireAnswerRepository questionnaireAnswerRepository,            
            IQuestionSectionRepository questionnaireSectionDataRepository)
            : base(tenantService, configuration, "questionnaire")
        {
            CreateIndexIfNotExists("questionnaires_temporary_application_number",
                Builders<IQuestionnaire>.IndexKeys.Ascending(t => t.TenantId)
                    .Ascending(i => i.TemporaryApplicationNumber), true);
            CreateIndexIfNotExists("questionnaires_username",
                Builders<IQuestionnaire>.IndexKeys.Ascending(t => t.TenantId)
                    .Ascending(i => i.TemporaryApplicationNumber).Ascending(t => t.UserName), false);
            CreateIndexIfNotExists("questionnaires_status",
                Builders<IQuestionnaire>.IndexKeys.Ascending(t => t.TenantId)
                    .Ascending(i => i.TemporaryApplicationNumber).Ascending(t => t.Status), false);
            CreateIndexIfNotExists("questionnaires_tenant",
                Builders<IQuestionnaire>.IndexKeys.Ascending(t => t.TenantId)
                    .Ascending(i => i.TemporaryApplicationNumber).Ascending(t => t.TenantId), false);
            
            IndexKeysDefinition<IQuestionnaire> keys = Builders<IQuestionnaire>.IndexKeys.Combine(new BsonDocument("TenantId", 1), new BsonDocument("UserState.UserName", 1));               
            CreateIndexIfNotExists("questionnaires_userstate_username", keys, false);

            CreateIndexIfNotExists("questionnaires_application_number",
                Builders<IQuestionnaire>.IndexKeys.Ascending(t => t.TenantId)
                    .Ascending(i => i.ApplicationNumber), false);

            QuestionnaireAnswerRepository = questionnaireAnswerRepository;
            QuestionnaireSectionDataRepository = questionnaireSectionDataRepository;
        }

        private IQuestionnaireAnswerRepository QuestionnaireAnswerRepository { get; }
        private IQuestionSectionRepository QuestionnaireSectionDataRepository { get; }

        
        public async Task<IQuestionnaire> GetQuestionnaireWithoutDetail(string temporaryApplicationNumber)
        {
              //Need to fetch the form using TemporaryApplicationID as well.
            var questionnaire = await Query.FirstOrDefaultAsync(x => x.TemporaryApplicationNumber == temporaryApplicationNumber);

            return questionnaire;
        }

        public async Task<IQuestionnaire> GetQuestionnaire(string temporaryApplicationNumber, string userName)
        {
            if (string.IsNullOrEmpty(temporaryApplicationNumber))
                return null;
            
            //Need to fetch the form using TemporaryApplicationID as well.
            var questionnaire = await GetQuestionnaire(temporaryApplicationNumber);

            if (questionnaire == null)
                return null;

            var userState = questionnaire.UserState.FirstOrDefault(u => u.UserName == userName);

            if (userState == null)
                throw new UnauthorizedAccessException("User is not authorized");

            questionnaire.Status = userState.Status == 0? FormStatus.Open : userState.Status;

            return questionnaire;
        }

        
        public async Task<IQuestionnaire> GetQuestionnaire(string temporaryApplicationNumber)
        {
            //Need to fetch the form using TemporaryApplicationID as well.
            var questionnaire = Query.FirstOrDefault(x => x.TemporaryApplicationNumber == temporaryApplicationNumber);
            if (questionnaire==null)
            {
                return null;
            }
            // This is used to convert existing application which are in porecess and not sbumitted to follow the new structure
            // TO DO Remove this block after deployment on UAT , Staging server.
            if (questionnaire.ApplicationForm.Sections != null && questionnaire.Status == FormStatus.Open)
            {
                //need to seperate section to new collection as per oc-86

                questionnaire.ApplicationForm.Sections.ForEach(sec =>
                {
                    sec.TemporaryApplicationNumber = temporaryApplicationNumber;
                    sec.TenantId = TenantService.Current.Id;
                });

                await QuestionnaireSectionDataRepository.InsertSections(temporaryApplicationNumber,questionnaire.ApplicationForm.Sections);

                //update the questionnaire section to null in questionnaire collection.
                await Collection.UpdateOneAsync
                (
                    Builders<IQuestionnaire>.Filter
                        .Where(a => a.TenantId == TenantService.Current.Id &&
                                    a.TemporaryApplicationNumber == temporaryApplicationNumber),
                    Builders<IQuestionnaire>.Update.Set(a => a.ApplicationForm.Sections, null)
                );
            }

            if (questionnaire.ApplicationForm.Sections == null)
            {
                //need to fetch all the section form section collection and update that over here
                var sections =
                    await QuestionnaireSectionDataRepository.GetAllApplicationSection(temporaryApplicationNumber);
                questionnaire.ApplicationForm.Sections = sections;
            }

            questionnaire = FillAnswersFromQuestionnaireAnswer(questionnaire);

            return questionnaire;
        }

        public async Task<List<IQuestionnaire>> GetQuestionnaires()
        {
            return await Query.Where(a => a.TenantId == TenantService.Current.Id && a.ApplicationNumber == null).ToListAsync();            
        }

        public async Task<IQuestion> GetQuestionByQuestionId(string temporaryApplicationNumber,
            IAnswerUpdateRequest questionUpdateRequest)
        {
            var sections =
                await QuestionnaireSectionDataRepository.GetApplicationSection(temporaryApplicationNumber,
                    questionUpdateRequest.SectionId);

            var question = sections?.SubSections
                .FirstOrDefault(ss => ss.SubSectionId == questionUpdateRequest.SubSectionId)
                ?.QuestionSections
                .FirstOrDefault(qs => qs.SeqNo == questionUpdateRequest.QuestionSectionSeqNo)
                ?.QuestionList
                .FirstOrDefault(q => q.QuestionId == questionUpdateRequest.QuestionId);
            return question;
        }

        public async Task<IQuestionnaire> GetQuestionnaireFromApplicationNumber(string applicationNumber)
        {
            //Need to fetch the form using Application Number.
            var questionnaire = Query.FirstOrDefault(x => x.ApplicationNumber == applicationNumber);
            if (questionnaire == null)
                return null;
            
            if (questionnaire.ApplicationForm.Sections == null)
            {
                //need to fetch all the section form section collection and update that over here
                var sections = await QuestionnaireSectionDataRepository.GetAllApplicationSection(applicationNumber);
                questionnaire.ApplicationForm.Sections = sections;
            }

            questionnaire = FillAnswersFromQuestionnaireAnswer(questionnaire);

            return await Task.Run(() => questionnaire);
        }

        public async Task<string> GetApplicationNumberGivenUserName(string userName)
        {
            //TODO: By Nayan: What if there is multiple applications
            //This function returns the form if exists for the user and having the status as open.
            var temporaryApplicationNumber = await Query
                .Where(x => x.UserState.Any(y => y.UserName == userName))
                .Select(x => x.TemporaryApplicationNumber)
                .FirstOrDefaultAsync();


            //If questionnaire is not null, then it means there are open applications. If null, then there are no Open applications.
            return string.IsNullOrEmpty(temporaryApplicationNumber) ? null : temporaryApplicationNumber;
        }

        // Added New method as GetApplicationNumberGivenUserName(string userName) has to use in so many places.
        public async Task<List<string>> GetMultipleTemporaryApplicationNumberByUserName(string userName)
        {           
            //This function returns the form if exists for the user and having the status as open.
            var resultsData = await Query
                .Where(x => x.UserState.Any(y => y.UserName == userName))
                .Select(x => x.TemporaryApplicationNumber)
                .ToListAsync();

            //If questionnaire is not null, then it means there are open applications. If null, then there are no Open applications.
            return (!resultsData.Any()) ? null : resultsData;
        }
        public async Task<ISection> GetSectionById(string temporaryApplicationNumber, int sectionId)
        {
            //This function returns the form id if exists for the user and have the status as open.
            //var section = Query.FirstOrDefault(x => x.TemporaryApplicationNumber == temporaryApplicationNumber)?.ApplicationForm.Sections.FirstOrDefault(x => x.SectionId == sectionId);
            var section =
                await QuestionnaireSectionDataRepository.GetApplicationSection(temporaryApplicationNumber, sectionId);

            //If questionnaire is not null, then it means there are open applications. If null, then there are no Open applications.
            return await FillAnswersFromQuestionnaireAnswerForSection(temporaryApplicationNumber, section);
        }

        private bool DoesUserHaveAnyOpenApplication(string userName)
        {
            //This function checks whether there is any application for the user with status as 'Open'.
            var questionnaire = Query.FirstOrDefault(x => x.UserName == userName && x.Status == FormStatus.Open);

            //If questionnaire is not null, then it means there are open applications. If null, then there are no Open applications.
            var status = questionnaire != null;

            return status;
        }

        public async Task<bool> AddQuestionnaire(string userName, IForm form, string temporaryApplicationNumber,
            bool allowNew = false)
        {
            //This function adds a questionnaire for the borrower to fill in.

            //Get Questionnaire for the applicant based on the userId/ApplicantId
            //If there are no Open Application (DoesUserHaveAnyOpenApplication = false)
            if (allowNew || DoesUserHaveAnyOpenApplication(userName) == false)
            {

                form.Sections.ForEach(sec =>
                {
                    sec.TemporaryApplicationNumber = temporaryApplicationNumber;
                    sec.TenantId = TenantService.Current.Id;
                });

                await QuestionnaireSectionDataRepository.InsertSections(temporaryApplicationNumber,form.Sections);
                form.Sections = null;

                //If there are no open applications for the given user. Then add a new form for the user to fill in.
                //Get a new form from the form lookup and save it into Questionnaire collection. Assign this form the User Id/Applicant Id to it.
                //now section are stored in different collection,so we have to make form section to null;
                IQuestionnaire questionnaire = new Questionnaire();
                questionnaire.ApplicationForm = new ApplicationForm(form);
                questionnaire.Status = FormStatus.Open;
                questionnaire.TemporaryApplicationNumber = temporaryApplicationNumber;
                questionnaire.TenantId = TenantService.Current.Id;
                questionnaire.UserName = userName;
                questionnaire.CreatedDate = new TimeBucket(DateTime.Now);
                questionnaire.UserState = new List<IUserAccessedStatus>()
                {
                    new UserAccessedStatus
                    {
                        UserName = userName,
                        UserEmailId = userName,
                        LastAccessedSection = 1,
                        LastAccessedSectionSeqNo = 1,
                        LastAccessedSubSectionSeqNo = 1,
                        HighestSectionReached = 1,
                        Status = FormStatus.Open,
                        ApplicantType = ApplicantType.Borrower.ToString()
                    }
                };

                //setting the default values to 1. So that the UI shows the first section.
                questionnaire.HighestSectionReached = 1;

                await Collection.InsertOneAsync(questionnaire);
            }
            else
            {
                throw new QuestionnaireAlreadyExistsException(
                    "There is already a Questionnaire having status as Open.");
            }

            return true;
        }


        public async Task<bool> UpdateApplicationNumber(string temporaryApplicationNumber, string applicationNumber,
            string userName)
        {
            //This function assigns Application Number to the Questionnaire.
            //Get Questionnaire for the applicant.

            //Now at the time of creating the Application, the status is set to "Post Application"
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber &&
                                a.UserState.Any(u => u.UserName == userName)),
                Builders<IQuestionnaire>.Update.Set(a => a.ApplicationNumber, applicationNumber)
                    .Set(a => a.Status, FormStatus.PostApplication)
                    .Set(a => a.UserState[-1].Status, FormStatus.PostApplication)
            );

            return true;
        }

        public async Task<IQuestion> UpdateQuestion(IAnswerUpdateRequest answerRequest, IQuestion questionToUpdate)
        {
            IQuestionnaireQuestionAnswer questionnaireQuestionAnswer = new QuestionnaireQuestionAnswer();
            questionnaireQuestionAnswer.TemporaryApplicationNumber = answerRequest.TemporaryApplicationNumber;
            questionnaireQuestionAnswer.SectionId = answerRequest.SectionId;
            questionnaireQuestionAnswer.SubSectionId = answerRequest.SubSectionId;
            questionnaireQuestionAnswer.QuestionSectionSeqNo = answerRequest.QuestionSectionSeqNo;
            questionnaireQuestionAnswer.QuestionId = answerRequest.QuestionId;
            questionnaireQuestionAnswer.Answer = answerRequest.Answer;

            await QuestionnaireAnswerRepository.InsertOrUpdateAnswer(questionnaireQuestionAnswer);

            if (questionToUpdate != null)
            {
                questionToUpdate.Answer = answerRequest.Answer;
            }

            return questionToUpdate;
        }

        public async Task UpdateQuestionnaireStatus(string applicationNumber, int highestSectionReached, string highestSubSectionReached, IUserAccessedStatus userStatus,
            int? sectionIdCompleted= null, bool? sectionIdIsCompleted=null)
        {
            if (applicationNumber == null) throw new ArgumentNullException(nameof(applicationNumber));
            if (userStatus == null) throw new ArgumentNullException(nameof(userStatus));
            
            var t1= Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == applicationNumber &&
                                a.HighestSectionReached <= highestSectionReached),
                Builders<IQuestionnaire>.Update
                    .Set(a => a.HighestSectionReached, highestSectionReached)
                    .Set(a => a.HighestSubSectionReached, highestSubSectionReached)
            );

            var t2=Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == applicationNumber &&
                                a.UserState.Any(u =>
                                    u.UserName == userStatus.UserName &&
                                    u.HighestSectionReached <= userStatus.HighestSectionReached)),
                Builders<IQuestionnaire>.Update
                    .Set(a => a.UserState[-1].LastAccessedSection, userStatus.LastAccessedSection)
                    .Set(a => a.UserState[-1].LastAccessedSubSectionSeqNo, userStatus.LastAccessedSubSectionSeqNo)
                    .Set(a => a.UserState[-1].LastAccessedSectionSeqNo, userStatus.LastAccessedSectionSeqNo)
                    .Set(a => a.UserState[-1].LastAccessedSubSection, userStatus.LastAccessedSubSection)
                    .Set(a => a.UserState[-1].LastAccessedSubSectionId, userStatus.LastAccessedSubSectionId)
                    .Set(a => a.UserState[-1].HighestSectionReached, userStatus.HighestSectionReached)
                    .Set(a => a.UserState[-1].HighestSubSectionReached, userStatus.HighestSubSectionReached)
            );

            if (sectionIdCompleted != null && sectionIdIsCompleted!=null)
            {
                var t3=QuestionnaireSectionDataRepository.UpdateSectionCompleted(sectionIdCompleted.GetValueOrDefault(),
                    applicationNumber, sectionIdIsCompleted.GetValueOrDefault());
                await Task.WhenAll(t1, t2, t3);
            }
            else
            {
                await Task.WhenAll(t1, t2);
            }

        }

        public async Task UpdateStatus(string temporaryApplicationNumber, string userName, FormStatus status,
            int lastAccessedSection, string lastAccessedSubSectionId)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.Status, status)
            );

            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber &&
                                a.UserState.Any(u => u.UserName == userName)),
                Builders<IQuestionnaire>.Update.Set(a => a.Status, status)
                    .Set(a => a.UserState[-1].LastAccessedSection, lastAccessedSection)
                    .Set(a => a.UserState[-1].LastAccessedSubSectionId, lastAccessedSubSectionId)
                    .Set(a => a.UserState[-1].Status, status)
            );

        }

        public async Task UpdateStatus(string applicationNumber, FormStatus status, string userName)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.ApplicationNumber == applicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.Status, status)
            );

            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.ApplicationNumber == applicationNumber &&
                                a.UserState.Any(u => u.UserName == userName)),
                Builders<IQuestionnaire>.Update.Set(a => a.Status, status)
                    .Set(a => a.UserState[-1].Status, status)
            );

        }

        public async Task UpdateUserStates(string temporaryApplicationNumber,
            List<IUserAccessedStatus> userStates)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.UserState, userStates)
            );
        }

        public async Task UpdateUserStateWithSpouseUsername(string temporaryApplicationNumber, string username, string spouseUsername)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber &&
                                a.UserState.Any(u=>u.UserName == username)),
                Builders<IQuestionnaire>.Update.Set(a => a.UserState[-1].SpouseUserName, spouseUsername)
            );
        }

        public async Task PushUserState(string temporaryApplicationNumber,
            IUserAccessedStatus userState)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Push(a => a.UserState, userState)
            );
        }

        public async Task UpdateLastAccessedStatus(string temporaryApplicationNumber, string userName,
            int sectionId, string subSectionId)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber &&
                                a.UserState.Any(u => u.UserName == userName)),
                Builders<IQuestionnaire>.Update
                    .Set(a => a.UserState[-1].LastAccessedSection, sectionId)
                    .Set(a => a.UserState[-1].LastAccessedSubSectionId, subSectionId)
            );

            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber &&
                                a.HighestSectionReached <= sectionId),
                Builders<IQuestionnaire>.Update
                    .Set(a => a.HighestSectionReached, sectionId)
                    .Set(a => a.HighestSubSectionReached, subSectionId)
            );

        }


        public async Task AssignLoanOfficerToQuestionnaire(string temporaryApplicationNumber,
            string loanOfficerUserName)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.AssignedLoanOfficerUserName, loanOfficerUserName)
            );
        }

        private IQuestionnaire FillAnswersFromQuestionnaireAnswer(IQuestionnaire questionnaire)
        {
            var questionnaireAnswerList = QuestionnaireAnswerRepository
                .GetByApplicationNumber(questionnaire.TemporaryApplicationNumber)
                .Result;

            if (questionnaireAnswerList == null)
                return questionnaire;

            if (questionnaire.ApplicationForm.Sections == null)
                return questionnaire;

            foreach (var questionnaireAnswer in questionnaireAnswerList)
            {
                IQuestion question = null;
                ISubSection subSection = null;
                IQuestionSection questionSection = null;
                var section =
                    questionnaire.ApplicationForm.Sections.FirstOrDefault(s =>
                        s.SectionId == questionnaireAnswer.SectionId);
                if (section != null)
                    subSection =
                        section.SubSections.FirstOrDefault(ss => ss.SubSectionId == questionnaireAnswer.SubSectionId);

                if (questionnaireAnswer.QuestionSectionSeqNo > 0)
                {
                    if (subSection != null)
                        questionSection = subSection.QuestionSections.FirstOrDefault(qs =>
                            qs.SeqNo == questionnaireAnswer.QuestionSectionSeqNo);

                    if (questionSection != null)
                    {
                        question = questionSection.QuestionList.FirstOrDefault(q =>
                            q.QuestionId == questionnaireAnswer.QuestionId);
                    }
                }
                else
                {
                    if (subSection?.QuestionSections != null)
                    {
                        question = subSection.QuestionSections.SelectMany(qs => qs.QuestionList)
                            .FirstOrDefault(q => q.QuestionId == questionnaireAnswer.QuestionId);
                    }
                }

                if (question != null)
                {
                    question.Answer = questionnaireAnswer.Answer;
                }
            }

            return questionnaire;
        }

        private async Task<ISection> FillAnswersFromQuestionnaireAnswerForSection(string temporaryApplicationNumber,
            ISection section)
        {
            if (temporaryApplicationNumber == null) throw new ArgumentNullException(nameof(temporaryApplicationNumber));

            if (section == null)
                return null;
            
            var questionnaireAnswerList =
                await QuestionnaireAnswerRepository.GetBySectionId(
                    temporaryApplicationNumber, section.SectionId);

            if (questionnaireAnswerList == null)
                return section;

            foreach (var questionnaireAnswer in questionnaireAnswerList)
            {
                IQuestion question;
                //TODO: Note when it will be possible to have 0?
                if (questionnaireAnswer.QuestionSectionSeqNo > 0)
                {
                    question = section.SubSections
                        .FirstOrDefault(ss => ss.SubSectionId == questionnaireAnswer.SubSectionId)
                        ?.QuestionSections.FirstOrDefault(qs => qs.SeqNo == questionnaireAnswer.QuestionSectionSeqNo)
                        ?.QuestionList.FirstOrDefault(q => q.QuestionId == questionnaireAnswer.QuestionId);
                }
                else
                {
                    question = section.SubSections
                        .FirstOrDefault(ss => ss.SubSectionId == questionnaireAnswer.SubSectionId)
                        ?.QuestionSections.SelectMany(qs => qs.QuestionList)
                        .FirstOrDefault(q => q.QuestionId == questionnaireAnswer.QuestionId);
                }

                if (question != null)
                {
                    question.Answer = questionnaireAnswer.Answer;
                }
            }

            return section;
        }


        public async Task<bool> RemoveFromUserState(string applicationNumber,List<string> userList)
        {
            var questionnaire = Query.FirstOrDefault(x => x.TemporaryApplicationNumber == applicationNumber);

            if (questionnaire == null) 
                return false;
            
            var usersToBeRemoved = questionnaire.UserState.Where(x => !userList.Contains(x.UserName));

            var filter = Builders<IQuestionnaire>.Filter.Where(x => x.TemporaryApplicationNumber == applicationNumber &&
                                                                    x.TenantId == TenantService.Current.Id);

            var update = Builders<IQuestionnaire>.Update.PullAll(x => x.UserState, usersToBeRemoved);

            await Collection.UpdateOneAsync(filter, update);

            return true;

        }

        public async Task<bool> UpdateSpouseUserNameInUserState(string applicationNumber,string userName)
        {
            var filter =  Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == applicationNumber &&
                                a.UserState.Any(u => u.UserName == userName));

            var update = Builders<IQuestionnaire>.Update.Set(b => b.UserState[-1].SpouseUserName, "");

           var result =  await Collection.UpdateOneAsync(filter,update);

            return result.IsModifiedCountAvailable;
        }

        public async Task<bool> UpdateUserEmailIdInUserState(string applicationNumber, string userName, string emailId)
        {
            var filter =  Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == applicationNumber &&
                                a.UserState.Any(u => u.UserName == userName));

            var update = Builders<IQuestionnaire>.Update.Set(b => b.UserState[-1].UserEmailId, emailId);

           var result =  await Collection.UpdateOneAsync(filter,update);

            return result.IsModifiedCountAvailable;
        }

        public async Task UpdateReferenceApplicationNumber(string referenceApplicationNumber, string temporaryApplicationNumber)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.ReferenceApplicationNumber, referenceApplicationNumber)
            );
        }
    }
}
