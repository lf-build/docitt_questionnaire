﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using MongoDB.Driver.Linq;

namespace Docitt.Questionnaire.Persistence
{
    public class FormRepository : MongoRepository<IForm, Form>, IFormRepository
    {
        static FormRepository()
        {
            QuestionnaireRegisterClassMap.Initialize();
        }

        public FormRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "formlookup")
        {
            CreateIndexIfNotExists("form-active", Builders<IForm>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.FormName).Ascending(i=>i.IsActive));
            CreateIndexIfNotExists("form-version", Builders<IForm>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.FormName).Ascending(i=>i.Version));

        }

        public async Task<IForm> GetActive(string formName)
        {
            return await Query.Where(x => x.FormName == formName && x.IsActive).FirstOrDefaultAsync();
        }

        public async Task<IForm> Get(string formName, string version)
        {
            return await Query.Where(x => x.FormName == formName && x.Version==version).FirstOrDefaultAsync();
        }

        public async Task Activate(string formName, string version)
        {
            var existing=await Query.Where(x => x.FormName == formName && x.IsActive).FirstOrDefaultAsync();
            if (existing != null)
            {
                throw new InvalidOperationException($"{formName} has already active version {existing.Version}.");
            }

            await Collection.UpdateOneAsync
            (
                Builders<IForm>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.FormName == formName &&
                                a.Version == version),
                Builders<IForm>.Update.Set(a => a.IsActive, true).Set(a => a.WasEverActive, true)
            );

        }
        
        public async Task Deactivate(string formName, string version)
        {
            await Collection.UpdateOneAsync
            (
                Builders<IForm>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.FormName==formName &&
                                a.Version==version),
                Builders<IForm>.Update.Set(a => a.IsActive, false)
            );
        }

        public async Task<List<IForm>> GetAll(string formName)
        {
            return await Query.Where(x => x.FormName == formName).ToListAsync();
        }

        public async Task<List<IForm>> GetAll()
        {
            return await Query.ToListAsync();
        }
        
      
    }
}