﻿using LendFoundry.Foundation.Date;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace Docitt.Questionnaire.Persistence
{
    public static class QuestionnaireRegisterClassMap
    {
        static QuestionnaireRegisterClassMap()
        {                
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IApplicationForm, ApplicationForm>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ISectionBasic, SectionBasic>());
           // BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ISubSection, SubSection>());
           // BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IQuestionSection, QuestionSection>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IAvailableAns, AvailableAns>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IQuestion, Question>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IChildConditionalQuestion, ChildConditionalQuestion>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IDynamicRequired, DynamicRequired>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IDynamicTemplate, DynamicTemplate>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ISectionCompletion, SectionCompletion>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILinkTemplate, LinkTemplate>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IFooterInput, FooterInput>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IFooterLink, FooterLink>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IDynamicQuestionSectionTemplate, DynamicQuestionSectionTemplate>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IFooterInputActionConfig, FooterInputActionConfig>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IUserAccessedStatus, UserAccessedStatus>());

            BsonClassMap.RegisterClassMap<Form>(map =>
            {
                map.AutoMap();
                var type = typeof(Form);
                map.SetIgnoreExtraElements(true);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<AvailableAns>(map =>
            {
                map.AutoMap();
                var type = typeof(AvailableAns);

                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Question>(map =>
            {
                map.AutoMap();
                var type = typeof(Question);
                 map.SetIgnoreExtraElements(true);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<Section>(map =>
            {
                map.AutoMap();
                var type = typeof(Section);
                map.SetIgnoreExtraElements(true);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            
            BsonClassMap.RegisterClassMap<SubSection>(map =>
            {
                map.AutoMap();
                var type = typeof(SubSection);
                map.SetIgnoreExtraElements(true);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            if (!BsonClassMap.IsClassMapRegistered(typeof(QuestionSection)))
            {
                BsonClassMap.RegisterClassMap<QuestionSection>(map =>
                {
                    map.AutoMap();
                    var type = typeof(QuestionSection);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
                
            }
            

            BsonClassMap.RegisterClassMap<Questionnaire>(map =>
            {
                map.AutoMap();
                var type = typeof(Questionnaire);
                map.SetIgnoreExtraElements(true);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            
             BsonClassMap.RegisterClassMap<ApplicationForm>(map =>
            {
                map.AutoMap();
                var type = typeof(ApplicationForm);
                map.SetIgnoreExtraElements(true);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<SectionBasic>(map =>
            {
                map.AutoMap();
                var type = typeof(SectionBasic);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<DynamicTemplate>(map =>
            {
                map.AutoMap();
                var type = typeof(DynamicTemplate);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<FooterInput>(map =>
            {
                map.AutoMap();
                var type = typeof(FooterInput);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<FooterLink>(map =>
            {
                map.AutoMap();
                var type = typeof(FooterLink);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<DynamicQuestionSectionTemplate>(map =>
            {
                map.AutoMap();
                var type = typeof(DynamicQuestionSectionTemplate);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<FooterInputActionConfig>(map =>
            {
                map.AutoMap();
                var type = typeof(FooterInputActionConfig);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<UserAccessedStatus>(map =>
            {
                map.AutoMap();
                var type = typeof(UserAccessedStatus);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<ChildConditionalQuestion>(map =>
            {
                map.AutoMap();
                var type = typeof(ChildConditionalQuestion);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<SectionCompletion>(map =>
            {
                map.AutoMap();
                var type = typeof(SectionCompletion);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LinkTemplate>(map =>
            {
                map.AutoMap();
                var type = typeof(LinkTemplate);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public static void Initialize()
        {
            
        }
    }
}