﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using MongoDB.Driver.Linq;
using System.Linq;

namespace Docitt.Questionnaire.Persistence
{
    public class QuestionSectionRepository : MongoRepository<ISection, Section>,
        IQuestionSectionRepository
    {
        private static object LockObject  {get;} = new object();
        
        public QuestionSectionRepository(LendFoundry.Tenant.Client.ITenantService tenantService,
            IMongoConfiguration configuration,
            IQuestionSubSectionRepository questionnaireSubSectionsDataRepository)
            : base(tenantService, configuration, "sections")
        {
            CreateIndexIfNotExists("tenant_tempapplicationno_sectionId",
                Builders<ISection>.IndexKeys.Ascending(t => t.TenantId).Descending(i => i.TemporaryApplicationNumber)
                    .Ascending(i => i.SectionId), true);
            QuestionnaireSubSectionsDataRepository = questionnaireSubSectionsDataRepository;
        }


        private IQuestionSubSectionRepository QuestionnaireSubSectionsDataRepository { get; }


        public async Task<List<ISection>> GetAllApplicationSection(string temporaryApplicationNumber)
        {
            //return await Query.Where(x => x.TemporaryApplicationNumber == TemporaryApplicationNumber).ToListAsync();

            var subSections = new List<ISubSection>();

            var sections = await Query.Where(x => x.TemporaryApplicationNumber == temporaryApplicationNumber)
                .ToListAsync();

            //get all the subsection for given application number.
            var subSectionList =
                await QuestionnaireSubSectionsDataRepository.GetAllByApplicationNumber(temporaryApplicationNumber);

            //for old or existing application to migrate to new structure
            foreach (var section in sections)
            {
                var result = subSectionList.Any(x => x.SectionId == section.SectionId);
                if (section.SubSections != null && !result)
                {
                    section.SubSections.ForEach(sec =>
                    {
                        sec.ApplicationNumber = temporaryApplicationNumber;
                        sec.TenantId = TenantService.Current.Id;
                        sec.SectionId = section.SectionId;
                    });

                    subSections.AddRange(section.SubSections);
                }
                else if (section.SubSections == null)
                {
                    var sectionSubSections = subSectionList.Where(x => x.SectionId == section.SectionId).ToList();
                    section.SubSections = sectionSubSections;
                }
                else if (result)
                {
                    //update the section subsection to null in section collection.
                    await Collection.UpdateManyAsync
                    (
                        Builders<ISection>.Filter
                            .Where(a => a.TenantId == TenantService.Current.Id &&
                                        a.TemporaryApplicationNumber == temporaryApplicationNumber),
                        Builders<ISection>.Update.Set(a => a.SubSections, null)
                    );
                }
            }

            if (subSections.Count <= 0)
                return sections;

            //This is backward compatible patch.. however when multiple request comes it's breaking so added lock.
            lock (LockObject)
            {                
                QuestionnaireSubSectionsDataRepository.AddSubSections(temporaryApplicationNumber,subSections, true).Wait();   
            }            

            //update the section subsection to null in section collection.
            await Collection.UpdateManyAsync
            (
                Builders<ISection>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber),
                Builders<ISection>.Update.Set(a => a.SubSections, null)
            );

            return sections;
        }

        public async Task<List<ISection>> GetAllApplicationSectionWithoutDetail(string temporaryApplicationNumber)
        {
            var sections = await Query.Where(x => x.TemporaryApplicationNumber == temporaryApplicationNumber)
                .ToListAsync();

            return sections;
        }

        public async Task<ISection> GetApplicationSection(string temporaryApplicationNumber, int sectionId)
        {
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(temporaryApplicationNumber));

            var section = await Query
                .Where(x => x.TemporaryApplicationNumber == temporaryApplicationNumber && x.SectionId == sectionId)
                .FirstOrDefaultAsync();
            var subSections = new List<ISubSection>();
            if (section == null) 
                return null;
            
            if (section.SubSections != null)
            {
                section.SubSections.ForEach(sec =>
                {
                    sec.ApplicationNumber = temporaryApplicationNumber;
                    sec.TenantId = TenantService.Current.Id;
                    sec.SectionId = section.SectionId;
                });

                subSections.AddRange(section.SubSections);
            }
            else if (section.SubSections == null)
            {
                var sectionSubSections =
                    await QuestionnaireSubSectionsDataRepository.GetAllBySectionId(temporaryApplicationNumber,
                        section.SectionId);
                section.SubSections = sectionSubSections;
            }

            if (subSections.Count <= 0) 
                return section;
            
            //Note: Backward compatibility code
            await QuestionnaireSubSectionsDataRepository.AddSubSections(temporaryApplicationNumber,subSections, true);

            //update the section subsection to null in section collection.
            await Collection.UpdateOneAsync
            (
                Builders<ISection>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == temporaryApplicationNumber &&
                                a.SectionId == sectionId),
                Builders<ISection>.Update.Set(a => a.SubSections, null)
            );

            return section;
        }


        //TODO: By Nayan: Please check and remove if not in use
        //Pending to migrate as this method where used that end point is never called so need to do proper
        // testing for this end point.
        public async Task UpdateSections(string temporaryApplicationNumber, List<ISection> sections)
        {
            if (sections == null) throw new ArgumentNullException(nameof(sections));
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(temporaryApplicationNumber));
            foreach (var section in sections)
            {
                await Collection.ReplaceOneAsync(
                    sec => sec.TenantId == TenantService.Current.Id &&
                           sec.TemporaryApplicationNumber == temporaryApplicationNumber &&
                           sec.SectionId == section.SectionId,
                    section);
            }
        }

        public async Task UpdateSectionCompleted(int sectionId, string temporaryApplicationNumber,
            bool isCompleted)
        {
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(temporaryApplicationNumber));
            var filter = Builders<ISection>.Filter.Where(x =>
                x.TemporaryApplicationNumber == temporaryApplicationNumber &&
                x.TenantId == TenantService.Current.Id &&
                x.SectionId == sectionId);
            var update = Builders<ISection>.Update.Set(x => x.IsCompleted, isCompleted);

            await Collection.UpdateOneAsync(filter, update);
        }

        public async Task InsertSections(string temporaryApplicationNumber,List<ISection> sections)
        {
            if (sections == null) throw new ArgumentNullException(nameof(sections));
            var subSections = new List<ISubSection>();

            foreach (var section in sections)
            {
                if (section.SubSections == null) 
                    continue;
                
                section.SubSections.ForEach(sec =>
                {
                    sec.ApplicationNumber = section.TemporaryApplicationNumber;
                    sec.TenantId = TenantService.Current.Id;
                    sec.SectionId = section.SectionId;
                });

                subSections.AddRange(section.SubSections);
                section.SubSections = null;
            }

            if (subSections.Count > 0)
            {
                await QuestionnaireSubSectionsDataRepository.AddSubSections(temporaryApplicationNumber,subSections);
            }

            await Collection.InsertManyAsync(sections);
        }
       
    }
}
