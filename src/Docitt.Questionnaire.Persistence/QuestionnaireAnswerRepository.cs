﻿
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq.Expressions;

namespace Docitt.Questionnaire.Persistence
{
    public class QuestionnaireAnswerRepository : 
        MongoRepository<IQuestionnaireQuestionAnswer, QuestionnaireQuestionAnswer>, 
        IQuestionnaireAnswerRepository
    {
        
        public QuestionnaireAnswerRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "questionnaireanswer")
        {
            CreateIndexIfNotExists("questionnaires_answer", Builders<IQuestionnaireQuestionAnswer>.IndexKeys
                .Ascending(t => t.TenantId)
                .Ascending(i => i.TemporaryApplicationNumber)
                .Ascending(t => t.SectionId)
                .Ascending(t => t.SubSectionId)
                .Ascending(t => t.QuestionSectionSeqNo)
                .Ascending(t => t.QuestionId), true);
        }

        public async Task<List<IQuestionnaireQuestionAnswer>> GetByApplicationNumber(string temporaryApplicationNumber)
        {
             return await Query.Where(q => q.TemporaryApplicationNumber == temporaryApplicationNumber).ToListAsync();
        }
        
        public async Task<List<IQuestionnaireQuestionAnswer>> GetBySectionId(string temporaryApplicationNumber, int sectionId)
        {
            return await Query.Where(q => q.TemporaryApplicationNumber == temporaryApplicationNumber 
                && q.SectionId == sectionId).ToListAsync();
        }

        public async Task<List<IQuestionnaireQuestionAnswer>> GetByQuestionId(string temporaryApplicationNumber, int sectionId, string subSectionId, string questionId)
        {
            return await Query.Where(q => q.TemporaryApplicationNumber == temporaryApplicationNumber
                                          && q.SectionId == sectionId && q.SubSectionId == subSectionId &&
                                          q.QuestionId == questionId).ToListAsync();
        }

        public async Task InsertOrUpdateAnswer(IQuestionnaireQuestionAnswer questionnaireQuestionAnswer)
        {
            if (questionnaireQuestionAnswer == null)
                throw new ArgumentNullException(nameof(questionnaireQuestionAnswer));

            Expression<Func<IQuestionnaireQuestionAnswer, bool>> query = q =>
                q.TenantId == TenantService.Current.Id 
                && q.TemporaryApplicationNumber == questionnaireQuestionAnswer.TemporaryApplicationNumber
                && q.SectionId == questionnaireQuestionAnswer.SectionId
                && q.SubSectionId == questionnaireQuestionAnswer.SubSectionId
                && q.QuestionSectionSeqNo == questionnaireQuestionAnswer.QuestionSectionSeqNo
                && q.QuestionId == questionnaireQuestionAnswer.QuestionId; 
            
            questionnaireQuestionAnswer.TenantId = TenantService.Current.Id;

            try
            {
                await Collection.FindOneAndReplaceAsync(query, questionnaireQuestionAnswer, 
                    new FindOneAndReplaceOptions<IQuestionnaireQuestionAnswer, IQuestionnaireQuestionAnswer> 
                    { IsUpsert = true });
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw exception
                await Collection.FindOneAndReplaceAsync(query, questionnaireQuestionAnswer, 
                    new FindOneAndReplaceOptions<IQuestionnaireQuestionAnswer, IQuestionnaireQuestionAnswer> 
                    { IsUpsert = true });
            }
            catch (MongoCommandException)
            {
                //Retry one more time, if fail it will throw exception
                await Collection.FindOneAndReplaceAsync(query, questionnaireQuestionAnswer, 
                    new FindOneAndReplaceOptions<IQuestionnaireQuestionAnswer, IQuestionnaireQuestionAnswer> 
                    { IsUpsert = true });
            }
        }

        public async Task InsertAnswers(List<IQuestionnaireQuestionAnswer> questionAnswers)
        {
            if (questionAnswers == null) 
                throw new ArgumentNullException(nameof(questionAnswers));
            if (questionAnswers.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(questionAnswers));

            try
            {
                foreach (var questionAnswer in questionAnswers)
                {
                    questionAnswer.TenantId = TenantService.Current.Id;
                }

                await Collection.InsertManyAsync(questionAnswers);

            }
            catch (MongoBulkWriteException)
            {
                foreach (var questionAnswer in questionAnswers)
                {
                   await InsertOrUpdateAnswer(questionAnswer);
                }
            }
        }

        public async Task DeleteQuestionByQuestionId(IQuestionnaireQuestionAnswer questionnaireQuestionAnswer)
        {
            Expression<Func<IQuestionnaireQuestionAnswer, bool>> query = q =>
                q.TenantId == TenantService.Current.Id
                && q.TemporaryApplicationNumber == questionnaireQuestionAnswer.TemporaryApplicationNumber
                && q.SectionId == questionnaireQuestionAnswer.SectionId
                && q.SubSectionId == questionnaireQuestionAnswer.SubSectionId
                && q.QuestionSectionSeqNo == questionnaireQuestionAnswer.QuestionSectionSeqNo
                && q.QuestionId == questionnaireQuestionAnswer.QuestionId;

            try
            {
                await Collection.DeleteOneAsync(query);
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw exception
                await Collection.DeleteOneAsync(query);
            }
            catch (MongoCommandException)
            {
                //Retry one more time, if fail it will throw exception
                await Collection.DeleteOneAsync(query);
            }
        }

        public async  Task Remove(string applicationNumber, int sectionId, List<string> subSectionIds){

             if (subSectionIds == null) throw new ArgumentNullException(nameof(subSectionIds));
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

           // subSectionIds = subSectionIds.ConvertAll(d => d.ToLower());
            await Collection.DeleteManyAsync(x =>
                x.TenantId == TenantService.Current.Id &&
                x.TemporaryApplicationNumber == applicationNumber &&
                x.SectionId == sectionId &&
                subSectionIds.Contains(x.SubSectionId));
        }

    }
}