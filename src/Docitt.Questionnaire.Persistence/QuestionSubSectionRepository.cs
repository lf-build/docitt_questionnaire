﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;
using MongoDB.Driver.Linq;
using System.Linq;

namespace Docitt.Questionnaire.Persistence
{
    public class QuestionSubSectionRepository : MongoRepository<ISubSection, SubSection>,
        IQuestionSubSectionRepository
    {
        public QuestionSubSectionRepository(LendFoundry.Tenant.Client.ITenantService tenantService,
            IMongoConfiguration configuration,
            IQuestionSectionsRepository questionSectionRepository)
            : base(tenantService, configuration, "sub-sections")
        {
            CreateIndexIfNotExists("tenant_applicationNumber_sectionId_subsectionId",
                Builders<ISubSection>.IndexKeys.Ascending(t => t.TenantId).Descending(i => i.ApplicationNumber)
                    .Ascending(i => i.SectionId).Ascending(i => i.SubSectionId), true);
            CreateIndexIfNotExists("tenant_applicationNumber_sectionId",
                Builders<ISubSection>.IndexKeys.Ascending(t => t.TenantId).Descending(i => i.ApplicationNumber)
                    .Ascending(i => i.SectionId), false);
            QuestionSectionRepository = questionSectionRepository;
        }

        private IQuestionSectionsRepository QuestionSectionRepository { get; }

        public async Task<List<ISubSection>> GetAllByApplicationNumber(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            //TODO: By Nayan: Check usage of this and need to avoid if possible
            var subSections = await Query.Where(x => x.ApplicationNumber == applicationNumber)
                .ToListAsync();

            var questionSections = await QuestionSectionRepository.GetByApplicationNumber(applicationNumber);
            if (questionSections == null)
                return subSections;

            foreach (var subSection in subSections)
            {
                var questionSection = questionSections.Where(x =>
                    x.ApplicationNumber == subSection.ApplicationNumber
                    && x.SectionId == subSection.SectionId
                    && string.Equals(x.SubSectionId.ToLower(), subSection.SubSectionId.ToLower(),
                        StringComparison.InvariantCultureIgnoreCase)).ToList();

                subSection.QuestionSections = questionSection;
            }

            return subSections;
        }

        public async Task<List<ISubSection>> GetAllBySectionId(string applicationNumber, int sectionId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            //TODO: By Nayan: Check usage of this and need to avoid if possible
            var subSections = (await Query.Where(x =>
                x.ApplicationNumber == applicationNumber && x.SectionId == sectionId).ToListAsync()).OrderBy(x => x.Id).ToList();
            var questionSections =
                await QuestionSectionRepository.GetBySectionId(applicationNumber, sectionId);

            if (questionSections == null)
                return subSections;

            foreach (var subSection in subSections)
            {
                var questionSection = questionSections.Where(x =>
                    x.ApplicationNumber == subSection.ApplicationNumber
                    && x.SectionId == subSection.SectionId
                    && string.Equals(x.SubSectionId, subSection.SubSectionId, StringComparison.CurrentCultureIgnoreCase)).ToList();

                subSection.QuestionSections = questionSection;
            }

            return subSections;
        }

        public async Task<List<ISubSection>> GetAllBySectionIdWithoutDetail(string applicationNumber, int sectionId)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(applicationNumber));

            var subSections = await Query.Where(x =>
                x.ApplicationNumber == applicationNumber && x.SectionId == sectionId).OrderBy(x => x.Id).ToListAsync();

            return subSections;
        }

        public async Task AddSubSections(string temporaryApplicationNumber, int sectionId, List<ISubSection> subsections)
        {
            if (temporaryApplicationNumber == null) throw new ArgumentNullException(nameof(temporaryApplicationNumber));
            if (subsections == null) throw new ArgumentNullException(nameof(subsections));

            subsections.ForEach(sec =>
            {
                sec.ApplicationNumber = temporaryApplicationNumber;
                sec.TenantId = TenantService.Current.Id;
                sec.SectionId = sectionId;
            });

            await AddSubSections(temporaryApplicationNumber, subsections);
        }

        public async Task AddSubSections(string applicationNumber, List<ISubSection> subSections, bool isExistingApplication = false)
        {
            if (subSections == null) throw new ArgumentNullException(nameof(subSections));

            var questionSectionList = new List<IQuestionSection>();
            var tasks = new List<Task>();
            foreach (var subSection in subSections)
            {
                subSection.ApplicationNumber = applicationNumber;
                if (subSection.QuestionSections == null)
                    continue;

                subSection.QuestionSections.ForEach(sec =>
                {
                    sec.ApplicationNumber = applicationNumber;
                    sec.TenantId = TenantService.Current.Id;
                    sec.SectionId = subSection.SectionId;
                    sec.SubSectionId = subSection.SubSectionId;
                });

                questionSectionList.AddRange(subSection.QuestionSections);

                if (!isExistingApplication)
                {
                    subSection.QuestionSections = null;
                }
            }

            tasks.Add(Collection.InsertManyAsync(subSections));

            if (questionSectionList.Count > 0)
            {
                tasks.Add(QuestionSectionRepository.Add(questionSectionList));
            }

            //TODO: By Nayan: Review it later, not sure why we need this code?
            if (isExistingApplication)
            {
                //update the question section subsection to null in sub section collection.
                tasks.Add(Collection.UpdateManyAsync
                (
                    Builders<ISubSection>.Filter
                        .Where(a => a.TenantId == TenantService.Current.Id &&
                                    a.ApplicationNumber == applicationNumber),
                    Builders<ISubSection>.Update.Set(a => a.QuestionSections, null)
                ));
            }

            await Task.WhenAll(tasks);
        }

        public async Task UpdateSubSectionWithSubSectionId(IAnswerUpdateRequest answerRequest, ISubSection subSection)
        {
            if (answerRequest == null) throw new ArgumentNullException(nameof(answerRequest));
            if (subSection == null) throw new ArgumentNullException(nameof(subSection));

            //now question section is moved to other collection so remove existing collection and add new collection.
            var questionSections = subSection.QuestionSections;
            questionSections.ForEach(sec =>
            {
                sec.ApplicationNumber = answerRequest.TemporaryApplicationNumber;
                sec.TenantId = TenantService.Current.Id;
                sec.SectionId = answerRequest.SectionId;
                sec.SubSectionId = subSection.SubSectionId;
            });

            subSection.QuestionSections = null;

            //TODO: By Nayan: Review it later.. why we need to remove and add again

            await QuestionSectionRepository.Remove(answerRequest.TemporaryApplicationNumber, answerRequest.SectionId,
                subSection.SubSectionId);
            await QuestionSectionRepository.Add(questionSections);

            await Collection.ReplaceOneAsync(
                sec => sec.TenantId == TenantService.Current.Id &&
                       sec.ApplicationNumber == answerRequest.TemporaryApplicationNumber &&
                       sec.SectionId == answerRequest.SectionId && sec.SubSectionId == answerRequest.SubSectionId,
                subSection);

            subSection.QuestionSections = questionSections;

        }


        public async Task UpdateUserName(string temporaryApplicationNumber, int sectionId, string subSectionId, string userName)
        {
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(temporaryApplicationNumber));
            if (string.IsNullOrWhiteSpace(subSectionId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(subSectionId));
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userName));
            if (sectionId <= 0) throw new ArgumentOutOfRangeException(nameof(sectionId));

            var update = Builders<ISubSection>.Update.Set(p => p.UserName, userName);

            await Collection.UpdateOneAsync(p => p.TenantId == TenantService.Current.Id &&
                                                 p.ApplicationNumber == temporaryApplicationNumber &&
                                                 p.SectionId == sectionId &&
                                                 p.SubSectionId == subSectionId
                , update);
        }

        public async Task RemoveSubSections(string temporaryApplicationNumber, int sectionId,
            List<string> subSectionIds)
        {
            if (subSectionIds == null) throw new ArgumentNullException(nameof(subSectionIds));
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(temporaryApplicationNumber));
            await QuestionSectionRepository.Remove(temporaryApplicationNumber, sectionId, subSectionIds);

            await Collection.DeleteManyAsync(x =>
                x.ApplicationNumber == temporaryApplicationNumber &&
                x.TenantId == TenantService.Current.Id &&
                x.SectionId == sectionId &&
                subSectionIds.Contains(x.SubSectionId));
        }

        /// <summary>
        /// Get the list of subsection for given applicaiton number and except supplied userNames
        /// </summary>
        /// <param name="applicationNumber">The application number</param>
        /// <param name="userNames">Teh list of username</param>
        /// <returns>List of sub section information</returns>
        public async Task<List<SectionWithSubSectionId>> GetSubSectionList(string applicationNumber, List<string> userNames)
        {
            /* var subSections = await Query.Where(x =>
               x.ApplicationNumber == applicationNumber && !userNames.Contains(x.UserName)).ToListAsync();
               */
            var filter = Builders<ISubSection>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                  a.ApplicationNumber == applicationNumber && !userNames.Contains(a.UserName));

            var results = await Collection.Find(filter).Project(u => new SectionWithSubSectionId
            {
                SectionId = u.SectionId,
                SubSectionId = u.SubSectionId
            }).ToListAsync();
            return results;
        }

        public async Task<string> GetMaritalSubSectionId(string applicationNumber, string userName, string templateName, int sectionId)
        {
            string subSectionId = string.Empty;

            var subSection = await Query.Where(x =>
                x.ApplicationNumber == applicationNumber &&
                x.SectionId == sectionId &&
                x.UserName == userName &&
                x.TemplateFieldName == templateName).FirstOrDefaultAsync(); //"coBorrowerMaritalStatusInfo"

            if (subSection != null)
                return subSection.SubSectionId;

            return subSectionId;
        }

        public async Task<bool> UpdateSubSectionWithTemplateId(string temporaryApplicationNumber, int sectionId, List<string> subSectionIds, string userName, string newTemplateId)
        {

            var builders = Builders<ISubSection>.Filter;
            var filter = builders
                        .Where(a => a.TenantId == TenantService.Current.Id &&
                                    a.ApplicationNumber == temporaryApplicationNumber &&
                                    a.SectionId == sectionId &&
                                   a.UserName == userName
                                      && subSectionIds.Contains(a.SubSectionId)
                                    );
            var update = Builders<ISubSection>.Update.Set(b => b.TemplateId, newTemplateId);

            var result = await Collection.UpdateManyAsync(filter, update);
            return result.IsAcknowledged;
        }

        public async Task<string> GetSubSectionIdByTemplateName(string applicationNumber, string templateName, int sectionId)
        {
            string subSectionId = string.Empty;

            var subSection = await Query.Where(x =>
                x.ApplicationNumber == applicationNumber &&
                x.SectionId == sectionId &&               
                x.TemplateFieldName == templateName).FirstOrDefaultAsync(); //"coBorrowerMaritalStatusInfo"

            if (subSection != null)
                return subSection.SubSectionId;

            return subSectionId;
        }


    }
}
