using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace Docitt.Questionnaire.Api.Controllers
{
    /// <summary>
    /// Represents Questionnaire class.
    /// </summary>
    [Route("/")]
    public class FormController: ExtendedController
    {
        private IFormService FormService { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="formService"></param>
        public FormController(IFormService formService)
        {
            FormService = formService;
        }

        /// <summary>
        /// Add the new form
        /// </summary>
        /// <returns></returns>
        [HttpPost("/forms")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Add([FromBody] Form form)
        {
            return await ExecuteAsync(async () =>
            {
                await FormService.Add(form);
                return NoContent();
            });
        }

        /// <summary>
        /// Get All forms
        /// </summary>
        /// <returns></returns>
        [HttpGet("/forms/all")]
        [ProducesResponseType(typeof(List<IForm>), 200)]
        public async Task<IActionResult> GetAll()
        {
            return await ExecuteAsync(async () => Ok(await FormService.GetAll()));
        }

        /// <summary>
        /// Get Active version for given formName
        /// </summary>
        /// <param name="formName"></param>
        /// <returns></returns>
        [HttpGet("/forms/{formName}/active")]
        [ProducesResponseType(typeof(IForm), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetActive(string formName)
        {
            return await ExecuteAsync(async () => Ok(await FormService.GetActive(formName)));
        }

        /// <summary>
        /// Get form by version number
        /// </summary>
        /// <param name="formName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        [HttpGet("/forms/{formName}/{version}")]
        [ProducesResponseType(typeof(IForm), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetActive(string formName, string version)
        {
            return await ExecuteAsync(async () => Ok(await FormService.Get(formName,version)));
        }

        /// <summary>
        /// Update the form
        /// </summary>
        /// <param name="formName"></param>
        /// <param name="version"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("/forms/{formName}/{version}")]
        [ProducesResponseType(typeof(IForm), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Update(string formName, string version,[FromBody] Form form)
        {
            return await ExecuteAsync(async () => Ok(await FormService.Update(formName,version,form)));
        }

        /// <summary>
        /// Activate version for given formName
        /// </summary>
        /// <param name="formName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        [HttpPost("/forms/{formName}/{version}/activate")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Activate(string formName, string version)
        {
            return await ExecuteAsync(async () =>
            {
                await FormService.Activate(formName, version);
                return NoContent();
            });
        }

        

        /// <summary>
        /// Deactivate version for given formName
        /// </summary>
        /// <param name="formName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        [HttpPost("/forms/{formName}/{version}/deactivate")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Deactivate(string formName,string version)
        {
            return await ExecuteAsync(async () =>
            {
                await FormService.Deactivate(formName, version);
                return NoContent();
            });
        }

        
        /// <summary>
        /// Get All version of forms for given formName
        /// </summary>
        /// <param name="formName"></param>
        /// <returns></returns>
        [HttpGet("/forms/{formName}")]
        [ProducesResponseType(typeof(List<IForm>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAll(string formName)
        {
            return await ExecuteAsync(async () => Ok(await FormService.GetAll(formName)));
        }
        
     
    } 
}