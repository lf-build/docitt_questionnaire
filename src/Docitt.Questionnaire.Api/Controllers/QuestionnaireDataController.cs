
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;

namespace Docitt.Questionnaire.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class QuestionnaireDataController : ExtendedController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalData"></param>
        public QuestionnaireDataController(IQuestionnaireDataService originalData)
        {
            QuestionnaireOriginalData = originalData;

        }

        private IQuestionnaireDataService QuestionnaireOriginalData { get; }
        /// <summary>
        /// Get the Questionnaire Original data for questionnaire
        /// </summary>
        /// <param name="applicationnumber">The application id</param>
        /// <param name="sectionname">The application id</param>  
        /// <param name="borrowerId">The application id</param>          
        /// <returns>Return the profile section data</returns>
        [HttpGet("questionnaire/{applicationnumber}/section/{sectionname}/borrower/{borrowerId?}/")]
        [ProducesResponseType(typeof(Dictionary<string, object>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetQuestionnaireSectionOriginalmData(string applicationnumber,string sectionname,string borrowerId="")
        {
            try
            {
                var result = await QuestionnaireOriginalData.GetQuestionnaireSectionData(applicationnumber,borrowerId,sectionname);
            
                return Ok(result);
            }
            
            catch (System.Exception)
            {
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
        }

         /// <summary>
         /// Get the Questionnaire Original data for questionnaire
         /// </summary>
         /// <param name="applicationnumber">The application id</param>
         /// <param name="borrowerId">The borrower id</param>
         /// <returns></returns>
        [HttpGet("questionnaire/{applicationnumber}/section/all/borrower/{borrowerId?}")]
        [ProducesResponseType(typeof(Dictionary<string, object>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetQuestionnaireSectionOriginalmData(string applicationnumber,string borrowerId="")
        {
            try
            {
                var result = await QuestionnaireOriginalData.GetAllSectionOriginalData(applicationnumber,borrowerId);
            
                return Ok(result);
            }
            
            catch (System.Exception)
            {
                return ErrorResult.InternalServerError("Server error. Try later.");
            }
        }





    }
}