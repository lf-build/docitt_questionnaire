﻿#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using System.Web;
using LendFoundry.Foundation.Client;
using Docitt.Questionnaire.Abstractions;
#else
using RestSharp.Contrib;
#endif

namespace Docitt.Questionnaire.Api.Controllers
{
    /// <summary>
    /// Represents Questionnaire class.
    /// </summary>
    [Route("/")]
    public class QuestionnaireController : ExtendedController
    {
#region "Constructors"

/// <summary>
/// Represents constructor class.
/// </summary>
/// <param name="service"></param>
/// <param name="logger"></param>
public QuestionnaireController(IQuestionnaireService service, ILogger logger):base(logger)
        {
            QuestionnaireService = service;

        }
        
#endregion

#region "Private Properties"

        private IQuestionnaireService QuestionnaireService { get; }

#endregion

#region "Public Methods"


        #region "Application"

        /// <summary>
        /// This endpoint provides application information.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpGet("/application/{temporaryApplicationNumber}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplication), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> Application(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetApplication(temporaryApplicationNumber)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to fetch the Summary data based on temporary application number.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/summary")]
#if DOTNET2
        [ProducesResponseType(typeof(List<Dictionary<string,Summary>>), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetSummary(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetSummary(temporaryApplicationNumber)));
            }
            catch (SectionNotFoundException exception)
            {
                //This should be logged as error as when it's not user entered
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return  UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to fetch the Summary data based on temporary application number , this will be displayed 
        /// on Lender Portal --> Application Tab.
        /// </summary>
        /// <param name="temporaryApplicationNumber">Questionnaire application number</param>
        /// <param name="allowBlankField">This is bool value , used to display all the section if questionnaire answer given or not.
        /// if true then blank question is display if false then blank question is not display in summary screen.</param>
        /// <returns>User wise Questionnaire summary data.</returns>
        [HttpGet("questionnaire/{temporaryApplicationNumber}/summary/{allowBlankField}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<Dictionary<string,Summary>>), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetQuestionnaireSummary(string temporaryApplicationNumber,bool allowBlankField)
        {
            try
            {
                //we have to display all the section if answer given or not that time need to pass true other wise pass false;
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetQuestionnaireSummary(temporaryApplicationNumber, allowBlankField)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point fetches the summary based on Application number.
        /// This endpoint will be called from Lender dashboard only.
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet("application/{applicationNumber}/summary")]
#if DOTNET2
        [ProducesResponseType(typeof(List<Dictionary<string, Summary>>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> GetApplicationSummary(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetApplicationSummary(applicationNumber)));
            }
            catch (ApplicationDoesNotExistsException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point fetches the loan information based on user name.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/user/{userName}/loan-detail")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransitionFormExtended), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> GetApplicationLoanDetail(string temporaryApplicationNumber, string userName)
        {
            // This endpoint will be called from Lender dashboard only.            
            try
            {
                // userName = HttpUtility.UrlDecode(userName).Replace(" ","+");
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetApplicationLoanDetail(temporaryApplicationNumber, userName)));
            }
            catch (ApplicationDoesNotExistsException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This endpoint creates the Post application, after the summary screen.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/create-post-application")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> GenerateApplication(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.CreatePostApplication(temporaryApplicationNumber)));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// To update the status as "PostApplication" on pressing Begin button on welcome to post application screen
        /// </summary>
        /// status can be "PostApplication" if "WelcomePostApplication" or "Application" if "WelcomePersonalDashboard"
        /// <returns></returns>
        [HttpPost("/updatestatus/{applicationNumber}/{applicationStatus}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicationCreationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> UpdateApplicationStatus(string applicationNumber, string applicationStatus)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.UpdateApplicationStatus(applicationNumber, applicationStatus)));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This end point fetches the application status.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/applicationstatus")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicationOverAllStatus), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        //[HttpGet("/application/status")]
        public async Task<IActionResult> GetApplicationStatus()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetApplicationStatus()));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (ApplicationNotAvailableForThisQuestionnaireException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point creates the Application after the Summary screen based on the logged in user's data.
        /// </summary>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/create-application")]
        [ProducesResponseType(typeof(IApplicationCreationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GenerateApplicationByApplicationNumber(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.Confirm(temporaryApplicationNumber)));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                if (ex is AggregateException exception && exception.InnerExceptions[0] is ClientException clientException ){
                    return ErrorResult.BadRequest(clientException.Error.Message);
                }

                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This end point is called after the Post application required conditions are visited. This endpoint shows two button "Take a Tour", "Lets Go".
        /// On the click of "Lets Go", user is taken to Borrower dash board.
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpPut("/{applicationNumber}/complete-post-application")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> CompletePostApplication(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.CompletePostApplication(applicationNumber)));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationNumber"></param>
        /// <returns></returns>
        [HttpGet("/{applicationNumber}/basic-info")]
#if DOTNET2
        [ProducesResponseType(typeof(ISectionListResponse), 200)]
        [ProducesResponseType(typeof(string ), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetInformation(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetBasicInformation(applicationNumber)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message);
            }
            catch (InvalidUserException)
            {
                return UnauthorizedResult();
            }
        }

        #endregion

        #region "Sections"

        /// <summary>
        /// This end point fetches the section list based on Temporary Application Number and Form Type (Full/Short)
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="formType"></param>
        /// <returns></returns>
        //[HttpGet("/qualifyingApplication/{temporaryApplicationNumber}/section-list")]
        [HttpGet("/{temporaryApplicationNumber}/section-list/{formType}")]
#if DOTNET2
        [ProducesResponseType(typeof(ISectionListResponse), 200)]
        [ProducesResponseType(typeof(string ), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetSectionList(string temporaryApplicationNumber, string formType)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetSectionList(formType, temporaryApplicationNumber)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return  UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point fetches the Section list based on form type and logged in user name.
        /// </summary>
        /// <param name="formType"></param>
        /// <returns></returns>
        [HttpPost("/section-list/{formType}")]
#if DOTNET2
        [ProducesResponseType(typeof(ISectionListResponse), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetSectionList(string formType)
        {
            //This endpoint does two things
            //It creates a Questionnaire, if there does not exist any questionnaire.
            //After the Questionnaire is created it lists doen the Sections in that Questionnaire.
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetSectionList(formType)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        ///.New
        /// <summary>
        /// This end point fetches the Section list based on form type and Application Number.
        /// </summary>
        /// <param name="formType"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/section-list/{formType}")]
#if DOTNET2
        [ProducesResponseType(typeof(ISectionListResponse), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetSectionListByApplicationNumber(string formType, string temporaryApplicationNumber)
        {
            //This endpoint does two things
            //It creates a Questionnaire, if there does not exist any questionnaire.
            //After the Questionnaire is created it lists doen the Sections in that Questionnaire.
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetSectionList(formType, temporaryApplicationNumber)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to fetch the json of one section at a time. This is used when ever the user moves from one section to other section.
        /// complete Questionnaire JSON is not fetched on the front end. At present, one section JSON is fetched.
        /// Based on the logged in user's access permission, the questions are either editable, hidden or read only.
        /// </summary>

        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpGet("/section/{sectionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(ISection), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetSection(int sectionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetSection(sectionId)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return  UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to fetch the json of one section at a time. This is used when ever the user moves from one section to other section.
        /// complete Questionnaire JSON is not fetched on the front end. At present, one section JSON is fetched.
        /// Based on the logged in user's access permission, the questions are either editable, hidden or read only.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/section/{sectionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(ISection), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetSection(string temporaryApplicationNumber, int sectionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetSection(temporaryApplicationNumber, sectionId)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to fetch the json of one section at a time. 
        /// It will return raw section data without any processing.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/section/{sectionId}/raw")]
#if DOTNET2
        [ProducesResponseType(typeof(ISection), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetRawSection(string temporaryApplicationNumber, int sectionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetRawSection(temporaryApplicationNumber, sectionId)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult();
            }
        }

        /// <summary>
        /// This end point adds question section using a template on the fly to a sub section. This is used for income screen. 
        /// </summary>
        /// <param name="questionSectionRequest"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/questionsection")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> AddQuestionSection([FromBody] QuestionSection questionSectionRequest, string temporaryApplicationNumber, int sectionId, string subSectionId)
        {

            try
            {
                //This endpoint will be adding a Question Section using a template.
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.AppendQuestionSection(questionSectionRequest, temporaryApplicationNumber, sectionId, subSectionId)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to set "isCompleted" status for a section. This is used by the front end to show the tick mark in the navigation.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="isCompleted"></param>
        /// <returns></returns>
        [HttpPut("/{temporaryApplicationNumber}/section/{sectionId}/{isCompleted}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> UpdateSectionCompletionStatus([FromRoute] string temporaryApplicationNumber, [FromRoute] int sectionId, [FromRoute] bool isCompleted)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.UpdateSectionCompletionStatus(temporaryApplicationNumber, sectionId, isCompleted)));
            }
            catch (SectionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point deletes the question section on the fly.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="questionsectionseqno"></param>
        /// <returns></returns>
        [HttpDelete("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/questionsection/{questionsectionseqno}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteQuestionSection(string temporaryApplicationNumber, int sectionId, string subSectionId, int questionsectionseqno)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.DeleteQuestionSection(temporaryApplicationNumber, sectionId, subSectionId, questionsectionseqno)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        

        #endregion

        #region "SubSections"

        /// <summary>
        /// This end point is used for creating sub sections on the fly using a template.
        /// </summary>
        /// <param name="subSectionRequest"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/section/{sectionId}/subsections")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> AddSubSections([FromBody] AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, int sectionId)
        {
            try
            {
                //This endpoint will be adding a SubSection using a template.
                //The payload will have the TemplateName, the Seq no.
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.AppendSubSections(subSectionRequest, temporaryApplicationNumber, sectionId)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (SubSectionDoesNotExistsException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to delete the sub sections.
        /// </summary>
        /// <param name="subSectionIds"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpDelete("/{temporaryApplicationNumber}/section/{sectionId}/subsectionids")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteSubSections([FromBody] SubSectionIdList subSectionIds, string temporaryApplicationNumber, int sectionId)
        {
            try
            {
                //return await ExecuteAsync(async () => Ok(await questionnaireService.DeleteSubSections(temporaryApplicationNumber, sectionId, subSectionIds)));
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.RemoveSubSections(temporaryApplicationNumber, sectionId, subSectionIds)));

            }
            catch (QuestionnaireNotFoundException ex)
            {
                Logger.Error(ex.Message, ex);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (SectionDoesNotExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This endpoint deletes sub section.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        [HttpDelete("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subSectionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteSubSection(string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.DeleteSubSection(temporaryApplicationNumber, sectionId, subSectionId)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (SubSectionDoesNotExistsException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to delete the sub section.
        /// Remove if not in use.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        [HttpDelete("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subSectionId}/questionsection")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteSubSection1(string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.DeleteSubSection(temporaryApplicationNumber, sectionId, subSectionId)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (SubSectionDoesNotExistsException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

#endregion

#region "Events"

        /// <summary>
        /// This end point is used to call events on the "Next" and "Back" button click.
        /// "Send Invitation" is one such event that is called using this end point.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/event/{eventName?}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ExecuteEvent(string temporaryApplicationNumber, int sectionId, string subSectionId, string eventName)
        {
            //This function will do two things it will update last section Id.
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.ExecuteEvent(temporaryApplicationNumber, sectionId, subSectionId, eventName)));
            }
            catch (QuestionNotFoundException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (UserEmailAlreadyExistsException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
        }

#endregion

#region "Questionnaires"

        /// <summary>
        /// This end point creates a new Quetionnaire.
        /// </summary>
        /// <param name="formType"></param>
        /// <param name="allowNew"></param>
        /// <returns></returns>
        [HttpPost("/{formType}/{allowNew?}")]
        [ProducesResponseType(typeof(IQuestionnaire), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(QuestionnaireAlreadyExistsException), 400)]
        public async Task<IActionResult> GetStarted(string formType, bool allowNew=false)
        {
            try
            {
                //This endpoint is combination of two functions.
                //First 'addQuestionnaire' , Add Questinnaire if user does not have the Questionnaire.
                //Second 'GetQuestionnaire', Get Questionnaire if the Questionnaire exists for the user.
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetStarted(formType, allowNew)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (QuestionnaireAlreadyExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                throw new QuestionnaireAlreadyExistsException(ex.Message);
            }
        }

        /// <summary>
        /// This end point fetches the purpose of loan question. If the Answer is blank, then the Purchase, Refinance screen is shown, else the screen is not shown.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/loan-purpose-question")]
#if DOTNET2
        [ProducesResponseType(typeof(IQuestion), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetQuestion(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetLoanPurposeQuestion(temporaryApplicationNumber)));
            }
            catch (QuestionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to fetch the question JSON.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/question/{questionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetQuestion(string temporaryApplicationNumber, int sectionId, string subSectionId, string questionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetQuestion(temporaryApplicationNumber, sectionId, subSectionId, questionId, null)));
            }
            catch (QuestionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Get the UserState based on the application number.
        /// </summary>
        /// <param name="applicationNumber">application number</param>
        /// <returns>returns userstate</returns>
        [HttpGet("/{applicationNumber}/userstate")]
        [ProducesResponseType(typeof(IQuestionnaire), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(500)]
        [ProducesResponseType(401)]
        public async Task<IActionResult> GetUserStateInformation(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetUserStateInformation(applicationNumber)));
            }
            catch (QuestionnaireNotFoundException ex)
            {                
                Logger.Error("Questionnaire not found {applicationNumber}",ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                Logger.Error("Error happened {applicationNumber}", ex);
                return ErrorResult.InternalServerError("Some error has happened");
            }
        }

        /// <summary>
        /// This end point fetches the Questionnaire based on the temporary application number.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        //[HttpGet("qualifyingapplication/{temporaryApplicationNumber}")]
        [HttpGet("/{temporaryApplicationNumber}")]
#if DOTNET2
        [ProducesResponseType(typeof(IQuestionnaire), 200)]
        [ProducesResponseType(typeof(string), 200)]
        
#endif
        public async Task<IActionResult> GetQuestionnaire(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetQuestionnaire(temporaryApplicationNumber)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used get the list of Questionnaires.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/qualifyingapplications")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IQuestionnaire>), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(401)]
#endif
        public async Task<IActionResult> GetQuestionnaires()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetQuestionnaires()));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return  UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }
        

        /// <summary>
        /// This end point is used to fetch the status of the Questionnaire
        /// </summary>
        /// <param name="temporaryapplicationNumber"></param>
        /// <returns></returns>
        //[HttpGet("qualifyingApplication/status/{temporaryapplicationNumber}")]
        [HttpGet("/status/{temporaryapplicationNumber}")]
        [ProducesResponseType(typeof(IQuestionnaire), 200)]
        [ProducesResponseType(typeof(string), 200)]
        public async Task<IActionResult> GetQuestionnaireStatus(string temporaryapplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetQuestionnaireStatus(temporaryapplicationNumber)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message);  
            }
        }

        /// <summary>
        /// This end point fetches the Questionnaire status.
        /// </summary>        
        /// <returns></returns>
        //[HttpGet("qualifyingApplication/status")]
        [HttpGet("/status")]
        [ProducesResponseType(typeof(IQuestionnaire), 200)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult),400)]
        public async Task<IActionResult> GetQuestionnaireStatus()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetQuestionnaireStatus()));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Update the username for applicant's subsections and userstate which was saved as invite id 
        /// at the time of invite.
        /// </summary>        
        /// <returns></returns>
        [HttpPut("/updateusername/inviteid/{inviteId}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult),400)]
#endif
        public async Task<IActionResult> UpdateUsernameField(string inviteId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.UpdateUsernameField(inviteId)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                                                                  //return ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point fetches the bank info for the assets screen.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/all-bank-info")]
#if DOTNET2
        [ProducesResponseType(typeof(Dictionary<string,List<IBankInfo>>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        
#endif
        public async Task<IActionResult> GetBankInfo(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetBankInfo(temporaryApplicationNumber)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                // return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point is used to get the bank info. This is not is use now.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/{customerId?}/bank-alert-info")]
#if DOTNET2
        [ProducesResponseType(typeof(IBankAlert), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]    
#endif
        public async Task<IActionResult> GetBankAlertInfo(string temporaryApplicationNumber, string customerId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetBankAlertInfo(temporaryApplicationNumber, customerId)));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This end point creates a Questionnaire for the logged in user, if it does not exist.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="allowNew"></param>
        /// <returns></returns>
        [HttpPost("/questionnaire/{type}/{allowNew?}")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddQuestionnaire(string type,bool allowNew=false)
        {
            try
            {
                //This function generates a Questionniare for a borrower.
                //The borrower will be using these forms to fill the application.
                var formType = QuestionnaireService.GetFormType(type);
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.AddQuestionnaire(formType, allowNew)));
            }
            catch (QuestionnaireAlreadyExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (FormDoesNotExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This end point creates the Questionnaire if it does not exist for the logged in user,  and provides the Questionnaire status.
        /// </summary>
        /// <param name="formType"></param>
        /// <returns></returns>
        [HttpPost("/status/{formType}")]
#if DOTNET2
        [ProducesResponseType(typeof(IQuestionnaire), 200)]
        [ProducesResponseType(typeof(string), 200)]
       
#endif
        public async Task<IActionResult> CreateQuestionnaireAndGetQuestionnaireStatus(string formType)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.CreateQuestionnaireAndGetQuestionnaireStatus(formType)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                                                                  //return ErrorResult.BadRequest(exception.Message);
            }
        }

//        /// <summary>
//        /// This is not used at present. It will be used by the admin user.
//        /// </summary>
//        /// <param name="question"></param>
//        /// <param name="formId"></param>
//        /// <param name="sectionId"></param>
//        /// <returns></returns>
//        [HttpPut("addquestion/{FormId}/{sectionId}")]
//#if DOTNET2
//        [ProducesResponseType(typeof(bool), 200)]
//        [ProducesResponseType(typeof(ErrorResult), 400)]
//
//#endif
//
//        public async Task<IActionResult> AddQuestion([FromBody] Question question, string formId, string sectionId)
//        {
//            return await ExecuteAsync(async () => Ok(await QuestionnaireService.AddQuestion(question, formId, sectionId)));
//        }

        /// <summary>
        /// This end point adds bank info for the logged in user. This is not being used at present.
        /// </summary>
        /// <param name="bankInfo"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/bank-info")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> AddBankInfo([FromBody] BankInfo bankInfo, string temporaryApplicationNumber)
        {

            try
            {
                //This endpoint will be adding a Question Section using a template.

                return await ExecuteAsync(async () => Ok(await QuestionnaireService.AddBankInfo(bankInfo, temporaryApplicationNumber)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                // return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This endpoint updates the Purpose of loan Question. Called on selection of purchase or refinance. 
        /// </summary>
        /// <param name="questionRequest"></param>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpPut("/update/{temporaryApplicationNumber}/purpose-of-loan")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 200)]
[ProducesResponseType(401)]
#endif
        public async Task<IActionResult> UpdatePurposeOfLoanQuestion([FromBody] AnswerUpdateRequest questionRequest, string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.UpdatePurposeOfLoanQuestion(temporaryApplicationNumber, questionRequest)));
            }
            catch (QuestionNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return Ok(exception.Message); //return Ok(exception.Message); // ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidUserException exception)
            {
                Logger.Error(exception.Message, exception);
                return  UnauthorizedResult(); //ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// This end point updates one question at a time.
        /// </summary>
        /// <param name="questionRequest"></param>
        /// <param name="temporaryapplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <param name="questionSectionSeqNo"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        //[HttpPut("qualifyingapplication/{temporaryapplicationNumber}/update/")]
        [HttpPut("/update/{temporaryapplicationNumber}/section/{sectionId}/subsection/{subsectionId}/questionsection/{questionSectionSeqNo}/question/{questionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> UpdateQuestion([FromBody] AnswerUpdateRequest questionRequest, string temporaryapplicationNumber, int sectionId, string subSectionId, int questionSectionSeqNo, string questionId)
        {
            try
            {
                questionRequest.SectionId = sectionId;
                questionRequest.SubSectionId = subSectionId;
                questionRequest.QuestionId = questionId;
                questionRequest.QuestionSectionSeqNo = questionSectionSeqNo;

                questionRequest.TemporaryApplicationNumber = temporaryapplicationNumber;
                //return await ExecuteAsync(async () => Ok(await questionnaireService.UpdateQuestion(questionRequest))); //, temporaryapplicationNumber)));
                //return await UpdateQuestion(questionRequest);
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.UpdateQuestionAndStatus(questionRequest))); //, temporaryapplicationNumber)));
            }
            catch (QuestionNotFoundException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (QuestionnaireNotFoundException ex)
            {
                Logger.Error(ex.Message, ex);
                //return Ok(ex.Message); //return ErrorResult.BadRequest(ex.Message);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (SectionDoesNotExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (SubSectionDoesNotExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (QuestionSectionDoesNotExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (AnswerNotProvidedException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidUserException ex)
            {
                Logger.Error(ex.Message, ex);
                return UnauthorizedResult();
            }
            catch (InvalidAnswerException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (UserEmailAlreadyExistsException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// This end point is called to delete bank info.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="bankInfoId"></param>
        /// <returns></returns>
        [HttpDelete("/{temporaryApplicationNumber}/bank-info/{bankInfoId}")]
#if DOTNET2
        [ProducesResponseType(typeof(Dictionary<string,List<IBankInfo>>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> DeleteBankInfo(string temporaryApplicationNumber, string bankInfoId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.DeleteBankInfo(temporaryApplicationNumber, bankInfoId)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                // return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (SubSectionDoesNotExistsException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

#endregion

#region "Users"

        /// <summary>
        /// This end point is used to fetch the list of all the applicants in the Questionnaire.
        /// </summary>
        /// <param name="temporaryapplicationNumber"></param>
        /// <param name="forApplicantScreen"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryapplicationNumber}/borrowers-and-co-borrowers/{forApplicantScreen?}")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IBorrowerCoBorrowerInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> GetBorrowersAndCoBorrowers(string temporaryapplicationNumber, bool forApplicantScreen = true)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetBorrowersAndCoBorrowers(temporaryapplicationNumber, forApplicantScreen)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }

        }

        /// <summary>
        /// This end point is used to fetch the list of all the applicants in the Questionnaire including coborrowerspouse.
        /// </summary>
        /// <param name="temporaryapplicationNumber"></param>
        /// <param name="forApplicantScreen"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryapplicationNumber}/borrowers-and-co-borrowers-spouse/{forApplicantScreen?}")]
        [ProducesResponseType(typeof(List<IBorrowerCoBorrowerInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> GetBorrowersAndCoBorrowersWithSpouse(string temporaryapplicationNumber, bool forApplicantScreen = true)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetBorrowersAndCoBorrowersAndSpouse(temporaryapplicationNumber, forApplicantScreen)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }

        }

        /// <summary>
        /// This end point is used to fetch the list of all the applicants in the Questionnaire including coborrowerspouse.
        /// </summary>
        /// <param name="temporaryapplicationNumber"></param>
        /// <param name="forApplicantScreen"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryapplicationNumber}/borrowers-and-co-borrowers-spousename/{forApplicantScreen?}")]
        [ProducesResponseType(typeof(List<IBorrowerCoBorrowerInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> GetBorrowersAndCoBorrowersWithSpouseName(string temporaryapplicationNumber, bool forApplicantScreen = true)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetBorrowersAndCoBorrowersAndSpouseName(temporaryapplicationNumber, forApplicantScreen)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }

        }

        /// <summary>
        /// This end point is used to assign applicant as primary borrower. The existing borrower will then become a co-borrower.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="sectionId"></param>
        /// <param name="subSectionId"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/assign-primary-borrower")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> AssignPrimaryBorrower(string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            //This function will assign one of the borrowers as Primary Borrower. The existing borrower will then become a co-borrower.
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.AssignPrimaryBorrower(temporaryApplicationNumber, sectionId, subSectionId)));
            }
            catch (QuestionNotFoundException ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This endpoint creates the Post application, after the summary screen.
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="borrower"></param>
        /// <param name="allowEmail"></param>
        /// <returns></returns>
        [HttpPost("/{temporaryApplicationNumber}/create-coborrower/{allowEmail?}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddNewCoborrowerForHub(string temporaryApplicationNumber, [FromBody]AddCoBorrowerModal borrower, bool allowEmail = true)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.AddNewCoborrowerForHub(temporaryApplicationNumber,borrower, allowEmail)));
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                return ErrorResult.BadRequest(ex.Message);
            }
        }


#endregion

#region Applications

    /// <summary>
        /// This end point is used to fetch the list of all the applicantions
        /// </summary>
        /// <returns></returns>
        [HttpGet("/applications/all")]
#if DOTNET2
        [ProducesResponseType(typeof(List<IApplication>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

#endif
        public async Task<IActionResult> GetAllSubmittedApplications()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.GetAllSubmittedApplications()));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }

        }


#endregion

#region Rental Copied all dependant fields
         /// <summary>
        /// This end point is used to UpdateRefinanceAddressSameQuestions
        /// </summary>
        /// <returns></returns>
        [HttpPut("/updateForHub/{temporaryApplicationNumber}/{sectionId}/{subSectionId}")]
        [ProducesResponseType(typeof(List<IApplication>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateRefinanceAddressSameQuestions(string  temporaryApplicationNumber,int sectionId,string subSectionId)
        {
            try
            {
                await QuestionnaireService.UpdateRefinanceAddressSameQuestions(temporaryApplicationNumber,sectionId,subSectionId);
                return Ok();
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }

        }
#endregion
        private static ActionResult UnauthorizedResult()
        {
#if DOTNET2
            return new UnauthorizedResult();
#else
            return new HttpUnauthorizedResult();
#endif
        }
#endregion

    #region Validate Email - Already Exists
     /// <summary>
        /// This end point updates one question at a time.
        /// </summary>
        /// <param name="temporaryapplicationNumber"></param>
        /// <param name="emailId"></param>
        /// <returns></returns>
        [HttpGet("/{temporaryapplicationNumber}/check/email/{emailId}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> IsEmailAlreadyExists(string temporaryapplicationNumber, string emailId)
        {
            return await ExecuteAsync(async () => Ok(await QuestionnaireService.IsEmailAlreadyExists(temporaryapplicationNumber,emailId)));
        }

    #endregion

        /// <summary>
        /// This end point is used to all the sub seciton and related information for all the user except borrower
        /// </summary>
        /// <param name="temporaryapplicationNumber">The application number</param>
        /// <param name="sectionId">sections id</param>
        /// <param name="subsectionId">subsection id </param>
        /// <param name="actionType">The action Type , All = for all user except borrower
        ///     spouse == for spouse user
        ///    coborrower = for all the coborrower
        /// </param>
        /// <param name="subSectionIds">list of subsections ids</param>
        /// <returns></returns>
        [HttpDelete("/{temporaryapplicationNumber}/section/{sectionId}/subsection/{subsectionId}/remove-user-subsections/{actionType}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> RemoveAllSubSectionwithUser([FromBody] SubSectionIdList subSectionIds,string temporaryapplicationNumber,int sectionId, string subsectionId, string actionType = "All")
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await QuestionnaireService.RemoveSubSectionAndAnswer(temporaryapplicationNumber, subSectionIds,sectionId,actionType)));
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                //return Ok(exception.Message); //return ErrorResult.BadRequest(exception.Message);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <returns></returns>
        [HttpPut("/{temporaryapplicationNumber}/section/{sectionId}/subsection/{subSectionId}/questionsection/{questionSectionSeqNo}/questionId/{questionId}/questiontype/{questionType}/event/{eventName}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> EventReminderInvite(
            [FromBody] ReminderInviteRequest reminderInviteRequest,
            string temporaryApplicationNumber, 
            int sectionId, 
            string subSectionId, 
            int questionSectionSeqNo, 
            string questionId, 
            string questionType, 
            string eventName)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(
                        await QuestionnaireService.EventReminderInvite(reminderInviteRequest, temporaryApplicationNumber, 
                        sectionId, subSectionId, questionSectionSeqNo, questionId, questionType, eventName)
                    )
                );
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// To check if the applicant has signedup in docitt application
        /// </summary>
        /// <param name="temporaryApplicationNumber">application number</param>
        /// <param name="applicantEmailId">applicant email id</param>
        /// <returns></returns>
        [HttpGet("{temporaryapplicationNumber}/applicant/{applicantEmailId}")]
        [ProducesResponseType(typeof(ApplicantSignUpStatus), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> HasUserSignedUp(string temporaryApplicationNumber, string applicantEmailId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(
                        await QuestionnaireService.HasUserSignedUp(temporaryApplicationNumber, applicantEmailId)
                    )
                );
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Copy Application data
        /// </summary>
        /// <param name="fromApplicationNumber"></param>
        /// <param name="toApplicationNumber"></param>
        /// <returns></returns>
        [HttpPost("{fromApplicationNumber}/{toApplicationNumber}/copy")]
        [ProducesResponseType(typeof(ApplicantSignUpStatus), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CopyAnswer(string fromApplicationNumber, string toApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    await QuestionnaireService.CopyAnswer(fromApplicationNumber, toApplicationNumber);
                    return NoContent();
                });
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        
        /// <summary>
        /// Copy Application data
        /// </summary>
        /// <param name="fromApplicationNumber"></param>
        /// <param name="toApplicationNumber"></param>
        /// <returns></returns>
        [HttpPost("{fromApplicationNumber}/{toApplicationNumber}/spouse-information/copy")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CopyAnswerForSpouse(string fromApplicationNumber, string toApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(                
                    await QuestionnaireService.CopyAnswerForSpouse(fromApplicationNumber, toApplicationNumber)
                ));
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        /// <summary>
        /// Verify if application belongs to the logged in user
        /// </summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <returns></returns>
        [HttpGet("{temporaryApplicationNumber}/verify-application")]
        [ProducesResponseType(401)]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyApplication(string temporaryApplicationNumber)
        {
            try
            {
                if (await(QuestionnaireService.VerifyApplication(temporaryApplicationNumber)))
                {
                    return NoContent();
                }
                else
                {
                    return UnauthorizedResult();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

       /// <summary>
       /// Send Invite to individual co-borrower when borrower adding and inviting.
       /// </summary>
       /// <param name="InviteRequest">Invite request object.</param>
       /// <param name="temporaryApplicationNumber">Application number</param>
       /// <param name="sectionId">Section id</param>
       /// <param name="subSectionId">Sub Section id</param>
       /// <param name="questionSectionSeqNo">question section seq no</param>
       /// <param name="questionId">question id</param>
       /// <returns>return true or false</returns>
        [HttpPut("/{temporaryapplicationNumber}/section/{sectionId}/subsection/{subSectionId}/questionsection/{questionSectionSeqNo}/questionId/{questionId}/invite-coborrower")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> SendInviteToCoBorrower(
            [FromBody] SendInviteRequest InviteRequest,
            string temporaryApplicationNumber, 
            int sectionId, 
            string subSectionId, 
            int questionSectionSeqNo, 
            string questionId)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(
                        await QuestionnaireService.SendInviteToCoBorrower(InviteRequest, temporaryApplicationNumber, 
                        sectionId, subSectionId, questionSectionSeqNo, questionId)
                    )
                );
            }
            catch (QuestionnaireNotFoundException exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

       /// <summary>
       ///set joint to individual
       /// </summary>     
       /// <param name="temporaryApplicationNumber">Application number</param>       
       /// <returns>return true or false</returns>
        [HttpPut("/{temporaryapplicationNumber}/joint-to-individual-credit")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> JointToIndividualCredit(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(
                        await QuestionnaireService.JointToIndividualCredit(temporaryApplicationNumber)
                    )
                );
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
       /// set "Do you want to add another co-borrower" to no  and remove all the 
       /// co-borrower related information.
       /// </summary>     
       /// <param name="temporaryApplicationNumber">Application number</param>       
       /// <returns>return true or false</returns>
        [HttpPut("/{temporaryapplicationNumber}/remove-coborrower")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> RemoveCoBorrower(string temporaryApplicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(
                        await QuestionnaireService.RemoveCoBorrower(temporaryApplicationNumber)
                    )
                );
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message, exception);
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}