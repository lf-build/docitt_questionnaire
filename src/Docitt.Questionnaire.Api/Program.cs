﻿#if DOTNET2

using System;
using Microsoft.AspNetCore.Hosting;

namespace Docitt.Questionnaire.Api
{
    internal class Program
    {
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine($"Started on Port {5000}");
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }

        }


    }
}

#endif