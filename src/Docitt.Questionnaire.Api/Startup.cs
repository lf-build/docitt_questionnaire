﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.NumberGenerator.Client;

using Docitt.Questionnaire.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;

using LendFoundry.Security.Identity.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.StatusManagement.Client;
using Docitt.Application.Client;
using Docitt.Applicant.Client;
using Docitt.UserProfile.Client;
using LendFoundry.Security.Encryption;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Persistence.Mongo;
using System.Collections.Generic;
using Docitt.AssignmentEngine.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.Foundation.ServiceDependencyResolver;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Http;
#endif
namespace Docitt.Questionnaire.Api
{
    internal class Startup
    {
        //public Startup(IHostingEnvironment env) { }
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "DocittQuestionnaire"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
               {
                   { "Bearer", new string[] { } }
               });

                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "Docitt.Questionnaire.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#endif
            //services.AddSwaggerDocumentation();
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<QuestionnaireConfiguration>(Settings.ServiceName);
            //services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddNumberGeneratorService();
            services.AddUserProfileService();
            services.AddApplicantService();
            services.AddDecisionEngine();
            services.AddStatusManagementService();
            services.AddApplicationService();
            //services.AddActionCenter(Settings.RequiredCondition.Host, Settings.RequiredCondition.Port);
            services.AddMongoConfiguration(Settings.ServiceName);

            //services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddIdentityService();
            services.AddAssignmentService();
            services.AddInviteService();
            services.AddDependencyServiceUriResolver<QuestionnaireConfiguration>(Settings.ServiceName);
            services.AddTransient<Docitt.Application.IApplicationRequest, Docitt.Application.ApplicationRequest>();
            services.AddTransient<IQuestionnaireConfiguration, QuestionnaireConfiguration>();
            services.AddTransient<QuestionnaireConfiguration>(p => p.GetService<IConfigurationService<QuestionnaireConfiguration>>().Get());
            services.AddTransient<IApplicationFields>(p => p.GetService<IConfigurationService<ApplicationFields>>().Get());

            services.AddTransient<IQuestionnaireService, QuestionnaireService>();
            
            services.AddTransient<IQuestionnaireAnswerRepository, QuestionnaireAnswerRepository>();
            services.AddTransient<IQuestionnaireRepository, QuestionnaireRepository>();

            //// Service
            services.AddTransient<IInviteValidationService, InviteValidationService>();
            
            services.AddTransient<IQuestionnaireConfiguration, QuestionnaireConfiguration>();
            services.AddTransient<IFormRepository, FormRepository>();
            services.AddTransient<IFormService, FormService>();
            services.AddTransient<IQuestionnaireDataService, QuestionnaireDataService>();
            services.AddTransient<IQuestionSectionRepository, QuestionSectionRepository>();
            services.AddTransient<IQuestionSubSectionRepository, QuestionSubSectionRepository>();
             services.AddTransient<IQuestionSectionsRepository, QuestionSectionsRepository>();
            services.AddEncryptionHandler();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseHealthCheck();
		    app.UseCors(env);
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            //app.UseSwaggerDocumentation();
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "DOCITT questionnaire Service");
            });

#endif 
            
            // app.UseErrorHandling();
        //   app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseRequestLogging();
            app.UseMvc();

            app.UseConfigurationCacheDependency();
        }
    }

}
