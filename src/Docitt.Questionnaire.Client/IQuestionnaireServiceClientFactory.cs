﻿using LendFoundry.Security.Tokens;

namespace Docitt.Questionnaire.Client
{
    public interface IQuestionnaireServiceClientFactory
    {
        IQuestionnaireService Create(ITokenReader reader);
    }
}
