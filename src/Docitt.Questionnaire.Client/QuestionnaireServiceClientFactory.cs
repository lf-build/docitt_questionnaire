﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.Questionnaire.Client
{
    public class QuestionnaireServiceClientFactory : IQuestionnaireServiceClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public QuestionnaireServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public QuestionnaireServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; set; }

        private Uri Uri { get; }


        public IQuestionnaireService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("questionnaire");
            }


            var client = Provider.GetServiceClient(reader, uri);
            return new QuestionnaireService(client);
        }

       
    }
}
