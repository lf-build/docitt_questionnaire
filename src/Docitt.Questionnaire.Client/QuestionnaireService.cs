﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using Docitt.Application;
using System.Linq;

namespace Docitt.Questionnaire.Client
{
    public class QuestionnaireService : IQuestionnaireService, IQuestionnaireDataService
    {
        #region Constructors
        public QuestionnaireService(IServiceClient client)
        {
            Client = client;
        }
        #endregion

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion

        #region Public Methods

        public FormType GetFormType(string type)
        {
            throw new NotImplementedException();
        }

        #region "FormLookup"

        public Task<bool> CreateFormTemplate(IForm form)
        {
            throw new NotImplementedException();
        }

        public async Task<IForm> CreateFormTemplateReturnId(IForm form, string type, string formName)
        { 
                return await Client.PostAsync<IForm, Form>("/create-form/{formName}/{type}", form, true);
        }

        public async Task<string> GetActiveFormName()
        {
            var response = await Client.GetAsync<dynamic>($"/get-active-form");
            return Convert.ToString(response);
        }

        #endregion

        #region "Application"

        public async Task<IApplicationOverAllStatus> GetApplicationStatus()
        {
            return await Client.GetAsync<ApplicationOverAllStatus>($"/applicationstatus");
        }

        public async Task<List<Dictionary<string, ISummary>>> GetSummary(string temporaryApplicationNumber, IQuestionnaire questionnaire = null, bool isFromLenderDashboard = false, bool isAllowBlankFieldData = false)
        {
            return await Client.GetAsync<List<Dictionary<string, ISummary>>>($"/{temporaryApplicationNumber}/summary");
        }

        public Task<List<Dictionary<string, List<ISubSectionSummary>>>> GetSummary(string temporaryApplicationNumber, IQuestionnaire questionnaire = null)
        {
            throw new NotImplementedException();
        }

        public Task<List<Dictionary<string, ISummary>>> GetApplicationSummary(string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<IApplication> GetApplication(string temporaryApplicationNumber)
        {
            return await Client.GetAsync<Application>($"/application/{temporaryApplicationNumber}");
        }

        public async Task<ITransitionFormExtended> GetApplicationLoanDetail(string temporaryApplicationNumber, string userName)
        {
            userName = System.Web.HttpUtility.UrlEncode(userName);
            return await Client.GetAsync<TransitionForm>($"/{temporaryApplicationNumber}/user/{userName}/loan-detail");
        }

        public Task<IApplicationResponse> CreateApplication(IQuestionnaire questionnaire,IBorrowerCoBorrowerSpouseInfo borrowerInfo)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationResponse> CreateApplication()
        {
            throw new NotImplementedException();
        }

        public async Task<IApplicationCreationResponse> Confirm(string temporaryApplicationNumber)
        {
            //GenerateApplication
            return await Client.PostAsync<dynamic, ApplicationCreationResponse>($"/{temporaryApplicationNumber}/create-application", null, true);
        }

        public Task<bool> CreatePostApplication(string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateApplicationStatus(string temporaryApplicationNumber, string applicationStatus)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CompletePostApplication(string applicationNumber)
        {
           var response = await Client.PutAsync<dynamic, dynamic>($"/{applicationNumber}/complete-post-application", null, true);
           return Convert.ToBoolean(response);
        }

        public Task<ITransitionFormExtended> GenerateApplicationFromQuestionnaire()
        {
            throw new NotImplementedException();
        }

        public async Task<IUserBasicInfo> GetBasicInformation(string applicationNumber)
        {
            return await Client.GetAsync<UserBasicInfo>($"/{applicationNumber}/basic-info");
        }
        

        #endregion

        #region "Sections"

        public async Task<ISection> GetSection(int SectionId)
        {
            var sectionId = Convert.ToString(SectionId);
            return await Client.GetAsync<Section>($"/section/{sectionId}");
        }

        public async Task<ISection> GetSection(string temporaryApplicationNumber, int SectionId)
        {
             var sectionId = Convert.ToString(SectionId);
              return await Client.GetAsync<Section>($"/{temporaryApplicationNumber}/section/{sectionId}");
        }

        public async Task<ISection> GetRawSection(string temporaryApplicationNumber, int SectionId)
        {
             var sectionId = Convert.ToString(SectionId);
              return await Client.GetAsync<Section>($"/{temporaryApplicationNumber}/section/{sectionId}/raw");
        }

        public async Task<ISectionListResponse> GetSectionList(string temporaryApplicationNumber, string formType)
        {

            return await Client.GetAsync<SectionListResponse>($"/{temporaryApplicationNumber}/section-list/{formType}");
        }

        public async Task<ISectionListResponse> GetSectionList(string formType)
        {
            return await Client.GetAsync<SectionListResponse>($"/section-list/{formType}");
        }

        public Task<bool> AddSection(ISection sectionRequest, string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionSection(IAddQuestionSectionRequest questionSectionRequest, string temporaryApplicationNumber, int sectionId, string subSectionId, string questionSectionId)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> AddQuestionSection(QuestionSection questionSectionRequest, string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            var sectionIdString = Convert.ToString(sectionId);
             var response= await Client.PostAsync<QuestionSection, dynamic>($"/{temporaryApplicationNumber}/section/{sectionIdString}/subsection/{subSectionId}/questionsection", questionSectionRequest, true);
             return Convert.ToBoolean(response);
        }

        public Task<bool> AppendQuestionSection(QuestionSection questionSectionRequest, string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateSectionCompletionStatus(string temporaryApplicationNumber, int sectionId, bool isCompleted)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteQuestionSection(string temporaryApplicationNumber, int sectionId, string subSectionId, int questionSectionSeqNo)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region "SubSections"

        public async Task<bool> AddSubSections(AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, int sectionId)
        {         
             var sectionIdString = Convert.ToString(sectionId);
             var response= await Client.PostAsync<AddSubSectionsRequest, dynamic>($"/{temporaryApplicationNumber}/section/{sectionIdString}/subsections", subSectionRequest, true);
             return Convert.ToBoolean(response);
        }

        public async Task<bool> AppendSubSections(AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, string sectionId)
        {
             var response= await Client.PostAsync<AddSubSectionsRequest, dynamic>($"/{temporaryApplicationNumber}/section/{sectionId}/subsections", subSectionRequest, true);
             return Convert.ToBoolean(response);
        }

        public Task<bool> AppendSubSections(AddSubSectionsRequest subSectionRequest, string temporaryApplicationNumber, int sectionId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RemoveSubSections(string temporaryApplicationNumber, int sectionId, SubSectionIdList subSectionIds)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> DeleteSubSection(string temporaryApplicationNumber, int sectionId, string subSectionId, IQuestionnaire questionnaire = null)
        {
            var request = new RestRequest("/{formName}/{type}", Method.DELETE);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);
            request.AddUrlSegment("sectionId", Convert.ToString(sectionId));
            request.AddUrlSegment("subSectionId", subSectionId);

            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<bool> DeleteSubSections(string temporaryApplicationNumber, int sectionId, SubSectionIdList subSectionIds)
        {
            var request = new RestRequest("/{temporaryApplicationNumber}/section/{sectionId}/subsectionids", Method.DELETE);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);
            request.AddUrlSegment("sectionId", Convert.ToString(sectionId));
            request.AddJsonBody(subSectionIds);

            return await Client.ExecuteAsync<bool>(request);
        }

        #endregion

        #region "Events"

        public async Task<bool> ExecuteEvent(string temporaryApplicationNumber, int sectionId, string subSectionId, string eventName)
        {
            var request = new RestRequest("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/event/{eventName?}", Method.POST);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);
            request.AddUrlSegment("sectionId", Convert.ToString(sectionId));
            request.AddUrlSegment("subsectionId", subSectionId);
            request.AddUrlSegment("eventName", eventName);

            return await Client.ExecuteAsync<bool>(request);
        }

        #endregion

        #region "Questionnaires"
        public async Task<List<IUserAccessedStatus>> GetUserStateInformation(string applicationNumber)
        {
            return (await Client.GetAsync<List<UserAccessedStatus>>($"{applicationNumber}/userstate")).Cast<IUserAccessedStatus>().ToList();
        }
        public async Task<List<IQuestionnaire>> GetQuestionnaires()
        {
             return await Client.GetAsync<List<IQuestionnaire>>($"/qualifyingapplications");
        }

        public Task<IQuestionnaire> GetQuestionnaire()
        {
            throw new NotImplementedException();
        }

        public async Task<IQuestionnaire> GetQuestionnaire(string temporaryApplicationNumber, string userName = null)
        {
            return await Client.GetAsync<Questionnaire>($"/{temporaryApplicationNumber}");
        }

        public async Task<IQuestionnaire> GetStarted(string formType, bool allowNew=false)
        {
            return await Client.PostAsync<dynamic, Questionnaire>($"/{formType}/{allowNew}", null, true);
        }

        public Task<bool> UpdateUsernameField(string inviteId)
        {
            throw new NotImplementedException();
        }

        public async Task<IQuestionnaire> GetQuestionnaireStatus()
        {
            return await Client.PostAsync<dynamic, Questionnaire>($"/status", null, true);
        }

        public async Task<IQuestionnaire> GetQuestionnaireStatus(string temporaryApplicationNumber)
        {
            return await Client.GetAsync<Questionnaire>($"/status/{temporaryApplicationNumber}");

        }

        public Task<IQuestion> GetByQuestionId(string questionId)
        {
            throw new NotImplementedException();
        }

        public Task<ITransitionFormExtended> GetInformationFromQuestionnaire(string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<IQuestion> GetQuestion(string temporaryApplicationNumber, int sectionId, string subSectionId, string questionId, IQuestionnaire questionnaire=null)
        {
            var sectionIdString = Convert.ToString(sectionId);
                 return await Client.GetAsync<Question>($"/{temporaryApplicationNumber}/section/{sectionIdString}/subsection/{subSectionId}/question/{questionId}");
        }

        public async Task<IQuestion> GetLoanPurposeQuestion(string temporaryApplicationNumber)
        {
             return await Client.GetAsync<Question>($"/{temporaryApplicationNumber}/loan-purpose-question");
        }

        public Task<Dictionary<string, List<IBankInfo>>> GetBankInfo(string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<IBankAlert> GetBankAlertInfo(string temporaryApplicationNumber, string customerId)
        {
             return await Client.GetAsync<BankAlert>($"/{temporaryApplicationNumber}/{customerId}/bank-alert-info");
        }

        public async Task<IQuestionnaire> CreateQuestionnaireAndGetQuestionnaireStatus(string formType)
        {
            return await Client.PostAsync<dynamic, Questionnaire>($"/status/{formType}", null, true);
        }

        public Task<IQuestionnaireService> Add(IQuestionnaireRequest questionnaireRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> AddQuestion(IQuestion question, string formId, string sectionId)
        {
            var response = await Client.PutAsync<IQuestion, dynamic>($"addquestion/{formId}/{sectionId}", question, true);
            return Convert.ToBoolean(response);
        }

        public Task<string> AddQuestionnaire(FormType formType, bool allowNew=false)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddBankInfo(BankInfo bankInfo, string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateQuestion(IAnswerUpdateRequest questionRequest)
        {
            var   sectionId= Convert.ToString(questionRequest.SectionId);
            var temporaryApplicationNumber= questionRequest.TemporaryApplicationNumber;
            var subsectionId=questionRequest.SubSectionId;
            var questionId=questionRequest.QuestionId;
            var response = await Client.PutAsync<IAnswerUpdateRequest, dynamic>($"/update/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/question/{questionId}", questionRequest, true);
            return Convert.ToBoolean(response);
        }

        public Task<bool> UpdatePurposeOfLoanQuestion(string temporaryApplicationNumber, AnswerUpdateRequest questionRequest)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateQuestionAndStatus(IAnswerUpdateRequest questionRequest)
        {
            throw new NotImplementedException();
        }

        public Task<Dictionary<string, List<IBankInfo>>> DeleteBankInfo(string temporaryApplicationNumber, string bankInfoId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region "Users"

        public Task<IBorrowerCoBorrowerInfo> GetBorrowerInfoFromTheMapping(int sectionId, List<ISubSection> subSections, ApplicantType applicantType)
        {
            throw new NotImplementedException();
        }

        public Task<IBorrowerCoBorrowerInfo> GetBorrowerInfoFromTheMapping(int sectionId, List<ISubSection> subSections)
        {
            throw new NotImplementedException();
        }

        public async Task<List<IBorrowerCoBorrowerInfo>> GetBorrowersAndCoBorrowers(string temporaryApplicationNumber, bool forApplicantScreen = true, IQuestionnaire questionnaire = null, bool isSpouseFromJointCredit = true, bool getCoborrowerSpouses = false)
        {            
              return (await Client.GetAsync<List<BorrowerCoBorrowerInfo>>($"/{temporaryApplicationNumber}/borrowers-and-co-borrowers")).Cast<IBorrowerCoBorrowerInfo>().ToList();
        }

        public async Task<List<IBorrowerCoBorrowerInfo>> GetBorrowersAndCoBorrowersAndSpouse(string temporaryApplicationNumber, bool ForApplicantScreen = false)
        {            
              return (await Client.GetAsync<List<BorrowerCoBorrowerInfo>>($"/{temporaryApplicationNumber}/borrowers-and-co-borrowers-spouse")).Cast<IBorrowerCoBorrowerInfo>().ToList();
        }

         public async Task<List<IBorrowerCoBorrowerSpouseInfo>> GetBorrowersAndCoBorrowersAndSpouseName(string temporaryApplicationNumber, bool ForApplicantScreen = false, IQuestionnaire questionnaire = null)
        {            
              return (await Client.GetAsync<List<BorrowerCoBorrowerSpouseInfo>>($"/{temporaryApplicationNumber}/borrowers-and-co-borrowers-spousename")).Cast<IBorrowerCoBorrowerSpouseInfo>().ToList();
        }

        public async Task<bool> AssignPrimaryBorrower(string temporaryApplicationNumber, int sectionId, string subSectionId)
        {
            var request = new RestRequest("/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subsectionId}/event/{eventName?}", Method.POST);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);
            request.AddUrlSegment("sectionId", Convert.ToString(sectionId));
            request.AddUrlSegment("subsectionId", subSectionId);

            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<List<Dictionary<string, ISummary>>> GetQuestionnaireSummary(string temporaryApplicationNumber, bool AllowBlankAnswerField)
        {
            return await Client.GetAsync<List<Dictionary<string, ISummary>>>($"questionnaire/{temporaryApplicationNumber}/summary");
        }

        #endregion

        #region "Questionnaire Section Configuration"

        public Task<InviteHubResponse> AddNewCoborrowerForHub(string temporaryApplicationNumber, AddCoBorrowerModal borrower, bool allowEmail)
        {
            throw new NotImplementedException();
        }

        #endregion

        #endregion

        #region Submitted Applications All

        public async Task<List<Docitt.Application.IApplication>> GetAllSubmittedApplications()
        {
            var result = await Client.GetAsync<List<Docitt.Application.Application>>($"/applications/all");
            return result.ToList<Docitt.Application.IApplication>();
        }
        
        #endregion

        #region Rental Copied all dependant fields

        public Task UpdateRefinanceAddressSameQuestions(string temporaryApplicationNumber,int sectionId, string subSectionId)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> IsEmailAlreadyExists(string temporaryApplicationNumber, string emailId)
        {
            var request = new RestRequest("/{temporaryApplicationNumber}/check/email/{emailId}", Method.GET);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);
            request.AddUrlSegment("emailId", emailId);

            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<object> GetQuestionnaireSectionData(string applicationId, string borrowerId, string sectionName)
        {
           var request = new RestRequest("/questionnaire/{applicationnumber}/section/{sectionname}/borrower/{borrowerId?}", Method.GET);
            request.AddUrlSegment("applicationnumber", applicationId);
             request.AddUrlSegment("sectionname", sectionName);
            request.AddUrlSegment("borrowerId", borrowerId);

            return await Client.ExecuteAsync<object>(request);
        }

        public async Task<Dictionary<string, object>> GetAllSectionOriginalData(string applicationId, string borrowerId)
        {
            var request = new RestRequest("/questionnaire/{applicationnumber}/section/all/borrower/{borrowerId?}", Method.GET);
            request.AddUrlSegment("applicationnumber", applicationId);            
            request.AddUrlSegment("borrowerId", borrowerId);

            return await Client.ExecuteAsync<Dictionary<string, object>>(request);
        }

        public async Task<bool> RemoveSubSectionAndAnswer(string applicationNumber, SubSectionIdList subSectionIds, int sectionId, string actionType)
        {

            var request = new RestRequest("/{temporaryapplicationNumber}/section/{sectionId}/remove-user-subsections/{actionType}", Method.DELETE);
            request.AddUrlSegment("temporaryApplicationNumber", applicationNumber);
            request.AddUrlSegment("sectionId", Convert.ToString(sectionId));
            request.AddUrlSegment("actionType", actionType);
            request.AddJsonBody(subSectionIds);
            return await Client.ExecuteAsync<bool>(request);            
        }

        #endregion

        public Task<bool> EventReminderInvite(ReminderInviteRequest reminderInviteRequest, string temporaryApplicationNumber, 
            int sectionId, string subSectionId, int questionSectionSeqNo, string questionId, string questionType, string eventName)
        {
            throw new NotImplementedException();
        }

        public async Task<IApplicantSignUpStatus> HasUserSignedUp(string temporaryApplicationNumber, string applicantEmailId)
        {
            var request = new RestRequest("/{temporaryApplicationNumber}/applicant/{applicantEmailId}", Method.GET);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);            
            request.AddUrlSegment("applicantEmailId", applicantEmailId);

            return await Client.ExecuteAsync<ApplicantSignUpStatus>(request);
        }

        public async Task CopyAnswer(string fromApplicationNumber, string toApplicationNumber)
        {
            var request = new RestRequest("/{fromApplicationNumber}/{toApplicationNumber}/copy", Method.POST);
            request.AddUrlSegment("fromApplicationNumber", fromApplicationNumber);            
            request.AddUrlSegment("toApplicationNumber", toApplicationNumber);

            await Client.ExecuteAsync(request); 
        }
        
        public async Task<bool> CopyAnswerForSpouse(string fromApplicationNumber, string toApplicationNumber)
        {
            var request = new RestRequest("/{fromApplicationNumber}/{toApplicationNumber}/spouse-information/copy", Method.POST);
            request.AddUrlSegment("fromApplicationNumber", fromApplicationNumber);            
            request.AddUrlSegment("toApplicationNumber", toApplicationNumber);

            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<bool> VerifyApplication(string temporaryApplicationNumber)
        {
            var request = new RestRequest("/{temporaryApplicationNumber}/verify-application", Method.GET);
            request.AddUrlSegment("temporaryApplicationNumber", temporaryApplicationNumber);

            return await Client.ExecuteAsync(request);
        }

        public async Task<bool> SendInviteToCoBorrower(SendInviteRequest InviteRequest, string temporaryApplicationNumber, int sectionId, string subSectionId, int questionSectionSeqNo, string questionId)
        {
            var response = await Client.PutAsync<SendInviteRequest, dynamic>($"/{temporaryApplicationNumber}/section/{sectionId}/subsection/{subSectionId}/questionsection/{questionSectionSeqNo}/questionId/{questionId}/invite-coborrower",InviteRequest, true);
           return Convert.ToBoolean(response);
        }

        public async Task<bool> JointToIndividualCredit(string applicationNumber)
        {
            ///{temporaryapplicationNumber}/joint-individual-credit"
           var response = await Client.PutAsync<string, dynamic>($"/{applicationNumber}/joint-individual-credit",null, true);
           return Convert.ToBoolean(response);
        }

        public async Task<bool> RemoveCoBorrower(string applicationNumber)
        {
           var response = await Client.PutAsync<string, dynamic>($"/{applicationNumber}/remove-coborrower",null, true);
           return Convert.ToBoolean(response);
        }
    }
}
