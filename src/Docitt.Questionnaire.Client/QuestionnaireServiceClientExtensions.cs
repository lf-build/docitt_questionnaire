﻿using LendFoundry.Security.Tokens; 

using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Questionnaire.Client
{
    public static class QuestionnaireServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddQuestionnaireService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IQuestionnaireServiceClientFactory>(p => new QuestionnaireServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IQuestionnaireServiceClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddQuestionnaireService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IQuestionnaireServiceClientFactory>(p => new QuestionnaireServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IQuestionnaireServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddQuestionnaireService(this IServiceCollection services)
        {
            services.AddTransient<IQuestionnaireServiceClientFactory>(p => new QuestionnaireServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IQuestionnaireServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

       
    }
}
