using LendFoundry.NumberGenerator;

namespace LendFoundry.Application.Mocks
{
    public class FakeApplicationNumberGenerator //: IGeneratorService
    {
        private int Id { get; set; }


        public string TakeNext(string entityId)
        {
            Id++;
            return Id.ToString();
        }
    }
}