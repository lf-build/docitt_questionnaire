﻿using Docitt.Questionnaire;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Application.Mocks
{
    public class FakeQuestionnaireService 
    {
        public ITenantTime TenantTime { get; set; }
        public List<IQuestionnaire> Questionnaires { get; } = new List<IQuestionnaire>();
        public int CurrentId { get; set; }

        public FakeQuestionnaireService(ITenantTime tenantTime, IEnumerable<IQuestionnaire> questionnaires):this(tenantTime)
        {
            Questionnaires.AddRange(questionnaires);
            CurrentId = Questionnaires.Count() + 1;
        }

        public FakeQuestionnaireService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
            CurrentId = 1;
        }


        /*public IApplicant Add(Applicant.IApplicantRequest applicant)
        {
            if (Applicants.Any(p => p.Email == applicant.Email))
                throw new ApplicantWithSameEmailAlreadyExist();

            applicant.Id = (CurrentId++).ToString();
            Applicants.Add(applicant);
            return applicant;
        }

        public void Update(string applicantId, IApplicant applicant)
        {
            Applicants.Remove(Applicants.First(a => a.Id.Equals(applicant.Id)));
            Applicants.Add(applicant);
        }

        public IApplicant Get(string applicantId)
        {
            return Applicants.First(applicant => applicant.Id.Equals(applicantId));
        }

        public void Delete(string applicantId)
        {
            Applicants.Remove(Applicants.First(a => a.Id.Equals(applicantId)));
        }

        public IEnumerable<IApplicant> Search(ISearchApplicantRequest searchApplicantRequest)
        {
            return Applicants.Where(a =>
            a.Ssn.ToLower() == searchApplicantRequest.Ssn.ToLower() &&
            a.FirstName.ToLower() == searchApplicantRequest.FirstName.ToLower() &&
            a.LastName.ToLower() == searchApplicantRequest.LastName.ToLower() &&
            a.Email.ToLower() == searchApplicantRequest.Email.ToLower() &&
            a.DateOfBirth == searchApplicantRequest.DateOfBirth
            ).ToList();
        }

        public void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateName(string applicantId, IApplicantNameRequest applicantNameRequest, string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public IApplicant GetByUserId(string userId)
        {
            return Mock.Of<IApplicant>();
            return Applicants.FirstOrDefault(applicant => applicant.UserIdentity.UserId.Equals(userId));
        }

        public void AttachUserId(string applicantId, string userId)
        {
            throw new NotImplementedException();
        }

        public void LinkToUser(string applicantId, string userId)
        {
            throw new NotImplementedException();
        }

        public string CreateIdentityToken(string applicantId)
        {
            throw new NotImplementedException();
        }

        public bool IsTokenValid(string token)
        {
            throw new NotImplementedException();
        }

        public IApplicant FindByTokenAndLast4Ssn(string token, string lastFourSsn)
        {
            throw new NotImplementedException();
        }
        */

        public Task<IQuestionnaireService> Add(IQuestionnaireRequest questionnaireRequest)
        {
            throw new NotImplementedException();
        }

        public Task<IQuestion> GetByQuestionId(string questionId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateQuestion(IQuestionRequest questionRequest, string temporaryapplicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<IQuestionnaireResponse> GetQuestionnaire(string temporaryApplicationNumber)
        {
            return Task.Run(() => (IQuestionnaireResponse)new QuestionnaireResponse(Questionnaires.First(q => q.TemporaryApplicationNumber.Equals(temporaryApplicationNumber))));
        }

        public Task<bool> CreateFormTemplate(IForm form)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestion(IQuestion question, string formId, string sectionId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionnaire(string userId, string formId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionnaire()
        {
            throw new NotImplementedException();
        }
    }
}