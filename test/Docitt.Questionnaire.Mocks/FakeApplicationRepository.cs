﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using Docitt.Questionnaire;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Application.Mocks
{
    public class FakeQuestionnaireRepository
    {
        public FakeQuestionnaireRepository(ITenantTime tenantTime, IEnumerable<IQuestionnaire> questionnaires) : this(tenantTime)
        {
            Questionnaires.AddRange(questionnaires);
        }

        public FakeQuestionnaireRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public ITenantTime TenantTime { get; set; }

        public List<IQuestionnaire> Questionnaires { get; set; } = new List<IQuestionnaire>();


        public Task<IQuestionnaire> Get(string id)
        {
            return Task.FromResult(Questionnaires.FirstOrDefault(questionnaire => questionnaire.TemporaryApplicationNumber.Equals(id)));
        }

        /*
        public void Add(IQuestionnaire application)
        {
            //IT IS JUST FOR ROLLBACK TEST
            if (application.Purpose == "Should fail by Fake Repo")
                throw new ArgumentException(nameof(application));
            Applications.Add(application);
        }

        public void Remove(IQuestionnaire application)
        {
            throw new NotImplementedException();
        }

        public void Update(IQuestionnaire application)
        {
            Applications.Remove(GetByApplicationNumber(application.ApplicationNumber));
            Applications.Add(application);
        }

        public IQuestionnaire GetByApplicationNumber(string applicationNumber)
        {
            return Applications.FirstOrDefault(applicant => applicant.ApplicationNumber.Equals(applicationNumber));
        }

        public List<string> GetApplicationNumbersByApplicantNumber(string applicantId)
        {
            return
                          Applications.Where(
                              application => application.Applicants.Any(applicant => applicantId == applicant.ApplicantId))
                              .Select(application => application.ApplicationNumber).ToList();
        }

        public void UpdateApplication(string applicationNumber, IQuestionnaireUpdateRequest applicationRequest)
        {
            var applicant = GetByApplicationNumber(applicationNumber);
            applicant.Amount = applicationRequest.Amount;
            applicant.Purpose = applicationRequest.Purpose;
        }

        public IQuestionnaire UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.Addresses = addresses;

            return application;
        }

        public IQuestionnaire UpdatePrimaryAddress(string applicationNumber, List<IAddress> addresses)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.IsPrimary == true);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Primary Applicant not found for application {applicationNumber} ");

            applicantToUpdate.Addresses = addresses;

            return application;
        }

        public IQuestionnaire UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.PhoneNumbers = phoneNumbers;

            return application;
        }

        public IQuestionnaire UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");
            var applicants = application.Applicants;
            var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
            if (applicantToUpdate == null)
                throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

            applicantToUpdate.CurrentEmployer = applicantUpdateRequest.CurrentEmployer;
            applicantToUpdate.EmploymentStatus = applicantUpdateRequest.EmploymentStatus;
            applicantToUpdate.HomeOwnership = applicantUpdateRequest.HomeOwnership;
            applicantToUpdate.StatedGrossAnnualIncome = applicantUpdateRequest.StatedGrossAnnualIncome;
            return application;
        }

        public Task<IEnumerable<IQuestionnaire>> All(Expression<Func<IQuestionnaire, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IQuestionnaire, bool>> query)
        {
            throw new NotImplementedException();
        }

        public IQuestionnaire UpdateScore(string applicationNumber, string scoreName, Score score)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            var scores = new List<Score>(application.Scores ?? new List<Score>()) { score };
            application.Scores = scores;
            return application;
        }

        public IQuestionnaire UpdateBankAccounts(string applicationNumber, List<IBankAccount> bankAccounts)
        {
            var application = GetByApplicationNumber(applicationNumber);
            if (application == null)
                throw new NotFoundException($"Application {applicationNumber} not found");

            application.BankAccounts = bankAccounts;
            return application;
        }

        public void UpdateAnnualIncome(string applicationNumber, double annualIncome)
        {
            Applications = Applications.Select(a =>
            {
                if (a.ApplicationNumber == applicationNumber)
                {
                    a.VerifiedAnnualIncome = annualIncome;
                }
                return a;
            }).ToList();            
        }
        */

        public IQuestion GetByQuestionId(string questionId)
        {
            throw new NotImplementedException();
        }

        public IQuestion UpdateQuestion(IQuestionRequest questionRequest)
        {
            throw new NotImplementedException();
        }

        public IQuestion AddQuestion(IQuestion question)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllQuestions()
        {
            throw new NotImplementedException();
        }

        public IQuestionnaire GetQuestionnaire(string tempApplicationNumber)
        {
            return Questionnaires.FirstOrDefault(questionnaire => questionnaire.TemporaryApplicationNumber.Equals(tempApplicationNumber));
        }

        //Task<IQuestionnaire> IRepository<IQuestionnaire>.Get(string id)
        //{
        //    throw new NotImplementedException();
        //}

        public Task<IEnumerable<IQuestionnaire>> All(Expression<Func<IQuestionnaire, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IQuestionnaire item)
        {
            throw new NotImplementedException();
        }

        public void Remove(IQuestionnaire item)
        {
            throw new NotImplementedException();
        }

        public void Update(IQuestionnaire item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IQuestionnaire, bool>> query)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionnaire(string userId, IForm form, string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }
    }
}
