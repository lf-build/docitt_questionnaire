﻿using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;

namespace Docitt.Questionnaire.Client.Tests
{
    public class QuestionnaireServiceClientTests
    {
        #region Constructors
        public QuestionnaireServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            QuestionnaireServiceClient = new QuestionnaireService(MockServiceClient.Object);
        }
        #endregion

        #region Private Properties
        public IQuestionnaireService QuestionnaireServiceClient { get; }
        public IRestRequest Request { get; set; }
        public Mock<IServiceClient> MockServiceClient { get; }

        #endregion

        #region Public Methods
        //[Fact]
        //public async Task Client_Add_Application()
        //{
        //    var application = new QuestionnaireRequest();

        //    MockServiceClient.Setup(s => s.ExecuteAsync<QuestionnaireResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                   new QuestionnaireResponse()));

        //    var result = await QuestionnaireServiceClient.Add(application);
        //    Assert.Equal("/", Request.Resource);
        //    Assert.Equal(Method.POST, Request.Method);
        //    Assert.NotNull(result);
        //}

        //[Fact]
        //public async Task Client_Add_MerchantApplication()
        //{
        //    var application = new ApplicationRequest();

        //    MockServiceClient.Setup(s => s.ExecuteAsync<ApplicationResponse>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                   new ApplicationResponse()));

        //    var result = await ApplicationServiceClient.AddMerchantApplication(application);
        //    Assert.Equal("/lead", Request.Resource);
        //    Assert.Equal(Method.POST, Request.Method);
        //    Assert.NotNull(result);
        //}

        //[Fact]
        //public async Task Client_Get_QuestionnaireByTemporaryApplicationNumber()
        //{
        //    MockServiceClient.Setup(s => s.ExecuteAsync<Questionnaire>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                   new Questionnaire()
        //                   { ApplicationNumber = "1" }));

        //    var result = await QuestionnaireServiceClient.GetByApplicationNumber("1");
        //    Assert.Equal("/qualifyingApplication/{applicationNumber}", Request.Resource);
        //    Assert.Equal(Method.GET, Request.Method);
        //    Assert.NotNull(result);
        //}

        //[Fact]
        //public async Task Client_LinkBankInformation_Application()
        //{

        //    MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                  true));

        //    await QuestionnaireServiceClient.UpdateBankInformation("1", new BankInformation());
        //    Assert.Equal("/{applicationNumber}/bankinformation", Request.Resource);
        //    Assert.Equal(Method.PUT, Request.Method);
        //}


        //[Fact]
        //public async Task Client_LinkEmailInformation_Application()
        //{

        //    MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                  true));

        //    await ApplicationServiceClient.UpdateEmailInformation("1", new EmailAddress());
        //    Assert.Equal("/{applicationNumber}/emailinforamtion", Request.Resource);
        //    Assert.Equal(Method.PUT, Request.Method);
        //}

        //[Fact]
        //public async Task Client_LinkSetSelfDeclaredExpense_Application()
        //{
        //    MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                  true));

        //    await ApplicationServiceClient.UpdateSelfDeclaredExpense("1", new SelfDeclareExpense());
        //    Assert.Equal("/{applicationNumber}/declaredexpense", Request.Resource);
        //    Assert.Equal(Method.PUT, Request.Method);
        //}

        //[Fact]
        //public async Task Client_LinkEmploymentDetail_Application()
        //{
        //    MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                  true));

        //    await ApplicationServiceClient.UpdateEmploymentDetail("1", new EmploymentDetail());
        //    Assert.Equal("/{applicationNumber}/employmentinformation", Request.Resource);
        //    Assert.Equal(Method.PUT, Request.Method);
        //}

        //[Fact]
        //public async Task Client_SetOffers_Application()
        //{
        //    MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                  true));

        //    await ApplicationServiceClient.UpdateOffers("1", "initialoffer", new List<ILoanOffer>());
        //    Assert.Equal("{applicationNumber}/{offerType}/setoffers", Request.Resource);
        //    Assert.Equal(Method.PUT, Request.Method);
        //}

        //[Fact]
        //public async Task Client_SetSelectedOffer_Application()
        //{
        //    MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                  true));

        //    await ApplicationServiceClient.SetSelectedOffer("1", "f01", "initialoffer");
        //    Assert.Equal("/{applicationNumber}/{offerId}/{offerType}/setselectedoffer", Request.Resource);
        //    Assert.Equal(Method.PUT, Request.Method);
        //}

        //[Fact]
        //public async Task Client_getApplicationOffers_Application()
        //{
        //    MockServiceClient.Setup(s => s.ExecuteAsync<List<ILoanOffer>>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .Returns(
        //             () =>
        //               Task.FromResult(
        //                    new List<ILoanOffer>()
        //                   ));

        //    await ApplicationServiceClient.GetOffers("1", "initialoffer");
        //    Assert.Equal("/{applicationNumber}/{offerType}/getoffers", Request.Resource);
        //    Assert.Equal(Method.GET, Request.Method);
        //}
        #endregion
    }
}
