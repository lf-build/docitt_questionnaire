﻿using LendFoundry.Foundation.Date;
using Docitt.Questionnaire.Mocks;
using Docitt.Questionnaire.Tests.Fakes;
using Docitt.Questionnaire.Api.Controllers;
using Xunit;
using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;
using Docitt.Applicant;

namespace Docitt.Questionnaire.Api.Tests
{
    public class QuestionnaireControllerTest
    {
        #region Public  Methods

        [Fact]
        public void GetQuestionnaireReturnOk()
        {
            /*IQuestionnaire questionRequest = new Questionnaire();
            questionRequest.ApplicantId = "12345";
            questionRequest.SectionId = 1;
            questionRequest.TemporaryApplicationNumber = "1000";
            questionRequest.QuestionInfo = new Question();
            questionRequest.QuestionInfo.Answer = "John";
            questionRequest.QuestionInfo.QuestionId = "2";
            questionRequest.QuestionInfo.QuestionType = QuestionType.Null;
            string sJSON = JSON.Serialize(questionRequest);
            sJSON = sJSON;
            */
            //Logic to create JSON based on the classes. This JSON will be used in MongoDB.

            ChildConditionalQuestion childquestions = new ChildConditionalQuestion();
            childquestions.Answer = "Yes";
            childquestions.QuestionId = "qLASTNAME";

            List<IChildConditionalQuestion> cqs = new List<IChildConditionalQuestion>();
            cqs.Add(childquestions);

            Question q = new Question();
            q.QuestionId = "qFULLNAME";
            q.QuestionType = QuestionType.Text;
            q.Required = "False";
            q.SeqNo = 1;
            q.QuestionText = "Full Name";
            q.ChildQuestions = cqs;

            Question q2 = new Question();
            q2.QuestionId = "qLASTNAME";
            q2.QuestionType = QuestionType.Text;
            q2.Required = "False";
            q2.SeqNo = 2;
            q2.QuestionText = "Last Name";

            AvailableAns ans = new AvailableAns();
            ans.Answer = "Purchase";
            ans.Icon= "Purchase.jpg";

            AvailableAns ans1 = new AvailableAns();
            ans1.Answer = "Refinance";
            ans1.Icon = "Refinance.jpg";


            Question q3 = new Question();
            q3.QuestionId = "3";
            q3.QuestionType = QuestionType.SelectIcon;
            q3.Required = "False";
            q3.SeqNo = 3;
            q3.AvailableAns = new List<IAvailableAns>();
            q3.AvailableAns.Add(ans);
            q3.AvailableAns.Add(ans1);
            q3.QuestionText = "Purpose of Loan";

            Form objForm = new Form();
            objForm.FormId = "1";
            objForm.FormName = "Pre Qualifying Questions";
            Section s = new Section();
            s.SectionName = "Qualifying details";
            //a.Sections.Add(s);
            List<ISection> ss = new List<ISection>();
            ss.Add(s);
            objForm.Sections = ss;
            objForm.Sections[0].SectionId = 1;
            objForm.Sections[0].SeqNo = 1;

            List<IQuestion> questions = new List<IQuestion>();
            questions.Add(q);
            questions.Add(q2);
            questions.Add(q3);

            Questionnaire questionaire = new Questionnaire();
            //questionaire.QuestionnaireId = 1;
            questionaire.ApplicationForm = objForm; // new Form();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = questions;
            questionaire.Id = "1";
            questionaire.TemporaryApplicationNumber = "1";
            questionaire.Id = "1";
            questionaire.TenantId = "my-tenant";
            questionaire.ApplicantId = "12345";
            //string sJSON = JSON.Serialize(questionaire);

            var dummyQuestionnaire = new Questionnaire();
            //dummyQuestionnaire.QuestionnaireId = 1;
            dummyQuestionnaire.ApplicationForm = objForm; // new Form();
            //Temporary commented dummyQuestionnaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented dummyQuestionnaire.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TemporaryApplicationNumber = "1";
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TenantId = "my-tenant";
            dummyQuestionnaire.ApplicantId = "1";

            var dummyQuestionnaires = new List<IQuestionnaire>();
            dummyQuestionnaires.Add(dummyQuestionnaire);

            var dummyQuestionnaire2 = new Questionnaire();
            //dummyQuestionnaire2.QuestionnaireId = 2;
            dummyQuestionnaire2.ApplicationForm = objForm; // new Form();
            //Temporary commented dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire2.Id = "2";
            dummyQuestionnaire2.TemporaryApplicationNumber = "1000";
            dummyQuestionnaire2.Id = "1";
            dummyQuestionnaire2.TenantId = "my-tenant";
            dummyQuestionnaire2.ApplicantId = "1000";
            dummyQuestionnaires.Add(dummyQuestionnaire2);

            var questionnaireController = GetController(dummyQuestionnaires);

            var response = questionnaireController.GetQuestionnaire("1000");
            var result = Assert.IsType<HttpOkObjectResult>(response); //(questionnaireController.GetQuestionnaire("1000"));

            response = response;
/*
            var response = questionnaireController.GetQuestionnaire("1000");

            /////
            var result = Assert.IsType<HttpOkObjectResult>(response); //(questionnaireController.GetQuestionnaire("1000"));
            var questionnaire = (Task<IQuestionnaireResponse>)result.Value;

            /////

            Assert.IsType<HttpOkObjectResult>(response);
            //var questionnaire = ((HttpOkObjectResult)response).Value as IQuestionnaireResponse;

            Assert.NotNull(questionnaire);

            Assert.NotNull(questionnaire.Result.TemporaryApplicationNumber);
            Assert.Equal("1000", questionnaire.Result.TemporaryApplicationNumber);
            */
        }


        private List<IQuestionnaire> GetData()
        {
            ChildConditionalQuestion childquestions = new ChildConditionalQuestion();
            childquestions.Answer = "Yes";
            childquestions.QuestionId = "qLASTNAME";

            List<IChildConditionalQuestion> cqs = new List<IChildConditionalQuestion>();
            cqs.Add(childquestions);

            Question q = new Question();
            q.QuestionId = "qFULLNAME";
            q.QuestionType = QuestionType.Text;
            q.Required = "False";
            q.SeqNo = 1;
            q.QuestionText = "Full Name";
            q.ChildQuestions = cqs;

            Question q2 = new Question();
            q2.QuestionId = "qLASTNAME";
            q2.QuestionType = QuestionType.Text;
            q2.Required = "False";
            q2.SeqNo = 2;
            q2.QuestionText = "Last Name";

            AvailableAns ans = new AvailableAns();
            ans.Answer = "Purchase";
            ans.Icon = "Purchase.jpg";

            AvailableAns ans1 = new AvailableAns();
            ans1.Answer = "Refinance";
            ans1.Icon = "Refinance.jpg";


            Question q3 = new Question();
            q3.QuestionId = "3";
            q3.QuestionType = QuestionType.SelectIcon;
            q3.Required = "False";
            q3.SeqNo = 3;
            q3.AvailableAns = new List<IAvailableAns>();
            q3.AvailableAns.Add(ans);
            q3.AvailableAns.Add(ans1);
            q3.QuestionText = "Purpose of Loan";

            Form objForm = new Form();
            objForm.FormId = "1";
            objForm.FormName = "Pre Qualifying Questions";
            Section s = new Section();
            s.SectionName = "Qualifying details";
            //a.Sections.Add(s);
            List<ISection> ss = new List<ISection>();
            ss.Add(s);
            objForm.Sections = ss;
            objForm.Sections[0].SectionId = 1;
            objForm.Sections[0].SeqNo = 1;

            List<IQuestion> questions = new List<IQuestion>();
            questions.Add(q);
            questions.Add(q2);
            questions.Add(q3);

            Questionnaire questionaire = new Questionnaire();
            //questionaire.QuestionnaireId = 1;
            questionaire.ApplicationForm = objForm; // new Form();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = questions;
            questionaire.Id = "1";
            questionaire.TemporaryApplicationNumber = "1";
            questionaire.Id = "1";
            questionaire.TenantId = "my-tenant";
            questionaire.ApplicantId = "12345";
            //string sJSON = JSON.Serialize(questionaire);

            var dummyQuestionnaire = new Questionnaire();
            //dummyQuestionnaire.QuestionnaireId = 1;
            dummyQuestionnaire.ApplicationForm = objForm; // new Form();
            //Temporary commented dummyQuestionnaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented dummyQuestionnaire.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TemporaryApplicationNumber = "1";
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TenantId = "my-tenant";
            dummyQuestionnaire.ApplicantId = "1";

            var dummyQuestionnaires = new List<IQuestionnaire>();
            dummyQuestionnaires.Add(dummyQuestionnaire);

            var dummyQuestionnaire2 = new Questionnaire();
            //dummyQuestionnaire2.QuestionnaireId = 2;
            dummyQuestionnaire2.ApplicationForm = objForm; // new Form();
            //Temporary commented dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire2.Id = "2";
            dummyQuestionnaire2.TemporaryApplicationNumber = "1000";
            dummyQuestionnaire2.Id = "1";
            dummyQuestionnaire2.TenantId = "my-tenant";
            dummyQuestionnaire2.ApplicantId = "1000";
            dummyQuestionnaires.Add(dummyQuestionnaire2);
            return dummyQuestionnaires;   
        }


        [Fact]
        public void GetQuestionnaireReturnQuestionnaireNotFoundException()
        {
            var dummyQuestionnaires = GetData();

            var questionnaireController = GetController(dummyQuestionnaires);
            Assert.ThrowsAsync<FormDoesNotExistsException>(() => questionnaireController.GetQuestionnaire("2000"));           
        }

        //[Fact]
        //public void GetCreateFormTemplateReturnOK()
        //{
        //    var dummyForms = GetForms();
        //    Form form = (Form) GetForm("1", "Form1");
        //    var questionnaireController = GetController(dummyForms);
        //    var response = questionnaireController.CreateTemplateForm(form, "Form1");

        //    var s = "";
        //    /////
        //    var result = Assert.IsType<HttpOkObjectResult>(response); //(questionnaireController.GetQuestionnaire("1000"));
        //    var questionnaire = (Task<IQuestionnaireResponse>)result.Value;
        //    /////

        //    Assert.IsType<HttpOkObjectResult>(response);
        //    //var questionnaire = ((HttpOkObjectResult)response).Value as IQuestionnaireResponse;

        //    Assert.NotNull(questionnaire);

        //    Assert.NotNull(questionnaire.Result.TemporaryApplicationNumber);
        //    Assert.Equal("1000", questionnaire.Result.TemporaryApplicationNumber);
        //}

        private List<IForm> GetForms()
        {
            var dummyForms = new List<IForm>();
            dummyForms.Add(GetForm("1", "Form 1"));
            dummyForms.Add(GetForm("2", "Form 2"));
            dummyForms.Add(GetForm("3", "Form 3"));
            dummyForms.Add(GetForm("4", "Form 4"));

            return dummyForms;
        }

        private IForm GetForm(string FormId, string FormName)
        {
            //This function creates a Form with 1 section having 4 questions.
            ChildConditionalQuestion childquestions = new ChildConditionalQuestion();
            childquestions.Answer = "Yes";
            childquestions.QuestionId = "5";

            List<IChildConditionalQuestion> childQuestions = new List<IChildConditionalQuestion>();
            childQuestions.Add(childquestions);

            Question q = new Question();
            q.QuestionId = "1";
            q.QuestionType = QuestionType.Text;
            q.Required = "False";
            q.SeqNo = 1;
            q.QuestionText = "Full Name";
            q.ChildQuestions = childQuestions;

            Question q2 = new Question();
            q2.QuestionId = "2";
            q2.QuestionType = QuestionType.Text;
            q2.Required = "False";
            q2.SeqNo = 2;
            q2.QuestionText = "Last Name";

            AvailableAns ans = new AvailableAns();
            ans.Answer = "Purchase";
            ans.Icon = "Purchase.jpg";

            AvailableAns ans1 = new AvailableAns();
            ans1.Answer = "Refinance";
            ans1.Icon = "Refinance.jpg";


            Question q3 = new Question();
            q3.QuestionId = "3";
            q3.QuestionType = QuestionType.SelectIcon;
            q3.Required = "False";
            q3.SeqNo = 3;
            q3.AvailableAns = new List<IAvailableAns>();
            q3.AvailableAns.Add(ans);
            q3.AvailableAns.Add(ans1);
            q3.QuestionText = "Purpose of Loan";

            Question q4 = new Question();
            q4.QuestionId = "4";
            q4.QuestionType = QuestionType.TextEmail;
            q4.Required = "False";
            q4.SeqNo = 4;
            q4.AvailableAns = null;
            q4.QuestionText = "Email";

            Question q5 = new Question();
            q5.QuestionId = "5";
            q5.QuestionType = QuestionType.TextCurr;
            q5.Required = "False";
            q5.SeqNo = 5;
            q5.AvailableAns = null;
            q5.QuestionText = "Amount";

            Form objForm = new Form();
            objForm.FormId = FormId;
            objForm.FormName = FormName;

            Section s = new Section();
            s.SectionName = "Qualifying details";

            List<ISection> ss = new List<ISection>();
            ss.Add(s);
            objForm.Sections = ss;
            objForm.Sections[0].SectionId = 1;
            objForm.Sections[0].SeqNo = 1;

            List<IQuestion> questions = new List<IQuestion>();
            questions.Add(q);
            questions.Add(q2);
            questions.Add(q3);
            questions.Add(q4);
            questions.Add(q5);

            Questionnaire questionaire = new Questionnaire();
            //questionaire.QuestionnaireId = 1;
            questionaire.ApplicationForm = objForm; // new Form();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = questions;
            questionaire.Id = "1";
            questionaire.TemporaryApplicationNumber = "1";
            questionaire.Id = "1";
            questionaire.TenantId = "my-tenant";
            questionaire.ApplicantId = "12345";

            //string sJSON = JSON.Serialize(questionaire);
            /*var dummyQuestionnaire = new Questionnaire();
            dummyQuestionnaire.QuestionnaireId = 1;
            dummyQuestionnaire.ApplicationForm = objForm; // new Form();
            dummyQuestionnaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            dummyQuestionnaire.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TemporaryApplicationNumber = "1";
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TenantId = "my-tenant";
            dummyQuestionnaire.ApplicantId = "1";

            var dummyForms = new List<IForm>();
            dummyForms.Add(dummyForm);

            var dummyQuestionnaire2 = new Questionnaire();
            dummyQuestionnaire2.QuestionnaireId = 2;
            dummyQuestionnaire2.ApplicationForm = objForm; // new Form();
            dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire2.Id = "2";
            dummyQuestionnaire2.TemporaryApplicationNumber = "1000";
            dummyQuestionnaire2.Id = "1";
            dummyQuestionnaire2.TenantId = "my-tenant";
            dummyQuestionnaire2.ApplicantId = "1000";
            dummyQuestionnaires.Add(dummyQuestionnaire2);
            */
            return questionaire.ApplicationForm;
        }

        #endregion

        #region Private Methods
        private static QuestionnaireController GetController(List<IQuestionnaire> questionnaires = null)
        {
            return null;
            //var questionnaireService = new FakeQuestionnaireService(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>());
            //var questionnaireRepository = new FakeQuestionnaireRepository(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>());
            //return new QuestionnaireController(GetQuestionnaireService(questionnaireRepository, questionnaires));
        }

        private static QuestionnaireController GetController(List<IForm> forms = null)
        {
            return null;
            //var questionnaireService = new FakeQuestionnaireService(new UtcTenantTime(), forms ?? new List<IForm>());
            //var questionnaireRepository = new FakeFormRepository(new UtcTenantTime(), forms ?? new List<IForm>());
            //return new QuestionnaireController(GetQuestionnaireService(questionnaireRepository, forms));
        }

        private static QuestionnaireController GetController(FakeQuestionnaireRepository questionnaireRepository, IEnumerable<IQuestionnaire> applications = null)
        {
            return null;
            //return new QuestionnaireController(GetQuestionnaireService(questionnaireRepository, applications));
        }
        private static QuestionnaireController GetController(IQuestionnaireService applicationService, IApplicantService applicantService)
        {
            return new QuestionnaireController(applicationService);
        }

        private static IQuestionnaireService GetQuestionnaireService(IEnumerable<IQuestionnaire> questionnaires = null)
        {
            return null;
            //return new FakeQuestionnaireService(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>());
        }

        private static IQuestionnaireService GetQuestionnaireService(IEnumerable<IForm> forms = null)
        {
            return null;
            //return new FakeQuestionnaireService(new UtcTenantTime(), forms ?? new List<IForm>());
        }

        private static IQuestionnaireService GetQuestionnaireService(IQuestionnaireRepository questionnaireRepository, IEnumerable<IQuestionnaire> questionnaires = null, IEnumerable<IForm> forms = null)
        {
            return null;
            //return new QuestionnaireService(new FakeQuestionnaireRepository(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>()),
            //    Mock.Of<IGeneratorService>(),
            //    Mock.Of<ILogger>(),
            //    Mock.Of<IEventHubClient>(),
            //    new QuestionnaireConfiguration()  { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, /*Mock.Of<IEntityStatusService>(),*/ Mock.Of<ITenantTime>(), new FakeFormRepository(new UtcTenantTime(), forms ?? new List<IForm>()), Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>());
        }

        private static IQuestionnaireService GetQuestionnaireService(IFormRepository formRepository, IEnumerable<IForm> forms = null)
        {
            return null;
            //return new QuestionnaireService(new FakeQuestionnaireRepository(new UtcTenantTime(), forms ?? new List<IForm>()),
            //    Mock.Of<IGeneratorService>(),
            //    Mock.Of<ILogger>(),
            //    Mock.Of<IEventHubClient>(),
            //    new QuestionnaireConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, /*Mock.Of<IEntityStatusService>(),*/ Mock.Of<ITenantTime>(), new FakeFormRepository(new UtcTenantTime(), forms ?? new List<IForm>()), Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>());
        }

        //private static IQuestionnaireService GetQuestionnaireService(IApplicantService applicantService,IQuestionnaireRepository applicationRepository ,List<IQuestionnaire> applications = null)
        //{
        //    return new ApplicationService(applicantService,
        //        applicationRepository, new FakeApplicationNumberGenerator(),
        //        Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new ApplicationConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<IEntityStatusService>(), Mock.Of<ITenantTime>());
        //}
        #endregion
    }
}
