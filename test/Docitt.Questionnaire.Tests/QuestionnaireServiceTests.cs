﻿using System;
using System.Collections.Generic;
using Moq;
using Xunit;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Docitt.Questionnaire.Mocks;
using System.Threading.Tasks;
using Docitt.Questionnaire.Tests.Fakes;

namespace Docitt.Questionnaire.Tests
{
    public class QuestionnaireServiceTests
    {
        #region Public Methods

        /// <summary>
        /// This function populates the object with the data. It creates collection of questionnaire.
        /// </summary>
        /// <returns></returns>
        private List<IQuestionnaire> GetData()
        {
            //This function creates a Questionnaire with 1 section having 4 questions.
            ChildConditionalQuestion childquestions = new ChildConditionalQuestion();
            childquestions.Answer = "Yes";
            childquestions.QuestionId = "5";

            List<IChildConditionalQuestion> cqs = new List<IChildConditionalQuestion>();
            cqs.Add(childquestions);

            Question q = new Question();
            q.QuestionId = "1";
            q.QuestionType = QuestionType.Text;
            q.Required = "False";
            q.SeqNo = 1;
            q.QuestionText = "Full Name";
            q.ChildQuestions = cqs;

            Question q2 = new Question();
            q2.QuestionId = "2";
            q2.QuestionType = QuestionType.Text;
            q2.Required = "False";
            q2.SeqNo = 2;
            q2.QuestionText = "Last Name";

            AvailableAns ans = new AvailableAns();
            ans.Answer = "Purchase";
            ans.Icon = "Purchase.jpg";

            AvailableAns ans1 = new AvailableAns();
            ans1.Answer = "Refinance";
            ans1.Icon = "Refinance.jpg";


            Question q3 = new Question();
            q3.QuestionId = "3";
            q3.QuestionType = QuestionType.SelectIcon;
            q3.Required = "False";
            q3.SeqNo = 3;
            q3.AvailableAns = new List<IAvailableAns>();
            q3.AvailableAns.Add(ans);
            q3.AvailableAns.Add(ans1);
            q3.QuestionText = "Purpose of Loan";

            Question q4 = new Question();
            q4.QuestionId = "4";
            q4.QuestionType = QuestionType.TextEmail;
            q4.Required = "False";
            q4.SeqNo = 4;
            q4.AvailableAns = null;
            q4.QuestionText = "Email";

            Question q5 = new Question();
            q5.QuestionId = "5";
            q5.QuestionType = QuestionType.TextCurr;
            q5.Required = "False";
            q5.SeqNo = 5;
            q5.AvailableAns = null;
            q5.QuestionText = "Amount";


            Form objForm = new Form();
            objForm.FormId = "1";
            objForm.FormName = "Pre Qualifying Questions";
            Section s = new Section();
            s.SectionName = "Qualifying details";
            //a.Sections.Add(s);
            List<ISection> ss = new List<ISection>();
            ss.Add(s);
            objForm.Sections = ss;
            objForm.Sections[0].SectionId = 1;
            objForm.Sections[0].SeqNo = 1;

            List<IQuestion> questions = new List<IQuestion>();
            questions.Add(q);
            questions.Add(q2);
            questions.Add(q3);
            questions.Add(q4);
            questions.Add(q5);

            Questionnaire questionaire = new Questionnaire();
            //questionaire.QuestionnaireId = 1;
            questionaire.ApplicationForm = objForm; // new Form();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = questions;
            questionaire.Id = "1";
            questionaire.TemporaryApplicationNumber = "1";
            questionaire.Id = "1";
            questionaire.TenantId = "my-tenant";
            questionaire.ApplicantId = "12345";
            //string sJSON = JSON.Serialize(questionaire);

            var dummyQuestionnaire = new Questionnaire();
            //dummyQuestionnaire.QuestionnaireId = 1;
            dummyQuestionnaire.ApplicationForm = objForm; // new Form();
            //Temporary commented dummyQuestionnaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented dummyQuestionnaire.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TemporaryApplicationNumber = "1";
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TenantId = "my-tenant";
            dummyQuestionnaire.ApplicantId = "1";

            var dummyQuestionnaires = new List<IQuestionnaire>();
            dummyQuestionnaires.Add(dummyQuestionnaire);

            var dummyQuestionnaire2 = new Questionnaire();
            //dummyQuestionnaire2.QuestionnaireId = 2;
            dummyQuestionnaire2.ApplicationForm = objForm; // new Form();
            //Temporary commented dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire2.Id = "2";
            dummyQuestionnaire2.TemporaryApplicationNumber = "1000";
            dummyQuestionnaire2.Id = "1";
            dummyQuestionnaire2.TenantId = "my-tenant";
            dummyQuestionnaire2.ApplicantId = "1000";
            dummyQuestionnaires.Add(dummyQuestionnaire2);
            return dummyQuestionnaires;
        }

        private List<IForm> GetForms()
        {
            var dummyForms = new List<IForm>();
            dummyForms.Add(GetForm("1", "Form 1"));
            dummyForms.Add(GetForm("2", "Form 2"));
            dummyForms.Add(GetForm("3", "Form 3"));
            dummyForms.Add(GetForm("4", "Form 4"));

            return dummyForms;
        }

        private IForm GetForm(string FormId, string FormName)
        {
            //This function creates a Form with 1 section having 4 questions.
            ChildConditionalQuestion childquestions = new ChildConditionalQuestion();
            childquestions.Answer = "Yes";
            childquestions.QuestionId = "5";

            List<IChildConditionalQuestion> childQuestions = new List<IChildConditionalQuestion>();
            childQuestions.Add(childquestions);

            Question q = new Question();
            q.QuestionId = "1";
            q.QuestionType = QuestionType.Text;
            q.Required = "False";
            q.SeqNo = 1;
            q.QuestionText = "Full Name";
            q.ChildQuestions = childQuestions;

            Question q2 = new Question();
            q2.QuestionId = "2";
            q2.QuestionType = QuestionType.Text;
            q2.Required = "False";
            q2.SeqNo = 2;
            q2.QuestionText = "Last Name";

            AvailableAns ans = new AvailableAns();
            ans.Answer = "Purchase";
            ans.Icon = "Purchase.jpg";

            AvailableAns ans1 = new AvailableAns();
            ans1.Answer = "Refinance";
            ans1.Icon = "Refinance.jpg";


            Question q3 = new Question();
            q3.QuestionId = "3";
            q3.QuestionType = QuestionType.SelectIcon;
            q3.Required = "False";
            q3.SeqNo = 3;
            q3.AvailableAns = new List<IAvailableAns>();
            q3.AvailableAns.Add(ans);
            q3.AvailableAns.Add(ans1);
            q3.QuestionText = "Purpose of Loan";

            Question q4 = new Question();
            q4.QuestionId = "4";
            q4.QuestionType = QuestionType.TextEmail;
            q4.Required = "False";
            q4.SeqNo = 4;
            q4.AvailableAns = null;
            q4.QuestionText = "Email";

            Question q5 = new Question();
            q5.QuestionId = "5";
            q5.QuestionType = QuestionType.TextCurr;
            q5.Required = "False";
            q5.SeqNo = 5;
            q5.AvailableAns = null;
            q5.QuestionText = "Amount";

            Form objForm = new Form();
            objForm.FormId = FormId;
            objForm.FormName = FormName;

            Section s = new Section();
            s.SectionName = "Qualifying details";

            List<ISection> ss = new List<ISection>();
            ss.Add(s);
            objForm.Sections = ss;
            objForm.Sections[0].SectionId = 1;
            objForm.Sections[0].SeqNo = 1;

            List<IQuestion> questions = new List<IQuestion>();
            questions.Add(q);
            questions.Add(q2);
            questions.Add(q3);
            questions.Add(q4);
            questions.Add(q5);

            Questionnaire questionaire = new Questionnaire();
            //questionaire.QuestionnaireId = 1;
            questionaire.ApplicationForm = objForm; // new Form();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            //Temporary commented questionaire.ApplicationForm.Sections[0].Questions = questions;
            questionaire.Id = "1";
            questionaire.TemporaryApplicationNumber = "1";
            questionaire.Id = "1";
            questionaire.TenantId = "my-tenant";
            questionaire.ApplicantId = "12345";

            //string sJSON = JSON.Serialize(questionaire);
            /*var dummyQuestionnaire = new Questionnaire();
            dummyQuestionnaire.QuestionnaireId = 1;
            dummyQuestionnaire.ApplicationForm = objForm; // new Form();
            dummyQuestionnaire.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            dummyQuestionnaire.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TemporaryApplicationNumber = "1";
            dummyQuestionnaire.Id = "1";
            dummyQuestionnaire.TenantId = "my-tenant";
            dummyQuestionnaire.ApplicantId = "1";

            var dummyForms = new List<IForm>();
            dummyForms.Add(dummyForm);

            var dummyQuestionnaire2 = new Questionnaire();
            dummyQuestionnaire2.QuestionnaireId = 2;
            dummyQuestionnaire2.ApplicationForm = objForm; // new Form();
            dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = new List<IQuestion>();
            dummyQuestionnaire2.ApplicationForm.Sections[0].Questions = questions;
            dummyQuestionnaire2.Id = "2";
            dummyQuestionnaire2.TemporaryApplicationNumber = "1000";
            dummyQuestionnaire2.Id = "1";
            dummyQuestionnaire2.TenantId = "my-tenant";
            dummyQuestionnaire2.ApplicantId = "1000";
            dummyQuestionnaires.Add(dummyQuestionnaire2);
            */
            return questionaire.ApplicationForm;
        }

        private IQuestionRequest GetQuestionRequest()
        {
            QuestionRequest questionRequest = new QuestionRequest();
            questionRequest.ApplicantId = "1000";
            questionRequest.QuestionInfo = new Question();
            questionRequest.QuestionInfo.QuestionId = "1";
            questionRequest.QuestionInfo.Answer = "John";
            questionRequest.QuestionInfo.QuestionType = QuestionType.Text;
            questionRequest.SectionId = 1;
            questionRequest.TemporaryApplicationNumber = "1000";

            return questionRequest;
        }

        private IQuestionRequest GetInvalidEmailQuestionRequest()
        {
            QuestionRequest questionRequest = new QuestionRequest();
            questionRequest.ApplicantId = "1000";
            questionRequest.QuestionInfo = new Question();
            questionRequest.QuestionInfo.QuestionId = "4";
            questionRequest.QuestionInfo.Answer = "email.com";
            questionRequest.QuestionInfo.QuestionType = QuestionType.Text;
            questionRequest.SectionId = 1;
            questionRequest.TemporaryApplicationNumber = "1000";

            return questionRequest;
        }

        private IQuestionRequest GetInvalidAmountQuestionRequest()
        {
            QuestionRequest questionRequest = new QuestionRequest();
            questionRequest.ApplicantId = "1000";
            questionRequest.QuestionInfo = new Question();
            questionRequest.QuestionInfo.QuestionId = "5";
            questionRequest.QuestionInfo.Answer = "Non Numeric";
            questionRequest.SectionId = 1;
            questionRequest.TemporaryApplicationNumber = "1000";

            return questionRequest;
        }


        /// <summary>
        /// This function will throw an exception because the data is invalid (email address is 'email.com' which is not a valid input)
        /// </summary>
        //[Fact]
        //public async void QuestionnaireService_Update_Question_ThrowsWhenEmailIsInvalid()
        //{
        //    List<IQuestionnaire> questionnaires = new List<IQuestionnaire>();

        //    questionnaires = GetData();
        //    var fakeQuestionnaireService = GetQuestionnaireService(GetData());

        //    var fakeQuestionnaireRepository = GetFakeQuestionnaireRepository(questionnaires);
        //    //var fakeFormRepository = GetFakeFormRepository(forms);

        //    var service = GetQuestionnaireService(fakeQuestionnaireService, fakeQuestionnaireRepository);

        //    var questionRequest = new QuestionRequest();
        //    var questionToUpdate = GetInvalidEmailQuestionRequest();
        //    //var result = await service.UpdateQuestion(questionToUpdate, "1000");
        //    //Assert.Equal(true, result);
        //    //Assert.Equal("John", questionToUpdate.QuestionInfo.Answer);

        //    await Assert.ThrowsAsync<ArgumentException>(async () => await service.UpdateQuestion(questionToUpdate, "1000"));
        //}

        /// <summary>
        /// This function will throw an exception because the data is invalid (amount field is a non numeric value which is not a valid input)
        /// </summary>
        //[Fact]
        //public async void QuestionnaireService_Update_Question_ThrowsWhenAmountValueIsInvalid()
        //{
        //    List<IQuestionnaire> questionnaires = new List<IQuestionnaire>();

        //    questionnaires = GetData();
        //    var fakeQuestionnaireService = GetQuestionnaireService(GetData());

        //    var fakeQuestionnaireRepository = GetFakeQuestionnaireRepository(questionnaires);
        //    var service = GetQuestionnaireService(fakeQuestionnaireService, fakeQuestionnaireRepository);

        //    var questionRequest = new QuestionRequest();
        //    var questionToUpdate = GetInvalidAmountQuestionRequest();


        //    await Assert.ThrowsAsync<ArgumentException>(async () => await service.UpdateQuestion(questionToUpdate, "1000"));
        //}

        //[Fact]
        //public async Task QuestionnaireService_Update_Question()
        //{
        //    List<IQuestionnaire> questionnaires = new List<IQuestionnaire>();

        //    questionnaires = GetData();
        //    var fakeQuestionnaireService = GetQuestionnaireService(GetData());

        //    var fakeQuestionnaireRepository = GetFakeQuestionnaireRepository(questionnaires);
        //    var service = GetQuestionnaireService(fakeQuestionnaireService, fakeQuestionnaireRepository);

        //    var questionRequest = new QuestionRequest();
        //    var questionToUpdate = GetQuestionRequest();
        //    bool result = false;// await service.UpdateQuestion(questionToUpdate, "1000");
        //    Assert.Equal(true, result);
        //    Assert.Equal("John", questionToUpdate.QuestionInfo.Answer);

        //}

        //[Fact]
        //public async Task QuestionnaireService_CreateFormTemplate()
        //{
        //    List<IForm> forms = new List<IForm>();
        //    forms = GetForms();

        //    List<IQuestionnaire> questionnaires = new List<IQuestionnaire>();
        //    questionnaires = GetData();


        //    var fakeQuestionnaireService = GetQuestionnaireService(questionnaires, forms);

        //    var fakeFormRepository = GetFakeFormRepository(forms);
        //    var fakeQuestionnaireRepository = GetFakeQuestionnaireRepository(questionnaires);

        //    var service = GetQuestionnaireService(fakeQuestionnaireService, fakeQuestionnaireRepository, fakeFormRepository);

        //    var questionRequest = new QuestionRequest();
        //    var questionToUpdate = GetQuestionRequest();
        //    var result = await service.CreateFormTemplate(GetForm("Form1", "Form1"));
        //    Assert.Equal(true, result);

        //}

        #endregion

        #region Private Methods
        private static FakeQuestionnaireService GetApplicantService(IEnumerable<IQuestionnaire> questionnaires = null)
        {
            return new FakeQuestionnaireService(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>());
        }

        private static FakeQuestionnaireRepository GetFakeQuestionnaireRepository(List<IQuestionnaire> questionnaires = null)
        {
            return new FakeQuestionnaireRepository(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>());
        }

        private static FakeFormRepository GetFakeFormRepository(List<IForm> forms = null)
        {
            return new FakeFormRepository(new UtcTenantTime(), forms ?? new List<IForm>());
        }

        private static IQuestionnaireService GetQuestionnaireService(IQuestionnaireService questionnaireService, IQuestionnaireRepository fakeQuestionnaireRepository, IFormRepository fakeFormRepository)
        {
            return null;
            //return new QuestionnaireService(fakeQuestionnaireRepository, Mock.Of<IGeneratorService>(), Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new QuestionnaireConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>(), fakeFormRepository, Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>());
        }

        private static IQuestionnaireService GetQuestionnaireService(IQuestionnaireService questionnaireService, IQuestionnaireRepository fakeQuestionnaireRepository)
        {
            return null;
            //return new QuestionnaireService(fakeQuestionnaireRepository, Mock.Of<IGeneratorService>(), Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new QuestionnaireConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>(), null, Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>());
        }

        private static IQuestionnaireService GetQuestionnaireService(List<IQuestionnaire> questionnaires = null, List<IForm> forms = null)
        {
            return null;
            //    return new QuestionnaireService(new FakeQuestionnaireRepository(new UtcTenantTime(), questionnaires ?? new List<IQuestionnaire>()), Mock.Of<IGeneratorService>(), Mock.Of<ILogger>(), Mock.Of<IEventHubClient>(), new QuestionnaireConfiguration() { ExpiryDays = 60, InitialLeadStatus = "200.01", InitialStatus = "200.02" }, Mock.Of<ITenantTime>(), new FakeFormRepository(new UtcTenantTime(), forms ?? new List<IForm>()), Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>());
        }


        #endregion
    }
}
