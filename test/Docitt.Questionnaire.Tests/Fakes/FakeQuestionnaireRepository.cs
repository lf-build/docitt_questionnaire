﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace Docitt.Questionnaire.Mocks
{
    public class FakeQuestionnaireRepository 
    {
        #region Constructors
        public FakeQuestionnaireRepository(ITenantTime tenantTime, IEnumerable<IQuestionnaire> questionnaires) : this(tenantTime)
        {
            Questionnaires.AddRange(questionnaires);
        }

        public FakeQuestionnaireRepository(ITenantTime tenantTime, IEnumerable<IForm> forms) : this(tenantTime)
        {
            Forms.AddRange(forms);
        }

        public FakeQuestionnaireRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }
        #endregion

        #region Private Properties
        public ITenantTime TenantTime { get; set; }
        public List<IQuestionnaire> Questionnaires { get; set; } = new List<IQuestionnaire>();
        public List<IForm> Forms { get; set; } = new List<IForm>();
        #endregion

        #region Public Methods
        public Task<IQuestionnaire> Get(string id)
        {
            return Task.FromResult(Questionnaires.FirstOrDefault(q => q.TemporaryApplicationNumber.Equals(id)));
        }

        public IQuestion GetByQuestionId(string questionId)
        {
            throw new NotImplementedException();
        }

        public IQuestion UpdateQuestion(IQuestionRequest questionRequest)
        {
            var Sections = new List<ISection>();
            var questionToUpdate = new Question();
            IQuestionnaire questionnaire = new Questionnaire();
            ISection sectionFound = new Section();

            //Get Questionnaire for the applicant
            questionnaire = GetQuestionnaire(questionRequest.TemporaryApplicationNumber);//GetByApplicationIdFilter(questionRequest.ApplicantId);

            if (questionnaire != null)
            {
                //Get the section that needs to be updated.
                //sectionFound = questionnaire.ApplicationForm.Sections.Find(section);
                Sections = questionnaire.ApplicationForm.Sections;
                var sectiontoUpdate = Sections.FirstOrDefault(Section => Section.SectionId == questionRequest.SectionId);

                //Temporary commented questionToUpdate = (Question)sectiontoUpdate.Questions.FirstOrDefault(question => question.QuestionId == questionRequest.QuestionInfo.QuestionId);
                questionToUpdate = null;
                if (questionToUpdate == null)
                {
                    throw new NotFoundException($"Question to update the answer for the applicant {questionRequest.ApplicantId} not found");
                }
            }
            else
            {
                throw new NotFoundException($"Questionnaire for the applicant {questionRequest.ApplicantId} not found");
            }

            var isAnswerValid = ValidateAnswer(questionToUpdate, questionRequest.QuestionInfo.Answer);

            if (isAnswerValid == false)
            {
                throw new ArgumentException($"The answer '{questionRequest.QuestionInfo.Answer}' is invalid for the question '{questionToUpdate.QuestionText}'");
            }
            //If the Questionnaire exist and the Question to update exists then update the question.

            //Update the answer
            questionToUpdate.Answer = questionRequest.QuestionInfo.Answer;

            //Save the last updated section.
            //questionnaire.LastAccessedSection = questionRequest.SectionId;

            //Update the section
            //questionnaire.ApplicationForm.Sections = Sections;
            /*
            Collection.UpdateOne
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == questionRequest.TemporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.ApplicationForm.Sections, Sections)
            );

            Collection.UpdateOne
            (
                Builders<IQuestionnaire>.Filter
                    .Where(a => a.TenantId == TenantService.Current.Id &&
                                a.TemporaryApplicationNumber == questionRequest.TemporaryApplicationNumber),
                Builders<IQuestionnaire>.Update.Set(a => a.LastAccessedSection, questionRequest.SectionId)
            );
            */
            IQuestionResponse questionResponse = new QuestionResponse(questionRequest);

            return questionToUpdate;
        }

        public IQuestion AddQuestion(IQuestion question)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAllQuestions()
        {
            throw new NotImplementedException();
        }

        public IQuestionnaire GetQuestionnaire(string tempApplicationNumber)
        {
            var questionnaire = Questionnaires.Where(q => q.TemporaryApplicationNumber == tempApplicationNumber).FirstOrDefault();

            if (questionnaire == null)
                throw new QuestionnaireNotFoundException($"Questionnaire having temporary application number {tempApplicationNumber} not found");

            return questionnaire;
        }

        public Task<IEnumerable<IQuestionnaire>> All(Expression<Func<IQuestionnaire, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IQuestionnaire item)
        {
            throw new NotImplementedException();
        }

        public void Remove(IQuestionnaire item)
        {
            throw new NotImplementedException();
        }

        public void Update(IQuestionnaire item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IQuestionnaire, bool>> query)
        {
            throw new NotImplementedException();
        }

        private bool ValidateAnswer( IQuestion questionToUpdate, string answer)
        {
            return true;
            ////Here answer will be validated.
            ////It will be validated agianst the Data type and the 'AvailableAns'

            ////Validate the Data Type
            //QuestionType answerDataType = questionToUpdate.QuestionType;
            //bool validDataType = false;
            //bool validAnswer = false;

            ////Validate the answer if it is a Date type
            //if (answerDataType == QuestionType.Date && ParseString(answer) == dataType.System_DateTime)
            //{
            //    validDataType = true;
            //    validAnswer = true;
            //}

            //if (answerDataType == QuestionType.TextCurr && ParseString(answer) == dataType.System_Int32)
            //{
            //    validDataType = true;
            //    validAnswer = true;
            //}

            //if (answerDataType == QuestionType.TextCurr && ParseString(answer) == dataType.System_Double)
            //{
            //    validDataType = true;
            //    validAnswer = true;
            //}

            //if (answerDataType == QuestionType.TextNum && ParseString(answer) == dataType.System_Int64)
            //{
            //    validDataType = true;
            //    validAnswer = true;
            //}

            //if (answerDataType == QuestionType.TextEmail && ParseString(answer) == dataType.System_Email)
            //{
            //    validDataType = true;
            //    validAnswer = true;
            //}


            ////Validate answer if it is a Select type
            //if (answerDataType == QuestionType.Select)
            //{
            //    // If the data type is "Select" then make sure the Answer given by the user should be in 'Available Ans'
            //    if (questionToUpdate.AvailableAns != null)
            //    {
            //        for (int i = 0; i <= questionToUpdate.AvailableAns.Count - 1; i++)
            //        {
            //            if (questionToUpdate.AvailableAns[i].Answer == answer)
            //            {
            //                validAnswer = true;
            //                validDataType = true;
            //                break;
            //            }
            //        }
            //    }
            //}

            //if (answerDataType == QuestionType.Text && ParseString(answer) == dataType.System_String)
            //{
            //    validDataType = true;
            //    validAnswer = true;
            //}

            //return validAnswer;
        }

        enum dataType
        {
            System_Boolean = 0,
            System_Int32 = 1,
            System_Int64 = 2,
            System_Double = 3,
            System_DateTime = 4,
            System_String = 5,
            System_Email = 6
        }

        private dataType ParseString(string str)
        {

            bool boolValue;
            Int32 intValue;
            Int64 bigintValue;
            double doubleValue;
            DateTime dateValue;

            // Place checks higher in if-else statement to give higher priority to type.

            if (IsValidEmailId(str))
                return dataType.System_Email;
            else if (bool.TryParse(str, out boolValue))
                return dataType.System_Boolean;
            else if (Int32.TryParse(str, out intValue))
                return dataType.System_Int32;
            else if (Int64.TryParse(str, out bigintValue))
                return dataType.System_Int64;
            else if (double.TryParse(str, out doubleValue))
                return dataType.System_Double;
            else if (DateTime.TryParse(str, out dateValue))
                return dataType.System_DateTime;
            else return dataType.System_String;
        }

        public static bool IsValidEmailId(string InputEmail)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(InputEmail);
            if (match.Success)
                return true;
            else
                return false;
        }

        public bool CreateForm(string userId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionnaire(string userId, IForm form, string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        /*
        public void Add(IQuestionnaire application)
        {
            //IT IS JUST FOR ROLLBACK TEST
            if (application.PurposeOfLoan == PurposeOfLoan.Vacation)
                throw new ArgumentException(nameof(application));
            Applications.Add(application);
        }

        public void Remove(IQuestionnaire application)
        {
            Applications.Remove(application);
        }

        public void Update(IQuestionnaire application)
        {
            throw new NotImplementedException();
        }

        public async Task<IQuestionnaire> GetByApplicationId(string applicationNumber)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                return applicationData;
            });
        }

        public Task UpdateApplication(string applicationNumber, IQuestionnaireUpdateRequest applicationUpdateRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<IQuestionnaire> LinkBankInformation(string applicationNumber, IBankInformation bankInformation)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.LinkedBankInformation = bankInformation;
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> LinkEmailInformation(string applicationNumber, IEmailAddress emailInfo)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.LinkedEmailAddress = emailInfo;
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> LinkIncomeInformation(string applicationNumber, IncomeInformation incomeInfo)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.LinkedIncomeInformation = incomeInfo;
                return applicationData;
            });
        }

        public Task<IQuestionnaire> UpdateSelfDeclaredExpense(string applicationId, SelfDeclareExpense declareExpense)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IQuestionnaire>> All(Expression<Func<IQuestionnaire, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IQuestionnaire, bool>> query)
        {
            throw new NotImplementedException();
        }

        public string GetApplicantId(string applicationId)
        {
            throw new NotImplementedException();
        }

        public async Task<IQuestionnaire> UpdateSelfDeclaredExpense(string applicationNumber, ISelfDeclareExpense expenseInformation)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.SelfDeclareExpense = expenseInformation;
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> LinkEmploymentInformation(string applicationNumber, IEmploymentDetail employmentInformation)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.EmploymentDetail = employmentInformation;
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> SetInitialOffers(string applicationNumber, IList<ILoanOffer> initialLoanOffers)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.InitialOffers = initialLoanOffers;
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> SetFinalOffers(string applicationNumber, IList<ILoanOffer> finalLoanOffers)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                applicationData.FinalOffers = finalLoanOffers;
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> SetSelectedInitialOffer(string applicationNumber, string offerId, string offerType)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.InitialOffers == null)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                var lstInitialOffers = applicationData.InitialOffers;

                var currentSelectedOffer = lstInitialOffers.FirstOrDefault(offer => offer.IsSelectedOffer == true);
                if (currentSelectedOffer != null)
                {
                    currentSelectedOffer.IsSelectedOffer = false;
                }

                var currentOffer = lstInitialOffers.FirstOrDefault(offer => offer.OfferId.Equals(offerId));
                if (currentOffer == null)
                    throw new NotFoundException($"Initial offer with id { offerId } is not found");
                currentOffer.IsSelectedOffer = true;

                applicationData.InitialOffers = lstInitialOffers;
               
                return applicationData;
            });
        }

        public async Task<IQuestionnaire> SetSelectedFinalOffer(string applicationNumber, string offerId, string offerType)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.FinalOffers == null)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                var lstFinalOffers = applicationData.FinalOffers;

                var currentSelectedOffer = lstFinalOffers.FirstOrDefault(offer => offer.IsSelectedOffer == true);
                if (currentSelectedOffer != null)
                {
                    currentSelectedOffer.IsSelectedOffer = false;
                }

                var currentOffer = lstFinalOffers.FirstOrDefault(offer => offer.OfferId.Equals(offerId));
                if (currentOffer == null)
                    throw new NotFoundException($"Initial offer with id { offerId } is not found");
                currentOffer.IsSelectedOffer = true;

                applicationData.FinalOffers = lstFinalOffers;

                return applicationData;
            });
        }

        public async Task<List<ILoanOffer>> GetInitalOffers(string applicationNumber)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.InitialOffers == null || applicationData.InitialOffers.ToList().Count <= 0)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                return applicationData.InitialOffers.ToList();
            });
        }

        public async Task<List<ILoanOffer>> GetFinalOffers(string applicationNumber)
        {
            return await Task.Run(() =>
            {
                var applicationData = Applications.Where(application => application.ApplicationNumber == applicationNumber).FirstOrDefault();

                if (applicationData == null)
                    throw new NotFoundException($"Application {applicationNumber} not found");

                if (applicationData.FinalOffers == null || applicationData.FinalOffers.ToList().Count <= 0)
                    throw new NotFoundException($"Initial offers for {applicationNumber} not found");

                return applicationData.FinalOffers.ToList();
            });
        }

        public Task<IQuestionnaire> LinkIncomeInformation(string applicationNumber, IIncomeInformation incomeInformation)
        {
            throw new NotImplementedException();
        }
        #endregion

        //public IQuestionnaire GetByApplicationNumber(string applicationNumber)
        //{
        //    return Applications.FirstOrDefault(applicant => applicant.ApplicationNumber.Equals(applicationNumber));
        //}

        //public List<string> GetApplicationNumbersByApplicantNumber(string applicantId)
        //{
        //    return
        //                  Applications.Where(
        //                      application => application.Applicants.Any(applicant => applicantId==applicant.ApplicantId))
        //                      .Select(application => application.ApplicationNumber).ToList();
        //}

        //public void UpdateApplication(string applicationNumber, IQuestionnaireUpdateRequest applicationRequest)
        //{
        //    var applicant = GetByApplicationNumber(applicationNumber);
        //    applicant.Amount = applicationRequest.Amount;
        //    applicant.Purpose = applicationRequest.Purpose;
        //}

        //public IQuestionnaire UpdateAddresses(string applicationNumber, string applicantId, List<IAddress> addresses)
        //{
        //    var application = GetByApplicationNumber(applicationNumber);
        //    if (application == null)
        //        throw new NotFoundException($"Application {applicationNumber} not found");
        //    var applicants = application.Applicants;
        //    var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
        //    if (applicantToUpdate == null)
        //        throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

        //    applicantToUpdate.Addresses = addresses;

        //    return application;
        //}

        //public IQuestionnaire UpdatePhoneNumbers(string applicationNumber, string applicantId, List<IPhoneNumber> phoneNumbers)
        //{
        //    var application = GetByApplicationNumber(applicationNumber);
        //    if (application == null)
        //        throw new NotFoundException($"Application {applicationNumber} not found");
        //    var applicants = application.Applicants;
        //    var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
        //    if (applicantToUpdate == null)
        //        throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

        //    applicantToUpdate.PhoneNumbers = phoneNumbers;

        //    return application;
        //}

        //public IQuestionnaire UpdateApplicant(string applicationNumber, string applicantId, IApplicantUpdateRequest applicantUpdateRequest)
        //{
        //    var application = GetByApplicationNumber(applicationNumber);
        //    if (application == null)
        //        throw new NotFoundException($"Application {applicationNumber} not found");
        //    var applicants = application.Applicants;
        //    var applicantToUpdate = applicants.FirstOrDefault(applicant => applicant.ApplicantId == applicantId);
        //    if (applicantToUpdate == null)
        //        throw new NotFoundException($"Applicant {applicantId} not found for application {applicationNumber} ");

        //    applicantToUpdate.CurrentEmployer = applicantUpdateRequest.CurrentEmployer;
        //    applicantToUpdate.EmploymentStatus = applicantUpdateRequest.EmploymentStatus;
        //    applicantToUpdate.HomeOwnership = applicantUpdateRequest.HomeOwnership;
        //    applicantToUpdate.StatedGrossAnnualIncome = applicantUpdateRequest.StatedGrossAnnualIncome;
        //    return application;
        //}


        //public Task<IEnumerable<IQuestionnaire>> All(Expression<Func<IQuestionnaire, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        //{
        //    throw new NotImplementedException();
        //}

        //public int Count(Expression<Func<IQuestionnaire, bool>> query)
        //{
        //    throw new NotImplementedException();
        //}

        //public IQuestionnaire UpdateScore(string applicationNumber, string scoreName, Score score)
        //{
        //    throw new NotImplementedException();
        //}
        */
        #endregion

    }
}
