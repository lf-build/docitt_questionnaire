﻿//using LendFoundry.Security.Identity;
//using LendFoundry.Foundation.Date;
//using CreditExchange.Applicant;

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Docitt.Questionnaire.Tests
//{
//    public class FakeQuestionnaireService : IQuestionnaireService
//    {
//        #region Constructors
//        public FakeQuestionnaireService(ITenantTime tenantTime, IEnumerable<IQuestionnaire> questionnaires) : this(tenantTime)
//        {
//            Questionnaires.AddRange(questionnaires);
//            CurrentId = Questionnaires.Count() + 1;
//        }

//        public FakeQuestionnaireService(ITenantTime tenantTime)
//        {
//            TenantTime = tenantTime;
//            CurrentId = 1;
//        }
//        #endregion

//        #region Private Properties
//        private ITenantTime TenantTime { get; set; }
//        public List<IQuestionnaire> Questionnaires { get; } = new List<IQuestionnaire>();
//        public int CurrentId { get; set; }
//        #endregion

//        #region Public Methods
//        /*public async Task<IApplicant> Add(IApplicantRequest applicantRequest)
//        {
//            IApplicant applicant = null;
//            await Task.Run(() =>
//            {
//                if (applicantRequest.UserName == "TestUserExists")
//                    throw new UsernameAlreadyExists("TestUserExists");
//                applicant = new Applicant.Applicant(applicantRequest);
//                applicant.Id = (CurrentId++).ToString();
//                Applicants.Add(applicant);
//                //response = new Applicant.Applicant(applicantRequest);
//            });
//            return applicant;
//        }

//        public void Update(string applicantId, IApplicant applicant)
//        {
//            Applicants.Remove(Applicants.First(a => a.Id.Equals(applicant.Id)));
//            Applicants.Add(applicant);
//        }

//        public async Task<IApplicant> Get(string applicantId)
//        {
//            IApplicant applicantDetail = null;
//            await Task.Run(() =>
//            {
//                applicantDetail =  Applicants.First(applicant => applicant.Id.Equals(applicantId));
//            });
//            return applicantDetail;
//        }

//        public async Task Delete(string applicantId)
//        {
//            await Task.Run(() =>
//            {
//                var applicant = Applicants.Where(a => a.Id.Equals(applicantId)).FirstOrDefault();
//                Applicants.Remove(applicant);
//            });
//        }

//        public Task UpdateApplicant(string applicantNumber, IUpdateApplicantRequest applicantRequest)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetAddresses(string applicantId, List<IAddress> addresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetAddresses(string applicantId, List<Applicant.IAddress> addresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task AssociateUser(string applicantId, string userId)
//        {
//            throw new NotImplementedException();
//        }

//        public Task UpdateAddresses(string applicantId, List<Applicant.IAddress> addresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task UpdatePhoneNumbers(string applicantId, List<Applicant.IPhoneNumber> phoneNumbers)
//        {
//            throw new NotImplementedException();
//        }

//        public Task UpdateEmailAddresses(string applicantId, List<Applicant.IEmailAddress> emailAddresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetPhoneNumbers(string applicantId, List<Applicant.IPhoneNumber> phoneNumbers)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetEmailAddresses(string applicantId, List<Applicant.IEmailAddress> emailAddresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetEmployment(string applicantId, List<Applicant.IEmploymentDetail> emailAddresses)
//        {
//            throw new NotImplementedException();
//        }

//        public Task SetBanks(string applicantId, List<Applicant.IBankInformation> banks)
//        {
//            throw new NotImplementedException();
//        }
//        */
//        public Task<IQuestionnaireService> Add(IQuestionnaireRequest questionnaireRequest)
//        {
//            throw new NotImplementedException();
//        }

//        public Task<IQuestion> GetByQuestionId(string questionId)
//        {
//            throw new NotImplementedException();
//        }

//        public Task<bool> UpdateQuestion(IQuestionRequest questionRequest, string temporaryapplicationNumber)
//        {
//            throw new NotImplementedException();
//        }

//        public async Task<IQuestionnaireResponse> GetQuestionnaire(string temporaryApplicationNumber)
//        {
//            IQuestionnaire questionnaire = null;
//            await Task.Run(() =>
//            {
//                questionnaire = Questionnaires.First(q => q.TemporaryApplicationNumber.Equals(temporaryApplicationNumber));
//            });

//            return await Task.Run(() => new QuestionnaireResponse(questionnaire));
//        }

//        #endregion
//    }
//}
