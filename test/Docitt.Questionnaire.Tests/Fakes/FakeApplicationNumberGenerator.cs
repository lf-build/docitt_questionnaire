using LendFoundry.NumberGenerator;

namespace Docitt.Questionnaire.Mocks
{
    public class FakeApplicationNumberGenerator //: IGeneratorService
    {
        #region Private Properties
        private int Id { get; set; }
        #endregion

        #region Public Methods
        public string TakeNext(string entityId)
        {
            Id++;
            return Id.ToString();
        }
        #endregion
    }
}