﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using LendFoundry.Foundation.Persistence;

namespace Docitt.Questionnaire.Mocks
{
    public class FakeFormRepository 
    {
        #region Constructors
        public FakeFormRepository(ITenantTime tenantTime, IEnumerable<IForm> forms) : this(tenantTime)
        {
            Forms.AddRange(forms);
        }
        public FakeFormRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }
        #endregion

        #region Private Properties
        public ITenantTime TenantTime { get; set; }
        public List<IForm> Forms { get; set; } = new List<IForm>();
        #endregion
      
        public bool CreateTemplateForm(string userId)
        {
            throw new NotImplementedException();
        }

        public bool AddQuestion(IQuestion question, string FormId, string sectionId)
        {
            throw new NotImplementedException();
        }

        //IQuestion IFormRepository.GetByFormId(string formId)
        //{
        //    throw new NotImplementedException();
        //}

        //IQuestion IFormRepository.UpdateForm(IFormRequest formRequest)
        //{
        //    throw new NotImplementedException();
        //}

        //List<IForm> IFormRepository.GetAllForms()
        //{
        //    throw new NotImplementedException();
        //}

        //Task<IForm> IFormRepository.GetForm(string formId)
        //{
        //    throw new NotImplementedException();
        //}


        public void CreateFormTemplate(IForm form)
        {
            Forms.Add(form);
        }

        //Task<IForm> IRepository<IForm>.Get(string id)
        //{
        //    throw new NotImplementedException();
        //}

        //Task<IEnumerable<IForm>> IRepository<IForm>.All(Expression<Func<IForm, bool>> query, int? skip, int? quantity)
        //{
        //    throw new NotImplementedException();
        //}

        //void IRepository<IForm>.Add(IForm item)
        //{
        //    throw new NotImplementedException();
        //}

        //void IRepository<IForm>.Remove(IForm item)
        //{
        //    throw new NotImplementedException();
        //}

        //void IRepository<IForm>.Update(IForm item)
        //{
        //    throw new NotImplementedException();
        //}

        //int IRepository<IForm>.Count(Expression<Func<IForm, bool>> query)
        //{
        //    throw new NotImplementedException();
        //}

        //Task<bool> IFormRepository.AddQuestion(IQuestion question, string formId, string sectionId)
        //{
        //    throw new NotImplementedException();
        //}

        public Task<IForm> GetForm(string formId)
        {
            throw new NotImplementedException();
        }

        //Task<bool> IFormRepository.CreateFormTemplate(IForm form)
        //{
        //    //throw new NotImplementedException();
        //    Forms.Add(form);

        //    return Task.Run(() => true);
        //}

        //async Task<IForm> IFormRepository.VerifyFormExists(string formName)
        //{
        //    var form = Forms.Where(f => f.FormName.ToLower() == formName.ToLower()).FirstOrDefault();
                        
        //    return await Task.Run(async () =>  form);
        //}
    }
}
