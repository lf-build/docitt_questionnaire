﻿using LendFoundry.Foundation.Date;
using LendFoundry.NumberGenerator;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Tests.Fakes
{
    public class FakeQuestionnaireService
    {
        #region Private Properties
        private IGeneratorService ApplicationNumberGenerator { get; }
        private QuestionnaireConfiguration QuestionnaireConfigurations { get; }
        private ITenantTime TenantTime { get; set; }
        private List<IQuestionnaire> Applications { get; } = new List<IQuestionnaire>();

        private List<IForm> Forms { get; } = new List<IForm>();

        private int CurrentId { get; set; }
        #endregion

        #region Constructors
        public FakeQuestionnaireService(ITenantTime tenantTime, IGeneratorService applicationNumberGenerator, QuestionnaireConfiguration questionnaireConfigurations, IEnumerable<IQuestionnaire> applications) : this(tenantTime)
        {
            Applications.AddRange(applications);
            CurrentId = Applications.Count + 1;
            ApplicationNumberGenerator = applicationNumberGenerator;
            QuestionnaireConfigurations = questionnaireConfigurations;
        }


        public FakeQuestionnaireService(ITenantTime tenantTime, IEnumerable<IQuestionnaire> questionnaires) : this(tenantTime)
        {
            Applications.AddRange(questionnaires);
            CurrentId = Applications.Count + 1;
        }

        public FakeQuestionnaireService(ITenantTime tenantTime, IEnumerable<IForm> forms) : this(tenantTime)
        {
            Forms.AddRange(forms);
            CurrentId = Applications.Count + 1;
        }

        public FakeQuestionnaireService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
            CurrentId = 1;
        }

        public Task<IQuestionnaireService> Add(IQuestionnaireRequest questionnaireRequest)
        {
            throw new NotImplementedException();
        }

        public Task<IQuestion> GetByQuestionId(string questionId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateQuestion(IQuestionRequest questionRequest, string temporaryapplicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<IQuestionnaireResponse> GetQuestionnaire(string temporaryApplicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CreateFormTemplate(IForm form)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestion(IQuestion question, string formId, string sectionId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionnaire(string userId, string formId)
        {
            throw new NotImplementedException();
        }

        public Task<bool> AddQuestionnaire()
        {
            throw new NotImplementedException();
        }
        #endregion

        /*
        public async Task<IQuestionnaireResponse> Add(IQuestionnaireRequest applicationRequest)
        {
            IQuestionnaire application = new Application(applicationRequest);
            application.ApplicationNumber = ApplicationNumberGenerator.TakeNext("application");
           //application.ApplicationStatus = ApplicationConfigurations.InitialStatus;
            application.ApplicationDate = TenantTime.Now;
            application.ExpiredDate = TenantTime.Now.AddDays(ApplicationConfigurations.ExpiryDays);
            //application.ApplicationStatus = ApplicationConfigurations.InitialStatus;
            return await Task.FromResult(new ApplicationResponse(application));
        }

        public async Task<IQuestionnaireResponse> AddMerchantApplication(IQuestionnaireRequest applicationRequest)
        {
            IQuestionnaire application = new Application(applicationRequest);
            application.ApplicationNumber = ApplicationNumberGenerator.TakeNext("application");
            //application.ApplicationStatus = ApplicationConfigurations.InitialStatus;
            application.ApplicationDate = TenantTime.Now;
            application.ExpiredDate = TenantTime.Now.AddDays(ApplicationConfigurations.ExpiryDays);
            //application.ApplicationStatus = ApplicationConfigurations.InitialLeadStatus;
            return await Task.FromResult(new ApplicationResponse(application));
        }
        */
    }
}
